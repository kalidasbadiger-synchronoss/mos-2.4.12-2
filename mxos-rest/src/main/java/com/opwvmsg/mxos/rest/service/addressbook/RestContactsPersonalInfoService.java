/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Rest Service to Contacts Personal Info level operations like Read and Update.
 * 
 * @author mxos-dev
 */
public class RestContactsPersonalInfoService extends AbstractRestService
        implements IContactsPersonalInfoService {
    private static Logger logger = Logger
            .getLogger(RestContactsPersonalInfoService.class);

    @Override
    public PersonalInfo read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_SUB_URL, inputParams);

        final PersonalInfo personalInfo = (PersonalInfo) callRest(subUrl,
                inputParams, PersonalInfo.class, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return personalInfo;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.PERSONAL_INFO_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
