/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.saml;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.saml.ISamlService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * SAML service exposed to client which is responsible for doing SAML related
 * activities e.g getSAMLRequest, decodeSAMLResponse, etc.
 * 
 * @author mxos-dev
 */
public class RestSamlService extends AbstractRestService implements
        ISamlService {
    private static Logger logger = Logger.getLogger(RestSamlService.class);

    @Override
    public String getSAMLRequest(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.SAML_AUTHENTICATION_SUB_URL, inputParams);
        String samlRequest = (String) callRest(subUrl, inputParams,
                String.class, Operation.GET);
        return samlRequest;
    }

    @Override
    public Map<String, String> decodeSAMLAssertion(
            final Map<String, List<String>> inputParams) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.SAML_ASSERTION_SUB_URL, inputParams);

        final Map<String, String> samlAttrs = (Map<String, String>) callRest(
                subUrl, inputParams, new GenericType<Map<String, String>>() {
                }, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return samlAttrs;
    }
}
