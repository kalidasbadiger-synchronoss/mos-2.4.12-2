/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksAttachmentService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.opwvmsg.mxos.task.pojos.Attachment;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Tasks Attachments Object level operations like Read, Update
 * and List.
 * 
 * @author mxos-dev
 */
public class RestTasksAttachmentService extends AbstractRestService implements
        ITasksAttachmentService {
    private static Logger logger = Logger
            .getLogger(RestTasksAttachmentService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_ATTACHMENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public Attachment read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_ATTACHMENTS_SUB_URL, inputParams);

        final Attachment attachment = (Attachment) callRest(subUrl,
                inputParams, Attachment.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return attachment;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_ATTACHMENTS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Attachment> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_ATTACHMENTS_LIST_SUB_URL, inputParams);

        final List<Attachment> attachmentList = (List<Attachment>) callRest(
                subUrl, inputParams, new GenericType<List<Attachment>>() {
                }, Operation.GET);

        return attachmentList;
    }
}
