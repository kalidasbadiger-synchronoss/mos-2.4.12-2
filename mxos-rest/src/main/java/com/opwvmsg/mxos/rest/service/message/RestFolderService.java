/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.message;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Folder service exposed to client which is responsible for doing basic Folder
 * related activities e.g create, update, read, search and delete Folder via
 * REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestFolderService extends AbstractRestService implements
        IFolderService {
    private static Logger logger = Logger.getLogger(RestFolderService.class);

    private void validateFolderNamesIsList(
            final Map<String, List<String>> inputParams) {
        if (inputParams.containsKey(FolderProperty.folderName.name())) {
            if (inputParams.get(FolderProperty.folderName.name()).get(0)
                    .equals(MxOSConstants.list)) {
                List<String> folderName = new ArrayList<String>();
                folderName
                        .add(MxOSConstants.FORWARD_SLASH + MxOSConstants.list);
                inputParams.put(FolderProperty.folderName.name(), folderName);

            }
        }
    }

    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        validateFolderNamesIsList(inputParams);
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_SUB_URL, inputParams);
        String folderId = (String) callRest(subUrl, inputParams, String.class,
                Operation.PUT);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return folderId;
    }

    @Override
    public Folder read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        validateFolderNamesIsList(inputParams);
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_SUB_URL, inputParams);
        final Folder folder = (Folder) callRest(subUrl, inputParams,
                Folder.class, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return folder;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Folder> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_LIST_SUB_URL, inputParams);
        final List<Folder> folders = (List<Folder>) callRest(subUrl,
                inputParams, new GenericType<List<Folder>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return folders;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        validateFolderNamesIsList(inputParams);
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_SUB_URL, inputParams);
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        validateFolderNamesIsList(inputParams);
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_SUB_URL, inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.FOLDER_DELETE_ALL_SUB_URL, inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
