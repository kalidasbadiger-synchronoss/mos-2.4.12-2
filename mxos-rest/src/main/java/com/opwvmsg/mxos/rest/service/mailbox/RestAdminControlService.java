/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminControlService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Family Mailbox Admin Control operations interface which will be exposed to
 * the client. This interface is responsible for doing SenderBlocking related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestAdminControlService extends AbstractRestService implements
        IAdminControlService {
    private static Logger logger = Logger
            .getLogger(RestAdminControlService.class);

    @Override
    public AdminControl read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILRECEIPT_ADMIN_CONTROL_SUB_URL, inputParams);

        final AdminControl adminControl = (AdminControl) callRest(subUrl,
                inputParams, AdminControl.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return adminControl;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILRECEIPT_ADMIN_CONTROL_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

}
