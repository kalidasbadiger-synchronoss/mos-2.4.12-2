/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * MailboxBase service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMailboxBaseService extends AbstractRestService implements
        IMailboxBaseService {
    private static Logger logger = Logger
            .getLogger(RestMailboxBaseService.class);

    @Override
    public Base read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_BASE_SUB_URL, inputParams);

        Base base = (Base) callRest(subUrl, inputParams, Base.class,
                Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return base;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        // call logging api if custom and update 'status'
        final boolean isCustom = pool.isCustom();
        if (isCustom && inputParams != null) {
            if (inputParams.containsKey(MailboxProperty.status.toString())) {
                final Map<String, List<String>> loggingParams = new HashMap<String, List<String>>();
                loggingParams.put(MailboxProperty.email.toString(),
                        new ArrayList<String>());
                loggingParams.get(MailboxProperty.email.toString()).add(
                        inputParams.get(MailboxProperty.email.toString())
                                .get(0));
                loggingParams.put(MailboxProperty.status.toString(),
                        new ArrayList<String>());
                loggingParams.get(MailboxProperty.status.toString()).add(
                        inputParams.get(MailboxProperty.status.toString()).get(
                                0));
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.MAILBOX_BASE_SUB_URL, pool.isCustom(),
                        loggingParams);

                callRest(subUrl, inputParams, Operation.POST);

                inputParams.remove(MailboxProperty.status.toString());
                logger.info("Removed entry for the key "
                        + MailboxProperty.status.toString()
                        + " from inputParams");

                // Return if there are no other base attributes exist for
                // update.
                if (!(inputParams.size() > 1)) {
                    logger.info("No other attributes exist for mailbox update. Going to return");
                    if (logger.isDebugEnabled()) {
                        logger.debug("end");
                    }
                    return;
                }
            }
            logger.info("End of update " + MailboxProperty.status.toString()
                    + " with logging api. Continuing with rest of the api");
            if (inputParams
                    .containsKey(MailboxProperty.customFields.toString())
                    && (inputParams.get(MailboxProperty.customFields.name())
                            .get(0).contains("bgcOptionsPref"))) {
                final Map<String, List<String>> loggingParams = new HashMap<String, List<String>>();
                loggingParams.put(MailboxProperty.email.toString(),
                        new ArrayList<String>());
                loggingParams.get(MailboxProperty.email.toString()).add(
                        inputParams.get(MailboxProperty.email.toString())
                                .get(0));
                loggingParams.put(MailboxProperty.customFields.toString(),
                        new ArrayList<String>());
                loggingParams.get(MailboxProperty.customFields.toString()).add(
                        inputParams
                                .get(MailboxProperty.customFields.toString())
                                .get(0));
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.MAILBOX_BASE_SUB_URL, pool.isCustom(),
                        loggingParams);

                callRest(subUrl, inputParams, Operation.POST);

                // remove the key "bgcOptionsPref" entry from inputParams
                Map<String, String> cfmap = getMapFromString("Base",
                        inputParams.get(MailboxProperty.customFields.name())
                                .get(0));
                if (cfmap.containsKey("bgcOptionsPref")) {
                    cfmap.remove("bgcOptionsPref");
                }
                logger.info("Removed entry for the key bgcOptionsPref from inputParams");
                final StringBuffer customData = new StringBuffer();
                for (String key : cfmap.keySet()) {
                    // add this attribute data to customData
                    customData.append(key).append(":")
                            .append(cfmap.get(key).toString()).append("|");
                }
                final int len = customData.length();
                if (len > 0) {
                    customData.replace(len - 1, len, "");
                }
                inputParams.remove(MailboxProperty.customFields.toString());
                logger.info("Removed entry for the key "
                        + MailboxProperty.customFields.toString()
                        + " from inputParams");
                if (cfmap.size() > 0) {
                    inputParams.put(MailboxProperty.customFields.toString(),
                            new ArrayList<String>());
                    inputParams.get(MailboxProperty.customFields.toString())
                            .add(customData.toString());
                }
                // Return if there are no other base attributes exist for
                // update.
                if (!(inputParams.size() > 1)) {
                    logger.info("No other attributes exist for mailbox update. Going to return");
                    if (logger.isDebugEnabled()) {
                        logger.debug("end");
                    }
                    return;
                }
            }
        }

        // end of update 'status'
        // continue update of other base attributes
        logger.info("End of update bgcOptionsPref with logging api. Continuing with rest of the api");
        subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_BASE_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Base> search(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILBOX_BASE_SEARCH_SUB_URL, inputParams);
        List<Base> baseObjects = (List<Base>) callRest(subUrl, inputParams,
                new GenericType<List<Base>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return baseObjects;
    }

    /**
     * Static utility to convert customFields blob string to Map.
     * 
     * @param operationName API
     * @param input customFields
     * @return Map of strings
     * @throws MxOSException exception if any parsing error.
     */
    public static Map<String, String> getMapFromString(
            final String operationName, final String input)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        // TODO: Move this into utility
        Map<String, String> output = new HashMap<String, String>();
        if (input != null) {
            String[] entries = input.split("\\|");
            for (String entry : entries) {
                String[] values = entry.split("\\:");
                if (values.length > 1) {
                    output.put(values[0], values[1]);
                } else {
                    // null and empty are NOT allowed for COS APIs.
                    if (operationName.contains(MxOSConstants.COS_BASE_ENTITY)) {
                        // throw an error
                        throw new InvalidRequestException(
                                MailboxError.MBX_INVALID_CUSTOMFIELD.name());
                    } else {
                        // Null and empty are allowed in mailbox APIs.
                        output.put(values[0], "");
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return output;
    }
}
