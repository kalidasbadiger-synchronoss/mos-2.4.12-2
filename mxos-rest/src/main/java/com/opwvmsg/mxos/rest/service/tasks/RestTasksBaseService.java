/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksBaseService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Tasks Base Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestTasksBaseService extends AbstractRestService implements
        ITasksBaseService {
    private static Logger logger = Logger.getLogger(RestTasksBaseService.class);

    @Override
    public TaskBase read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_BASE_SUB_URL, inputParams);

        final TaskBase taskBase = (TaskBase) callRest(subUrl, inputParams,
                TaskBase.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return taskBase;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_BASE_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<TaskBase> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_BASE_LIST_SUB_URL, pool.isCustom(),
                inputParams);

        final List<TaskBase> taskList = (List<TaskBase>) callRest(subUrl,
                inputParams, new GenericType<List<TaskBase>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return taskList;
    }

}
