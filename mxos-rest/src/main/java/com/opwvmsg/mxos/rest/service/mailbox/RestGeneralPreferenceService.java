/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGeneralPreferenceService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * GeneralPreference service exposed to client which is responsible for doing
 * basic GeneralPreference related activities e.g read GeneralPreference, update
 * locale via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestGeneralPreferenceService extends AbstractRestService implements
        IGeneralPreferenceService {
    private static Logger logger = Logger
            .getLogger(RestGeneralPreferenceService.class);

    @Override
    public GeneralPreferences read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GENERAL_PREFERENCES_SUB_URL, inputParams);

        final GeneralPreferences generalPreferences = (GeneralPreferences) callRest(
                subUrl, inputParams, GeneralPreferences.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return generalPreferences;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GENERAL_PREFERENCES_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
