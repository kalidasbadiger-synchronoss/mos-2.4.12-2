/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.rest.service;

import java.util.Map.Entry;
import java.util.Properties;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceLoader;
import com.opwvmsg.mxos.interfaces.service.ServiceObjectEnum;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class RestMxOSContext extends IMxOSContext {
    private static Logger logger = Logger.getLogger(RestMxOSContext.class);

    /**
     * Default constructor.
     * 
     * @throws Exception
     */
    public RestMxOSContext() throws Exception {
        logger.info("RestMxOSContext created...");
    }

    @Override
    public void init(final String contextId, final Properties props) throws MxOSException {
        if (contextId == null || contextId.isEmpty()) {
            throw new IllegalArgumentException("Null or Empty contextId: " + contextId);
        }
        if (props == null || props.isEmpty()) {
            throw new IllegalArgumentException("Null or Empty props: " + props);
        }
        this.contextId = contextId;
        final String loadServices = props.getProperty("loadServices");
        ServiceObjectEnum[] services = null;
        if (loadServices != null && !loadServices.isEmpty()) {
            String[] servicesInConf = loadServices.trim().split(",");
            services = new ServiceObjectEnum[servicesInConf.length];
            for (int i = 0; i < servicesInConf.length; i++) {
                services[i] = ServiceObjectEnum.valueOf(servicesInConf[i].trim());
            }
        }
        serviceMap = ServiceLoader.loadServices(ContextEnum.REST, services);
        RestConnectionPool pool = new RestConnectionPool(props);
        for (Entry<String, Object> entry : serviceMap.entrySet()) {
            ((AbstractRestService)entry.getValue()).init(pool);
        }
    }

    @Override
    public void destroy() throws MxOSException {
        serviceMap.clear();
    }
}