/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Mailbox external account operations interface which will be exposed to the
 * client. This interface is responsible for doing mailbox external account
 * related operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestExternalAccountService extends AbstractRestService implements
        IExternalAccountsService {
    private static Logger logger = Logger
            .getLogger(RestExternalAccountService.class);

    @Override
    public ExternalAccounts read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.EXTERNALACCOUNT_SUB_URL, inputParams);

        final ExternalAccounts externalAccounts = (ExternalAccounts) callRest(
                subUrl, inputParams, ExternalAccounts.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return externalAccounts;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.EXTERNALACCOUNT_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
