/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Address Book Groups base Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestGroupsBaseService extends AbstractRestService implements IGroupsBaseService {
    private static Logger logger = Logger
            .getLogger(RestGroupsBaseService.class);

    @Override
    public List<GroupBase> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_BASE_LIST_SUB_URL, pool.isCustom(),
                inputParams);

        final List<GroupBase> groupBaseList = (List<GroupBase>) callRest(
                subUrl, inputParams, new GenericType<List<GroupBase>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return groupBaseList;
    }

    @Override
    public GroupBase read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_BASE_SUB_URL, inputParams);

        final GroupBase groupBase = (GroupBase) callRest(subUrl, inputParams,
                GroupBase.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return groupBase;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_BASE_SUB_URL, inputParams);;
        
        callRest(subUrl, inputParams, Operation.POST);
        
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
