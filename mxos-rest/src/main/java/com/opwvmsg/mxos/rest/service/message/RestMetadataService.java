/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.message;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SearchTermParser;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Message Meta Data service exposed to client which is responsible for reading
 * the message meta data. Message via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMetadataService extends AbstractRestService implements IMetadataService {
    private static Logger logger = Logger
            .getLogger(RestMetadataService.class);

    @Override
    public Metadata read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_SUB_URL, inputParams);
        final Metadata metadata = (Metadata) callRest(subUrl, inputParams,
                Metadata.class, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return metadata;
    }

    @Override
    public Map<String, Metadata> list(
            final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_LIST_SUB_URL, inputParams);
        final Map<String, Metadata> metaDataList = (Map<String, Metadata>) callRest(
                subUrl, inputParams, new GenericType<Map<String, Metadata>>() {
                }, Operation.GET);
        return metaDataList;
    }
    
    @Override
    public Map<String, Metadata> listUIDs(
            final Map<String, List<String>> inputParams) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_UID_LIST_SUB_URL, inputParams);
        
        final Map<String, Metadata> metaDatas = (Map<String, Metadata>) callRest(
                subUrl, inputParams, new GenericType<Map<String, Metadata>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return metaDatas;
    }
    
    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        if (inputParams == null || inputParams.size() <= 3) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_BAD_REQUEST.name());
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_SUB_URL, inputParams);
        
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }
    
    @Override
    public void updateMulti(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_MULTI_METADATA_SUB_URL, inputParams);
        
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }    

    @Override
    public Map<String, Metadata> readMulti(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_MULTI_METADATA_SUB_URL, inputParams);
        
        final Map<String, Metadata> metaDatas = (Map<String, Metadata>) callRest(
                subUrl, inputParams, new GenericType<Map<String, Metadata>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return metaDatas;
    }

    @Override
    public Map<String, Map<String, Metadata>> search(Map<String, List<String>> inputParams,
            SearchTerm searchTerm) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_SEARCH_SUB_URL, inputParams);
        if (searchTerm != null) {
            final String queryUrl = SearchTermParser.parse(searchTerm);
            inputParams.put(MxOSConstants.QUERY, Arrays.asList(queryUrl));
        }
        final Map<String, Map<String, Metadata>> metaDatas = (Map<String, Map<String, Metadata>>) callRest(
                subUrl, inputParams,
                new GenericType<Map<String, Map<String, Metadata>>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return metaDatas;
    }

    @Override
    public List<String> listUUIDs(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_METADATA_UUID_LIST_SUB_URL, inputParams);
        
        final List<String> metaDatas = (List<String>) callRest(subUrl,
                inputParams, new GenericType<List<String>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return metaDatas;
    }
    
}
