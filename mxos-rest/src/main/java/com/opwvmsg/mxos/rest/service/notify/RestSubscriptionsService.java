package com.opwvmsg.mxos.rest.service.notify;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

public class RestSubscriptionsService extends AbstractRestService implements
        ISubscriptionsService {
    private static Logger logger = Logger
            .getLogger(RestSubscriptionsService.class);

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC_SUBS, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public List<String> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC_SUBS, inputParams);

        final List<String> subscriptionList = (List<String>) callRest(subUrl,
                inputParams, new GenericType<List<String>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return subscriptionList;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        logger.error("UPDATE not supported for NotifySubscription.");
        throw new UnsupportedOperationException(
                "UPDATE not supported for NotifySubscription.");
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.NOTIFY_TOPIC_SUBS, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

}
