/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworkSiteService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Mailbox Social Network Site operations interface which will be exposed to the
 * client. This interface is responsible for doing SocialNetworkSite related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestSocialNetworkSiteService extends AbstractRestService implements
        ISocialNetworkSiteService {
    private static Logger logger = Logger
            .getLogger(RestSocialNetworkSiteService.class);

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> socialNetworkSite = inputParams
                .get(MailboxProperty.socialNetworkSite.name());
        if (socialNetworkSite != null && socialNetworkSite.size() > 1) {
            temp = new StringBuffer(RestConstants.SOCIALNETWORKS_SITE_SUB_URL)
                    .toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.SOCIALNETWORKS_SINGLE_SITE_SUB_URL)
                    .toString();
        }
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

    @Override
    public List<SocialNetworkSite> read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.SOCIALNETWORKS_SITE_SUB_URL, inputParams);

        final List<SocialNetworkSite> socialNetworkSites = (List<SocialNetworkSite>) callRest(
                subUrl, inputParams,
                new GenericType<List<SocialNetworkSite>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return socialNetworkSites;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.SOCIALNETWORKS_SITE_SUB_URL).append(
                "/{socialNetworkSite}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.SOCIALNETWORKS_SITE_SUB_URL).append(
                "/{socialNetworkSite}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
    }

}
