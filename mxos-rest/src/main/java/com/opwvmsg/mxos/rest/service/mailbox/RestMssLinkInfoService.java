/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMssLinkInfoService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * MailboxBase service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMssLinkInfoService extends AbstractRestService implements
        IMssLinkInfoService {
    private static Logger logger = Logger
            .getLogger(RestMssLinkInfoService.class);

    @Override
    public MssLinkInfo read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MSS_LINK_INFO_SUB_URL, inputParams);

        final MssLinkInfo mssLinkInfo = (MssLinkInfo) callRest(subUrl,
                inputParams, MssLinkInfo.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return mssLinkInfo;
    }
}
