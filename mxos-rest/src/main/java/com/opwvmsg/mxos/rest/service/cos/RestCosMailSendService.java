/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.cos;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestCRUD;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.ClientResponse;

/**
 * Rest Service to COS MailSend Object level operations like Read,
 * Update and List.
 *
 * @author mxos-dev
 */
public class RestCosMailSendService extends AbstractRestService implements ICosMailSendService {
    private static Logger logger = Logger
            .getLogger(RestCosMailSendService.class);

    /**
     * Default Constructor.
     *
     * @throws Exception on any error.
     */
    public RestCosMailSendService() throws MxOSException {
    }

    @Override
    public MailSend read(Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.COS_MAILSEND_SUB_URL, inputParams);
        
        final MailSend mailSend = (MailSend) callRest(subUrl, inputParams, MailSend.class, Operation.GET);
        
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return mailSend;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
        throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.COS_MAILSEND_SUB_URL, inputParams);
        
        callRest(subUrl, inputParams, Operation.POST);
        
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
