/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.message;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.message.IHeaderService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Rest Header service exposed to client which is responsible for doing basic
 * message Header related activities e.g read Header, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class RestHeaderService extends AbstractRestService implements
        IHeaderService {
    private static Logger logger = Logger.getLogger(RestHeaderService.class);

    @Override
    public Header read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_HEADER_SUB_URL, inputParams);

        final Header header = (Header) callRest(subUrl, inputParams,
                Header.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return header;
    }
}
