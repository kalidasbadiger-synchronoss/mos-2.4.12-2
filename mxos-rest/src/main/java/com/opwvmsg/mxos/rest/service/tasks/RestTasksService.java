/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.tasks;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.opwvmsg.mxos.task.pojos.Task;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Tasks Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public class RestTasksService extends AbstractRestService implements
        ITasksService {
    private static Logger logger = Logger.getLogger(RestTasksService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_SUB_URL, pool.isCustom(), inputParams);
        long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return id;
    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_SUB_URL, pool.isCustom(), inputParams);
        try {
            File file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams.get(TasksProperty.tasksList.name())
                    .get(0).getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(TasksProperty.tasksList.name());
            List<Long> ids = (List<Long>) callPostMultiValue(subUrl,
                    inputParams, file, new GenericType<List<Long>>() {
                    });
            if (logger.isDebugEnabled()) {
                logger.debug("end");
            }
            return ids;
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.TASKS_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_DELETE_SUB_URL, pool.isCustom(),
                inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_SUB_URL, pool.isCustom(), inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public Task read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_READ_SUB_URL, pool.isCustom(), inputParams);

        Task task = (Task) callRest(subUrl, inputParams, Task.class,
                Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return task;
    }

    @Override
    public List<Task> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_LIST_SUB_URL, pool.isCustom(), inputParams);
        List<Task> tasks = (List<Task>) callRest(subUrl, inputParams,
                new GenericType<List<Task>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return tasks;
    }

    @Override
    public void confirm(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.TASKS_CONFIRM_SUB_URL, pool.isCustom(),
                inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

}
