/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalStoreService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Mailbox External Store operations interface which will be exposed to the
 * client. This interface is responsible for doing ExternalStore related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestExternalStoreService extends AbstractRestService implements
        IExternalStoreService {
    private static Logger logger = Logger
            .getLogger(RestExternalStoreService.class);

    @Override
    public ExternalStore read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.EXTERNAL_STORE_SUB_URL, inputParams);

        final ExternalStore externalStore = (ExternalStore) callRest(subUrl,
                inputParams, ExternalStore.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return externalStore;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.EXTERNAL_STORE_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
