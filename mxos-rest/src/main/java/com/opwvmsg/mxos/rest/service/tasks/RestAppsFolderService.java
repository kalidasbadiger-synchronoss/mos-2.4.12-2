/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Tasks Base Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestAppsFolderService extends AbstractRestService implements
        IAppsFolderService {
    private static Logger logger = Logger
            .getLogger(RestAppsFolderService.class);

    private String getModule(final Map<String, List<String>> inputParams) {
        if (inputParams.containsKey(TasksProperty.module.name())) {
            return inputParams.get(TasksProperty.module.name()).get(0);
        } else {
            return null;
        }
    }

    @Override
    public Folder read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (getModule(inputParams).equals("task")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.TASKS_FOLDER_ID_SUB_URL, inputParams);
        } else if (getModule(inputParams).equals("addressBook")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.ABS_FOLDER_ID_SUB_URL, inputParams);
        } else {
            throw new InvalidRequestException(
                    TasksError.TSK_INVALID_MODULE.name());
        }
        Folder folder = (Folder) callRest(subUrl, inputParams, Folder.class,
                Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return folder;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        try {
            if (getModule(inputParams).equals("task")) {
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.TASKS_FOLDER_ID_SUB_URL, inputParams);
            } else if (getModule(inputParams).equals("addressBook")) {
                subUrl = RestConstants.getFinalSubUrl(
                        RestConstants.ABS_FOLDER_ID_SUB_URL, inputParams);
            } else {
                throw new InvalidRequestException(
                        TasksError.TSK_INVALID_MODULE.name());
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        }
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Folder> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (getModule(inputParams).equals("task")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.TASKS_FOLDER_LIST_SUB_URL, inputParams);
        } else if (getModule(inputParams).equals("addressBook")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.ABS_FOLDER_LIST_SUB_URL, inputParams);
        } else {
            throw new InvalidRequestException(
                    TasksError.TSK_INVALID_MODULE.name());
        }
        final List<Folder> foldersList = (List<Folder>) callRest(subUrl,
                inputParams, new GenericType<List<Folder>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return foldersList;
    }

    @Override
    public long create(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (getModule(inputParams).equals("task")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.TASKS_FOLDER_SUB_URL, inputParams);
        } else if (getModule(inputParams).equals("addressBook")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.ABS_FOLDER_SUB_URL, inputParams);
        } else {
            throw new InvalidRequestException(
                    TasksError.TSK_INVALID_MODULE.name());
        }
        long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return id;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (getModule(inputParams).equals("task")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.TASKS_FOLDER_ID_SUB_URL, inputParams);
        } else if (getModule(inputParams).equals("addressBook")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.ABS_FOLDER_ID_SUB_URL, inputParams);
        } else {
            throw new InvalidRequestException(
                    TasksError.TSK_INVALID_MODULE.name());
        }
        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (getModule(inputParams).equals("task")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.TASKS_FOLDERS_SUB_URL, inputParams);
        } else if (getModule(inputParams).equals("addressBook")) {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.ABS_FOLDERS_SUB_URL, inputParams);
        } else {
            throw new InvalidRequestException(
                    TasksError.TSK_INVALID_MODULE.name());
        }
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}