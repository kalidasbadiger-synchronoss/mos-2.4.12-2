/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAddressForDeliveryService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Mailbox Address For Delivery operations interface which will be exposed to
 * the client. This interface is responsible for doing address for delivery
 * related operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestAddressForDeliveryService extends AbstractRestService
        implements IAddressForDeliveryService {
    private static Logger logger = Logger
            .getLogger(RestAddressForDeliveryService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> addressesForLocalDelivery = inputParams
                .get(MailboxProperty.addressesForLocalDelivery.name());
        if (addressesForLocalDelivery != null
                && addressesForLocalDelivery.size() > 1) {
            temp = new StringBuffer(
                    RestConstants.ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL)
                    .toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.SINGLE_ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL)
                    .toString();
        }
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants
                .getFinalSubUrl(
                        RestConstants.ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL,
                        inputParams);

        final List<String> fwdAddressList = (List<String>) callRest(subUrl,
                inputParams, new GenericType<List<String>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return fwdAddressList;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL)
                .append("/{oldAddressForLocalDelivery}")
                .append("/{newAddressForLocalDelivery}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.ADDRESSES_FOR_LOCAL_DELIVERY_SUB_URL).append(
                "/{addressForLocalDelivery}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

}
