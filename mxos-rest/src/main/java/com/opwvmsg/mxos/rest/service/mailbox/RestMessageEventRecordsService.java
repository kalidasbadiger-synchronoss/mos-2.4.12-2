/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMessageEventRecordsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * MessageEventRecords service exposed to client which is responsible for doing
 * basic MessageEventRecords related activities e.g read and update
 * MessageEventRecords via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestMessageEventRecordsService extends AbstractRestService
        implements IMessageEventRecordsService {
    private static Logger logger = Logger
            .getLogger(RestMessageEventRecordsService.class);

    @Override
    public MessageEventRecords read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_EVENT_RECORDS_SUB_URL, inputParams);

        final MessageEventRecords messageEventRecords = (MessageEventRecords) callRest(
                subUrl, inputParams, MessageEventRecords.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return messageEventRecords;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MESSAGE_EVENT_RECORDS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
