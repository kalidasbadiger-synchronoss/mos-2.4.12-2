/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsExternalMembersService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Address Book Group external members Object level operations
 * like Create, Read, Update and Delete.
 * 
 * @author mxos-dev
 */
public class RestGroupsExternalMembersService extends AbstractRestService
        implements IGroupsExternalMembersService {
    private static Logger logger = Logger
            .getLogger(RestGroupsExternalMembersService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_EXTERNALMEMBERS_SUB_URL, inputParams);

        final long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);

        return 0;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_EXTERNALMEMBERS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_ALL_EXTERNALMEMBERS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public ExternalMember read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_EXTERNALMEMBERS_SUB_URL, inputParams);

        final ExternalMember externalMember = (ExternalMember) callRest(subUrl,
                inputParams, ExternalMember.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return externalMember;
    }

    @Override
    public List<ExternalMember> readAll(
            final Map<String, List<String>> inputParams) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_ALL_EXTERNALMEMBERS_SUB_URL, inputParams);

        final List<ExternalMember> externalMembersList = (List<ExternalMember>) callRest(
                subUrl, inputParams, new GenericType<List<ExternalMember>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return externalMembersList;
    }

}
