/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Address Book Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestContactsBaseService extends AbstractRestService implements
        IContactsBaseService {
    private static Logger logger = Logger
            .getLogger(RestContactsBaseService.class);

    @Override
    public List<ContactBase> list(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_BASE_LIST_SUB_URL, pool.isCustom(),
                inputParams);
        List<ContactBase> contacts = (List<ContactBase>) callRest(subUrl,
                inputParams, new GenericType<List<ContactBase>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return contacts;
    }

    @Override
    public ContactBase read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_BASE_SUB_URL, inputParams);

        ContactBase contact = (ContactBase) callRest(subUrl, inputParams,
                ContactBase.class, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return contact;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = null;
        subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_BASE_SUB_URL, inputParams);
        callRest(subUrl, inputParams, Operation.POST);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<ContactBase> search(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.CONTACTS_BASE_SEARCH_SUB_URL, inputParams);

        List<ContactBase> contacts = (List<ContactBase>) callRest(subUrl,
                inputParams, new GenericType<List<ContactBase>>() {
                }, Operation.GET);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return contacts;
    }
}
