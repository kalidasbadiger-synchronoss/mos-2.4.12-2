/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Mailbox SMS Notification Service operations interface which will be exposed
 * to the client. This interface is responsible for doing mailbox SMS
 * Notification Service related operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestSmsNotificationsService extends AbstractRestService implements
        ISmsNotificationsService {
    private static Logger logger = Logger
            .getLogger(RestSmsNotificationsService.class);

    public RestSmsNotificationsService() throws MxOSException {
    }

    @Override
    public SmsNotifications read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.SMSSERVICES_SMSNOTIFICATION_SUB_URL, inputParams);

        final SmsNotifications smsNotifications = (SmsNotifications) callRest(
                subUrl, inputParams, SmsNotifications.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return smsNotifications;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.SMSSERVICES_SMSNOTIFICATION_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
