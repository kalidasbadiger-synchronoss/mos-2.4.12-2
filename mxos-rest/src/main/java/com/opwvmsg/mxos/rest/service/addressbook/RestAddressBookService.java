/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IAddressBookService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Rest Service to Address Book Object level operations like Read, Update and
 * List.
 * 
 * @author mxos-dev
 */
public class RestAddressBookService extends AbstractRestService implements IAddressBookService {
    private static Logger logger = Logger
            .getLogger(RestAddressBookService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.ADDRESS_BOOK_SUB_URL, pool.isCustom(),
                inputParams);
        long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return id;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.ADDRESS_BOOK_SUB_URL, pool.isCustom(),
                inputParams);
        callRest(subUrl, inputParams, Operation.DELETE);
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
