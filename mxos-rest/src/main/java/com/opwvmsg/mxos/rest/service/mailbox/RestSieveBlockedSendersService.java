/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISieveBlockedSendersService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Sieve Blocked Senders service exposed to client which is responsible for
 * doing basic Sieve Blocked Senders related activities e.g create, update, read
 * and delete of Sieve Blocked Senders via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestSieveBlockedSendersService extends AbstractRestService
        implements ISieveBlockedSendersService {
    private static Logger logger = Logger
            .getLogger(RestSieveBlockedSendersService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp;
        List<String> sieveFilterBlockedSender = inputParams
                .get(MailboxProperty.sieveFilterBlockedSender.name());
        if (sieveFilterBlockedSender != null
                && sieveFilterBlockedSender.size() > 1) {
            temp = new StringBuffer(
                    RestConstants.MAILRECEIPT_SIEVE_BLOCKED_SENDERS_SUB_URL)
                    .toString();
        } else {
            temp = new StringBuffer(
                    RestConstants.MAILRECEIPT_SINGLE_SIEVE_BLOCKED_SENDERS_SUB_URL)
                    .toString();
        }
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.MAILRECEIPT_SIEVE_BLOCKED_SENDERS_SUB_URL,
                inputParams);

        final List<String> sieveBlockedSenderList = (List<String>) callRest(
                subUrl, inputParams, new GenericType<List<String>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return sieveBlockedSenderList;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILRECEIPT_SIEVE_BLOCKED_SENDERS_SUB_URL)
                .append("/{oldBlockedSender}/{newBlockedSender}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String temp = new StringBuffer(
                RestConstants.MAILRECEIPT_SIEVE_BLOCKED_SENDERS_SUB_URL)
                .append("/{blockedSender}").toString();
        final String subUrl = RestConstants.getFinalSubUrl(temp, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
