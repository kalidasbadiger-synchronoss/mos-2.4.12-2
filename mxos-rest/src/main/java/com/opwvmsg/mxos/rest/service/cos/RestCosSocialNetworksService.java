/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.rest.service.cos;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSocialNetworksService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Mailbox Social Network operations interface which will be exposed to the
 * client. This interface is responsible for doing SocialNetworks related
 * operations (like Read, Update etc.).
 * 
 * @author mxos-dev
 */
public class RestCosSocialNetworksService extends AbstractRestService implements
        ICosSocialNetworksService {
    private static Logger logger = Logger
            .getLogger(RestCosSocialNetworksService.class);

    @Override
    public SocialNetworks read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.COS_SOCIALNETWORKS_SUB_URL, inputParams);

        final SocialNetworks socialNetworks = (SocialNetworks) callRest(subUrl,
                inputParams, SocialNetworks.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return socialNetworks;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.COS_SOCIALNETWORKS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

}
