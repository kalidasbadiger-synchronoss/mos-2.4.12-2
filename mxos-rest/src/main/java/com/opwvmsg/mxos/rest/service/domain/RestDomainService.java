/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.domain;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestDomainService extends AbstractRestService implements
        IDomainService {
    private static Logger logger = Logger.getLogger(RestDomainService.class);

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.DOMAIN_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public Domain read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.DOMAIN_SUB_URL, inputParams);

        final Domain domain = (Domain) callRest(subUrl, inputParams,
                Domain.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return domain;
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.DOMAIN_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.DOMAIN_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public List<Domain> search(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.DOMAIN_SEARCH_SUB_URL, inputParams);

        final List<Domain> domainList = (List<Domain>) callRest(subUrl,
                inputParams, new GenericType<List<Domain>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return domainList;
    }
}
