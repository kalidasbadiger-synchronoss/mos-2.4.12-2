/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.rest.service.addressbook;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsMembersService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;
import com.sun.jersey.api.client.GenericType;

/**
 * Rest Service to Address Book Group member Object level operations like
 * Create, Read, Update and Delete.
 * 
 * @author mxos-dev
 */
public class RestGroupsMembersService extends AbstractRestService implements
        IGroupsMembersService {
    private static Logger logger = Logger
            .getLogger(RestGroupsMembersService.class);

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_MEMBERS_SUB_URL, inputParams);

        long id = (Long) callRest(subUrl, inputParams, Long.class,
                Operation.PUT);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return id;
    }

    @Override
    public List<Long> createMulti(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_MULTI_MEMBERS_SUB_URL, inputParams);
        File file = null;
        try {
            file = File.createTempFile(MxOSConstants.EXTENSION,
                    MxOSConstants.EXTENSION);
            byte[] dataBytes = inputParams
                    .get(AddressBookProperty.memberList.name()).get(0)
                    .getBytes(MxOSConstants.UTF8);
            FileUtils.writeByteArrayToFile(file, dataBytes);
            inputParams.remove(AddressBookProperty.memberList.name());
        } catch (final Exception e) {
            logger.error("Exception encountered", e);
            throw new MxOSException(ErrorCode.MXS_CONNECTION_ERROR.name(), e);
        }

        final List<Long> groupMemberIds = (List<Long>) callPostMultiValue(
                subUrl, inputParams, file, new GenericType<List<Long>>() {
                });
        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return groupMemberIds;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_MEMBERS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_ALL_MEMBERS_SUB_URL, inputParams);

        callRest(subUrl, inputParams, Operation.DELETE);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }

    @Override
    public Member read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_MEMBERS_SUB_URL, inputParams);

        final Member member = (Member) callRest(subUrl, inputParams,
                Member.class, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return member;
    }

    @Override
    public List<Member> readAll(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }

        final String subUrl = RestConstants.getFinalSubUrl(
                RestConstants.GROUPS_ALL_MEMBERS_SUB_URL, inputParams);

        final List<Member> memberList = (List<Member>) callRest(subUrl,
                inputParams, new GenericType<List<Member>>() {
                }, Operation.GET);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return memberList;
    }

}
