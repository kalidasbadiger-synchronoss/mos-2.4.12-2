/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.rest.service.process;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;
import com.opwvmsg.mxos.rest.service.AbstractRestService;
import com.opwvmsg.mxos.rest.service.RestConstants;

/**
 * Send Mail service exposed to client which is responsible for sending the mail
 * via REST API of MxOS.
 * 
 * @author mxos-dev
 */
public class RestSendMailService extends AbstractRestService implements
        ISendMailService {
    private static Logger logger = Logger.getLogger(RestSendMailService.class);

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        final String subUrl;
        if (inputParams.containsKey(SmsProperty.toAddress.name())) {
            subUrl = RestConstants.getFinalSubUrl(RestConstants.SEND_MAIL_URL,
                    inputParams);
        } else {
            subUrl = RestConstants.getFinalSubUrl(
                    RestConstants.SEND_MAIL_SUB_URL, inputParams);
        }

        callRest(subUrl, inputParams, Operation.POST);

        if (logger.isDebugEnabled()) {
            logger.debug("end");
        }
        return;
    }
}
