/*
 * Copyright (c) 2005-2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 *
 * $Id: //paf/paf31-903-1105-108-1/util/src/com/openwave/intermail/mail/GroupHeartBeatListener.java#6 $
 *
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.net.DatagramPacket;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;



/**
 * Class manages the active mss list. Class will instantiated when Mx config key -"disableRedirectToSurrogateMSS" 
 * is false. This class extends the thread and that main thread continuously receives heartbeats for each mss mention in  
 * clusterhashmap from Multicastgroup address and maintains a thread for each mss. 
 * It also checks for dead mss servers once at the time of start up.
 * 
 * @author ashwini_gandhi
 * 
 */
public class GroupHeartBeatListener extends Thread {

    private static final Logger logger = Logger
            .getLogger(GroupHeartBeatListener.class);

    private static GroupHeartBeatListener groupHeartBeatListener;
    private MulticastSocket socket = null;
    private SocketAddress group = null;

    // This map stores 'cluster:mssHost' as key and its corresponding thread as
    // its value.
    private static HashMap mssThreadpoolMap = null;

    // This timer used to schedule a task which remove dead mssHost from
    // acliveList at startup time.
    private Timer timer = null;

    private static boolean status = false;

    private int count = 0;

    private boolean heartbeatListenToAllInterfaces = true;

    /**
     * Static method which returns instance of GroupHeartBeatListener.
     * 
     * @return GroupHeartBeatListener instance of GroupHeartBeatListener
     */
    public static synchronized GroupHeartBeatListener getInstance() {
        if (null == groupHeartBeatListener || !status) {
            groupHeartBeatListener = new GroupHeartBeatListener();
        }
        return groupHeartBeatListener;
    }

    /*
     * GroupHeartBeatListener Constructor It initialize mssThreadpoolMap. It adds
     * a key for each mss in clusterhashmap. It does not start a
     * MssHeartBeatListener here. Instead it puts null as value.
     */
    private GroupHeartBeatListener() {
        logger.info("Active mss list feature is enabled");
        // Get keySet from MssHostsTableHandler
        MssHostsTableHandler handler = MssHostsTableHandler.getInstance();
        Set keyset = handler.getMssHostKeySet();
        int totalMss = keyset.size();
        if (mssThreadpoolMap == null) {
            mssThreadpoolMap = new HashMap();
        }
        Config config = Config.getInstance();
        if (keyset != null) {
            Iterator it = keyset.iterator();
            while (it.hasNext()) {
                String host = (String) it.next();
                MssHeartBeatListener mssthread = null;
                mssThreadpoolMap.put(host, mssthread);
            }
        }

        heartbeatListenToAllInterfaces = config
                .getBoolean("heartbeatListenToAllInterfaces",true);
				
        if (logger.isDebugEnabled()) {
            logger.debug("GroupHeartBeatListener Thread is initiated");    
        }
		
        // After this TimeInterval DOWN Mss will be idetified and will be remove from the Active List.
        long mssStatusCheckInterval = 5;
        try {		
            long heartbeatInterval = config
                            .getInt(IntermailConfig.HEARTBEAT_INTERVAL, 1);
            mssStatusCheckInterval = totalMss * heartbeatInterval ;
            if (logger.isDebugEnabled()) {
                logger.debug("mssStatusCheckInterval = "+mssStatusCheckInterval);
            }
        } catch (ConfigException cfe) {
            logger.error(cfe.getMessage());
        }		
        status = false;
        // Schedule a task to remove dead mss servers from active list.
        timer = new Timer();
        timer.schedule(new CheckDeadMssTask(), mssStatusCheckInterval*1000);
    }
	
    /**
     * @param status
     *  If true- thread is running. If false- thread is exited form
     *  run method
     */
    public static void setStatus(boolean status) {
        GroupHeartBeatListener.status = status;
    }
	
	/**
     * @return boolean status
     */
    public static boolean isStatus() {
		return status;
	}

    public void run() {
        while (!isInterrupted()) {
            try {
                Config config = Config.getInstance();
                String HEARTBEAT_GROUP = config
                        .get(IntermailConfig.HEARTBEAT_GROUP);
                int HEARTBEAT_PORT = config
                        .getInt(IntermailConfig.HEARTBEAT_PORT);

                socket = new MulticastSocket(HEARTBEAT_PORT);
                group = new InetSocketAddress (HEARTBEAT_GROUP,HEARTBEAT_PORT);

                if (heartbeatListenToAllInterfaces) {
                    NetworkInterface intf;
                    for(Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) 
                    {
                        intf = (NetworkInterface)en.nextElement();
                        socket.joinGroup(group, intf);
                        if (logger.isDebugEnabled()) {
                            logger.debug("joined " + group + " on " + intf.getName());
                        }
                    }
                } else {
                    socket.joinGroup(group,null);
                }
                DatagramPacket packet;

                while (status == true) {
                    try {
                        byte[] buf = new byte[256];
                        packet = new DatagramPacket(buf, buf.length);
                        socket.receive(packet);

                        String received = new String(packet.getData());
                        received = received.trim();
                        MssHostsTableHandler handler = MssHostsTableHandler.getInstance();
                        if (handler.isMssHostPresent(received) == true) {
                            if (logger.isDebugEnabled()) {
                                logger.debug("KeepAlive message for : " + received);
                            }
                            if (mssThreadpoolMap != null && status == true) {
                                MssHeartBeatListener tempMssThread = null;
                                tempMssThread = (MssHeartBeatListener) mssThreadpoolMap
                                        .get(received);
                                if (tempMssThread == null) {
                                    MssHeartBeatListener mssthread = 
                                        new MssHeartBeatListener(received);
                                    mssthread.setKeepaliveMessage(received);
                                    mssthread.start();
                                    mssThreadpoolMap.put(received, mssthread);
                                    handler.updateTables(mssthread.getName(),true);
                                } else {
                                    if (tempMssThread.isStatus() == true && status == true) {
                                        MssHeartBeatListener mss = null;
                                        mss = (MssHeartBeatListener) 
                                               mssThreadpoolMap.get(received);
                                        mss.setKeepaliveMessage(received);
                                    } else if (tempMssThread.isStatus() == false && status == true) {
                                        if (logger.isDebugEnabled()) {
                                            logger.debug(received
                                                    + " is UP again. Adding "
                                                    + received
                                                    + " in Active list of Mss.");    
                                        }
                                        
                                        MssHeartBeatListener mssthread = 
                                                    new MssHeartBeatListener(received);
                                        mssthread.setKeepaliveMessage(received);
                                        handler.updateTables(mssthread.getName(),true);
                                        mssthread.start();
                                        mssThreadpoolMap.put(received,mssthread);
                                    }
                                }
                            }
                        }
                        count=0;
                    } catch (SocketTimeoutException e) {                        
                        logger.error(e.getMessage());
                        throw e;
                    }
                }

            } catch (Exception e) {
                try {
                    synchronized (socket) {
                        if(heartbeatListenToAllInterfaces) {
                            NetworkInterface intf;
                            for(Enumeration en=NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) 
                            {
                                intf=(NetworkInterface)en.nextElement();
                                socket.leaveGroup(group, intf);
                                logger.debug("left " + group + " on " + intf.getName());
                            }
                        }
                        else {
                            socket.leaveGroup(group, null);
                        }
                        socket.close();
                        if (logger.isDebugEnabled()) {
                            logger.debug("Socket is being closed");
                        }
                    }                    
                    stopMssListenerThreads();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Got an Exception while receiving heartbeat");
                        logger.debug("Stopped all the running MssListenerThreads");
                    }
                    count++;
                    if(count == 5) {
                        logger.info("Socket connection failed 5 times...hence exiting.");
                        break;
                    }
                    continue;
                } catch (Exception e1) {
                    stopMssListenerThreads();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Got an Exception while closing Socket");
                        logger.debug("Stopped all the running MssListenerThreads after " +
                                "getting error while closing socket");
                    }
                    socket = null;
                    count++;
                    if(count == 5) {
                        logger.info("Socket connection failed 5 times...hence exiting.");
                        break;
                    }
                    continue;
                }                
            }
        }

        // just log this in case we exit and didn't expect to..
        logger.info("GroupHeartBeat Listener Thread is exiting.");
    }

    /**
     * Method removes those mssHost entries from active list which are not alive at
     * the WE start up
     */
    public class CheckDeadMssTask extends TimerTask {
        public void run() {
            Iterator it = mssThreadpoolMap.keySet().iterator();
            while (it.hasNext()) {
                String host = (String) it.next();
                MssHeartBeatListener mssthread = (MssHeartBeatListener) mssThreadpoolMap
                        .get(host);

                synchronized (socket) {
                    if (null != socket && !socket.isClosed() && mssthread == null) {
                        logger.info(host + " - Mss server is DOWN at start up.");
                        MssHostsTableHandler.getInstance().updateTables(host,false);
                    }
                }
            }
        }
    }

    /**
     * This stops all running MssListenerThreads.
     */
    public static void stopMssListenerThreads() {
    synchronized (mssThreadpoolMap) {
        Set keyset = mssThreadpoolMap.keySet();
        if (keyset != null) {
            Iterator it = keyset.iterator();
            while (it.hasNext()) {
                String host = (String) it.next();
                MssHeartBeatListener mssthread = (MssHeartBeatListener) mssThreadpoolMap
                        .get(host);
                if (mssthread != null) {
                    mssthread.setStatus(false);
                    mssThreadpoolMap.put(host, null);
                }
            }
        }
    }
    }
}
