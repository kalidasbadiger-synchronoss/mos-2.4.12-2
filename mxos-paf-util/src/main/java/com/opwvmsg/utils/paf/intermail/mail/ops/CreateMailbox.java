/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will create a new Mailbox store for the user.
 */
public class CreateMailbox extends AbstractMssOperation {

    // input
    private String name;
    private String welcomeMsgId;
    private boolean suppressMers;
    private int options;

    private long inboxUIDValidity;
    private long lastAccessTime;

    // output
    MsRMId mssId; // Message Store Identifier.
    long accessRight; // access rights the use has
    MsRMId rootFolder; // Root folder identifier.
    MsRMId inboxFolder; // Inbox folder identifier.
    long featureFlag; // Access Mask

    // internal
    private String url;
    private String hostHeader;

    // constants
    public static final int CREATE_MAILBOX_GET_POP_LOCK = 0x01;

    /**
     * Constructor for CreateMailbox
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param welcomeMsgId the msgId of the welcome msg to use, or null to use
     *            default.
     * @param suppressMers if true, suppress MERS events for this operation.
     * @param options options for this operation. possible bits are: <br>
     *            CREATE_MAILBOX_GET_POP_LOCK - get the popserver lock for the
     *            user while creating the mailbox.
     * @param isStateless RME protocol chooser
     */
    public CreateMailbox(String host, String name, String welcomeMsgId,
            boolean suppressMers, int options) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.welcomeMsgId = welcomeMsgId;
        this.suppressMers = suppressMers;
        this.options = options;
        url = getMsUrl(host, name, MSS_P_CREATEMS);
    }

    /**
     * Constructor for CreateMailbox for Stateless RME
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param welcomeMsgId the msgId of the welcome msg to use, or null to use
     *            default.
     * @param inboxUIDValidity Inbox UID Validity
     * @param lastAccessTime Last Access Time of the Mailbox
     */
    public CreateMailbox(String host, String name, String realm,
            String welcomeMsgId, long inboxUIDValidity, long lastAccessTime) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_CREATEMS);
        this.welcomeMsgId = welcomeMsgId;
        this.inboxUIDValidity = inboxUIDValidity;
        this.lastAccessTime = lastAccessTime;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_CREATEMS, true);
            rmeSocket.setReadyToClose();
            releaseConnection();
        } else {
            callRme(MSS_SL_CREATEMS);
        }
    }

    // RME FUNCTIONS

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {

        // client Ids
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        // ms url
        outStream.writeString(url);
        outStream.writeString("INBOX");
        outStream.writeString("SentMail");
        outStream.writeString("Trash");
        outStream.writeString("anonymous");
        outStream.writeString(welcomeMsgId);
        outStream.writeEmptyCos();
        outStream.writeBoolean(suppressMers);
        outStream.writeInt(-1);
        if (outStream.rmeProtAtLeast(RME_CREATE_MS_WITH_OPTIONS_VER)) {
            outStream.writeInt(options);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // we dont care about anything in the reply here except Rmid and status
        inStream.readInt(); // the msid, ignored, since it will be lost when we
                            // close the cnxn
        inStream.readInt(); // the r/w access - we ignore this
        inStream.readInt(); // root rmid
        inStream.readInt(); // inbox rmid
        inStream.readInt(); // outbox rmid
        inStream.readInt(); // trash rmid
        inStream.readInt(); // the access id flag
        inStream.readString(); // the inbox name - we know it's "INBOX"

        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.opwvmsg.utils.paf.intermail.rme.AbstractOperation#constructHttpData()
     */
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_CREATEMS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        outStream.writeString(url); // ms url
        outStream.writeString("INBOX"); // Inbox Folder Name. Hard coded
        outStream.writeString(welcomeMsgId);
        outStream.writeInt((int) inboxUIDValidity);
        outStream.writeInt((int) lastAccessTime);
        outStream.flush();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.opwvmsg.utils.paf.intermail.rme.AbstractOperation#receiveHttpData()
     */
    protected void receiveHttpData() throws IOException {
        mssId = new MsRMId(inStream); // Message Store Identifier.
        accessRight = inStream.readInt(); // access rights the user has
        rootFolder = new MsRMId(inStream); // Root folder identifier.
        inboxFolder = new MsRMId(inStream);// Inbox folder identifier.
        featureFlag = inStream.readInt();// access mask
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the mssId
     */
    public MsRMId getMssId() {
        return mssId;
    }

    /**
     * @return the accessRight
     */
    public long getAccessRight() {
        return accessRight;
    }

    /**
     * @return the rootFolder
     */
    public MsRMId getRootFolder() {
        return rootFolder;
    }

    /**
     * @return the inboxFolder
     */
    public MsRMId getInboxFolder() {
        return inboxFolder;
    }

    /**
     * @return the featureFlag
     */
    public long getFeatureFlag() {
        return featureFlag;
    }
}
