/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.extsrv.ServiceManager;
import com.opwvmsg.utils.paf.intermail.extsrv.clients.MessageExamine;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MimeMsg;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

public class UpdateMessageBlob extends AbstractMssOperation {

    public final static String SKIP_MIME_PARSING_ON_CREATE = "skipMimeParsingOnCreateMessage";

    // input
    private byte[] textBytes;
    private String name;
    private String pathname;
    private int[] uids;
    private String[] docids;
    protected int options;
    private int accessId;
    private String username;
    private String peer_IP;
    private String text;
    // output
    private String[] pathnameArray;
    private Msg[] messages;
    // internal
    private String url;

    // constants
    public static final int UPDATE_MSGS_UPDATE_ONLY = 0x20;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder (or folder hint) for the messages
     * @param uids The uids of the messages to read
     * @param flags Flags to update on the messages
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            READ_MSGS_GET_HDRS - retrieve message headers (must be
     *            specified with GET_MIME) <br>
     *            READ_MSGS_GET_TEXT - retrieve message text <br>
     *            READ_MSGS_GET_MIME - retrieve MIME info for message <br>
     *            READ_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            READ_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            READ_MSGS_UPDATE_ONLY - do not read the messages specified,
     *            only update the flags <br>
     *            READ_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     * @param username - for Legal Intercept - the current user's username
     * @param peer_IP - for Legal Intercept - the IP address of the user
     * @param text Message text to be updated
     */
    public UpdateMessageBlob(String host, String name, String pathname,
            int[] uids, int accessId, int options, String username,
            String peer_IP, String text) {
        super(RmeDataModel.CL);
        // not accepting msgids at this point
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_CL_UPDATEMSGCONTENT);
        this.pathname = pathname;
        pathnameArray = buildPathnameArray(pathname);
        if (uids == null) {
            uids = new int[0];
        }
        this.uids = uids;
        this.docids = new String[0];
        this.accessId = accessId;
        this.options = options;
        this.username = username;
        this.peer_IP = peer_IP;
        textBytes = new byte[text.length()];
        for (int i = 0; i < text.length(); i++) {
            textBytes[i] = (byte) text.charAt(i);
        }

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (pathname == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "pathname", "UpdateMessageInMailBox" }));
        }

        if (username != null
                && ServiceManager
                        .hookHasEnabledServices(MessageExamine.MESSAGE_WRITE_REQUEST)) {
            Msg[] messages = new Msg[1];
            messages[0] = new Msg(textBytes);
            MessageExamine MEop = new MessageExamine(username,
                    Config.getDefaultApp(), peer_IP, "UpdateMessageInMailBox",
                    messages, 0, messages[0].getText().length, false, pathname,
                    host, name);
            try {
                MEop.execute(); // do not return any errors from LI
                // they will just be logged.
            } catch (IntermailException ie) {
            }
        }

        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_CL_UPDATEMSGCONTENT);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_CL_UPDATEMSGCONTENT", host,
                            "dataModel=" + rmeDataModel }));
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);

        // msg specs
        outStream.writeInt(uids.length);
        for (int i = 0; i < uids.length; i++) {
            outStream.write(1);
            outStream.writeInt(uids[i]);
            outStream.writeString(null);
        }
        writeMessage();
        outStream.writeInt(accessId);
    }

    /**
     * This creates an RME Message object given the content provided.
     * 
     * @throws IOException
     */
    private void writeMessage() throws IOException {

        outStream.writeBoolean(true); // have it

        outStream.writeString(null);

        // write raw string bytes - no length prefix
        byte[] bytes2 = new byte[5];
        bytes2[4] = 0;
        for (int i = 0; i < 4; i++) {
            bytes2[i] = (byte) "SMTP".charAt(i);
        }
        outStream.write(bytes2, 0, bytes2.length);
        outStream.writeInt(0); // empty recips
        outStream.writeInt((int) (System.currentTimeMillis() / 1000));
        outStream.writeInt(textBytes.length); // mimeSize
        MimeMsg mimeMessage = new MimeMsg(textBytes, 0, textBytes.length);
        mimeMessage.writeToStream(outStream);
        outStream.writeInt(0); // no attributes
        outStream.writeInt(1); // 1 body part;
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x2A864886); // oid part 1
        outStream.writeInt(0xF70F0334); // oid part2
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x2A864886); // p1
        outStream.writeInt(0xF70F0436); // x400 oid
        outStream.writeByte(0x0A); // unknown mime message
        outStream.writeString("X-Intermail-Unknown-MIME-Type=unparsedmessage");
        outStream.writeString("");
        outStream.writeString("");
        outStream.writeString("");
        // body text, finally!
        int textLen = textBytes.length;
        outStream.writeInt(textLen);
        // write raw string bytes - no null terminate
        outStream.write(textBytes, 0, textBytes.length);
        // done!
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {

        logEvents = inStream.readLogEventArray();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // TODO Auto-generated method stub

    }

    @Override
    protected void receiveHttpData() throws IOException {
        // TODO Auto-generated method stub

    }

}
