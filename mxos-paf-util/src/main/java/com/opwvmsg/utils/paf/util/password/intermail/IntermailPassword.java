/*
 * Copyright (c) 2005 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/intermail/IntermailPassword.java#3 $
 */

package com.opwvmsg.utils.paf.util.password.intermail;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.util.Base64;
import com.opwvmsg.utils.paf.util.Utilities;
import com.opwvmsg.utils.paf.util.password.HashType;
import com.opwvmsg.utils.paf.util.password.Password;
import com.opwvmsg.utils.paf.util.password.PasswordException;

/**
 * This class wraps the Intermail password operations. All known algorithms are
 * implemented locally. Custom password types are not supported, but this class
 * can be extended by the customer to add up to 5 custom algorithms.
 */
public class IntermailPassword implements Password {

    /**
     * A logger
     */
    private static final Logger logger = Logger.getLogger("com.openwave.paf.util.password.intermail.IntermailPassword");

    /**
     * Constructor
     */
    public IntermailPassword() {

    }

    /**
     * Verify the user-supplied password against the correct password given its
     * hash type. Returns true if the verification succeeds.
     * <p>
     * The following hash types are supported:
     * <p>
     * <ul>
     * <li>clear</li>
     * <li>MD5</li>
     * <li>Unix crypt</li>
     * <li>SHA</li>
     * <li>SSHA1</li>
     * <li>Custom1-5</li>- these types are supported but not implemented
     * </ul>
     * <p>
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user-supplied password
     * @param pwType the correct password's hash type
     * @return true if verification succeeds
     */
    public boolean checkPassword(String password, String userPassword,
                                 HashType pwType) {

        if (password == null || userPassword == null) {
            return false;
        }
        if (pwType == HashType.CLEAR) {
            return checkClear(password, userPassword);
        } else if (pwType == HashType.MD5) {
            return checkMd5(password, userPassword);
        } else if (pwType == HashType.UNIX) {
            return checkUnix(password, userPassword);
        } else if (pwType == HashType.SHA1) {
            return checkSha1(password, userPassword);
        } else if (pwType == HashType.SSHA1) {
            return checkSsha1(password, userPassword);
        } else if (pwType == HashType.SHA256) {
            return checkSha256(password, userPassword);
        } else if (pwType == HashType.SSHA256) {
            return checkSsha256(password, userPassword);
        } else if (pwType == HashType.CUSTOM1) {
            return checkCustom1(password, userPassword);
        } else if (pwType == HashType.CUSTOM2) {
            return checkCustom2(password, userPassword);
        } else if (pwType == HashType.CUSTOM3) {
            return checkCustom3(password, userPassword);
        } else if (pwType == HashType.CUSTOM4) {
            return checkCustom4(password, userPassword);
        } else if (pwType == HashType.CUSTOM5) {
            return checkCustom5(password, userPassword);
        } else {
            logger.error("Invalid hash type specified.");
        }
        return false;
    }

    /**
     * Hashes a password according to the given hash type. If something goes
     * wrong, it throws an exception. Thought was given to having it just return
     * the password back to the caller, but that seemed risky in that the caller
     * could unknowingly propagate (display or store) a password in the clear.
     * <p>
     * The following hash types are supported:
     * <p>
     * <ul>
     * <li>clear</li>
     * <li>MD5</li>
     * <li>Unix crypt</li>
     * <li>SHA1</li>
     * <li>SSHA1</li>
     * <li>SHA256</li>
     * <li>SSHA256</li>
     * <li>Custom1-5</li>
     * (supported but not implemented) These will throw a PasswordException with
     * this implementation.
     * </ul>
     * <p>
     * 
     * @param password the password
     * @param pwType1 the desired hash type, as a string of one letter
     * @return the hashed password
     * @throws PasswordException if anything goes wrong with the operation
     */
    public String hashPassword(String password, HashType pwType)
            throws PasswordException {
        // each supported hashType is implemented in its own method.
        // this way a subclass can override just the hashing functions
        // if desired.
        if (password == null) {
            throw new PasswordException("Null password supplied.");
        }
        if (pwType == HashType.CLEAR) {
            return hashClear(password);
        } else if (pwType == HashType.MD5) {
            return hashMd5(password);
        } else if (pwType == HashType.UNIX) {
            return hashUnix(password);
        } else if (pwType == HashType.SHA1) {
            return hashSha1(password);
        } else if (pwType == HashType.SSHA1) {
            return hashSsha1(password);
        } else if (pwType == HashType.SHA256) {
            return hashSha256(password);
        } else if (pwType == HashType.SSHA256) {
            return hashSsha256(password);
        } else if (pwType == HashType.CUSTOM1) {
            return hashCustom1(password);
        } else if (pwType == HashType.CUSTOM2) {
            return hashCustom2(password);
        } else if (pwType == HashType.CUSTOM3) {
            return hashCustom3(password);
        } else if (pwType == HashType.CUSTOM4) {
            return hashCustom4(password);
        } else if (pwType == HashType.CUSTOM5) {
            return hashCustom5(password);
        } else {
            throw new PasswordException("Invalid hash type specified.");
        }
    }

    /**
     * A hash function for the clear password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashClear(String password) {
        return password;
    }

    /**
     * A hash function for the md5 password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashMd5(String password) {
        Random byteGenerator = new Random();
        byte[] rawSalt = new byte[16];
        byteGenerator.nextBytes(rawSalt);
        String salt = Utilities.byteArrayToHexString(rawSalt);
        try {
            return hashMd5WithSalt(password, salt.getBytes("us-ascii"));
        } catch (UnsupportedEncodingException ignore) {}
        return "";
    }
     
    private String hashMd5WithSalt(String password, byte[] salt) {
        try { 
            MessageDigest digest = MessageDigest.getInstance("MD5");
            byte[] passwordBytes = password.getBytes("UTF-8");
            byte[] bytesToEncode = new byte[2 + (2 * salt.length)
                    + passwordBytes.length];
            System.arraycopy(salt, 0, bytesToEncode, 0, salt.length);
            bytesToEncode[salt.length] = (byte)89;
            System.arraycopy(passwordBytes, 0, bytesToEncode, salt.length + 1,
                passwordBytes.length);
            bytesToEncode[salt.length + 1 + passwordBytes.length] = (byte)247;
            System.arraycopy(salt, 0, bytesToEncode, salt.length + 2
                    + passwordBytes.length, salt.length);
            byte[] encodedBytes = digest.digest(bytesToEncode);
            String finalString = Utilities.byteArrayToHexString(encodedBytes);
            return finalString + new String (salt, "us-ascii");
        } catch (UnsupportedEncodingException ignore) {
            // can't happen
        } catch (NoSuchAlgorithmException ignore2) {
        }
        // should never get here
        return "";
    }

    /**
     * A hash function for the unix password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashUnix(String password) {
        return Crypt.crypt(password, null);
    }

    /**
     * A hash function for the SHA1 password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashSha1(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            byte[] passwordBytes = password.getBytes("UTF-8");
            byte[] encodedBytes = digest.digest(passwordBytes);
            return Base64.encode(new String(encodedBytes, 0));
        } catch (UnsupportedEncodingException ignore) {
            // can't happen
        } catch (NoSuchAlgorithmException ignore) {
        }
        return "";
    }

    /**
     * A hash function for the ssha1 password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashSsha1(String password) {
        Random byteGenerator = new Random();
        byte[] rawSalt = new byte[8];
        byteGenerator.nextBytes(rawSalt);
        String salt = Utilities.byteArrayToHexString(rawSalt);
        try {
            return hashSsha1WithSalt(password, salt.getBytes("us-ascii"));
        } catch (UnsupportedEncodingException ignore) {}
        return "";
    }
    

    private String hashSsha1WithSalt(String password, byte[] salt) {
        try {
            MessageDigest ssha1Digest = MessageDigest.getInstance("SHA-1");
            byte[] passwordBytes = password.getBytes("UTF-8");
            byte[] saltedPasswordBytes = new byte[passwordBytes.length
                    + salt.length];
            System.arraycopy(passwordBytes, 0, saltedPasswordBytes, 0,
                passwordBytes.length);
            System.arraycopy(salt, 0, saltedPasswordBytes,
                passwordBytes.length, salt.length);

            byte[] encodedBytes = ssha1Digest.digest(saltedPasswordBytes);
            return Base64.encode(new String(encodedBytes, 0)
                    + new String(salt, 0));
        } catch (UnsupportedEncodingException ignore) {
            // can't happen
        } catch (NoSuchAlgorithmException ignore) {
        }
        return "";
    }
    
    
    /**
     * A hash function for the SHA-256 password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashSha256(String password) {
        byte[] encodedBytes = DigestUtils.sha256(password);
        return Base64.encode(new String(encodedBytes, 0));
    }
    /**
     * A hash function for the ssha-256 password type.
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashSsha256(String password) {
        Random byteGenerator = new Random();
        byte[] rawSalt = new byte[32];
        byteGenerator.nextBytes(rawSalt);
        String salt = Utilities.byteArrayToHexString(rawSalt);
        try {
            return hashSsha256WithSalt(password, salt.getBytes("us-ascii"));
        } catch (UnsupportedEncodingException ignore) {
        }
        return "";
    }
        
    
    private String hashSsha256WithSalt(String password, byte[] salt) {
        try {
            byte[] passwordBytes = password.getBytes("UTF-8");
            byte[] saltedPasswordBytes = new byte[passwordBytes.length
                    + salt.length];
            System.arraycopy(passwordBytes, 0, saltedPasswordBytes, 0,
                    passwordBytes.length);
            System.arraycopy(salt, 0, saltedPasswordBytes,
                    passwordBytes.length, salt.length);

            byte[] encodedBytes = DigestUtils.sha256(saltedPasswordBytes);
            return Base64.encode(new String(encodedBytes, 0)
                    + new String(salt, 0));
        } catch (UnsupportedEncodingException ignore) {
            // can't happen
        }
        return "";
    }

    /**
     * A hash function for the custom1 password type. (not implemented in this
     * provider - will throw PasswordException)
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashCustom1(String password) throws PasswordException {
        throw new PasswordException("The Custom1 hash type is not implemented");
    }

    /**
     * A hash function for the custom2 password type. (not implemented in this
     * provider - will throw PasswordException)
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashCustom2(String password) throws PasswordException {
        throw new PasswordException("The Custom2 hash type is not implemented");
    }

    /**
     * A hash function for the custom3 password type. (not implemented in this
     * provider - will throw PasswordException)
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashCustom3(String password) throws PasswordException {
        throw new PasswordException("The Custom3 hash type is not implemented");

    }

    /**
     * A hash function for the custom4 password type. (not implemented in this
     * provider - will throw PasswordException)
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashCustom4(String password) throws PasswordException {
        throw new PasswordException("The Custom4 hash type is not implemented");
    }

    /**
     * A hash function for the custom5 password type. (not implemented in this
     * provider - will throw PasswordException)
     * 
     * @param password
     * @return the hashed password value
     */
    protected String hashCustom5(String password) throws PasswordException {
        throw new PasswordException("The Custom5 hash type is not implemented");
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkClear(String password, String userPassword) {
        return (hashClear(password).equals(userPassword));
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkMd5(String password, String userPassword) {
        try  {
            byte[] salt = password.substring(32).getBytes("us-ascii");                    
            return (hashMd5WithSalt(userPassword, salt).equals(password));
        } catch (UnsupportedEncodingException ignore) {}
        return false;
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkUnix(String password, String userPassword) {
        String salt = password.substring(0, 2);
        return (Crypt.crypt(userPassword, salt).equals(password));
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkSha1(String password, String userPassword) {
        if (password.startsWith("{")) {
            return checkSha1(password.substring(5), userPassword);
            // skip {sha} if it exists (backwards compatibility)
        }
        return (hashSha1(userPassword).equals(password));
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkSsha1(String password, String userPassword) {
        if (password.startsWith("{")) {
            return checkSsha1(password.substring(6), userPassword);
            // skip {ssha} if it exists (backwards compatibility)
        }
        byte [] decodedBytes = Base64.decodeBase64(password);
        byte[] salt = new byte[decodedBytes.length - 20];
        System.arraycopy(decodedBytes, 20, salt, 0, salt.length);
        return (hashSsha1WithSalt(userPassword, salt).equals(password));
        
    }
    
    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkSha256(String password, String userPassword) {
        return (hashSha256(userPassword).equals(password));
    }

    /**
     * Checks to see if the clear encoded password matches the user password
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkSsha256(String password, String userPassword) {
        byte[] decodedBytes = Base64.decodeBase64(password);
        byte[] salt = new byte[decodedBytes.length - 32];
        System.arraycopy(decodedBytes, 32, salt, 0, salt.length);
        return (hashSsha256WithSalt(userPassword, salt).equals(password));
    }

    /**
     * Checks to see if the custom1 encoded password matches the user password
     * This implementation assumes a non-salted hash algorithm. If a salted
     * algorithm is used, this method must be overridden as well.
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkCustom1(String password, String userPassword) {
        try {
            return (hashCustom1(userPassword).equals(password));
        } catch (PasswordException ignore) {
        }
        return false;
    }

    /**
     * Checks to see if the custom2 encoded password matches the user password
     * This implementation assumes a non-salted hash algorithm. If a salted
     * algorithm is used, this method must be overridden as well.
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkCustom2(String password, String userPassword) {
        try {
            return (hashCustom2(userPassword).equals(password));
        } catch (PasswordException ignore) {
        }
        return false;
    }

    /**
     * Checks to see if the custom3 encoded password matches the user password
     * This implementation assumes a non-salted hash algorithm. If a salted
     * algorithm is used, this method must be overridden as well.
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkCustom3(String password, String userPassword) {
        try {
            return (hashCustom3(userPassword).equals(password));
        } catch (PasswordException ignore) {
        }
        return false;
    }

    /**
     * Checks to see if the custom4 encoded password matches the user password
     * This implementation assumes a non-salted hash algorithm. If a salted
     * algorithm is used, this method must be overridden as well.
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkCustom4(String password, String userPassword) {
        try {
            return (hashCustom4(userPassword).equals(password));
        } catch (PasswordException ignore) {
        }
        return false;
    }

    /**
     * Checks to see if the custom5 encoded password matches the user password.
     * This implementation assumes a non-salted hash algorithm. If a salted
     * algorithm is used, this method must be overridden as well.
     * 
     * @param password the correct (encrypted) password
     * @param userPassword the user supplied password
     * @return true if the passwords match, false otherwise.
     */
    protected boolean checkCustom5(String password, String userPassword) {
        try {
            return (hashCustom5(userPassword).equals(password));
        } catch (PasswordException ignore) {
        }
        return false;
    }

    /***************************************************************************
     * Java-based implementation of the unix crypt(3) command
     * 
     * Based upon C source code written by Eric Young, eay@psych.uq.oz.au Java
     * conversion by John F. Dumas, jdumas@zgs.com
     * 
     * Found at http://locutus.kingwoodcable.com/jfd/crypt.html Minor
     * optimizations by Wes Biggs, wes@cacas.org Saved about 6-9% in class file
     * size, 1% speed gain Note: Crypt.class is much smaller when compiled with
     * javac -O
     **************************************************************************/

    public static final class Crypt {
        private Crypt() {
        } // defined so class can't be instantiated.

        private static final int ITERATIONS = 16;

        private static final boolean shifts2[] = { false, false, true, true,
                true, true, true, true, false, true, true, true, true, true,
                true, false };

        private static final int skb[][] = {
                {
                /* for C bits (numbered as per FIPS 46) 1 2 3 4 5 6 */
                0x00000000, 0x00000010, 0x20000000, 0x20000010, 0x00010000,
                        0x00010010, 0x20010000, 0x20010010, 0x00000800,
                        0x00000810, 0x20000800, 0x20000810, 0x00010800,
                        0x00010810, 0x20010800, 0x20010810, 0x00000020,
                        0x00000030, 0x20000020, 0x20000030, 0x00010020,
                        0x00010030, 0x20010020, 0x20010030, 0x00000820,
                        0x00000830, 0x20000820, 0x20000830, 0x00010820,
                        0x00010830, 0x20010820, 0x20010830, 0x00080000,
                        0x00080010, 0x20080000, 0x20080010, 0x00090000,
                        0x00090010, 0x20090000, 0x20090010, 0x00080800,
                        0x00080810, 0x20080800, 0x20080810, 0x00090800,
                        0x00090810, 0x20090800, 0x20090810, 0x00080020,
                        0x00080030, 0x20080020, 0x20080030, 0x00090020,
                        0x00090030, 0x20090020, 0x20090030, 0x00080820,
                        0x00080830, 0x20080820, 0x20080830, 0x00090820,
                        0x00090830, 0x20090820, 0x20090830, },
                {
                /* for C bits (numbered as per FIPS 46) 7 8 10 11 12 13 */
                0x00000000, 0x02000000, 0x00002000, 0x02002000, 0x00200000,
                        0x02200000, 0x00202000, 0x02202000, 0x00000004,
                        0x02000004, 0x00002004, 0x02002004, 0x00200004,
                        0x02200004, 0x00202004, 0x02202004, 0x00000400,
                        0x02000400, 0x00002400, 0x02002400, 0x00200400,
                        0x02200400, 0x00202400, 0x02202400, 0x00000404,
                        0x02000404, 0x00002404, 0x02002404, 0x00200404,
                        0x02200404, 0x00202404, 0x02202404, 0x10000000,
                        0x12000000, 0x10002000, 0x12002000, 0x10200000,
                        0x12200000, 0x10202000, 0x12202000, 0x10000004,
                        0x12000004, 0x10002004, 0x12002004, 0x10200004,
                        0x12200004, 0x10202004, 0x12202004, 0x10000400,
                        0x12000400, 0x10002400, 0x12002400, 0x10200400,
                        0x12200400, 0x10202400, 0x12202400, 0x10000404,
                        0x12000404, 0x10002404, 0x12002404, 0x10200404,
                        0x12200404, 0x10202404, 0x12202404, },
                {
                /* for C bits (numbered as per FIPS 46) 14 15 16 17 19 20 */
                0x00000000, 0x00000001, 0x00040000, 0x00040001, 0x01000000,
                        0x01000001, 0x01040000, 0x01040001, 0x00000002,
                        0x00000003, 0x00040002, 0x00040003, 0x01000002,
                        0x01000003, 0x01040002, 0x01040003, 0x00000200,
                        0x00000201, 0x00040200, 0x00040201, 0x01000200,
                        0x01000201, 0x01040200, 0x01040201, 0x00000202,
                        0x00000203, 0x00040202, 0x00040203, 0x01000202,
                        0x01000203, 0x01040202, 0x01040203, 0x08000000,
                        0x08000001, 0x08040000, 0x08040001, 0x09000000,
                        0x09000001, 0x09040000, 0x09040001, 0x08000002,
                        0x08000003, 0x08040002, 0x08040003, 0x09000002,
                        0x09000003, 0x09040002, 0x09040003, 0x08000200,
                        0x08000201, 0x08040200, 0x08040201, 0x09000200,
                        0x09000201, 0x09040200, 0x09040201, 0x08000202,
                        0x08000203, 0x08040202, 0x08040203, 0x09000202,
                        0x09000203, 0x09040202, 0x09040203, },
                {
                /* for C bits (numbered as per FIPS 46) 21 23 24 26 27 28 */
                0x00000000, 0x00100000, 0x00000100, 0x00100100, 0x00000008,
                        0x00100008, 0x00000108, 0x00100108, 0x00001000,
                        0x00101000, 0x00001100, 0x00101100, 0x00001008,
                        0x00101008, 0x00001108, 0x00101108, 0x04000000,
                        0x04100000, 0x04000100, 0x04100100, 0x04000008,
                        0x04100008, 0x04000108, 0x04100108, 0x04001000,
                        0x04101000, 0x04001100, 0x04101100, 0x04001008,
                        0x04101008, 0x04001108, 0x04101108, 0x00020000,
                        0x00120000, 0x00020100, 0x00120100, 0x00020008,
                        0x00120008, 0x00020108, 0x00120108, 0x00021000,
                        0x00121000, 0x00021100, 0x00121100, 0x00021008,
                        0x00121008, 0x00021108, 0x00121108, 0x04020000,
                        0x04120000, 0x04020100, 0x04120100, 0x04020008,
                        0x04120008, 0x04020108, 0x04120108, 0x04021000,
                        0x04121000, 0x04021100, 0x04121100, 0x04021008,
                        0x04121008, 0x04021108, 0x04121108, },
                {
                /* for D bits (numbered as per FIPS 46) 1 2 3 4 5 6 */
                0x00000000, 0x10000000, 0x00010000, 0x10010000, 0x00000004,
                        0x10000004, 0x00010004, 0x10010004, 0x20000000,
                        0x30000000, 0x20010000, 0x30010000, 0x20000004,
                        0x30000004, 0x20010004, 0x30010004, 0x00100000,
                        0x10100000, 0x00110000, 0x10110000, 0x00100004,
                        0x10100004, 0x00110004, 0x10110004, 0x20100000,
                        0x30100000, 0x20110000, 0x30110000, 0x20100004,
                        0x30100004, 0x20110004, 0x30110004, 0x00001000,
                        0x10001000, 0x00011000, 0x10011000, 0x00001004,
                        0x10001004, 0x00011004, 0x10011004, 0x20001000,
                        0x30001000, 0x20011000, 0x30011000, 0x20001004,
                        0x30001004, 0x20011004, 0x30011004, 0x00101000,
                        0x10101000, 0x00111000, 0x10111000, 0x00101004,
                        0x10101004, 0x00111004, 0x10111004, 0x20101000,
                        0x30101000, 0x20111000, 0x30111000, 0x20101004,
                        0x30101004, 0x20111004, 0x30111004, },
                {
                /* for D bits (numbered as per FIPS 46) 8 9 11 12 13 14 */
                0x00000000, 0x08000000, 0x00000008, 0x08000008, 0x00000400,
                        0x08000400, 0x00000408, 0x08000408, 0x00020000,
                        0x08020000, 0x00020008, 0x08020008, 0x00020400,
                        0x08020400, 0x00020408, 0x08020408, 0x00000001,
                        0x08000001, 0x00000009, 0x08000009, 0x00000401,
                        0x08000401, 0x00000409, 0x08000409, 0x00020001,
                        0x08020001, 0x00020009, 0x08020009, 0x00020401,
                        0x08020401, 0x00020409, 0x08020409, 0x02000000,
                        0x0A000000, 0x02000008, 0x0A000008, 0x02000400,
                        0x0A000400, 0x02000408, 0x0A000408, 0x02020000,
                        0x0A020000, 0x02020008, 0x0A020008, 0x02020400,
                        0x0A020400, 0x02020408, 0x0A020408, 0x02000001,
                        0x0A000001, 0x02000009, 0x0A000009, 0x02000401,
                        0x0A000401, 0x02000409, 0x0A000409, 0x02020001,
                        0x0A020001, 0x02020009, 0x0A020009, 0x02020401,
                        0x0A020401, 0x02020409, 0x0A020409, },
                {
                /* for D bits (numbered as per FIPS 46) 16 17 18 19 20 21 */
                0x00000000, 0x00000100, 0x00080000, 0x00080100, 0x01000000,
                        0x01000100, 0x01080000, 0x01080100, 0x00000010,
                        0x00000110, 0x00080010, 0x00080110, 0x01000010,
                        0x01000110, 0x01080010, 0x01080110, 0x00200000,
                        0x00200100, 0x00280000, 0x00280100, 0x01200000,
                        0x01200100, 0x01280000, 0x01280100, 0x00200010,
                        0x00200110, 0x00280010, 0x00280110, 0x01200010,
                        0x01200110, 0x01280010, 0x01280110, 0x00000200,
                        0x00000300, 0x00080200, 0x00080300, 0x01000200,
                        0x01000300, 0x01080200, 0x01080300, 0x00000210,
                        0x00000310, 0x00080210, 0x00080310, 0x01000210,
                        0x01000310, 0x01080210, 0x01080310, 0x00200200,
                        0x00200300, 0x00280200, 0x00280300, 0x01200200,
                        0x01200300, 0x01280200, 0x01280300, 0x00200210,
                        0x00200310, 0x00280210, 0x00280310, 0x01200210,
                        0x01200310, 0x01280210, 0x01280310, },
                {
                /* for D bits (numbered as per FIPS 46) 22 23 24 25 27 28 */
                0x00000000, 0x04000000, 0x00040000, 0x04040000, 0x00000002,
                        0x04000002, 0x00040002, 0x04040002, 0x00002000,
                        0x04002000, 0x00042000, 0x04042000, 0x00002002,
                        0x04002002, 0x00042002, 0x04042002, 0x00000020,
                        0x04000020, 0x00040020, 0x04040020, 0x00000022,
                        0x04000022, 0x00040022, 0x04040022, 0x00002020,
                        0x04002020, 0x00042020, 0x04042020, 0x00002022,
                        0x04002022, 0x00042022, 0x04042022, 0x00000800,
                        0x04000800, 0x00040800, 0x04040800, 0x00000802,
                        0x04000802, 0x00040802, 0x04040802, 0x00002800,
                        0x04002800, 0x00042800, 0x04042800, 0x00002802,
                        0x04002802, 0x00042802, 0x04042802, 0x00000820,
                        0x04000820, 0x00040820, 0x04040820, 0x00000822,
                        0x04000822, 0x00040822, 0x04040822, 0x00002820,
                        0x04002820, 0x00042820, 0x04042820, 0x00002822,
                        0x04002822, 0x00042822, 0x04042822, } };

        private static final int SPtrans[][] = {
                {
                /* nibble 0 */
                0x00820200, 0x00020000, 0x80800000, 0x80820200, 0x00800000,
                        0x80020200, 0x80020000, 0x80800000, 0x80020200,
                        0x00820200, 0x00820000, 0x80000200, 0x80800200,
                        0x00800000, 0x00000000, 0x80020000, 0x00020000,
                        0x80000000, 0x00800200, 0x00020200, 0x80820200,
                        0x00820000, 0x80000200, 0x00800200, 0x80000000,
                        0x00000200, 0x00020200, 0x80820000, 0x00000200,
                        0x80800200, 0x80820000, 0x00000000, 0x00000000,
                        0x80820200, 0x00800200, 0x80020000, 0x00820200,
                        0x00020000, 0x80000200, 0x00800200, 0x80820000,
                        0x00000200, 0x00020200, 0x80800000, 0x80020200,
                        0x80000000, 0x80800000, 0x00820000, 0x80820200,
                        0x00020200, 0x00820000, 0x80800200, 0x00800000,
                        0x80000200, 0x80020000, 0x00000000, 0x00020000,
                        0x00800000, 0x80800200, 0x00820200, 0x80000000,
                        0x80820000, 0x00000200, 0x80020200, },
                {
                /* nibble 1 */
                0x10042004, 0x00000000, 0x00042000, 0x10040000, 0x10000004,
                        0x00002004, 0x10002000, 0x00042000, 0x00002000,
                        0x10040004, 0x00000004, 0x10002000, 0x00040004,
                        0x10042000, 0x10040000, 0x00000004, 0x00040000,
                        0x10002004, 0x10040004, 0x00002000, 0x00042004,
                        0x10000000, 0x00000000, 0x00040004, 0x10002004,
                        0x00042004, 0x10042000, 0x10000004, 0x10000000,
                        0x00040000, 0x00002004, 0x10042004, 0x00040004,
                        0x10042000, 0x10002000, 0x00042004, 0x10042004,
                        0x00040004, 0x10000004, 0x00000000, 0x10000000,
                        0x00002004, 0x00040000, 0x10040004, 0x00002000,
                        0x10000000, 0x00042004, 0x10002004, 0x10042000,
                        0x00002000, 0x00000000, 0x10000004, 0x00000004,
                        0x10042004, 0x00042000, 0x10040000, 0x10040004,
                        0x00040000, 0x00002004, 0x10002000, 0x10002004,
                        0x00000004, 0x10040000, 0x00042000, },
                {
                /* nibble 2 */
                0x41000000, 0x01010040, 0x00000040, 0x41000040, 0x40010000,
                        0x01000000, 0x41000040, 0x00010040, 0x01000040,
                        0x00010000, 0x01010000, 0x40000000, 0x41010040,
                        0x40000040, 0x40000000, 0x41010000, 0x00000000,
                        0x40010000, 0x01010040, 0x00000040, 0x40000040,
                        0x41010040, 0x00010000, 0x41000000, 0x41010000,
                        0x01000040, 0x40010040, 0x01010000, 0x00010040,
                        0x00000000, 0x01000000, 0x40010040, 0x01010040,
                        0x00000040, 0x40000000, 0x00010000, 0x40000040,
                        0x40010000, 0x01010000, 0x41000040, 0x00000000,
                        0x01010040, 0x00010040, 0x41010000, 0x40010000,
                        0x01000000, 0x41010040, 0x40000000, 0x40010040,
                        0x41000000, 0x01000000, 0x41010040, 0x00010000,
                        0x01000040, 0x41000040, 0x00010040, 0x01000040,
                        0x00000000, 0x41010000, 0x40000040, 0x41000000,
                        0x40010040, 0x00000040, 0x01010000, },
                {
                /* nibble 3 */
                0x00100402, 0x04000400, 0x00000002, 0x04100402, 0x00000000,
                        0x04100000, 0x04000402, 0x00100002, 0x04100400,
                        0x04000002, 0x04000000, 0x00000402, 0x04000002,
                        0x00100402, 0x00100000, 0x04000000, 0x04100002,
                        0x00100400, 0x00000400, 0x00000002, 0x00100400,
                        0x04000402, 0x04100000, 0x00000400, 0x00000402,
                        0x00000000, 0x00100002, 0x04100400, 0x04000400,
                        0x04100002, 0x04100402, 0x00100000, 0x04100002,
                        0x00000402, 0x00100000, 0x04000002, 0x00100400,
                        0x04000400, 0x00000002, 0x04100000, 0x04000402,
                        0x00000000, 0x00000400, 0x00100002, 0x00000000,
                        0x04100002, 0x04100400, 0x00000400, 0x04000000,
                        0x04100402, 0x00100402, 0x00100000, 0x04100402,
                        0x00000002, 0x04000400, 0x00100402, 0x00100002,
                        0x00100400, 0x04100000, 0x04000402, 0x00000402,
                        0x04000000, 0x04000002, 0x04100400, },
                {
                /* nibble 4 */
                0x02000000, 0x00004000, 0x00000100, 0x02004108, 0x02004008,
                        0x02000100, 0x00004108, 0x02004000, 0x00004000,
                        0x00000008, 0x02000008, 0x00004100, 0x02000108,
                        0x02004008, 0x02004100, 0x00000000, 0x00004100,
                        0x02000000, 0x00004008, 0x00000108, 0x02000100,
                        0x00004108, 0x00000000, 0x02000008, 0x00000008,
                        0x02000108, 0x02004108, 0x00004008, 0x02004000,
                        0x00000100, 0x00000108, 0x02004100, 0x02004100,
                        0x02000108, 0x00004008, 0x02004000, 0x00004000,
                        0x00000008, 0x02000008, 0x02000100, 0x02000000,
                        0x00004100, 0x02004108, 0x00000000, 0x00004108,
                        0x02000000, 0x00000100, 0x00004008, 0x02000108,
                        0x00000100, 0x00000000, 0x02004108, 0x02004008,
                        0x02004100, 0x00000108, 0x00004000, 0x00004100,
                        0x02004008, 0x02000100, 0x00000108, 0x00000008,
                        0x00004108, 0x02004000, 0x02000008, },
                {
                /* nibble 5 */
                0x20000010, 0x00080010, 0x00000000, 0x20080800, 0x00080010,
                        0x00000800, 0x20000810, 0x00080000, 0x00000810,
                        0x20080810, 0x00080800, 0x20000000, 0x20000800,
                        0x20000010, 0x20080000, 0x00080810, 0x00080000,
                        0x20000810, 0x20080010, 0x00000000, 0x00000800,
                        0x00000010, 0x20080800, 0x20080010, 0x20080810,
                        0x20080000, 0x20000000, 0x00000810, 0x00000010,
                        0x00080800, 0x00080810, 0x20000800, 0x00000810,
                        0x20000000, 0x20000800, 0x00080810, 0x20080800,
                        0x00080010, 0x00000000, 0x20000800, 0x20000000,
                        0x00000800, 0x20080010, 0x00080000, 0x00080010,
                        0x20080810, 0x00080800, 0x00000010, 0x20080810,
                        0x00080800, 0x00080000, 0x20000810, 0x20000010,
                        0x20080000, 0x00080810, 0x00000000, 0x00000800,
                        0x20000010, 0x20000810, 0x20080800, 0x20080000,
                        0x00000810, 0x00000010, 0x20080010, },
                {
                /* nibble 6 */
                0x00001000, 0x00000080, 0x00400080, 0x00400001, 0x00401081,
                        0x00001001, 0x00001080, 0x00000000, 0x00400000,
                        0x00400081, 0x00000081, 0x00401000, 0x00000001,
                        0x00401080, 0x00401000, 0x00000081, 0x00400081,
                        0x00001000, 0x00001001, 0x00401081, 0x00000000,
                        0x00400080, 0x00400001, 0x00001080, 0x00401001,
                        0x00001081, 0x00401080, 0x00000001, 0x00001081,
                        0x00401001, 0x00000080, 0x00400000, 0x00001081,
                        0x00401000, 0x00401001, 0x00000081, 0x00001000,
                        0x00000080, 0x00400000, 0x00401001, 0x00400081,
                        0x00001081, 0x00001080, 0x00000000, 0x00000080,
                        0x00400001, 0x00000001, 0x00400080, 0x00000000,
                        0x00400081, 0x00400080, 0x00001080, 0x00000081,
                        0x00001000, 0x00401081, 0x00400000, 0x00401080,
                        0x00000001, 0x00001001, 0x00401081, 0x00400001,
                        0x00401080, 0x00401000, 0x00001001, },
                {
                /* nibble 7 */
                0x08200020, 0x08208000, 0x00008020, 0x00000000, 0x08008000,
                        0x00200020, 0x08200000, 0x08208020, 0x00000020,
                        0x08000000, 0x00208000, 0x00008020, 0x00208020,
                        0x08008020, 0x08000020, 0x08200000, 0x00008000,
                        0x00208020, 0x00200020, 0x08008000, 0x08208020,
                        0x08000020, 0x00000000, 0x00208000, 0x08000000,
                        0x00200000, 0x08008020, 0x08200020, 0x00200000,
                        0x00008000, 0x08208000, 0x00000020, 0x00200000,
                        0x00008000, 0x08000020, 0x08208020, 0x00008020,
                        0x08000000, 0x00000000, 0x00208000, 0x08200020,
                        0x08008020, 0x08008000, 0x00200020, 0x08208000,
                        0x00000020, 0x00200020, 0x08008000, 0x08208020,
                        0x00200000, 0x08200000, 0x08000020, 0x00208000,
                        0x00008020, 0x08008020, 0x08200000, 0x00000020,
                        0x08208000, 0x00208020, 0x00000000, 0x08000000,
                        0x08200020, 0x00008000, 0x00208020 } };

        private static final int byteToUnsigned(byte b) {
            int value = (int)b;
            return (value >= 0) ? value : value + 256;
        }

        private static int fourBytesToInt(byte b[], int offset) {
            return byteToUnsigned(b[offset++])
                    | (byteToUnsigned(b[offset++]) << 8)
                    | (byteToUnsigned(b[offset++]) << 16)
                    | (byteToUnsigned(b[offset]) << 24);
        }

        private static final void intToFourBytes(int iValue, byte b[],
                                                 int offset) {
            b[offset++] = (byte)((iValue) & 0xff);
            b[offset++] = (byte)((iValue >>> 8) & 0xff);
            b[offset++] = (byte)((iValue >>> 16) & 0xff);
            b[offset] = (byte)((iValue >>> 24) & 0xff);
        }

        private static final void PERM_OP(int a, int b, int n, int m,
                                          int results[]) {
            int t;

            t = ((a >>> n) ^ b) & m;
            a ^= t << n;
            b ^= t;

            results[0] = a;
            results[1] = b;
        }

        private static final int HPERM_OP(int a, int n, int m) {
            int t;

            t = ((a << (16 - n)) ^ a) & m;
            a = a ^ t ^ (t >>> (16 - n));

            return a;
        }

        private static int[] des_set_key(byte key[]) {
            int schedule[] = new int[ITERATIONS * 2];

            int c = fourBytesToInt(key, 0);
            int d = fourBytesToInt(key, 4);

            int results[] = new int[2];

            PERM_OP(d, c, 4, 0x0f0f0f0f, results);
            d = results[0];
            c = results[1];

            c = HPERM_OP(c, -2, 0xcccc0000);
            d = HPERM_OP(d, -2, 0xcccc0000);

            PERM_OP(d, c, 1, 0x55555555, results);
            d = results[0];
            c = results[1];

            PERM_OP(c, d, 8, 0x00ff00ff, results);
            c = results[0];
            d = results[1];

            PERM_OP(d, c, 1, 0x55555555, results);
            d = results[0];
            c = results[1];

            d = (((d & 0x000000ff) << 16) | (d & 0x0000ff00)
                    | ((d & 0x00ff0000) >>> 16) | ((c & 0xf0000000) >>> 4));
            c &= 0x0fffffff;

            int s, t;
            int j = 0;

            for (int i = 0; i < ITERATIONS; i++) {
                if (shifts2[i]) {
                    c = (c >>> 2) | (c << 26);
                    d = (d >>> 2) | (d << 26);
                } else {
                    c = (c >>> 1) | (c << 27);
                    d = (d >>> 1) | (d << 27);
                }

                c &= 0x0fffffff;
                d &= 0x0fffffff;

                s = skb[0][(c) & 0x3f]
                        | skb[1][((c >>> 6) & 0x03) | ((c >>> 7) & 0x3c)]
                        | skb[2][((c >>> 13) & 0x0f) | ((c >>> 14) & 0x30)]
                        | skb[3][((c >>> 20) & 0x01) | ((c >>> 21) & 0x06)
                                | ((c >>> 22) & 0x38)];

                t = skb[4][(d) & 0x3f]
                        | skb[5][((d >>> 7) & 0x03) | ((d >>> 8) & 0x3c)]
                        | skb[6][(d >>> 15) & 0x3f]
                        | skb[7][((d >>> 21) & 0x0f) | ((d >>> 22) & 0x30)];

                schedule[j++] = ((t << 16) | (s & 0x0000ffff)) & 0xffffffff;
                s = ((s >>> 16) | (t & 0xffff0000));

                s = (s << 4) | (s >>> 28);
                schedule[j++] = s & 0xffffffff;
            }
            return schedule;
        }

        private static final int D_ENCRYPT(int L, int R, int S, int E0, int E1,
                                           int s[]) {
            int t, u, v;

            v = R ^ (R >>> 16);
            u = v & E0;
            v = v & E1;
            u = (u ^ (u << 16)) ^ R ^ s[S];
            t = (v ^ (v << 16)) ^ R ^ s[S + 1];
            t = (t >>> 4) | (t << 28);

            L ^= SPtrans[1][(t) & 0x3f] | SPtrans[3][(t >>> 8) & 0x3f]
                    | SPtrans[5][(t >>> 16) & 0x3f]
                    | SPtrans[7][(t >>> 24) & 0x3f] | SPtrans[0][(u) & 0x3f]
                    | SPtrans[2][(u >>> 8) & 0x3f]
                    | SPtrans[4][(u >>> 16) & 0x3f]
                    | SPtrans[6][(u >>> 24) & 0x3f];

            return L;
        }

        private static final int[] body(int schedule[], int Eswap0, int Eswap1) {
            int left = 0;
            int right = 0;
            int t = 0;

            for (int j = 0; j < 25; j++) {
                for (int i = 0; i < ITERATIONS * 2; i += 4) {
                    left = D_ENCRYPT(left, right, i, Eswap0, Eswap1, schedule);
                    right = D_ENCRYPT(right, left, i + 2, Eswap0, Eswap1,
                        schedule);
                }
                t = left;
                left = right;
                right = t;
            }

            t = right;

            right = (left >>> 1) | (left << 31);
            left = (t >>> 1) | (t << 31);

            left &= 0xffffffff;
            right &= 0xffffffff;

            int results[] = new int[2];

            PERM_OP(right, left, 1, 0x55555555, results);
            right = results[0];
            left = results[1];

            PERM_OP(left, right, 8, 0x00ff00ff, results);
            left = results[0];
            right = results[1];

            PERM_OP(right, left, 2, 0x33333333, results);
            right = results[0];
            left = results[1];

            PERM_OP(left, right, 16, 0x0000ffff, results);
            left = results[0];
            right = results[1];

            PERM_OP(right, left, 4, 0x0f0f0f0f, results);
            right = results[0];
            left = results[1];

            int out[] = new int[2];

            out[0] = left;
            out[1] = right;

            return out;
        }

        public static final String alphabet = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        public static final String crypt(String original, String salt) {

            char charZero = 0;
            char charOne = 0;
            char[] buffer = new char[13];

            if (salt == null) {
                // generate random salt
                Random random = new Random();
                charZero = alphabet.charAt(random.nextInt(alphabet.length()));
                charOne = alphabet.charAt(random.nextInt(alphabet.length()));
            } else {
                while (salt.length() < 2)
                    salt += "A";
                charZero = salt.charAt(0);
                charOne = salt.charAt(1);
            }

            buffer[0] = charZero;
            buffer[1] = charOne;

            int Eswap0 = alphabet.indexOf(charZero);
            int Eswap1 = alphabet.indexOf(charOne) << 4;
            byte key[] = new byte[8];

            for (int i = 0; i < key.length; i++)
                key[i] = (byte)0;

            for (int i = 0; i < key.length && i < original.length(); i++)
                key[i] = (byte)(((int)original.charAt(i)) << 1);

            int schedule[] = des_set_key(key);
            int out[] = body(schedule, Eswap0, Eswap1);

            byte b[] = new byte[9];

            intToFourBytes(out[0], b, 0);
            intToFourBytes(out[1], b, 4);
            b[8] = 0;

            for (int i = 2, y = 0, u = 0x80; i < 13; i++) {
                for (int j = 0, c = 0; j < 6; j++) {
                    c <<= 1;

                    if (((int)b[y] & u) != 0)
                        c |= 1;

                    u >>>= 1;

                    if (u == 0) {
                        y++;
                        u = 0x80;
                    }
                    buffer[i] = alphabet.charAt(c);
                }
            }
            return new String(buffer);
        }
    }
}
