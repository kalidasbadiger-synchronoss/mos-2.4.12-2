/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/intermail/IntermailConfigDbReader.java#1 $
 *  
 */

package com.opwvmsg.utils.paf.config.intermail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.util.Utilities;

class IntermailConfigDbReader {

    private byte[] fileBuffer = null;
    private int currentPos = 0;
    private int startPos = 0;
    private int length = 0;
    private int lineNumber = 0;
    private StringBuffer currentBuffer = new StringBuffer(100);

    /**
     * Sets up a Config db reader for the specified config filename
     * 
     * @param configDbName the full path to the config.db file.
     */
    protected IntermailConfigDbReader(String configDbName, boolean localConfig) {
        FileInputStream fs = null;
        try {
            File configDb = new File(configDbName);
            length = (int)configDb.length();

            fs = new FileInputStream(configDb);
            fileBuffer = new byte[length];
            fs.read(fileBuffer, 0, length);
        } catch (FileNotFoundException fnf) {
            throw new ConfigException("Config file not found in path: "
                    + configDbName);
        } catch (IOException ioe) {
            throw new ConfigException("Error reading config.db at: "
                    + configDbName);
        } finally {
            if (fs != null) {
                try {
                    fs.close();
                } catch (IOException ioe) {
                }
            }
        }
        // ok at this point, bytes are read in
        // verify the header cksums

        int warnLen = IntermailConfig.warningLine.length();
        while (startPos < warnLen
                && (fileBuffer[currentPos++] == IntermailConfig.warningLine.charAt(startPos++))) {

        }
        if (startPos != warnLen) {
            throw new ConfigException(
                    "ERROR: config.db header file is missing or corrupted");
        }
        String newKey = getTrimLine();
        String oldKey = getTrimLine();
        // don't check cksum if this is a local config file.
        if (!localConfig) {
            startPos = currentPos;
            byte digest[] = new byte[16];
            try {
                MessageDigest md5 = MessageDigest.getInstance("md5");
                md5.update(fileBuffer, startPos, length - startPos);
                md5.digest(digest, 0, 16);
            } catch (Exception e) {
                throw new ConfigException("error computing config.db cksum!");
            }
            
            String calcSum = null;
            if (newKey == null || newKey.length() == 0 && oldKey != null
                    && oldKey.length() == 32) {
                // no new key, so calculate old sum:
                byte[] fakeDigest = new byte[16];
                for (int i = 0; i < 16; i++) {
                    fakeDigest[i] = fileBuffer[i + startPos];
                }
                newKey = oldKey;
                calcSum = Utilities.byteArrayToHexString(fakeDigest);
                
            } else {
                // no old key, so the new one better be good
                calcSum = Utilities.byteArrayToHexString(digest);
            }
            
            if (newKey == null || !newKey.equals(calcSum)) {
                throw new ConfigException("ERROR: config.db checksum is not valid!");
            }
        }
    }

    /**
     * Gets a line from the buffer, trimming the whitespace.
     * 
     * @return a line from the config.db file
     */
    private String getTrimLine() {
        if (currentPos >= length)
            return null;

        StringBuffer line = new StringBuffer(100);

        line.setLength(0);
        byte c = fileBuffer[currentPos++];
        while (c != '\n') {
            line.append((char)c);
            if (currentPos >= length)
                break;
            c = fileBuffer[currentPos++];
        }

        return line.toString().trim();
    }

    /**
     * Gets the next key name from the config.db
     * 
     * @return key name
     */
    private String getNextKey() {

        String curKey = null;
        if (getLine(':')) {
            curKey = currentBuffer.toString();
            if (validateKey(curKey)) {
                // got a valid key!
                return curKey;
            }
            throw new ConfigException(
                    "ERROR reading config.db at line number: " + lineNumber
                            + "; " + curKey + " is is not a valid key.");
        }
        throw new ConfigException("ERROR reading config.db at line number: "
                + lineNumber + "; key name not found.");
    }

    /**
     * Gets the next value in the config.db.
     * 
     * @return String with one value
     */
    private String getNextValue() {
        if (getLine('[') && getLine(']')) {
            return currentBuffer.toString();
        }
        throw new ConfigException("ERROR reading config.db at line number: "
                + lineNumber + "; value not found.");
    }

    /**
     * Validate that the key name appears to be valid
     * 
     * @param keyIn the key name
     * @return true if the key is valid, false otherwise
     */
    protected static boolean validateKey(String keyIn) {
        String[] splitKey = Utilities.dsvToArray(keyIn, '/', true);
        if (splitKey.length < 4) {
            return false;
        }
        for (int i = 1; i < splitKey.length; i++) {
            if (i == 1 && splitKey[i].equals("*")) {
                continue;
            }
            if (!validKeyChars(splitKey[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * Checks the String to see if it contains only valid key chars
     * 
     * @param key the key to check
     * @return true if the key contains only valid characters, false otherwise
     */
    private static boolean validKeyChars(String key) {
        for (int i = 0; i < key.length(); i++) {
            char currentChar = key.charAt(i);
            if (!(Character.isLetterOrDigit(currentChar) || currentChar == '/'
                    || currentChar == '-' || currentChar == '_'
                    || currentChar == '.' || currentChar == '%')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Reads the buffer until stopChar is found, or error. Takes into account
     * all of the intermail escape sequences. The resulting String is set in the
     * currentBuffer, and this String does not include the stopChar.
     * 
     * @param stopChar the characater to read to
     * @return true if the character was found, false otherwise
     */
    private boolean getLine(char stopChar) {
        // reads until stopChar, or error
        // returns true if stopChar was found
        // result is in the currentBuffer.

        currentBuffer.setLength(0);
        while (fileBuffer[currentPos] == '\n') { // skip over leading blank
            // lines
            currentPos++;
            lineNumber++;
        }
        int nesting = 0; // no nesting of []'s yet
        while (currentPos < length) { // loop is broken out of
            // replace escape sequences
            char ch = (char)fileBuffer[currentPos];
            if (ch == '\\') {
                if (++currentPos >= length) {
                    return false;
                }
                ch = (char)fileBuffer[currentPos++];
                switch (ch) {
                case 'r': // carriage return
                    currentBuffer.append('\r');
                    break;
                case 'n':
                    currentBuffer.append('\n');
                    break;
                case 't':
                    currentBuffer.append('\t');
                    break;
                case '\n':
                    lineNumber++;
                    break;
                case '[':
                    currentBuffer.append('[');// IM's '[' escape
                    break;
                case ']':
                    currentBuffer.append(']');// IM's ']' escape
                    break;
                case '\\':
                    currentBuffer.append('\\'); // IM's '\' escape
                    break;
                default:
                    currentBuffer.append(ch);
                }
                continue;
            } else {
                if (ch == '\n') {
                    lineNumber++;
                }
                if (Character.isWhitespace(ch) && stopChar == ':') {
                    return false; // uh oh... colon not found before a space
                }

                // advance currentPos for next loop.
                currentPos++;

                if (stopChar == ']' && ch == '[') {
                    nesting++;
                }
                if (ch == stopChar && nesting == 0) {
                    return true;
                }
                if (stopChar == ']' && ch == ']') {
                    nesting--;
                }
                currentBuffer.append(ch);
            }
        }
        return false;
    }

    /**
     * checks to see if there is another value for this key, or if the next line
     * starts a new key.
     * 
     * @return true, if there is another value, false otherwise
     */
    private boolean anotherValue() {
        char ch;
        if (currentPos >= length) {
            return false;
        }
        while (Character.isWhitespace(ch = (char)fileBuffer[currentPos])) {
            if (ch == '\n') {
                lineNumber++;
            }
            currentPos++;
            if (currentPos >= length) {
                return false;
            }
        }
        return ch == '[';
    }

    /**
     * Parses the config.db, and returns a ConfigDict with the key/value pairs
     * 
     * @return ConfigDict representing the config.db
     */
    protected ConfigDict parseConfigDb() {
        ConfigDict configDict = new ConfigDict();

        ArrayList currentValue = new ArrayList(20);
        Map keysToConvert = new HashMap();
        String currentKey = null;
        String[] valArray;

        lineNumber = 1;
        while (currentPos < length) {
            currentKey = getNextKey();
            boolean convertValuesLater = false;
            if (currentKey == null || currentKey.length() == 0) {
                break;
            }

            while (anotherValue()) {
                // get raw value
                String value = getNextValue();
                // check value for extended characters
                currentValue.add(value);
            }

            if (currentValue.size() > 0) {
                valArray = new String[currentValue.size()];
                currentValue.toArray(valArray);
                configDict.put(currentKey, valArray);
                currentValue.clear();
            }

        }

        return configDict;
    }

}
