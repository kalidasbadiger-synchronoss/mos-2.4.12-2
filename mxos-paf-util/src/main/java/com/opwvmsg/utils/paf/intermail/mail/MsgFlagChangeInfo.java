/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates all of the data required for RME StoreMsgFlags.
 */
public class MsgFlagChangeInfo {
    private UUID msgUUID;
    private int count;
    private int modifiedFlags;
    private int newFlagValues;
    private String keywords;

    /**
     * Constructor for MsgFlagChangeInfo
     * 
     */
    public MsgFlagChangeInfo() {
        this.msgUUID = UUID.randomUUID();
        this.count = 1; // For backward compatibility
        this.modifiedFlags = 0;
        this.newFlagValues = 0;
        this.keywords = "";
    }

    /**
     * Writes this MsgFlagChangeInfo object to RME stream
     * 
     * @param outStream the rme output stream
     * @throws IOException on any IO error
     */
    public void write(RmeOutputStream outStream) throws IOException {
        outStream.writeUUID(msgUUID);
        outStream.writeInt(count);
        outStream.writeByte(modifiedFlags);
        outStream.writeByte(newFlagValues);
        outStream.writeString(keywords);
    }

    /**
     * @return the msgUUID
     */
    public UUID getMsgUUID() {
        return msgUUID;
    }

    /**
     * @param msgUUID the msgUUID to set
     */
    public void setMsgUUID(UUID msgUUID) {
        this.msgUUID = msgUUID;
    }

    /**
     * @return the modifiedFlags
     */
    public int getModifiedFlags() {
        return modifiedFlags;
    }

    /**
     * @param modifiedFlags the modifiedFlags to set
     */
    public void setModifiedFlags(int modifiedFlags) {
        this.modifiedFlags = modifiedFlags;
    }

    /**
     * @return the newFlagValues
     */
    public int getNewFlagValues() {
        return newFlagValues;
    }

    /**
     * @param newFlagValues the newFlagValues to set
     */
    public void setNewFlagValues(int newFlagValues) {
        this.newFlagValues = newFlagValues;
    }

    /**
     * @return the keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

}
