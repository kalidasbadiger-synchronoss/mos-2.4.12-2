/*
 * Copyright (c) 2002 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/config/file/InitFileConfig.java#1 $
 *
 */

package com.opwvmsg.utils.paf.config.file;

import java.io.File;
import java.util.Map;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.config.NoSuchProviderException;

/**
 * This servlet uses <code>init</code> to set up a file-based provider
 * (properties file) on application startup.
 */
public class InitFileConfig extends HttpServlet {
    private static final Logger logger =
        Logger.getLogger("com.openwave.paf.config.file.InitFileConfig");

    private static final String CONFIG_PARAM = "config-file";
    private static final String DEFAULT_PATH = "WEB-INF" + File.separator +
                                               "config.properties";
    private static final String FILE_PROVIDER = "file";

    /**
     * Set up a Config object to use a file-based provider. The path to the
     * properties file will be set in the Config object. If the path is not 
     * provided as a servlet parameter, DEFAULT_PATH within the servlet is 
     * context is used.
     */
    public void init() throws ServletException {
        String path = getInitParameter(CONFIG_PARAM);
        // set the relative part of the path
        if (path == null) {
            path = DEFAULT_PATH;
        } else {
            path = path.replace('/', File.separatorChar);
        }

        // create the full path
        String base = getServletContext().getRealPath("/");
        if (! base.endsWith(File.separator)) {
            base = base + File.separator;
        }
        path = base + path;
        if (logger.isDebugEnabled()) {
            logger.debug("config file: "+path);
        }
        Map setup = new HashMap();
        setup.put("path", path);
        try {
            Config.setConfig(FILE_PROVIDER, setup);
            if (logger.isDebugEnabled()) {
                try {
                    Config config = Config.getInstance();
                } catch (ConfigException e) {
                    logger.debug("Config.getInstance()", e);
                }
            }
        } catch (NoSuchProviderException e) {
            throw new ServletException(e);
        }
    }
}
