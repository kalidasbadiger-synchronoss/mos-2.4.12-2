/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will delete the specified messages
 */
public class RemoveMultiMsgs extends AbstractMssOperation {

    // input
    private String name;
    private UUID folderUUID;
    private UUID[] msgUUIDs;
    private int options;
    
    //constants
    public static final int REMOVE_MULTI_MSGS_SPECIAL_DELETE_FLAG = 0x800;
    public static final int REMOVE_MULTI_MSGS_DISABLE_NOTIFICATION = 0x1000;

    // internal
    private String url;
    private String hostHeader;

    /**
     * Constructor for RemoveMultiMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID The folder where the messages to be deleted reside
     * @param msgUUIDs the list of uids to delete
     * @param options options for this operation. possible bits are: <br>
     *            SL_CREATE_MSG_SPECIAL_DELETE_FLAG = 0x800 - Set this if you
     *            want to mark message as specially deleted otherwise it will be
     *            processed as not-specially deleted <br>
     *            SL_CREATE_MSG_DISABLE_NOTIFICATION = 0x1000 - Set this if you
     *            do not want notification to be sent otherwise notification
     *            will be sent <br>
     */
    public RemoveMultiMsgs(String host, String name, String realm, UUID folderUUID,
            UUID[] msgUUIDs, int options) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_REMOVEMULTIMSGS);
        this.folderUUID = folderUUID;
        this.msgUUIDs = msgUUIDs;
        this.options = options;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (msgUUIDs == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "msgUUIDs", "RemoveMultiMsgs" }));
        }
        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_REMOVEMULTIMSGS", host, "dataModel=" + rmeDataModel }));
        } else {
            callRme(MSS_SL_REMOVEMULTIMSGS);
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_REMOVEMULTIMSGS));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeUUID(folderUUID);
        outStream.writeUUIDArray(msgUUIDs);
        outStream.writeInt(options);
        outStream.flush();
    }

    @Override
    protected void receiveHttpData() throws IOException {
        logEvents = inStream.readLogEvent();
    }

}
