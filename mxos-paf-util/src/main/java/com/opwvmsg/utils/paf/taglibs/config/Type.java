/*
 * Copyright (c) 2002-2005 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/taglibs/config/Type.java#1 $
 */

package com.opwvmsg.utils.paf.taglibs.config;

import java.util.HashMap;
import java.util.Map;


/**
 * An enumeration of the return types that can be requested.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */
class Type {

    public static final Type STRING = new Type("string");
    public static final Type INT = new Type("int");
    public static final Type FLOAT = new Type("float");
    public static final Type BOOLEAN = new Type("boolean");

    private static Map types = new HashMap();

    static {
        types.put(STRING.toString(), STRING);
        types.put(INT.toString(), INT);
        types.put(FLOAT.toString(), FLOAT);
        types.put(BOOLEAN.toString(), BOOLEAN);
    }

    /**
     * Gets the <code>Type</code> with the given name.
     *
     * @param name The name.
     *
     * @return The <code>Type</code>, or <code>null</code> if none exists
     *     with this name.
     */
    public static Type forName(String name) {
        return (Type) types.get(name);
    }


    /**
     * The name.
     */
    private final String name;


    /**
     * Creates an instance with the given name.
     *
     * @param name The name.
     */
    private Type(String name) {
        this.name = name;
    }


    /**
     * Gets the name of this instance.
     *
     * @return The name.
     */
    public String toString() {
        return name;
    }
}
