/*
 *
 *      Copyright 2001 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/io/FileManagerFactoryImpl.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util.io;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * FileManagerFactoryImpl is a simple implementation of the
 * FileManagerFactory interface.
 * <p>
 * A FileManagerFactory is responsible for generating FileManagers.  File 
 * management constraints are set on the factory that apply to all 
 * FileManagers created by the factory.  FileManagers may have constraints 
 * that are more restrictive than those specified by this the factory.
 *
 * @author Brad Kohn
 * @version $Revision: #1 $ 
 */

// In the future this class will implement a Lifecycle interface.  For now
// it simply has start and stop methods with addLifecycleListener and 
// removeLifecycleListener commented out.

public class FileManagerFactoryImpl implements FileManagerFactory {

    private final int maxAgeLimit;
    private final int thresholdLimit;
    private final float thresholdMarginLimit;
    private final int lowFrequencyLimit;
    private final int highFrequencyLimit;
    private final File rootDirectory;
    private Map managers;
    private ThreadGroup threadGroup;

    /**
     * Construct a file manager with the specified limits in the 
     * specified directory.
     *
     * @param maxAgeLimit The maximum age a file may have, specified in 
     *                    minutes
     * @param thresholdLimit The total size a FileManager's directory needs 
     *                    to be to cause files to be deleted upon the managers
     *                    next cleanup, specified in MB
     * @param thresholdMarginLimit The percentage of the threshold at which to 
     *                    stop a cleanup.  This must be a number between 0 
     *                    and 1.
     * @param lowFrequencyLimit The frequency is how often a FileManager's
     *                    thread checks for a cleanup, specified in minutes.
     *                    This is the lower limit for the frequency.
     * @param highFrequencyLimit The frequency is how often a FileManager's
     *                    thread checks for a cleanup, specified in minutes.
     *                    This is the upper limit for the frequency.
     * @param directory   The absolute path for the directory in which to 
     *                    create FileManagers.
     * @throws IllegalArgumentException if parameters fall outside of an
     *                    acceptable range.  maxAgeLimit must be >=0, 
     *                    thresholdLimit must be >= 0, thresholdMarginLimit
     *                    must be between 0 and 1, lowFrequencyLimit must be >
     *                    0, and highFrequencyLimit must be >= 
     *                    lowFrequencyLimit.
     */
    public FileManagerFactoryImpl(int maxAgeLimit, int thresholdLimit,
                         float thresholdMarginLimit, int lowFrequencyLimit,
                         int highFrequencyLimit, String directory) {
        if (maxAgeLimit < 0) {
            throw new IllegalArgumentException("maxAgeLimit must be >= 0");
        }
        if (thresholdLimit < 0) {
            throw new IllegalArgumentException("thresholdLimit must be >= 0");
        }
        if (thresholdMarginLimit < 0 || thresholdMarginLimit > 1) {
            throw new IllegalArgumentException("thresholdMarginLimit must be "
                                               + "between 0 and 1");
        }
        if (lowFrequencyLimit <= 0) {
            throw new IllegalArgumentException("lowFrequencyLimit must be "
                                               + "> 0");
        }
        if (highFrequencyLimit < lowFrequencyLimit) {
            throw new IllegalArgumentException("highFrequencyLimit must be "
                                               + ">= lowFrequencyLimit");
        }

        this.maxAgeLimit = maxAgeLimit;
        this.thresholdLimit = thresholdLimit;
        this.thresholdMarginLimit = thresholdMarginLimit;
        this.lowFrequencyLimit = lowFrequencyLimit;
        this.highFrequencyLimit = highFrequencyLimit;
        rootDirectory = new File(directory);
        managers = new HashMap(11);
    }
    
    /**
     * Construct a FileManager with the specified constraints, limited by
     * the constraints on this FileManagerFactory.
     *
     * @param maxAge      The maximum age a file may have, specified in 
     *                    minutes.  If this value is higher than this 
     *                    factory's limit, that value will be used instead.
     * @param threshold   The total size a FileManager's directory needs 
     *                    to be to cause files to be deleted upon the managers
     *                    next cleanup, specified in MB.  If this value is 
     *                    higher than this factory's limit, that value will be
     *                    used instead.
     * @param thresholdMargin The percentage of the threshold at which to 
     *                    stop a cleanup.  This must be a number between 0 
     *                    and 1.  If this value is higher than this factory's 
     *                    limit, that value will be used instead.
     * @param frequency   The frequency is how often the FileManager's thread 
     *                    checks for a cleanup, specified in minutes.  This 
     *                    value must be between this factory's limit range.  
     *                    If not, the closer of those values will be used 
     *                    instead.
     * @param directory   The name of the directory within this manager's root
     *                    directory (not the full path) in which to create 
     *                    managed files.
     * @return            A FileManager with the specified configuration.  If
     *                    a FileManager with the same configuration already
     *                    exists in the directory, that manager will be 
     *                    returned.  If there is a FileManager for that
     *                    directory with a different configuration, an
     *                    IllegalStateException will be thrown.  Otherwise a
     *                    new FileManager will be created and returned.
     * @throws IllegalArgumentException if parameters fall outside of an
     *                    acceptable range.  maxAge must be >=0, threshold
     *                    must be >= 0, thresholdMargin must be between 0 and
     *                    1, and frequency must be > 0.
     * @throws IllegalStateException if there is already a FileManager for
     *                    the directory with different a configuration or if
     *                    the manager has not yet been started.
     */
    public FileManager getFileManager(int maxAge, int threshold, 
                         float thresholdMargin, int frequency, 
                         String directory) {
        if (threadGroup == null) {
            throw new IllegalStateException("FileManager has not been " +
                                                    "started");
        }
        if (maxAge < 0) {
            throw new IllegalArgumentException("maxAge must be >= 0");
        }
        if (threshold < 0) {
            throw new IllegalArgumentException("threshold must be >= 0");
        }
        if (thresholdMargin < 0 || thresholdMargin > 1) {
            throw new IllegalArgumentException("thresholdMargin must be "
                                               + "between 0 and 1");
        }
        if (frequency <= 0) {
            throw new IllegalArgumentException("frequency must be > 0");
        }

        int fmMaxAge = maxAge < maxAgeLimit ? maxAge : maxAgeLimit;
        int fmThreshold = threshold < thresholdLimit ? 
                                       threshold : thresholdLimit;
        float fmThresholdMargin = thresholdMargin < thresholdMarginLimit ? 
                                       thresholdMargin : thresholdMarginLimit;
        int fmFrequency = frequency < lowFrequencyLimit ? 
                                       lowFrequencyLimit : frequency;
        fmFrequency = fmFrequency < highFrequencyLimit ? 
                                       fmFrequency : highFrequencyLimit;
        File fmDirectory = new File(rootDirectory, directory);

        fmDirectory.mkdirs();

        FileManagerImpl manager = null;
        boolean verifyParameters = false;

        synchronized (managers) {
            manager = (FileManagerImpl)managers.get(directory);
            if (manager == null) {
                manager = new FileManagerImpl(fmMaxAge, fmThreshold, 
                                              fmThresholdMargin, fmFrequency, 
                                              fmDirectory);
                managers.put(directory, manager);
            } else {
                verifyParameters = true;
            }
        }

        if (verifyParameters) {
            if (manager.getMaxAge() != fmMaxAge ||
                manager.getMaxAge() != fmMaxAge ||
                manager.getThreshold() != fmThreshold ||
                manager.getThresholdMargin() != fmThresholdMargin ||
                manager.getFrequency() != fmFrequency) {

                throw new IllegalStateException("Different FileManager already"
                                                + "exists");
            }
        } else {
            Thread thread = new Thread(threadGroup, manager, "FileManager " +
                                       manager.getDirectory());
            thread.setDaemon(true);
            thread.start();
        }
        return manager;
    }
    
    /**
     * Create the ThreadGroup for all of this factory's FileManager threads.
     */
    synchronized public void start() {
        threadGroup = new ThreadGroup("FileManager ThreadGroup " + 
                                      rootDirectory.getAbsolutePath());
    }
    
    /**
     * Stop all managers' cleanup threads.
     */
    synchronized public void stop() {
        Thread[] threads = new Thread[threadGroup.activeCount()];
        int numThreads = threadGroup.enumerate(threads);
        threadGroup.interrupt();
        try {
            for (int i = 0; i < numThreads; i++) {
                threads[i].join();
            }
            threadGroup.destroy();
        } catch (InterruptedException e) {
        }
        threadGroup = null;
        synchronized (managers) {
            managers.clear();
        }
    }

    /**
     * Add a LifecycleEvent listener to this manager
     *
     * @param listener The listener to add
     */
    //public void addLifecycleListener(LifecycleListener listener);


    /**
     * Remove a LifecycleEvent listener to this manager
     *
     * @param listener The listener to remove
     */
    //public void removeLifecycleListener(LifecycleListener listener);
}

