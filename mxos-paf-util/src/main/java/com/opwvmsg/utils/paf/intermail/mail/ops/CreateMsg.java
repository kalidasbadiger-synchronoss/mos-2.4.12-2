/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.extsrv.ServiceManager;
import com.opwvmsg.utils.paf.intermail.extsrv.clients.MessageExamine;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MimeMsg;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will create a new message in the user's store.
 */
public class CreateMsg extends AbstractMssOperation {
    // skip MimeMessage Parsing on create Message API
    public final static String SKIP_MIME_PARSING_ON_CREATE = "skipMimeParsingOnCreateMessage";

    // input
    private String name;

    private byte[] textBytes;
    private String from;
    private int options;
    private int accessId;
    private String flags;
    private String[] userFlags;
    private String pathname;
    private int imapUid;
    private long expireTime;
    private boolean isPrivate;
    private String username;
    private String peerIp;

    private UUID folderUUID;
    private String keywords;
    private String msName;
    private String oldMsgId;
    private String conversationId;

    // output
    private Msg newMsg;
    private UUID msgUUID;
    private int folderUidValidity;
    private int msgUid;
    private String docId;
    
    // internal
    private String url;
    private String hostHeader;
    private String[] pathnameArray;
    private int uid;
    private String msgId;
    private long arrivalTime;
    private String msgType;
    private String msgPriority;

    // constants
    public static final int CREATE_MSG_IGNORE_QUOTA = 0x01;
    public static final int CREATE_MSG_BOUNCE_NOTIFY = 0x02;
    public static final int CREATE_MSG_FOLDER_ON_DEMAND = 0x04;
    public static final int CREATE_MSG_FOR_IMAP_APND = 0x08;
    public static final int CREATE_MSG_SUPPRESS_MERS = 0x10;
    public static final int CREATE_MSG_STRICT_MSGID = 0x20;
    public static final int CREATE_MSG_GET_ATTACHMENT_STATUS = 0x40;

    public static final int SL_CREATE_MSG_IGNORE_QUOTA = 0x02;
    public static final int SL_CREATE_MSG_BOUNCE_NOTIFY = 0x04;
    public static final int SL_CREATE_MSG_SET_FLAGS = 0x08;
    public static final int SL_CREATE_MSG_WITH_COS = 0x10;
    public static final int SL_CREATE_MSG_FOLDER_ON_DEMAND = 0x20;
    public static final int SL_CREATE_MSG_STRICT_MSGID = 0x200;
    public static final int SL_CREATE_MSG_CHECK_SPECIAL_QUOTA = 0x400;
    public static final int SL_CREATE_MSG_SPECIAL_DELETE_FLAG = 0x800;
    public static final int SL_CREATE_MSG_DISABLE_NOTIFICATION = 0x1000;
    public static final int SL_CREATE_MSG_SAFE_INSERT = 0x2000; //deprecated
    public static final int SL_CREATE_MSG_CONVERSATION_ID_PRESENT = 0x4000; 

    /**
     * Constructor for CreateMsg
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder where this message will be created
     * @param text The text of this message - only the lower 8 bits of each
     *            character are used.
     * @param from the envelope from address for this message
     * @param flags a String of 7 bytes representing the initial msg flags
     * @param isPrivate flag to set if this message is private
     * @param arrivalTime arrival time.  Set zero for default current time.
     * @param expireTime a (long) epoch time specifying when this message expires
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            CREATE_MSG_IGNORE_QUOTA - bypass the quota check when
     *            inserting this message. <br>
     *            CREATE_MSG_BOUNCE_NOTIFY - notify user if this message is
     *            bounced b/c of quota <br>
     *            CREATE_MSG_FOLDER_ON_DEMAND - create folder on the fly if
     *            necessary <br>
     *            CREATE_MSG_FOR_IMAP_APND - this message is for IMAP append <br>
     *            CREATE_MSG_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     *            CREATE_MSG_STRICT_MSGID - raise error on duplicate msg ids <br>
     * @param username - for Legal Intercept - the current user's username
     * @param peerIp - for Legal Intercept - the IP address of the user
     */
    public CreateMsg(String host, String name, String pathname, byte[] text,
            String from, String flags, String[] userFlags, boolean isPrivate,
            long arrivalTime, long expireTime, int accessId, int options,
            String username, String peerIp) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        textBytes = text;
        this.from = from;
        this.pathname = pathname;
        if (flags == null) {
            flags = Msg.DEFAULT_FLAGS;
        }
        this.flags = flags;
        this.userFlags = userFlags;
        this.isPrivate = isPrivate;
        this.imapUid = -1; // have imail create one
        this.arrivalTime = arrivalTime;
        this.expireTime = expireTime;
        url = getMsUrl(host, name, MSS_P_CL_CREATEMSG);
        pathnameArray = buildPathnameArray(pathname);
        this.accessId = accessId;
        this.options = options;
        this.username = username;
        this.peerIp = peerIp;
    }

    /**
     * Constructor for CreateMsg
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder where this message will be created
     * @param text The text of this message - only the lower 8 bits of each
     *            character are used.
     * @param from the envelope from address for this message
     * @param flags a String of 7 bytes representing the initial msg flags
     * @param isPrivate flag to set if this message is private
     * @param imapUid a (int) specifying imapUid of the message
     * @param arrivalTime arrival time.  Set zero for default current time.
     * @param expireTime a (long) epoch time specifying when this message expires
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            CREATE_MSG_IGNORE_QUOTA - bypass the quota check when
     *            inserting this message. <br>
     *            CREATE_MSG_BOUNCE_NOTIFY - notify user if this message is
     *            bounced b/c of quota <br>
     *            CREATE_MSG_FOLDER_ON_DEMAND - create folder on the fly if
     *            necessary <br>
     *            CREATE_MSG_FOR_IMAP_APND - this message is for IMAP append <br>
     *            CREATE_MSG_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     *            CREATE_MSG_STRICT_MSGID - raise error on duplicate msg ids <br>
     * @param username - for Legal Intercept - the current user's username
     * @param peerIp - for Legal Intercept - the IP address of the user
     */
    public CreateMsg(String host, String name, String pathname, byte[] text,
            String from, String flags, String[] userFlags, boolean isPrivate,
            int imapUid, long arrivalTime, long expireTime, int accessId,
            int options, String username, String peerIp) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        textBytes = text;
        this.from = from;
        this.pathname = pathname;
        if (flags == null) {
            flags = Msg.DEFAULT_FLAGS;
        }
        this.flags = flags;
        this.userFlags = userFlags;
        this.isPrivate = isPrivate;
        this.imapUid = imapUid; // have imail create one
        this.arrivalTime = arrivalTime;
        this.expireTime = expireTime;
        url = getMsUrl(host, name, MSS_P_CL_CREATEMSG);
        pathnameArray = buildPathnameArray(pathname);
        this.accessId = accessId;
        this.options = options;
        this.username = username;
        this.peerIp = peerIp;
    }

    /**
     * Constructor for CreateMsg for Stateless RME.
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID The folder where this message will be created
     * @param text The text of this message - only the lower 8 bits of each
     *            character are used.
     * @param flags a String of 7 bytes representing the initial msg flags
     * @param keywords User Flags
     * @param isPrivate flag to set if this message is private
     * @param arrivalTime arrival time. Set zero for default current time.
     * @param expireTime a (long) epoch time specifying when this message
     *            expires
     * @param options options for this operation. possible bits are: <br>
     *            SL_CREATE_MSG_IGNORE_QUOTA = 0x02 - bypass the quota check
     *            when inserting this message. <br>
     *            SL_CREATE_MSG_BOUNCE_NOTIFY = 0x04 - notify user if this
     *            message is bounced b/c of quota <br>
     *            SL_CREATE_MSG_SET_FLAGS = 0x08 - create message with Message
     *            flags <br>
     *            SL_CREATE_MSG_WITH_COS = 0x10 - create folder on the fly if
     *            necessary (NOT SUPPORTED)<br>
     *            SL_CREATE_MSG_FOLDER_ON_DEMAND = 0x20 - create folder on the
     *            fly if necessary <br>
     *            SL_CREATE_MSG_STRICT_MSGID = 0x200 - Request MSS to use
     *            MTA-created message id. <br>
     *            SL_CREATE_MSG_CHECK_SPECIAL_QUOTA = 0x400 - Request MSS to use
     *            Check Special Quota for Message Deposit. (Used for Special
     *            Handling feature for CDC-25 ) <br>
     *            SL_CREATE_MSG_SPECIAL_DELETE_FLAG = 0x800 - Set this if you
     *            want to mark message as specially deleted otherwise it will be
     *            processed as not-specially deleted <br>
     *            SL_CREATE_MSG_DISABLE_NOTIFICATION = 0x1000 - Set this if you
     *            do not want notification to be sent otherwise notification
     *            will be sent <br>
     *            SL_CREATE_MSG_CONVERSATION_ID_PRESENT = 0x4000 - Request MSS
     *            to use the conversationId if present <br>
     * @param msName - the current user's username/mailboxId
     * @param oldMsgId - oldMsgId of the migrated message (used by migration)
     * @param conversationId - conversationId of the migrated message. If
     *            present and do not generate it on MSS. (used by migration)
     * 
     */
    public CreateMsg(String host, String name, String realm, UUID folderUUID,
            byte[] text, String from, String flags, String keywords,
            boolean isPrivate, long arrivalTime, long expireTime, int options,
            String msName, String oldMsgId, String conversationId) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.folderUUID = folderUUID;
        textBytes = text;
        this.from = from;
        if (flags == null)
            this.flags = "TFFFFF-";
        else
            this.flags = flags;
        this.keywords = keywords;
        this.isPrivate = isPrivate;
        this.arrivalTime = arrivalTime;
        this.expireTime = expireTime;
        url = getMsUrl(host, name, MSS_SL_CREATEMESSAGE);
        this.options = options;
        this.msName = msName;
        this.oldMsgId = oldMsgId;
        this.imapUid = -1; // let imail create one
        this.conversationId = conversationId;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * Constructor for CreateMsg for Stateless RME.
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID The folder where this message will be created
     * @param text The text of this message - only the lower 8 bits of each
     *            character are used.
     * @param flags a String of 7 bytes representing the initial msg flags
     * @param keywords User Flags
     * @param isPrivate flag to set if this message is private
     * @param arrivalTime arrival time.  Set zero for default current time.
     * @param expireTime a (long) epoch time specifying when this message expires
     * @param options options for this operation. possible bits are: <br>
     *            SL_CREATE_MSG_IGNORE_QUOTA = 0x02 - bypass the quota check
     *            when inserting this message. <br>
     *            SL_CREATE_MSG_BOUNCE_NOTIFY = 0x04 - notify user if this
     *            message is bounced b/c of quota <br>
     *            SL_CREATE_MSG_SET_FLAGS = 0x08 - create message with Message
     *            flags <br>
     *            SL_CREATE_MSG_WITH_COS = 0x10 - create folder on the fly if
     *            necessary (NOT SUPPORTED)<br>
     *            SL_CREATE_MSG_FOLDER_ON_DEMAND = 0x20 - create folder on the
     *            fly if necessary <br>
     *            SL_CREATE_MSG_STRICT_MSGID = 0x200 - Request MSS to use
     *            MTA-created message id. <br>
     *            SL_CREATE_MSG_CHECK_SPECIAL_QUOTA = 0x400 - Request MSS to use
     *            Check Special Quota for Message Deposit. (Used for Special
     *            Handling feature for CDC-25 ) <br>
     *            SL_CREATE_MSG_SPECIAL_DELETE_FLAG = 0x800 - Set this if you
     *            want to mark message as specially deleted otherwise it will be
     *            processed as not-specially deleted <br>
     *            SL_CREATE_MSG_DISABLE_NOTIFICATION = 0x1000 - Set this if you
     *            do not want notification to be sent otherwise notification
     *            will be sent <br>
     *            SL_CREATE_MSG_CONVERSATION_ID_PRESENT = 0x4000 - Request MSS
     *            to use the conversationId if present<br>
     * @param msName - the current user's username/mailboxId
     * @param oldMsgId - oldMsgId of the migrated message (used by migration)
     * @param imapUid a (int) specifying imapUid of the message, pass -1 for
     *            imail to create imapUid
     * @param conversationId - conversationId of the migrated message. If
     *            present and do not generate it on MSS. (used by migration)           
     */
    public CreateMsg(String host, String name, String realm, UUID folderUUID,
            byte[] text, String from, String flags, String keywords,
            boolean isPrivate, long arrivalTime, long expireTime, int options,
            String msName, String oldMsgId, int imapUid, String conversationId) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        this.folderUUID = folderUUID;
        textBytes = text;
        this.from = from;
        if (flags == null)
            this.flags = "TFFFFF-";
        else
            this.flags = flags;
        this.keywords = keywords;
        this.isPrivate = isPrivate;
        this.arrivalTime = arrivalTime;
        this.expireTime = expireTime;
        url = getMsUrl(host, name, MSS_SL_CREATEMESSAGE);
        this.options = options;
        this.msName = msName;
        this.oldMsgId = oldMsgId;
        this.imapUid = imapUid;
        this.conversationId = conversationId;
        this.hostHeader = getHostHeader(host, name, realm);
    }
    
    /**
     * Get the Msg object returned from the server
     * 
     * @return the new message object
     */
    public Msg getMsg() {
        return newMsg;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }

        if (textBytes == null || textBytes.length == 0) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "textBytes", "CreateMsg" }));
        }

        if (rmeDataModel == RmeDataModel.CL) {
            if (pathname == null) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Ms.BadRequestArg",
                                new String[] { "pathname", "CreateMsg" }));
            }

            // need to send to LI before creating
            if (username != null
                    && ServiceManager
                            .hookHasEnabledServices(MessageExamine.MESSAGE_WRITE_REQUEST)) {
                Msg[] messages = new Msg[1];
                messages[0] = new Msg(textBytes);
                MessageExamine MEop = new MessageExamine(username,
                        Config.getDefaultApp(), peerIp, "CreateMsg", messages,
                        0, messages[0].getText().length, false, pathname, host,
                        name);
                try {
                    MEop.execute(); // do not return any errors from LI
                    // they will just be logged.
                } catch (IntermailException ie) {
                }
            }

            callRme(MSS_P_CL_CREATEMSG);
        } else { // Leopard
            if ((options & SL_CREATE_MSG_SET_FLAGS) == SL_CREATE_MSG_SET_FLAGS) {
                if (flags == null) {
                    throw new IntermailException(LogEvent.ERROR,
                            LogEvent.formatLogString("Ms.BadRequestArg",
                                    new String[] { "flags", "CreateSLMsg" }));
                }
            }
            callRme(MSS_SL_CREATEMESSAGE);
        }

    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {

        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);

        // from and text are the only two required fields;
        writeMessage();

        Msg.writeMsgFlags(outStream, flags);

        // keyword support
        outStream.writeStringArray(userFlags);
        outStream.writeEmptyCos();

        outStream.writeInt(accessId);
        outStream.writeBoolean(isPrivate);
        outStream.writeInt(imapUid);
        outStream.writeInt(options);
        outStream.writeInt((int) (expireTime));
        if (inStream.rmeProtAtLeast(AbstractOperation.RME_INDEXING_SERVER_VER)) {
            outStream.writeString(""); // Sending empty pRequestedMessageId
        }
    }

    /**
     * This creates an RME Message object on RME over HTTP given the content
     * provided.
     * 
     * @throws IOException
     */
    private void writeMessageOnHttp() throws IOException {

        outStream.writeBoolean(true); // have it

        outStream.writeString(from);

        // write raw string bytes - no length prefix
        byte[] bytes2 = new byte[5];
        bytes2[4] = 0;
        for (int i = 0; i < 4; i++) {
            bytes2[i] = (byte) "SMTP".charAt(i);
        }
        outStream.write(bytes2, 0, bytes2.length);
        outStream.writeInt(0); // empty recips
        if (arrivalTime > 0) {
            outStream.writeInt((int) arrivalTime);
        } else {
            outStream.writeInt((int) (System.currentTimeMillis() / 1000));
        }
        outStream.writeInt(textBytes.length); // mimeSize
        boolean skipParsing = false;
        final String temp = System.getProperty(SKIP_MIME_PARSING_ON_CREATE);
        if (!"".equals(temp)) {
            try {
                skipParsing = Boolean.parseBoolean(temp);
            } catch (Exception e) {
                skipParsing = false;
            }
        }
        if (skipParsing) {
            outStream.writeBoolean(false); // skip mime decode
        } else {
            MimeMsg mimeMessage = new MimeMsg(textBytes, 0, textBytes.length);
            mimeMessage.writeToStream(outStream);
        }
        outStream.writeInt(0); // firstAccessedTime
        outStream.writeInt(0); // lastAccessedTime
        outStream.writeInt(0); // expiryTime
        outStream.writeInt(0); // firstSeenTime
        outStream.writeInt(0); // no attributes
        outStream.writeInt(1); // 1 body part;
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x8648862A); // oid part 1
        outStream.writeInt(0x34030FF7); // oid part2
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x8648862A); // p1
        outStream.writeInt(0x36040FF7); // x400 oid
        outStream.writeByte(0x0A); // unknown mime message
        outStream.writeString("X-Intermail-Unknown-MIME-Type=unparsedmessage");
        outStream.writeString("");
        outStream.writeString("");
        outStream.writeString("");
        // body text, finally!
        int textLen = textBytes.length;
        outStream.writeInt(textLen);
        // write raw string bytes - no null terminate
        outStream.write(textBytes, 0, textBytes.length);
        // done!
    }

    /**
     * This creates an RME Message object given the content provided.
     * 
     * @throws IOException
     */
    private void writeMessage() throws IOException {

        outStream.writeBoolean(true); // have it

        outStream.writeString(from);

        // write raw string bytes - no length prefix
        byte[] bytes2 = new byte[5];
        bytes2[4] = 0;
        for (int i = 0; i < 4; i++) {
            bytes2[i] = (byte) "SMTP".charAt(i);
        }
        outStream.write(bytes2, 0, bytes2.length);
        outStream.writeInt(0); // empty recips
        if (arrivalTime > 0) {
            outStream.writeInt((int) arrivalTime);
        } else {
            outStream.writeInt((int) (System.currentTimeMillis() / 1000));
        }
        outStream.writeInt(textBytes.length); // mimeSize
        boolean skipParsing = false;
        final String temp = System.getProperty(SKIP_MIME_PARSING_ON_CREATE);
        if (!"".equals(temp)) {
            try {
                skipParsing = Boolean.parseBoolean(temp);
            } catch (Exception e) {
                skipParsing = false;
            }
        }
        if (skipParsing) {
            outStream.writeBoolean(false); // skip mime decode
        } else {
            MimeMsg mimeMessage = new MimeMsg(textBytes, 0, textBytes.length);
            mimeMessage.writeToStream(outStream);
        }
        outStream.writeInt(0); // no attributes
        outStream.writeInt(1); // 1 body part;
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x2A864886); // oid part 1
        outStream.writeInt(0xF70F0334); // oid part2
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x2A864886); // p1
        outStream.writeInt(0xF70F0436); // x400 oid
        outStream.writeByte(0x0A); // unknown mime message
        outStream.writeString("X-Intermail-Unknown-MIME-Type=unparsedmessage");
        outStream.writeString("");
        outStream.writeString("");
        outStream.writeString("");
        // body text, finally!
        int textLen = textBytes.length;
        outStream.writeInt(textLen);
        // write raw string bytes - no null terminate
        outStream.write(textBytes, 0, textBytes.length);
        // done!
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        uid = inStream.readInt();
        msgId = inStream.readString();
        arrivalTime = inStream.readInt();
        msgType = inStream.readString();
        msgPriority = inStream.readString();
        if (inStream.rmeProtAtLeast(AbstractOperation.RME_INDEXING_SERVER_VER)) {
            inStream.readString(); // Reading Doc id
        }
        if (inStream.rmeProtAtLeast(AbstractOperation.RME_MX8_VER)) {
            inStream.readBoolean(); // Reading hasAttachementKnownKnown
            inStream.readBoolean(); // Reading hasAttachementKnown
        }
        newMsg = new Msg(uid, msgId, arrivalTime, msgType, msgPriority, flags);
        logEvents = inStream.readLogEvent();
    }

    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_CREATEMESSAGE));
        int rmeVersion = Integer.parseInt(System.getProperty("mssRmeVersion",
                HTTP_RMEVERSION));
        if (rmeVersion >= 176) {
            postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION_176);
        } else {
            postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);
        }

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeInt(options);
        writeMessageOnHttp();
        outStream.writeUUID(folderUUID);
        outStream.writeString(msName);

        if ((options & SL_CREATE_MSG_WITH_COS) == SL_CREATE_MSG_WITH_COS) {
            outStream.writeEmptyCos();
        }

        if ((options & SL_CREATE_MSG_SET_FLAGS) == SL_CREATE_MSG_SET_FLAGS) {
            Msg.writeMsgFlags(outStream, flags);
            // keyword support
            outStream.writeString(keywords);
        }

        outStream.writeBoolean(isPrivate);
        outStream.writeInt((int) expireTime);
        outStream.writeString(oldMsgId);
        outStream.writeInt(imapUid);
        if (rmeVersion >= 176
                && (options & SL_CREATE_MSG_CONVERSATION_ID_PRESENT)
                == SL_CREATE_MSG_CONVERSATION_ID_PRESENT) {
            outStream.writeString(conversationId);
        }
        outStream.flush();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.opwvmsg.utils.paf.intermail.rme.AbstractOperation#receiveHttpData()
     */
    protected void receiveHttpData() throws IOException {
        msgUUID = inStream.readUUID(); // UUID of message being created
        folderUidValidity = inStream.readInt(); // IMAP folder UID validity
        msgUid = inStream.readInt(); // message UID
        docId = inStream.readByteString(); // docId - string format of msgUUID
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the msgUUID
     */
    public UUID getMsgUUID() {
        return msgUUID;
    }

    /**
     * @return the folderUidValidity
     */
    public int getFolderUidValidity() {
        return folderUidValidity;
    }

    /**
     * @return the msgUid
     */
    public int getMsgUid() {
        return msgUid;
    }

    /**
     * @return the docId
     */
    public String getDocId() {
        return docId;
    }

}
