/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/io/RmeSocket.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * An RmeSocket extends the Socket object and provides some extra processing on
 * the socket including the client or server side handshake, and conversion of
 * input/output streams to RME specific Input/Output Streams.
 */
public class RmeSocket extends Socket {

    private RmeOutputStream rmeOutStream = null;
    private RmeInputStream rmeInStream = null;
    private String host;
    private int port;
    private int rmeVer = AbstractOperation.RME_CURRENT_VERSION;
    private boolean isReady = false;
    private boolean readyToClose = false;
    private boolean connectionIsNew = true;

    private static final Logger logger = Logger.getLogger(RmeSocket.class);

    /**
     * RmeSocket constructer for RmeServerSocket. This will create a blank
     * RmeSocket object to be filled in when RmeServerSocket accepts a
     * connection.
     */
    protected RmeSocket() {
    }

    /**
     * Performs the server side of an RME handshake. Assumes that the socket
     * connection has already been established by RmeServerSocket.
     * 
     * @throws IOException on any io error or rme handshake error.
     */
    protected void initServerSession() throws IOException {
        // this is called from a server after an accept has
        // been made
        // set up the input stream overrides here
        LogEvent returnLog = null;
        rmeInStream = new RmeInputStream(getInputStream());
        rmeOutStream = new RmeOutputStream(getOutputStream());
        boolean littleEndian = rmeInStream.readBoolean();
        if (littleEndian) {
            rmeInStream.setLittleEndian(true);
        }
        int clientVer = rmeInStream.readInt();
        if (clientVer < AbstractOperation.RME_MIN_VER) {
            // make log event
            returnLog = new LogEvent(LogEvent.URGENT,
                                     "Rme.ProtocolMismatch",
                                     new String[] {
                                             Integer.toString(clientVer),
                                             Integer.toString(rmeVer) });

            // put data fmt
            rmeOutStream.setRmeVer(AbstractOperation.RME_MIN_VER);
            rmeInStream.setRmeVer(AbstractOperation.RME_MIN_VER);
            rmeOutStream.write(0); // java uses big endian
            rmeOutStream.writeInt(AbstractOperation.RME_MIN_VER);

            // send log event
            rmeOutStream.writeLogEvent(returnLog);
            // flush
            rmeOutStream.flush();

            return;
        }
        if (clientVer < rmeVer) {
            // if the client is lower than us,
            // then pretend that we are lower..
            rmeVer = clientVer;
            rmeOutStream.setRmeVer(clientVer);
            rmeInStream.setRmeVer(clientVer);
        }
        rmeOutStream.write(0); // java uses big endian
        rmeOutStream.writeInt(rmeVer);
        rmeOutStream.flush();

        String username = rmeInStream.readString();
        String pass = rmeInStream.readString();
        int auth = rmeInStream.readInt();
        // right now there is no auth checking!

        rmeOutStream.writeLogEvent(returnLog);
        rmeOutStream.flush();

        isReady = true;
    }

    /**
     * RmeSocket client constructor. This constructor will open the socket to
     * the server specified, set the timeout, and perform the intial rme
     * handshake. If everything succeeds (no exception is thrown) then the
     * socket is open and ready to send the first rme client request.
     * 
     * @param hostIn - the hostname of the remote server
     * @param portIn - the port of the remote server
     * @param rmeTimeout - the socket timeout
     * @throws UnknownHostException - if the host cannot be found
     * @throws IOException - if there is any error setting up the socket itself
     * @throws IntermailException - if there is any RME error in the handshake.
     */
    public RmeSocket(String hostIn, int portIn, int rmeTimeout)
            throws UnknownHostException, IOException, IntermailException {

        // call the superclass constructor with no connection information
        super();

        host = hostIn;
        port = portIn;
        
        Config config = Config.getInstance();
        int connectTimeout = config.getInt("rmeConnectTimeout", 0) * 1000;
        
        // connect the socket with a connection timeout
        connect(new InetSocketAddress(host, port), connectTimeout);

        // set any other standard socket things here, if needed.
        if (rmeTimeout != -1) {
            setSoTimeout(rmeTimeout);
        }

        // set up the input stream overrides here
        rmeInStream = new RmeInputStream(super.getInputStream());
        rmeOutStream = new RmeOutputStream(super.getOutputStream());

        //start rme handshake
        rmeOutStream.write(0); // java uses big endian
        rmeOutStream.writeInt(AbstractOperation.RME_CURRENT_VERSION);
        rmeOutStream.flush();

        boolean littleEndian = rmeInStream.readBoolean();
        rmeInStream.setLittleEndian(littleEndian);

        int rmeServerVer = rmeInStream.readInt();
        if (rmeServerVer > rmeVer
                || rmeServerVer < AbstractOperation.RME_MIN_VER) {
            throw new IntermailException(LogEvent.ERROR,
                                         LogEvent.formatLogString("Rme.ProtocolMismatch",
                                                                  new String[] {
                                                                          Integer.toString(rmeVer),
                                                                          Integer.toString(rmeServerVer) }));
        }
        rmeOutStream.setRmeVer(rmeServerVer);
        rmeInStream.setRmeVer(rmeServerVer);

        // these are dummy values and are ignored.
        rmeOutStream.sendAuth("imail", "imail");
        rmeOutStream.flush();

        if (rmeInStream.readBoolean()) { // has log event
            LogEvent status = new LogEvent(rmeInStream);
            close();
            throw status.getIntermailException();
        }

        isReady = true;
    }

    /**
     * Test to see if the socket is ready for an RME operation
     * 
     * @return true if socket is ready.
     */
    public boolean isReady() {
        return isReady;
    }

    /**
     * Gets the hostname of the server that this socket is connected to.
     * 
     * @return hostname
     */
    public String getHost() {
        return host;
    }

    /**
     * Test to determine if socket is ready to be closed.
     * 
     * @return Returns true if this socket has been marked for closure. Returns
     *         false if socket can return to the socket pool
     */
    public boolean readyToClose() {
        return readyToClose;
    }

    /**
     * This methods marks the socket for closure as soon as it is released. It
     * will not re-enter a socket pool.
     */
    public void setReadyToClose() {
        readyToClose = true;
    }

    /**
     * Sets a flag to mark that this socket has processed at least one RME
     * transaction.
     */
    public void setUsed() {
        connectionIsNew = false;
    }

    /**
     * Test to determine if this is a new connection, or an old one from a
     * socket pool
     * 
     * @return true if this is a new socket, false otherwise.
     */
    public boolean isNew() {
        return connectionIsNew;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.net.Socket#getInputStream()
     */
    public InputStream getInputStream() {
        return rmeInStream;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.net.Socket#getOutputStream()
     */
    public OutputStream getOutputStream() {
        return rmeOutStream;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.net.Socket#close()
     */
    public void close() throws IOException {
        // don't bother with the "graceful" shutdown..
        // (i.e sending RMECLOSE to server)
        // the server will detect that the socket closed
        // and do the right thing. (c-api doesn't use graceful
        // shutdown either).
        super.close();
        isReady = false;
    }

}

