/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates all of the AccessInfo data RME
 * 
 * @author mxos-dev
 */
public class AccessInfo {
    private static final Logger logger = Logger.getLogger(AccessInfo.class);

    // Attributes
    private long lastLoginTime;
    private long lastSuccessfulLoginTime;
    private long lastFailedLoginTime;
    private long numFailedLoginAttempts;
    private long smtpLastLoginTime;
    private long popLastLoginTime;
    private long imapLastLoginTime;
    private long webmailLastLoginTime;
    private int count = 0;

    // Constants for MailboxAccess Info Attribute Types
    public static final byte ATTR_LAST_LOGIN_TIME = (byte) 0x02;
    public static final byte ATTR_LAST_SUCCESSFUL_LOGIN_TIME = (byte) 0x03;
    public static final byte ATTR_LAST_FAILED_LOGIN_TIME = (byte) 0x04;
    public static final byte ATTR_NUM_FAILED_LOGIN_ATTEMPTS = (byte) 0x05;
    public static final byte ATTR_POP_LAST_LOGIN_TIME = (byte) 0x06;    
    public static final byte ATTR_IMAP_LAST_LOGIN_TIME = (byte) 0x07;    
    public static final byte ATTR_SMTP_LAST_LOGIN_TIME = (byte) 0x08;
    public static final byte ATTR_WEBMAIL_LAST_LOGIN_TIME = (byte) 0x09;
    
    /**
     * Default Constructor.
     * 
     * @param inStream
     * @throws IOException
     */
    public AccessInfo(){
        this.lastLoginTime = -1;
        this.lastSuccessfulLoginTime = -1;
        this.lastFailedLoginTime = -1;
        this.numFailedLoginAttempts = -1;
        this.smtpLastLoginTime = -1;
        this.popLastLoginTime = -1;
        this.imapLastLoginTime = -1;
        this.webmailLastLoginTime = -1;        
    }

    /**
     * Constructor. Reads the input Stream and constructs the AccessInfo.
     * 
     * @param inStream
     * @throws IOException
     */
    public AccessInfo(RmeInputStream inStream) throws IOException {
        byte attrType = 0;
        int numAttributes = inStream.readInt();
        for (int i = 0; i < numAttributes; i++) {
            attrType = inStream.readByte();
            //MSS always send string size. Here we do not need to use it but still we have to extract it from stream
            int size = inStream.readInt();
            switch (attrType) {
            case ATTR_LAST_LOGIN_TIME:
                lastLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_LAST_SUCCESSFUL_LOGIN_TIME:
                lastSuccessfulLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_LAST_FAILED_LOGIN_TIME:
                lastFailedLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_NUM_FAILED_LOGIN_ATTEMPTS:
                numFailedLoginAttempts = inStream.readLong64Blob();
                break;
            case ATTR_SMTP_LAST_LOGIN_TIME:
                smtpLastLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_POP_LAST_LOGIN_TIME:
                popLastLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_IMAP_LAST_LOGIN_TIME:
                imapLastLoginTime = inStream.readLong64Blob();
                break;
            case ATTR_WEBMAIL_LAST_LOGIN_TIME:
                webmailLastLoginTime = inStream.readLong64Blob();
                break;
            default:
                logger.debug("Invalid Attribute Type " + attrType
                        + " received in the MSS_SL_MAILBOXACCESSINFO response.");
            }
        }
    }

    /**
     * Method to write AccessInfo.
     * 
     * @param accessInfo
     * @param outStream: OutStream to write MessageMetadata.
     * @return: void.
     * @throws: IOException
     */
    public void write(RmeOutputStream outStream) throws IOException {
        
        outStream.writeInt(this.count);
        if (this.lastLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_LOGIN_TIME);
            outStream.writeLongBlob(lastLoginTime, true);
        }
        if (this.lastSuccessfulLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_SUCCESSFUL_LOGIN_TIME);
            outStream.writeLongBlob(lastSuccessfulLoginTime, true);
        }
        if (this.lastFailedLoginTime != -1) {
            outStream.writeByte(ATTR_LAST_FAILED_LOGIN_TIME);
            outStream.writeLongBlob(lastFailedLoginTime, true);
        }
        if (this.numFailedLoginAttempts != -1) {
            outStream.writeByte(ATTR_NUM_FAILED_LOGIN_ATTEMPTS);
            outStream.writeLongBlob(numFailedLoginAttempts, true);
        }
        if (this.smtpLastLoginTime != -1) {
            outStream.writeByte(ATTR_SMTP_LAST_LOGIN_TIME);
            outStream.writeLongBlob(smtpLastLoginTime, true);
        }
        if (this.popLastLoginTime != -1) {
            outStream.writeByte(ATTR_POP_LAST_LOGIN_TIME);
            outStream.writeLongBlob(popLastLoginTime, true);
        }
        if (this.imapLastLoginTime != -1) {
            outStream.writeByte(ATTR_IMAP_LAST_LOGIN_TIME);
            outStream.writeLongBlob(imapLastLoginTime, true);
        }
        if (this.webmailLastLoginTime != -1) {
            outStream.writeByte(ATTR_WEBMAIL_LAST_LOGIN_TIME);
            outStream.writeLongBlob(webmailLastLoginTime, true);
        }
        
    }

    /**
     * @return the lastLoginTime
     */
    public long getLastLoginTime() {
        return lastLoginTime;
    }

    /**
     * @return the lastSuccessfulLoginTime
     */
    public long getLastSuccessfulLoginTime() {
        return lastSuccessfulLoginTime;
    }

    /**
     * @return the lastFailedLoginTime
     */
    public long getLastFailedLoginTime() {
        return lastFailedLoginTime;
    }

    /**
     * @return the numFailedLoginAttempts
     */
    public long getNumFailedLoginAttempts() {
        return numFailedLoginAttempts;
    }

    /**
     * @return the smtpLastLoginTime
     */
    public long getSmtpLastLoginTime() {
        return smtpLastLoginTime;
    }
    
    /**
     * @return the popLastLoginTime
     */
    public long getPopLastLoginTime() {
        return popLastLoginTime;
    }
    
    /**
     * @return the imapLastLoginTime
     */
    public long getImapLastLoginTime() {
        return imapLastLoginTime;
    }
    
    /**
     * @return the webmailLastLoginTime
     */
    public long getWebmailLastLoginTime() {
        return webmailLastLoginTime;
    }
    
    /**
     * for debugging - output all of the mailboxAccessInfo parameters
     * 
     * @param out a PrintStream to dump the data.
     */
    public void dump() {
        System.out.println();
        System.out.println("DUMPING MailboxInfo ");
        System.out.println("lastLoginTime: " + this.getLastLoginTime());
        System.out.println("lastSuccessfulLoginTime: "
                + this.getLastSuccessfulLoginTime());
        System.out.println("lastFailedLoginTime: "
                + this.getLastFailedLoginTime());
        System.out.println("numFailedLoginAttempts: "
                + this.getNumFailedLoginAttempts());
        System.out.println("smtpLastLoginTime: "
                + this.getSmtpLastLoginTime());
        System.out.println("popLastLoginTime: "
                + this.getPopLastLoginTime());
        System.out.println("imapLastLoginTime: "
                + this.getImapLastLoginTime());
        System.out.println("webmailLastLoginTime: "
                + this.getWebmailLastLoginTime());
        
        System.out.println("==== DONE ==== ");
        System.out.println();
    }

    /**
     * @param lastLoginTime the lastLoginTime to set
     */
    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        this.count++;
    }

    /**
     * @param lastSuccessfulLoginTime the lastSuccessfulLoginTime to set
     */
    public void setLastSuccessfulLoginTime(long lastSuccessfulLoginTime) {
        this.lastSuccessfulLoginTime = lastSuccessfulLoginTime;
        this.count++;
    }

    /**
     * @param lastFailedLoginTime the lastFailedLoginTime to set
     */
    public void setLastFailedLoginTime(long lastFailedLoginTime) {
        this.lastFailedLoginTime = lastFailedLoginTime;
        this.count++;
    }

    /**
     * @param numFailedLoginAttempts the numFailedLoginAttempts to set
     */
    public void setNumFailedLoginAttempts(long numFailedLoginAttempts) {
        this.numFailedLoginAttempts = numFailedLoginAttempts;
        this.count++;
    }

    /**
     * @param smtpLastLoginTime the smtpLastLoginTime to set
     */
    public void setSmtpLastLoginTime(long smtpLastLoginTime) {
        this.smtpLastLoginTime = smtpLastLoginTime;
        this.count++;
    }
    
    /**
     * @param popLastLoginTime the popLastLoginTime to set
     */
    public void setPopLastLoginTime(long popLastLoginTime) {
        this.popLastLoginTime = popLastLoginTime;
        this.count++;
    }
    
    /**
     * @param imapLastLoginTime the imapLastLoginTime to set
     */
    public void setImapLastLoginTime(long imapLastLoginTime) {
        this.imapLastLoginTime = imapLastLoginTime;
        this.count++;
    }
    
    /**
     * @param webmailLastLoginTime the webmailLastLoginTime to set
     */
    public void setWebmailLastLoginTime(long webmailLastLoginTime) {
        this.webmailLastLoginTime = webmailLastLoginTime;
        this.count++;
    }

}
