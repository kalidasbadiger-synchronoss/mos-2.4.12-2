/*
 *      Copyright (c) 2003 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/CancelledOperationException.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

/**
 * This exception indicates that a method has returned without
 * completing due to explicit cancellation.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class CancelledOperationException extends RuntimeException {

    /**
     * Create an instance.
     */
    public CancelledOperationException() {
        super();
    }

    /**
     * Create an instance with the specified message.
     *
     * @param message the message
     */
    public CancelledOperationException(String message) {
        super(message);
    }
}
