/*
 *      Copyright 2004 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/AvailabilityFilter.java#1 $
 *
 */

package com.opwvmsg.utils.paf.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.util.CancelledOperationException;
import com.opwvmsg.utils.paf.util.ReadWriteLock;
import com.opwvmsg.utils.paf.util.WriterPreferenceReadWriteLock;


/**
 * <p>This is an implementation of a simple servlet filter for
 * providing a basic availability check for a web application.
 * </p>
 * <p>If the attribute <i>application</i>_unavailable (determined
 * from <code>ServletContext.getServletContextName</code>) exists
 * then the service is considered to be unavailable.  The default 503
 * (Service Unavailable) HTTP status code is returned unless the
 * attribute has a non-empty value in which case the value is
 * interpreted to be a HTTP status code (see RFC 2616 - section 10)
 * that should be returned.
 * </p>
 * <p>For example, if the attribute <b>myapp_unavailable</b> exists
 * in the current context and has a value "404" then the response to
 * all requests will be a HTTP status code of 404 (Not Found).
 * </p>
 * <p>This class should be registered as a listener in <i>web.xml</i>.
 * </p>
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public class AvailabilityFilter implements Filter,
        ServletContextAttributeListener {

    private static final Logger logger =
            Logger.getLogger(AvailabilityFilter.class);

    private FilterConfig config;
    private ServletContext context;

    private static final String UNAVAILABLE = "_unavailable";
    private static Map unavailableEvents = new HashMap();
    private static ReadWriteLock lock = new WriterPreferenceReadWriteLock();
    private static String application;
    private static String attribute;
    private static int httpStatusCode = HttpServletResponse.SC_OK;

    /**
     * Initialize this instance.
     *
     * @param config the configuration
     * @see javax.servlet.Filter
     */
    public void init(FilterConfig config) throws ServletException {
        if (logger.isDebugEnabled()) {
            logger.debug("config: " + config);
        }
        this.config = config;
        context = config.getServletContext();
        application = context.getServletContextName();
        if (application == null || application.length() == 0) {
            application = "default";
        } else {
            application = application.replace('.','_');
        }
        attribute = application + UNAVAILABLE;
        if (logger.isDebugEnabled()) {
            logger.debug("attribute = " + attribute);
        }
        processUnavailableEvents();
    }

    /**
     * @see javax.servlet.Filter
     */
    public void destroy() {
        config = null;
        context = null;
    }

    /**
     * @see javax.servlet.Filter
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        String requestUri = httpRequest.getRequestURI();
        if (logger.isDebugEnabled()) {
            logger.debug("requestUri: " + requestUri
                         + ", context: [" + httpRequest.getContextPath()
                         + "], servlet: [" + httpRequest.getServletPath()
                         + "], pathinfo: [" + httpRequest.getPathInfo()
                         + "]");
        }

        int httpStatus = checkAvailability(httpRequest, httpResponse);
        if (httpStatus != HttpServletResponse.SC_OK) {
            if (logger.isInfoEnabled()) {
                String remote = request.getRemoteAddr();
                logger.info("availability: remoteAddr=" + remote
                        + ",status=" + httpStatus);
            }
            httpResponse.setStatus(httpStatus);
            return;
        }

        chain.doFilter(request, response);
    }

    /**
     * @see javax.servlet.ServletContextAttributeListener
     */
    public void attributeAdded(ServletContextAttributeEvent event) {
        if (logger.isDebugEnabled()) {
            logger.debug("attribute added: " +
                    event.getName() + " = " + event.getValue());
        }
        attributeUpdated(event);
    }

    /**
     * @see javax.servlet.ServletContextAttributeListener
     */
    public void attributeRemoved(ServletContextAttributeEvent event) {
        if (logger.isDebugEnabled()) {
            logger.debug("attribute removed: " + event.getName());
        }
        if (attribute != null && attribute.equals(event.getName())) {
            setAvailability(HttpServletResponse.SC_OK);
        } else if (attribute == null && event.getName().endsWith(UNAVAILABLE)) {
            synchronized (unavailableEvents) {
                if (logger.isDebugEnabled()) {
                    logger.debug("remove event: " + event.getName());
                }
                unavailableEvents.remove(event.getName());
            }
        }
    }

    /**
     * @see javax.servlet.ServletContextAttributeListener
     */
    public void attributeReplaced(ServletContextAttributeEvent event) {
        if (logger.isDebugEnabled()) {
            logger.debug("attribute replaced: " +
                    event.getName() + " = " + event.getValue());
        }
        attributeUpdated(event);
    }

    /**
     * Determines the health of the server and populates response.  At
     * a minimum the return value should be SC_OK.
     *
     * @param httpRequest the HTTP request to check the webapp health
     * @param httpResponse the HTTP response to populate
     * @return int HTTP response status
     */
    public int checkAvailability(HttpServletRequest httpRequest,
                                 HttpServletResponse httpResponse)
            throws ServletException, IOException {
        return getAvailability();
    }

    /**
     * Sets the availability of the current application.
     *
     * @param httpStatusCode if non-zero the application is
     *        considered unavailable and the given value is returned
     *        as the status code
     */
    private static void setAvailability(int httpStatusCode) {
        if (logger.isDebugEnabled()) {
            logger.debug("set HTTP status code: " + httpStatusCode);
        }
        try {
            lock.writeLock().acquire();
            try {
                AvailabilityFilter.httpStatusCode = httpStatusCode;
            } finally {
                lock.writeLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Gets the availability of the current application.
     *
     * @return zero if the application is available and non-zero otherwise
     */
    private static int getAvailability() {
        try {
            lock.readLock().acquire();
            try {
                if (logger.isDebugEnabled()) {
                    logger.debug("get HTTP status code: " + httpStatusCode);
                }
                return httpStatusCode;
            } finally {
                lock.readLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Process the queue of attributes for which notifications were
     * received prior to performing the filter initialization.
     */
    private static void processUnavailableEvents() {
        synchronized (unavailableEvents) {
            if (logger.isDebugEnabled()) {
                logger.debug("processing: " + unavailableEvents.size() +
                        " event" + (unavailableEvents.size() == 1 ? "" : "s"));
            }
            Iterator events = unavailableEvents.keySet().iterator();
            while (events.hasNext()) {
                try {
                    String name = (String)events.next();
                    ServletContextAttributeEvent event =
                            (ServletContextAttributeEvent)
                            unavailableEvents.get(name);
                    if (logger.isDebugEnabled()) {
                        logger.debug("processing: " +
                                name + " = " + event.getValue());
                    }
                    attributeUpdated(event);
                } finally {
                    events.remove();
                }
            }
        }
    }

    /**
     * Handle the given event, either an attribute added or an
     * attributed replaced event..
     */
    private static void attributeUpdated(ServletContextAttributeEvent event) {
        if (attribute != null) {
            String name = event.getName();
            if (attribute.equals(name)) {
                Object value = event.getValue();
                if (value instanceof String) {
                    try {
                        setAvailability(Integer.parseInt((String)value));
                    } catch (NumberFormatException nfe) {
                        logger.warn(name + " = " + value +
                                ": " + nfe.getMessage());
                    }
                } else {
                    logger.warn(name + " = " + value +
                            ": string value required");
                }
            }
        } else if (attribute == null && event.getName().endsWith(UNAVAILABLE)) {
            synchronized (unavailableEvents) {
                if (logger.isDebugEnabled()) {
                    logger.debug("add event: " +
                            event.getName() + " = " + event.getValue());
                }
                unavailableEvents.put(event.getName(), event);
            }
        }
    }
}
