/*
 *      FileManager.java
 *
 *      Copyright 2001 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/io/FileManager.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util.io;

import java.io.File;
import java.io.IOException;

/**
 * A FileManager is responsible for creating files and automatically deleting
 * them later.  At a specified frequency the manager will check each file in
 * its directory to see if it should be deleted.  
 * <p>
 * Files will be deleted when:
 * <dl>
 * <dt>a file exceeds a specified age</dt>
 * <dd>A file's age is determined by how recently the file was used, not how 
 * long the file has existed.  How recently a file was used is calculated 
 * based on the information available to the manager.  This includes the 
 * File's most recent modification time as well as the time of the last call 
 * to getManagedFile.  Note that a file that is read from, but never written 
 * to, will eventually exceed the maximum age.  In this case use 
 * getManagedFile periodically to keep the file around.</dd>
 * <dt>the space used by the manager's files exceeds a threshold</dt>
 * <dd>Once the space exceeds the threshold, files will be deleted, oldest 
 * first, until the total space falls below a certain percentage of the 
 * threshold.  This percentage is known as the thresholdMargin.</dd>
 * </dl>
 * <p>
 * Note that the manager does not guarantee space will be freed upon cleanup 
 * due to the possibility that Files may still be held open.
 *
 * @author Brad Kohn
 * @version $Revision: #1 $ 
 */
public interface FileManager {
    /**
     * Get the maximum age constraint for a managed file.
     *
     * @return            the maximum age constraint for a managed file
     */
    public int getMaxAge();

    /**
     * Get this manager's size threshold.
     *
     * @return            the size threshold
     */
    public int getThreshold();

    /**
     * Get this manager's threshold margin.
     *
     * @return            the threshold margin
     */
    public float getThresholdMargin();
    
    /**
     * Get this manager's cleanup frequency.
     *
     * @return            the cleanup frequency
     */
    public int getFrequency();
    
    /**
     * Get the name of this manager's directory.
     *
     * @return            this manager's directory
     */
    public String getDirectory();

    /**
     * Create a new managed file.  This file will have an automatically
     * generated unique name with a specified prefix and suffix (that must 
     * follow the rules defined by File.createTempFile).
     *
     * @param prefix      The prefix string to be used in generating the 
     *                    file's name.  It must be at least three characters 
     *                    long.
     * @param suffix      The suffix string to be used in generating the 
     *                    file's name.  It may be null, in which case the 
     *                    suffix ".tmp" will be used.
     * @return            the new File
     * @throws IOException if a File could not be created
     */
    public File createManagedFile(String prefix, String suffix) 
        throws IOException;

    /**
     * Get an already existing managed file.  This will also extend the 
     * lifetime of the file by setting its modification time to the current  
     * time.
     *
     * @param name        The name of the managed file
     * @return            The file or <code>null</code> if it does not exist
     */
    public File getManagedFile(String name);

    /**
     * Immediately delete all managed files.
     */
    public void deleteAll();

    /**
     * Immediately delete all managed files starting with the specified 
     * prefix.
     */
    public void deleteAllByPrefix(final String prefix);

    /**
     * Immediately delete all managed files ending with the specified suffix.
     */
    public void deleteAllBySuffix(final String suffix);

    /**
     * Return all managed files.
     */
    public File[] getAll();

    /**
     * Return all managed files starting with the specified prefix.
     */
    public File[] getAllByPrefix(final String prefix);

    /**
     * Return  all managed files ending with the specified suffix.
     */
    public File[] getAllBySuffix(final String suffix);
}
