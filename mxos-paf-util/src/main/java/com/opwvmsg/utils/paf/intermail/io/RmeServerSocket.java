/* 
 *      Copyright 2005 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: 
 *      
 */
package com.opwvmsg.utils.paf.intermail.io;

import java.net.ServerSocket;
import java.net.Socket;

import java.io.IOException;

/**
 * This extension of ServerSocket will generate initialized RmeSocket objects
 * from the accept() method.
 */
public class RmeServerSocket extends ServerSocket {

    /**
     * Initialize the ServerSocket to listen on the specified port. The
     * RmeServerSocket will start listenting when the accept() method is called.
     * 
     * @param port the port to use
     * @throws IOException on any IO error
     */
    public RmeServerSocket(int port) throws IOException {
        super(port);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.net.ServerSocket#accept()
     */
    public synchronized Socket accept() throws IOException {
        RmeSocket mySock = new RmeSocket();
        implAccept(mySock);
        mySock.initServerSession();
        return mySock;
    }
}

