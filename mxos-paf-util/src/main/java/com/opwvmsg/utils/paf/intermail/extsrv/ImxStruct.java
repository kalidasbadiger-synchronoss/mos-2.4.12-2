/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/ImxStruct.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates the generic ImxStruct used in extension server calls.
 * The ImxStruct object is composed of any number of (String key, ImxData value)
 * pairs.
 */
public class ImxStruct extends HashMap {

    /**
     * Construct this ImxStruct with a specific starting capacity
     * 
     * @param initialCapacity the starting capacity of the struct
     */
    public ImxStruct(int initialCapacity) {
        super(initialCapacity, 1);
    }

    /**
     * Constructor that will read an ImxStruct from the stream
     * 
     * @param inStream the RME input stream
     */
    public ImxStruct(RmeInputStream inStream) throws IOException {
        super();
        int structSize = inStream.readInt();
        for (int i = 0; i < structSize; i++) {
            String name = inStream.readString();
            ImxData data = new ImxData(inStream);
            put(name, data);
        }
    }

    /**
     * Overrides the HashMap put method to ensure that only ImxData objects are
     * added.
     * 
     * @see java.util.Collection#add(java.lang.Object)
     */
    public Object put(String key, Object value) {
        if (value instanceof ImxData) {
            super.put(key, value);
            return value;
        }
        throw new ClassCastException();
    }

    /**
     * Puts the integer value into the struct as an ImxData object
     * 
     * @param key the key string used to reference this entry
     * @param value the integer value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, int value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Puts the boolean value into the struct as an ImxData object
     * 
     * @param key the key string used to reference this entry
     * @param value the boolean value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, boolean value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Puts the byte array value into the struct as an ImxData object
     * 
     * @param key the key string used to reference this entry
     * @param value the byte array value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, byte[] value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Puts the unicode String value into the struct as an ImxData object Note
     * that the string will be decoded using the "UTF-8" charset. If this is not
     * desirable, use the byte array constructor.
     * 
     * @param key the key string used to reference this entry
     * @param value the unicode string value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, String value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Puts the ImxArray value into the struct as an ImxData object
     * 
     * @param key the key string used to reference this entry
     * @param value the ImxArray value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, ImxArray value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Puts the ImxStruct value into the struct as an ImxData object
     * 
     * @param key the key string used to reference this entry
     * @param value the ImxStruct value
     * @return the ImxData object placed in the struct
     */
    public Object put(String key, ImxStruct value) {
        return (super.put(key, new ImxData(value)));
    }

    /**
     * Get the value in this struct located by the key as a boolean.
     * 
     * @param key the String key used to reference this value
     * @return boolean value
     * @throws ClassCastException if the data type was not a boolean.
     * @throws NullPointerException if the data was not found
     */
    public boolean getBoolean(String key) {
        return ((ImxData) (super.get(key))).getBoolean();
    }

    /**
     * Get the value in this struct located by the key as an integer.
     * 
     * @param key the String key used to reference this value
     * @return integer value
     * @throws ClassCastException if the data type was not an integer.
     * @throws NullPointerException if the data was not found
     */
    public int getInt(String key) {
        return ((ImxData) (super.get(key))).getInt();
    }

    /**
     * Get the value in this struct located by the key as an ImxArray.
     * 
     * @param key the String key used to reference this value
     * @return ImxArray value
     * @throws ClassCastException if the data type was not an ImxArray.
     * @throws NullPointerException if the data was not found
     */
    public ImxArray getArray(String key) {
        return ((ImxData) (super.get(key))).getArray();
    }

    /**
     * Get the value in this struct located by the key as an ImxStruct.
     * 
     * @param key the String key used to reference this value
     * @return ImxStruct value
     * @throws ClassCastException if the data type was not an ImxStruct.
     * @throws NullPointerException if the data was not found
     */
    public ImxStruct getStruct(String key) {
        return ((ImxData) (super.get(key))).getStruct();
    }

    /**
     * Get the value in this struct located by the key as a byte array.
     * 
     * @param key the String key used to reference this value
     * @return byte array value
     * @throws ClassCastException if the data type was not an ImxString.
     * @throws NullPointerException if the data was not found
     */
    public byte[] getStringBytes(String key) {
        return ((ImxData) (super.get(key))).getImxString();
    }

    /**
     * Get the value in this struct located by the key as a unicode string. Note
     * that the bytes will be encoded using UTF-8. If this is not desirable, use
     * the getStringBytes method.
     * 
     * @param key the String key used to reference this value
     * @return unicode String value
     * @throws ClassCastException if the data type was not an ImxString.
     * @throws NullPointerException if the data was not found
     */
    public String getUtf8String(String key) {
        return ((ImxData) (super.get(key))).getUtf8String();
    }

    /**
     * Write an ImxStruct to the RME output stream
     * 
     * @param outStream the RME output stream
     */
    public void writeImxStruct(RmeOutputStream outStream) throws IOException {
        int structSize = size();
        Set keySet = keySet();
        Iterator iter = keySet.iterator();
        outStream.writeInt(structSize);
        while (iter.hasNext()) {
            String name = (String) iter.next();
            ImxData data = (ImxData) get(name);
            outStream.writeString(name);
            data.writeImxData(outStream);
        }
    }
}