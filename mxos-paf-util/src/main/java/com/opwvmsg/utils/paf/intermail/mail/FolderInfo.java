/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.UUID;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * This class encapsulates all of the FolderInfo data of the ReadMailbox RME
 * 
 * @author mxos-dev
 */
public class FolderInfo {

    private static final Logger logger = Logger.getLogger(FolderInfo.class);

    // FolderInfo Attributes
    private long msgExpirationLowerBound;
    private long msgFirstAccessedBound;
    private long msgArrivalTimeLowerBound;
    private long msgLastAccessedLowerBound;
    private String folderName;
    private String pathName;
    private UUID folderUUID;
    private UUID parentFolderUUID;
    private int folderUidValidity;
    private int folderNextUID;
    private long folderTotMsgs;
    private long folderTotMsgsUnRead;
    private long folderTotBytes;
    private long folderTotBytesUnRead;
    private int folderSubscribed;
    private String folderKeywords;

    // Constants for Folder Info Attribute Types
    public static final byte ATTR_MSG_EXPIRATION_LOWERBOUND_ID = (byte) 0x03;
    public static final byte ATTR_MSG_FIRST_ACCESSED_LOWERBOUND_ID = (byte) 0x04;
    public static final byte ATTR_MSG_ARRIVAL_TIME_LOWERBOUND_ID = (byte) 0x05;
    public static final byte ATTR_MSG_LAST_ACCESSED_LOWERBOUND_ID = (byte) 0x06;
    public static final byte ATTR_FOLDER_SUBSCRIBED = (byte) 0x07;
    public static final byte ATTR_FOLDER_KEYWORDS = (byte) 0x08;
    public static final byte ATTR_FOLDER_NAME_ID = (byte) 0x80;
    public static final byte ATTR_FOLDER_UID_ID = (byte) 0x81;
    public static final byte ATTR_PARENT_FOLDER_UID_ID = (byte) 0x82;
    public static final byte ATTR_FOLDER_UID_VALIDITY_ID = (byte) 0x83;
    public static final byte ATTR_FOLDER_NEXT_UID_ID = (byte) 0x84;
    public static final byte ATTR_FOLDER_TOT_MSGS_ID = (byte) 0x85;
    public static final byte ATTR_FOLDER_TOT_MSGS_UNREAD_ID = (byte) 0x86;
    public static final byte ATTR_FOLDER_TOT_BYTES_ID = (byte) 0x87;
    public static final byte ATTR_FOLDER_TOT_BYTES_UNREAD_ID = (byte) 0x88;

    /**
     * Default Constructor. Reads the input Stream and constructs the Folder
     * Info.
     * 
     * @param inStream
     * @throws IOException
     * @throws IntermailException
     */
    public FolderInfo(RmeInputStream inStream) throws IOException {
        int numOfAttrs = inStream.readInt();
        byte attrType = 0;
        for (int i = 0; i < numOfAttrs; i++) {
            attrType = inStream.readByte();
            switch (attrType) {
            case ATTR_FOLDER_NAME_ID:
                folderName = inStream.readByteString();
                break;
            case ATTR_FOLDER_UID_ID:
                inStream.readInt(); // Folder UUID Length
                folderUUID = inStream.readUUIDBlob();
                break;
            case ATTR_PARENT_FOLDER_UID_ID:
                inStream.readInt();// Parent Folder UUID Length
                parentFolderUUID = inStream.readUUIDBlob();
                break;
            case ATTR_FOLDER_UID_VALIDITY_ID:
                inStream.readInt();// Folder UID Validity Length
                folderUidValidity = inStream.readIntBlob();
                break;
            case ATTR_FOLDER_NEXT_UID_ID:
                inStream.readInt();// Folder Next UID Length
                folderNextUID = inStream.readIntBlob();
                break;
            case ATTR_FOLDER_TOT_MSGS_ID:
                inStream.readInt();// Folder Total Messages Value Length
                folderTotMsgs = inStream.readLongBlob();
                break;
            case ATTR_FOLDER_TOT_MSGS_UNREAD_ID:
                inStream.readInt();// Folder Total Messages Unread Value Length
                folderTotMsgsUnRead = inStream.readLongBlob();
                break;
            case ATTR_FOLDER_TOT_BYTES_ID:
                inStream.readInt();// Folder Total Bytes Value Length
                folderTotBytes = inStream.readLongBlob();
                break;
            case ATTR_FOLDER_TOT_BYTES_UNREAD_ID:
                inStream.readInt();// Folder Total Bytes Unread Value Length
                folderTotBytesUnRead = inStream.readLongBlob();
                break;
            case ATTR_FOLDER_SUBSCRIBED:
                inStream.readInt();// Folder Subscribed length
                folderSubscribed = inStream.readIntBlob();
                break;
            case ATTR_FOLDER_KEYWORDS:
                folderKeywords = inStream.readByteString();
                break;
            case ATTR_MSG_EXPIRATION_LOWERBOUND_ID:
                inStream.readInt();// Message expiration seconds lower bound
                msgExpirationLowerBound = inStream.readLongBlob();
                break;
            case ATTR_MSG_FIRST_ACCESSED_LOWERBOUND_ID:
                inStream.readInt();// Message first accessed time lower bound.
                msgFirstAccessedBound = inStream.readLongBlob();
                break;
            case ATTR_MSG_ARRIVAL_TIME_LOWERBOUND_ID:
                inStream.readInt();// Message arrival time lower bound.
                msgArrivalTimeLowerBound = inStream.readLongBlob();
                break;
            case ATTR_MSG_LAST_ACCESSED_LOWERBOUND_ID:
                inStream.readInt();// Message last access time lower bound
                msgLastAccessedLowerBound = inStream.readLongBlob();
                break;
            default:
                logger.error("Unsupprted Attribute Type " + attrType
                        + " value: " + inStream.readByteString()
                        + "received in the MSS_SL_MAILBOXINFO response.");
            }
        }
    }

    /**
     * @return the folderUUID
     */
    public UUID getFolderUUID() {
        return folderUUID;
    }

    /**
     * @param folderUUID the folderUUID to set
     */
    public void setFolderUUID(UUID folderUUID) {
        this.folderUUID = folderUUID;
    }

    /**
     * @return the parentFolderUUID
     */
    public UUID getParentFolderUUID() {
        return parentFolderUUID;
    }

    /**
     * @param parentFolderUUID the parentFolderUUID to set
     */
    public void setParentFolderUUID(UUID parentFolderUUID) {
        this.parentFolderUUID = parentFolderUUID;
    }

    /**
     * @return the folderUidValidity
     */
    public int getFolderUidValidity() {
        return folderUidValidity;
    }

    /**
     * @param folderUidValidity the folderUidValidity to set
     */
    public void setFolderUidValidity(int folderUidValidity) {
        this.folderUidValidity = folderUidValidity;
    }

    /**
     * @return the folderNextUID
     */
    public int getFolderNextUID() {
        return folderNextUID;
    }

    /**
     * @param folderNextUID the folderNextUID to set
     */
    public void setFolderNextUID(int folderNextUID) {
        this.folderNextUID = folderNextUID;
    }

    /**
     * @return the folderTotMsgs
     */
    public long getFolderTotMsgs() {
        return folderTotMsgs;
    }

    /**
     * @param folderTotMsgs the folderTotMsgs to set
     */
    public void setFolderTotMsgs(int folderTotMsgs) {
        this.folderTotMsgs = folderTotMsgs;
    }

    /**
     * @return the folderTotMsgsUnRead
     */
    public long getFolderTotMsgsUnRead() {
        return folderTotMsgsUnRead;
    }

    /**
     * @param folderTotMsgsUnRead the folderTotMsgsUnRead to set
     */
    public void setFolderTotMsgsUnRead(int folderTotMsgsUnRead) {
        this.folderTotMsgsUnRead = folderTotMsgsUnRead;
    }

    /**
     * @return the folderTotBytes
     */
    public long getFolderTotBytes() {
        return folderTotBytes;
    }

    /**
     * @param folderTotBytes the folderTotBytes to set
     */
    public void setFolderTotBytes(int folderTotBytes) {
        this.folderTotBytes = folderTotBytes;
    }

    /**
     * @return the folderTotBytesUnRead
     */
    public long getFolderTotBytesUnRead() {
        return folderTotBytesUnRead;
    }

    /**
     * @param folderTotBytesUnRead the folderTotBytesUnRead to set
     */
    public void setFolderTotBytesUnRead(int folderTotBytesUnRead) {
        this.folderTotBytesUnRead = folderTotBytesUnRead;
    }

    /**
     * @return the folderName
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     * @param folderName the folderName to set
     */
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    /**
     * for debugging - output all of the FolderInfo parameters
     * 
     * @param out a PrintStream to dump the data.
     */
    public void dump() {
        System.out.println("DUMPING FolderInfo ");
        System.out.println("folderName: " + this.getFolderName());
        if (this.getFolderUUID() != null)
            System.out
                    .println("folderUUID: " + this.getFolderUUID().toString());
        if (this.getParentFolderUUID() != null)
            System.out.println("parentFolderUUID: "
                    + this.getParentFolderUUID().toString());
        System.out.println("folderUidValidity: " + this.getFolderUidValidity());
        System.out.println("folderNextUID: " + this.getFolderNextUID());
        System.out.println("folderTotMsgs: " + this.getFolderTotMsgs());
        System.out.println("folderTotMsgsUnRead: "
                + this.getFolderTotMsgsUnRead());
        System.out.println("folderTotBytes: " + this.getFolderTotBytes());
        System.out.println("folderTotBytesUnRead: "
                + this.getFolderTotBytesUnRead());
        System.out.println("folderSubscribed: " + this.getFolderSubscribed());
        System.out.println("folderKeywords: " + this.getFolderKeywords());
        System.out.println("msgExpirationLowerBound: " + this.getMsgExpirationLowerBound());
        System.out.println("msgFirstAccessedBound: " + this.getMsgFirstAccessedBound());
        System.out.println("msgArrivalTimeLowerBound: " + this.getMsgArrivalTimeLowerBound());
        System.out.println("msgLastAccessedLowerBound: " + this.getMsgLastAccessedLowerBound());
        System.out.println("==== DONE ==== ");
        System.out.println();
    }

    /**
     * @return the pathName
     */
    public String getPathName() {
        return pathName;
    }

    /**
     * @param pathName the pathName to set
     */
    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    /**
     * @return the folderSubscribed
     */
    public int getFolderSubscribed() {
        return folderSubscribed;
    }

    /**
     * @param folderSubscribed the folderSubscribed to set
     */
    public void setFolderSubscribed(int folderSubscribed) {
        this.folderSubscribed = folderSubscribed;
    }

    /**
     * @return the folderKeywords
     */
    public String getFolderKeywords() {
        return folderKeywords;
    }

    /**
     * @param folderKeywords the folderKeywords to set
     */
    public void setFolderKeywords(String folderKeywords) {
        this.folderKeywords = folderKeywords;
    }

    /**
     * @return the msgExpirationLowerBound
     */
    public long getMsgExpirationLowerBound() {
        return msgExpirationLowerBound;
    }

    /**
     * @return the msgFirstAccessedBound
     */
    public long getMsgFirstAccessedBound() {
        return msgFirstAccessedBound;
    }

    /**
     * @return the msgArrivalTimeLowerBound
     */
    public long getMsgArrivalTimeLowerBound() {
        return msgArrivalTimeLowerBound;
    }

    /**
     * @return the msgLastAccessedLowerBound
     */
    public long getMsgLastAccessedLowerBound() {
        return msgLastAccessedLowerBound;
    }

    /**
     * @param msgExpirationLowerBound the msgExpirationLowerBound to set
     */
    public void setMsgExpirationLowerBound(long msgExpirationLowerBound) {
        this.msgExpirationLowerBound = msgExpirationLowerBound;
    }

    /**
     * @param msgFirstAccessedBound the msgFirstAccessedBound to set
     */
    public void setMsgFirstAccessedBound(long msgFirstAccessedBound) {
        this.msgFirstAccessedBound = msgFirstAccessedBound;
    }

    /**
     * @param msgArrivalTimeLowerBound the msgArrivalTimeLowerBound to set
     */
    public void setMsgArrivalTimeLowerBound(long msgArrivalTimeLowerBound) {
        this.msgArrivalTimeLowerBound = msgArrivalTimeLowerBound;
    }

    /**
     * @param msgLastAccessedLowerBound the msgLastAccessedLowerBound to set
     */
    public void setMsgLastAccessedLowerBound(long msgLastAccessedLowerBound) {
        this.msgLastAccessedLowerBound = msgLastAccessedLowerBound;
    }

}
