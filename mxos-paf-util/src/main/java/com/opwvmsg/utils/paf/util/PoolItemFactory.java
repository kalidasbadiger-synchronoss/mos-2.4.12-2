/*
 * Copyright (c) 2000-2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/PoolItemFactory.java#1 $
 */

package com.opwvmsg.utils.paf.util;

/**
 * An object that implements this interface can be used to create
 * items for a <code>Pool</code>.
 * <p>
 * This class was originally part of net.mobility.util
 *
 * @author Forrest Girouard
 */
public interface PoolItemFactory {

    /**
     * Create a new item for the pool.
     *
     * @return Object a new element.
     */
    public Object create();

}
