/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/MoveMsgs.java#1 $ 
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;

/**
 * This operation will move a set of messages from one folder to another. This
 * operation is a subset of the CopyMsgs operation. Calling CopyMsgs with the
 * option to delete the original messages is equivalent to this operation.
 */
public class MoveMsgs extends CopyMsgs {

    // constants
    public static final int MOVE_MSGS_FOLDER_IS_HINT = 0x01;
    public static final int MOVE_MSGS_MULTIPLE_OK = 0x02;
    public static final int MOVE_MSGS_GET_DEST_FOLDER = 0x04;
    public static final int MOVE_MSGS_DELETE_ORIGINALS = 0x08;
    public static final int MOVE_MSGS_SUPPRESS_MERS = 0x10;
    
    //Only for leapord RME model
    public static final int MOVE_MSGS_DISABLE_NOTIFICATION = 0x1000;

    /**
     * Constructor for MoveMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The name of the folder to copy from
     * @param destPathname The name of the folder to copy to
     * @param uids an array of integer UIDs to copy
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            MOVE_MSGS_FOLDER_IS_HINT - message(s) may not be in specified
     *            src folder <br>
     *            MOVE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            MOVE_MSGS_GET_DEST_FOLDER - load and return the destination
     *            folder with the reply <br>
     *            MOVE_MSGS_DELETE_ORIGINALS - after copy, delete original
     *            messages (This option is always true for this op) <br>
     *            MOVE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     */
    public MoveMsgs(String host, String name, String fromPathname,
            String destPathname, int[] uids, int accessId, int options) {
        super(host, name, fromPathname, destPathname, uids, accessId, options);
        // Set option to indicate this is to move messages
        this.options |= MOVE_MSGS_DELETE_ORIGINALS;
    }

    /**
     * Constructor for MoveMsgs
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param fromPathname The name of the folder to copy from
     * @param destPathname The name of the folder to copy to
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options options for this operation. possible bits are: <br>
     *            MOVE_MSGS_FOLDER_IS_HINT - message(s) may not be in specified
     *            src folder <br>
     *            MOVE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            MOVE_MSGS_GET_DEST_FOLDER - load and return the destination
     *            folder with the reply <br>
     *            MOVE_MSGS_DELETE_ORIGINALS - after copy, delete original
     *            messages (This option is always true for this op) <br>
     *            MOVE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation
     * @param messageIds the list of messageIds to delete
     */
    public MoveMsgs(String host, String name, String fromPathname,
            String destPathname, int accessId, int options, String[] messageIds) {
        super(host, name, fromPathname, destPathname, accessId, options,
                messageIds);
        // Set option to indicate this is to move messages
        this.options |= MOVE_MSGS_DELETE_ORIGINALS;
    }

    /**
     * Constructor for MoveMsgs for Stateless RME
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param messageMetadatas array of MessageMetadata to be moved
     * @param srcFolderUUID Source folder UUID
     * @param destFolderUUID Destination folder UUID
     * @param options options for this operation. possible bits are: <br>
     *            COPY_MSGS_DISABLE_NOTIFICATION - diable Notifications <br>     * 
     */
    public MoveMsgs(String host, String name, String realm,
            MessageMetadata[] messageMetadatas, UUID srcFolderUUID,
            UUID destFolderUUID, int options) {
        super(host, name, realm, messageMetadatas, srcFolderUUID,
                destFolderUUID, true, options);
    }
}