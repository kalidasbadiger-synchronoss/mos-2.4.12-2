/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/replystore/ReplyMode.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.replystore;

/**
 * A representation for the different types of autoreply modes.
 */
public class ReplyMode {
    private int mode;
    
    public final static ReplyMode NO_CHANGE = new ReplyMode(0);
    public final static ReplyMode NONE = new ReplyMode('N');
    public final static ReplyMode AUTO = new ReplyMode('R');
    public final static ReplyMode ECHO = new ReplyMode('E');
    public final static ReplyMode VACATION = new ReplyMode('V');

    /**
     * Constructs a new ReplyMode with the the specified mode.
     * 
     * @param mode the autoreply mde
     */
    private ReplyMode(int mode){
        this.mode = mode;
    }
    
    /**
     * Gets the int value of this mode.
     * 
     * @return integer mode value
     */
    public int getMode() {
        return mode;
    }
    
    /**
     * Converts a char to a ReplyMode.
     * 
     * @param mode the reply mode character (N,R,E,V)
     * @return the ReplyMode, or null if it is an invalid character.
     */
    public static ReplyMode getReplyMode(char mode) {
        if (mode == 'N' || mode == 'n') {
            return NONE;
        } else if (mode == 'R' || mode =='r') {
            return AUTO;
        } else if (mode == 'E' || mode == 'e') {
            return ECHO;
        } else if (mode == 'V' || mode == 'v') {
            return VACATION;
        } else if (mode == 0) {
            return NO_CHANGE;
        }
        return null;       
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    public String toString() {
        if (mode == 0) {
            return "0";
        }
        char[] modeChars = new char[1];
        modeChars[0] = (char)mode;
        return new String(modeChars);
    }

}
