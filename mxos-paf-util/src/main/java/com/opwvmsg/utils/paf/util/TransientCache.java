/*
 * Copyright (c) 2001-2004 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/TransientCache.java#1 $
 */

package com.opwvmsg.utils.paf.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * This class maintains a short-lived cache.
 * <p>
 * Note that even though this class extends Hashtable, it is not guaranteed to
 * do so in the future.  It will, however, always implement Map.
 * <p>
 * This class was originally part of net.mobility.util
 *
 * @author Forrest Girouard
 */
public class TransientCache extends Hashtable implements Cache, Map {

    /**
     * Default time out in milliseconds.
     */
    public static final long DEFAULT_TIMEOUT = 120 * 1000;

    /**
     * Minimum time in milliseconds between purges of stale values.
     */
    public static final long MIN_PURGE_INTERVAL = 10 * 1000;

    /**
     * Duration in milliseconds after which an entry in the cache
     * is considered stale.
     */
    private long timeout;

    /**
     * If true, the timeout is considered an idle timeout, i.e.
     * the cache entries only expire if they've not been retrieved
     * in the timeout period.
     */
    private boolean useIdleTimeout = false;

    /**
     * Duration in milliseconds after which stale entries are purged.
     */
    private long purgeInterval;

    /**
     * A queue of keys inserted in expiration order.
     */
    private LinkedList queue = new LinkedList();

    /**
     * The clock used for determining when entries are stale.
     */
    private static Clock clock = LazyClock.getInstance();

    /**
     * Initialize for the given timeout.  Also, start the daemon
     * thread that purges stale entries.
     *
     * @param timeout duration in milliseconds after which entries in
     *          the cache are considered stale
     */
    private void init(long timeout, boolean useIdleTimeout) {
        this.useIdleTimeout = useIdleTimeout;
        setTimeout(timeout);
        startReaper();
    }

    /**
     * Constructs a new, empty transient cache using whatever valid
     * parameters are given in the <code>Map</code> using defaults
     * for whatever isn't specified.
     *
     * The default time out is two minutes and the default load
     * factor is <code>0.75</code>.
     *
     * Starts a thread that purges stale entries.
     *
     * @param parameters a <code>Map</code> that contains parameter
     *        entries where both the name and value are
     *        <code>String</code> instances.  The parameter names are
     *        identical to the names of the parameters available with
     *        the other constructors.  The <code>String</code> values
     *        are converted to the appropriate types before being used.
     * @return an instance of a <code>TransientCache</code>
     *        constructed using the given parameters
     */
    public static Cache getInstance(Map parameters) {
        long timeout = DEFAULT_TIMEOUT;
        int initialCapacity = 11;
        float loadFactor = (float)0.75;
        boolean useIdleTimeout = false;
        if (parameters.get("timeout") != null) {
            timeout = Long.parseLong(
                    (String)parameters.get("timeout"));
        }
        if (parameters.get("initialCapacity") != null) {
            initialCapacity = Integer.parseInt(
                    (String)parameters.get("initialCapacity"));
        }
        if (parameters.get("loadFactor") != null) {
            loadFactor = Float.parseFloat(
                    (String)parameters.get("loadFactor"));
        }
        if (parameters.get("useIdleTimeout") != null) {
            useIdleTimeout = Boolean.valueOf(
                    (String)parameters.get("useIdleTimeout")).booleanValue();
        }
        return new TransientCache(timeout, initialCapacity, loadFactor, useIdleTimeout);
    }

    /**
     * Constructs a new, empty transient cache with a default time
     * out, capacity,  and load factor.  The default time out is two
     * minutes and the default load factor is <code>0.75</code>.
     *
     * Starts a thread that purges stale entries.
     */
    public TransientCache() {
        init(DEFAULT_TIMEOUT, false);
    }

    /**
     * Constructs a new, empty transient cache with the specified
     * time out and a default capacity and load factor.  The default
     * load factor is <code>0.75</code>.
     *
     * Starts a thread that purges stale entries.
     *
     * @param timeout time in milliseconds for after which
     *          entries are expired
     */
    public TransientCache(long timeout) {
        init(timeout, false);
    }

    /**
     * Constructs a new, empty transient cache with the specified
     * time out and a default capacity and load factor.  The default
     * load factor is <code>0.75</code>.
     *
     * Starts a thread that purges stale entries.
     *
     * @param timeout time in milliseconds for after which
     *          entries are expired
     */
    public TransientCache(long timeout, boolean idleTimeout) {
        init(timeout, idleTimeout);
    }

    /**
     * Constructs a new, empty transient cache with the specified
     * time out and initial capacity and the default load factor,
     * which is <code>0.75</code>.
     *
     * Starts a thread that purges stale entries.
     *
     * @param timeout time in milliseconds for after which
     *          entries are expired
     * @param initialCapacity the initial capacity of the cache
     */
    public TransientCache(long timeout, int initialCapacity) {
        super(initialCapacity);
        init(timeout, false);
    }

    /**
     * Constructs a new, empty transient cache with the specified
     * time out, initial capacity and load factor.
     *
     * Starts a thread that purges stale entries.
     *
     * @param timeout time in milliseconds for after which
     *          entries are expired
     * @param initialCapacity the initial capacity of the cache
     * @param loadFactor the load factor of the cache
     */
    public TransientCache(long timeout, int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        init(timeout, false);
    }

    /**
     * Constructs a new, empty transient cache with the specified
     * time out, initial capacity and load factor.
     *
     * Starts a thread that purges stale entries.
     *
     * @param timeout time in milliseconds for after which
     *          entries are expired
     * @param initialCapacity the initial capacity of the cache
     * @param loadFactor the load factor of the cache
     * @param useIdleTimeout true if the timeout should be an idle timeout
     */
    public TransientCache(long timeout, int initialCapacity, float loadFactor,
                          boolean useIdleTimeout) {
        super(initialCapacity, loadFactor);
        init(timeout, useIdleTimeout);
    }
    
    /**
     * Create and start the thread to remove stale entries.
     */
    private void startReaper() {
        Thread reaper = new Thread("TransientCache") {
            public void run() {
                while (!Thread.interrupted()) {
                    try {
                        Thread.sleep(getPurgeInterval());
                        purge();
                    } catch (InterruptedException e) {
                        return;
                    }
                }
            }
        };
        reaper.setDaemon(true);
        reaper.start();
    }

    /**
     * Purges all stale entries from the cache.
     */
    public synchronized void purge() {
        try {
            long time = clock.currentTimeMillis();
            Object key = queue.removeFirst();
            while (isStale(key, time)) {
                key = queue.removeFirst();
            }
            queue.addFirst(key); // wasn't stale so put key back into queue
        } catch (NoSuchElementException e) {
        }
    }

    /**
     * Determines if the entry to which the specified key is mapped
     * is stale using the given current time.
     */
    private synchronized boolean isStale(Object key, long time) {
        boolean stale = true;
        Entry entry = (Entry)super.get(key);
        if (entry != null) {
            if (time - entry.getTime() < timeout) {
                stale = false;
            } else {
                super.remove(key);
                entry.release();
            }
        }
        return stale;
    }

    /**
     * Sets the timeout for stale entries.
     */
    public synchronized void setTimeout(long timeout) {
        this.timeout = timeout;
        setPurgeInterval(timeout * 2);
    }

    /**
     * Gets the timeout for stale entries.
     */
    public synchronized long getTimeout() {
        return timeout;
    }

    /**
     * Sets the timeout state idle or absolute.
     */
    public synchronized void setIdleTimeout(boolean useIdleTimeout) {
        this.useIdleTimeout = useIdleTimeout;
    }

    /**
     * Gets the state of the useIdleTimeout setting
     */
    public synchronized boolean getIdleTimeout() {
        return useIdleTimeout;
    }

    /**
     * Sets the interval for purging stale entries.
     */
    public synchronized void setPurgeInterval(long interval) {
        purgeInterval = Math.max(interval, MIN_PURGE_INTERVAL);
    }

    /**
     * Gets the interval for purging stale entries.
     */
    public synchronized long getPurgeInterval() {
        return purgeInterval;
    }

    /**
     * Interface for cache values to implement when they need to do
     * cleanup before being purged.
     */
    public interface Releasable {
        public void release();
    }

    /**
     * Container to hold an entry in the <code>TransientCache</code>.
     */
    private static class Entry implements Releasable {

        /**
         * Entry value.
         */
        private Object value;

        /**
         * Creation time in milliseconds.
         */
        private long time;

        /**
         * Cache for this instance.
         */
        private TransientCache cache;

        /**
         * Constructs a timed entry.
         */
        private Entry(Object value) {
            setValue(value);
        }

        /**
         * Constructs a timed entry.
         */
        private Entry(Object value, TransientCache cache) {
            this(value);
            setCache(cache);
        }
        
        /**
         * Release the contents of this entry and return it to the
         * cache.
         */
        public void release() {
            if (value != null && value instanceof Releasable) {
                ((Releasable)value).release();
            }
            setValue(null);
            if (cache != null) {
                cache.releaseEntry(this);
            }
        }

        /**
         * Get the entry creation time.
         */
        public long getTime() {
            return time;
        }
    
        /**
         * Get the entry value.
         */
        public Object getValue() {
            return value;
        }

        /**
         * Set the value reseting the time when the value is non-null.
         */
        private Entry setValue(Object value) {
            this.value = value;
            if (value != null) {
                setTime();
            } else {
                time = 0L;
            }
            return this;
        }

        /**
         * Set the creation time.
         */
        private Entry setTime() {
            time = clock.currentTimeMillis();
            return this;
        }

        /**
         * Set the cache to which this instance should be released.
         */
        private void setCache(TransientCache cache) {
            this.cache = cache;
        }

    } // end class TransientCache.Entry

    /**
     * Pool of available <code>Entry</code> instances.
     */
    private ArrayList entries = new ArrayList();

    /**
     * Return an instance of <code>Entry</code> for the given
     * value.
     */
    private synchronized Entry createEntry(Object value) {
            Entry entry = null;
            if (entries != null) {
                int size = entries.size();
                if (size > 0) {
                    entry = (Entry)entries.get(size - 1);
                    entries.remove(size - 1);
                    entry.setValue(value);
                    entry.setCache(this);
                }
            }
            if (entry == null) {
                entry = new Entry(value, this);
            }
            return entry;
    }

    private synchronized void releaseEntry(Entry entry) {
        entries.add(entry);
    }

    /**
     * Returns the value to which the specified key is mapped in
     * the cache.
     */
    public synchronized Object get(Object key) {
        Entry entry = (Entry)super.get(key);
        if (entry != null) {
            if (clock.currentTimeMillis() - entry.getTime() < timeout) {
                if (useIdleTimeout) {
                    entry.setTime();
                }
                return entry.getValue();
            } else {
                super.remove(key);
                queue.remove(key);
                entry.release();
            }
        }
        return null;
    }

    /**
     * Maps the specified key to the specified value in the
     * cache.  Neither the key nor the value can be <code>null</code>.
     *
     * The value can be retrieved by calling the <code>get</code>
     * metheod with a key that is equal to the original key.
     */
    public synchronized Object put(Object key, Object value) {
        Entry entry = createEntry(value);
        Entry oldEntry = (Entry)super.put(key, entry);
        queue.remove(key);
        queue.add(key);
        if (oldEntry != null) {
            Object oldValue = null;
            if (entry.getTime() - oldEntry.getTime() < timeout) {
                oldValue = oldEntry.getValue();
            }
            oldEntry.release();
            return oldValue;
        }
        return null;
    }

    /**
     * Removes the key (and its corresponding value) from the cache.
     *
     * @param key an <code>Object</code> which specifies which entry
     *          to remove from the cache.
     * @return the entry <code>Object</code> which was removed
     *          (the value that matched the key, if it was in the cache.)
     *          if there was no value that matched the key, no attempt
     *          to remove the entry is made, and the return value will
     *          be <code>null</code>
     */
    public synchronized Object remove(Object key) {
        Entry entry = (Entry)super.remove(key);
        if (entry != null) {
            queue.remove(key);
            Object value = null;
            if (clock.currentTimeMillis() - entry.getTime() < timeout) {
                value = entry.getValue();
            }
            entry.release();
            return value;
        }
        return null;
    }

    public synchronized Map getMap() {
        return Collections.unmodifiableMap(this);
    }
} // end class TransientCache
