/*      
 *      Copyright 2005 Openwave Systmes, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systgems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: 
 */
package com.opwvmsg.utils.paf.intermail.config.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.config.AbstractConfigOperation;
import com.opwvmsg.utils.paf.intermail.config.Impact;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * This operation will install new or updated config keys to the master
 * config.db. Optionally, this operation can ask for the config key impacts
 * instead of actually installing the keys.
 */
public class InstallKeys extends AbstractConfigOperation {

    private Impact[] impacts = null;
    private boolean assess, broadcast, incremental;
    private String[] keyChanges;

    /**
     * This constructs the InstallKeys operation parameters
     * 
     * @param assess if true, only assess the config key changes, if false,
     *            install the keys.
     * @param broadcast if true, will broadcast the assess/change request to all servers.
     *                  if false, it will only contact the conf server  
     * @param keyChanges the key changes to make.  Each entry in this array is expected to be
     *       a String returned by either makeEqualsKey , makeRemoveKey,
     *       makePortAssignKey or makePortRangeKey
     * @param incremental if true, the key changes are incremental; if false,
     *            the keyChanges is assumed to represent the entire config.db
     */
    public InstallKeys(boolean assess, boolean broadcast, String[] keyChanges,
            boolean incremental) {
        this.assess = assess;
        this.broadcast = broadcast;
        this.keyChanges = keyChanges;
        this.incremental = incremental;
    }

    /**
     * Returns a string used by InstallKeys to specify that the
     * key should be set to the specified value.
     * 
     * @param key the key to be changed/added.
     * @param value the value for the key
     * @return a String suitable for passing to InstallKeys
     */
    public static String makeSetKey(String key, String value) {
        return key + "=" + value;
    }

    /**
     * Returns a string used by InstallKeys to specify that the
     * key should be removed.
     * 
     * @param key the key to be removed.
     * @return a String suitable for passing to InstallKeys
     */
    public static String makeRemoveKey(String key) {
        return key;
    }

    /**
     * Returns a string used by InstallKeys to specify that the
     * key should be set to a certain range of ports (port checking
     * against other keys will apply).
     * 
     * @param key the key to be changed/added.
     * @param port the basePort number
     * @param numPorts the number of ports in the range
     * @return a String suitable for passing to InstallKeys
     */
    public static String makePortRangeKey(String key, int port, int numPorts) {
        return key + "?" + port + ":" + numPorts;
    }
    
    /**
     * Returns a string used by InstallKeys to specify that the
     * key should be set to a certain port (port checking
     * against other keys will apply).
     * 
     * @param key the key to be changed/added.
     * @param port the port number
     * @return a String suitable for passing to InstallKeys
     */
    public static String makePortAssignKey(String key, int port) {
        return key + "?" + port;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        callRme(INSTALLKEYS, RME_OMIT_SENDING_CLIENT_INDEX, false);
    }

    /**
     * returns the list of Impacts that the proposed config key changes will
     * have on other servers. 
     * 
     * @return array of Impacts
     */
    public Impact[] getImpacts() {
        return impacts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeString(defaultHost);
        outStream.writeString(defaultApplication);
        outStream.writeBoolean(assess);
        outStream.writeBoolean(broadcast);
        outStream.writeStringArray(keyChanges);
        outStream.writeBoolean(incremental);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {

        logEvents = inStream.readLogEvent();

        int numImpacts = inStream.readInt();
        impacts = new Impact[numImpacts];
        for (int i = 0; i < numImpacts; i++) {
            boolean haveIt = inStream.readBoolean();
            if (haveIt) {
                impacts[i] = new Impact(inStream);
            }
        }
    }

	@Override
	protected void constructHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}


}

