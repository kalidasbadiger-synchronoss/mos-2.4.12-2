/*  H+ 
 *      Copyright 2004 Openwave Systems, Inc. All Rights Reserved.
 * 
 *      The copyright to the computer software herein is the property of 
 *      Openwave Systems, Inc. The software may be used and/or copied only 
 *      with the written permission of Openwave Systems, Inc. or in accordance 
 *      with the terms and conditions stipulated in the agreement/contract under 
 *      which the software has been supplied.
 * 
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/log/LogEvent.java#1 $
 *  H- */
package com.opwvmsg.utils.paf.intermail.log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;


import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class represents an Intermail Log Event. It can be returned from an RME
 * call, or generated from within this code. Each log event has a severity, and
 * a message id. This message id is a lookup into a table of known log events. A
 * message may have arguments used to format a generic log message for a
 * specific event.
 */
public class LogEvent {

    private static final Logger logger =
            Logger.getLogger(com.opwvmsg.utils.paf.intermail.log.LogEvent.class);

    private int severity; // (unsigned char on other side, though)
    private int msgId; // full message id
    private int msgType; // the message set
    private int msgNum; // the message number
    private String[] args;
    private String formattedString;

    // constants for severities
    public static final byte NOTIFICATION = 1;
    public static final byte WARNING = 2;
    public static final byte ERROR = 4;
    public static final byte URGENT = 8;
    public static final byte FATAL = 16;

    private static final String MSGCAT_PROPERTIES = "msgcatProperties";
    private static final String DEFAULT_MSGCAT_PROPERTIES = "msgcat.properties";
    private static Map errorsByName = new HashMap();
    private static Map detailsByError = new HashMap();

    static {
        Properties msgcat = new Properties();
        InputStream msgcatStream = null;

        // load properties file containing the message catalog
        try {
            Config config = Config.getInstance();
            msgcatStream = config.getClass().getClassLoader().
                    getResourceAsStream(
                    "com/opwvmsg/utils/paf/intermail/log/" + 
                    DEFAULT_MSGCAT_PROPERTIES);
            msgcat.load(msgcatStream);
        } catch (IOException ioe) {
            logger.debug(MSGCAT_PROPERTIES + ": " + ioe.getMessage());
        } catch (ConfigException ce) {
            logger.debug(MSGCAT_PROPERTIES + ": " + ce.getMessage());
        } finally {
            if (msgcatStream != null) {
                try {
                    msgcatStream.close();
                } catch (IOException ioe) {
                }
            }
        }

        // load message catalog: setName.mnemonic = msgId format
        Enumeration names = msgcat.propertyNames();
        while (names.hasMoreElements()) {
            String name = (String)names.nextElement();
            String value = msgcat.getProperty(name); 
            int index = -1;

            // parse property name to get category and mnemonic (dot separated)
            String category = null;
            String mnemonic = null;
            index = name.indexOf('.');
            if (index != -1) {
                category = name.substring(0, index);
                mnemonic = name.substring(++index);
            } else {
                logger.warn(name + ": malfored property name");
            }

            // parse property value to get msgId and message (space separated)
            Integer error = null;
            String message = null;
            index = value.indexOf(' ');
            if (index != -1) {
                try {
                    error = Integer.valueOf(value.substring(0, index), 16);
                } catch (NumberFormatException nfe) {
                    logger.warn(name + ": invalid message id: " + value);
                }
                message = value.substring(++index);
            } else {
                logger.warn(name + ": malfored property value: " + value);
            }

            if (category != null && mnemonic != null &&
                    error != null && message != null) {
                errorsByName.put(name, error);
                detailsByError.put(error,
                        new String[] { category, mnemonic, message });
            }
        }
    }

    private static final String[] DEFAULT_DETAILS = new String[] {
        "Unknown", "Unknown", "Unknown msg type: (%d/%d)"
    };

    private static final int CATEGORY = 0;
    private static final int MNEMONIC = 1;
    private static final int MESSAGE  = 2;

    private static final int DETAILS_SIZE = 3;

    private static String[] getDetails(int error) {
        return getDetails(new Integer(error));
    }

    private static String[] getDetails(Integer error) {
        String[] details = (String[])detailsByError.get(error);
        if (details == null || details.length != DETAILS_SIZE) {
            details = DEFAULT_DETAILS;
        }
        return details;
    }

    private static String getDetail(Integer error, int detail) {
        return getDetails(error)[detail];
    }

    private static String getCategory(Integer error) {
        return getDetail(error, CATEGORY);
    }

    private static String getMnemonic(Integer error) {
        return getDetail(error, MNEMONIC);
    }

    private static String  getMessage(Integer error) {
        return getDetail(error, MESSAGE );
    }

    private static String getErrorName(Integer error) {
        return getDetails(error)[CATEGORY] + "." + getDetails(error)[MNEMONIC];
    }

    /**
     * Gets the error message Mnemonic from the unique id.
     *
     * @param errorName a unique error message identifier
     * @return the specific error, <i>category</i>.<i>mnemonic</i>, or null 
     */
    public static String getErrorName(int error) {
        return getErrorName(new Integer(error));
    }

    /**
     * Gets a unique error message identifier.
     *
     * @param errorName the specific error, <i>category</i>.<i>mnemonic</i>
     * @return a unique error message identifier or -1 if no such error exists
     */
    public static int getError(String errorName) {
        Integer error = (Integer)errorsByName.get(errorName);
        if (error == null) {
            return -1;
        }
        return error.intValue();
    }

    /**
     * This constructor allows another log class in this package to create a log
     * event object with the appropriate data
     * 
     * @param sev log severity
     * @param msgId the log's full msgid
     * @param args any arguments.
     */
    public LogEvent(byte sev, String errorName, String[] args) {
        msgId = getError(errorName);
        msgType = msgId >> 16;
        msgNum = msgId & 0xFFFF;
        severity = sev;
        this.args = args;
        formattedString = formatLogString(errorName, args);
    }

    /**
     * Allows a generic log message to be created that is not one of the
     * standard log events.
     * 
     * @param sev the log severity
     * @param msg the message
     */
    public LogEvent(byte sev, String msg) {
        formattedString = msg;
        severity = sev;
    }

    /**
     * This constructor will read in a LogEvent from an RME input stream
     * 
     * @param inStream the stream to read from
     * @throws IOException on any io error.
     */
    public LogEvent(RmeInputStream inStream) throws IOException {
        // get log event from stream
        msgId = inStream.readInt();
        msgType = msgId >> 16;
        msgNum = msgId & 0xFFFF;

        severity = inStream.read();

        // get args and parse
        args = readArgs(inStream);

        // lookup log event, and fmt string
        formattedString = formatLogString(getErrorName(msgId), args);
    }

    /**
     * Send the LogEvent structure over the RME output stream.
     * 
     * @param outStream the stream used to write the data
     */
    public void send(RmeOutputStream outStream) throws IOException {
        outStream.writeInt(msgId);
        outStream.write(severity);
        writeArgs(outStream);
    }

    /**
     * Get the severity for this log event
     * 
     * @return severity
     */
    public int getSeverity() {
        return severity;
    }

    /**
     * Translates the intermail log severity of this event to a log4j log Level
     * 
     * @return Level for log4j
     */
    public Level getLog4jLevel() {
        return getLog4jLevel(severity);
    }

    /**
     * Translates a generic intermail log severity of to a log4j log Level
     * 
     * @param sev the intermail log severity
     * @return the corresponding log4j Level
     */
    public static Level getLog4jLevel(int sev) {
        switch (sev) {
        case (FATAL):
            return Level.FATAL;
        case (URGENT):
            return Level.ERROR;
        case (ERROR):
            return Level.ERROR;
        case (WARNING):
            return Level.WARN;
        case (NOTIFICATION):
            return Level.INFO;
        default:
            return Level.ERROR;
        }
    }

    /**
     * Get the full message id for this log event
     * 
     * @return message ID
     */
    public int getMsgId() {
        return msgId;
    }

    /**
     * Get the message type (which set) that the message belongs in
     * 
     * @return message type
     */
    public int getMsgType() {
        return msgType;
    }

    /**
     * Get the particular message number relative to the set
     * 
     * @return message number
     */
    public int getMsgNum() {
        return msgNum;
    }

    /**
     * Get a particular argument number, or null if the index is not in bounds
     * 
     * @param argNum the argument number to retrieve.
     * @return String argument or null.
     */
    public String getArg(int argNum) {
        if (argNum < 0 || argNum > args.length) {
            return null;
        }
        return args[argNum];
    }

    /**
     * Get the formatted log string. This string combines the generic message
     * from the catalog with the arguments to produce a string suitable for
     * logging.
     * 
     * @return formatted string
     */
    public String getFormattedString() {
        return formattedString;
    }

    /**
     * Throw an IntermailException based on this LogEvent
     * 
     * @throws IntermailException the exception requested
     */
    public IntermailException getIntermailException() {
        return new IntermailException(severity, formattedString);
    }

    /**
     * Log an event based on the passed in parameters
     * 
     * @param sev the log severity
     * @param message the formatted mesage string to log
     */

    private static Hashtable formats = new Hashtable(128);

    /**
     * Retrieves the generic log message based on the message ID, and returns
     * the formatted string based on the args passed in.
     * 
     * @param msgId the full message id
     * @param args any args needed to format the message
     * @return the formatted string
     */
    public static String formatLogString(String errorName, String[] args) {
        // log strings not imported yet
        int msgId = getError(errorName);
        int msgType = msgId >> 16;
        int msgNum = msgId & 0xFFFF;
        Integer error = new Integer(msgId);

        String constPart = getCategory(error) + getMnemonic(error)
                + "(" + msgType + "/" + msgNum + ") ";
        String localMsg = getMessage(error);
        MessageFormat format = (MessageFormat)formats.get(localMsg);
        if (format == null) {
            format = new MessageFormat(localMsg);
            formats.put(localMsg, format);
        }
        return constPart + format.format(args);
    }

    /**
     * Internal function to read the arguments from an RME stream
     * 
     * @param inStream the stream to read from
     * @return a string array of arguments
     * @throws IOException on any socket error
     */
    private String[] readArgs(RmeInputStream inStream) throws IOException {
        int[] argsIndices = inStream.readIntArray();
        int numStrings = inStream.readInt();
        byte[] stringBytes = inStream.readStringBytes();

        if (argsIndices.length != numStrings) {
            throw new IOException("Bad Log Event RME data");
        }

        // only a few of the args matter
        int numArgs = inStream.readInt();

        String[] args = new String[numArgs];
        if (numArgs > 0) {
            int argNum = 0;
            int start = 0;
            for (int i = 0; i < stringBytes.length; i++) {
                if (stringBytes[i] == 0) {
                    try {
                        args[argNum++] = new String(stringBytes, start, i
                                - start, "us-ascii");
                    } catch (UnsupportedEncodingException ue) {
                        args[argNum++] = "";
                    }
                    start = i + 1;
                    if (argNum == numArgs) {
                        break;
                    }
                }
            }
        }
        return args;
    }

    /**
     * Write this LogEvent's arguments to the RME stream
     * 
     * @param outStream the output stream used for writing
     * @throws IOException on any socket error
     */
    private void writeArgs(RmeOutputStream outStream) throws IOException {
        int[] argsIndices = new int[args.length];
        StringBuffer stringBuf = new StringBuffer(20);
        int currentIndex = 0;
        for (int i = 0; i < args.length; i++) {
            argsIndices[i] = currentIndex;
            stringBuf.append(args[i]);
            stringBuf.append(0);
            currentIndex = stringBuf.length();
        }
        outStream.writeIntArray(argsIndices);
        outStream.writeInt(args.length);
        byte[] bytes = stringBuf.toString().getBytes();
        outStream.write(bytes, 0, bytes.length);
        outStream.writeInt(args.length);
    }
}

