/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 */
package com.opwvmsg.utils.paf.intermail.dir;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.BasicAttributes;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * The Account class holds all of the account information returned from the
 * directory including user attributes, cos attributes, constraints, as well as
 * basic account info like smtp address and mailbox id.
 */
public class Account {

    public final static int ROLE_END_USER = 10;
    public final static int ROLE_GROUP_ADMIN = 100;
    public final static int ROLE_ORGUNIT_ADMIN = 300;
    public final static int ROLE_ORG_ADMIN = 400;
    public final static int ROLE_SUPERORG_ADMIN = 500;
    public final static int ROLE_CSR = 600;
    public final static int ROLE_SUPERCSR = 700;
    public final static int ROLE_PROVIDER_ADMIN = 800;
    public final static int ROLE_SUPERPROVIDER_ADMIN = 900;

    private static final Logger logger = Logger.getLogger(Account.class);

    /**
     * Representation of an attribute constraint
     */
    public static final class Constraint {
        private int role;
        private String name;
        private boolean modifiable;
        private String constraint;

        /**
         * Gets the name of the attribute that this constraint controls
         * 
         * @return attribute name
         */
        public String getName() {
            return name;
        }

        /**
         * Gets the role level for which this constraint applies The role is
         * expected to be one of the ROLE constants defined in this class
         * 
         * @return integer role
         */
        public int getRole() {
            return role;
        }

        /**
         * A simple check to determine if this attribute is modifiable at this
         * role level.
         * 
         * @return true if the attribute can be modified, false otherwise
         */
        public boolean getModifiable() {
            return modifiable;
        }

        /**
         * Gets the actual constraint on the attribute This will be in the form
         * of <br>
         * NO_MODIFY <br>
         * MULTI_VALUE_LIMIT,5 <br>
         * RANGE,0-100 <br>
         * CHOICE,0,1 <br>
         * 
         * @return the attribute constraint
         */
        public String getConstraint() {
            return constraint;
        }

        /**
         * Constructs the constraint from a role, attribute name, and constraint
         * string
         * 
         * @param role role level for the constraint
         * @param name attribute name for which this constraint applies
         * @param constraint the constraint string itself
         */
        public Constraint(final int role, final String name,
                final String constraint) {
            this.role = role;
            this.name = name;
            this.constraint = constraint;
            modifiable = !(constraint.regionMatches(true, 0, "NO_MODIFY", 0, 9));
        }

        /*
         * (non-Javadoc)
         * 
         * @see java.lang.Object#toString()
         */
        public String toString() {
            return "role=" + role + ", name=`" + name + "', modifiable="
                    + modifiable + ", constraint=`" + constraint + "'";
        }
    }

    /**
     * The <code>Account.Attributes</code> class represents the set of account
     * attributes.
     * 
     * Attributes with multiple values are supported. The multiple values are
     * stored in a <code>ArrayList</code>. The get method always returns the
     * first value of multi-valued entries and the put method detects multiple
     * values and creates <code>ArrayList</code> s as necessary.
     */
    public static final class Attributes extends BasicAttributes {
        /**
         * Read the attribute/value pairs from the RME stream
         * 
         * @param stream the RME input stream
         * @throws IOException on any IO error
         */
        public Attributes(RmeInputStream stream) throws IOException {
            super(true); // ignore case

            // read attributes
            byte[] attributeBytes = stream.readByteArray();
            int[] attributeIndexes = stream.readIntArray();
            byte[] valueBytes = stream.readByteArray();
            int[] valueIndexes = stream.readIntArray();
            int numAttributeIndexes = attributeIndexes.length;
            int numValueIndexes = valueIndexes.length;

            // these should be the same, but if not, get the smaller one
            if (numValueIndexes != numAttributeIndexes) {
                logger.log(
                    Level.ERROR,
                    LogEvent.formatLogString(
                        "Rme.ProtocolBadResponse",
                        new String[] { "Account read error: numAttributes != numValues" }));
            }
            int numServices = (numValueIndexes < numAttributeIndexes) ? numValueIndexes
                    : numAttributeIndexes;

            String attributeName = null;
            String value = null;
            for (int i = 0; i < numServices; i++) {
                int attributeIndex = attributeIndexes[i];
                int attrbuteLength = -1;
                // attribute index should not usually be -1, but we check just in case.
                if (attributeIndex != -1) {
                    while ((attributeBytes[attributeIndex + (++attrbuteLength)]) != 0) {

                    }
                }
                int valueIndex = valueIndexes[i];
                int valueLength = -1;
                // value index is -1 if the attribute requested did not exist
                if (valueIndex != -1) {
                    while ((valueBytes[valueIndex + (++valueLength)]) != 0){
                        
                    }
                }
                try {
                    if (attributeIndex != -1) {
                        attributeName = new String(attributeBytes,
                                attributeIndex, attrbuteLength, "UTF-8");
                        if (valueIndex != -1) {
                            value = new String(valueBytes, valueIndex,
                                    valueLength, "UTF-8");
                            Attribute existingAttribute = get(attributeName);
                            if (existingAttribute != null) {
                                existingAttribute.add(value);
                            } else {
                                put(attributeName, value);
                            }
                        }
                    }
                } catch (UnsupportedEncodingException ignore) {
                    // do nothing, this shouldn't happen
                }
            }

        }

    }

    // some constants for account data types
    public final static int STATUS_ACTIVE = 'A';
    public final static int STATUS_SUSPENDED = 'S';
    public final static int STATUS_DELETED = 'D';
    public final static int STATUS_MAINTENANCE = 'M';
    public final static int STATUS_LOCKED = 'L';
    public final static int STATUS_PROXY = 'P';

    public final static int PWHASH_CLEAR = 'C';
    public final static int PWHASH_UNIX = 'U';
    public final static int PWHASH_MD5_PO = 'M';
    public final static int PWHASH_SHA1 = 'H';
    public final static int PWHASH_SSHA1 = 'S';
    public final static int PWHASH_CUSTOM1 = '1';
    public final static int PWHASH_CUSTOM2 = '2';
    public final static int PWHASH_CUSTOM3 = '3';
    public final static int PWHASH_CUSTOM4 = '4';
    public final static int PWHASH_CUSTOM5 = '5';

    public final static int REPLY_NONE = 'N';
    public final static int REPLY_AUTO = 'R';
    public final static int REPLY_ECHO = 'E';
    public final static int REPLY_VACATION = 'V';

    /** The function was successful. */
    public static final int MS_SUCCESS = 0;

    /** The user does not exist. */
    public static final int MS_UNKNOWNUSER = 1;

    /** The given password is incorrect. */
    public static final int MS_BADPASSWORD = 2;

    /** Not used: Compatibility. */
    public static final int MS_FORWARDTO = 3;

    /**
     * Client error or connection error: Should only be used in client code
     * because it indicates that the dirCache is down.
     */
    public static final int MS_FAILED = 4;

    /** The mailbox is not currently active. */
    public static final int MS_INACTIVE = 5;

    /** The function succeeded, but no password was sent to the server. */
    public static final int MS_NULL_PASSWORD = 6;

    /** The mailbox is undergoing maintenance */
    public static final int MS_MAINTENANCE = 7;

    /** Catch-all server error, details should appear in server log file. */
    public static final int MS_OTHER = 8;

    private char status; // administrative state of account
    private char encryption; // encryption type for plainPassword
    private boolean localDeliveryEnabled; // local MSS delivery for account
    private boolean forwardingEnabled; // MTA forwarding for account
    private char reply; // status of auto-reply for account
    private int type; // admin or end user?
    private int lookupStatus; // status code of request

    private String smtpAddress; // e.g. "johndoe@isp.com"
    private String popAddress; // e.g. "johndoe"
    private String mssHost; // logical hostname of MSS
    private String replyHost; // autoreply host
    private String mssId; // numeric string, e.g. "100000563"
    private String password; // encrypted password
    private String cosName = "default"; // name of account's Class Of Service

    private Attributes accountCos = null;// per-account COS values
    private Attributes resultCos = null; // effective COS values
    private HashMap[] constraints = null; // attribute constraints

    /**
     * Read the account information from the rme input stream
     * 
     * @param inStream the RME input stream
     * @throws IOException on any error
     */
    public Account(RmeInputStream inStream) throws IOException {

        // constructor from an rme stream
        boolean haveIt = inStream.readBoolean();
        if (haveIt) {
            type = inStream.readInt();
            status = (char)inStream.readInt();
            mssHost = inStream.readString();
            mssId = inStream.readString();
            reply = (char)inStream.readInt();
            replyHost = inStream.readString();
            forwardingEnabled = inStream.readBoolean();
            localDeliveryEnabled = inStream.readBoolean();
            cosName = inStream.readString();
            resultCos = new Attributes(inStream);
            accountCos = new Attributes(inStream);
            populateConstraints();
            smtpAddress = inStream.readString();
            password = inStream.readString();
            encryption = (char)inStream.readInt();
            popAddress = inStream.readString();

            // fam mailbox things.. not used
            inStream.readString(); // family head
            inStream.readBoolean(); // approved Sender
            inStream.readString(); // reject action

            if (inStream.getRmeVer() >= AbstractOperation.RME_GETUSERINFO_WITH_DOMAIN_VER) {
                // domain stuff - ignored
                inStream.readInt(); // domain type
                inStream.readString();
                inStream.readInt(); // wildcard account
                inStream.readString();
                inStream.readInt(); // relay Host
                inStream.readString();
                inStream.readInt(); // rewrite Domain
                inStream.readString();
            }

            lookupStatus = inStream.readInt();
            
            
        }
    }

    /**
     * Gets the account status. Will be one of the constants STATUS_xxx
     * 
     * @return status
     */
    public final int getStatus() {
        return status;
    }

    /**
     * Get the lookup status. One of the MS_xxx xonstants.
     * 
     * @return the lookup status.
     */
    public final int getLookupStatus() {
        return lookupStatus;
    }

    /**
     * Get the password encryption type used. One of the PWHASH_xxx constants.
     * 
     * @return encryption type
     */
    public final int getEncryption() {
        return encryption;
    }

    /**
     * Checks if local delivery is enabled or not.
     * 
     * @return true if local delivery is enabled, false otherwise.
     */
    public final boolean isLocalDeliveryEnabled() {
        return localDeliveryEnabled;
    }

    /**
     * Checks if forwarding is enabled or not.
     * 
     * @return true if forwarding is enabled, false otherwise
     */
    public final boolean isForwardingEnabled() {
        return forwardingEnabled;
    }

    /**
     * Gets the autoreply mode. One of the REPLY_xxx constants.
     * 
     * @return autoreply mode
     */
    public final int getAutoreplyMode() {
        return reply;
    }

    /**
     * Gets the SMTP address for the account
     * 
     * @return SMTP address
     */
    public final String getSmtpAddress() {
        return smtpAddress;
    }

    /**
     * Gets the POP address for the account
     * 
     * @return POP address
     */
    public final String getPopAddress() {
        return popAddress;
    }

    /**
     * Gets the MSS host for the account If the account is in proxy mode, this
     * is the popProxyHost
     * 
     * @return MSS host
     */
    public final String getMssHost() {
        return mssHost;
    }

    /**
     * Get the autoreply host for this account If the account is in Proxy mode,
     * this is the smtpProxyHost
     * 
     * @return autoreply host
     */
    public final String getAutoreplyHost() {
        return replyHost;
    }

    /**
     * Get the MSS ID for this account
     * 
     * @return MSS ID
     */
    public final String getMssId() {
        return mssId;
    }

    /**
     * Get the encrypted password for this account.
     * 
     * @return password
     */
    public final String getPassword() {
        return password;
    }

    /**
     * Get the COS name that this account uses.
     * 
     * @return COS name
     */
    public final String getCosName() {
        return cosName;
    }

    /**
     * Get the attributes in the Account COS
     * 
     * @return account COS
     */
    public final Attributes getAccountCos() {
        return accountCos;
    }

    /**
     * Get the Attributes in the Result COS
     * 
     * @return result COS
     */
    public final Attributes getResultCos() {
        return resultCos;
    }

    /**
     * Lookup a constraint for a given attribute and role.
     * 
     * @param role the integer constant ROLE_xxx
     * @param attributeName the attribute name
     * @return the Constraint, or null if not found.
     */
    public final Constraint findConstraint(int role, final String attributeName) {
        if (attributeName == null) {
            return null;
        }

        //10 is element 0, then 100 is 1, 200=2... 900=9
        if (role == 10) {
            role = 0;
        } else {
            role /= 100;
        }

        if (role < 0 || role > 9) {
            return null;
        }

        HashMap roleHM = constraints[role];
        if (roleHM != null) {
            return (Constraint)roleHM.get(attributeName.toLowerCase());
        }
        return null;
    }

    /**
     * This method parses the constraint values and sorts them by role.
     */
    private void populateConstraints() {
        if (constraints != null) {
            return;
        }

        constraints = new HashMap[10];
        for (int i = 0; i < 10; i++) {
            constraints[i] = new HashMap();
        }

        if (resultCos == null) {
            return;
        }

        try {
            Attribute adminAttributeConstraint = resultCos.get("adminattributeconstraint");
            if (adminAttributeConstraint == null) {
                return;
            }
            NamingEnumeration constraintsEnum = adminAttributeConstraint.getAll();
            while (constraintsEnum.hasMore()) {
                try {
                    String constraint = (String)constraintsEnum.next();

                    if (constraint == null || constraint.length() <= 2) {
                        continue;
                    }

                    // strip off the angle brackets
                    constraint = constraint.substring(1,
                        constraint.length() - 1);

                    int comIdx = constraint.indexOf(',');
                    int start = comIdx + 1;
                    comIdx = constraint.indexOf(',', comIdx + 1);
                    String strRole = constraint.substring(start, comIdx);
                    // if there are no tokens where we would expect the
                    // constraint
                    // values
                    int intRole = 0;
                    int role = Integer.parseInt(strRole);
                    if (role == 10) {
                        intRole = 0;
                    } else {
                        intRole = role / 100;
                    }
                    if (intRole < 0 || intRole > 9) {
                        continue;
                    }
                    start = comIdx + 1;
                    comIdx = constraint.indexOf(',', comIdx + 1);
                    String cName = constraint.substring(start, comIdx);
                    start = comIdx + 1;
                    String cons = constraint.substring(start);
                    Constraint s = new Constraint(role, cName, cons);
                    constraints[intRole].put(cName.toLowerCase(), s);

                } catch (Exception ignore) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(ignore.getMessage());
                    }
                    continue;
                }
            }
        } catch (NamingException ignore) {
            // there were no constraints, this is ok
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append("smtpAddress = " + smtpAddress);
        buffer.append("\npopAddress = " + popAddress);
        buffer.append("\nmssHost (or popProxy) = " + mssHost);
        buffer.append("\nautoReplyHost (or smtpProxy) = " + replyHost);
        buffer.append("\nemailIntID = " + mssId);
        buffer.append("\npassword = " + password);
        buffer.append("\ncosName = " + cosName);
        buffer.append("\n-- Type values -- ");
        buffer.append("\nstatus = " + status);
        buffer.append("\nencryption = " + encryption);
        buffer.append("\nlocal_delivery_status = " + localDeliveryEnabled);
        buffer.append("\nforwarding_status = " + forwardingEnabled);
        buffer.append("\nreply = " + reply);
        buffer.append("\nlookupStatus = " + lookupStatus);
        buffer.append("\n-- Service Attributes --");
        buffer.append("\n - Account COS -\n");

        if (accountCos != null) {
            buffer.append(accountCos + "\n");
        } else {
            buffer.append("not set\n");
        }

        buffer.append(" - Result COS -\n");

        if (resultCos != null) {
            buffer.append(resultCos + "\n");
        } else {
            buffer.append("not set\n");
        }
        buffer.append(" - Constraints -\n");
        if (constraints != null) {
            for (int role = 0; role < 10; role++) {
                if (role == 0)
                    buffer.append("Role 10:\n");
                else
                    buffer.append("Role " + role * 100 + ":\n");

                HashMap cons = constraints[role];
                Set e = cons.keySet();
                for (Iterator i = e.iterator(); i.hasNext();) {
                    String k = (String)i.next();
                    buffer.append(k + " => "
                            + ((Constraint)cons.get(k)).toString() + "\n");
                }
                buffer.append("\n");
            }
        } else {
            buffer.append("not set\n");
        }
        return buffer.toString();
    }

}

