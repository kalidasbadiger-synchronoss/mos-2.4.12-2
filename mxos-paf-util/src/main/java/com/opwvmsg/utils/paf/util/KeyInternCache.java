/*
 * Copyright (c) 2004 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/KeyInternCache.java#1 $
 */

package com.opwvmsg.utils.paf.util;

import java.util.Map;

import org.apache.log4j.Logger;

/**
 * This is a subclass of <code>BasicCache</code> that transparently
 * interns all <code>String</code> keys.
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public class KeyInternCache extends BasicCache {
    private static Logger logger = Logger.getLogger(KeyInternCache.class);

    /**
     * Construct an empty cache.
     */
    public KeyInternCache() {
        super();
    }

    /**
     * Construct a cache with the given initial contents.
     *
     * @param initialContents initialize the cache with these contents
     */
    public KeyInternCache(Map initialContents) {
        super(initialContents);
    }

    /**
     * Put an object into the cache.
     *
     * @param key the key for the object
     * @param value the object
     *
     * @return the object previously stored in the cache under this
     *    key, or <code>null</code> if there was no such object
     */ 
    public synchronized Object put(Object key, Object value) {
        if (key != null && key instanceof String) {
            key = ((String)key).intern();
        }
        return super.put(key, value);
    }
}
