/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/UpdateMsgs.java#1 $ 
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

/**
 * This operation will update the flags on the supplied messages.
 */
public class UpdateMsgs extends ReadMsgs {

    // Update Msgs is just a special case of ReadMsgs.

    // constants
    public static final int UPDATE_MSGS_FOLDER_IS_HINT = 0x08;
    public static final int UPDATE_MSGS_MULTIPLE_OK = 0x10;
    public static final int UPDATE_MSGS_UPDATE_ONLY = 0x20;
    public static final int UPDATE_MSGS_SUPPRESS_MERS = 0x40;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder (or folder hint) for the messages
     * @param uids The uids of the messages to update
     * @param flags Flags to update on the messages
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            UPDATE_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            UPDATE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            UPDATE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     */
    public UpdateMsgs(String host, String name, String pathname, int[] uids,
            String flags, int accessId, int options, String[] setFlags,
            String[] clearFlags, String[] replaceFlags) {
        super(host, name, pathname, uids, flags, null, accessId, options, 0, 0,
                null, null, setFlags, clearFlags, replaceFlags);
        this.options |= UPDATE_MSGS_UPDATE_ONLY;
        this.options &= 0xF8; // dont allow the 3 "gets"
    }

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder (or folder hint) for the messages
     * @param docids The doc ids of the messages to read
     * @param flags Flags to update on the messages
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            UPDATE_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            UPDATE_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            UPDATE_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     */
    public UpdateMsgs(String host, String name, String pathname,
            String[] docIds, String flags, int accessId, int options,
            String[] setFlags, String[] clearFlags, String[] replaceFlags) {
        super(host, name, pathname, docIds, flags, null, accessId, options, 0,
                0, null, null, setFlags, clearFlags, replaceFlags);
        this.options |= UPDATE_MSGS_UPDATE_ONLY;
        this.options &= 0xF8; // dont allow the 3 "gets"
    }
}
