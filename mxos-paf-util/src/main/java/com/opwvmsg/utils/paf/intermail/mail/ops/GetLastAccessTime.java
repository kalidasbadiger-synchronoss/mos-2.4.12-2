/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will retrieve the last access time of the mailbox.
 * 
 * @author mos-dev
 */
public class GetLastAccessTime extends AbstractMssOperation {

    // input
    private String name;
    private String url;
    private int accessId;

    // output
    private int lastAccessTime;

    /**
     * Constructor for GetLastAccessTime
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     */
    public GetLastAccessTime(String host, String name, int accessId) {
        super(RmeDataModel.CL);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_CL_MBOXLASTACCESSTIME);
        this.accessId = accessId;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }

        callRme(MSS_P_CL_MBOXLASTACCESSTIME);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeInt(accessId);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        lastAccessTime = inStream.readInt();
        logEvents = inStream.readLogEvent();
    }

    /**
     * Get the last access time from the MSS
     * 
     * @return messages
     */
    public int getLastAccessTime() throws IOException {
        return lastAccessTime;
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation is not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation is not supported
    }

}
