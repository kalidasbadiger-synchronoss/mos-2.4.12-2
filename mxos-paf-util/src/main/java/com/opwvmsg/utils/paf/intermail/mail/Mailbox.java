/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/Mailbox.java#1 $
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This class encapsulates the data in an RME Mailbox object.
 */
public class Mailbox {
    private String name;
    private String host;
    private String url;
    private long msgsStored;
    private long bytesStored;
    private Folder rootFolder = null;

    public static final int ACCESS_ADMIN = 0;
    public static final int ACCESS_GENERIC_END_USER = 1;

    /**
     * Reads this mailbox object from an RME stream
     * 
     * @param inStream the rme input stream
     * @throws IOException on any IO error
     */
    public Mailbox(RmeInputStream inStream) throws IOException {
        int[] vals;
        url = inStream.readString(); // ms name
        inStream.readIntArray(); // request
        inStream.readIntArray(); // quals
        if (inStream.getRmeVer() >= AbstractOperation.RME_MX8_VER) {
           long[] lvals;
           lvals = inStream.readLongArray(); // vals
           if (lvals != null && lvals.length >= 2) {
               msgsStored = lvals[0];
               bytesStored = lvals[1];
           }
        } else {
           vals = inStream.readIntArray(); // vals
           if (vals != null && vals.length >= 2) {
               msgsStored = vals[0];
               bytesStored = vals[1];
           }
        }
    }

    /**
     * Creates an empty Mailbox object
     */
    public Mailbox() {
        msgsStored = 0;
        bytesStored = 0;
    }
    
    /**
     * Reads the mailbox information from a given root folder.
     * 
     * @param folder a loaded Folder object with message inventory data.
     */
    public Mailbox(Folder folder) {
        rootFolder = folder;
        calculateMailboxUsage(folder);
    }
    
    /**
     * Recursively walk through the folder list to determine bytes stored and msgs stored
     * from the folder inventories.
     * 
     * @param folder root folder
     */
    private void calculateMailboxUsage(Folder folder) {
        bytesStored += folder.getNumBytes();
        msgsStored += folder.getNumMsgs();
        Folder[] subFolders = folder.getSubFoldersArray();
        for (int i = 0; i < subFolders.length; i++) {
            calculateMailboxUsage(subFolders[i]);
        }
    }
    
    /**
     * Get the total number of messages stored in this mailbox
     * 
     * @return number of messages
     */
    public final long getMsgsStored() {
        return msgsStored;
    }

    /**
     * Get the total number of bytes stored in this mailbox
     * 
     * @return number of bytes
     */
    public final long getBytesStored() {
        return bytesStored;
    }

    /**
     * Set the number of bytes stored in the Mailbox. This does not update the
     * MSS, but just updates the value in this object.
     * 
     * @param newBytesStored the new size of the mailbox.
     */
    public void setBytesStored(int newBytesStored) {
        bytesStored = newBytesStored;
    }
    
    /**
     * Tests to see if this mailbox object loaded folders to get its information.
     * 
     * @return true if folder objects were loaded, false otherwise.
     */
    public boolean loadedFolders() {
        return (rootFolder != null);
    }
    
    /**
     * If the Mailbox was loaded using folders, this function will return the 
     * root folder.  If the Mailbox was loaded from RME, then this will return 
     * null.
     * 
     * @return root folder, if available; null otherwise
     */
    public Folder getRootFolder() {
        return rootFolder;
    }

}

