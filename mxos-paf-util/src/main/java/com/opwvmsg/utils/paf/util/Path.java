/*
 *      Path.java
 *      
 *      Copyright 2000 Openwave Systems Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Path.java#1 $
 *      
 */

package com.opwvmsg.utils.paf.util;


/**
 * This class models a hierarchical path specified with elements and 
 * separator characters, such as a file system path name or an URL path.
 *
 * <P>A path is considered to be composed of two parts: the parent path
 * is everything through the last separator character in the path; the
 * name is everything after that last separator.  A path always starts
 * with a separator character.  In all cases except the root path, 
 * getParent() + getName() equals getPath().
 *
 * <P>The root path of a hierarchy has a path of a single separator 
 * character, a name that is an empty string, and a 
 * <code>null</code> parent path.  When creating a <code>Path</code>, 
 * the absolute root path is specified as a single separator character.
 *
 * <P><code>Path</code> instances are immutable.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $ 
 */
public class Path {

    private char separator = '/';
    private String path = null;
    private String parent = null;
    private String name = null;


    /**
     * Create a new <code>Path</code>.  The given <code>path</code> is 
     * interpreted as an absolute path.
     *
     * @param separator the <code>char</code> that separates path components
     * @param path the <code>String</code> path name
     * 
     */
    public Path(char separator, String path) {
        this(separator, null, path);
    }

    /**
     * Create a new <code>Path</code>.  The given <code>path</code> name is 
     * interpreted as relative to the given <code>parent</code> path, or
     * as absolute if <code>parent</code> is <code>null</code>.  Absolute
     * paths must start with the separator character; relative paths must
     * not.  An absolute path of a single separator character refers to 
     * the root path.
     *
     * @param separator the <code>char</code> that separates path components
     * @param parent the <code>Path</code> of the parent, or <code>null</code>
     *           if <code>path</code> is absolute
     * @param path the <code>String</code> path name relative to 
     *           <code>parent</code>
     * 
     * @throws IllegalArgumentException if <code>parent</code> is non-null
     *      but <code>path</code> starts with <code>separator</code>, or
     *      if <code>parent</code> is <code>null</code> and <code>path</code>
     *      does not start with a separator, or if this is a relative path
     *      but the parent path does not have the same separator character.
     */
    public Path(char separator, Path parent, String path) {
        if (path == null) {
            throw new NullPointerException("null path");
        }

        this.separator = separator;

        if (parent == null) {
            // an absolute path
            if (path.length() == 0) {
                throw new IllegalArgumentException("empty absolute path");
            } else {
                if (path.charAt(0) != separator) {
                    throw new IllegalArgumentException(
                            "absolute path without initial separator");
                }
                this.path = path;
            }
        } else {
            // a relative path
            if (parent.getSeparator() != separator) {
                throw new IllegalArgumentException("mismatched separators");
            }
            if (path.length() != 0 && path.charAt(0) == separator) {
                throw new IllegalArgumentException(
                        "relative path starts with separator");
            }
            String parentPath = parent.getPath();
            if (parentPath.length() == 1) {
                this.path = parentPath + path;
            } else {
                this.path = parentPath + separator + path;
            }
        }

        if (this.path.length() > 1) {
            int fileIndex = this.path.lastIndexOf(separator);
            if (fileIndex == this.path.length() - 1) {
                // trim trailing separator, which will be present
                // if an empty relative path was specified
                this.path = this.path.substring(0, fileIndex);
                fileIndex = this.path.lastIndexOf(separator);
            }
            this.parent = this.path.substring(0, fileIndex + 1);
            this.name = this.path.substring(fileIndex + 1);
        } else {
            // root path
            this.parent = null;
            this.name = "";
        }
    }

    /**
     * Return the full path.  The returned value is guaranteed to start
     * with a separator character, but not end with one, unless this is
     * the root path, in which case a single separator is returned.
     *
     * @return the <code>String</code> full path
     */
    public String getPath() {
        return path;
    }

    /**
     * Return the name.  The returned value is guaranteed to neither start
     * or end with a separator character.  The name is the last segment of
     * the full path.  An empty string is returned for the root path.
     *
     * @return the <code>String</code> name
     */
    public String getName() {
        return name;
    }

    /**
     * Return the parent path.  The returned value is guaranteed to both start
     * and end with a separator character, unless this is the root path, in 
     * which case this method returns <code>null</code>.  The parent path
     * is the full path with the last segment omitted.
     *
     * @return the <code>String</code> parent path, or <code>null</code>
     *        if this is the root path
     */
    public String getParent() {
        return parent;
    }

    /**
     * Return the separator character for this <code>Path</code>.
     *
     * @return a <code>char</code> separator
     */
    public char getSeparator() {
        return separator;
    }

    /**
     * Return a <code>String</code> representation of this object.
     *
     * @return a <code>String</code>
     */
    public String toString() {
        return "Path[path=" + getPath() + "]";
    }
}
