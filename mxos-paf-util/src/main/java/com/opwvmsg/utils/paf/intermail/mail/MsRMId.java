/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * This class encapsulates the RMId details of the Folder/Mailbox object.
 * 
 * @author mOS-dev
 */
public class MsRMId {

    public static final String DEFAULT_FOLDER_UUID = "00000000-0000-0000-0000-000000000000";

    private int ndx; // RMESock client index
    private String msName; // Mailbox Name
    private UUID folderUUID; // Folder UUID
    private UUID clientUUID; // clientUUID

    /**
     * Default constructor with default values
     * 
     * @param inStream
     * @throws IOException
     */
    public MsRMId() {
        ndx = 0;
        msName = "";
        folderUUID = UUID.fromString(DEFAULT_FOLDER_UUID);
        clientUUID = UUID.fromString(DEFAULT_FOLDER_UUID);
    }

    /**
     * Creates an empty RMId object from RmeInputStream
     * 
     * @param inStream
     * @throws IOException
     */
    public MsRMId(RmeInputStream inStream) throws IOException {
        ndx = inStream.readInt();
        msName = inStream.readString();
        folderUUID = inStream.readUUID();
        clientUUID = inStream.readUUID();
    }

    public void writeMsRMId(RmeOutputStream outStream) throws IOException {
        outStream.writeInt(ndx);
        outStream.writeString(msName);
        outStream.writeUUID(folderUUID);
        outStream.writeUUID(clientUUID);
    }

    /**
     * @return the ndx
     */
    public int getNdx() {
        return ndx;
    }

    /**
     * @param ndx the ndx to set
     */
    public void setNdx(int ndx) {
        this.ndx = ndx;
    }

    /**
     * @return the msName
     */
    public String getMsName() {
        return msName;
    }

    /**
     * @param msName the msName to set
     */
    public void setMsName(String msName) {
        this.msName = msName;
    }

    /**
     * @return the folderUUID
     */
    public UUID getFolderUUID() {
        return folderUUID;
    }

    /**
     * @param folderUUID the folderUUID to set
     */
    public void setFolderUUID(UUID folderUUID) {
        this.folderUUID = folderUUID;
    }

    /**
     * @return the clientUUID
     */
    public UUID getClientUUID() {
        return clientUUID;
    }

    /**
     * @param clientUUID the clientUUID to set
     */
    public void setClientUUID(UUID clientUUID) {
        this.clientUUID = clientUUID;
    }

}
