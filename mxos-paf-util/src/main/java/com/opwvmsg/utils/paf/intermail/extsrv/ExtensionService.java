/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/ExtensionService.java#1 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.SocketPool;
import com.opwvmsg.utils.paf.util.Utilities;

/**
 * This class encapsulates a particular extension service including all
 * configuration attributes, connection information, and a link to the socket
 * pools used by this service.
 */
public class ExtensionService {
    private static final int EXECUTION_MANDATORY = 0;
    private static final int EXECUTION_ATTEMPT = 1;
    private static final int EXECUTION_COS = 2;

    private String serviceName = "default";
    private int execution = EXECUTION_ATTEMPT;
    private int logSeverity = LogEvent.WARNING;
    private String argument = "";
    private Set hooks = new HashSet(10);
    private int retries;
    private boolean sendCos;

    private ArrayList extServHostPorts = new ArrayList();
    private int numServers;
    private int currentServer = 0;

    // All of these values are configurable
    private int maxPool = DEFAULT_MAX_POOL;
    private int initialPool = DEFAULT_INITIAL_POOL;
    private int maxIdle = DEFAULT_MAX_IDLE;
    private int timeout = DEFAULT_TIMEOUT;

    // defaults
    private static int DEFAULT_MAX_POOL = 10;
    private static int DEFAULT_INITIAL_POOL = 1;
    private static int DEFAULT_MAX_IDLE = 600;
    private static int DEFAULT_TIMEOUT = 5000;

    private int numFailures = 0;
    private boolean disabled = false;

    private SocketPool thePool = null;

    private static final Map logSeverities = new HashMap(5);
    static {
        // these are severities specific to the extension services.
        // Although they are the same as other log severities right now,
        // this may not always be so.
        logSeverities.put("notification", new Integer(LogEvent.NOTIFICATION));
        logSeverities.put("warning", new Integer(LogEvent.WARNING));
        logSeverities.put("error", new Integer(LogEvent.ERROR));
        logSeverities.put("urgent", new Integer(LogEvent.URGENT));
        logSeverities.put("fatal", new Integer(LogEvent.FATAL));
    }

    private static final Logger logger = Logger.getLogger(ExtensionService.class);

    /**
     * Create a new service with default values
     */
    protected ExtensionService() {
    }

    /**
     * Parses one entry of the config key extensionsServiceAttributes and sets
     * the values in this class
     * 
     * @param configAttributes - the entry to parse
     * @throws IntermailExcpetion on any error
     */
    void setConfigAttributes(String configAttributes) throws IntermailException {
        // first, split into key value pairs
        List pairs = parseConfigAttributes(configAttributes);

        // first one is serviceName
        if (pairs.size() == 0) {
            return;
        }
        serviceName = (String) pairs.get(0);

        Set setValues = new HashSet(6);
        // now go over the rest, and set the attributes
        for (int i = 1; i < pairs.size(); i++) {
            boolean ok = true;
            String pair = (String) pairs.get(i);
            String key;
            String value;
            int colon = pair.indexOf(":");
            if (colon == -1) {
                // this will probably just hit the
                // unknown field error below.
                key = pair;
                value = "";
            } else {
                key = pair.substring(0, colon);
                value = pair.substring(colon + 1);
            }

            if (value.length() > 1 && value.charAt(0) == '\"') {
                value = value.substring(1, value.length() - 1);
            }
            if (setValues.contains(key)) {
                throw new IntermailException(LogEvent.ERROR,
                                             LogEvent.formatLogString("Ext.AmbiguousExtensionServiceField",
                                                                      new String[] {
                                                                              serviceName,
                                                                              key }));
            }
            if (key.equalsIgnoreCase("execution")) {
                ok = setExecution(value);
            } else if (key.equalsIgnoreCase("severity")) {
                ok = setSeverity(value);
            } else if (key.equalsIgnoreCase("hooks")) {
                ok = setHooks(value);
            } else if (key.equalsIgnoreCase("retries")) {
                ok = setRetries(value);
            } else if (key.equalsIgnoreCase("sendCos")) {
                setSendCos(Boolean.getBoolean(value));
            } else {
                throw new IntermailException(LogEvent.ERROR,
                                             LogEvent.formatLogString("Ext.InvalidExtensionServiceFieldNames",
                                                                      new String[] {
                                                                              serviceName,
                                                                              key }));
            }

            if (!ok) {
                throw new IntermailException(LogEvent.ERROR,
                                             LogEvent.formatLogString("Ext.InvalidExtensionServiceAttributes",
                                                                      new String[] {
                                                                              serviceName,
                                                                              key,
                                                                              value }));
            } else {
                setValues.add(key);
            }
        }
    }

    private List parseConfigAttributes(String configAttributes) {
        List pairs = new ArrayList();

        // need to handle quoted strings.
        boolean quoted = false;
        boolean added = false;
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < configAttributes.length(); i++) {
            char ch = configAttributes.charAt(i);
            switch (ch) {
            case (' '):
                if (quoted) {
                    buf.append(ch);
                    added = false;
                } else if (!added) {
                    pairs.add(buf.toString());
                    buf.setLength(0);
                    added = true;
                }
                break;
            case ('\\'):
                if (i++ < configAttributes.length()) {
                    buf.append(configAttributes.charAt(i));
                    added = false;
                }
                break;
            case ('\"'):
                quoted = !quoted;
                added = false;
                break;
            default:
                added = false;
                buf.append(ch);
            }
        }
        if (buf.length() > 0) {
            pairs.add(buf.toString());
        }
        return pairs;
    }

    /**
     * Parse a connection URL and add it to the list of connections if it
     * appears to be valid.
     * 
     * @param urlIn - the connection information, either rme://host:port or just
     *            host:port
     */
    void addConnection(String urlIn) {
        urlIn = urlIn.trim();
        int urlIndex = urlIn.indexOf("://");
        if (urlIndex != -1) {
            String protocol = urlIn.substring(0, urlIndex);
            if (!protocol.equalsIgnoreCase("rme")) {
                logger.log(Level.ERROR,
                           LogEvent.formatLogString("Ext.InvalidMappingCfg",
                                                    new String[] { urlIn }));
                return;
            }
            urlIn = urlIn.substring(urlIndex + 3);
        }
        int colon = urlIn.indexOf(":");
        if (colon == -1) {
            logger.log(Level.ERROR,
                       LogEvent.formatLogString("Ext.InvalidMappingCfg",
                                                new String[] { urlIn }));
            return;
        }
        String host = urlIn.substring(0, colon);
        Integer port = null;
        try {
            port = new Integer(urlIn.substring(colon + 1));
        } catch (NumberFormatException ne) {
            logger.log(Level.ERROR,
                       LogEvent.formatLogString("Nio.BadPortNumber",
                                                new String[] {
                                                        "extensionsMap ["
                                                                + serviceName
                                                                + "]",
                                                        urlIn.substring(colon + 1) }));
            return;
        }
        if (port.intValue() < 0) {
            logger.log(Level.ERROR,
                       LogEvent.formatLogString("Nio.BadPortNumber",
                                                new String[] {
                                                        "extensionsMap ["
                                                                + serviceName
                                                                + "]",
                                                        port.toString() }));
            return;
        }
        ServerHostPort hostPort = new ServerHostPort(host, port.intValue());
        extServHostPorts.add(hostPort);
        numServers++;
    }

    /**
     * Gets the number of configured servers that can handle this service
     * 
     * @return number of configured servers.
     */
    int getNumServers() {
        return numServers;
    }

    /**
     * Set the hooks for this service
     * 
     * @param hooksIn - CSV string of hook names
     * @return true if the list seems valid, false otherwise
     */
    private boolean setHooks(String hooksIn) {
        String[] hooksArray = Utilities.csvToArray(hooksIn);
        if (hooksArray.length == 0) {
            return false;
        }
        for (int i = 0; i < hooksArray.length; i++) {
            hooks.add(hooksArray[i]);
        }
        return true;
    }

    /**
     * Gets the set of hooks that this service is interested in.
     * 
     * @return Set of hook names.
     */
    Set getHooks() {
        return Collections.unmodifiableSet(hooks);
    }

    /**
     * Test to determine if the service handles a particular hook
     * 
     * @param hook the hook serviceName
     * @return true if the service is configured for that hook, else false
     */
    boolean handlesEvent(String hook) {
        return hooks.contains(hook);
    }

    /**
     * Set the RME timeout for connections this services uses.
     * 
     * @param timeout timeout in ms
     */
    void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /**
     * Set the max pool size for this service's connection pool
     * 
     * @param maxPoolSize max pool size.
     */
    void setMaxPool(int maxPoolSize) {
        maxPool = maxPoolSize;
    }

    /**
     * Set the minimum pool size for this service's connection pool
     * 
     * @param minPoolSize min pool size
     */
    void setInitialPool(int initialPoolSize) {
        initialPool = initialPoolSize;
    }

    /**
     * Initialize the socket pool.
     * 
     * @param acqTimeout - the timeout for connecting to the server
     * @param acqMaxTries - the max tries before giving up.
     */
    void initSockPool() {
        // assumes host and port have been set at least
        // returns false if not
        if (getNumServers() == 0) {
            return;
        }

        currentServer = 0;
        ServerHostPort currentHost = (ServerHostPort) extServHostPorts.get(currentServer);
        thePool = new SocketPool(maxPool,
                                 initialPool,
                                 maxIdle,
                                 currentHost.getHostname(),
                                 currentHost.getPort(),
                                 timeout,
                                 "extensionService(" + serviceName + ")");
    }

    /**
     * Tell this service to fail over to a new host if available. The failover
     * will only occur if the calling function is using the same host and port
     * as what is currently in use in the pool. (i.e if a thread has an old copy
     * of the pool, and we already failed over, don't fail over again)
     * 
     * @param host - the host that failed (
     * @param port - the port that failed
     * @return the new socket pool.
     */
    protected synchronized SocketPool failOver(String host, int port) {
        if (thePool.getHost().equals(host) && thePool.getPort() == port) {
            currentServer = (currentServer + 1) % numServers;
            SocketPool oldPool = thePool;
            ServerHostPort currentHost = (ServerHostPort) extServHostPorts.get(currentServer);
            thePool = new SocketPool(maxPool,
                                     initialPool,
                                     maxIdle,
                                     currentHost.getHostname(),
                                     currentHost.getPort(),
                                     timeout,
                                     "extensionService(" + serviceName + ")");
            oldPool.clear();
        }
        return thePool;

    }

    /**
     * Set the excection attribute for this service
     * 
     * @param execIn - the exceution type
     * @return true if the value was recognized, false otherwise
     */
    private boolean setExecution(String execIn) {
        execIn = execIn.trim().toLowerCase();
        if (execIn.equals("mandatory")) {
            execution = EXECUTION_MANDATORY;
        } else if (execIn.equals("cos")) {
            execution = EXECUTION_COS;
        } else if (execIn.equals("attempt")) {
            execution = EXECUTION_ATTEMPT;
        } else {
            return false;
        }
        return true;
    }

    /**
     * Test to see if this service is mandatory
     * 
     * @return true if service is mandatory, false otherwise
     */
    public boolean isMandatory() {
        return execution == EXECUTION_MANDATORY;
    }

    /**
     * Test to see if this service is an attempt only
     * 
     * @return true if the service's execution is attempt only, false otherwise
     */
    public boolean isAttempt() {
        return execution == EXECUTION_ATTEMPT;
    }

    /**
     * Test to see if execution of this service is COS based
     * 
     * @return true if the execution is cos based, false otherwise
     */
    public boolean isCos() {
        return execution == EXECUTION_COS;
    }

    /**
     * Set the log severity of this service (called from parseConfigAttributes)
     * 
     * @param severity the String severity to set
     * @return true if the severity was valid, false otherwise
     */
    private boolean setSeverity(String severity) {
        severity = severity.trim().toLowerCase();
        Integer sev = (Integer) logSeverities.get(severity);
        if (sev != null) {
            logSeverity = sev.intValue();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the log severity for this service
     * 
     * @return the log severity
     */
    int getLogSeverity() {
        return logSeverity;
    }

    /**
     * Get the arguments to this service
     * 
     * @return the String arguments
     */
    String getArgument() {
        return argument;
    }

    /**
     * Sets the number of retries to use.
     * 
     * @param retriesString - number of retries (as String)
     * @return true if the value is valid, false otherwise.
     */
    private boolean setRetries(String retriesString) {
        try {
            retries = Integer.parseInt(retriesString);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Resets the connection failure count on successful connection
     */
    protected synchronized void setSuccess() {
        numFailures = 0;
    }

    /**
     * Gets the current number of consecutive failures to connect for this
     * service
     * 
     * @return number of failures
     */
    protected synchronized int getFailures() {
        return numFailures;
    }

    /**
     * Increment the connection failure count by one.
     */
    protected synchronized void incrementFailures() {
        numFailures++;
    }

    /**
     * Disables this service
     */
    protected void disable() {
        disabled = true;
    }

    /**
     * Checks to see if this service is disabled
     * 
     * @return true if the service is disabled
     */
    protected boolean isDisabled() {
        return disabled;
    }

    /**
     * Sets whether this service is dependant on a COS
     * 
     * @param useCos true if this service uses the COS, false otherwise
     */
    private void setSendCos(boolean useCos) {
        sendCos = useCos;
    }

    /**
     * Get the current socket pool for this service
     * 
     * @return current socket pool
     */
    SocketPool getPool() {
        return thePool;
    }

    /**
     * Get the RME timeout set for this service
     * 
     * @return current RME timeout
     */
    int getTimeout() {
        return timeout;
    }

    /**
     * Get the initial pool size setting for this service
     * 
     * @return initial pool size
     */
    int getInitialPool() {
        return initialPool;
    }

    /**
     * Get the maximum pool size setting for this service
     * 
     * @return maximum pool size
     */
    int getMaxPool() {
        return maxPool;
    }

    /**
     * Get the serviceName of this service
     * 
     * @return serviceName
     */
    public String getName() {
        return serviceName;
    }

    private static class ServerHostPort {
        String hostname;
        int port;

        ServerHostPort(String hostname, int port) {
            this.hostname = hostname;
            this.port = port;
        }

        private String getHostname() {
            return hostname;
        }

        private int getPort() {
            return port;
        }
    }

}

