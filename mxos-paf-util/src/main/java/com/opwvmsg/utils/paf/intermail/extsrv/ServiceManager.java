/*  H+
 *      Copyright 2004-2005 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/extsrv/ServiceManager.java#1 $
 *      
 *  H- */
package com.opwvmsg.utils.paf.intermail.extsrv;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.SocketPool;

/**
 * This class is responsible for managing the various configured extension
 * services.
 */
public class ServiceManager {

    private static boolean initialized = false;
    private static HashMap services = null;
    private static HashMap servicesPerEvent = new HashMap();
    private static ArrayList executionOrder = null;
    private static int numServices = 0;

    private static boolean extensionsEnabled = false;

    private static int maxConnectFailures = 10;

    private static final Logger logger = Logger.getLogger(ServiceManager.class);
    
    private static final String DEFAULT_SERVICE = "default";

    /* Initialization */
    static {
        initRme();
    }
    
    /**
     * Given the service, return a connection to the server handling that
     * service.
     * 
     * @param service - the service to connect to
     * @param reuseSocket - if true, the pool may be used, if false, a new
     *            connection is returned.
     * @return a valid, initialized RmeSocket
     * @throws IntermailException - on any fatal error.
     */
    protected static RmeSocket getRmeSocket(ExtensionService service,
                                            boolean reuseSocket)
            throws IntermailException {
        if (service.isDisabled()) {
            throw new IntermailException(
                    LogEvent.ERROR,
                    LogEvent.formatLogString(
                            "Ext.ServiceIsDown",
                            new String[] { "Extension", service.getName() }));
        }

        SocketPool pool = service.getPool();
        int numConnections = service.getNumServers();

        RmeSocket sock = null;
        try {
            if (numConnections == 1) {
                if (reuseSocket) {
                    sock = (RmeSocket) pool.getItem();
                }
                if (sock == null) {
                    sock = pool.createSocket();
                }
            } else {
                String startHost = pool.getHost();
                int startPort = pool.getPort();
                while (sock == null) {
                    try {
                        if (reuseSocket) {
                            sock = (RmeSocket) pool.getItem();
                        }
                        if (sock == null) {
                            sock = pool.createSocket();
                        }
                    } catch (IntermailException ie) {
                        // this should cause a new pool to be created.
                        // however, we pass in host, in case
                        // our local sockPool is stale and someone
                        // already failed it over
                        pool = service.failOver(pool.getHost(), pool.getPort());
                        if (pool.getHost().equals(startHost)
                                && pool.getPort() == startPort) {
                            throw new IntermailException(
                                    LogEvent.URGENT,
                                    LogEvent.formatLogString(
                                            "Nio.ConnServerFail",
                                            new String[] {
                                                "extensionService(" +
                                                    service.getName() + ")",
                                                startHost,
                                                Integer.toString(startPort),
                                                "No backup servers available." }));
                        } else {
                            logger.log(
                                    Level.ERROR,
                                    LogEvent.formatLogString(
                                            "Nio.ConnServerFail",
                                            new String[] {
                                                "extensionService(" +
                                                    service.getName() + ")",
                                                pool.getHost(),
                                                Integer.toString(pool.getPort()),
                                                "Failing over to next server." }));
                        }
                    }
                }
            }
        } catch (IntermailException e) {
            // we failed to get a connection
            // increment the failed count
            service.incrementFailures();
            int failures = service.getFailures();
            logger.log(
                    Level.WARN,
                    LogEvent.formatLogString(
                            "Ext.FailedServiceTryNumber",
                            new String[] {
                                service.getName(),
                                Integer.toString(failures) }));
            if (failures >= maxConnectFailures) {
                service.disable();
                throw new IntermailException(
                        LogEvent.ERROR,
                        LogEvent.formatLogString(
                                "Ext.ServiceFailed",
                                new String[] {
                                    "Extension",
                                    service.getName(),
                                    Integer.toString(maxConnectFailures) }));
            }
            throw e;
        }
        service.setSuccess();
        return sock;
    }

    /**
     * Put the socket back into the correct pool.
     * 
     * @param sock - the socket to be returned to a pool
     * @param serv - the service to which this socket belongs to.
     */
    protected synchronized static void putRmeSocket(RmeSocket sock,
                                                    ExtensionService serv) {
        SocketPool pool = serv.getPool();
        pool.putItem(sock);
    }

    /**
     * Returns an ArrayList of configured Services for the given hook. The list
     * is ordered the order that the services should be executed in.
     * 
     * @param hook - the name of the hook
     * @return ordered list of services configured for that hook
     */
    protected static List getEnabledServicesList(String hook) {
        List services = (List) servicesPerEvent.get(hook.toLowerCase());
        if (services == null) {
            return new ArrayList(0);
        }
        return services;
    }

    public static boolean hookHasEnabledServices(String hook) {
        List services = (List) servicesPerEvent.get(hook.toLowerCase());
        if (services == null || services.size() == 0) {
            return false;
        }
        return true;
    }
    
    /**
     * Initialize the Extensions Server client Load all services and
     * configuration parameters. Errors will be reported, but this function
     * should not fail.
     */
    public synchronized static void initRme() {
        // check initialized again
        if (!initialized) {

            // get the list of enabled extensions
            Config config = Config.getInstance();
            String[] extServices = config.getArray(
                    "extensionsEnabled", new String[0]);
            if (extServices == null || extServices.length == 0) {
                extensionsEnabled = false;
                initialized = true;
                return;
            }

            // the execution order will be the same as the order
            // in the extensionsEnabled key.
            executionOrder = new ArrayList(extServices.length);
            for (int i = 0; i < extServices.length; i++) {
                String key = extServices[i].toLowerCase();
                if (key.length() > 0) {
                    executionOrder.add(extServices[i].toLowerCase());
                }
            }

            HashMap tmpServices = null;
            String[] attributes = config.getArray(
                    "extensionsServiceAttributes", new String[0]);
            tmpServices = new HashMap(attributes.length);
            if (attributes != null && attributes.length > 0) {
                for (int i = 0; i < attributes.length; i++) {
                    try {
                        //skip blank entries
                        if (attributes[i].trim().length() == 0) {
                            continue;
                        }
                        ExtensionService serv = new ExtensionService();
                        serv.setConfigAttributes(attributes[i]);
                        if (tmpServices.get(serv.getName()) == null) {
                            tmpServices.put(serv.getName().toLowerCase(), serv);
                        } else {
                            // this attr appeared twice
                            logger.log(Level.ERROR,
                                    LogEvent.formatLogString(
                                            "Ext.AmbiguousExtSrvAttrEntry",
                                            new String[] { serv.getName() }));
                        }
                    } catch (IntermailException ie) {
                        // report, but keep going
                        logger.log(ie.getLog4jLevel(), ie.getFormattedString());
                    }
                }
            }

            // Now, get the intersection of the
            // enabled list and the attribute list
            services = new HashMap(extServices.length);
            for (int i = 0; i < executionOrder.size(); i++) {
                String key = (String) executionOrder.get(i);
                ExtensionService serv = (ExtensionService) tmpServices.get(key);
                if (serv == null) {
                    if (services.get(key) != null) {
                        // this entry had two entries in the enabled list
                        logger.log(
                                Level.ERROR,
                                LogEvent.formatLogString(
                                        "Ext.RequestEventsDuplicateService",
                                        new String[] { key, "2" }));
                    } else {
                        // this entry had no mappings
                        logger.log(
                              Level.ERROR,
                              LogEvent.formatLogString(
                              "Ext.MissingExtensionsServiceAttributesEntries",
                              new String[] { key }));
                    }
                    // remove entry from order
                    executionOrder.remove(i--);
                } else {
                    services.put(key, serv);
                    tmpServices.remove(key);
                }
            }

            // Add to that, their connection info
            String[] connectionMap = config.getArray(
                    "extensionsMap", new String[0]);
            if (executionOrder.size() > 0
                    && (connectionMap == null || connectionMap.length == 0)) {
                logger.log(Level.ERROR,
                           LogEvent.formatLogString("Ext.NoMappingCfg",
                                                    new String[0]));
            }

            if (connectionMap != null) {
                HashMap map = parseConfigKeyMap(connectionMap, "extensionsMap");
                Iterator iter = map.keySet().iterator();
                while (iter.hasNext()) {
                    String key = ((String) iter.next()).toLowerCase();
                    ExtensionService serv = (ExtensionService) services.get(key);
                    if (serv == null) {
                        // quietly ignore this extra entry..
                        // if it was a typo, they will get another
                        // error when we find one missing below
                    } else {
                        serv.addConnection((String) map.get(key));
                    }
                }
            }

            // check all services again, and make sure they
            // all have connection info.. if not log error,
            // and remove from list
            for (int i = 0; i < executionOrder.size(); i++) {
                String key = (String) executionOrder.get(i);
                ExtensionService serv = (ExtensionService) services.get(key);
                if (serv.getNumServers() == 0) {
                    // this service has no mappings
                    logger.log(
                            Level.WARN,
                            LogEvent.formatLogString(
                                    "Ext.ServiceIsDown",
                                    new String[] { "MessageExamine", key }));
                    executionOrder.remove(i--);
                    services.remove(key);
                }
            }

            // get any pool settings
            Iterator servicesIterator = services.values().iterator();
            ExtensionService tmpService = null;
            Map servicesMap = getPerServiceIntValues(
                    config, services, "extensionsConnectionPoolMax");
            Integer defaultValue = (Integer)servicesMap.get(DEFAULT_SERVICE);
            while (servicesIterator.hasNext()) {
                tmpService = (ExtensionService)servicesIterator.next();
                Integer cfgValue = (Integer)servicesMap.get(tmpService.getName()); 
                if (cfgValue != null) {
                    tmpService.setMaxPool(cfgValue.intValue());
                } else if (defaultValue != null) {
                    tmpService.setMaxPool(defaultValue.intValue());
                }
            }

            servicesIterator = services.values().iterator();
            servicesMap = getPerServiceIntValues(
                    config, services, "extensionsConnectionPoolMin");
            defaultValue = (Integer)servicesMap.get(DEFAULT_SERVICE);
            while (servicesIterator.hasNext()) {
                tmpService = (ExtensionService)servicesIterator.next();
                Integer cfgValue = (Integer)servicesMap.get(tmpService.getName()); 
                if (cfgValue != null) {
                    tmpService.setInitialPool(cfgValue.intValue());
                } else if (defaultValue != null) {
                    tmpService.setInitialPool(defaultValue.intValue());
                }
            }

            servicesIterator = services.values().iterator();
            servicesMap = getPerServiceIntValues(
                    config, services, "extensionsConnectionTimeoutMS");
            defaultValue = (Integer)servicesMap.get(DEFAULT_SERVICE);
            while (servicesIterator.hasNext()) {
                tmpService = (ExtensionService)servicesIterator.next();
                Integer cfgValue = (Integer)servicesMap.get(tmpService.getName()); 
                if (cfgValue != null) {
                    tmpService.setTimeout(cfgValue.intValue());
                } else if (defaultValue != null) {
                    tmpService.setTimeout(defaultValue.intValue());
                }
            }

            // acq timeouts and retries
            maxConnectFailures = config.getInt("extensionsAcqMaxTimeouts", 10);

            // create a pool for each service
            // while we're looping, get all known hooks
            HashSet knownHooks = new HashSet(20);
            for (int i = 0; i < executionOrder.size(); i++) {
                ExtensionService serv = (ExtensionService) services.get(
                        (String) executionOrder.get(i));
                serv.initSockPool();
                knownHooks.addAll(serv.getHooks());
            }

            numServices = executionOrder.size();

            // get all known events. for each event, build a list
            // of services that handle that event.
            Iterator iter = knownHooks.iterator();
            while (iter.hasNext()) {
                String hook = (String) iter.next();
                List servicesList = initEnabledServicesList(hook);
                servicesPerEvent.put(hook.toLowerCase(), servicesList);
            }

            initialized = true;
        }
    }

    private static Map getPerServiceIntValues(Config config, Map services,
                                              String configKeyName) {
        Map returnMap = new HashMap();

        String[] poolMap = config.getArray(configKeyName, new String[0]);
        if (poolMap != null && poolMap.length > 0) {
            HashMap map = parseConfigKeyMap(poolMap, configKeyName);
            Iterator iter = map.keySet().iterator();
            while (iter.hasNext()) {
                String key = ((String) iter.next()).toLowerCase();
                if (!key.equals(DEFAULT_SERVICE)) {
                    ExtensionService serv = (ExtensionService) services.get(key);
                    if (serv == null) {
                        logger.log(
                            Level.WARN,
                            LogEvent.formatLogString(
                                "Ext.InvalidExtensionServiceFieldNames",
                                new String[] {
                                        configKeyName + " (config key)",
                                        key }));
                        // ignore this entry
                        continue;
                    }
                }
                int intValue = 0;
                try {
                    intValue = Integer.parseInt((String) map.get(key));
                } catch (NumberFormatException nfe) {
                    logger.log(
                        Level.WARN,
                        LogEvent.formatLogString(
                            "Ext.InvalidExtensionServiceAttributes",
                            new String[] {
                                    configKeyName + " (config key)",
                                    key,
                                    (String) map.get(key) }));
                    continue;
                }
                returnMap.put(key, new Integer(intValue));
            }
        }
        return returnMap;
    }

    /**
     * Initializes the list of enabled services for the specified hook.
     * 
     * @param hook - the name of the hook
     * @return the ordered list of configured services for that hook.
     */
    protected static List initEnabledServicesList(String hook) {
        ArrayList al = new ArrayList(executionOrder.size());
        for (int i = 0; i < numServices; i++) {
            ExtensionService serv = (ExtensionService) services.get(
                    (String) executionOrder.get(i));
            if (serv.handlesEvent(hook)) {
                al.add(serv);
            }
        }
        return al;
    }

    /**
     * Parses a multi-value config key with [servicename value] and places the
     * values into a HashMap
     * 
     * @param keysIn the multi-value config key
     * @return HashMap of key value pairs.
     */
    private static HashMap parseConfigKeyMap(String[] keysIn, String keyName) {
        HashMap map = new HashMap(keysIn.length);
        for (int i = 0; i < keysIn.length; i++) {
            String line = keysIn[i].trim();
            if (line.length() == 0) {
                continue;
            }
            int sp = line.indexOf(" ");
            if (sp == -1) {
                logger.log(Level.ERROR,
                           LogEvent.formatLogString("Ext.InvalidMappingCfg",
                                                    new String[] { keyName
                                                            + ": " + line }));
            } else {
                String key = line.substring(0, sp).trim();
                String value = line.substring(sp + 1).trim();
                map.put(key.toLowerCase(), value);
            }
        }
        return map;
    }

}
