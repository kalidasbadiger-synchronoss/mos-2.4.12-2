/*
 * Copyright (c) 2002 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/taglibs/config/ConfigMap.java#1 $
 *
 */

package com.opwvmsg.utils.paf.taglibs.config;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.util.EscapeType;
import com.opwvmsg.utils.paf.util.Utilities;


/**
 * This class provides a <code>Map</code> interface to configuration
 * data.  This class is specifically used by the config taglib and is
 * not intended for extension or direct instantiation.
 */
class ConfigMap implements Map {
    private static final Logger logger =
            Logger.getLogger("com.openwave.paf.config.ConfigMap");

    private Config config;
    private String host;
    private String app;
    private Type type;
    private String def;
    private boolean escapeXml;
    private EscapeType escapeType;


    public ConfigMap(Config config, String host, String app, Type type,
                     String def, boolean escapeXml, EscapeType escapeType) {
        this.config = config;
        this.host = host;
        this.app = app;
        this.type = type;
        this.def = def;
        this.escapeXml = escapeXml;
        this.escapeType = escapeType;
    }

    public void clear() {
        throw new UnsupportedOperationException();
    }

    public Object put(Object key, Object value) {
        throw new UnsupportedOperationException();
    }

    public void putAll(Map t) {
        throw new UnsupportedOperationException();
    }

    public Object remove(Object key) {
        throw new UnsupportedOperationException();
    }


    public Object get(Object key) {
        if (! (key instanceof String)) {
            throw new IllegalArgumentException("key not a string");
        }
        return getProperty((String) key);
    }

    public boolean containsKey(Object key) {
        if (! (key instanceof String)) {
            throw new IllegalArgumentException("key not a string");
        }
        return getProperty((String) key) != null;
    }

    private Object getProperty(String key) {
        Object result = null;
        try {
            if (type == Type.STRING) {
                if (def == null) {
                    result = config.get(host, app, key);
                } else {
                    result = config.get(host, app, key, def);
                }
            } else if (type == Type.INT) {
                if (def == null) {
                    result = new Integer(config.getInt(host, app, key));
                } else {
                    int idef = Integer.parseInt(def);
                    result = new Integer(config.getInt(host, app, key, idef));
                }
            } else if (type == Type.FLOAT) {
                if (def == null) {
                    result = new Float(config.getFloat(host, app, key));
                } else {
                    float fdef = Float.parseFloat(def);
                    result = new Float(config.getFloat(host, app, key, fdef));
                }
            } else if (type == Type.BOOLEAN) {
                if (def == null) {                
                    result = new Boolean(config.getBoolean(host, app, key));
                } else {
                    boolean bdef = Boolean.valueOf(def).booleanValue();
                    result = new Boolean(config.getBoolean(host, app, key, bdef));
                }
            }
        } catch (ConfigException e) {
            throw new IllegalStateException(e.toString());
        } catch (NumberFormatException e) {
            throw new IllegalStateException(e.toString());
        }
        if (escapeXml && result instanceof String) {
            result = Utilities.escapeXml((String) result, escapeType);
        }
        return result;
    }

    public boolean containsValue(Object value) {
        return false;
    }

    public Set entrySet() {
        return Collections.EMPTY_SET;
    }

    public Set keySet() {
        return Collections.EMPTY_SET;
    }

    public Collection values() {
        return Collections.EMPTY_SET;
    }

    public int size() {
        return 0;
    }

    public boolean isEmpty() {
        return true;
    }

}
