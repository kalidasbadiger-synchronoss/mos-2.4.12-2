/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/Folder.java#1 $
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;

/**
 * This class encapsulates all of the data returned from an RME Folder object.
 */
public class Folder {
    private boolean isRead = false;
    private String pathname;
    private String[] pathnameArray;
    private int UIDValidity;
    private int nextUID;
    private Inventory[] inventories;
    private int[] msgUids;
    private String[] msgIds;
    private int msgIdOffset = 0;
    private Folder[] subFolders; // if recursive
    private ArrayList folderNames;
    private int numMessages = -1;
    private int numUnreadMessages = -1;
    private long numBytes = -1;

    /**
     * Reads this folder object from an RME stream
     * 
     * @param inStream the rme input stream
     * @param storeMsgIds if false, the message ids will be thrown away,
     *     even if the MSS returns them (to save on memory)
     * @throws IOException on any IO error
     */
    public Folder(RmeInputStream inStream, 
                  boolean storeMsgIds) throws IOException {
        isRead = inStream.readBoolean();
        if (isRead) {
            pathnameArray = inStream.readStringArray();
            pathname = pathFromArray(pathnameArray);
            UIDValidity = inStream.readInt();
            nextUID = inStream.readInt();
            int numInv = inStream.readInt();
            inventories = new Inventory[numInv];
            numMessages = 0;
            numUnreadMessages = 0;
            numBytes = 0;
            for (int i = 0; i < numInv; i++) {
                inventories[i] = new Inventory(inStream);
                numMessages += inventories[i].getMsgCount();
                numBytes += inventories[i].getMsgBytes();
                if (inventories[i].getUnread()) {
                    numUnreadMessages += inventories[i].getMsgCount();
                }
            }
            msgUids = inStream.readIntArray();
            if (storeMsgIds) {
                msgIds = inStream.readStringArray(); 
            } else {
                inStream.readStringArray();
            }
            
            // reset numMsgs if the inventories are missing
            // This is only present to workaround the email bug
            // ITS 1149724
            if (numInv == 0) {
                numMessages = msgUids.length;
            }
            
            // read subfolders if any
            int numSubs = inStream.readInt();
            subFolders = new Folder[numSubs];
            folderNames = new ArrayList(20);
            for (int i = 0; i < numSubs; i++) {
                Folder f = new Folder(inStream, storeMsgIds);
                folderNames.add(f.getPathName());
                folderNames.addAll(f.getSubFolderNamesList());
                subFolders[i] = f;
            }
        }
    }

    /**
     * Get the folder name
     * 
     * @return folder's path name
     */
    public final String getPathName() {
        return pathname;
    }

    /**
     * Get the names of any subfolders under this folder
     * 
     * @return array of sub folder names
     */
    public final String[] getSubFolderNamesArray() {
        // needs to compile foldernames from subfolders
        // if they exist
        if (folderNames != null) {
            String[] subFolderNames = new String[folderNames.size()];
            folderNames.toArray(subFolderNames);
            return subFolderNames;
        } else {
            return new String[0];
        }
    }

    /**
     * Get the names of any subfolders under this folder
     * 
     * @return arraylist of sub folder names
     */
    protected List getSubFolderNamesList() {
        return folderNames;
    }

    /**
     * Get the UID Validity value for this folder
     * 
     * @return UIDValidity
     */
    public final int getUIDValidity() {
        return UIDValidity;
    }

    /**
     * Get the number of inventories loaded with this folder
     * 
     * @return number of inventories
     */
    public final int getNumInventory() {
        return inventories.length;
    }

    /**
     * Get the various inventories loaded with this folder
     * 
     * @return array of inventories
     */
    public Inventory[] getInventoriesArray() {
        return inventories;
    }

    /**
     * Get the number of subfolders under this folder
     * 
     * @return number of subfolders
     */
    public final int getNumSubfolders() {
        return subFolders.length;
    }

    /**
     * Get the list of subfolders under this folder, if loaded
     * 
     * @return array of sub folders
     */
    public Folder[] getSubFoldersArray() {
        return subFolders;
    }

    /**
     * Get the UIDs of the messages in this folder
     * 
     * @return array of uids
     */
    public final int[] getUids() {
        if (msgUids != null) {
            return msgUids;
        } else {
            return new int[0];
        }
    }

    /**
     * Get the messsage IDs of the messages in this folder
     * 
     * @return array of msgids
     */
    public final String[] getMsgIds() {
        if (msgIds != null) {
            return msgIds;
        } else {
            return new String[0];
        }
    }

    /**
     * Sets the absolute offset for the first message id in the list.
     */
    public final void setMsgIdsOffset(int msgIdOffset) {
        this.msgIdOffset = msgIdOffset;
    }

    /**
     * Get the absolute offset for the first message id in the list.
     * 
     * @return the offset of the first message in the message list
     */
    public final int getMsgIdsOffset() {
        return msgIdOffset;
    }
    
    /**
     * Get the number of messages in this folder
     * 
     * @return number of messages
     */
    public final int getNumMsgs() {
        // compiled from inventories if present
        // otherwise calculated from uid list length
        return numMessages;
    }

    /**
     * Get the number of unread messages in this folder
     * 
     * @return number of unread messages
     */
    public final int getNumUnreadMessages() {
        // compiled from inventories
        return numUnreadMessages;
    }

    /**
     * Get the number of bytes in this folder
     * 
     * @return number of bytes in this folder
     */
    public final long getNumBytes() {
        // compiled from inventories
        return numBytes;
    }

    /**
     * Get the next UID for this folder.
     *
     * @return the UID of the next message to be added to this folder
     */
    public final int getNextUID() {
        return nextUID;
    }

    /**
     * Convert the RME path name as a String array to a single String path name
     * 
     * @param pathIn
     * @return
     */
    private String pathFromArray(String[] pathIn) {
        if (pathIn == null) {
            return null;
        }
        String path = "";
        if (pathIn.length == 0) {
            return path;
        }
        if (pathIn.length == 1 && pathIn[0].equals("")) {
            return "/";
        }
        for (int i = 0; i < pathIn.length - 1; i++) {
            path += pathIn[i] + "/";
        }
        path += pathIn[pathIn.length - 1];
        return path;
    }

    /**
     * for debugging - output all of the folder parameters
     * 
     * @param out a PrintStream to dump the data.
     */
    public void dump(final java.io.PrintStream out) {
        out.println("DUMPING FOLDER " + getPathName());
        out.println("UIDValidity = " + UIDValidity);
        if (inventories != null) {
            out.println("INVENTORIES: " + inventories.length);
            for (int i = 0; i < inventories.length; i++) {
                inventories[i].dump(out);
            }
        }

        out.println("numMessages = " + numMessages);
        out.println("numUnreadMessages = " + numUnreadMessages);
        out.println("numBytes = " + numBytes);
        out.println("MSGUIDS: ");
        if (msgUids != null) {
            for (int i = 0; i < msgUids.length; i++) {
                out.print(msgUids[i] + ", ");
            }
        }
        out.println("");
        out.println("MSGIDS: ");
        if (msgIds != null) {
            for (int i = 0; i < msgIds.length; i++) {
                out.println(msgIds[i] + ", ");
            }
        }
        out.println("");

        if (subFolders != null) {
            out.println("SUBFOLDERS: " + subFolders.length);
            for (int i = 0; i < subFolders.length; i++) {
                subFolders[i].dump(out);
            }
        }

        out.println("==== DONE ==== ");
    }
}

