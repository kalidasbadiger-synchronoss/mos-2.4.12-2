/*
 * Copyright (c) 2005 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/GetMailboxLockState.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This RME operation will get the mailbox lock state.
 * 
 * Note: For some reason the RME returns a string (either "U" or "L"). This code
 * converts those strings back to a boolean (locked=true/false). If, in the
 * future, this operation can have more than two return types, this code will
 * have to be modified.
 */
public class GetMailboxLockState extends AbstractMssOperation {

    // input
    private String name;

    // output
    private boolean isLocked;

    // internal
    private String url;

    private static final String MAILBOX_LOCKED = "L";
    private static final String MAILBOX_UNLOCKED = "U";

    /**
     * Constructor for GetMailboxLockState
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     */
    public GetMailboxLockState(String host, String name,
            final RmeDataModel rmeDataModel) {
        super(rmeDataModel);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_GETMAILBOXLOCKSTATE);
    }

    /**
     * Returns the lock state of the mailbox.
     * 
     * @return true if the mailbox is locked, false otherwise.
     */
    public boolean isLocked() {
        return isLocked;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_GETMAILBOXLOCKSTATE);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_GETMAILBOXLOCKSTATE", host,
                            "dataModel=" + rmeDataModel }));
        }
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        String lockState = inStream.readString();
        if (lockState.equals(MAILBOX_LOCKED)) {
            isLocked = true;
        } else {
            isLocked = false;
        }
        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation is not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation is not supported
    }
}
