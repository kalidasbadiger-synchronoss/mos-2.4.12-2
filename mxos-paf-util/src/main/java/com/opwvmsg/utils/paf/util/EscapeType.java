/*
 *      Copyright (c) 2002-2006 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/EscapeType.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.io.InvalidObjectException;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * This enumeration defines the valid types of escapes
 * that may be performed on content.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class EscapeType implements Serializable {
    private static final long serialVersionUID = 7715071189334011189L;

    public static final EscapeType HDML = new EscapeType("hdml");
    public static final EscapeType HTML = new EscapeType("html");
    public static final EscapeType WML = new EscapeType("wml");
    public static final EscapeType XML = new EscapeType("xml");
    

    /**
     * Set of all defined values.  If this class is
     * modified to include additional values they must be added to
     * this array.  The array order is not significant.
     */
    private static EscapeType[] values = { HDML, HTML, WML, XML };

    /**
     * Map from field names to field values.
     */
    private static Map nameMap = new HashMap();

    static {
        for (int i = 0; i < values.length; i++) {
            nameMap.put(values[i].toString(), values[i]);
        }
    }

    /**
     * Get the instance with a given name, if any.
     *
     * @param name the name
     *
     * @return the instance, or <code>null</code> if there is no
     *    instance with that name.
     */
    public static EscapeType forName(String name) {
        return (EscapeType) nameMap.get(name);
    }


    /**
     * The name.
     */
    private final String name;


    /**
     * Class constructor.
     *
     * @param name the name
     */
    protected EscapeType(String name) {
        this.name = name;
    }


    public final boolean equals(Object obj) {
        return super.equals(obj);
    }

    public final int hashCode() {
        return super.hashCode();
    }

    public String toString() {
        return name;
    }

    /**
     * On deserialization, use the canonical instance.
     */
    protected Object readResolve() throws ObjectStreamException {
        EscapeType instance = (EscapeType) nameMap.get(name);
        if (instance == null) {
            throw new InvalidObjectException("unknown EscapeType instance '" 
                                             + name + "'");
        }
        return instance;
    }
}
