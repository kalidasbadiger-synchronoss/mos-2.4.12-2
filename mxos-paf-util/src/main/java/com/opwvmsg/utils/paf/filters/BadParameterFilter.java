/*
 * Copyright (c) 2006 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/BadParameterFilter.java#1 $
 */
package com.opwvmsg.utils.paf.filters;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.StringTokenizer;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;

/**
 * The BadParameterFilter blocks requests that carry parameters that are
 * suspected of being harmful.  This filter is effective at blocking requests
 * that contain cross site scripting (XSS) attacks.
 *
 * One class of security hole is leveraging request parameters passed via the URL
 * (specifically the get request) to inject script into the system that later
 * is directly rendered.  
 *
 * This filter aids in combating such an attack by denying the entry of certain
 * characters via the get request.  In rare cases, these characters may represent
 * acceptable data, but due to the danger such a hole invites these special cases
 * are handled by double encoding the parameter value.
 *
 * The list of disallowed characters can be set in the "disallowedCharacters" parameter
 * of this filter's configuration.  To avoid possible parsing problems, this list must
 * be encoded.
 *
 * @author Paul Lovvik
 */
public class BadParameterFilter implements Filter {
    private static final Logger logger = Logger.getLogger(BadParameterFilter.class);
    public static final String DISALLOWED_CHARACTERS_KEY = "disallowedCharacters";
    public static final String STRICT_ENCODING_KEY = "strictEncoding";
    public static final String DEFAULT_ENCODING = "UTF-8";

    /**
     * The set of disallowed characters.  These characters are considered to be
     * possibly harmful and must be double encoded to pass through this filter.
     */
    private String[] disallowedCharacters = null;

    /**
     * The set of disallowed characters in encoded form.  These characters are considered to be
     * possibly harmful and must be double encoded to pass through this filter.
     */
    private String[] encodedDisallowedCharacters = null;

    /**
     * If true, all parameters must be encoded.  Any parameter that is not encoded will
     * be rejected.
     */
    private boolean strictEncoding = true;

    /** 
     * The blocked Param string representation
     */
    private String blockedParam = null;
    public void init(FilterConfig fconfig) throws ServletException {
        Config config = Config.getInstance();
        disallowedCharacters = config.get(DISALLOWED_CHARACTERS_KEY,
                "<,>,\"").split(",");
        encodedDisallowedCharacters = new String [disallowedCharacters.length];
        try {
            for (int i = 0; i < this.disallowedCharacters.length; i++) {
                 this.encodedDisallowedCharacters[i] = URLEncoder.encode(
                            this.disallowedCharacters[i], DEFAULT_ENCODING);
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("Failed to Encode disallowed char from config file: ", e);
        } 
        String strictEncoding = fconfig.getInitParameter(STRICT_ENCODING_KEY);
        if (strictEncoding != null) {
            this.strictEncoding = Boolean.valueOf(strictEncoding).booleanValue();
        }
    }

    public void destroy() {

    }

    /**
     * Do the actual filtering work.  This filter works by comparing each parameter
     * value in the request with the re-encoded form of the decoded value.  If it is the
     * same, all is well.  If it is different, a user is possibly pumping instructions
     * into the query string trying to find or exploit a security flaw.
     *
     * Additionally the set of characters configured in the disallowed-characters
     * filter initialization parameter are rejected from all request parameters.  In the
     * case where one of these characters must pass through, the parameter value must be
     * double encoded.
     *
     * @param request the ServletRequest
     * @param response the ServletResponse
     * @param chain the FilterChain
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws ServletException, IOException {
        String encoding = getEncoding(request);
        Enumeration parameters = getRequestParameters((HttpServletRequest)request);
        if (parameters != null) {
            while (parameters.hasMoreElements()) {
                String parameter = (String)parameters.nextElement();
                String value = getParameterValue(parameter);
                if (value == null) {
                    /*
                     * No value - nothing to do
                     */
                    continue;
                }
                try {
                    if (this.strictEncoding && !isValueEncoded(value, encoding)) {
                        String name = getParameterName(parameter);
                        logger.error("The parameter " + name +
                                    " is not encoded.");
                        ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }
                    if (containsDisallowedCharacters(value, encoding)) {
                        /*
                         * The request is not valid.
                         */
                        String name = getParameterName(parameter);
                        logger.error("The parameter " + name +
                                    " contains a disallowed character (" + value + ").  " +
                                     "May need to use double encoding.");
                        ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }
                } catch (UnsupportedEncodingException e) {
                    logger.error("BadParameterFilter.doFilter - invalid encoding " + encoding, e);
                    ((HttpServletResponse)response).sendError(HttpServletResponse.SC_FORBIDDEN);
                    return;
                }
            }
        }
        chain.doFilter(request, response);
    }

    /**
     * Returns a string representation of the encoding.  Normally for HTTP requests this would
     * be UTF-8.  The EncodingFilter detects the decoding and sets it into the request.  If
     * the encoding has been set this method respects that encoding.  Otherwise the "UTF-8"
     * will be used.
     *
     * @param request the ServletRequest
     *
     * @return the encoding
     */
    private static String getEncoding(ServletRequest request) {
        String result = request.getCharacterEncoding();
        if (result == null) {
            result = DEFAULT_ENCODING;
        }
        return (result);
    }

    /**
     * Returns an Enumeration of the request parameters.  Each resulting string contains
     * both the name and value, separated by "=".
     *
     * @param request the request
     *
     * @return the request parameters
     */
    private Enumeration getRequestParameters(HttpServletRequest request) {
        /*
         * Note that request.getParameter decodes the value, so we have to use
         * HttpServletRequest.getQueryString and separate each value manually.
         */
        String queryString = ((HttpServletRequest)request).getQueryString();
        if (queryString != null) {
            StringTokenizer parameters = new StringTokenizer(queryString, "&");
            return (parameters);
        }
        return (null);
    }

    /**
     * Returns the value from the specified parameter string.
     *
     * @param parameter the request parameter string
     *
     * @return the value from the specified parameter string
     */
    private String getParameterValue(String parameter) {
        String value = null;
        int equalsIndex = parameter.indexOf("=");
        if (equalsIndex > 0 && parameter.length() > equalsIndex) {
            value = parameter.substring(equalsIndex + 1);
        }
        return (value);
    }

    /**
     * Returns the name from the specified parameter string.
     *
     * @param parameter the request parameter string
     *
     * @return the name from the specified parameter string
     */
    private String getParameterName(String parameter) {
        String name = null;
        int equalsIndex = parameter.indexOf("=");
        if (equalsIndex > 0) {
            name = parameter.substring(0, equalsIndex);
        }
        return(name);
    }

    /**
     * Returns true if the specified value is encoded with the specified encoding.
     *
     * @param value the value to test
     * @param encoding the encoding
     *
     * @return true if the specified value is encoded, false otherwise
     */
    private boolean isValueEncoded(String value, String encoding)
        throws UnsupportedEncodingException {
        String decodedValue = URLDecoder.decode(value, encoding);
        String encodedValue = URLEncoder.encode(decodedValue, encoding);
        return(encodedValue.equals(value));
    }

    /**
     * Compares the specified parameter value to the list of disallowed characters.
     * If a disallowed character occurs in the value, true is returned.
     *
     * @param value the value to test
     * @param encoding the encoding
     *
     * @return true if the specified value contains at least one disallowed character, false otherwise
     */
    private boolean containsDisallowedCharacters(String value, String encoding)
        throws UnsupportedEncodingException {
        if (this.disallowedCharacters == null) {
            return (false);
        }
        // This is to find out the valid cases to pass
        String decodedValue = URLDecoder.decode(value, encoding);
        for (int i = 0; i < this.encodedDisallowedCharacters.length; i++) {
            if (decodedValue.indexOf(this.encodedDisallowedCharacters[i]) >= 0) {
                return (false);
            }
        } 
        // Continue to find out blocked characters in double decoded string
        decodedValue = URLDecoder.decode(decodedValue, encoding);
        for (int i = 0; i < this.disallowedCharacters.length; i++) {
            if (decodedValue.indexOf(this.disallowedCharacters[i]) >= 0) {
                blockedParam = decodedValue;
                return (true);
            }
        }
        return (false);
    }
}
