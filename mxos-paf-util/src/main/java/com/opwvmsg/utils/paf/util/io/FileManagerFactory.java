/*
 *      FileManagerFactory.java
 *
 *      Copyright 2001 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/io/FileManagerFactory.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util.io;


/**
 * A FileManagerFactory is responsible for generating FileManagers.  File 
 * management constraints are set on the factory that apply to all 
 * FileManagers created by the factory.  FileManagers may have constraints 
 * that are more restrictive than those specified by this the factory.
 *
 * @author Brad Kohn
 * @version $Revision: #1 $ 
 */

public interface FileManagerFactory {
    /**
     * Construct a FileManager with the specified constraints, limited by
     * the constraints on this FileManagerFactory.
     *
     * @param maxAge      The maximum age a file may have, specified in 
     *                    minutes.  If this value is higher than this 
     *                    factory's limit, that value will be used instead.
     * @param threshold   The total size a FileManager's directory needs 
     *                    to be to cause files to be deleted upon the managers
     *                    next cleanup, specified in MB.  If this value is 
     *                    higher than this factory's limit, that value will be
     *                    used instead.
     * @param thresholdMargin The percentage of the threshold at which to 
     *                    stop a cleanup.  This must be a number between 0 
     *                    and 1.  If this value is higher than this factory's 
     *                    limit, that value will be used instead.
     * @param frequency   The frequency is how often the FileManager's thread 
     *                    checks for a cleanup, specified in minutes.  This 
     *                    value must be between this factory's limit range.  
     *                    If not, the closer of those values will be used 
     *                    instead.
     * @param directory   The name of the directory within this manager's root
     *                    directory (not the full path) in which to create 
     *                    managed files.
     * @return            A FileManager with the specified configuration.  If
     *                    a FileManager with the same configuration already
     *                    exists in the directory, that manager will be 
     *                    returned.  If there is a FileManager for that
     *                    directory with a different configuration, an
     *                    IllegalStateException will be thrown.  Otherwise a
     *                    new FileManager will be created and returned.
     * @throws IllegalArgumentException if parameters fall outside of an
     *                    acceptable range.  maxAge must be >=0, threshold
     *                    must be >= 0, thresholdMargin must be between 0 and
     *                    1, and frequency must be > 0.
     * @throws IllegalStateException if there is already a FileManager for
     *                    the directory with different a configuration.
     */
    public FileManager getFileManager(int maxAge, int threshold, 
                         float thresholdMargin, int frequency, 
                         String directory);
}

