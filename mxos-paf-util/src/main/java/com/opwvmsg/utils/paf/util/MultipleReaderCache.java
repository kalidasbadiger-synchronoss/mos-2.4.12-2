/*
 *      Copyright (c) 2003-2004 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/MultipleReaderCache.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * This cache implementation uses read/write locking.  Storage of objects is
 * permanent; they remain in the cache until they are removed
 * or the cache is reset. Nulls are permitted as a key and 
 * as stored objects.  The methods are synchronized for thread-safety
 * using a readers and writers protocol.  Multiple threads may
 * simultaneously call the <code>get</code> method.  If any thread
 * calls <code>put</code>, <code>remove</code>, or <code>reset</code>, 
 * all other threads are blocked until that operation completes.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class MultipleReaderCache implements Cache {

    private ReadWriteLock lock = new WriterPreferenceReadWriteLock();

    /**
     * The map that contains the cached objects.
     */
    private Map cache = new HashMap();


    /**
     * Get an object from the cache.
     *
     * @param key the key for the desired object
     *
     * @return the object, or <code>null</code> if the object is not
     *    in the cache
     *
     * @throws CancelledOperationException if the thread is interrupted
     *   while waiting to acquire a lock
     */
    public Object get(Object key) {
        try {
            lock.readLock().acquire();
            try {
                return cache.get(key);
            } finally {
                lock.readLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Put an object into the cache.
     *
     * @param key the key for the object
     * @param value the object
     *
     * @return the object previously stored in the cache under this
     *    key, or <code>null</code> if there was no such object
     *
     * @throws CancelledOperationException if the thread is interrupted
     *   while waiting to acquire a lock
     */
    public Object put(Object key, Object value) {
        try {
            lock.writeLock().acquire();
            try {
                return cache.put(key, value);
            } finally {
                lock.writeLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Remove an object from the cache.
     *
     * @param key the key for the object to remove
     *
     * @return the removed object, or <code>null</code> if there was
     *    no such object
     *
     * @throws CancelledOperationException if the thread is interrupted
     *   while waiting to acquire a lock
     */
    public Object remove(Object key) {
        try {
            lock.writeLock().acquire();
            try {
                return cache.remove(key);
            } finally {
                lock.writeLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Clear the contents of the cache.
     *
     * @throws CancelledOperationException if the thread is interrupted
     *   while waiting to acquire a lock
     */
    public void clear() {
        try {
            lock.writeLock().acquire();
            try {
                cache.clear();
            } finally {
                lock.writeLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }

    /**
     * Return an unmodifiable view of the current contents.
     *
     * @throws CancelledOperationException if the thread is interrupted
     *   while waiting to acquire a lock
     */
    public Map getMap() {
        try {
            lock.readLock().acquire();
            try {
                return Collections.unmodifiableMap(cache);
            } finally {
                lock.readLock().release();
            }
        } catch (InterruptedException e) {
            throw new CancelledOperationException();
        }
    }
}
