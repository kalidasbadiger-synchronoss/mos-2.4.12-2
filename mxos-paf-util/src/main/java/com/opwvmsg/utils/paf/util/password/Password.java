/*
 * Copyright (c) 2003-2004 Openwave Systems Inc.    All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.    The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/Password.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

/**
 * This interface provides hashing functions for passwords.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */

public interface Password {

    /**
     * Verifies the user-supplied password against the correct password given 
     * its hash type. Returns true if the verification succeeds.
     * <p>
     * The following hash types are supported:
     * <p><ul>
     * <li>clear</li>
     * <li>MD5</li>
     * <li>Unix crypt</li>
     * <li>SHA1</li>
     * <li>SSHA1</li>
     * <li>SHA256</li>
     * <li>SSHA256</li>
     * <li>Custom1-5</li>
     * </ul><p>
     * @param password the correct password
     * @param userPassword the user-supplied password
     * @param pwType the correct password's hash type
     * @return true if verification succeeds
     */
    public boolean checkPassword(String password, String userPassword,
                                 HashType pwType);

    /**
     * Hashes a password according to the given hash type.
     * <p>
     * The following hash types are supported:
     * <p><ul>
     * <li>clear</li>
     * <li>MD5</li>
     * <li>Unix crypt</li>
     * <li>SHA1</li>
     * <li>SSHA1</li>
     * <li>SHA256</li>
     * <li>SSHA256</li>
     * <li>Custom1-5</li>
     * </ul><p>
     * @param password the password
     * @param pwType the desired hash type
     * @return the hashed password
     * @throws PasswordException if anything goes wrong with the operation
     */
    public String hashPassword(String password, HashType pwType)
         throws PasswordException;
}
