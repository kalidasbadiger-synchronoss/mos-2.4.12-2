/*
 *
 *      Copyright (c) 2002-2003 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/log4j/InitServlet.java#1 $
 *
 */

package com.opwvmsg.utils.paf.log4j;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;

import org.apache.log4j.xml.DOMConfigurator;


/**
 * This servlet is used as an initializer for the log4j logging
 * system.  The configuration for 
 * logging is specified in an XML file that is named by the servlet
 * parameter "config" or located in the default location 
 * "/WEB-INF/log4j-config.xml".  To make use of this servlet, 
 * an application will define the servlet in its deployment descriptor,
 * including the value of the init parameter, if needed.
 * The servlet must be loaded prior to any use of log4j.  This
 * is typically achieved by defining a load on startup value
 * in the deployment descriptor that is smaller than that for any
 * servlets that use log4j or depend on other packages that use it.
 * For example:
 * <pre>
 * &lt;servlet&gt;
 *   &lt;servlet-name&gt;log4jinit&lt;/servlet-name&gt;
 *   &lt;servlet-class&gt;com.openwave.paf.log4j.InitServlet&lt;/servlet-class&gt;
 *   &lt;init-param&gt;
 *     &lt;param-name&gt;config&lt;/param-name&gt;
 *     &lt;param-value&gt;/WEB-INF/log4j-config.xml&lt;/param-value&gt;
 *   &lt;/init-param&gt;
 *
 *   &lt;load-on-startup&gt;1&lt;/load-on-startup&gt;
 * &lt;/servlet&gt;
 * </pre>
 *
 * <p>The servlet does not do any request processing and there is
 * no need to define a servlet mapping for it.
 *
 * @author Mark Abbott
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public class InitServlet extends HttpServlet {

    private static final String CONFIG_PARAM = "config";
    private static final String DEFAULT_CONFIG_FILE = 
        File.separator + "WEB-INF" + File.separator + "log4j-config.xml";

    /**
     * During servlet initialization, configure the log4j system
     * using the file specified in a context parameter, or using
     * a default file.
     *
     */
    public void init() {
        ServletContext context = getServletContext();
        String config = getInitParameter(CONFIG_PARAM);
        if (config == null) {
            config = DEFAULT_CONFIG_FILE;
        } else {
            config = config.replace('/', File.separatorChar);
        }
        String base = context.getRealPath("/");
        if (! base.endsWith(File.separator)) {
            base = base + File.separator;
        }
        DOMConfigurator.configure(base + config);
        ServletContextAppender.setServletContext(context);
    }
}
