/*
 *      Service.java
 *
 *      Copyright (c) 2002 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Service.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;


/**
 * This class manages the registration of service providers for
 * provider-based APIs.  Providers are located by searching in
 * the classpath of the current class loader for all files with
 * the same name as the <code>Class</code> of the desired
 * service and located in the classpath-relative directory
 * META-INF/services.  These are called service configuration files.
 *
 * <p>Each line of such a file is expected to contain the 
 * fully qualified class name of a single concrete class that 
 * implements the interface defined by the service.  Instances 
 * of all classes in all matching service configuration files are
 * returned in an <code>Iterator</code>.  The order of iteration
 * is undefined.
 *
 * <p>If the same concrete class is named in more than one
 * configuration file, it is only returned once.
 *
 * <p>Configuration files must be written in the UTF-8 encoding.
 * The '#' character is considered a comment character; it and
 * all subsequent characters on its line are ignored.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public final class Service {

    private static final String SERVICE_PATH = "META-INF/services/";


    /**
     * Get an iterator over the providers available for a service
     * from the current class loader.  Each call to
     * <code>Iterator.next</code> returns an instance of the next available
     * provider for the service.  There is no defined order to the 
     * returned providers.
     *
     * @param service the <code>Class</code> of the service to locate
     */
    public static Iterator providers(Class service) throws ServiceException {
        return new ProviderIterator(service);
    }


    private Service() {
    }


    /**
     * An iterator over the providers available for a service
     * from the current class loader.  Each call to
     * <code>next</code> returns an instance of the next available
     * provider for the service.
     */
    private static class ProviderIterator implements Iterator {

        /**
         * The service for which the providers are desired.
         */
        private Class service;

        /**
         * The ClassLoader to use for searching for provider configuration
         * files.
         */
        private ClassLoader loader;

        /**
         * The set of provider classes that have been seen in any
         * configuration file.
         */
        private Set seen = new HashSet();

        /**
         * An enumeration of provider configuration files available
         * on the classpath for a given service.
         */
        private Enumeration configs = null;

        /**
         * An iterator over the providers in a provider configuration file.
         */
        private Iterator providers = null;

        /**
         * The name of the class of the next provider to return.
         */
        private String nextProvider = null;


        /**
         * Create an instance for the specified service.
         *
         * @param service the <code>Class</code> of the service to locate
         */
        private ProviderIterator(Class service) {
            this.service = service;
            loader = Thread.currentThread().getContextClassLoader();
        }


        /**
         * Returns <code>true</code> if the iteration has more elements.
         *
         * @return <code>true</code> if the iteration has more elements
         * 
         * @throws ServiceException if a provider configuration file
         *    has an invalid syntax, or if an I/O error occurs during
         *    reading 
         */
        public boolean hasNext() throws ServiceException {
            if (nextProvider != null) {
                return true;
            }
            if (configs == null) {
                try {
                    String configPath = SERVICE_PATH + service.getName();
                    configs = loader.getResources(configPath);
                } catch (IOException e) {
                    fail(service, ": " + e);
                }
            }
            while (providers == null || !providers.hasNext()) {
                if (!configs.hasMoreElements()) {
                    return false;
                }
                providers = 
                    parseConfig(service, (URL)configs.nextElement());
            }
            nextProvider = (String)providers.next();

            return true;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next element in the iteration
         *
         * @throws ServiceException
         */
        public Object next() throws ServiceException {
            if (!hasNext()) {
                throw new NoSuchElementException();
            }
            String providerName = nextProvider;
            nextProvider = null;
            try {
                return Class.forName(providerName, true, loader).newInstance();
            } catch (ClassNotFoundException e) {
                fail(service, "Provider " + providerName + " not found");
            } catch (Exception e) {
                fail(service, "Provider " + providerName 
                              + " could not be instantiated: " + e);
            }
            // not reached
            return null;
        }

        /**
         * Removal of elements is not supported by this iterator.
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }

        /**
         * Parse a provider configuration file.
         *
         * @param service the <code>Class</code> of the service to locate
         * @param url the URL of the configuration file
         *
         * @return an <code>Iterator</code> over the class names contained
         *    in the config file
         *
         * @throws ServiceException if an I/O error occurs while reading 
         *     the configuration file or if the file has an invalid format
         */
        private Iterator parseConfig(Class service, URL url)
                throws ServiceException {
            InputStream stream = null;
            BufferedReader reader = null;
            List names = new ArrayList();
            try {
                stream = url.openStream();
                reader = new BufferedReader(new InputStreamReader(stream, 
                                                                  "utf-8"));
                String line = null;
                while ((line = reader.readLine()) != null) {
                    int index = line.indexOf('#');
                    if (index >= 0) {
                        line = line.substring(0, index);
                    }
                    line = line.trim();
                    if (line.length() > 0 && !seen.contains(line)) {
                        names.add(line);
                        seen.add(line);
                    }
                }
            } catch (IOException e) {
                fail(service, ": " + e);
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                    if (stream != null) {
                        stream.close();
                    }
                } catch (IOException e) {
                    fail(service, ": " + e);
                }
            }
            return names.iterator();
        }

        /**
         * Format an error message and throw a ServiceException.
         *
         * @param service the service class
         * @param msg the error message
         *
         * @throws ServiceException always
         */
        private void fail(Class service, String msg)
                throws ServiceException {
            throw new ServiceException(service.getName() + ": " + msg);
        }
    }
}
