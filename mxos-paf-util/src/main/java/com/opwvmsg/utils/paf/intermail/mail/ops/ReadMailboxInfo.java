/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.FolderInfo;
import com.opwvmsg.utils.paf.intermail.mail.MailboxInfo;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * This operation will read the advanced data for an Intermail folder
 * 
 * @author mxos-dev
 */
public class ReadMailboxInfo extends AbstractMssOperation {

    // input
    private String name;
    private int accessId;
    private boolean ignorePopDeleted;

    // output
    private int mailboxStatus;
    private MailboxInfo[] mailboxInfo;
    private FolderInfo[] folderInfo;

    // internal
    private String url;
    private int numMailboxInfo;
    private int numFolderInfo;
    private String hostHeader;

    /**
     * Constructor for ReadMailboxInfo
     * 
     * @param host The MSS hostname for the user
     * @param name The MSS id (MailboxId) of the user
     * @param realm The mailRealm of the user
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param ignorePopDeleted This is boolean value false - Gets all messages
     *            including popDeleted Messages true - Do not get messages
     *            marked as popDeleted
     */
    public ReadMailboxInfo(String host, String name, String realm,
            int accessId, boolean ignorePopDeleted, boolean isHttpEnabledForOp) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_MAILBOXINFO);
        this.accessId = accessId;
        this.ignorePopDeleted = ignorePopDeleted;
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        // check args
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        callRme(MSS_SL_MAILBOXINFO);
    }

    // RME FUNCTIONS
    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        // Legacy RME operation is not supported
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        // debugReceive();
        mailboxStatus = inStream.readInt();

        numMailboxInfo = inStream.readInt();
        mailboxInfo = new MailboxInfo[numMailboxInfo];
        if (numMailboxInfo > 0) {
            for (int i = 0; i < numMailboxInfo; i++) {
                mailboxInfo[i] = new MailboxInfo(inStream);
            }
        }

        numFolderInfo = inStream.readInt();
        folderInfo = new FolderInfo[numFolderInfo];
        if (numFolderInfo > 0) {
            for (int i = 0; i < numFolderInfo; i++) {
                folderInfo[i] = new FolderInfo(inStream);
            }
        }
        logEvents = inStream.readLogEvent();
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_MAILBOXINFO));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        // URL
        outStream.writeString(url);
        outStream.writeInt(accessId);
        outStream.writeBoolean(ignorePopDeleted);
        outStream.flush();
    }

    /**
     * @return the mailboxStatus
     */
    public int getMailboxStatus() {
        return mailboxStatus;
    }

    /**
     * @return the mailboxInfo
     */
    public MailboxInfo[] getMailboxInfo() {
        return mailboxInfo;
    }

    /**
     * @return the folderInfo
     */
    public FolderInfo[] getFolderInfo() {
        return folderInfo;
    }
}
