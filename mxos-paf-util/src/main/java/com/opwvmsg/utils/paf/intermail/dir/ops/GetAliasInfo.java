/*
 * Copyright (c) 2005-2006 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/dir/ops/GetAliasInfo.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.dir.ops;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.opwvmsg.utils.paf.intermail.dir.AbstractDirectoryOperation;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

/**
 * RME operation to get the user's aliases
 */
public class GetAliasInfo extends AbstractDirectoryOperation {

    // input
    private String smtpAddress = null;
    
    // output
    private String[] aliases = new String[0]; 
    private int returnStatus = 0;
    
    /**
     * Constructor that takes an SMTP address and creates a request that will
     * look up that user's aliases
     * 
     * @param smtpAddress the user's SMTP address
     */
    public GetAliasInfo(String smtpAddress) {
        this.smtpAddress = smtpAddress;    
    }
    
    /* (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (smtpAddress == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg",
                        new String[] { "smtpAddress", "GetAliasInfo" }));
        }

        try {
            if (smtpAddress.getBytes("UTF-8").length > MAX_USERNAME_LENGTH) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Rme.ClientParmLengthError",
                                new String[] { "smtpAddress", "getAliasInfo",
                                "S", smtpAddress}));            
            }
        } catch (UnsupportedEncodingException ignore) { // never happen
        }

        callRme(DIR_P_GETALIASINFO);
    }
   
    /**
     * Get the array of alias addresses assigned to this user
     * 
     * @return list of alias addresses.
     */
    public String[] getAliases(){
        return aliases;
    }
    
    /**
     * Get the return code associated with the request
     *
     * @return and integer code
     */
    public int getReturnCode() {
        return returnStatus;
    }


    /* (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeString(smtpAddress);
        outStream.writeInt(ReadUserInfoAuth.SMTPQUERY);
    }
    
    /* (non-Javadoc)
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        if (inStream.readBoolean()) { // is reply null?
            returnStatus = inStream.readInt();
            aliases = inStream.readStringArray();
        }
    }

	@Override
	protected void constructHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}

}
