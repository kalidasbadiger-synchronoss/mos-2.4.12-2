/*
 * Copyright (c) 2005-2006 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/dir/ops/GetForwardInfo.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.dir.ops;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import com.opwvmsg.utils.paf.intermail.dir.AbstractDirectoryOperation;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;

/**
 * RME operation to get user's forwarding addresses
 */
public class GetForwardInfo extends AbstractDirectoryOperation {

    // input
    private String smtpAddress = null;

    // output
    private String[] forwardingAddresses = new String[0];
    private int returnStatus = 0;

    /**
     * Constructor that takes an SMTP address and creates a request that will
     * look up that user's forwards
     * 
     * @param smtpAddress the user's SMTP address
     */
    public GetForwardInfo(String smtpAddress) {
        this.smtpAddress = smtpAddress;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (smtpAddress == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "smtpAddress", "GetForwardInfo" }));
        }
        
        try {
            if (smtpAddress.getBytes("UTF-8").length > MAX_USERNAME_LENGTH) {
                throw new IntermailException(LogEvent.ERROR,
                        LogEvent.formatLogString("Rme.ClientParmLengthError",
                                new String[] { "smtpAddress", "getForwardInfo",
                                "S", smtpAddress}));            
            }
        } catch (UnsupportedEncodingException ignore) { // never happen
        }
        
        callRme(DIR_P_GETFWDINFO);

    }

    /**
     * Get the array of forwarding addresses assigned to this user
     * 
     * @return list of forwarding addresses.
     */
    public String[] getForwardingAddresses() {
        return forwardingAddresses;
    }

    /**
     * Get the return code associated with the request
     *
     * @return and integer code
     */
    public int getReturnCode() {
        return returnStatus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(1); // always 1 request
        outStream.writeBoolean(true); // first request != null
        outStream.writeString(smtpAddress);
        outStream.writeInt(ReadUserInfoAuth.SMTPQUERY);
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.rme.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        inStream.readInt(); // this is always 1 reply
        if (inStream.readBoolean()) { // is reply null?
            int returnStatus = inStream.readInt();
            forwardingAddresses = inStream.readStringArray();
        }
        logEvents = inStream.readLogEvent();
    }

	@Override
	protected void constructHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.openwave.intermail.AbstractOperation#getConnection()
	 */
	protected void createHttpClient(boolean usePool, int port)
			throws IntermailException {
	}

	@Override
	protected void receiveHttpData() throws IOException {
		// TODO Auto-generated method stub
		
	}
}