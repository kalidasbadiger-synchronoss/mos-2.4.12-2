/*
 *
 *      Copyright 2001 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/io/FileManagerImpl.java#1 $
 *
 */

package com.opwvmsg.utils.paf.util.io;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * FileManagerImpl is a simple implementation of the FileManager interface.
 * <p>
 * A FileManager is responsible for creating files and automatically deleting
 * them later.  At a specified frequency the manager will check each file in
 * its directory to see if it should be deleted.  
 * <p>
 * Files will be deleted when:
 * <dl>
 * <dt>a file exceeds a specified age</dt>
 * <dd>A file's age is determined by how recently the file was used, not how 
 * long the file has existed.  How recently a file was used is calculated 
 * based on the information available to the manager.  This includes the 
 * File's most recent modification time as well as the time of the last call 
 * to getManagedFile.  Note that a file that is read from, but never written 
 * to, will eventually exceed the maximum age.  In this case use 
 * getManagedFile periodically to keep the file around.</dd>
 * <dt>the space used by the manager's files exceeds a threshold</dt>
 * <dd>Once the space exceeds the threshold, files will be deleted, oldest 
 * first, until the total space falls below a certain percentage of the 
 * threshold.  This percentage is known as the thresholdMargin.</dd>
 * </dl>
 * <p>
 * Note that the manager does not guarantee space will be freed upon cleanup 
 * due to the possibility that Files may still be held open.
 *
 * @author Brad Kohn
 * @version $Revision: #1 $ 
 */

class FileManagerImpl implements FileManager, Runnable {
    private final int maxAge;
    private final int threshold;
    private final float thresholdMargin;
    private final int frequency;
    private final File directory;

    /**
     * Construct a new file manager
     *
     * @param maxAge      The maximum age a file may have, specified in 
     *                    minutes.
     * @param threshold   The total size a FileManager's directory needs 
     *                    to be to cause files to be deleted upon the managers
     *                    next cleanup, specified in MB.
     * @param thresholdMargin The percentage of the threshold at which to 
     *                    stop a cleanup.  This must be a number between 0 
     *                    and 1.
     * @param frequency   The frequency is how often the ThreadManager's 
     *                    thread checks for a cleanup, specified in minutes.
     * @param directory   The directory in which to create managed files.
     */
    protected FileManagerImpl(int maxAge, int threshold, float thresholdMargin,
                              int frequency, File directory) {
        this.maxAge = maxAge;
        this.threshold = threshold;
        this.thresholdMargin = thresholdMargin;
        this.frequency = frequency;
        this.directory = directory;
    }
    
    /**
     * Run the cleanup thread.
     */
    public void run() {
        try {
            while (!Thread.interrupted()) {
                Thread.sleep(frequency * 60 * 1000);
                cleanup();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    /**
     * Get the maximum age constraint for a managed file.
     *
     * @return            the maximum age constraint for a managed file
     */
    public int getMaxAge() {
        return maxAge;
    }

    /**
     * Get this manager's size threshold.
     *
     * @return            the size threshold
     */
    public int getThreshold() {
        return threshold;
    }

    /**
     * Get this manager's threshold margin.
     *
     * @return            the threshold margin
     */
    public float getThresholdMargin() {
        return thresholdMargin;
    }
    
    /**
     * Get this manager's cleanup frequency.
     *
     * @return            the cleanup frequency
     */
    public int getFrequency() {
        return frequency;
    }
    
    /**
     * Get the name of this manager's directory.
     *
     * @return            this manager's directory
     */
    public String getDirectory() {
        return directory.getName();
    }

    /**
     * Create a new managed file.  This file will have an automatically
     * generated unique name with a specified prefix and suffix (that must 
     * follow the rules defined by File.createTempFile).
     *
     * @param prefix      The prefix string to be used in generating the 
     *                    file's name.  It must be at least three characters 
     *                    long.
     * @param suffix      The suffix string to be used in generating the 
     *                    file's name.  It may be null, in which case the 
     *                    suffix ".tmp" will be used.
     * @return            the new File
     * @throws IOException if a File could not be created
     */
    public File createManagedFile(String prefix, String suffix) 
        throws IOException {

        return File.createTempFile(prefix, suffix, directory);
    }

    /**
     * Get an already existing managed file.  This will also extend the 
     * lifetime of the file by setting its modification time to the current  
     * time.
     *
     * @param name        The name of the managed file
     * @return            The File or <code>null</code> if it does not exist
     */
    public File getManagedFile(String name) {
        File file = new File(directory, name);
        if (file.exists()) {
            file.setLastModified(System.currentTimeMillis());
            return file;
        }
        return null;
    }

    /**
     * Immediately delete all managed files.
     */
    public void deleteAll() {
        File[] files = getAll();
        for (int i = 0; i < files.length; i++) {
            files[i].delete();
        }
    }

    /**
     * Immediately delete all managed files starting with the specified 
     * prefix.
     */
    public void deleteAllByPrefix(final String prefix) {
        File[] files = getAllByPrefix(prefix);
        for (int i = 0; i < files.length; i++) {
            files[i].delete();
        }
    }

    /**
     * Immediately delete all managed files ending with the specified suffix.
     */
    public void deleteAllBySuffix(final String suffix) {
        File[] files = getAllBySuffix(suffix);
        for (int i = 0; i < files.length; i++) {
            files[i].delete();
        }
    }

    /**
     * Return all managed files.
     */
    public File[] getAll() {
        return directory.listFiles();
    }

    /**
     * Return all managed files starting with the specified prefix.
     */
    public File[] getAllByPrefix(final String prefix) {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.startsWith(prefix);
            }
        };
        return directory.listFiles(filter);
    }

    /**
     * Return all managed files ending with the specified suffix.
     */
    public File[] getAllBySuffix(final String suffix) {
        FilenameFilter filter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(suffix);
            }
        };
        return directory.listFiles(filter);
    }

    /**
     * Perform this manager's periodic cleanup.  First, all files in this
     * manager's directory that are older than the specified maximum age are
     * deleted.  Then, if the total size of the remaining files exceeds this
     * manager's threshold, files will be deleted, least recently modified 
     * first, until the total size reaches the percentage of the threshold 
     * specified by the manager's threshold margin.  If two files were last
     * modified at the same time, the files will be deleted in alphabetical
     * order.
     */
     protected void cleanup() {
        File[] files = directory.listFiles();
        if (files != null) {
            TreeSet oldestFiles = new TreeSet(new FileAgeComparator());
            long totalBytes = 0;
            int maxAgeMS = maxAge * 60 * 1000;
            long currentMS = System.currentTimeMillis();
            long oldestFile = currentMS - maxAgeMS;

            for (int i = 0; i < files.length; i++) {
                if (files[i].lastModified() >= oldestFile
                        || !files[i].delete()) {
                    totalBytes += files[i].length();
                    oldestFiles.add(files[i]);
                }
            }

            long thresholdBytes = (long)threshold * 1024 * 1024;

            if (totalBytes > thresholdBytes) {
                Iterator iter = oldestFiles.iterator();
                while (totalBytes > thresholdBytes * thresholdMargin && 
                       iter.hasNext()) {

                    File next = (File)iter.next();
                    long length = next.length();
                    if (next.delete()) {
                        totalBytes -= length;
                    }
                }
            }
        }
    }

    /**
     * Comparator to compare files based on their last modified time, least
     * recently before most recently.
     */
    private class FileAgeComparator implements Comparator{
        /**
         * Compare two files' last modified time and return -1 or 1 depending
         * on if the first file was modified before or after the second one.
         * If the two files were modified at the same time, the files' names
         * are compared using File.compareTo.
         */
        public int compare(Object o1, Object o2) {
            File f1 = (File)o1;
            File f2 = (File)o2;
            long difference = f1.lastModified() - f2.lastModified();
            if (difference == 0) {
                return f1.compareTo(f2);
            }
            return difference < 0 ? -1 : 1;
        }
    }
}
