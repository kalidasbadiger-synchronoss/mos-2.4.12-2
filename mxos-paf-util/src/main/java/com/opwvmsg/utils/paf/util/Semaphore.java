/*
 * Semaphore.java
 *
 * Copyright (c) 2006 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/Semaphore.java#1 $
 */

package com.opwvmsg.utils.paf.util;

/**
 * This is a standard implementation of a semaphore.
 * <p>
 * Copied with permission from Concurrent Programming in Java's website at
 * http://gee.cs.oswego.edu/dl/cpj/
 */
public class Semaphore {

    /**
     * Current number of available permits
     */
    private int permits;

    /**
     * @param initialPermits the initial number of permits for this Semaphore
     */
    public Semaphore(int initialPermits) { 
        permits = initialPermits; 
    }

    /**
     * Increment the number of available permits and notify a thread that is
     * waiting to acquire.
     */
    public synchronized void release() {
        ++permits;
        notify();
    }

    /**
     * If there are permits available (permits>0), decrement the number and
     * return.  Otherwise wait for a release.
     *
     * @throws InterruptedException if the active thread is already
     *            interrupted or becomes interrupted while waiting for the
     *            release
     */
    public void acquire() throws InterruptedException {
        if (Thread.interrupted())  {
            throw new InterruptedException();
        }
        synchronized (this) {
            try {
                while (permits <= 0) {
                    wait();
                }
                --permits;
            } catch (InterruptedException ie) {
                notify();
                throw ie;
            }
        }
    }

    /**
     * This method is the same as acquire, but with a timeout.
     *
     * @param msecs the number of milliseconds to wait
     * @return true if this Semaphore was acquired before time ran out
     * @throws InterruptedException if the active thread is already
     *            interrupted or becomes interrupted while waiting for the
     *            release
     */
    public boolean attempt(long msecs)throws InterruptedException{
        if (Thread.interrupted()) {
            throw new InterruptedException();
        }
        synchronized (this) {
            if (permits > 0) {     // Same as acquire but messier
                --permits;
                return true;
            } else if (msecs <= 0) {  // avoid timed wait if not needed
                return false;
            } else {
                try {
                    long startTime = System.currentTimeMillis();
                    long waitTime = msecs;

                    while (true) {
                        wait(waitTime);
                        if (permits > 0) {
                            --permits;
                            return true;
                        } else {      // Check for time-out
                            long now = System.currentTimeMillis();
                            waitTime = msecs - (now - startTime);
                            if (waitTime <= 0) {
                                return false;
                            }
                        }
                    }
                } catch(InterruptedException ie) { 
                    notify();
                    throw ie;
                }
            }
        }
    }
}
