/*
 * Copyright (c) 2005-2011 Openwave Systems Inc. All rights reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id: //paf/paf31-903-1105-108-1/util/src/com/openwave/intermail/mail/MssHostInfo.java#1 $
 *  
 */
package com.opwvmsg.utils.paf.intermail.mail;

/**
 * pojo class for holding information about mss host 
 * @author nitesh_kumar
 *
 */
public class MssHostInfo {

    // mss host name
    private String hostName;
    
    //cluster Name with which particular host belongs 
    private String clusterName;
    
    // minimum range of host for bucket 
    private int min;
    
    //maximum range of host for bucket 	
    private int max;
    
    //Current status of host
    private boolean isRunning;
    
    public MssHostInfo(String hostName, String clusterName, int min, int max, boolean isRunning) {
        this.hostName = hostName;
        this.clusterName = clusterName;
        this.min = min;
        this.max = max;
        this.isRunning = isRunning;
    }
    
    
    /**
     * @return the hostName
     */
    public String getHostName() {
        return hostName;
    }

    public String getClusterName() {
		return clusterName;
	}

	/**
     * @return the min
     */
    public int getMin() {
        return min;
    }

    /**
     * @return the max
     */
    public int getMax() {
        return max;
    }

    /**
     * @return the status
     */
    public boolean isRunning() {
        return isRunning;
    }


    /**
     * @param status the status to set
     */
    public void setRunning(boolean status) {
        this.isRunning = status;
    }

	public String toString() {
		return "MssHostInfo [hostName=" + hostName + ", clusterName="
				+ clusterName + ", min=" + min + ", max=" + max + ", isRunning="
				+ isRunning + "]";
	}

}
