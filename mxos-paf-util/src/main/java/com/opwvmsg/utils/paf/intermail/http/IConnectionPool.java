/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.utils.paf.intermail.http;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;

/**
 * Connection pool interface.
 * 
 * @param <T>
 *            This describes Connection type.
 * @author mxos-dev
 */
public interface IConnectionPool<T> {

	/**
	 * Method to get Connection object.
	 * 
	 * @return Connection Object
	 * @throws MxOSException
	 */
	T borrowObject() throws IntermailException;

	/**
	 * Method to return Connection object back to the pool.
	 * 
	 * @param Connection
	 *            Object
	 * @throws MxOSException
	 */
	void returnObject(T HttpRmeClient) throws IntermailException;
}