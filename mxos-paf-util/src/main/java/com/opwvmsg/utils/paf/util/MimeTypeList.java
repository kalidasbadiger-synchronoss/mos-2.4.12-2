/*
 * Copyright (c) 2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/MimeTypeList.java#1 $
 */
package com.opwvmsg.utils.paf.util;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;

/**
 * Immutable list of MIME types that can be represented as an array of
 * strings or a comma-delimited list.
 */
public class MimeTypeList {
    private static final Logger log = Logger.getLogger(MimeTypeList.class);

    private static final ContentType[] NO_CONTENT_TYPES = { };
    private ContentType[] types = { };

    /**
     * Constructs an empty MIME type list.
     */
    public MimeTypeList() {
    }

    /**
     * Constructs a MIME type list from a comma-separated list.
     * MIME types are trimmed, so spaces may be present.
     */
    public MimeTypeList(String commaList) {
        this(StringUtils.split(commaList, ","));
    }

    /**
     * Constructs a MIME type list from an array of content types.
     */
    public MimeTypeList(ContentType[] list) {
        this.types = new ContentType[list.length];
        System.arraycopy(list, 0, this.types, 0, list.length);
    }

    /**
     * Constructs a MIME type list from an array of types.
     */
    public MimeTypeList(String[] list) {
        types = new ContentType[list.length];
        for (int i = 0; i < types.length; ++i) {
            String type = list[i].trim();
            try {
                types[i] = new ContentType(type);
            } catch (ParseException exc) {
                log.warn("Cannot process MIME type " + type);
                try {
                    types[i] = new ContentType("application/octet-stream");
                } catch (ParseException ignore) {
                    // Can't happen
                }
            }
        }
    }

    /**
     * Constructs a MIME type list from a list of type strings.
     */
    public MimeTypeList(List list) {
        this((String[])list.toArray(new String[list.size()]));
    }

    /**
     * Returns a new MIME type list as the combination of this one and another.
     */
    public MimeTypeList mergeWith(MimeTypeList other) {
        ContentType[] merged = new ContentType[this.types.length + other.types.length];
        System.arraycopy(this.types, 0, merged, 0, this.types.length);
        System.arraycopy(other.types, 0, merged, this.types.length, other.types.length);
        return new MimeTypeList(merged);
    }

    /**
     * Returns true if a type is matched by a type in this list.
     */
    public boolean matches(String mimeType) {
        for (int i = 0; i < types.length; ++i) {
            if (types[i].match(mimeType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns true if a type is matched by a type in this list.
     */
    public boolean matches(ContentType contentType) {
        for (int i = 0; i < types.length; ++i) {
            if (types[i].match(contentType)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns a new MIME type list filtered to include types found in
     * this list and the <tt>filterBy</tt> list. Assumes the filtering
     * list is the more general one, that is, filterBy.match(this) is
     * used so that the filter can contain, say, "image/*" when this
     * lists contains "image/gif,image/jpeg".
     */
    public MimeTypeList filter(MimeTypeList filterBy) {
        List /* ContentType */ matches = new ArrayList();
        for (int i = 0; i < this.types.length; ++i) {
            for (int j = 0; j < filterBy.types.length; ++j) {
                if (filterBy.types[j].match(this.types[i])) {
                    matches.add(this.types[i]);
                    break;
                }
            }
        }
        return new MimeTypeList((ContentType[])matches.toArray(NO_CONTENT_TYPES));
    }

    /**
     * Returns the list as an array of strings.
     */
    public String[] asArray() {
        String[] result = new String[types.length];
        for (int i = 0; i < types.length; ++i) {
            result[i] = types[i].toString();
        }
        return result;
    }

    /**
     * Returns an iterator of types as Strings.
     */
    public Iterator iterator() {
        return Arrays.asList(asArray()).iterator();
    }

    /**
     * Returns the list as a comma-separated list with no spaces.
     */
    public String asCommaList() {
        return asString(",");
    }

    /**
     * Returns the list as a list separated by any string.
     */
    public String asString(String sep) {
        return StringUtils.join(asArray(), sep);
    }

    /**
     * Returns an array of extensions corresponding to the MIME types.
     * Duplicate extensions are removed.
     */
    public String[] getExtensionArray() {
        TreeSet extSet = new TreeSet();
        for (int i = 0; i < types.length; ++i) {
            extSet.addAll(Arrays.asList(MimeExtension.getAllExtensions(
                                            types[i].getBaseType())));
        }
        return (String[])extSet.toArray(new String[extSet.size()]);
    }

    /**
     * Returns a comma-separated string of extensions corresponding to the MIME types.
     */
    public String getExtensions() {
        return StringUtils.join(getExtensionArray(), ", ");
    }

    public String toString() {
        return asString(", ");
    }
}
