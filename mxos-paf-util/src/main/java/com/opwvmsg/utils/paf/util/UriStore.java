/*
 * Copyright (c) 2003 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/UriStore.java#1 $
 */
package com.opwvmsg.utils.paf.util;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpSession;

/**
 * Synchronized cache for storing URI cache-invalidation values. <p>
 *
 * The internal map contains URIs as keys and a value object that is added
 * to the URI as a request parameter. When the URI is invalidated, the
 * update method is called and the value modified. This ensures the next
 * time the URI is used in a request, the full URI is unique and the page
 * is reloaded.
 *
 * @author Paul Linden
 * @version $Revision: #1 $
 */
public class UriStore implements Serializable {
    /**
     * The map that contains the cached URI key/value pairs.
     */
    private Map cache = new HashMap();

    /**
     * Gets instance of UriStore. Checks if one exists already in session and
     * creates one if not.
     * 
     * @param session current HTTP session
     * @param create specifies whether to create an instance if one doesn't
     *               exist
     * @return an instance of the cache
     */
    public static UriStore getInstance(HttpSession session, boolean create) {

        String PAF_CACHE = "paf_uriCaching";
        UriStore store = null;

        store = (UriStore) session.getAttribute(PAF_CACHE);
        if (store == null && create) {
            store = new UriStore();
            session.setAttribute(PAF_CACHE, store);
        }
        return store;
    }

    /**
     * Updates value specified by key. The value is guaranteed to be
     * different after this method is called.
     *
     * @param key cache entry to be modified
     */
    public synchronized void update(Object key) {
        Integer value = (Integer) get(key);
        if (value == null) {
            value = new Integer((new Random()).nextInt(Integer.MAX_VALUE));
        } else {
            value = new Integer(value.intValue() + 1);
        }
        cache.put(key, value);
    }

    /**
     * Gets the value Object from the URI cache.
     * 
     * @param key the key of the object
     * @return the object stored in the cache under this key
     */
    public synchronized Object get(Object key) {
        return cache.get(key);
    }

    /**
     * Remove an object from the cache.
     *
     * @param key the key for the object to remove
     * @return the removed object, or <code>null</code> if there was
     *    no such object
     */
    public synchronized Object remove(Object key) {
        return cache.remove(key);
    }
    
    /**
     * Clear the contents of the cache.
     */
    public synchronized void clear() {
        cache.clear();
    }
}
