/*
 * Copyright (c) 2003 Openwave Systems Inc.  All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc.  The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/password/PasswordFactory.java#1 $
 */

package com.opwvmsg.utils.paf.util.password;

import java.util.Iterator;
import java.util.Map;
import java.util.Hashtable;

import com.opwvmsg.utils.paf.util.Service;
import com.opwvmsg.utils.paf.util.password.PasswordException;
import com.opwvmsg.utils.paf.util.password.spi.PasswordProvider;

/**
 * This factory class produces implementations of <code>Password</code>.
 * The choice of implementation is made by selecting the type of provider to
 * be used. The first provider that can be found that claims it can
 * handle the given type will be used.
 * <p>
 * This class is immutable if the provider class for the configured
 * provider is immutable, which is usually the case.
 *
 * @author Conrad Damon
 * @version $Revision: #1 $
 */
public class PasswordFactory {

    private String type;

    private PasswordProvider provider = null;
    private static Map foundClasses = new Hashtable();

    /**
     * Construct a new <code>PasswordFactory</code>.
     *
     * @param type the type of password provider to use
     * @throws PasswordException if no provider of the given type can be found
     */
    public PasswordFactory(String type) throws PasswordException {
        Class providerClass = (Class)foundClasses.get(type);
        if (providerClass != null) {
            try {
                this.provider = (PasswordProvider)providerClass.newInstance();
                return;
            } catch (Exception ie) {
                //any problems whatsoever, just fall through
                this.provider = null;
            }
        }
        Iterator i = Service.providers(PasswordProvider.class);
        while (i.hasNext()) {
            PasswordProvider provider = (PasswordProvider) i.next();
            if (provider.isType(type)) {
                foundClasses.put (type, provider.getClass());
                this.provider = provider;
                break;
            }
        }
        if (provider == null) {
            throw new PasswordException("Provider " + type + " not found");
        }
    }

    /**
     * Get a specified <code>Password</code>.
     *
     * @param setup any configuration data the implementation may 
     *    require or support
     * @return the password hasher
     * @throws PasswordException for any error
     */
    public Password getPassword(Map setup) throws PasswordException {
        return provider.getPassword(setup);
    }
}
