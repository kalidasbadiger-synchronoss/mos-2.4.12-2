/*
 *      WriterPreferenceReadWriteLock.java
 *      
 *      Copyright 2000 Openwave Systems Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      Originally written by Doug Lea and released into the public domain.
 *      This may be used for any purposes whatsoever without acknowledgment.
 *      Thanks for the assistance and support of Sun Microsystems Labs,
 *      and everyone contributing, testing, and using this code.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/util/WriterPreferenceReadWriteLock.java#1 $
 *      
 */

package com.opwvmsg.utils.paf.util;

/** 
 * A ReadWriteLock that prefers waiting writers over
 * waiting readers when there is contention. 
 * <p>
 * The locks are <em>NOT</em> reentrant. In particular,
 * even though it may appear to usually work OK,
 * a thread holding a read lock should not attempt to
 * re-acquire it. Doing so risks lockouts when there are
 * also waiting writers.
 *
 * @author Doug Lea
 * @version $Revision: #1 $ 
 */
public class WriterPreferenceReadWriteLock implements ReadWriteLock {

    protected long activeReaders = 0; 
    protected Thread activeWriter = null;
    protected long waitingReaders = 0;
    protected long waitingWriters = 0;

    protected final ReaderLock readerLock = new ReaderLock();
    protected final WriterLock writerLock = new WriterLock();


    /**
     * Get the readLock.
     *
     * @return the readLock
     *
     **/
    public Sync writeLock() { 
      return writerLock; 
    }

    /** 
     * Get the writeLock.
     *
     * @return the writeLock
     *
     */
    public Sync readLock() { 
        return readerLock; 
    }

    /*
      A bunch of small synchronized methods are needed
      to allow communication from the Lock objects
      back to this object, that serves as controller
    */

    /**
     *
     */
    protected synchronized void cancelledWaitingReader() { 
        --waitingReaders; 
    }

    /**
     *
     */
    protected synchronized void cancelledWaitingWriter() { 
        --waitingWriters; 
    }

    /**
     *
     */
    protected boolean allowReader() {
        return activeWriter == null && waitingWriters == 0;
    }

    /**
     *
     */
    protected synchronized boolean startRead() {
        boolean allowRead = allowReader();
        if (allowRead) {
            ++activeReaders;
        }
        return allowRead;
    }

    /**
     *
     */
    protected synchronized boolean startWrite() {
        // The allowWrite expression cannot be modified without
        // also changing startWrite, so is hard-wired
        boolean allowWrite = (activeWriter == null && activeReaders == 0);
        if (allowWrite) {
            activeWriter = Thread.currentThread();
        }
        return allowWrite;
    }


    /* 
       Each of these variants is needed to maintain atomicity
       of wait counts during wait loops. They could be
       made faster by manually inlining each other. We hope that
       compilers do this for us though.
    */

    /**
     *
     */
    protected synchronized boolean startReadFromNewReader() {
        boolean pass = startRead();
        if (!pass) {
            ++waitingReaders;
        }
        return pass;
    }

    /**
     *
     */
    protected synchronized boolean startWriteFromNewWriter() {
        boolean pass = startWrite();
        if (!pass) {
            ++waitingWriters;
        }
        return pass;
    }

    /**
     *
     */
    protected synchronized boolean startReadFromWaitingReader() {
        boolean pass = startRead();
        if (pass) {
            --waitingReaders;
        }
        return pass;
    }

    /**
     *
     */
    protected synchronized boolean startWriteFromWaitingWriter() {
        boolean pass = startWrite();
        if (pass) {
            --waitingWriters;
        }
        return pass;
    }

    /**
     * Called upon termination of a read.
     * Returns the object to signal to wake up a waiter, 
     * or <code>null</code> if no such
     */
    protected synchronized Signaller endRead() {
        if (--activeReaders == 0 && waitingWriters > 0) {
            return writerLock;
        } else {
            return null;
        }
    }

  
    /**
     * Called upon termination of a write.
     * Returns the object to signal to wake up a waiter, 
     * or <code>null</code> if no such
     */
    protected synchronized Signaller endWrite() {
        activeWriter = null;
        if (waitingReaders > 0 && allowReader()) {
            return readerLock;
        } else if (waitingWriters > 0) {
            return writerLock;
        } else {
            return null;
        }
    }


    /**
     * Reader and Writer requests are maintained in two different
     * wait sets, by two different objects. These objects do not
     * know whether the wait sets need notification since they
     * don't know preference rules. So, each supports a
     * method that can be selected by main controlling object
     * to perform the notifications.  This base class simplifies mechanics.
     */

    /**
     * Base for ReaderLock and WriterLock.
     */
    protected abstract class Signaller  { 
        abstract void signalWaiters();
    }

    /**
     *
     */
    protected class ReaderLock extends Signaller implements Sync {

        public  void acquire() throws InterruptedException {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            synchronized(this) {
                if (!startReadFromNewReader()) {
                    for (;;) {
                        try { 
                            ReaderLock.this.wait();  
                            if (startReadFromWaitingReader()) {
                                return;
                            }
                        } catch (InterruptedException e) {
                            cancelledWaitingReader();
                            ie = e;
                            break;
                        }
                    }
                }
            }
            if (ie != null) {
                // Fall through outside synch on interrupt.
                // This notification is not really needed here, 
                // but may be in plausible subclasses
                writerLock.signalWaiters();
                throw ie;
            }
        }

        public void release() {
            Signaller s = endRead();
            if (s != null) {
                s.signalWaiters();
            }
        }

        synchronized void signalWaiters() { 
            ReaderLock.this.notifyAll(); 
        }

        public boolean attempt(long msecs) throws InterruptedException { 
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            synchronized(this) {
                if (msecs <= 0) {
                    return startRead();
                } else if (startReadFromNewReader()) {
                    return true;
                } else {
                    long waitTime = msecs;
                    long start = System.currentTimeMillis();
                    for (;;) {
                        try { 
                            ReaderLock.this.wait(waitTime);  
                        } catch (InterruptedException e) {
                            cancelledWaitingReader();
                            ie = e;
                            break;
                        }
                        if (startReadFromWaitingReader()) {
                            return true;
                        } else {
                            waitTime = msecs - (System.currentTimeMillis() 
                                                - start);
                            if (waitTime <= 0) {
                                cancelledWaitingReader();
                                break;
                            }
                        }
                    }
                }
            }
            writerLock.signalWaiters();
            if (ie != null) {
                throw ie;
            } else {
                // timed out
                return false;
            }
        }
        
    }


    protected class WriterLock extends Signaller implements Sync {

        public void acquire() throws InterruptedException {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            synchronized(this) {
                if (!startWriteFromNewWriter()) {
                    for (;;) {
                        try { 
                            WriterLock.this.wait();  
                            if (startWriteFromWaitingWriter()) {
                                return;
                            }
                        } catch (InterruptedException e) {
                            cancelledWaitingWriter();
                            WriterLock.this.notify();
                            ie = e;
                            break;
                        }
                    }
                }
            }
            if (ie != null) {
                // Fall through outside synch on interrupt.
                // On exception, we may need to signal readers.
                // It is not worth checking here whether it 
                // is strictly necessary.
                readerLock.signalWaiters();
                throw ie;
            }
        }

        public void release(){
            Signaller s = endWrite();
            if (s != null) {
                s.signalWaiters();
            }
        }

        synchronized void signalWaiters() { 
            WriterLock.this.notify(); 
        }

        public boolean attempt(long msecs) throws InterruptedException { 
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            InterruptedException ie = null;
            synchronized(this) {
                if (msecs <= 0) {
                    return startWrite();
                } else if (startWriteFromNewWriter()) {
                    return true;
                } else {
                    long waitTime = msecs;
                    long start = System.currentTimeMillis();
                    for (;;) {
                        try { 
                            WriterLock.this.wait(waitTime);  
                        } catch (InterruptedException e) {
                            cancelledWaitingWriter();
                            WriterLock.this.notify();
                            ie = e;
                            break;
                        }
                        if (startWriteFromWaitingWriter()) {
                            return true;
                        } else {
                            waitTime = msecs - (System.currentTimeMillis() 
                                                - start);
                            if (waitTime <= 0) {
                                cancelledWaitingWriter();
                                WriterLock.this.notify();
                                break;
                            }
                        }
                    }
                }
            }
            readerLock.signalWaiters();
            if (ie != null) {
                throw ie;
            } else { 
                // timed out
                return false;
            }
        }
    }

}
