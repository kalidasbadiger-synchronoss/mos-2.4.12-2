/*
 * Copyright (c) 2005-2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 *
 * $Id: //paf/paf31-903-1105-108-1/util/src/com/openwave/intermail/mail/MssHeartBeatListener.java#2 $
 *
 */
package com.opwvmsg.utils.paf.intermail.mail;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.intermail.IntermailConfig;


/**
 * This class is instantiated for each mss in clusterHashmap key. This class
 * receives heartbeat for corresponding mss and updates Active list accordingly.
 * 
 * @author ashwini_gandhi
 * 
 */
public class MssHeartBeatListener extends Thread {

    private static final Logger logger = Logger
            .getLogger(MssHeartBeatListener.class);

    private String keepaliveMessage = null;
    private boolean status = true;
    private int missedHeartBeats = 0;
    private long bufferTimeInterval = 0;

    public MssHeartBeatListener(String received) {
        this.setName(received);        
        logger.info("MssHeartBeatListener Thread is initialised for " + this.getName());
    }

    /**
     * @return received heartbeat message
     */
    private String getKeepaliveMessage() {
        return keepaliveMessage;
    }

    /**
     * Method sets keep alive message
     * @param keepaliveMessage received heartbeat message
     */
    void setKeepaliveMessage(String keepaliveMessage) {
        this.keepaliveMessage = keepaliveMessage;
    }

    /**
     * Method returns the status of current thread
     * @return status Boolean
     */
    boolean isStatus() {
        return status;
    }

    /**
     * Method set status
     * @param status
     */
    void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * Override Thread class run method 
     */
    public void run() {
        Config config = Config.getInstance();
        try {
            while (this.isStatus() == true) {
                long heartbeatInterval = config
                        .getInt(IntermailConfig.HEARTBEAT_INTERVAL, 1);
                int hearbeatMisses = config
                        .getInt(IntermailConfig.HEARTBEAT_MISSES, 3);
                long bufferTimeInterval = config
                        .getInt(IntermailConfig.BUFFER_TIMEINTERVAL,100);

                if (logger.isDebugEnabled()) {
                    logger.debug("heartbeatInterval = "
                            + heartbeatInterval + " hearbeatMisses = "
                            + hearbeatMisses);  
                }   
                String msg = this.getKeepaliveMessage();
                if (msg != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("In MssHeartBeatListener for "
                                + this.getName() + " Got Keepalive msg for " + msg);    
                    }
                    this.setKeepaliveMessage(null);
                    this.missedHeartBeats = 0;
                } else {
                    this.missedHeartBeats++;
                    if (logger.isDebugEnabled()) {
                        logger.debug("Dint get Keepalive msg for "
                                + this.getName()+" heartbeatmisscount = "+this.missedHeartBeats);    
                    }                    
                    if (this.missedHeartBeats > hearbeatMisses) {
                        logger.info(this.getName()
                                + " Mss server is down. Removing it from Active list of Mss");
                        boolean state = MssHostsTableHandler.getInstance()
                                .updateTables(this.getName(), false);                        
                        this.setStatus(false);
                        break;
                    }
                }
                sleep(heartbeatInterval * 1000 + bufferTimeInterval);
            }
        } catch (InterruptedException e) {
           logger.error("An interruption occur while sleeping ", e); 
        } finally {
            this.setStatus(false);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("MssHeartBeatListener Thread is exiting."
                        + this.getName());
        }
    }
}
