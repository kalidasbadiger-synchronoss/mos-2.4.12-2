/*  H+
 *      Copyright 2004 Software.com, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Software.com, Inc.  The software may be used and/or copied only
 *      with the written permission of Software.com, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ConnectionManager.java#2 $ 
 *  H- */
package com.opwvmsg.utils.paf.intermail.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.io.RmeSocket;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.rme.SocketPool;

/**
 * This ServiceManager managers connections to the various mss connection pools,
 * and implements Mss specific features like round robin port selection in the
 * case of a multiple process configuration. Note that Mss Redirection is
 * implemented at the operation level in AbstractMssOperation, not here.
 */
public class ConnectionManager {

    // config items
    private static int rmeTimeout = 120000; //2 mins

    private static int mssPoolCapacity = 75;

    private static int mssMinPool = 0;
    private static int mssMaxIdle = 600;

    private static boolean initialized = false;
    private static Config config = null;

    // For the mss, there are several pools, once for each mss instance
    static private HashMap mssSocketPool = null;

    private static final Logger logger = Logger.getLogger(ConnectionManager.class);

    /* Initialization */
    static {
        initRme();        
    }
    
    /**
     * Given an mss hostname (and optionally a port number) get an initialized
     * connection to the mss
     * 
     * @param hostName The MSS hostname
     * @param port the MSS port number, or 0 to use the next available port.
     * @param usePool true if connections may come from the pool, false if a new
     *            connection is required.
     * @return an initialized RmeSocket
     * @throws IntermailException on any RME or socket error
     */
    protected static RmeSocket getRmeSock(String hostName, int port,
                                          boolean usePool)
            throws IntermailException {

        if (hostName == null || !config.isAConfiguredHost(hostName)) {
            throw new IntermailException(LogEvent.ERROR,
                                         LogEvent.formatLogString("Conf.NotAConfiguredHost",
                                                                  new String[] { hostName }));
        }

        RmeSocket sock = null;
        SocketPool mssPool = null;
        MssSockPoolInstances instances = null;
        int startPort = 0;
        int nextPort = 0;
        if (port != 0) {
            mssPool = getSpecificPool(hostName, port);
            nextPort = port;
        } else {
            instances = getMssInstances(hostName);
            mssPool = instances.getNextPool();
            startPort = mssPool.getPort();
            nextPort = startPort;
        }
        do {
            if (usePool) {
                sock = (RmeSocket) mssPool.getItem();
            }

            if (sock == null) {
                sock = mssPool.createSocket();
            }

            if (sock == null && instances != null) {
                // try failover
                logger.log(Level.ERROR,
                           LogEvent.formatLogString("Nio.ConnServerFail",
                                                    new String[] {
                                                            "mss",
                                                            hostName,
                                                            Integer.toString(nextPort),
                                                            "Failing over to next mss port." }));

                nextPort = instances.getNextPort(nextPort);
                mssPool = instances.getSpecificPool(nextPort);
            }
        } while ((sock == null) && (instances != null && nextPort != startPort));

        if (sock == null) {
            // failed.
            throw new IntermailException(LogEvent.URGENT,
                                         LogEvent.formatLogString("Nio.ConnServerFail",
                                                                  new String[] {
                                                                          "mss",
                                                                          hostName,
                                                                          Integer.toString(startPort),
                                                                          "No failover available." }));
        }
        return sock;
    }

    /**
     * Puts a socket back into the pool.
     * 
     * @param sock the socket
     * @throws IntermailException
     */
    protected static void putRmeSock(RmeSocket sock) throws IntermailException {
        SocketPool mssPool = getSpecificPool(sock.getHost(), sock.getPort());
        mssPool.putItem(sock);
    }

    /**
     * Given a hostname, return a MssSockPoolInstances Object.
     * 
     * @param hostName the MSS host
     * @return MssSockPoolInstances representing all instances on that host
     */
    private synchronized static MssSockPoolInstances getMssInstances(
                                                                     String hostName) {
        MssSockPoolInstances instances = (MssSockPoolInstances) mssSocketPool.get(hostName);
        if (instances == null) {
            instances = initMssHost(hostName);
        }
        return instances;
    }

    /**
     * Given a hostname and a port, get a specific socket pool.
     * 
     * @param hostName the MSS hostname
     * @param port the mss port
     * @return SocketPool for that particular connection
     */
    private synchronized static SocketPool getSpecificPool(String hostName,
                                                           int port) {
        MssSockPoolInstances instances = (MssSockPoolInstances) mssSocketPool.get(hostName);
        if (instances == null) {
            instances = initMssHost(hostName);
        }
        return instances.getSpecificPool(port);
    }

    /**
     * Read the configuration data and initialize this Pool
     */
    public synchronized static void initRme() {
        // check initializd again
        if (!initialized) {
            config = Config.getInstance();
            try {
                rmeTimeout = config.getInt("rmeLookupTimeout") * 1000;
            } catch (ConfigException ce) {
                rmeTimeout = config.getInt("netTimeout", 240) * 1000;
            }
            mssPoolCapacity = config.getInt("mssMaxConnectionPoolSize", 50);
            mssMinPool = config.getInt("mssMinConnectionPoolSize", 0);
            mssMaxIdle = config.getInt("mssMaxConnectionPoolIdleTimeSec", 600);

            mssSocketPool = new HashMap(20);
            initialized = true;
            
            // If maximum pool limit is disabled then not to start the thread.
            // The additional sockets that are created beyond the mss.pool.minSize
            // are released after use
            if (mssPoolCapacity != -1) {
                ReleaseSocketThread releaseSocketthreead = new ReleaseSocketThread();
                Thread SocketThread = new Thread(releaseSocketthreead);
                SocketThread.start();
            }
        }
    }

    /**
     * Set up the appropriate socket pools for the specified MSS host as it is
     * encountered for the first time
     * 
     * @param hostName the MSS host
     * @return MssSockPoolInstances representing all instances on that host
     */
    private static MssSockPoolInstances initMssHost(String hostName) {
        Config config = Config.getInstance();
        int mssBase = config.getInt(hostName, "mss", "mssBasePort");
        int mssNum = config.getInt(hostName, "mss", "mssNumPorts", 1);

        SocketPool[] pools = new SocketPool[mssNum];
        for (int i = 0; i < mssNum; i++) {
            pools[i] = new SocketPool(mssPoolCapacity,
                                      mssMinPool,
                                      mssMaxIdle,
                                      hostName,
                                      mssBase + i,
                                      rmeTimeout,
                                      "mss");
        }
        MssSockPoolInstances inst = new MssSockPoolInstances(mssBase,
                                                             mssNum,
                                                             pools);

        mssSocketPool.put(hostName, inst);
        return inst;
    }

    /**
     * This private subclass helps manage multiple mss instances per host. It
     * implements the round-robin port shuffling for each host, and maintains a
     * list of pools for each instance.
     */
    private static class MssSockPoolInstances {
        int basePort;
        int numPorts;
        SocketPool[] sockPools;
        int curPortOffset = 0;

        /**
         * If server uses more than one port, assigns new sockets in a round
         * robin fashion to different ports
         * 
         * @return the next port to use.
         *  
         */
        private synchronized int getNextPortOffset() {
            if (numPorts == 1) {
                return 0;
            } else {
                // increment curPortOffset, and mod it
                curPortOffset = (curPortOffset + 1) % numPorts;
                return curPortOffset;
            }
        }

        /**
         * Given a port number, returns the next port in use for this mss
         * (useful for failover.) not tied to the round robin distribution of
         * ports like the above function.
         * 
         * @param portNum the current port number
         * @return the next port number in use
         */
        private int getNextPort(int portNum) {
            if (numPorts == 1) {
                return basePort;
            } else {
                // increment curPortOffset, and mod it
                return ((portNum - basePort) + 1) % numPorts;
            }
        }

        /**
         * Initialize this object.
         * 
         * @param basePortIn Mss base port
         * @param numPortsIn number of Mss ports (instances) on this host
         * @param sockPoolsIn an array of socket pools for each instance
         */
        private MssSockPoolInstances(int basePortIn, int numPortsIn,
                SocketPool[] sockPoolsIn) {
            basePort = basePortIn;
            numPorts = numPortsIn;
            sockPools = sockPoolsIn;
            curPortOffset = (new Random()).nextInt(numPorts);
        }

        /**
         * Gets the next pool to use, assigned in round-robin fashion
         * 
         * @return the next SocketPool to use
         */
        SocketPool getNextPool() {
            return sockPools[getNextPortOffset()];
        }

        /**
         * Gets a specific pool from these instances.
         * 
         * @param portNum the port number of the specific instance
         * @return SocketPool with connections to that port
         */
        SocketPool getSpecificPool(int portNum) {
            if (portNum < basePort || portNum >= basePort + numPorts) {
                return null;
            }
            return sockPools[portNum - basePort];
        }

        /**
         * Gets scoketPools associated with this instance
         * @return SocketPool array associated with this instance
         */
        SocketPool[] getSockPoolsArray () {
            return sockPools;
    }
}

    /**
     * This private subclass, a thread that monitors and releases the stale
     * socket connections from the Socketpools
     * This is not implemented in paf31-903-1105-108-1
     */
   private static class ReleaseSocketThread implements Runnable {
        // The idle time in milliseconds
        long mssMaxIdleTimeMS = mssMaxIdle * 1000;

        public void run() {
            // Will contain mss host name list
            ArrayList<String> hostNameList = new ArrayList<String>();
            while (true) {
                try {
                    Thread.sleep(mssMaxIdleTimeMS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                int mssSocketPoolSize = mssSocketPool.size();
                if (mssSocketPoolSize > 0) {
                    Set keySet = mssSocketPool.keySet();
                    // Add the hostnames to hostNameList only if there are new
                    // mssInstances. This is to avoid populating Arraylist
                    // everytime the thread iterates
                    if (mssSocketPoolSize != hostNameList.size()) {
                        hostNameList.clear();
                        // get Lock on mssSocketPool
                        synchronized (mssSocketPool) {
                            Iterator it = keySet.iterator();
                            while (it.hasNext()) {
                                hostNameList.add((String) it.next());
                            }
                        }
                    }
                    
                    MssSockPoolInstances mssInstance = null;
                    for (int i = 0; i < hostNameList.size(); i++) {
                        String hostName = hostNameList.get(i);
                        mssInstance =
                            (MssSockPoolInstances) mssSocketPool.get(hostName);
                        // get socketpool array
                        SocketPool[] socketPool = mssInstance.getSockPoolsArray();
                        if (logger.isDebugEnabled()) {
                            logger.debug("START: Purge expired socket connection from " +
                                    "pool for Host:"+ hostName + " having " + socketPool.length + "pools");
                        }
                        // iterate through this array
                        for (int k = 0; k < socketPool.length; k++) {
                            // get the specific socketpool
                            SocketPool tempPool = socketPool[k];
                            while (true) {
                                RmeSocket socket = (RmeSocket) tempPool.getItem();
                                if (null == socket) {
                                    break;
                                } else if (!socket.readyToClose()) {
                                    tempPool.putItem(socket);
                                    break;
                                } else {
                                    tempPool.putItem(socket);
                                }
                            }
                        }
                        if (logger.isDebugEnabled()) {
                            logger.debug("END: Purged expired socket connection from pool for Host:"
                                    + hostName);
                        }
                    }
                }
            }
        }
    }
}