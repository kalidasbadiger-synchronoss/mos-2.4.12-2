/*
 * Copyright 2005 Openwave Systems, Inc. All Rights Reserved.
 * 
 * The copyright to the computer software herein is the property of Openwave
 * Systems, Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems, Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied.
 * 
 * $Id:
 */
package com.opwvmsg.utils.paf.intermail.config;

import java.io.IOException;

import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

/**
 * Represesnts a config key impact.  Can be sent to, or received from, config server.
 */
public class Impact {

    private int impact;
    private String keyName;
    private String host;
    private String app;
    private String message;
    private int firstPort, lastPort;

    // all of the possible impact types.  
    public static final int IMPACT_ASSIGNED_PORT = 0;
    public static final int IMPACT_NOT_FETCHED = 1;
    public static final int IMPACT_TRIVIAL = 2;
    public static final int IMPACT_SERV_TIMEDOUT = 3;
    public static final int IMPACT_SERV_REPORT = 4;
    public static final int IMPACT_SERV_NO_REPORT = 5;
    public static final int IMPACT_ROGUE_TIMEDOUT = 6;
    public static final int IMPACT_ROGUE_REPORT = 7;
    public static final int IMPACT_INFO = 8;
    public static final int IMPACT_FIXABLE = 9;
    public static final int IMPACT_RESTART = 10;
    public static final int IMPACT_SERV_STOP = 11;
    public static final int IMPACT_SERV_START = 12;
    public static final int IMPACT_NOT_SERVER = 13;
    public static final int IMPACT_INVALID_DATA = 14;
    public static final int IMPACT_NOT_POSSIBLE = 15;
    public static final int IMPACT_PORT_CONFLICT = 16;
    public static final int IMPACT_FAILOVER_CONFLICT = 17;
    public static final int IMPACT_ALL_HOSTS_CONFIGURED = 18;
    public static final int IMPACT_SHARED_PORTS = 19;
    public static final int IMPACT_BAD_KEY = 20;
    public static final int IMPACT_NOT_PORT = 21;
    public static final int IMPACT_NOT_PROPAGATED = 22;


    /**
     * From an RME stream, read the data into this class.
     * 
     * @param inStream an rme input stream
     * @throws IOException on any socket error
     */
    public Impact(RmeInputStream inStream) throws IOException {
        impact = inStream.readInt();
        keyName = inStream.readString();
        host = inStream.readString();
        app = inStream.readString();
        message = inStream.readString();
        firstPort = inStream.readInt();
        lastPort = inStream.readInt();
    }

    /** Initialize the Impact with passed in values.
     * 
     * @param keyName the name of the config key
     * @param host the host name for this config key
     * @param app the application for this config key
     * @param message a message for this impact (usually not used)
     * @param firstPort The first port number in a range of ports
     * @param lastPort The last port number in a range of parts
     * @param impact - the impact constant (one of the IMPACT_xxx constants)
     */
    public Impact(String keyName, String host, String app, String message,
            int firstPort, int lastPort, int impact) {
        this.keyName = keyName;
        this.host = host;
        this.app = app;
        this.message = message;
        this.firstPort = firstPort;
        this.lastPort = lastPort;
        this.impact = impact;
    }
    
    /**
     * Returns the impact code for this impact.
     * The impact code is one of the IMPACT_XXX constants.
     * 
     * @return impact code.
     */
    public int getImpactCode(){
        return impact;
    }

    /**
     * Gets the key name associated with this impact
     * 
     * @return key name
     */
    public String getKeyName(){
        return keyName;
    }

    /**
     * Gets the host name associated with this impact
     * 
     * @return host name
     */
    public String getHost(){
        return host;
    }

    /**
     * Gets the application name associated with this impact
     * 
     * @return app name
     */
    public String getApp(){
        return app;
    }
    
    /**
     * Returns the message associated with this impact.
     * (Mostly this is not used anymore)
     * 
     * @return message
     */
    public String getMessage(){
        return message;
    }
    
    /**
     * Gets the first port (or only port if not a range) for
     * this key, if it is a port key.
     * 
     * @return first (or only) port in a range of ports.
     */
    public int getFirstPort(){
        return firstPort;
    }
    
    /**
     * Returns the last port in the port range for this key.
     * 
     * @return last port in the range of ports
     */
    public int getLastPort(){
        return lastPort;
    }
    
    /**
     * Writes the Impact data to the rme stream
     * 
     * @param outStream the RME output stream
     * @throws IOException on any io error
     */
    protected void writeImpact(RmeOutputStream outStream) throws IOException {
        outStream.writeInt(impact);
        outStream.writeString(keyName);
        outStream.writeString(host);
        outStream.writeString(app);
        outStream.writeString(message);
        outStream.writeInt(firstPort);
        outStream.writeInt(lastPort);
    }

    /**
     * Writes the Impact data as an RME pointer (i.e can be null)
     * 
     * @param outStream rme output stream
     * @throws IOException on any io error
     */
    protected void writeImpactPtr(RmeOutputStream outStream) throws IOException {
        outStream.writeBoolean(true);
        writeImpact(outStream);
    }
}