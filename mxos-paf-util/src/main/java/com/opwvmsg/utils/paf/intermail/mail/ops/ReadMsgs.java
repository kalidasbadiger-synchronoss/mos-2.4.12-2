/*  H+
 *      Copyright 2004 Openwave Systems, Inc.  All Rights Reserved.
 *      
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems, Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems, Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *      
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/intermail/mail/ops/ReadMsgs.java#1 $ 
 *  H- */

package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.intermail.extsrv.ServiceManager;
import com.opwvmsg.utils.paf.intermail.extsrv.clients.MessageExamine;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.rme.AbstractOperation;

/**
 * This operation will read a message from the MSS. It has a variety of options
 * to control how much of the message you will retrieve from the most basic of
 * meta data, to the headers, mime information, or the text itself.
 */
public class ReadMsgs extends AbstractMssOperation {

    // input
    private String name;
    private String pathname;
    private int[] uids;
    private String[] docids;
    protected int options; // for UpdateMsgs
    private String[] headers;
    private int accessId;
    private int offset;
    private int length;
    private String flags;
    private String username;
    private String peer_IP;
    private String[] setFlags;
    private String[] clearFlags;
    private String[] replaceFlags;

    // output
    private Msg[] messages;

    // internal
    private String url;
    private String[] pathnameArray;

    // constants
    public static final int READ_MSGS_GET_HDRS = 0x01;
    public static final int READ_MSGS_GET_TEXT = 0x02;
    public static final int READ_MSGS_GET_MIME = 0x04;
    public static final int READ_MSGS_FOLDER_IS_HINT = 0x08;
    public static final int READ_MSGS_MULTIPLE_OK = 0x10;
    public static final int READ_MSGS_UPDATE_ONLY = 0x20;
    public static final int READ_MSGS_SUPPRESS_MERS = 0x40;
    public static final int READ_MSGS_GET_ATTACHMENT_STATUS = 0x80;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder (or folder hint) for the messages
     * @param uids The uids of the messages to read
     * @param flags Flags to update on the messages
     * @param headers An array of headers to retrieve
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            READ_MSGS_GET_HDRS - retrieve message headers (must be
     *            specified with GET_MIME) <br>
     *            READ_MSGS_GET_TEXT - retrieve message text <br>
     *            READ_MSGS_GET_MIME - retrieve MIME info for message <br>
     *            READ_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            READ_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            READ_MSGS_UPDATE_ONLY - do not read the messages specified,
     *            only update the flags <br>
     *            READ_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     * @param offset the offset of the text to read from
     * @param length the legnth of the text to read
     * @param username - for Legal Intercept - the current user's username
     * @param peer_IP - for Legal Intercept - the IP address of the user
     * @param setFlags custom flags to set
     * @param clearFlags custom flags to clear
     * @param replaceFlags custom flags to replace
     */
    public ReadMsgs(String host, String name, String pathname, int[] uids,
            String flags, String[] headers, int accessId, int options,
            int offset, int length, String username, String peer_IP,
            String[] setFlags, String[] clearFlags, String[] replaceFlags) {
        super(RmeDataModel.CL);
        // not accepting msgids at this point
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_CL_RETRIEVEMSGS);
        this.pathname = pathname;
        pathnameArray = buildPathnameArray(pathname);
        if (uids == null) {
            uids = new int[0];
        }
        this.uids = uids;
        this.docids = new String[0];
        if (flags == null) {
            flags = Msg.DEFAULT_FLAGS;
        }
        this.flags = flags;

        this.headers = headers;
        this.accessId = accessId;
        this.options = options;
        this.offset = offset;
        this.length = length;
        this.username = username;
        this.peer_IP = peer_IP;
        this.setFlags = setFlags;
        this.clearFlags = clearFlags;
        this.replaceFlags = replaceFlags;
    }

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param pathname The folder (or folder hint) for the messages
     * @param docids The doc ids of the messages to read
     * @param flags Flags to update on the messages
     * @param headers An array of headers to retrieve
     * @param accessId The accessId of the caller - one of Mailbox.ACCESS_ADMIN
     *            or Mailbox.ACCESS_GENERIC_END_USER
     * @param options - options for this operation. possible bits are: <br>
     *            READ_MSGS_GET_HDRS - retrieve message headers (must be
     *            specified with GET_MIME) <br>
     *            READ_MSGS_GET_TEXT - retrieve message text <br>
     *            READ_MSGS_GET_MIME - retrieve MIME info for message <br>
     *            READ_MSGS_FOLDER_IS_HINT - messages may not be in specified
     *            src folder <br>
     *            READ_MSGS_MULTIPLE_OK - if looking in multiple folders,
     *            multiple uid matches are ok <br>
     *            READ_MSGS_UPDATE_ONLY - do not read the messages specified,
     *            only update the flags <br>
     *            READ_MSGS_SUPPRESS_MERS - suppress MERS events for this
     *            operation <br>
     * @param offset the offset of the text to read from
     * @param length the legnth of the text to read
     * @param username - for Legal Intercept - the current user's username
     * @param peer_IP - for Legal Intercept - the IP address of the user
     * @param setFlags custom flags to set
     * @param clearFlags custom flags to clear
     * @param replaceFlags custom flags to replace
     */
    public ReadMsgs(String host, String name, String pathname, String[] docids,
            String flags, String[] headers, int accessId, int options,
            int offset, int length, String username, String peer_IP,
            String[] setFlags, String[] clearFlags, String[] replaceFlags) {
        super(RmeDataModel.CL);
        // not accepting msgids at this point
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_P_CL_RETRIEVEMSGS);
        this.pathname = pathname;
        pathnameArray = buildPathnameArray(pathname);
        this.uids = new int[0];
        if (docids == null) {
            docids = new String[0];
        }
        this.docids = docids;

        if (flags == null) {
            flags = Msg.DEFAULT_FLAGS;
        }
        this.flags = flags;

        this.headers = headers;
        this.accessId = accessId;
        this.options = options;
        this.offset = offset;
        this.length = length;
        this.username = username;
        this.peer_IP = peer_IP;
        this.setFlags = setFlags;
        this.clearFlags = clearFlags;
        this.replaceFlags = replaceFlags;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (pathname == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "pathname", "ReadMsgs" }));
        }
        if (flags == null || flags.length() != Msg.MSG_FLAGS_SIZE) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "flags", "ReadMsgs" }));
        }
        if (rmeDataModel == RmeDataModel.CL) {
            callRme(MSS_P_CL_RETRIEVEMSGS);
        } else {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_P_CL_RETRIEVEMSGS", host,
                            "dataModel=" + rmeDataModel }));
        }
        if (username != null
                && ServiceManager
                        .hookHasEnabledServices(MessageExamine.MESSAGE_READ_RESULTS)) {
            MessageExamine MEop = new MessageExamine(username,
                    Config.getDefaultApp(), peer_IP, "ReadMsgs", messages,
                    offset, length, true, pathname, host, name);
            try {
                MEop.execute(); // do not return any errors from LI
                // they will just be logged.
            } catch (IntermailException ie) {
            }
        }

    }

    /**
     * Get the message objects returned from the MSS
     * 
     * @return messages
     */
    public Msg[] getMessages() {
        return messages;
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {
        outStream.writeInt(AbstractOperation.RME_DEFAULT_CLIENT_INDEX);
        outStream.writeString(url);
        outStream.writeStringArray(pathnameArray);
        // msg specs
        outStream.writeInt(uids.length + docids.length);
        for (int i = 0; i < uids.length; i++) {
            outStream.write(1);
            outStream.writeInt(uids[i]);
            outStream.writeString(null);
        }

        for (int i = 0; i < docids.length; i++) {
            outStream.write(1);
            outStream.writeInt(-1);
            outStream.writeString(docids[i]);
        }

        Msg.writeMsgFlags(outStream, flags);

        // keyword support
        outStream.writeStringArray(setFlags);
        outStream.writeStringArray(clearFlags);
        outStream.writeStringArray(replaceFlags);

        outStream.writeStringArray(headers);
        outStream.writeInt(accessId);
        outStream.writeInt(offset);
        outStream.writeInt(length);
        outStream.writeInt(options);
    }

    /*
     * (non-Javadoc)
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // read Message
        int numMsgs = inStream.readInt();

        messages = new Msg[numMsgs];
        for (int i = 0; i < numMsgs; i++) {
            messages[i] = new Msg(inStream);
        }
        logEvents = inStream.readLogEventArray();
    }

    @Override
    protected void constructHttpData() throws IOException {
        // RME operation not supported
    }

    @Override
    protected void receiveHttpData() throws IOException {
        // RME operation not supported
    }
}
