/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.replystore;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.mail.internet.MimeUtility;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;
import com.opwvmsg.utils.paf.intermail.io.RmeInputStream;
import com.opwvmsg.utils.paf.intermail.io.RmeOutputStream;

public class Reply {

    private String text;
    private int version;
    private int type;

    private static String autoReplyCharset = null;

    public final static int AUTOREPLY = 1;
    public final static int SIGNATURE = 2;
    public final static int ADDRESSBOOK = 3;
    public final static int GREENLIST = 4;
    public final static int USERFILTER = 5;
    public final static int WEBEDGE_OPTIONS = 6;
    public final static int WEBEDGE_ADDRESSBOOK = 7;
    public final static int BINARY_DATA = 8;
    public final static int WEBEDGE_META_FILTERS = 9;
    public final static int WEBEDGE_EXTERNAL_MAIL = 10;
    //WEBEDGE_ADDRESSBOOK_EMAILLIST is deprecated
    //public final static int WEBEDGE_ADDRESSBOOK_EMAILLIST = 11;
    public final static int DIRECTORY_BACKUP = 11;
    public final static int WEBEDGE_TO_ADRESSBOOK_BACKUP = 12;
    public final static int WEBEDGE_TO_SIEVERULES_BACKUP = 13;
    public final static int ACCOUNT_ACTIVE = 14;
    public final static int LAVA_OPTIONS = 15;
    public final static int WEBEDGE_SORTED_ADDRESSBOOK = 16;

    private static Logger logger = Logger.getLogger(Reply.class);

    static {
        try {
            Config config = Config.getInstance();
            autoReplyCharset = config.get(null, "mta", "autoReplyCharset",
                "utf-8");
        } catch (ConfigException ignore) {
            // error logged below
        }
        if (autoReplyCharset == null) {
            logger.error("/*/mta/autoReplyCharset is not defined in config.db.  Assuming UTF-8.");
            autoReplyCharset = "UTF-8";
        }
        autoReplyCharset = MimeUtility.javaCharset(autoReplyCharset);
    }

    /**
     * Default constructor
     */
    public Reply() throws IOException {
        this.version = -1;
        this.text = null;
    }
    
    /**
     * Reads & returns the reply object from the RME stream. Note that if this is the
     * autoreply message (type 1) then we will assume that the data matches the
     * charset specified by autoReplyCharset, otherwise UTF-8 is assumed.
     *
     * @param inStream the RME stream
     * @param type the autoreply type
     * @throws IOException on any IO or RME error
     */
    public void readReply(RmeInputStream inStream, int type) throws IOException {

        // we only ask for one reply in the request,
        // so we can assume only one response
        this.type = type;

        this.text = null;
        int numMessages = inStream.readInt();
        // we only expect 1 message, but we should handle the case
        // if there are more than 1.

        if (numMessages >= 1) {
            byte[] textBytes = inStream.readStringBytes();
            try {
                if (type == AUTOREPLY) {
                    text = new String(textBytes, autoReplyCharset);
                } else {
                    text = new String(textBytes, "UTF-8");
                }
            } catch (UnsupportedEncodingException e) {
                logger.error("autoReplyCharset "
                        + autoReplyCharset
                        + " is invalid;"
                        + " using UTF-8.  Please check the value of this key in config.db.");
                try {
                    text = new String(textBytes, "UTF-8");
                } catch (UnsupportedEncodingException ignore) {
                }
            }
            while (--numMessages > 0) {
                // if there are more than 1 text strings we throw the rest
                // away right now, since they are not expected. This
                // is just to keep the stream in sync.
                inStream.readStringBytes();
            }
        }

        int[] verArray = inStream.readIntArray();
        if (verArray.length >= 1) {
            version = verArray[0];
        }
    }

    public void readSLReply(RmeInputStream inStream, int type) throws IOException {

        // we only ask for one reply in the request,
        // so we can assume only one response
        this.type = type;

        this.text = null;
        byte[] textBytes = inStream.readByteArray();
        try {
            if (type == AUTOREPLY) {
                text = new String(textBytes, autoReplyCharset);
            } else {
                text = new String(textBytes, "UTF-8");
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("autoReplyCharset "
                    + autoReplyCharset
                    + " is invalid;"
                    + " using UTF-8.  Please check the value of this key in config.db.");
            try {
                text = new String(textBytes, "UTF-8");
            } catch (UnsupportedEncodingException ignore) {
            }
        }

        this.version = inStream.readInt();
    }
    
    /**
     * Gets the text of the reply field.
     *
     * @return text String
     */
    public String getText() {
        return text;
    }

    /**
     * Gets the version of the autoreply field
     *
     * @return version
     */
    public int getVersion() {
        return version;
    }

    /**
     * Gets the autoreply type for this Reply field
     *
     * @return the autoreply type
     */
    public int getType() {
        return type;
    }

    /**
     * Sets the text of this Reply object to prepare for update.
     *
     * @param text the new text string for the reply field.
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Sets the version number for the reply text.
     * This should only be updated by UpdateReply as incorrect
     * values will cause errors when this object is saved to the mss.
     *
     * @param version the new version number
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Writes this Reply field to the RME stream.
     *
     * @param outStream the current RME stream.
     * @throws IOException on any IO error.
     */
    public void writeReply(RmeOutputStream outStream) throws IOException {
        byte[] textBytes = null;

        outStream.writeInt(1); // 1 message
        try {
            if (type == AUTOREPLY && !(autoReplyCharset.equalsIgnoreCase("utf-8"))) {
                textBytes = text.getBytes(autoReplyCharset);
                outStream.writeStringBytes(textBytes);
            } else {
                outStream.writeString(text);
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("autoReplyCharset " + autoReplyCharset
                + " is invalid; using UTF-8.  "
                + "Please check the value of this key in config.db.");
            outStream.writeString(text);
        }
        outStream.writeInt(type);
        outStream.writeIntArray(new int[] { version }); // version
        outStream.writeBoolean(false); // not mig server
    }
    
    /**
     * Writes this Reply field to the Stateless RME stream.
     *
     * @param outStream the current RME stream.
     * @throws IOException on any IO error.
     */
    public void writeSLReply(RmeOutputStream outStream) throws IOException {
        byte[] textBytes = null;

        outStream.writeInt(type);
        try {
            if (type == AUTOREPLY && !(autoReplyCharset.equalsIgnoreCase("utf-8"))) {
                textBytes = text.getBytes(autoReplyCharset);
                outStream.writeStringBytes(textBytes);
            } else {
                outStream.writeString(text);
            }
        } catch (UnsupportedEncodingException e) {
            logger.error("autoReplyCharset " + autoReplyCharset
                + " is invalid; using UTF-8.  "
                + "Please check the value of this key in config.db.");
            outStream.writeString(text);
        }
        outStream.writeInt(version); // version
    }
}

