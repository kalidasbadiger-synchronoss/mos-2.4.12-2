/*
 *      PingFilter.java
 *
 *      Copyright 2002 Openwave Systems Inc.  All Rights Reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/filters/PingFilter.java#1 $
 *
 */

package com.opwvmsg.utils.paf.filters;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;


/**
 * <p>This is an implementation of a simple servlet filter for
 * providing a basic health check for a web application.
 * </p>
 * <p>When an incoming request has a URI path (from
 * <code>HttpServletRequest.getRequestURI</code>) that starts with
 * the value of the required "pingUri" parameter, the servlet filter
 * will send an empty response.
 * </p>
 * <p>The optional "adminHosts" parameter can contain a regular
 * expression that matches IP addresses.  If it is defined, then the
 * ability to ping is limited to requests from a host with an address
 * that matches the expression.  By default, adminHosts is undefined
 * meaning that ping requests from any host will be honored.
 * </p>
 * <p>The optional "accessDeniedPolicy" parameter can contain the
 * integer zero or any valid HTTP response code (see RFC 2616 -
 * section 10).  If access is denied based on the value of adminHosts
 * and this parameter has a non-zero value then the request is not
 * processed further and this status code is returned.  If this
 * parameter has a value of zero then the request is passed through
 * this filter unmodified.
 *
 * @author Forrest Girouard
 * @version $Revision: #1 $
 */
public class PingFilter implements Filter {

    private static final Logger logger = Logger.getLogger(PingFilter.class);

    private static final String PING_URI_PARAM = "pingUri";

    private static final String PING_REQUEST = "pingRequest";

    private static final String ADMIN_HOSTS_PARAM = "adminHosts";

    private static final String ACCESS_DENIED_POLICY_PARAM = "accessDeniedPolicy";


    private FilterConfig config;

    private ServletContext context;

    private String pingUri;

    private Pattern adminHosts;

    private int accessDeniedPolicy = HttpServletResponse.SC_FORBIDDEN;


    /**
     * Initialize this instance.
     *
     * @param filterConfig the configuration
     */
    public void init(FilterConfig filterConfig) throws ServletException {
        this.config = filterConfig;
        context = filterConfig.getServletContext();
        pingUri = filterConfig.getInitParameter(PING_URI_PARAM);

        String adminHostsStr = filterConfig.getInitParameter(ADMIN_HOSTS_PARAM);
        if (adminHostsStr != null) {
            try {
                adminHosts = Pattern.compile(adminHostsStr, Pattern.DOTALL);
            } catch (PatternSyntaxException e) {
                throw new ServletException("bad adminhosts pattern: "
                                           + adminHostsStr);
            }
        }

        String accessDeniedPolicyStr =
                filterConfig.getInitParameter(ACCESS_DENIED_POLICY_PARAM);
        if (accessDeniedPolicyStr != null) {
            try {
                accessDeniedPolicy = Integer.parseInt(accessDeniedPolicyStr);
            } catch (NumberFormatException e) {
                throw new ServletException("bad accessDeniedPolicy: "
                                           + accessDeniedPolicyStr);
            }
        }

        String pingRequest = null;
        try {
            Config config = Config.getInstance();
            pingRequest = config.get(PING_REQUEST);
        } catch (ConfigException e) {
            logger.warn(PING_REQUEST + ": no such config entry.");
        }
        if (pingRequest != null && pingUri != null &&
                !pingUri.equals(pingRequest)) {
            logger.warn("ping: mismatch - web.xml:pingUri, " + pingUri +
                    ", doesn't match config.db:pingRequest, " + pingRequest);
        }

        if (logger.isInfoEnabled()) {
            logger.info("pingUri = " + pingUri);
            logger.info("pingRequest = " + pingRequest);
            logger.info("adminHostsStr = " + adminHostsStr);
            logger.info("accessDeniedPolicy = " + accessDeniedPolicy);
        }
    }

    /**
     */
    public void destroy() {
        config = null;
        context = null;
        pingUri = null;
        adminHosts = null;
    }

    /**
     */
    public FilterConfig getFilterConfig() {
        return config;
    }

    /**
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain)
            throws ServletException, IOException {

        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpServletResponse httpResponse = (HttpServletResponse)response;
        String requestUri = httpRequest.getRequestURI();
        if (logger.isDebugEnabled()) {
            logger.debug("requestUri: " + requestUri
                         + ", context: [" + httpRequest.getContextPath()
                         + "], servlet: [" + httpRequest.getServletPath()
                         + "], pathinfo: [" + httpRequest.getPathInfo()
                         + "]");
        }

        if (pingUri != null && requestUri.startsWith(pingUri)) {
            boolean doPing = true;
            int httpStatus = 0;
            if (adminHosts != null) {
                String remote = request.getRemoteAddr();
                Matcher matcher = adminHosts.matcher(remote);
                if (!matcher.matches()) {
                    doPing = false;
                    httpStatus = accessDeniedPolicy;
                }
            }
            if (doPing) {
                httpStatus = handlePing(httpRequest, httpResponse);
            }
            if (httpStatus != 0) {
                httpResponse.setStatus(httpStatus);
                return;
            }
        }

        chain.doFilter(request, response);
    }

    /**
     * Determines the health of the server and populates response.  At
     * a minimum the return value should be SC_OK.
     *
     * @param httpRequest the HTTP request to check the webapp health
     * @param httpResponse the HTTP response to populate
     * @return int HTTP response status
     */
    public int handlePing(HttpServletRequest httpRequest,
                          HttpServletResponse httpResponse)
            throws ServletException, IOException {
        return HttpServletResponse.SC_OK;
    }
}
