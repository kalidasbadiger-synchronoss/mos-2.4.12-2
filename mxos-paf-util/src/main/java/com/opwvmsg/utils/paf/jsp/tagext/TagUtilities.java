/*
 *      Copyright (c) 2002-2006 Openwave Systems Inc.  All rights reserved.
 *
 *      The copyright to the computer software herein is the property of
 *      Openwave Systems Inc.  The software may be used and/or copied only
 *      with the written permission of Openwave Systems Inc. or in accordance
 *      with the terms and conditions stipulated in the agreement/contract
 *      under which the software has been supplied.
 *
 *      $Id: //paf/mainline/util/src/main/java/com/openwave/paf/jsp/tagext/TagUtilities.java#1 $
 *
 */

package com.opwvmsg.utils.paf.jsp.tagext;

import javax.servlet.jsp.PageContext;

import com.opwvmsg.utils.paf.util.EscapeType;
import com.opwvmsg.utils.paf.util.Utilities;


/**
 * JSP tag utility methods.
 *
 * @author Mark Abbott
 * @version $Revision: #1 $
 */
public class TagUtilities {

    /**
     * Key for scoped attribute indicating XML escaping.
     */    
    static public String KEY_XML_ESCAPES =
        "com.openwave.paf.jsp.tagext.xmlescape";

    /**
     * Key for scoped attribute indicating WML escaping.
     */
    static public String KEY_WML_ESCAPES = 
        "com.openwave.paf.jsp.tagext.wmlescape";

    /**
     * Key for scoped attribute indicating HDML escaping.
     */
    static public String KEY_HDML_ESCAPES = 
        "com.openwave.paf.jsp.tagext.hdmlescape";
    
    /**
     * Key for scoped attribute indicating backward compatibility
     * mode for escaping.
     */
    static public String KEY_ESCAPES_DEFAULT_OFF = 
        "com.openwave.paf.jsp.tagext.escapesdefaultoff";


    /**
     * Determine the correct escape type to use given the
     * set of attributes currently in scope. XML
     * escaping is the default type.  If the scoped
     * attributed named by <code>KEY_HDML_ESCAPES</code> is present
     * in any scope, HDML escaping is performed.  If
     * KEY_WML_ESCAPES is present, WML escaping is indicated.
     *
     * @param context the page context in which to look for
     *   scoped attributes
     *
     * @return the escaped text
     */
    public static EscapeType findEscapeType(PageContext context) {
        if (context.findAttribute(KEY_XML_ESCAPES) != null) {
            return EscapeType.XML;
        }
        if (context.findAttribute(KEY_WML_ESCAPES) != null) {
            return EscapeType.WML;
        }
        if (context.findAttribute(KEY_HDML_ESCAPES) != null) {
            return EscapeType.HDML;
        }
        return EscapeType.HTML;
    }

    /**
     * Check for XML escape compatibility mode.  This is indicated
     * by the presence of the scoped attribute defined by
     * KEY_ESCAPES_DEFAULT_OFF.  If true, it means that by default
     * no XML escaping should be done by PAF text processing tags,
     * compatible with older PAF behavior.
     *
     * @param context the page context
     */
    public static boolean isEscapeCompatibilityMode(PageContext context) {
        return context.findAttribute(KEY_ESCAPES_DEFAULT_OFF) != null;
    }

    /**
     * Replace unsafe characters in a string with their XML escape
     * sequences.  The characters &amp; &lt; &gt; &quot; &apos;
     * are replaced with their entity equivalents.  If the scoped
     * attributed named by <code>KEY_HDML_ESCAPES</code> is present
     * in any scope, then the dollar sign ($) is also escaped.  If
     * KEY_WML_ESCAPES is present, non-breaking spaces and
     * soft hyphens are escaped in addition to all HDML and XML
     * escapes.
     *
     * @param text the input text
     * @param context the page context in which to look for
     *   scoped attributes
     *
     * @return the escaped text
     */
    public static String escapeXml(String text, PageContext context) {
        return Utilities.escapeXml(text, findEscapeType(context));
    }


    /**
     * No instances.
     */
    private TagUtilities() {
    }
}
