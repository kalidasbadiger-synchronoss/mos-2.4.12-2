/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.utils.paf.intermail.mail.ops;

import java.io.IOException;
import java.util.UUID;

import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.log.LogEvent;
import com.opwvmsg.utils.paf.intermail.mail.AbstractMssOperation;
import com.opwvmsg.utils.paf.intermail.mail.MimeMsg;
import com.opwvmsg.utils.paf.intermail.mail.MsRMId;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

public class UpdateMsgBlobStream extends AbstractMssOperation {

    // input
    private String name;
    private UUID folderUUID;
    private UUID msgUUID;
    private byte[] textBytes;

    // output
    private byte[] msgBytes;
    private int numBytes;

    // internal
    private String url;
    private String hostHeader;

    /**
     * @param host The MSS hostname for the user
     * @param name The MSS id of the user
     * @param realm The mailRealm of the user
     * @param folderUUID Folder from which we want list of messages.
     * @param msgUUIDs Only one msgUUID as part of this parameter.
     * @param text message text to be updated.
     */
    public UpdateMsgBlobStream(String host, String name, String realm,
            UUID folderUUID, UUID msgUUID, String text) {
        super(RmeDataModel.Leopard);
        this.host = resolveClusterToMSS(host, name);
        this.name = name;
        url = getMsUrl(host, name, MSS_SL_UPDATEMSGCONTENT);
        this.folderUUID = folderUUID;
        this.msgUUID = msgUUID;
        textBytes = new byte[text.length()];
        for (int i = 0; i < text.length(); i++) {
            textBytes[i] = (byte) text.charAt(i);
        }
        this.hostHeader = getHostHeader(host, name, realm);
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#execute()
     */
    public void execute() throws IntermailException {
        if (host == null || name == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadURL", new String[] { url }));
        }
        if (folderUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "folderUUID", "GetMsgStream" }));
        }
        if (msgUUID == null) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.BadRequestArg", new String[] {
                            "msgUUID", "GetMsgStream" }));
        }

        if (rmeDataModel != RmeDataModel.Leopard) {
            throw new IntermailException(LogEvent.ERROR,
                    LogEvent.formatLogString("Ms.OpNotSupported", new String[] {
                            "MSS_SL_UPDATEMSGCONTENT", host }));
        } else {
            callRme(MSS_SL_UPDATEMSGCONTENT);
        }

    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#sendData()
     */
    protected void sendData() throws IOException {

    }

    /**
     * This creates an RME Message object given the content provided.
     * 
     * @throws IOException
     */
    private void writeMessage() throws IOException {

        outStream.writeBoolean(true); // have it

        outStream.writeString(null);

        // write raw string bytes - no length prefix
        byte[] bytes2 = new byte[5];
        bytes2[4] = 0;
        for (int i = 0; i < 4; i++) {
            bytes2[i] = (byte) "SMTP".charAt(i);
        }
        outStream.write(bytes2, 0, bytes2.length);
        outStream.writeInt(0); // empty recips
        outStream.writeInt((int) (System.currentTimeMillis() / 1000));
        outStream.writeInt(textBytes.length); // mimeSize
        MimeMsg mimeMessage = new MimeMsg(textBytes, 0, textBytes.length);
        mimeMessage.writeToStream(outStream);
        outStream.writeInt(0); // firstAccessedTime
        outStream.writeInt(0); // lastAccessedTime
        outStream.writeInt(0); // expiryTime
        outStream.writeInt(0); // firstSeenTime
        outStream.writeInt(0); // no attributes
        outStream.writeInt(1); // 1 body part;
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x8648862A); // oid part 1
        outStream.writeInt(0x34030FF7); // oid part2
        outStream.writeByte(8); // oid byte length;
        outStream.writeInt(0x8648862A); // p1
        outStream.writeInt(0x36040FF7); // x400 oid
        outStream.writeByte(0x0A); // unknown mime message
        outStream.writeString("X-Intermail-Unknown-MIME-Type=unparsedmessage");
        outStream.writeString("");
        outStream.writeString("");
        outStream.writeString("");
        // body text, finally!
        int textLen = textBytes.length;
        outStream.writeInt(textLen);
        // write raw string bytes - no null terminate
        outStream.write(textBytes, 0, textBytes.length);
        // done!
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    protected void receiveData() throws IOException {
        // Legacy RME operation is not supported
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void constructHttpData() throws IOException {
        postMethod.addHeader(HTTP_HOST_KEY, hostHeader);
        postMethod.addHeader(HTTP_CONTENT_TYPE_KEY, HTTP_CONTENT_OCTET_STREAM);
        postMethod.addHeader(HTTP_RMECLASS_KEY, HTTP_RMECLASS_1);
        postMethod.addHeader(HTTP_RMEOPERATION_KEY,
                Integer.toString(MSS_SL_UPDATEMSGCONTENT));
        postMethod.addHeader(HTTP_RMEVERSION_KEY, HTTP_RMEVERSION);

        MsRMId msRMId = new MsRMId();
        msRMId.writeMsRMId(outStream);

        outStream.writeString(url);
        writeMessage();
        outStream.writeUUID(folderUUID);
        outStream.writeUUID(msgUUID);

        outStream.flush();

    }

    /**
     * (non-Javadoc)
     * 
     * @see com.openwave.intermail.io.AbstractOperation#receiveData()
     */
    @Override
    protected void receiveHttpData() throws IOException {
        logEvents = inStream.readLogEvent();
    }

    /**
     * @return the msgBytes
     */
    public byte[] getMsgBytes() {
        return msgBytes;
    }

    /**
     * @return the numBytes
     */
    public int getNumBytes() {
        return numBytes;
    }
}
