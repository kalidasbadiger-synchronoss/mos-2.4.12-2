
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Flags object of Task
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "hidenFlag",
    "hidenSubFoldersFlag",
    "publicationFlag",
    "subscriptionFlag"
})
public class Flags implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenFlag")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType hidenFlag;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenSubFoldersFlag")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType hidenSubFoldersFlag;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("publicationFlag")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType publicationFlag;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("subscriptionFlag")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType subscriptionFlag;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenFlag")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getHidenFlag() {
        return hidenFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenFlag")
    public void setHidenFlag(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType hidenFlag) {
        this.hidenFlag = hidenFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenSubFoldersFlag")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getHidenSubFoldersFlag() {
        return hidenSubFoldersFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("hidenSubFoldersFlag")
    public void setHidenSubFoldersFlag(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType hidenSubFoldersFlag) {
        this.hidenSubFoldersFlag = hidenSubFoldersFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("publicationFlag")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getPublicationFlag() {
        return publicationFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("publicationFlag")
    public void setPublicationFlag(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType publicationFlag) {
        this.publicationFlag = publicationFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("subscriptionFlag")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSubscriptionFlag() {
        return subscriptionFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("subscriptionFlag")
    public void setSubscriptionFlag(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType subscriptionFlag) {
        this.subscriptionFlag = subscriptionFlag;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
