
package com.opwvmsg.mxos.task.pojos;

import javax.annotation.Generated;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonValue;

@Generated("com.googlecode.jsonschema2pojo")
public enum DaysInWeek {

    SUNDAY("Sunday"),
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday");
    private final String value;

    private DaysInWeek(String value) {
        this.value = value;
    }

    @JsonValue
    @Override
    public String toString() {
        return this.value;
    }

    @JsonCreator
    public static DaysInWeek fromValue(String value) {
        for (DaysInWeek c: DaysInWeek.values()) {
            if (c.value.equalsIgnoreCase(value)) {
                return c;
            }
        }
        throw new IllegalArgumentException(value);
    }

    @JsonCreator
    public static DaysInWeek fromOrdinal(Integer value) {
        for (DaysInWeek c: DaysInWeek.values()) {
            if (c.ordinal() == value) {
                return c;
            }
        }
        throw new IllegalArgumentException();
    }

}
