
package com.opwvmsg.mxos.task.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Task object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "taskBase",
    "recurrence",
    "participants",
    "details",
    "attachments"
})
public class Task implements Serializable
{

    /**
     * Base object of Task
     * (Required)
     * 
     */
    @JsonProperty("taskBase")
    private TaskBase taskBase;
    /**
     * Recurrence object of Task
     * (Required)
     * 
     */
    @JsonProperty("recurrence")
    private Recurrence recurrence;
    /**
     * Participant objects of Task
     * 
     */
    @JsonProperty("participants")
    private List<Participant> participants;
    /**
     * Detail object of task
     * (Required)
     * 
     */
    @JsonProperty("details")
    private Details details;
    /**
     * attachment objects of task
     * 
     */
    @JsonProperty("attachments")
    private List<Attachment> attachments;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Base object of Task
     * (Required)
     * 
     */
    @JsonProperty("taskBase")
    public TaskBase getTaskBase() {
        return taskBase;
    }

    /**
     * Base object of Task
     * (Required)
     * 
     */
    @JsonProperty("taskBase")
    public void setTaskBase(TaskBase taskBase) {
        this.taskBase = taskBase;
    }

    /**
     * Recurrence object of Task
     * (Required)
     * 
     */
    @JsonProperty("recurrence")
    public Recurrence getRecurrence() {
        return recurrence;
    }

    /**
     * Recurrence object of Task
     * (Required)
     * 
     */
    @JsonProperty("recurrence")
    public void setRecurrence(Recurrence recurrence) {
        this.recurrence = recurrence;
    }

    /**
     * Participant objects of Task
     * 
     */
    @JsonProperty("participants")
    public List<Participant> getParticipants() {
        return participants;
    }

    /**
     * Participant objects of Task
     * 
     */
    @JsonProperty("participants")
    public void setParticipants(List<Participant> participants) {
        this.participants = participants;
    }

    /**
     * Detail object of task
     * (Required)
     * 
     */
    @JsonProperty("details")
    public Details getDetails() {
        return details;
    }

    /**
     * Detail object of task
     * (Required)
     * 
     */
    @JsonProperty("details")
    public void setDetails(Details details) {
        this.details = details;
    }

    /**
     * attachment objects of task
     * 
     */
    @JsonProperty("attachments")
    public List<Attachment> getAttachments() {
        return attachments;
    }

    /**
     * attachment objects of task
     * 
     */
    @JsonProperty("attachments")
    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
