
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Filters object for mailSend and mailReceipt
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "sieveFilters",
    "bmiFilters",
    "commtouchFilters",
    "cloudmarkFilters",
    "mcAfeeFilters"
})
public class Filters implements Serializable
{

    /**
     * sieve filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("sieveFilters")
    private SieveFilters sieveFilters;
    /**
     * bmi filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("bmiFilters")
    private BmiFilters bmiFilters;
    /**
     * commtouch filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("commtouchFilters")
    private CommtouchFilters commtouchFilters;
    /**
     * cloudmarkFilters filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkFilters")
    private CloudmarkFilters cloudmarkFilters;
    /**
     * mcAfeeFilters filters object of mailSend and mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeFilters")
    private McAfeeFilters mcAfeeFilters;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * sieve filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("sieveFilters")
    public SieveFilters getSieveFilters() {
        return sieveFilters;
    }

    /**
     * sieve filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("sieveFilters")
    public void setSieveFilters(SieveFilters sieveFilters) {
        this.sieveFilters = sieveFilters;
    }

    /**
     * bmi filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("bmiFilters")
    public BmiFilters getBmiFilters() {
        return bmiFilters;
    }

    /**
     * bmi filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("bmiFilters")
    public void setBmiFilters(BmiFilters bmiFilters) {
        this.bmiFilters = bmiFilters;
    }

    /**
     * commtouch filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("commtouchFilters")
    public CommtouchFilters getCommtouchFilters() {
        return commtouchFilters;
    }

    /**
     * commtouch filters object of mailSend filters
     * (Required)
     * 
     */
    @JsonProperty("commtouchFilters")
    public void setCommtouchFilters(CommtouchFilters commtouchFilters) {
        this.commtouchFilters = commtouchFilters;
    }

    /**
     * cloudmarkFilters filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkFilters")
    public CloudmarkFilters getCloudmarkFilters() {
        return cloudmarkFilters;
    }

    /**
     * cloudmarkFilters filters object of mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("cloudmarkFilters")
    public void setCloudmarkFilters(CloudmarkFilters cloudmarkFilters) {
        this.cloudmarkFilters = cloudmarkFilters;
    }

    /**
     * mcAfeeFilters filters object of mailSend and mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeFilters")
    public McAfeeFilters getMcAfeeFilters() {
        return mcAfeeFilters;
    }

    /**
     * mcAfeeFilters filters object of mailSend and mailReceipt filters
     * (Required)
     * 
     */
    @JsonProperty("mcAfeeFilters")
    public void setMcAfeeFilters(McAfeeFilters mcAfeeFilters) {
        this.mcAfeeFilters = mcAfeeFilters;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
