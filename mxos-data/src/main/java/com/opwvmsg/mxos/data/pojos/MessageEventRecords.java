
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MessageEventRecords object of internalInfo
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "eventRecordingEnabled",
    "maxHeaderLength",
    "maxFilesSizeKB"
})
public class MessageEventRecords implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("eventRecordingEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType eventRecordingEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("maxHeaderLength")
    private Integer maxHeaderLength;
    @org.codehaus.jackson.annotate.JsonProperty("maxFilesSizeKB")
    private Integer maxFilesSizeKB;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("eventRecordingEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType getEventRecordingEnabled() {
        return eventRecordingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("eventRecordingEnabled")
    public void setEventRecordingEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType eventRecordingEnabled) {
        this.eventRecordingEnabled = eventRecordingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxHeaderLength")
    public Integer getMaxHeaderLength() {
        return maxHeaderLength;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxHeaderLength")
    public void setMaxHeaderLength(Integer maxHeaderLength) {
        this.maxHeaderLength = maxHeaderLength;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFilesSizeKB")
    public Integer getMaxFilesSizeKB() {
        return maxFilesSizeKB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("maxFilesSizeKB")
    public void setMaxFilesSizeKB(Integer maxFilesSizeKB) {
        this.maxFilesSizeKB = maxFilesSizeKB;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
