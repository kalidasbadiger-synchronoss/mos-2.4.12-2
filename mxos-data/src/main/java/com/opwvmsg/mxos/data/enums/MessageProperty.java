/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All LDAP attributes used by this application.
 *
 * @see <i>Email Mx LDAP schema Reference Guide</i>
 * @author mxos-dev
 */
public enum MessageProperty implements DataMap.Property {

    /**
     * <tt>MessageId</tt>: Specifies MessageId for the Openwave Email
     * application. This id uniquely identifies message.
     *
     * @since mxos 2.0
     */
    messageId,

    /**
     * <tt>Status</tt>: Specifies message Status e.g. soft deleted, etc.
     *
     * @since mxos 2.0
     */
    status,

    /**
     * <tt>Uid</tt>: Specifies IMAP UID.
     *
     * @since mxos 2.0
     */
    uid,

    /**
     * <tt>Flags</tt>: Specifies overall message Flags.
     *
     * @since mxos 2.0
     */
    flags,

    /**
     * <tt>FlagRecent</tt>: Specifies flagsRecent.
     *
     * @since mxos 2.0
     */
    flagRecent,

    /**
     * <tt>FlagSeen</tt>: Specifies FlagSeen.
     *
     * @since mxos 2.0
     */
    flagSeen,

    /**
     * <tt>FlagUnread</tt>: Specifies FlagUnread.
     *
     * @since mxos 2.0
     */
    flagUnread,

    /**
     * <tt>FlagAns</tt>: Specifies FlagAns.
     *
     * @since mxos 2.0
     */
    flagAns,

    /**
     * <tt>FlagFlagged</tt>: Specifies FlagFlagged.
     *
     * @since mxos 2.0
     */
    flagFlagged,

    /**
     * <tt>FlagDel</tt>: Specifies FlagDel.
     *
     * @since mxos 2.0
     */
    flagDel,

    /**
     * <tt>FlagRes</tt>: Specifies FlagRes.
     *
     * @since mxos 2.0
     */
    flagRes,

    /**
     * <tt>FlagBounce</tt>: Specifies FlagBounce.
     *
     * @since mxos 2.0
     */
    flagBounce,

    /**
     * <tt>FlagPriv</tt>: Specifies FlagPriv.
     *
     * @since mxos 2.0
     */
    flagPriv,

    /**
     * <tt>FlagDraft</tt>: Specifies FlagDraft.
     *
     * @since mxos 2.0
     */
    flagDraft,

    /**
     * <tt>Type</tt>: Specifies message Type e.g. VOICE MAIL, EMAIL, etc.
     *
     * @since mxos 2.0
     */
    type,

    /**
     * <tt>Priority</tt>: Specifies message Priority.
     *
     * @since mxos 2.0
     */
    priority,

    /**
     * <tt>DeliverNDR</tt>: Specifies message delivery NDR report.
     *
     * @since mxos 2.0
     */

    deliverNDR,

    /**
     * <tt>ArrTime</tt>: Specifies message arrival time.
     *
     * @since mxos 2.0
     */
    arrivalTime,
    
    /**
     * <tt>firstSeenTime</tt>: Specifies message first seen time.
     *
     * @since mxos 2.0
     */
    firstSeenTime,
    
    /**
     * <tt>FirstccessedTime</tt>: Specifies message first access time.
     *
     * @since mxos 2.0
     */
    firstAccessedTime,

    /**
     * <tt>LastAccessedTime</tt>: Specifies message last access time.
     *
     * @since mxos 2.0
     */
    lastAccessedTime,

    /**
     * <tt>ExpTime</tt>: Specifies message expiry time.
     *
     * @since mxos 2.0
     */
    expireOn,

    /**
     * <tt>Size</tt>: Specifies message Size.
     *
     * @since mxos 2.0
     */
    size,

    /**
     * <tt>Count</tt>: Contains message sharing Count, for dedup purpose.
     *
     * @since mxos 2.0
     */
    count,

    /**
     * <tt>CompressionType</tt>: Contains message blob compression status.
     *
     * @since mxos 2.0
     */
    compressiontype,

    /**
     * <tt>MessageBody</tt>: Contains actual message body.
     *
     * @since mxos 2.0
     */
    messagebody,

    /**
     * <tt>SearchQuery</tt>: Contains Search Query parameters.
     *
     * @since mxos 2.0
     */
    searchquery,

    /**
     * <tt>SortQuery</tt>: Contains Sort Query parameters.
     *
     * @since mxos 2.0
     */
    sortquery,

    /**
     * <tt>optionFolderIsHint</tt>: Contains optionFolderIsHint.
     *
     * @since mxos 2.0
     */
    optionFolderIsHint,

    /**
     * <tt>optionMultipleOk</tt>: Contains optionMultipleOk.
     *
     * @since mxos 2.0
     */
    optionMultipleOk,

    /**
     * <tt>optionGetDestFolder</tt>: Contains optionGetDestFolder.
     *
     * @since mxos 2.0
     */
    optionGetDestFolder,

    /**
     * <tt>optionDeleteOriginals</tt>: Contains optionDeleteOriginals.
     *
     * @since mxos 2.0
     */
    optionDeleteOriginals,

    /**
     * <tt>optionSupressMers</tt>: Contains optionSupressMers.
     *
     * @since mxos 2.0
     */
    optionSupressMers,

    /**
     * <tt>isAdmin</tt>
     *
     * @since mxos 2.0
     */
    isAdmin,
 
    /**
     * <tt>offset</tt>
     *
     * @since mxos 2.0
     */
    offset,

    /**
     * <tt>lenght</tt>
     *
     * @since mxos 2.0
     */
    length,

    /**
     * <tt>isPrivate</tt>
     *
     * @since mxos 2.0
     */
    isPrivate,

    /**
     * <tt>expireTime</tt>
     *
     * @since mxos 2.0
     */
    expireTime,

    /**
     * <tt>keywords</tt>
     *
     * @since mxos 2.0
     */
    keywords,

    /**
     * <tt>keywords</tt>
     *
     * @since mxos 2.1
     */
    deleteKeyword,
    
    /**
     * <tt>oldMsgId</tt>
     *
     * @since mxos 2.0
     */
    oldMsgId,
    
    /**
     * <tt>ignoreQuotaFlag</tt>
     *
     * @since mxos 2.1.30
     */
    ignoreQuotaFlag,

    /**
     * <tt>popDeletedFlag</tt>
     *
     * @since mxos 2.0
     */
    popDeletedFlag,
    
    /**
     * <tt>conversationViewReq</tt>
     *
     * @since mxos 2.0
     */
    conversationViewReq,
    
    hasAttachments,
    
    richMailFlag,
    
    subject,
    
    from,
    
    to,
    
    cc,
    
    sentDate,
    
    threadId,
    
    sender,
    
    bcc,

    message, 
    
    msgBase64Encoded,
    
    /**
     * <tt>disableNotificationFlag</tt>
     *
     * @since mxos 2.0
     */
    disableNotificationFlag
    
}
