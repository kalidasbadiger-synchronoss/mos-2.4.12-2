/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All OX setting attributes used by mOS
 *
 * @author mxos-dev
 *
 */
public enum OXSettingsProperty implements DataMap.Property {
    
    // for ox DB field selection
    ox_setting,
    
    // user
    preferredLanguage,
    
    // user setting mail
    send_addr;

}
