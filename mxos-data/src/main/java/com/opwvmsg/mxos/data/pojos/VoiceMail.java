
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Voice Mail object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "voiceMailAccessEnabled",
    "faxAccessEnabled",
    "voiceMailFaxReplyEnabled",
    "voiceMailCallFlow",
    "voiceMailPromptLevel",
    "voiceMailSkipPin",
    "autoPlayVoiceMail",
    "autoPlayVoiceMailDateTime",
    "autoPrintFaxMessage",
    "autoPrintVoiceMail",
    "voiceMailLocale",
    "extendedAbsenceControl",
    "voiceMailOutDialOption"
})
public class VoiceMail implements Serializable
{

    @JsonProperty("voiceMailAccessEnabled")
    private String voiceMailAccessEnabled;
    @JsonProperty("faxAccessEnabled")
    private String faxAccessEnabled;
    @JsonProperty("voiceMailFaxReplyEnabled")
    private String voiceMailFaxReplyEnabled;
    @JsonProperty("voiceMailCallFlow")
    private String voiceMailCallFlow;
    @JsonProperty("voiceMailPromptLevel")
    private String voiceMailPromptLevel;
    @JsonProperty("voiceMailSkipPin")
    private String voiceMailSkipPin;
    @JsonProperty("autoPlayVoiceMail")
    private String autoPlayVoiceMail;
    @JsonProperty("autoPlayVoiceMailDateTime")
    private String autoPlayVoiceMailDateTime;
    @JsonProperty("autoPrintFaxMessage")
    private String autoPrintFaxMessage;
    @JsonProperty("autoPrintVoiceMail")
    private String autoPrintVoiceMail;
    @JsonProperty("voiceMailLocale")
    private String voiceMailLocale;
    @JsonProperty("extendedAbsenceControl")
    private String extendedAbsenceControl;
    @JsonProperty("voiceMailOutDialOption")
    private String voiceMailOutDialOption;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("voiceMailAccessEnabled")
    public String getVoiceMailAccessEnabled() {
        return voiceMailAccessEnabled;
    }

    @JsonProperty("voiceMailAccessEnabled")
    public void setVoiceMailAccessEnabled(String voiceMailAccessEnabled) {
        this.voiceMailAccessEnabled = voiceMailAccessEnabled;
    }

    @JsonProperty("faxAccessEnabled")
    public String getFaxAccessEnabled() {
        return faxAccessEnabled;
    }

    @JsonProperty("faxAccessEnabled")
    public void setFaxAccessEnabled(String faxAccessEnabled) {
        this.faxAccessEnabled = faxAccessEnabled;
    }

    @JsonProperty("voiceMailFaxReplyEnabled")
    public String getVoiceMailFaxReplyEnabled() {
        return voiceMailFaxReplyEnabled;
    }

    @JsonProperty("voiceMailFaxReplyEnabled")
    public void setVoiceMailFaxReplyEnabled(String voiceMailFaxReplyEnabled) {
        this.voiceMailFaxReplyEnabled = voiceMailFaxReplyEnabled;
    }

    @JsonProperty("voiceMailCallFlow")
    public String getVoiceMailCallFlow() {
        return voiceMailCallFlow;
    }

    @JsonProperty("voiceMailCallFlow")
    public void setVoiceMailCallFlow(String voiceMailCallFlow) {
        this.voiceMailCallFlow = voiceMailCallFlow;
    }

    @JsonProperty("voiceMailPromptLevel")
    public String getVoiceMailPromptLevel() {
        return voiceMailPromptLevel;
    }

    @JsonProperty("voiceMailPromptLevel")
    public void setVoiceMailPromptLevel(String voiceMailPromptLevel) {
        this.voiceMailPromptLevel = voiceMailPromptLevel;
    }

    @JsonProperty("voiceMailSkipPin")
    public String getVoiceMailSkipPin() {
        return voiceMailSkipPin;
    }

    @JsonProperty("voiceMailSkipPin")
    public void setVoiceMailSkipPin(String voiceMailSkipPin) {
        this.voiceMailSkipPin = voiceMailSkipPin;
    }

    @JsonProperty("autoPlayVoiceMail")
    public String getAutoPlayVoiceMail() {
        return autoPlayVoiceMail;
    }

    @JsonProperty("autoPlayVoiceMail")
    public void setAutoPlayVoiceMail(String autoPlayVoiceMail) {
        this.autoPlayVoiceMail = autoPlayVoiceMail;
    }

    @JsonProperty("autoPlayVoiceMailDateTime")
    public String getAutoPlayVoiceMailDateTime() {
        return autoPlayVoiceMailDateTime;
    }

    @JsonProperty("autoPlayVoiceMailDateTime")
    public void setAutoPlayVoiceMailDateTime(String autoPlayVoiceMailDateTime) {
        this.autoPlayVoiceMailDateTime = autoPlayVoiceMailDateTime;
    }

    @JsonProperty("autoPrintFaxMessage")
    public String getAutoPrintFaxMessage() {
        return autoPrintFaxMessage;
    }

    @JsonProperty("autoPrintFaxMessage")
    public void setAutoPrintFaxMessage(String autoPrintFaxMessage) {
        this.autoPrintFaxMessage = autoPrintFaxMessage;
    }

    @JsonProperty("autoPrintVoiceMail")
    public String getAutoPrintVoiceMail() {
        return autoPrintVoiceMail;
    }

    @JsonProperty("autoPrintVoiceMail")
    public void setAutoPrintVoiceMail(String autoPrintVoiceMail) {
        this.autoPrintVoiceMail = autoPrintVoiceMail;
    }

    @JsonProperty("voiceMailLocale")
    public String getVoiceMailLocale() {
        return voiceMailLocale;
    }

    @JsonProperty("voiceMailLocale")
    public void setVoiceMailLocale(String voiceMailLocale) {
        this.voiceMailLocale = voiceMailLocale;
    }

    @JsonProperty("extendedAbsenceControl")
    public String getExtendedAbsenceControl() {
        return extendedAbsenceControl;
    }

    @JsonProperty("extendedAbsenceControl")
    public void setExtendedAbsenceControl(String extendedAbsenceControl) {
        this.extendedAbsenceControl = extendedAbsenceControl;
    }

    @JsonProperty("voiceMailOutDialOption")
    public String getVoiceMailOutDialOption() {
        return voiceMailOutDialOption;
    }

    @JsonProperty("voiceMailOutDialOption")
    public void setVoiceMailOutDialOption(String voiceMailOutDialOption) {
        this.voiceMailOutDialOption = voiceMailOutDialOption;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
