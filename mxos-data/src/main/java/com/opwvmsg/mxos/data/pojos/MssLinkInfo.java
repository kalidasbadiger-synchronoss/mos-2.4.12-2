
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MssLinkInfo object used to connect MSS
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "mailboxId",
    "messageStoreHosts",
    "autoReplyHost",
    "realm",
    "isAdmin"
})
public class MssLinkInfo implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    private String mailboxId;
    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    private List<String> messageStoreHosts;
    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    private String autoReplyHost;
    @org.codehaus.jackson.annotate.JsonProperty("realm")
    private String realm;
    @org.codehaus.jackson.annotate.JsonProperty("isAdmin")
    private com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse isAdmin;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public String getMailboxId() {
        return mailboxId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("mailboxId")
    public void setMailboxId(String mailboxId) {
        this.mailboxId = mailboxId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    public List<String> getMessageStoreHosts() {
        return messageStoreHosts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    public void setMessageStoreHosts(List<String> messageStoreHosts) {
        this.messageStoreHosts = messageStoreHosts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    public String getAutoReplyHost() {
        return autoReplyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    public void setAutoReplyHost(String autoReplyHost) {
        this.autoReplyHost = autoReplyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("realm")
    public String getRealm() {
        return realm;
    }

    @org.codehaus.jackson.annotate.JsonProperty("realm")
    public void setRealm(String realm) {
        this.realm = realm;
    }

    @org.codehaus.jackson.annotate.JsonProperty("isAdmin")
    public com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse getIsAdmin() {
        return isAdmin;
    }

    @org.codehaus.jackson.annotate.JsonProperty("isAdmin")
    public void setIsAdmin(com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
