/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Contacts attributes referenced in WebEdge by this application.
 * 
 * @author mxos-dev
 */
public enum WEContactsProperty implements DataMap.Property {

    // common
    Id,

    // base
    Notes,

    // name
    FirstName, MiddleName, LastName, DisplayName, Nickname, Title,

    // personalInfo
    Spouse, HomeUrl, Children, Pager,

    // personalInfoAddress
    HomeCity, HomeState, HomeZip, HomeCountry, HomeAddress1,

    // personalInfoCommunication
    HomePhone, OtherPhone, MobilePhone, HomeFax, AltEmail, ImProvider,

    // workInfo
    Profession, Company, Department, WorkUrl,

    // workInfoAddress
    WorkCity, WorkState, WorkZip, WorkCountry, WorkAddress1,

    // workInfoCommunication
    WorkPhone, WorkFax, Email,

    // events
    BirthdayDay, BirthdayMonth, BirthdayYear, AnniversaryDay, AnniversaryMonth, AnniversaryYear,

    // to-be-mapped
     BuddyName,  Website, PGPKey, 

    //Groups
    listMembers, listName, listDescription,
    
    EntryType;

    @Override
    public String toString() {
        // This is required, as enum name cannot contain '.'.
        if (this == EntryType) {
            return "entry.type";
        } else if (this == listMembers) {
            return "list.Members";
        } else if (this == listName) {
            return "list.Name";
        } else if(this == listDescription) {
            return "list.Description";
        } else {
            return super.toString();
        }
    }
}
