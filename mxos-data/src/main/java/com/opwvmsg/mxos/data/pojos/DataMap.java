/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-data/src/main/java/com/openwave/mxos/data/DataMap.java#1 $
 */

package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * DataMap that consists of identity and its properties.
 *
 * @author Aricent
 */
public class DataMap implements Serializable {
    private static final long serialVersionUID = 189933857252886374L;

    /**
     * DataMap property enum has to implement this interface.
     */
    public interface Property {

        /**
         * This method overlaps {@link Enum#name()}.
         *
         * @return the name of this enum constant
         */
        String name();
    }
    private final Map<String, Object> props = new HashMap<String, Object>();

    /**
     * Creates an DataMap object.
     *
     * @param init properties
     */
    public DataMap(Map<Property, ? extends Object> init) {
        for (Map.Entry<Property, ? extends Object> entry : init.entrySet()) {
            setProperty(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Creates an DataMap object.
     *
     */
    public DataMap() {
    }

    /**
     * Clear the DataMap.
     */
    public void clear() {
        props.clear();
    }

    /**
     * Returns whether the specfied property has been loaded.
     *
     * @param key property key
     * @return true if loaded, otherwise false
     */
    public boolean contains(Property key) {
        return contains(key.name());
    }

    /**
     * Returns whether the specfied property has been loaded.
     *
     * @param key property key
     * @return true if loaded, otherwise false
     */
    public boolean contains(String key) {
        return props.containsKey(key.toLowerCase());
    }
    /**
     * Returns Set of Keys.
     *
     * @return Set of Keys.
     */
    public Set<String> getKeySet() {
        return props.keySet();
    }
    /**
     * Returns a property.
     *
     * @param key property key
     * @return property value
     */
    public String getProperty(Property key) {
        return (String) props.get(key.name().toLowerCase());
    }
    /**
     * Returns a property.
     *
     * @param key property key
     * @return property value
     */
    public Object getPropertyAsObject(Property key) {
        return props.get(key.name().toLowerCase());
    }

    /**
     * Returns a property.
     *
     * @param key property key
     * @return property value
     */
    public String getProperty(String key) {
        return (String) props.get(key.toLowerCase());
    }

    /**
     * Returns a string property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if available, otherwise default value
     */
    public String getProperty(Property key, String defaultValue) {
        return getProperty(key.name(), defaultValue);
    }

    /**
     * Returns a string array property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if available, otherwise default value
     */
    public String[] getProperty(Property key, String[] defaultValue) {
        return getProperty(key.name(), defaultValue);
    }

    /**
     * Returns an integer property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if available, otherwise default value
     */
    public int getProperty(Property key, int defaultValue) {
        String value = (String) props.get(key.name().toLowerCase());
        if (value != null) {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                return defaultValue;
            }
        } else {
            return defaultValue;
        }
    }

    /**
     * Returns a boolean property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if available, otherwise default value
     */
    public boolean getProperty(Property key, boolean defaultValue) {
        String value = (String) props.get(key.name().toLowerCase());
        if (value != null) {
            return Boolean.parseBoolean(value);
        } else {
            return defaultValue;
        }
    }

    /**
     * Returns a property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if not null, otherwise default value
     */
    public String getProperty(String key, String defaultValue) {
        String value = (String) props.get(key.toLowerCase());
        return value != null ? value : defaultValue;
    }

    /**
     * Returns a string array property.
     *
     * @param key property key
     * @param defaultValue default value
     * @return property value if available, otherwise default value
     */
    public String[] getProperty(String key, String[] defaultValue) {
        Object value = props.get(key.toLowerCase());
        if (value == null) {
            return defaultValue;
        } else if (value instanceof String[]) {
            return (String[]) value;
        } else {
            return new String[]{(String) value};
        }
    }

    /**
     * Sets a property.
     *
     * @param key property key
     * @param value property value
     */
    public void setProperty(Property key, Object value) {
        props.put(key.name().toLowerCase(), value);
    }

    /**
     * Sets a property.
     *
     * @param key property key
     * @param value property value
     */
    public void setProperty(String key, Object value) {
        props.put(key.toLowerCase(), value);
    }

    /**
     * Returns string representation of this object.
     *
     * @return string representation of this object
     */
    @Override
    public String toString() {
        // Do not display Password.
        StringBuffer sb = new StringBuffer("");
        for (Map.Entry<String, Object> entry : props.entrySet()) {
            if (entry.getKey().equalsIgnoreCase("mailpassword")) {
                sb.append(entry.getKey() + "=XXXXXXXXXX, ");
            } else {
                sb.append(entry.getKey() + "=" + entry.getValue() + ", ");
            }
        }
        return new String(sb);
    }

}
