/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * All Notification Service attributes used by this application.
 * 
 * @author mxos-dev
 */
public enum NotificationProperty implements DataMap.Property {
    
    /**
     * <tt>topic</tt>: Specifies topic created by IMAP server. ImapServ creates 
     * topic and adds subscriptions that would be looked up by MSS. 
     *
     * @since mxos 2.0
     */
    topic,

    /**
     * <tt>subscription</tt>: Subscription is an arbitrary string used by IMAP
     * server to store the clientId and callback URL
     *
     * @since mxos 2.0
     */
    subscription,
    
    /**
     * <tt>message</tt>: JSON Message object received for publish.
     *
     * @since mxos 2.0
     */
    notifyMessage
}
