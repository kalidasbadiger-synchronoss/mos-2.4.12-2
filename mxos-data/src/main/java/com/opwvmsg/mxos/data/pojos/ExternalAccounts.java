
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * External Accounts object of Mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "externalMailAccountsAllowed",
    "promptForExternalAccountSync",
    "mailAccounts"
})
public class ExternalAccounts implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("externalMailAccountsAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType externalMailAccountsAllowed;
    @org.codehaus.jackson.annotate.JsonProperty("promptForExternalAccountSync")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType promptForExternalAccountSync;
    @org.codehaus.jackson.annotate.JsonProperty("mailAccounts")
    private List<MailAccount> mailAccounts;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("externalMailAccountsAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getExternalMailAccountsAllowed() {
        return externalMailAccountsAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("externalMailAccountsAllowed")
    public void setExternalMailAccountsAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType externalMailAccountsAllowed) {
        this.externalMailAccountsAllowed = externalMailAccountsAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("promptForExternalAccountSync")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getPromptForExternalAccountSync() {
        return promptForExternalAccountSync;
    }

    @org.codehaus.jackson.annotate.JsonProperty("promptForExternalAccountSync")
    public void setPromptForExternalAccountSync(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType promptForExternalAccountSync) {
        this.promptForExternalAccountSync = promptForExternalAccountSync;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailAccounts")
    public List<MailAccount> getMailAccounts() {
        return mailAccounts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("mailAccounts")
    public void setMailAccounts(List<MailAccount> mailAccounts) {
        this.mailAccounts = mailAccounts;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
