
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailStore object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "largeMailboxPlatformEnabled",
    "maxMessages",
    "maxStorageSizeKB",
    "maxMessagesSoftLimit",
    "maxStorageSizeKBSoftLimit",
    "mobileMaxMessages",
    "mobileMaxStorageSizeKB",
    "mobileMaxMessagesSoftLimit",
    "mobileMaxStorageSizeKBSoftLimit",
    "quotaWarningThreshold",
    "quotaBounceNotify",
    "folderQuota",
    "groupAdminAllocations",
    "externalStore"
})
public class MailStore implements Serializable
{

    @JsonProperty("largeMailboxPlatformEnabled")
    private String largeMailboxPlatformEnabled;
    @JsonProperty("maxMessages")
    private Integer maxMessages;
    @JsonProperty("maxStorageSizeKB")
    private Long maxStorageSizeKB;
    @JsonProperty("maxMessagesSoftLimit")
    private Integer maxMessagesSoftLimit;
    @JsonProperty("maxStorageSizeKBSoftLimit")
    private Long maxStorageSizeKBSoftLimit;
    @JsonProperty("mobileMaxMessages")
    private Integer mobileMaxMessages;
    @JsonProperty("mobileMaxStorageSizeKB")
    private Long mobileMaxStorageSizeKB;
    @JsonProperty("mobileMaxMessagesSoftLimit")
    private Integer mobileMaxMessagesSoftLimit;
    @JsonProperty("mobileMaxStorageSizeKBSoftLimit")
    private Long mobileMaxStorageSizeKBSoftLimit;
    @JsonProperty("quotaWarningThreshold")
    private Integer quotaWarningThreshold;
    @JsonProperty("quotaBounceNotify")
    private String quotaBounceNotify;
    @JsonProperty("folderQuota")
    private String folderQuota;
    /**
     * groupAdminAllocations
     * <p>
     * 
     * 
     */
    @JsonProperty("groupAdminAllocations")
    private Object groupAdminAllocations;
    /**
     * externalStore
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalStore")
    private Object externalStore;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("largeMailboxPlatformEnabled")
    public String getLargeMailboxPlatformEnabled() {
        return largeMailboxPlatformEnabled;
    }

    @JsonProperty("largeMailboxPlatformEnabled")
    public void setLargeMailboxPlatformEnabled(String largeMailboxPlatformEnabled) {
        this.largeMailboxPlatformEnabled = largeMailboxPlatformEnabled;
    }

    @JsonProperty("maxMessages")
    public Integer getMaxMessages() {
        return maxMessages;
    }

    @JsonProperty("maxMessages")
    public void setMaxMessages(Integer maxMessages) {
        this.maxMessages = maxMessages;
    }

    @JsonProperty("maxStorageSizeKB")
    public Long getMaxStorageSizeKB() {
        return maxStorageSizeKB;
    }

    @JsonProperty("maxStorageSizeKB")
    public void setMaxStorageSizeKB(Long maxStorageSizeKB) {
        this.maxStorageSizeKB = maxStorageSizeKB;
    }

    @JsonProperty("maxMessagesSoftLimit")
    public Integer getMaxMessagesSoftLimit() {
        return maxMessagesSoftLimit;
    }

    @JsonProperty("maxMessagesSoftLimit")
    public void setMaxMessagesSoftLimit(Integer maxMessagesSoftLimit) {
        this.maxMessagesSoftLimit = maxMessagesSoftLimit;
    }

    @JsonProperty("maxStorageSizeKBSoftLimit")
    public Long getMaxStorageSizeKBSoftLimit() {
        return maxStorageSizeKBSoftLimit;
    }

    @JsonProperty("maxStorageSizeKBSoftLimit")
    public void setMaxStorageSizeKBSoftLimit(Long maxStorageSizeKBSoftLimit) {
        this.maxStorageSizeKBSoftLimit = maxStorageSizeKBSoftLimit;
    }

    @JsonProperty("mobileMaxMessages")
    public Integer getMobileMaxMessages() {
        return mobileMaxMessages;
    }

    @JsonProperty("mobileMaxMessages")
    public void setMobileMaxMessages(Integer mobileMaxMessages) {
        this.mobileMaxMessages = mobileMaxMessages;
    }

    @JsonProperty("mobileMaxStorageSizeKB")
    public Long getMobileMaxStorageSizeKB() {
        return mobileMaxStorageSizeKB;
    }

    @JsonProperty("mobileMaxStorageSizeKB")
    public void setMobileMaxStorageSizeKB(Long mobileMaxStorageSizeKB) {
        this.mobileMaxStorageSizeKB = mobileMaxStorageSizeKB;
    }

    @JsonProperty("mobileMaxMessagesSoftLimit")
    public Integer getMobileMaxMessagesSoftLimit() {
        return mobileMaxMessagesSoftLimit;
    }

    @JsonProperty("mobileMaxMessagesSoftLimit")
    public void setMobileMaxMessagesSoftLimit(Integer mobileMaxMessagesSoftLimit) {
        this.mobileMaxMessagesSoftLimit = mobileMaxMessagesSoftLimit;
    }

    @JsonProperty("mobileMaxStorageSizeKBSoftLimit")
    public Long getMobileMaxStorageSizeKBSoftLimit() {
        return mobileMaxStorageSizeKBSoftLimit;
    }

    @JsonProperty("mobileMaxStorageSizeKBSoftLimit")
    public void setMobileMaxStorageSizeKBSoftLimit(Long mobileMaxStorageSizeKBSoftLimit) {
        this.mobileMaxStorageSizeKBSoftLimit = mobileMaxStorageSizeKBSoftLimit;
    }

    @JsonProperty("quotaWarningThreshold")
    public Integer getQuotaWarningThreshold() {
        return quotaWarningThreshold;
    }

    @JsonProperty("quotaWarningThreshold")
    public void setQuotaWarningThreshold(Integer quotaWarningThreshold) {
        this.quotaWarningThreshold = quotaWarningThreshold;
    }

    @JsonProperty("quotaBounceNotify")
    public String getQuotaBounceNotify() {
        return quotaBounceNotify;
    }

    @JsonProperty("quotaBounceNotify")
    public void setQuotaBounceNotify(String quotaBounceNotify) {
        this.quotaBounceNotify = quotaBounceNotify;
    }

    @JsonProperty("folderQuota")
    public String getFolderQuota() {
        return folderQuota;
    }

    @JsonProperty("folderQuota")
    public void setFolderQuota(String folderQuota) {
        this.folderQuota = folderQuota;
    }

    /**
     * groupAdminAllocations
     * <p>
     * 
     * 
     */
    @JsonProperty("groupAdminAllocations")
    public Object getGroupAdminAllocations() {
        return groupAdminAllocations;
    }

    /**
     * groupAdminAllocations
     * <p>
     * 
     * 
     */
    @JsonProperty("groupAdminAllocations")
    public void setGroupAdminAllocations(Object groupAdminAllocations) {
        this.groupAdminAllocations = groupAdminAllocations;
    }

    /**
     * externalStore
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalStore")
    public Object getExternalStore() {
        return externalStore;
    }

    /**
     * externalStore
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalStore")
    public void setExternalStore(Object externalStore) {
        this.externalStore = externalStore;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
