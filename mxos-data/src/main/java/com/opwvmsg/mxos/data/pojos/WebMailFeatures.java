
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Base object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "businessFeaturesEnabled",
    "fullFeaturesEnabled",
    "lastFullFeaturesConnectionDate",
    "allowPasswordChange",
    "addressBookFeatures"
})
public class WebMailFeatures implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("businessFeaturesEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType businessFeaturesEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("fullFeaturesEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType fullFeaturesEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("lastFullFeaturesConnectionDate")
    private Object lastFullFeaturesConnectionDate;
    @org.codehaus.jackson.annotate.JsonProperty("allowPasswordChange")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType allowPasswordChange;
    /**
     * WebMailFeaturesAddressBook object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("addressBookFeatures")
    private AddressBookFeatures addressBookFeatures;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("businessFeaturesEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getBusinessFeaturesEnabled() {
        return businessFeaturesEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("businessFeaturesEnabled")
    public void setBusinessFeaturesEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType businessFeaturesEnabled) {
        this.businessFeaturesEnabled = businessFeaturesEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fullFeaturesEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getFullFeaturesEnabled() {
        return fullFeaturesEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("fullFeaturesEnabled")
    public void setFullFeaturesEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType fullFeaturesEnabled) {
        this.fullFeaturesEnabled = fullFeaturesEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastFullFeaturesConnectionDate")
    public Object getLastFullFeaturesConnectionDate() {
        return lastFullFeaturesConnectionDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastFullFeaturesConnectionDate")
    public void setLastFullFeaturesConnectionDate(Object lastFullFeaturesConnectionDate) {
        this.lastFullFeaturesConnectionDate = lastFullFeaturesConnectionDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowPasswordChange")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getAllowPasswordChange() {
        return allowPasswordChange;
    }

    @org.codehaus.jackson.annotate.JsonProperty("allowPasswordChange")
    public void setAllowPasswordChange(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType allowPasswordChange) {
        this.allowPasswordChange = allowPasswordChange;
    }

    /**
     * WebMailFeaturesAddressBook object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("addressBookFeatures")
    public AddressBookFeatures getAddressBookFeatures() {
        return addressBookFeatures;
    }

    /**
     * WebMailFeaturesAddressBook object of mailbox
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("addressBookFeatures")
    public void setAddressBookFeatures(AddressBookFeatures addressBookFeatures) {
        this.addressBookFeatures = addressBookFeatures;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
