
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Admin Control object of family mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "adminBlockedSendersList",
    "adminApprovedSendersList",
    "adminRejectAction",
    "adminRejectInfo",
    "adminDefaultDisposition"
})
public class AdminControl implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("adminBlockedSendersList")
    private List<String> adminBlockedSendersList;
    @org.codehaus.jackson.annotate.JsonProperty("adminApprovedSendersList")
    private List<String> adminApprovedSendersList;
    @org.codehaus.jackson.annotate.JsonProperty("adminRejectAction")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AdminRejectActionFlag adminRejectAction;
    @org.codehaus.jackson.annotate.JsonProperty("adminRejectInfo")
    private String adminRejectInfo;
    @org.codehaus.jackson.annotate.JsonProperty("adminDefaultDisposition")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AdminDefaultDispositionAction adminDefaultDisposition;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("adminBlockedSendersList")
    public List<String> getAdminBlockedSendersList() {
        return adminBlockedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminBlockedSendersList")
    public void setAdminBlockedSendersList(List<String> adminBlockedSendersList) {
        this.adminBlockedSendersList = adminBlockedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminApprovedSendersList")
    public List<String> getAdminApprovedSendersList() {
        return adminApprovedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminApprovedSendersList")
    public void setAdminApprovedSendersList(List<String> adminApprovedSendersList) {
        this.adminApprovedSendersList = adminApprovedSendersList;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminRejectAction")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AdminRejectActionFlag getAdminRejectAction() {
        return adminRejectAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminRejectAction")
    public void setAdminRejectAction(com.opwvmsg.mxos.data.enums.MxosEnums.AdminRejectActionFlag adminRejectAction) {
        this.adminRejectAction = adminRejectAction;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminRejectInfo")
    public String getAdminRejectInfo() {
        return adminRejectInfo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminRejectInfo")
    public void setAdminRejectInfo(String adminRejectInfo) {
        this.adminRejectInfo = adminRejectInfo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminDefaultDisposition")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AdminDefaultDispositionAction getAdminDefaultDisposition() {
        return adminDefaultDisposition;
    }

    @org.codehaus.jackson.annotate.JsonProperty("adminDefaultDisposition")
    public void setAdminDefaultDisposition(com.opwvmsg.mxos.data.enums.MxosEnums.AdminDefaultDispositionAction adminDefaultDisposition) {
        this.adminDefaultDisposition = adminDefaultDisposition;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
