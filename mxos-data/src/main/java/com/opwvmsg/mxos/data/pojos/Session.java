/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MxOSConstants.java#3 $
 */

package com.opwvmsg.mxos.data.pojos;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Search node. This class holds attributes useful for message
 * search operations like search query - search value - search operator, etc.
 *
 * @author AJ
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public final class Session {

    private String session;

    public Session(String session) {
        this.session = session;
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }
}
