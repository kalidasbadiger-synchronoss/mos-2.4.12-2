
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * MailBox object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "base",
    "webMailFeatures",
    "credentials",
    "generalPreferences",
    "mailSend",
    "mailReceipt",
    "mailAccess",
    "mailStore",
    "socialNetworks",
    "externalAccounts",
    "smsServices",
    "internalInfo"
})
public class Mailbox implements Serializable
{

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("base")
    private Base base;
    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("webMailFeatures")
    private WebMailFeatures webMailFeatures;
    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("credentials")
    private Credentials credentials;
    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("generalPreferences")
    private GeneralPreferences generalPreferences;
    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailSend")
    private MailSend mailSend;
    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailReceipt")
    private MailReceipt mailReceipt;
    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailAccess")
    private MailAccess mailAccess;
    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailStore")
    private MailStore mailStore;
    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("socialNetworks")
    private SocialNetworks socialNetworks;
    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("externalAccounts")
    private ExternalAccounts externalAccounts;
    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("smsServices")
    private SmsServices smsServices;
    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("internalInfo")
    private InternalInfo internalInfo;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("base")
    public Base getBase() {
        return base;
    }

    /**
     * Base object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("base")
    public void setBase(Base base) {
        this.base = base;
    }

    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("webMailFeatures")
    public WebMailFeatures getWebMailFeatures() {
        return webMailFeatures;
    }

    /**
     * WebMailFeatures object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("webMailFeatures")
    public void setWebMailFeatures(WebMailFeatures webMailFeatures) {
        this.webMailFeatures = webMailFeatures;
    }

    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("credentials")
    public Credentials getCredentials() {
        return credentials;
    }

    /**
     * Credentials object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("credentials")
    public void setCredentials(Credentials credentials) {
        this.credentials = credentials;
    }

    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("generalPreferences")
    public GeneralPreferences getGeneralPreferences() {
        return generalPreferences;
    }

    /**
     * GeneralPreferences object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("generalPreferences")
    public void setGeneralPreferences(GeneralPreferences generalPreferences) {
        this.generalPreferences = generalPreferences;
    }

    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailSend")
    public MailSend getMailSend() {
        return mailSend;
    }

    /**
     * MailSend object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailSend")
    public void setMailSend(MailSend mailSend) {
        this.mailSend = mailSend;
    }

    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailReceipt")
    public MailReceipt getMailReceipt() {
        return mailReceipt;
    }

    /**
     * MailReceipt object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailReceipt")
    public void setMailReceipt(MailReceipt mailReceipt) {
        this.mailReceipt = mailReceipt;
    }

    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailAccess")
    public MailAccess getMailAccess() {
        return mailAccess;
    }

    /**
     * MailAccess object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailAccess")
    public void setMailAccess(MailAccess mailAccess) {
        this.mailAccess = mailAccess;
    }

    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailStore")
    public MailStore getMailStore() {
        return mailStore;
    }

    /**
     * MailStore object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("mailStore")
    public void setMailStore(MailStore mailStore) {
        this.mailStore = mailStore;
    }

    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("socialNetworks")
    public SocialNetworks getSocialNetworks() {
        return socialNetworks;
    }

    /**
     * SocialNetworks object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("socialNetworks")
    public void setSocialNetworks(SocialNetworks socialNetworks) {
        this.socialNetworks = socialNetworks;
    }

    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("externalAccounts")
    public ExternalAccounts getExternalAccounts() {
        return externalAccounts;
    }

    /**
     * ExternalAccounts object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("externalAccounts")
    public void setExternalAccounts(ExternalAccounts externalAccounts) {
        this.externalAccounts = externalAccounts;
    }

    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("smsServices")
    public SmsServices getSmsServices() {
        return smsServices;
    }

    /**
     * SMSServices object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("smsServices")
    public void setSmsServices(SmsServices smsServices) {
        this.smsServices = smsServices;
    }

    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("internalInfo")
    public InternalInfo getInternalInfo() {
        return internalInfo;
    }

    /**
     * InternalInfo object of mailbox
     * (Required)
     * 
     */
    @JsonProperty("internalInfo")
    public void setInternalInfo(InternalInfo internalInfo) {
        this.internalInfo = internalInfo;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
