/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.data.enums;

import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * Mailbox metadata attributes for LeapFrog.
 *
 * @author mxos-dev
 */
public enum MailboxMetaProperty implements DataMap.Property {
     /**
     * <tt>mailboxid</tt>: Specifies mailboxid for the Openwave Email
     * application. This id uniquely identifies the mailbox.
     *
     * @since mxos 2.0
     */
    mailboxId,

    /**
     * <tt>status</tt>: Specifies mailbox status. For now this is reserved, in
     * future it can be used to mark mailbox soft deleted.
     *
     * @since mxos 2.0
     */
    status,

    /**
     * <tt>lastAccessDate</tt>: Specifies when mailbox was accessed last.
     * Whenever any activity happens on mailbox then this attribute needs to be
     * updated.
     *
     * @since mxos 2.0
     */
    lastAccessDate,

    /**
     * <tt>numMsgRead</tt>: Specifies count for read messages present inside a
     * mailbox.
     *
     * @since mxos 2.0
     */
    numMsgRead,

    /**
     * <tt>numMsgs</tt>: Specifies count for total messages present inside a
     * mailbox.
     *
     * @since mxos 2.0
     */
    numMsgs,

    /**
     * <tt>sizeMsgRead</tt>: Specifies total size for read messages present
     * inside a mailbox.
     *
     * @since mxos 2.0
     */
    sizeMsgRead,

    /**
     * <tt>sizeMsgs</tt>: Specifies total size for messages present inside a
     * mailbox.
     *
     * @since mxos 2.0
     */
    sizeMsgs,

    /**
     * <tt>systemFolders</tt>: Specifies system folders need to be created
     * during mailbox creation.
     *
     * @since mxos 2.0
     */
    systemFolders

}
