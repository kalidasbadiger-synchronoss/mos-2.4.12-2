/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.data.enums;

/**
 * All possible query list supported by MxOS.
 *
 * @see <i>mxos Reference Guide</i>
 *
 * @author mxos-dev
 */
public enum TasksDBTypes {

    /**
     * <tt>OX</tt>: OX database.
     *
     * @since mxos 2.0
     */
    ox
}
