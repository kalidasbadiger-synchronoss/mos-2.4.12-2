
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * InternalInfo object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "version",
    "webmailVersion",
    "selfCareAccessEnabled",
    "selfCareSSLAccessEnabled",
    "messageStoreHosts",
    "smtpProxyHost",
    "imapProxyHost",
    "popProxyHost",
    "autoReplyHost",
    "realm",
    "imapProxyAuthenticationEnabled",
    "remoteCallTracingEnabled",
    "interManagerAccessEnabled",
    "interManagerSSLAccessEnabled",
    "voiceMailAccessEnabled",
    "faxAccessEnabled",
    "ldapUtilitiesAccessType",
    "addressBookProvider",
    "messageEventRecords",
    "lastAccessDate"
})
public class InternalInfo implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("version")
    private String version;
    @org.codehaus.jackson.annotate.JsonProperty("webmailVersion")
    private String webmailVersion;
    @org.codehaus.jackson.annotate.JsonProperty("selfCareAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType selfCareAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("selfCareSSLAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType selfCareSSLAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    private List<String> messageStoreHosts;
    @org.codehaus.jackson.annotate.JsonProperty("smtpProxyHost")
    private String smtpProxyHost;
    @org.codehaus.jackson.annotate.JsonProperty("imapProxyHost")
    private String imapProxyHost;
    @org.codehaus.jackson.annotate.JsonProperty("popProxyHost")
    private String popProxyHost;
    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    private String autoReplyHost;
    @org.codehaus.jackson.annotate.JsonProperty("realm")
    private String realm;
    @org.codehaus.jackson.annotate.JsonProperty("imapProxyAuthenticationEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType imapProxyAuthenticationEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("remoteCallTracingEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType remoteCallTracingEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("interManagerAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType interManagerAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("interManagerSSLAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType interManagerSSLAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("voiceMailAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType voiceMailAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("faxAccessEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType faxAccessEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("ldapUtilitiesAccessType")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AccessType ldapUtilitiesAccessType;
    @org.codehaus.jackson.annotate.JsonProperty("addressBookProvider")
    private com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType addressBookProvider;
    /**
     * MessageEventRecords object of internalInfo
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("messageEventRecords")
    private MessageEventRecords messageEventRecords;
    @org.codehaus.jackson.annotate.JsonProperty("lastAccessDate")
    private Object lastAccessDate;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @org.codehaus.jackson.annotate.JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailVersion")
    public String getWebmailVersion() {
        return webmailVersion;
    }

    @org.codehaus.jackson.annotate.JsonProperty("webmailVersion")
    public void setWebmailVersion(String webmailVersion) {
        this.webmailVersion = webmailVersion;
    }

    @org.codehaus.jackson.annotate.JsonProperty("selfCareAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSelfCareAccessEnabled() {
        return selfCareAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("selfCareAccessEnabled")
    public void setSelfCareAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType selfCareAccessEnabled) {
        this.selfCareAccessEnabled = selfCareAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("selfCareSSLAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSelfCareSSLAccessEnabled() {
        return selfCareSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("selfCareSSLAccessEnabled")
    public void setSelfCareSSLAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType selfCareSSLAccessEnabled) {
        this.selfCareSSLAccessEnabled = selfCareSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    public List<String> getMessageStoreHosts() {
        return messageStoreHosts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("messageStoreHosts")
    public void setMessageStoreHosts(List<String> messageStoreHosts) {
        this.messageStoreHosts = messageStoreHosts;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpProxyHost")
    public String getSmtpProxyHost() {
        return smtpProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smtpProxyHost")
    public void setSmtpProxyHost(String smtpProxyHost) {
        this.smtpProxyHost = smtpProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapProxyHost")
    public String getImapProxyHost() {
        return imapProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapProxyHost")
    public void setImapProxyHost(String imapProxyHost) {
        this.imapProxyHost = imapProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popProxyHost")
    public String getPopProxyHost() {
        return popProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("popProxyHost")
    public void setPopProxyHost(String popProxyHost) {
        this.popProxyHost = popProxyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    public String getAutoReplyHost() {
        return autoReplyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("autoReplyHost")
    public void setAutoReplyHost(String autoReplyHost) {
        this.autoReplyHost = autoReplyHost;
    }

    @org.codehaus.jackson.annotate.JsonProperty("realm")
    public String getRealm() {
        return realm;
    }

    @org.codehaus.jackson.annotate.JsonProperty("realm")
    public void setRealm(String realm) {
        this.realm = realm;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapProxyAuthenticationEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getImapProxyAuthenticationEnabled() {
        return imapProxyAuthenticationEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("imapProxyAuthenticationEnabled")
    public void setImapProxyAuthenticationEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType imapProxyAuthenticationEnabled) {
        this.imapProxyAuthenticationEnabled = imapProxyAuthenticationEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("remoteCallTracingEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getRemoteCallTracingEnabled() {
        return remoteCallTracingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("remoteCallTracingEnabled")
    public void setRemoteCallTracingEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType remoteCallTracingEnabled) {
        this.remoteCallTracingEnabled = remoteCallTracingEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("interManagerAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getInterManagerAccessEnabled() {
        return interManagerAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("interManagerAccessEnabled")
    public void setInterManagerAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType interManagerAccessEnabled) {
        this.interManagerAccessEnabled = interManagerAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("interManagerSSLAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getInterManagerSSLAccessEnabled() {
        return interManagerSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("interManagerSSLAccessEnabled")
    public void setInterManagerSSLAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType interManagerSSLAccessEnabled) {
        this.interManagerSSLAccessEnabled = interManagerSSLAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("voiceMailAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getVoiceMailAccessEnabled() {
        return voiceMailAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("voiceMailAccessEnabled")
    public void setVoiceMailAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType voiceMailAccessEnabled) {
        this.voiceMailAccessEnabled = voiceMailAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("faxAccessEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getFaxAccessEnabled() {
        return faxAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("faxAccessEnabled")
    public void setFaxAccessEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType faxAccessEnabled) {
        this.faxAccessEnabled = faxAccessEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("ldapUtilitiesAccessType")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AccessType getLdapUtilitiesAccessType() {
        return ldapUtilitiesAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("ldapUtilitiesAccessType")
    public void setLdapUtilitiesAccessType(com.opwvmsg.mxos.data.enums.MxosEnums.AccessType ldapUtilitiesAccessType) {
        this.ldapUtilitiesAccessType = ldapUtilitiesAccessType;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addressBookProvider")
    public com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType getAddressBookProvider() {
        return addressBookProvider;
    }

    @org.codehaus.jackson.annotate.JsonProperty("addressBookProvider")
    public void setAddressBookProvider(com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType addressBookProvider) {
        this.addressBookProvider = addressBookProvider;
    }

    /**
     * MessageEventRecords object of internalInfo
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("messageEventRecords")
    public MessageEventRecords getMessageEventRecords() {
        return messageEventRecords;
    }

    /**
     * MessageEventRecords object of internalInfo
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("messageEventRecords")
    public void setMessageEventRecords(MessageEventRecords messageEventRecords) {
        this.messageEventRecords = messageEventRecords;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastAccessDate")
    public Object getLastAccessDate() {
        return lastAccessDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastAccessDate")
    public void setLastAccessDate(Object lastAccessDate) {
        this.lastAccessDate = lastAccessDate;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
