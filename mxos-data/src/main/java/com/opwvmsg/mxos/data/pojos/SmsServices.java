
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SMS Services object of mailbox
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "smsServicesAllowed",
    "smsServicesMsisdn",
    "smsServicesMsisdnStatus",
    "lastSmsServicesMsisdnStatusChangeDate",
    "smsOnline",
    "smsNotifications"
})
public class SmsServices implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("smsServicesAllowed")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsServicesAllowed;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdn")
    private String smsServicesMsisdn;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdnStatus")
    private com.opwvmsg.mxos.data.enums.MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdnStatus;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastSmsServicesMsisdnStatusChangeDate")
    private Object lastSmsServicesMsisdnStatusChangeDate;
    /**
     * send mail object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsOnline")
    private SmsOnline smsOnline;
    /**
     * sms notifications object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsNotifications")
    private SmsNotifications smsNotifications;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("smsServicesAllowed")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmsServicesAllowed() {
        return smsServicesAllowed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smsServicesAllowed")
    public void setSmsServicesAllowed(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsServicesAllowed) {
        this.smsServicesAllowed = smsServicesAllowed;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdn")
    public String getSmsServicesMsisdn() {
        return smsServicesMsisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdn")
    public void setSmsServicesMsisdn(String smsServicesMsisdn) {
        this.smsServicesMsisdn = smsServicesMsisdn;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdnStatus")
    public com.opwvmsg.mxos.data.enums.MxosEnums.SmsServicesMsisdnStatusType getSmsServicesMsisdnStatus() {
        return smsServicesMsisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsServicesMsisdnStatus")
    public void setSmsServicesMsisdnStatus(com.opwvmsg.mxos.data.enums.MxosEnums.SmsServicesMsisdnStatusType smsServicesMsisdnStatus) {
        this.smsServicesMsisdnStatus = smsServicesMsisdnStatus;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastSmsServicesMsisdnStatusChangeDate")
    public Object getLastSmsServicesMsisdnStatusChangeDate() {
        return lastSmsServicesMsisdnStatusChangeDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("lastSmsServicesMsisdnStatusChangeDate")
    public void setLastSmsServicesMsisdnStatusChangeDate(Object lastSmsServicesMsisdnStatusChangeDate) {
        this.lastSmsServicesMsisdnStatusChangeDate = lastSmsServicesMsisdnStatusChangeDate;
    }

    /**
     * send mail object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsOnline")
    public SmsOnline getSmsOnline() {
        return smsOnline;
    }

    /**
     * send mail object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsOnline")
    public void setSmsOnline(SmsOnline smsOnline) {
        this.smsOnline = smsOnline;
    }

    /**
     * sms notifications object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsNotifications")
    public SmsNotifications getSmsNotifications() {
        return smsNotifications;
    }

    /**
     * sms notifications object of smsServices
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("smsNotifications")
    public void setSmsNotifications(SmsNotifications smsNotifications) {
        this.smsNotifications = smsNotifications;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
