
package com.opwvmsg.mxos.data.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * SMS Notifications object of smsServices
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "smsBasicNotificationsEnabled",
    "smsAdvancedNotificationsEnabled"
})
public class SmsNotifications implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("smsBasicNotificationsEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsBasicNotificationsEnabled;
    @org.codehaus.jackson.annotate.JsonProperty("smsAdvancedNotificationsEnabled")
    private com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsAdvancedNotificationsEnabled;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("smsBasicNotificationsEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmsBasicNotificationsEnabled() {
        return smsBasicNotificationsEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smsBasicNotificationsEnabled")
    public void setSmsBasicNotificationsEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsBasicNotificationsEnabled) {
        this.smsBasicNotificationsEnabled = smsBasicNotificationsEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smsAdvancedNotificationsEnabled")
    public com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType getSmsAdvancedNotificationsEnabled() {
        return smsAdvancedNotificationsEnabled;
    }

    @org.codehaus.jackson.annotate.JsonProperty("smsAdvancedNotificationsEnabled")
    public void setSmsAdvancedNotificationsEnabled(com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType smsAdvancedNotificationsEnabled) {
        this.smsAdvancedNotificationsEnabled = smsAdvancedNotificationsEnabled;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
