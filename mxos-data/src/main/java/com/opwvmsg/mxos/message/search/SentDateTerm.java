/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the search key sentDate.
 * 
 */
public class SentDateTerm extends EpochTimeTerm {

	private static final long serialVersionUID = 7992283655110856747L;

	public SentDateTerm(char op, String epochTimeStr) {
		super(op, epochTimeStr);
	}

	@Override
	public boolean match(Message message) {
	    Metadata metadata = message.getMetadata();
		if (metadata == null || metadata.getSentDate() == null)
			return false;
		return super.match(metadata.getSentDate() * 1000);
	}

	@Override
	public String toString() {
		return new StringBuffer("(").append(MessageProperty.sentDate.name())
				.append(opStr).append(epochTimeStr).append(")").toString();
	}

}
