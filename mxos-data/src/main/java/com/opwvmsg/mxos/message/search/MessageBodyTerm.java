/*
 * Copyright (c) 2015 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.index.memory.MemoryIndex;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Message;

/**
 * This class implements comparisons for the message body. The
 * comparison is case-insensitive. The pattern is a simple string that must
 * appear as a substring in the Subject.
 */
public final class MessageBodyTerm extends StringTerm {
    /**
     * 
     */
    private static final long serialVersionUID = 6664043211578353597L;
    private static MemoryIndex index = new MemoryIndex();
    private String[] stopWords = {};

    public String[] getStopWords() {
        return stopWords;
    }

    public void setStopWords(String[] stopWords) {
        this.stopWords = stopWords;
    }

    /**
     * Constructor.
     * 
     * @param pattern the pattern to search for
     */
    public MessageBodyTerm(char op, String pattern) {
        // Note: comparison is case-insensitive
        super(op, pattern);
    }

    /**
     * The match method.
     * 
     * @param msg the pattern match is applied to this Message's subject header
     * @return true if the pattern match succeeds, otherwise false
     */
    @Override
    public boolean match(Message message) {
        Body body = message.getBody();
        if (body == null || body.getMessageBlob() == null) {
            return false;
        }
        String bodyString = body.getMessageBlob();
        String[] bodyParts = bodyString.split("\r\n");
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i< bodyParts.length; i++) {
            String[] pair = bodyParts[i].split(":");
            if ( pair[0].equalsIgnoreCase("to") ||
               pair[0].equalsIgnoreCase("from") ||
               pair[0].equalsIgnoreCase("subject") ||
               pair[0].equalsIgnoreCase("cc")) {
                ;
            }
            else {
                if(i == bodyParts.length - 1)
                    sb.append(bodyParts[i]);
                else
                    sb.append(bodyParts[i]).append("\r\n");
            }
        }
        bodyString = sb.toString();
        
        if(op == '=')
            return bodyString.equalsIgnoreCase(pattern);
        Analyzer analyzer = new MessageBodyAnalyzer(stopWords);
        QueryParser parser = new QueryParser("content", analyzer);
        parser.setAllowLeadingWildcard(true);
        parser.setDefaultOperator(QueryParser.AND_OPERATOR);
        float score = 0.0f;
        index.reset();
        index.addField("content", bodyString, analyzer);
        try {
            if(this.getPattern().startsWith("\"") &&
                    this.getPattern().endsWith("\"")) {
                score = index.search(parser.parse(this.getPattern()));
            } else {
                String[] terms = this.getPattern().split("\\s+");
                StringBuilder termBuilder = new StringBuilder();
                for(int i = 0; i< terms.length; i++){
                    if(!terms[i].equals("")) {
                        if (terms[i].equals("and") || 
                             terms[i].equals("or") || 
                             terms[i].equals("not")) {
                            termBuilder.append(terms[i].toUpperCase()).append(" ");
                        } else if (i != terms.length - 1)
                            termBuilder.append("*").append(terms[i]).append("*").append(" ");
                        else
                            termBuilder.append("*").append(terms[i]).append("*");
                    }
                }
                score = index.search(parser.parse(termBuilder.toString()));
            }
        } catch (ParseException e) {
            // ignore here
        }
        boolean isMatch = (score > 0.0f) ? true : false;
        return isMatch;
    }

    /**
     * Equality comparison.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MessageBodyTerm))
            return false;
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return new StringBuffer("(").append(MessageProperty.messagebody.name())
                .append(op).append(pattern).append(")").toString();
    }
}
