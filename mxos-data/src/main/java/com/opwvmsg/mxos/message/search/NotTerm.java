/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.message.pojos.Message;

/**
 * This class implements the logical NEGATION operator.
 *
 */
public final class NotTerm extends SearchTerm {

    private static final long serialVersionUID = 2545613363551064156L;
    /**
     * The search term to negate.
     *
     * @serial
     */
    protected SearchTerm term;

    public NotTerm(SearchTerm t) {
	term = t;
    }

    /**
     * Return the term to negate.
     */
    public SearchTerm getTerm() {
	return term;
    }

    /* The NOT operation */
    public boolean match(Message message) {
	return !term.match(message);
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof NotTerm))
	    return false;
	NotTerm nt = (NotTerm)obj;
	return nt.term.equals(this.term);
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	return term.hashCode() << 1;
    }

    @Override
    public String toString() {
        return new StringBuffer("(!").append(term.toString()).append(")")
                .toString();
    }
}
