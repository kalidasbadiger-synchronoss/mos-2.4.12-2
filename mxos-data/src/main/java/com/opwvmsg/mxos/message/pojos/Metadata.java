
package com.opwvmsg.mxos.message.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Metadata object of Message
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "uid",
    "folderId",
    "type",
    "priority",
    "flagRecent",
    "flagSeen",
    "flagUnread",
    "flagAns",
    "flagFlagged",
    "flagDel",
    "flagBounce",
    "flagPriv",
    "flagRichMail",
    "flagDraft",
    "popDeletedFlag",
    "keywords",
    "references",
    "hasAttachments",
    "deliverNDR",
    "sentDate",
    "arrivalTime",
    "lastAccessedTime",
    "expireOn",
    "size",
    "threadId",
    "from",
    "to",
    "cc",
    "bcc",
    "replyTo",
    "inReplyTo",
    "blobMessageId",
    "oldMsgId",
    "subject"
})
public class Metadata implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("uid")
    private Long uid;
    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    private Object folderId;
    @org.codehaus.jackson.annotate.JsonProperty("type")
    private java.lang.String type;
    @org.codehaus.jackson.annotate.JsonProperty("priority")
    private java.lang.String priority;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRecent")
    private Boolean flagRecent;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagSeen")
    private Boolean flagSeen;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagUnread")
    private Boolean flagUnread;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagAns")
    private Boolean flagAns;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagFlagged")
    private Boolean flagFlagged;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDel")
    private Boolean flagDel;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagBounce")
    private Boolean flagBounce;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagPriv")
    private Boolean flagPriv;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRichMail")
    private java.lang.Integer flagRichMail;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDraft")
    private Boolean flagDraft;
    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("popDeletedFlag")
    private com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse popDeletedFlag;
    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    private List<java.lang.String> keywords;
    @org.codehaus.jackson.annotate.JsonProperty("references")
    private java.lang.String references;
    @org.codehaus.jackson.annotate.JsonProperty("hasAttachments")
    private Boolean hasAttachments;
    @org.codehaus.jackson.annotate.JsonProperty("deliverNDR")
    private java.lang.Integer deliverNDR;
    @org.codehaus.jackson.annotate.JsonProperty("sentDate")
    private Long sentDate;
    @org.codehaus.jackson.annotate.JsonProperty("arrivalTime")
    private Long arrivalTime;
    @org.codehaus.jackson.annotate.JsonProperty("lastAccessedTime")
    private Long lastAccessedTime;
    @org.codehaus.jackson.annotate.JsonProperty("expireOn")
    private Long expireOn;
    @org.codehaus.jackson.annotate.JsonProperty("size")
    private Long size;
    @org.codehaus.jackson.annotate.JsonProperty("threadId")
    private java.lang.String threadId;
    @org.codehaus.jackson.annotate.JsonProperty("from")
    private java.lang.String from;
    @org.codehaus.jackson.annotate.JsonProperty("to")
    private List<java.lang.String> to;
    @org.codehaus.jackson.annotate.JsonProperty("cc")
    private List<java.lang.String> cc;
    @org.codehaus.jackson.annotate.JsonProperty("bcc")
    private List<java.lang.String> bcc;
    @org.codehaus.jackson.annotate.JsonProperty("replyTo")
    private List<java.lang.String> replyTo;
    @org.codehaus.jackson.annotate.JsonProperty("inReplyTo")
    private java.lang.String inReplyTo;
    @org.codehaus.jackson.annotate.JsonProperty("blobMessageId")
    private java.lang.String blobMessageId;
    @org.codehaus.jackson.annotate.JsonProperty("oldMsgId")
    private java.lang.String oldMsgId;
    @org.codehaus.jackson.annotate.JsonProperty("subject")
    private java.lang.String subject;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("uid")
    public Long getUid() {
        return uid;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("uid")
    public void setUid(Long uid) {
        this.uid = uid;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public Object getFolderId() {
        return folderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public void setFolderId(Object folderId) {
        this.folderId = folderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("type")
    public java.lang.String getType() {
        return type;
    }

    @org.codehaus.jackson.annotate.JsonProperty("type")
    public void setType(java.lang.String type) {
        this.type = type;
    }

    @org.codehaus.jackson.annotate.JsonProperty("priority")
    public java.lang.String getPriority() {
        return priority;
    }

    @org.codehaus.jackson.annotate.JsonProperty("priority")
    public void setPriority(java.lang.String priority) {
        this.priority = priority;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRecent")
    public Boolean getFlagRecent() {
        return flagRecent;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRecent")
    public void setFlagRecent(Boolean flagRecent) {
        this.flagRecent = flagRecent;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagSeen")
    public Boolean getFlagSeen() {
        return flagSeen;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagSeen")
    public void setFlagSeen(Boolean flagSeen) {
        this.flagSeen = flagSeen;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagUnread")
    public Boolean getFlagUnread() {
        return flagUnread;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagUnread")
    public void setFlagUnread(Boolean flagUnread) {
        this.flagUnread = flagUnread;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagAns")
    public Boolean getFlagAns() {
        return flagAns;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagAns")
    public void setFlagAns(Boolean flagAns) {
        this.flagAns = flagAns;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagFlagged")
    public Boolean getFlagFlagged() {
        return flagFlagged;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagFlagged")
    public void setFlagFlagged(Boolean flagFlagged) {
        this.flagFlagged = flagFlagged;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDel")
    public Boolean getFlagDel() {
        return flagDel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDel")
    public void setFlagDel(Boolean flagDel) {
        this.flagDel = flagDel;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagBounce")
    public Boolean getFlagBounce() {
        return flagBounce;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagBounce")
    public void setFlagBounce(Boolean flagBounce) {
        this.flagBounce = flagBounce;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagPriv")
    public Boolean getFlagPriv() {
        return flagPriv;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagPriv")
    public void setFlagPriv(Boolean flagPriv) {
        this.flagPriv = flagPriv;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRichMail")
    public java.lang.Integer getFlagRichMail() {
        return flagRichMail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagRichMail")
    public void setFlagRichMail(java.lang.Integer flagRichMail) {
        this.flagRichMail = flagRichMail;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDraft")
    public Boolean getFlagDraft() {
        return flagDraft;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("flagDraft")
    public void setFlagDraft(Boolean flagDraft) {
        this.flagDraft = flagDraft;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("popDeletedFlag")
    public com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse getPopDeletedFlag() {
        return popDeletedFlag;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("popDeletedFlag")
    public void setPopDeletedFlag(com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse popDeletedFlag) {
        this.popDeletedFlag = popDeletedFlag;
    }

    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    public List<java.lang.String> getKeywords() {
        return keywords;
    }

    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    public void setKeywords(List<java.lang.String> keywords) {
        this.keywords = keywords;
    }

    @org.codehaus.jackson.annotate.JsonProperty("references")
    public java.lang.String getReferences() {
        return references;
    }

    @org.codehaus.jackson.annotate.JsonProperty("references")
    public void setReferences(java.lang.String references) {
        this.references = references;
    }

    @org.codehaus.jackson.annotate.JsonProperty("hasAttachments")
    public Boolean getHasAttachments() {
        return hasAttachments;
    }

    @org.codehaus.jackson.annotate.JsonProperty("hasAttachments")
    public void setHasAttachments(Boolean hasAttachments) {
        this.hasAttachments = hasAttachments;
    }

    @org.codehaus.jackson.annotate.JsonProperty("deliverNDR")
    public java.lang.Integer getDeliverNDR() {
        return deliverNDR;
    }

    @org.codehaus.jackson.annotate.JsonProperty("deliverNDR")
    public void setDeliverNDR(java.lang.Integer deliverNDR) {
        this.deliverNDR = deliverNDR;
    }

    @org.codehaus.jackson.annotate.JsonProperty("sentDate")
    public Long getSentDate() {
        return sentDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("sentDate")
    public void setSentDate(Long sentDate) {
        this.sentDate = sentDate;
    }

    @org.codehaus.jackson.annotate.JsonProperty("arrivalTime")
    public Long getArrivalTime() {
        return arrivalTime;
    }

    @org.codehaus.jackson.annotate.JsonProperty("arrivalTime")
    public void setArrivalTime(Long arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastAccessedTime")
    public Long getLastAccessedTime() {
        return lastAccessedTime;
    }

    @org.codehaus.jackson.annotate.JsonProperty("lastAccessedTime")
    public void setLastAccessedTime(Long lastAccessedTime) {
        this.lastAccessedTime = lastAccessedTime;
    }

    @org.codehaus.jackson.annotate.JsonProperty("expireOn")
    public Long getExpireOn() {
        return expireOn;
    }

    @org.codehaus.jackson.annotate.JsonProperty("expireOn")
    public void setExpireOn(Long expireOn) {
        this.expireOn = expireOn;
    }

    @org.codehaus.jackson.annotate.JsonProperty("size")
    public Long getSize() {
        return size;
    }

    @org.codehaus.jackson.annotate.JsonProperty("size")
    public void setSize(Long size) {
        this.size = size;
    }

    @org.codehaus.jackson.annotate.JsonProperty("threadId")
    public java.lang.String getThreadId() {
        return threadId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("threadId")
    public void setThreadId(java.lang.String threadId) {
        this.threadId = threadId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("from")
    public java.lang.String getFrom() {
        return from;
    }

    @org.codehaus.jackson.annotate.JsonProperty("from")
    public void setFrom(java.lang.String from) {
        this.from = from;
    }

    @org.codehaus.jackson.annotate.JsonProperty("to")
    public List<java.lang.String> getTo() {
        return to;
    }

    @org.codehaus.jackson.annotate.JsonProperty("to")
    public void setTo(List<java.lang.String> to) {
        this.to = to;
    }

    @org.codehaus.jackson.annotate.JsonProperty("cc")
    public List<java.lang.String> getCc() {
        return cc;
    }

    @org.codehaus.jackson.annotate.JsonProperty("cc")
    public void setCc(List<java.lang.String> cc) {
        this.cc = cc;
    }

    @org.codehaus.jackson.annotate.JsonProperty("bcc")
    public List<java.lang.String> getBcc() {
        return bcc;
    }

    @org.codehaus.jackson.annotate.JsonProperty("bcc")
    public void setBcc(List<java.lang.String> bcc) {
        this.bcc = bcc;
    }

    @org.codehaus.jackson.annotate.JsonProperty("replyTo")
    public List<java.lang.String> getReplyTo() {
        return replyTo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("replyTo")
    public void setReplyTo(List<java.lang.String> replyTo) {
        this.replyTo = replyTo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("inReplyTo")
    public java.lang.String getInReplyTo() {
        return inReplyTo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("inReplyTo")
    public void setInReplyTo(java.lang.String inReplyTo) {
        this.inReplyTo = inReplyTo;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blobMessageId")
    public java.lang.String getBlobMessageId() {
        return blobMessageId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("blobMessageId")
    public void setBlobMessageId(java.lang.String blobMessageId) {
        this.blobMessageId = blobMessageId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("oldMsgId")
    public java.lang.String getOldMsgId() {
        return oldMsgId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("oldMsgId")
    public void setOldMsgId(java.lang.String oldMsgId) {
        this.oldMsgId = oldMsgId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("subject")
    public java.lang.String getSubject() {
        return subject;
    }

    @org.codehaus.jackson.annotate.JsonProperty("subject")
    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
