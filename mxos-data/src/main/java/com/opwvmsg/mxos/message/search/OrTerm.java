/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.message.pojos.Message;

/**
 * This class implements the logical OR operator on individual SearchTerms.
 *
 */
public final class OrTerm extends SearchTerm {

    private static final long serialVersionUID = 8775813588928549317L;
    /**
     * The array of terms on which the OR operator should
     * be applied.
     *
     * @serial
     */
    protected SearchTerm[] terms;

    /**
     * Constructor that takes two operands.
     *
     * @param t1 first term
     * @param t2 second term
     */
    public OrTerm(SearchTerm t1, SearchTerm t2) {
	terms = new SearchTerm[2];
	terms[0] = t1;
	terms[1] = t2;
    }

    /**
     * Constructor that takes an array of SearchTerms.
     *
     * @param t array of search terms
     */
    public OrTerm(SearchTerm[] t) {
	terms = new SearchTerm[t.length];
	for (int i = 0; i < t.length; i++)
	    terms[i] = t[i];
    }

    /**
     * Return the search terms.
     */
    public SearchTerm[] getTerms() {
	return (SearchTerm[])terms.clone();
    }

    /**
     * The OR operation. <p>
     *
     * The terms specified in the constructor are applied to
     * the given object and the OR operator is applied to their results.
     *
     * @param msg	The specified SearchTerms are applied to this Message
     *			and the OR operator is applied to their results.
     * @return		true if the OR succeds, otherwise false
     */

    public boolean match(Message message) {
	for (int i=0; i < terms.length; i++)
	    if (terms[i].match(message))
		return true;
	return false;
    }

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof OrTerm))
	    return false;
	OrTerm ot = (OrTerm)obj;
	if (ot.terms.length != terms.length)
	    return false;
	for (int i=0; i < terms.length; i++)
	    if (!terms[i].equals(ot.terms[i]))
		return false;
	return true;
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	int hash = 0;
	for (int i=0; i < terms.length; i++)
	    hash += terms[i].hashCode();
	return hash;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("(|");
        for (int i=0; i < terms.length; i++) {
            sb.append(terms[i].toString());
        }
        sb.append(")");
        return sb.toString();
    }
}
