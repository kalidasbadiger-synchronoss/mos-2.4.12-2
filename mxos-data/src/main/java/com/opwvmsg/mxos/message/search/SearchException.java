/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

/**
 * The exception thrown when a Search expression could not be handled.
 *
 */

public class SearchException extends Exception {

    private static final long serialVersionUID = 5339335378801878943L;

    /**
     * Constructs a SearchException with no detail message.
     */
    public SearchException() {
	super();
    }

    /**
     * Constructs a SearchException with the specified detail message.
     * @param s		the detail message
     */
    public SearchException(String s) {
	super(s);
    }

    /**
     * Constructs a SearchException with the specified detail message.
     * @param s         the detail message
     */
    public SearchException(String s, Exception e) {
        super(s, e);
    }
}
