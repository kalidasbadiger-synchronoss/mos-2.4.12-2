/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import java.util.ArrayList;
import java.util.List;

/**
 * This class implements the match method for Strings. The current
 * implementation provides only for substring matching. We
 * could add comparisons (like strcmp ...).
 *
 */
public abstract class StringTerm extends SearchTerm {

    private static final long serialVersionUID = 1274042129007696269L;
    
    /**
     * The pattern.
     *
     * @serial
     */
    protected final String pattern;
    final protected char op;
    protected List<String> parts;
    protected boolean analysisPattern;

    /**
     * Ignore case when comparing?
     *
     * @serial
     */
    protected boolean ignoreCase;

    protected StringTerm(char op, final String pattern) {
        this.op = op;
        ignoreCase = true;
        this.pattern = pattern.toLowerCase();
        analysisPattern = false;
    }
    
    protected StringTerm(boolean analysisPattern, char op, final String pattern) {
        this.op = op;
        ignoreCase = true;
        this.pattern = pattern.toLowerCase();
        this.analysisPattern = analysisPattern;
        if(analysisPattern) 
            analysisPattern(this.pattern);
    }

    protected StringTerm(char op, final String pattern, boolean ignoreCase) {
        this.op = op;
        this.ignoreCase = ignoreCase;
        this.pattern = ignoreCase ? pattern.toLowerCase() : pattern;
    }

    /**
     * Return the string to match with.
     */
    public String getPattern() {
        return pattern;
    }

    /**
     * Return true if we should ignore case when matching.
     */
    public boolean getIgnoreCase() {
        return ignoreCase;
    }

    protected boolean match(String s) {
        if (analysisPattern) {
            boolean isMatch = false;
            if (op == SearchOperator.Contains) {
                final String testString = ignoreCase ? s.toLowerCase() : s;
                for (String part : parts) {
                    isMatch |= testString.contains(part);
                }
            } else if (op == SearchOperator.Equals) {
                final String testString = ignoreCase ? s.toLowerCase() : s;
                for (String part : parts) {
                    isMatch |= testString.equals(part);
                }
            }
            return isMatch;
        } else {
            if (op == SearchOperator.Contains) {
                final String testString = ignoreCase ? s.toLowerCase() : s; 
                return testString.contains(pattern);
            } else if (op == SearchOperator.Equals) {
                return ignoreCase ? pattern.equalsIgnoreCase(s) : pattern.equals(s);
            }
            return false;
        }
    }

    /**
     * Equality comparison.
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof StringTerm))
            return false;
        StringTerm st = (StringTerm)obj;
        if (op != st.op) {
            return false;
        }
        if (ignoreCase)
            return st.pattern.equalsIgnoreCase(this.pattern) &&
                    st.ignoreCase == this.ignoreCase;
        else
            return st.pattern.equals(this.pattern) &&
                    st.ignoreCase == this.ignoreCase;
    }

    /**
     * Compute a hashCode for this object.
     */
    @Override
    public int hashCode() {
        return ignoreCase ? pattern.hashCode() : ~pattern.hashCode();
    }
    
    protected void analysisPattern(String s) {
        final char quoteMark = '"'; 
        final char beginOR = 'o';
        final char endOR = 'r';
        final char blankSpace = ' ';
        parts = new ArrayList<String>();
        boolean isInQuoteMarks = false;
        int start, end, i;
        start = end = i = 0;
        while (i < s.length()) {
           switch(s.charAt(i)){
           case quoteMark:
               end = i;
               if(isInQuoteMarks){
                   parts.add(s.substring(start,end).trim());
                   isInQuoteMarks = false;
               } else {
                   isInQuoteMarks = true;
               }
               start = end = i + 1;
               i++;
               break;
           case beginOR:
                end = i;
                if(!isInQuoteMarks){
                    if (i + 2 < s.length() &&
                            s.charAt(i + 1) == endOR && 
                            s.charAt(i + 2) == blankSpace &&
                            s.charAt(i - 1) == blankSpace) {
                        if (end - start > 1)
                            parts.add(s.substring(start, end).trim());
                        i+=3;
                        start = end = i;
                    } else {
                        if(i == s.length()-1)
                            parts.add(s.substring(start,end+1).trim());
                        i++;
                    }
                } else
                    i++;
                break;
           default:
               end = i++;
               if(end == s.length()-1)
                   parts.add(s.substring(start,end+1).trim());
               break;
           }
        }
    }
}
