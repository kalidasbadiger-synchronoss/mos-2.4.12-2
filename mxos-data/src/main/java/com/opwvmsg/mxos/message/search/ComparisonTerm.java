/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

/**
 * This class models the comparison operator. This is an abstract
 * class; subclasses implement comparisons for different datatypes.
 *
 */
public abstract class ComparisonTerm extends SearchTerm {

    private static final long serialVersionUID = 3654545235725177225L;

    public static final int LE = 1;
    public static final int LT = 2;
    public static final int EQ = 3;
    public static final int NE = 4;
    public static final int GT = 5;
    public static final int GE = 6;

    /**
     * The comparison.
     *
     * @serial
     */
    protected int comparison;

    /**
     * Equality comparison.
     */
    public boolean equals(Object obj) {
	if (!(obj instanceof ComparisonTerm))
	    return false;
	ComparisonTerm ct = (ComparisonTerm)obj;
	return ct.comparison == this.comparison;
    }

    /**
     * Compute a hashCode for this object.
     */
    public int hashCode() {
	return comparison;
    }
}
