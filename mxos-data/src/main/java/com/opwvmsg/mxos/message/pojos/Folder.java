
package com.opwvmsg.mxos.message.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.annotate.JsonValue;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Folder object of mOS
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "folderId",
    "folderName",
    "nextUID",
    "uidValidity",
    "numMessages",
    "numReadMessages",
    "numUnreadMessages",
    "folderSizeBytes",
    "folderSizeReadBytes",
    "folderSizeUnreadBytes",
    "parentFolderId",
    "msgLastAccessedTimeLB",
    "folderSubscribed",
    "keywords"
})
public class Folder implements Serializable
{

    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    private Object folderId;
    @org.codehaus.jackson.annotate.JsonProperty("folderName")
    private java.lang.String folderName;
    @org.codehaus.jackson.annotate.JsonProperty("nextUID")
    private Long nextUID;
    @org.codehaus.jackson.annotate.JsonProperty("uidValidity")
    private Long uidValidity;
    @org.codehaus.jackson.annotate.JsonProperty("numMessages")
    private java.lang.Integer numMessages;
    @org.codehaus.jackson.annotate.JsonProperty("numReadMessages")
    private java.lang.Integer numReadMessages;
    @org.codehaus.jackson.annotate.JsonProperty("numUnreadMessages")
    private java.lang.Integer numUnreadMessages;
    @org.codehaus.jackson.annotate.JsonProperty("folderSizeBytes")
    private Long folderSizeBytes;
    @org.codehaus.jackson.annotate.JsonProperty("folderSizeReadBytes")
    private Long folderSizeReadBytes;
    @org.codehaus.jackson.annotate.JsonProperty("folderSizeUnreadBytes")
    private Long folderSizeUnreadBytes;
    @org.codehaus.jackson.annotate.JsonProperty("parentFolderId")
    private Object parentFolderId;
    @org.codehaus.jackson.annotate.JsonProperty("msgLastAccessedTimeLB")
    private Long msgLastAccessedTimeLB;
    @org.codehaus.jackson.annotate.JsonProperty("folderSubscribed")
    private Folder.FolderSubscribed folderSubscribed;
    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    private java.lang.String keywords;
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public Object getFolderId() {
        return folderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderId")
    public void setFolderId(Object folderId) {
        this.folderId = folderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderName")
    public java.lang.String getFolderName() {
        return folderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderName")
    public void setFolderName(java.lang.String folderName) {
        this.folderName = folderName;
    }

    @org.codehaus.jackson.annotate.JsonProperty("nextUID")
    public Long getNextUID() {
        return nextUID;
    }

    @org.codehaus.jackson.annotate.JsonProperty("nextUID")
    public void setNextUID(Long nextUID) {
        this.nextUID = nextUID;
    }

    @org.codehaus.jackson.annotate.JsonProperty("uidValidity")
    public Long getUidValidity() {
        return uidValidity;
    }

    @org.codehaus.jackson.annotate.JsonProperty("uidValidity")
    public void setUidValidity(Long uidValidity) {
        this.uidValidity = uidValidity;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numMessages")
    public java.lang.Integer getNumMessages() {
        return numMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numMessages")
    public void setNumMessages(java.lang.Integer numMessages) {
        this.numMessages = numMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numReadMessages")
    public java.lang.Integer getNumReadMessages() {
        return numReadMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numReadMessages")
    public void setNumReadMessages(java.lang.Integer numReadMessages) {
        this.numReadMessages = numReadMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numUnreadMessages")
    public java.lang.Integer getNumUnreadMessages() {
        return numUnreadMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("numUnreadMessages")
    public void setNumUnreadMessages(java.lang.Integer numUnreadMessages) {
        this.numUnreadMessages = numUnreadMessages;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeBytes")
    public Long getFolderSizeBytes() {
        return folderSizeBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeBytes")
    public void setFolderSizeBytes(Long folderSizeBytes) {
        this.folderSizeBytes = folderSizeBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeReadBytes")
    public Long getFolderSizeReadBytes() {
        return folderSizeReadBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeReadBytes")
    public void setFolderSizeReadBytes(Long folderSizeReadBytes) {
        this.folderSizeReadBytes = folderSizeReadBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeUnreadBytes")
    public Long getFolderSizeUnreadBytes() {
        return folderSizeUnreadBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSizeUnreadBytes")
    public void setFolderSizeUnreadBytes(Long folderSizeUnreadBytes) {
        this.folderSizeUnreadBytes = folderSizeUnreadBytes;
    }

    @org.codehaus.jackson.annotate.JsonProperty("parentFolderId")
    public Object getParentFolderId() {
        return parentFolderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("parentFolderId")
    public void setParentFolderId(Object parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    @org.codehaus.jackson.annotate.JsonProperty("msgLastAccessedTimeLB")
    public Long getMsgLastAccessedTimeLB() {
        return msgLastAccessedTimeLB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("msgLastAccessedTimeLB")
    public void setMsgLastAccessedTimeLB(Long msgLastAccessedTimeLB) {
        this.msgLastAccessedTimeLB = msgLastAccessedTimeLB;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSubscribed")
    public Folder.FolderSubscribed getFolderSubscribed() {
        return folderSubscribed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("folderSubscribed")
    public void setFolderSubscribed(Folder.FolderSubscribed folderSubscribed) {
        this.folderSubscribed = folderSubscribed;
    }

    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    public java.lang.String getKeywords() {
        return keywords;
    }

    @org.codehaus.jackson.annotate.JsonProperty("keywords")
    public void setKeywords(java.lang.String keywords) {
        this.keywords = keywords;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Generated("com.googlecode.jsonschema2pojo")
    public static enum FolderSubscribed {

        FALSE("false"),
        TRUE("true");
        private final java.lang.String value;

        private FolderSubscribed(java.lang.String value) {
            this.value = value;
        }

        @JsonValue
        @Override
        public java.lang.String toString() {
            return this.value;
        }

        @JsonCreator
        public static Folder.FolderSubscribed fromValue(java.lang.String value) {
            for (Folder.FolderSubscribed c: Folder.FolderSubscribed.values()) {
                if (c.value.equalsIgnoreCase(value)) {
                    return c;
                }
            }
            throw new IllegalArgumentException(value);
        }

        @JsonCreator
        public static Folder.FolderSubscribed fromOrdinal(java.lang.Integer value) {
            for (Folder.FolderSubscribed c: Folder.FolderSubscribed.values()) {
                if (c.ordinal() == value) {
                    return c;
                }
            }
            throw new IllegalArgumentException();
        }

    }

}
