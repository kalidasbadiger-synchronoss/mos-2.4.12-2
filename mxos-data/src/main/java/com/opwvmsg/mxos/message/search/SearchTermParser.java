/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;
import java.util.ArrayList;
import java.util.List;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.SystemFolder;

/**
 * Utility class for all Term related operations.
 *
 * @author mxos-dev
 */
public class SearchTermParser {

    protected static final char START = '(';
    protected static final char END = ')';
    protected static final char AND = '&';
    protected static final char OR = '|';
    protected static final char NOT = '!';
    protected static final char EQUAL = '=';
    protected static final char APPROX = '~';
    protected static final String START_AND = "(&";
    protected static final String START_OR = "(|";
    protected static final String START_NOT = "(!";
    
    /**
     * Method to generate query string representation from SearchTerm.
     *
     * @param st SearchTerm
     * @return string, query string representation
     */
    public static String parse(final SearchTerm st) {
        String s = "";
        if (st != null) {
            s = st.toString();
        }
        return s;
    }
    
    private static class ParserState {
        public String query;
        public int index;
        public int length;
        public String folderName;
    };
    
    /**
     * 
     * Method to parse the given String to SearchTerm objects.
     *
     * @param query - searchTerm query
     * @param folderName - folder to which the requested message belongs
     * @return SearchTerm, SearchTerm Object
     */
    public static SearchTerm parse(final String query, final String folderName) {
        SearchTerm st = null;
        ParserState state = new ParserState();
        state.query = query;
        state.index = 0;
        state.length = query.length();
        state.folderName = folderName;
        
        st = parse_internal(state);
        return st;
    }
    
    public static SearchTerm parse(final String query) {
        return parse(query, SystemFolder.INBOX.name());
    }
    
    private static SearchTerm parse_internal(ParserState state) {
        if (state.index >= state.length) {
            return null;
        }
        
        // check the first character.  It's ok opening parenthesis is missing
        if (state.query.charAt(state.index) == SearchOperator.Begin) { 
            if (++state.index >= state.length) {
                return null;
            }
        }
        
        // check the first character
        char first = state.query.charAt(state.index);
        switch (first) {
        case SearchOperator.And:
        case SearchOperator.Or:
            ++state.index;
            List<SearchTerm> terms = new ArrayList<SearchTerm>();
            while (true) {
                SearchTerm st = parse_internal(state);
                if (st == null) {
                    break;
                }
                terms.add(st);
            }

            if (terms.isEmpty()) {
                return null;
            }

            SearchTerm[] termsArray = new SearchTerm[terms.size()];
            int i = 0;
            for (SearchTerm t : terms) {
                termsArray[i++] = t;
            }
            return (first == SearchOperator.And) ? 
                    new AndTerm(termsArray) : new OrTerm(termsArray);
        case SearchOperator.Not:
            ++state.index;
            SearchTerm st = parse_internal(state);
            if (st != null) {
                return new NotTerm(st);
            } else {
                return null;
            }
        case SearchOperator.End:
            ++state.index;
            return null;
        }
        
        // It's key value pair if we reach here.
        StringBuilder left = new StringBuilder();
        StringBuilder right = new StringBuilder();
        StringBuilder token = left;
        char op = '\0';
        for (; state.index < state.length; ++state.index) {
            char ch = state.query.charAt(state.index);
            if ((ch == SearchOperator.Contains || ch == SearchOperator.Equals || ch == SearchOperator.Regex)
                    && token == left) {
                op = ch;
                token = right;
            } else if ((ch == SearchOperator.GreaterThan || ch == SearchOperator.LessThan)
            		&& token == left) {
            	if ((state.index + 1 < state.length) && (state.query.charAt(state.index + 1) == SearchOperator.Equals)) {
            		// Skip a character if it's a ">=" or "<=" operator
        			state.index++;
        			op = (ch == SearchOperator.GreaterThan) ? SearchOperator.GreaterThanOrEqual : SearchOperator.LessThanOrEqual;
            	} else {
            		op = ch;
            	}
            	token = right;
            } else if (ch == SearchOperator.Escape) {
                ++state.index;
                if (state.index < state.length) {
                    token.append(state.query.charAt(state.index));
                }
            } else if (ch == SearchOperator.End && token == right) { 
                ++state.index;
                break;
            } else {
                token.append(ch);
            }
        }
        
        SearchTerm st = prepareTerm(op, left.toString(), right.toString(), state.folderName);
        return st;
    }
    
    /**
     * Method to prepare the SearchTerm object from key and value.
     *
     * @param op - operator
     * @param key - search key
     * @param value - search value
     * @param folderName - folder to which the requested message belongs
     * @return SearchTerm, SearchTerm object
     */
    private static SearchTerm prepareTerm(char op, String key, String value, String folderName) {
        SearchTerm st = null;
        // subject case
        if (key.equals(MessageProperty.subject.name())) {
            st = new SubjectTerm(op, value);
        } else if (key.equals(MessageProperty.to.name())) {
            st = new ToTerm(op, value);
        } else if (key.equals(MessageProperty.from.name())) {
            st = new FromTerm(op, value);
        } else if (key.equals(MessageProperty.cc.name())) {
            st = new CcTerm(op, value);
        } else if (key.equals(MessageProperty.arrivalTime.name())) {
            st = new ArrivalTimeTerm(op, value);
        } else if (key.equals(MessageProperty.sentDate.name()) && folderName != null && SystemFolder.SentMail.name().equals(folderName)) {
            st = new SentDateTerm(op, value);
        } else if (key.equals(MessageProperty.uid.name())) {
            st = new UIDTerm(op, value);
        } else if (key.equals(MessageProperty.flagUnread.name())){
            st = new FlagUnreadTerm(op,Boolean.valueOf(value));
        } else if (key.equals(MessageProperty.flagDel.name())){
            st = new FlagDelTerm(op,Boolean.valueOf(value));
        } else if (key.equals(MessageProperty.messagebody.name())) {
            st = new MessageBodyTerm(op, value);
        }
        return st;
    }
}
