package com.opwvmsg.mxos.message.search;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;

public class MessageBodyAnalyzer extends Analyzer {
    
    private CharArraySet stopWords = null;
        
    public MessageBodyAnalyzer() {
        this.stopWords = StopAnalyzer.ENGLISH_STOP_WORDS_SET;
    }
    
    public MessageBodyAnalyzer(String[] stops) {
        this.stopWords = StopFilter.makeStopSet(stops);
    }

    @Override
    protected TokenStreamComponents createComponents(String reader) {
        Tokenizer source = new StandardTokenizer();
        TokenStream filter = new LowerCaseFilter(source);
        filter = new StopFilter(filter, stopWords);
        return new TokenStreamComponents(source, filter);
    }

}
