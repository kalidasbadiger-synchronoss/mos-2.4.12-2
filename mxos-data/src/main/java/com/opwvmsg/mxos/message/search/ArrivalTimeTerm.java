/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.message.search;

import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * This class implements comparisons for the search key arrivalTime.
 * 
 */
public class ArrivalTimeTerm extends EpochTimeTerm {

	private static final long serialVersionUID = 1936075486632035930L;

	public ArrivalTimeTerm(char op, String epochTimeStr) {
		super(op, epochTimeStr);
	}

	@Override
	public boolean match(Message message) {
	    Metadata metadata = message.getMetadata();
		if (metadata == null || metadata.getArrivalTime() == null)
			return false;
		return super.match(metadata.getArrivalTime() * 1000);
	}

	@Override
	public String toString() {
		return new StringBuffer("(").append(MessageProperty.arrivalTime.name())
				.append(opStr).append(epochTimeStr).append(")").toString();
	}

}
