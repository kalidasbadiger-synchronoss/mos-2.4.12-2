
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Group object of mxos
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "groupBase",
    "members",
    "externalMembers",
    "subscriptionLinks"
})
public class Group implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupBase")
    private GroupBase groupBase;
    /**
     * members
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("members")
    private List<Member> members;
    /**
     * externalMembers
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalMembers")
    private List<ExternalMember> externalMembers;
    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    private Set<SubscriptionLink> subscriptionLinks = new HashSet<SubscriptionLink>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupBase")
    public GroupBase getGroupBase() {
        return groupBase;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("groupBase")
    public void setGroupBase(GroupBase groupBase) {
        this.groupBase = groupBase;
    }

    /**
     * members
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("members")
    public List<Member> getMembers() {
        return members;
    }

    /**
     * members
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("members")
    public void setMembers(List<Member> members) {
        this.members = members;
    }

    /**
     * externalMembers
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalMembers")
    public List<ExternalMember> getExternalMembers() {
        return externalMembers;
    }

    /**
     * externalMembers
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("externalMembers")
    public void setExternalMembers(List<ExternalMember> externalMembers) {
        this.externalMembers = externalMembers;
    }

    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    public Set<SubscriptionLink> getSubscriptionLinks() {
        return subscriptionLinks;
    }

    /**
     * subscriptionLinks
     * <p>
     * 
     * (Required)
     * 
     */
    @JsonProperty("subscriptionLinks")
    public void setSubscriptionLinks(Set<SubscriptionLink> subscriptionLinks) {
        this.subscriptionLinks = subscriptionLinks;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
