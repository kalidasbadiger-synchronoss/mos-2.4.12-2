
package com.opwvmsg.mxos.addressbook.pojos;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import javax.annotation.Generated;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.codehaus.jackson.annotate.JsonAnyGetter;
import org.codehaus.jackson.annotate.JsonAnySetter;
import org.codehaus.jackson.annotate.JsonPropertyOrder;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * Subscription object of AddressBook
 * 
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "otherUserId",
    "sharedContacts",
    "sharedGroups"
})
public class Subscription implements Serializable
{

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("otherUserId")
    private java.lang.String otherUserId;
    /**
     * sharedContacts
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedContacts")
    private Set<SharedContact> sharedContacts = new HashSet<SharedContact>();
    /**
     * sharedGroups
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedGroups")
    private Set<SharedGroup> sharedGroups = new HashSet<SharedGroup>();
    private Map<java.lang.String, Object> additionalProperties = new HashMap<java.lang.String, Object>();

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("otherUserId")
    public java.lang.String getOtherUserId() {
        return otherUserId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("otherUserId")
    public void setOtherUserId(java.lang.String otherUserId) {
        this.otherUserId = otherUserId;
    }

    /**
     * sharedContacts
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedContacts")
    public Set<SharedContact> getSharedContacts() {
        return sharedContacts;
    }

    /**
     * sharedContacts
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedContacts")
    public void setSharedContacts(Set<SharedContact> sharedContacts) {
        this.sharedContacts = sharedContacts;
    }

    /**
     * sharedGroups
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedGroups")
    public Set<SharedGroup> getSharedGroups() {
        return sharedGroups;
    }

    /**
     * sharedGroups
     * <p>
     * 
     * (Required)
     * 
     */
    @org.codehaus.jackson.annotate.JsonProperty("sharedGroups")
    public void setSharedGroups(Set<SharedGroup> sharedGroups) {
        this.sharedGroups = sharedGroups;
    }

    @Override
    public java.lang.String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<java.lang.String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(java.lang.String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
