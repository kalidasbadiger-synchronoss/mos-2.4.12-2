package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSocialNetworksService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworkSiteService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworksService;

public class SocialNetworksAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation,
            String inputstring, String testCaseName, String expectedOutput,
            String expResult) {
        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case SocialNetworksService: {
                switch(operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET SOCIAL NETWORK");
                        testStatus = readSocialNetworkService(context, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                        
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE SOCIAL NETWORK");
                        testStatus = updateSocialNetworkService(context, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case SocialNetworkSiteService: {
                switch(operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("CREATE SOCIAL NETWORKSITE");
                        testStatus = createSocialNetworkSiteService(context, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("GET SOCIAL NETWORKSITE");
                        testStatus = readSocialNetworkSiteService(context, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE SOCIAL NETWORKSITE");
                        testStatus = updateSocialNetworkSiteService(context, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("DELETE SOCIAL NETWORKSITE");
                        testStatus = deleteSocialNetworkSiteService(context, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosSocialNetworksService: {
                switch(operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COSSOCIAL NETWORK");
                        testStatus = readCosSocialNetworksService(context, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;                        
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS SOCIAL NETWORK");
                        testStatus = updateCosSocialNetworkService(context, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;

    }

    public static int readSocialNetworkService(final IMxOSContext context,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        SocialNetworks socialnetworks = null;
        String method = "read SocialNetworkService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworksService SocialNetworksService = (ISocialNetworksService) context
                    .getService(ServiceEnum.SocialNetworksService.name());
            socialnetworks = SocialNetworksService
                    .read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("socialnetworks = " + socialnetworks);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && socialnetworks != null) {
                result = CommonUtil.comparePayload(socialnetworks.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateSocialNetworkService(final IMxOSContext context,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update SocialNetworkService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworksService SocialNetworksService = (ISocialNetworksService) context
                    .getService(ServiceEnum.SocialNetworksService.name());
            SocialNetworksService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int createSocialNetworkSiteService(
            final IMxOSContext context, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Create SocialNetworkSiteService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworkSiteService SocialNetworkSiteService = (ISocialNetworkSiteService) context
                    .getService(ServiceEnum.SocialNetworkSiteService.name());

            SocialNetworkSiteService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readSocialNetworkSiteService(final IMxOSContext context,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<SocialNetworkSite> socialnetworks = null;
        String method = "read SocialNetworkSiteService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworkSiteService SocialNetworkSiteService = (ISocialNetworkSiteService) context
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            socialnetworks = SocialNetworkSiteService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("socialnetworks = " + socialnetworks);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && socialnetworks != null) {
                result = CommonUtil.comparePayload(socialnetworks.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateSocialNetworkSiteService(
            final IMxOSContext context, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update SocialNetworkSiteService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworkSiteService SocialNetworkSiteService = (ISocialNetworkSiteService) context
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            SocialNetworkSiteService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int deleteSocialNetworkSiteService(
            final IMxOSContext context, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete SocialNetworkSiteService";
        try {
            CommonUtil.printOnBoth(method);
            final ISocialNetworkSiteService SocialNetworkSiteService = (ISocialNetworkSiteService) context
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            SocialNetworkSiteService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosSocialNetworksService(final IMxOSContext context,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        SocialNetworks cossocialnetworks = null;
        String method = "read CosSocialNetworkService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosSocialNetworksService CosSocialNetworksService = (ICosSocialNetworksService) context
                    .getService(ServiceEnum.CosSocialNetworksService.name());
            cossocialnetworks = CosSocialNetworksService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("Cossocialnetworks = " + cossocialnetworks);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && cossocialnetworks != null) {
                result = CommonUtil.comparePayload(
                        cossocialnetworks.toString(), expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosSocialNetworkService(final IMxOSContext context,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosSocialNetworkService";
        try {
            CommonUtil.printOnBoth(method);
            final ICosSocialNetworksService CosSocialNetworksService = (ICosSocialNetworksService) context
                    .getService(ServiceEnum.CosSocialNetworksService.name());
            CosSocialNetworksService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
