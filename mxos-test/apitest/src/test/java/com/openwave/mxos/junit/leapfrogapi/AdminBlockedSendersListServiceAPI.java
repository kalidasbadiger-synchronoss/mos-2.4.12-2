package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminBlockedSendersListService;

public class AdminBlockedSendersListServiceAPI {

    static Logger alltestlog = Logger
            .getLogger(AdminBlockedSendersListServiceAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case PUT: {
            CommonUtil.printstartAPI("ADD ADMIN BLOCKED SENDERS LIST");
            testStatus = create(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case GET: {
            CommonUtil.printstartAPI("GET ADMIN BLOCKED SENDERS LIST");
            testStatus = read(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case POST: {
            CommonUtil.printstartAPI("UPDATE ADMIN BLOCKED SENDERS LIST");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETE: {
            CommonUtil.printstartAPI("DELETE ADMIN BLOCKED SENDERS LIST");
            testStatus = delete(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE BLOCKED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAdminBlockedSendersListService adminBlockedSendersListService = (IAdminBlockedSendersListService) context
                    .getService(service.name());
            adminBlockedSendersListService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
            } catch (MxOSException e) {
                errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "GET BLOCKED ADMIN SENDERS LIST";
        List<String> adminBlockedSendersList = null;
        try {
            CommonUtil.printOnBoth(method);
            IAdminBlockedSendersListService adminBlockedSendersListService = (IAdminBlockedSendersListService) context
            .getService(service.name());
            adminBlockedSendersList = adminBlockedSendersListService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("adminBlockedSendersList="+adminBlockedSendersList);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && adminBlockedSendersList != null) {
                result = CommonUtil.comparePayload(
                        adminBlockedSendersList.toString(), expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "UPDATE BLOCKED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAdminBlockedSendersListService adminBlockedSendersListService = (IAdminBlockedSendersListService) context
            .getService(service.name());
            adminBlockedSendersListService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "DELETE BLOCKED ADMIN SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method+"Starts");
            IAdminBlockedSendersListService adminBlockedSendersListService = (IAdminBlockedSendersListService) context
            .getService(service.name());
            adminBlockedSendersListService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
