package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.BmiFilters;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.McAfeeFilters;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
//import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSenderBlockingService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptBMIFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCommtouchFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptMcAfeeFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISenderBlockingService;

public class MailReceiptAPI {

    static Logger alltestlog = Logger.getLogger(MailReceiptAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {
        System.out.println("MailReceiptAPI =");
        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
        case MailReceiptService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAIL RECEIPET");
                    testStatus = read(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAIL RECEIPET");
                    testStatus = update(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAIL RECEIPET FILTER");
                    testStatus = readFilter(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptSieveFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAIL RECEIPET FILTER");
                    testStatus = readSieveFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAIL RECEIPET FILTER");
                    testStatus = updateSieveFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case SenderBlockingService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET SENDER BLOCKING");
                    testStatus = readSenderblocking(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE SENDER BLOCKING");
                    testStatus = updateSenderblocking(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptBMIFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAIL RECEIPT BMI FILTERS");
                    testStatus = readBmiFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAIL RECEIPT BMI FILTERS");
                    testStatus = updateBmiFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptCommtouchFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAILRECEIPT COMMTOUCH FILTERS");
                    testStatus = readCommTouchFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAILRECEIPT COMMTOUCH FILTERS");
                    testStatus = updateCommTouchFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptCloudmarkFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAILRECEIPT CLOUDMARK FILTERS");
                    testStatus = readCloudmarkFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAILRECEIPT CLOUDMARK FILTERS");
                    testStatus = updateCloudmarkFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case MailReceiptMcAfeeFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET MAILRECEIPT MCAFEE FILTERS");
                    testStatus = readMcAfeeFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE MAILRECEIPT MCAFEE FILTERS");
                    testStatus = updateMcAfeeFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case CosMailReceiptService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS MAIL RECEIPET");
                    testStatus = readCos(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS MAIL RECEIPET");
                    testStatus = updateCos(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        case CosMailReceiptSieveFiltersService:{
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS MAIL RECEIPET FILTER");
                    testStatus = readCosSieveFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS MAIL RECEIPET FILTER");
                    testStatus = updateCosSieveFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
/*        case CosMailReceiptBMIFiltersService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS MAIL RECEIPT BMI FILTERS");
                    testStatus = readCosBmiFilters(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS MAIL RECEIPT BMI FILTERS");
                    testStatus = updateCosBmiFilters(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
*/        case CosSenderBlockingService: {
            switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET COS SENDER BLOCKING");
                    testStatus = readCosSenderblocking(context, service, map, expResult, expectedOutput,
                            checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE COS SENDER BLOCKING");
                    testStatus = updateCosSenderblocking(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
            }
        }
        }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read MailReceipt";
        MailReceipt mailreceipt = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptService MailReceiptService = (IMailReceiptService) context.getService(service.name());
            mailreceipt = MailReceiptService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailreceipt = " + mailreceipt);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailreceipt != null) {
                result = CommonUtil.comparePayload(mailreceipt.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update MailReceipt";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptService mailReceiptService = (IMailReceiptService) context.getService(service.name());
            mailReceiptService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    // ///////////
    public static int readCos(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read COS MailReceipt";
        MailReceipt mailreceipt = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptService CosMailReceiptService = (ICosMailReceiptService) context.getService(service.name());
            mailreceipt = CosMailReceiptService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailreceipt = " + mailreceipt);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailreceipt != null) {
                result = CommonUtil.comparePayload(mailreceipt.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCos(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update COS MailReceipt";
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptService CosMailReceiptService = (ICosMailReceiptService) context.getService(service.name());
            CosMailReceiptService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readFilter(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Mailfilter";
        Filters mailfilter = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptFiltersService MailReceiptFiltersService = (IMailReceiptFiltersService) context.getService(service.name());
            mailfilter = MailReceiptFiltersService
                    .read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailfilter = " + mailfilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailfilter != null) {
                result = CommonUtil.comparePayload(mailfilter.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int readSieveFilters(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read mailseivefilter";
        SieveFilters mailseivefilter = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptSieveFiltersService MailReceiptSieveFiltersService = (IMailReceiptSieveFiltersService) context.getService(service.name());
            mailseivefilter = MailReceiptSieveFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("mailseivefilter = " + mailseivefilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailseivefilter != null) {
                result = CommonUtil.comparePayload(mailseivefilter.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateSieveFilters(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update MailReceiptSieveFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptSieveFiltersService MailReceiptSieveFiltersService = (IMailReceiptSieveFiltersService) context.getService(service.name());
            MailReceiptSieveFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosSieveFilters(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Cos mailseivefilter";
        SieveFilters mailseivefilter = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptSieveFiltersService cosMailReceiptSieveFiltersService = (ICosMailReceiptSieveFiltersService) context.getService(service.name());
            mailseivefilter = cosMailReceiptSieveFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("mailseivefilter = " + mailseivefilter);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailseivefilter != null) {
                result = CommonUtil.comparePayload(mailseivefilter.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCosSieveFilters(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update CosMailReceiptSieveFiltersService";
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptSieveFiltersService cosMailReceiptSieveFiltersService = (ICosMailReceiptSieveFiltersService) context.getService(service.name());
            cosMailReceiptSieveFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readSenderblocking(
            final IMxOSContext context, ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Sender Blocking";
        SenderBlocking senderblocking = null;
        try {
            CommonUtil.printOnBoth(method);

            ISenderBlockingService senderBlockingService = (ISenderBlockingService) context.getService(service.name());
            senderblocking = senderBlockingService
                    .read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("SenderBlockingService = "
                    + senderBlockingService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && senderblocking != null) {
                result = CommonUtil.comparePayload(senderblocking.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateSenderblocking(final IMxOSContext context, ServiceEnum service, 
            MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update Sender Blocking";
        try {
            CommonUtil.printOnBoth(method);
            ISenderBlockingService senderBlockingService = (ISenderBlockingService) context.getService(service.name());
            senderBlockingService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosSenderblocking(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read COS Sender Blocking";
        SenderBlocking senderblocking = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosSenderBlockingService cosSenderBlockingService = (ICosSenderBlockingService) context.getService(service.name());
            senderblocking = cosSenderBlockingService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("CosSenderBlockingService = "
                    + cosSenderBlockingService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && senderblocking != null) {
                result = CommonUtil.comparePayload(senderblocking.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCosSenderblocking(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update Cos Sender Blocking";
        try {
            CommonUtil.printOnBoth(method);
            ICosSenderBlockingService cosSenderBlockingService = (ICosSenderBlockingService) context.getService(service.name());
            cosSenderBlockingService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readBmiFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Sender Blocking";
        BmiFilters bmiFilters = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptBMIFiltersService mailReceiptBMIFiltersService = (IMailReceiptBMIFiltersService) context.getService(service.name());
            bmiFilters = mailReceiptBMIFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("SenderBlockingService = "
                    + mailReceiptBMIFiltersService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && bmiFilters != null) {
                result = CommonUtil.comparePayload(bmiFilters.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateBmiFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update Bmi Filterg";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptBMIFiltersService mailReceiptBMIFiltersService = (IMailReceiptBMIFiltersService) context.getService(service.name());
            mailReceiptBMIFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

/*    public static int readCosBmiFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Cos Sender Blocking";
        BmiFilters bmiFilters = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptBMIFiltersService cosMailReceiptBMIFiltersService = (ICosMailReceiptBMIFiltersService) context.getService(service.name());
            bmiFilters = cosMailReceiptBMIFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("SenderBlockingService = "
                    + cosMailReceiptBMIFiltersService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && bmiFilters != null) {
                result = CommonUtil.comparePayload(bmiFilters.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
*/

/*    public static int updateCosBmiFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update Cos Bmi Filterg";
        try {
            CommonUtil.printOnBoth(method);
            ICosMailReceiptBMIFiltersService cosMailReceiptBMIFiltersService = (ICosMailReceiptBMIFiltersService) context.getService(service.name());
            cosMailReceiptBMIFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
*/

    public static int readCommTouchFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "MailReceipt Commtouch Filters";
        CommtouchFilters commtouchfilters = null;
        try {
            CommonUtil.printOnBoth(method);

            IMailReceiptCommtouchFiltersService MailReceiptCommtouchFiltersService = (IMailReceiptCommtouchFiltersService) context.getService(service.name());
            commtouchfilters = MailReceiptCommtouchFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("MailReceiptCommtouchFiltersService = "
                    + MailReceiptCommtouchFiltersService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && commtouchfilters != null) {
                result = CommonUtil.comparePayload(commtouchfilters.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCommTouchFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "MailReceipt Commtouch Filters";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptCommtouchFiltersService MailReceiptCommtouchFiltersService = (IMailReceiptCommtouchFiltersService) context.getService(service.name());
            MailReceiptCommtouchFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCloudmarkFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "mail Receipt CloudmarkFilters";
        CloudmarkFilters cloudmarkFilters = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptCloudmarkFiltersService mailReceiptCloudmarkFiltersService = (IMailReceiptCloudmarkFiltersService) context.getService(service.name());
            cloudmarkFilters = mailReceiptCloudmarkFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("MailReceiptCommtouchFiltersService = "
                    + mailReceiptCloudmarkFiltersService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && cloudmarkFilters != null) {
                result = CommonUtil.comparePayload(cloudmarkFilters.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCloudmarkFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "mailReceipt Cloudmark Filters";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptCloudmarkFiltersService mailReceiptCloudmarkFiltersService = (IMailReceiptCloudmarkFiltersService) context.getService(service.name());
            mailReceiptCloudmarkFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readMcAfeeFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "MailReceipt McAfee Filters Service";
        McAfeeFilters mcafeefilters = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptMcAfeeFiltersService mailReceiptMcAfeeFiltersService = (IMailReceiptMcAfeeFiltersService) context.getService(service.name());
            mcafeefilters = mailReceiptMcAfeeFiltersService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("MailReceiptMcAfeeFiltersService = "
                    + mailReceiptMcAfeeFiltersService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mcafeefilters != null) {
                result = CommonUtil.comparePayload(mcafeefilters.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateMcAfeeFilters(
            final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "MailReceipt McAfee Filters Service";
        try {
            CommonUtil.printOnBoth(method);
            IMailReceiptMcAfeeFiltersService mailReceiptMcAfeeFiltersService = (IMailReceiptMcAfeeFiltersService) context.getService(service.name());
            mailReceiptMcAfeeFiltersService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int verifymailreceiptOutput(MailReceipt mailreceipt,
            String expectedOutput) {
        System.out.println("Verifying GeneralPreferences Output ....");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs;
        int errorcount = 0;
        int paramexist = 0;
        alltestlog.info("paramKeyValPairs  ...  = " + expectKeyValPairs.length);

        // need to handle the case when expected output has non standard pattern
        // Would work it on at the time of tool enhancement phase

        if (expectKeyValPairs.length == 0) {
            return 1;
        }
        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";
            expectinterKeyValPairs = expectKeyValPairs[count].split(delimiter);
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.AUTOREPLYMODE)) {
                paramexist = 1;
                if (mailreceipt.getAutoReplyMode().toString()
                        .equals(expectinterKeyValPairs[1].toString())) {
                    alltestlog.info("Actual " + expectinterKeyValPairs[0]
                            + " = " + mailreceipt.getAutoReplyMode()
                            + " Expected " + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                } else {
                    alltestlog.error("ERROR: Actual "
                            + expectinterKeyValPairs[0] + " = "
                            + mailreceipt.getAutoReplyMode() + " Expected "
                            + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                    errorcount++;
                }
            }
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.AUTOREPLYMSG)) {
                paramexist = 1;
                if (mailreceipt.getAutoReplyMessage().toString()
                        .equals(expectinterKeyValPairs[1].toString())) {
                    alltestlog.info("Actual " + expectinterKeyValPairs[0]
                            + " = " + mailreceipt.getAutoReplyMessage()
                            + " Expected " + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                } else {
                    alltestlog.error("ERROR: Actual "
                            + expectinterKeyValPairs[0] + " = "
                            + mailreceipt.getAutoReplyMessage() + " Expected "
                            + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                    errorcount++;
                }
            }
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.MAXRECMSGSIZEKB)) {
                paramexist = 1;
                if (mailreceipt.getMaxReceiveMessageSizeKB().toString()
                        .equals(expectinterKeyValPairs[1].toString())) {
                    alltestlog.info("Actual " + expectinterKeyValPairs[0]
                            + " = " + mailreceipt.getMaxReceiveMessageSizeKB()
                            + " Expected " + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                } else {
                    alltestlog.error("ERROR: Actual "
                            + expectinterKeyValPairs[0] + " = "
                            + mailreceipt.getMaxReceiveMessageSizeKB()
                            + " Expected " + expectinterKeyValPairs[0] + " = "
                            + expectinterKeyValPairs[1]);
                    errorcount++;
                }
            }
            /*
             * if (expectinterKeyValPairs[0]
             * .equalsIgnoreCase(LfConstants.FWDENABLE)) { paramexist = 1; if
             * (mailreceipt.getFilteringEnabled().toString()
             * .equals(expectinterKeyValPairs[1].toString())) {
             * alltestlog.info("Actual " + expectinterKeyValPairs[0] + " = " +
             * mailreceipt.getFilteringEnabled() + " Expected " +
             * expectinterKeyValPairs[0] + " = " + expectinterKeyValPairs[1]); }
             * else { alltestlog.error("ERROR: Actual " +
             * expectinterKeyValPairs[0] + " = " +
             * mailreceipt.getFilteringEnabled() + " Expected " +
             * expectinterKeyValPairs[0] + " = " + expectinterKeyValPairs[1]);
             * errorcount++; } }
             */
            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[1]
                        + " is not present in actual output");
                errorcount++;
            }

        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }

}
