package com.openwave.mxos.junit.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class Messageparms {


    Map<String,String> mailbox;

    public Messageparms(){
        mailbox = new HashMap<String,String>();
    }
    public void setMessageProperty(String propertyName, String propertyValue){
        mailbox.put(propertyName, propertyValue);
    }

    public String getMessageProperty(String propertyName){
        return mailbox.get(propertyName);
    }

    public String toString(){
        StringBuffer messageAttributes = new StringBuffer();
        Set<String> keys = mailbox.keySet();

        Iterator<String> keyIterator = keys.iterator();
        while(keyIterator.hasNext()){
            String key = keyIterator.next();
            messageAttributes.append(key);
            messageAttributes.append("=");
            messageAttributes.append(getMessageProperty(key));
            if(keyIterator.hasNext())
                messageAttributes.append("&");
        }
        return messageAttributes.toString();
    }

}



