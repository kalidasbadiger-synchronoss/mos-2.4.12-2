package com.openwave.mxos.junit.helper;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
			
public class ErrorCodes {


	static Logger alltestlog = Logger.getLogger(ErrorCodes.class);

	public static HashMap geterrorcode (String PathForXml) {
	
    	HashMap errorCodeHash = new HashMap <NamedNodeMap ,NamedNodeMap>(); 
    	
		File testcaseFolder = new File(PathForXml);
		File[] listOfFiles = testcaseFolder.listFiles();
		alltestlog.info("Total Number of files:: " + listOfFiles.length);
		for (File file : listOfFiles) {
			Pattern pattern = Pattern.compile("\\~\\$");
			Matcher matcher = pattern.matcher(file.toString());
			if (matcher.find()) {
				alltestlog.info("SKIP: Reading file :: " + file);
				continue;
			}
			Pattern pattern1 = Pattern.compile("error-codes.xml$");
			Matcher matcher1 = pattern1.matcher(file.toString());

			if (!matcher1.find()) {
				alltestlog.info("SKIP: Reading file :: " + file);
				continue;
			}
			alltestlog.info("Reading file :: " + file);
			try {

	            DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
	            Document doc = docBuilder.parse (new File(file.toString()));

	            // normalize text representation
	            doc.getDocumentElement ().normalize ();
	            alltestlog.info("Root element of the doc is " + 
	                 doc.getDocumentElement().getNodeName());

	            NodeList listOfPersons = doc.getElementsByTagName("error");
	            int totalPersons = listOfPersons.getLength();
	            alltestlog.info("Total no of people : " + totalPersons);

	            for(int s=0; s<listOfPersons.getLength() ; s++){
                  Node firstPersonNode = listOfPersons.item(s);
	              NamedNodeMap attributes=firstPersonNode.getAttributes();
	              alltestlog.info(attributes.getNamedItem("code").getNodeValue() + " == " + 
	            		  attributes.getNamedItem("message").getNodeValue()  );
	  	          errorCodeHash.put(attributes.getNamedItem("code").getNodeValue(),attributes.getNamedItem("message").getNodeValue());
	            }
	        }catch (SAXParseException err) {
	        	alltestlog.error ("** Parsing error" + ", line " 
	             + err.getLineNumber () + ", uri " + err.getSystemId ());
	        	alltestlog.error(" " + err.getMessage ());

	        }catch (SAXException e) {
	        Exception x = e.getException ();
	        ((x == null) ? e : x).printStackTrace ();

	        }catch (Throwable t) {
	        t.printStackTrace ();
	        }	
		}	
		Set set = errorCodeHash.entrySet(); 
		Iterator i = set.iterator(); 
		// Display elements 
		/*while(i.hasNext()) { 
		Map.Entry me = (Map.Entry)i.next(); 
		System.out.print("***************"); 
		System.out.print(me.getKey() + ": "); 
		System.out.println(me.getValue()); 
		} 
		*/
		return errorCodeHash;		
	}
	
	
}
