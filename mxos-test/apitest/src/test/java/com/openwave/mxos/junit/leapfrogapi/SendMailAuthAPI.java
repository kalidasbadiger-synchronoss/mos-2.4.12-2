package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
//import com.opwvmsg.mxos.data.pojos.DataMap;
//import com.opwvmsg.mxos.data.pojos.;
//import com.opwvmsg.mxos.data.pojos.MessageMetaData;
//import com.opwvmsg.mxos.data.pojos.SearchNode;
//import com.opwvmsg.mxos.data.pojos.SearchQuery;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
//import com.opwvmsg.mxos.interfaces.service.SearchOperation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendMailService;

public class SendMailAuthAPI {

    static Logger alltestlog = Logger.getLogger(MailboxAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case POST: {
            CommonUtil.printstartAPI("Create message");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
      }
        return 1;
    }

   
    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        int result;
        String errorCode = LfConstants.HTTPSUCCESS;
        try {
            ISendMailService sendMailService = (ISendMailService) context
                    .getService(service.name());
            CommonUtil.printOnBoth("Send Mail");
            sendMailService.process(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Send Mail...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("Send Mail", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

  
}


