package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.task.pojos.Task;

public class TasksAPI {
	
    static Logger alltestlog = Logger.getLogger(TasksAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case TasksService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("Create TasksService");
                        testStatus = create(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("Read TasksService");
                        testStatus = read(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case LIST: {
                        CommonUtil.printstartAPI("List TasksService");
                        testStatus = list(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }                    
                    case DELETE: {
                        CommonUtil.printstartAPI("Delete TasksServcie");
                        testStatus = delete(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETEALL: {
                        CommonUtil.printstartAPI("Delete All TasksServcie");
                        testStatus = deleteAll(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }                    
                }
            }
          }
        return 0;
    }
    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE TasksServcie";
        try {
            CommonUtil.printOnBoth(method);
            ITasksService taskService = (ITasksService) context
                    .getService(service.name());
            long taskID = taskService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("task id = " + taskID);
            LeapFrogTest.taskIdArray.add(taskID);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Task task = null;
        int result;
        String method = "Read TaskServcie";
        try {
            CommonUtil.printOnBoth(method);
            ITasksService taskService = (ITasksService) context
                    .getService(service.name());
            task = taskService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Task is = " + task);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && task != null) {
                result = CommonUtil.comparePayload(task.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "DELETE TaskService";
        try {
            CommonUtil.printOnBoth(method);
            ITasksService taskService = (ITasksService) context
                    .getService(service.name());
            taskService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    public static int deleteAll(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
    	 String errorCode = LfConstants.HTTPSUCCESS;
         int result;
         String method = "DELETE All TaskService";
         try {
             CommonUtil.printOnBoth(method);
             ITasksService taskService = (ITasksService) context
                     .getService(service.name());
             taskService.deleteAll(map.getMultivaluedMap());
             CommonUtil.printOnBoth(method + " ..done");
         } catch (MxOSException e) {
             errorCode = CommonUtil.getPrintErrorCode(method, e);
         }
         result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
         return result;
    	
    }
    public static int list(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<Task> taskList = null;

        String method = "LIST TasksService";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            CommonUtil.printOnBoth(method);
            final ITasksService taskService = (ITasksService) context
                    .getService(service.name());
            taskList = taskService.list(multiMap);
            CommonUtil.printOnBoth("...task list == " + taskList);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && taskList != null) {
                result = CommonUtil.comparePayload(taskList.toString(),
                        expectedOutput);
            }
        }
        return result;    	
    }

}
