package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailAccessService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;

public class MailAccessAPI {

    static Logger alltestlog = Logger.getLogger(MailReceiptAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
        case MailAccessService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET MAIL ACCESS");
                testStatus = read(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE MAIL ACCESS");
                testStatus = update(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        case CosMailAccessService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET COS MAIL ACCESS");
                testStatus = readCos(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE COS MAIL ACCESS");
                testStatus = updateCos(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }

            case MailAccessAllowedIpService: {
                switch (operation) {
                case GET: {
                    CommonUtil.printstartAPI("GET ALLOWEDIPS");
                    testStatus = readAllowedIp(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case POST: {
                    CommonUtil.printstartAPI("UPDATE ALLOWEDIPS");
                    testStatus = updateAllowedIp(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case PUT: {
                    CommonUtil.printstartAPI("UPDATE ALLOWEDIPS");
                    testStatus = createAllowedIp(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                case DELETE: {
                    CommonUtil.printstartAPI("UPDATE ALLOWEDIPS");
                    testStatus = deleteAllowedIp(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
                    return testStatus;
                }
                }
            }
            }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read MailAccess";
        MailAccess mailaccess = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailAccessService mailaccessService = (IMailAccessService) context
                    .getService(service.name());
            mailaccess = mailaccessService.read(map.getMultivaluedMap());
            alltestlog.info("mailaccess = " + mailaccess);
            CommonUtil.printOnBoth("mailaccess = " + mailaccess);
            CommonUtil.printOnBoth(method + " ...done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailaccess != null) {
                result = CommonUtil.comparePayload(mailaccess.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update MailAccess";
        try {
            CommonUtil.printOnBoth(method);
            IMailAccessService mailaccessService = (IMailAccessService) context
                    .getService(service.name());
            mailaccessService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCos(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read CosMailAccess";
        MailAccess cosmailaccess = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosMailAccessService cosMailaccessService = (ICosMailAccessService) context
                    .getService(service.name());
            cosmailaccess = cosMailaccessService.read(map.getMultivaluedMap());
            alltestlog.info("cosmailaccess = " + cosmailaccess);
            CommonUtil.printOnBoth("cosmailaccess = " + cosmailaccess);
            CommonUtil.printOnBoth(method + " ...done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && cosmailaccess != null) {
                result = CommonUtil.comparePayload(cosmailaccess.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCos(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update CosMailAccess";
        try {
            CommonUtil.printOnBoth(method);
            ICosMailAccessService cosMailaccessService = (ICosMailAccessService) context
                    .getService(service.name());
            cosMailaccessService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int createAllowedIp(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Add allowed IP";
        try {
        	IMailAccessAllowedIpService mailAccessAllowedIpService = (IMailAccessAllowedIpService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            mailAccessAllowedIpService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int updateAllowedIp(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update Allowed Ip";
        try {
        	IMailAccessAllowedIpService mailAccessAllowedIpService = (IMailAccessAllowedIpService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            mailAccessAllowedIpService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int readAllowedIp(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Allowed Ip";
        List<String> allowedIp = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailAccessAllowedIpService mailAccessAllowedIpService = (IMailAccessAllowedIpService) context
                    .getService(service.name());
            allowedIp = mailAccessAllowedIpService.read(map.getMultivaluedMap());
            alltestlog.info("mailaccess = " + allowedIp);
            CommonUtil.printOnBoth("mailaccess = " + allowedIp);
            CommonUtil.printOnBoth(method + " ...done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && allowedIp != null) {
                result = CommonUtil.comparePayload(allowedIp.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
    
    public static int deleteAllowedIp(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete Allowed Ip";
        try {
        	IMailAccessAllowedIpService mailAccessAllowedIpService = (IMailAccessAllowedIpService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            mailAccessAllowedIpService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
