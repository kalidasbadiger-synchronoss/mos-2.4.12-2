package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.openwave.mxos.junit.mainscript.Leapfrogtest.LeapFrogTest;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;

public class AddressbookGroupAPI {

	   static Logger alltestlog = Logger.getLogger(AddressbookGroupAPI.class);
	    static String service = "Addressbook GROUP API";

	    public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);
	        int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }

	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);
	        
       switch (service) {
       case GroupsService:{
	        switch (operation) {
            case PUT: {
                CommonUtil.printstartAPI("CREATE ADDRESSBOOK GROUP");
                testStatus = create(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
	        case DELETE: {
	             CommonUtil.printstartAPI("DELETE ADDRESSBOOK GROUP");
	             testStatus = delete(context, service, map, expResult,
	                            expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	        case DELETEALL: {
	             CommonUtil.printstartAPI("DELETEALL ADDRESSBOOK GROUP");
	             testStatus = deleteall(context, service, map, expResult,
	                                expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
	                }
       		}
       }
	        return 1;
	   }
	    
	    public static int create (final IMxOSContext context,
	            ServiceEnum service, MxosRequestMap map, String expErrorCode,
	            String expectedOutput, int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        String method = "create " + service;
            long groupId;
        try {
	            CommonUtil.printOnBoth(method);
           final IGroupsService groupsService = (IGroupsService) 
           context.getService(ServiceEnum.GroupsService.name());
	            groupId =  groupsService.create(map.getMultivaluedMap());
	   		LeapFrogTest.groupidarray.add(groupId);
            CommonUtil.printOnBoth("groupId = " + groupId );
            CommonUtil.printOnBoth(method + "...Done");
	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode(method, e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

	    public static int delete(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IGroupsService groupsService = (IGroupsService) 
	            context.getService(ServiceEnum.GroupsService.name());
	            CommonUtil.printOnBoth("delete " + service + "service");
	            groupsService.delete(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("delete " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("delete " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

	    public static int deleteall(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IGroupsService groupsService = (IGroupsService) 
	            context.getService(ServiceEnum.GroupsService.name());
	            CommonUtil.printOnBoth("deleteAll " + service + "service");
	            groupsService.deleteAll(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("deleteAll " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("deleteAll " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

}
