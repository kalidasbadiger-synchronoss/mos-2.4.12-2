package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedSendersListService;

public class AllowedSendersListServiceAPI {

    static Logger alltestlog = Logger
            .getLogger(AllowedSendersListServiceAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case PUT: {
            CommonUtil.printstartAPI("ADD ALLOWED SENDERS LIST");
            testStatus = create(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case GET: {
            CommonUtil.printstartAPI("GET ALLOWED SENDERS LIST");
            testStatus = read(context, service, map, expResult, expectedOutput,
                    checkExpOutput);
            return testStatus;
        }
        case POST: {
            CommonUtil.printstartAPI("UPDATE ALLOWED SENDERS LIST");
            testStatus = update(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        case DELETE: {
            CommonUtil.printstartAPI("DELETE ALLOWED SENDERS LIST");
            testStatus = delete(context, service, map, expResult,
                    expectedOutput, checkExpOutput);
            return testStatus;
        }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE ALLOWED SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAllowedSendersListService allowedSendersListService = (IAllowedSendersListService) context
                    .getService(service.name());
            allowedSendersListService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "GET ALLOWED SENDERS LIST";
        List<String> allowedsenderslist = null;
        try {
            CommonUtil.printOnBoth(method);
            IAllowedSendersListService allowedSendersListService = (IAllowedSendersListService) context
            .getService(service.name());
            allowedsenderslist = allowedSendersListService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && allowedsenderslist != null) {
                result = CommonUtil.comparePayload(
                        allowedsenderslist.toString(), expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "UPDATE ALLOWED SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAllowedSendersListService allowedSendersListService = (IAllowedSendersListService) context
            .getService(service.name());
            allowedSendersListService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "DELETE ALLOWED SENDERS LIST";
        try {
            CommonUtil.printOnBoth(method);
            IAllowedSendersListService allowedSendersListService = (IAllowedSendersListService) context
            .getService(service.name());
            allowedSendersListService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int verifymailforwardservice(List<String> mailforward,
            String expectedOutput) {

        System.out.println("Verifying mailforward Output ....");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs;
        int errorcount = 0;
        int paramexist = 0;
        alltestlog.info("paramKeyValPairs  ...  = " + expectKeyValPairs.length);

        // need to handle the case when expected output has non standard pattern
        // Would work it on at the time of tool enhancement phase

        if (expectKeyValPairs.length == 0) {
            return 1;
        }
        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";
            expectinterKeyValPairs = expectKeyValPairs[count].split(delimiter);
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.FWDADDRESS)) {
                paramexist = 1;
                String[] expectedlist = expectinterKeyValPairs[1].split("&");
                for (int count1 = 0; count1 < expectedlist.length; count1++) {
                    boolean yesorno = mailforward.contains(expectedlist[count]);
                    if (yesorno) {
                        alltestlog.info("Expected " + expectinterKeyValPairs[0]
                                + " = " + expectedlist[count] + " is found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + mailforward.toString());
                    } else {
                        alltestlog.info("ERROR :Expected "
                                + expectinterKeyValPairs[0] + " = "
                                + expectedlist[count] + " is NOT found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + mailforward.toString());
                        errorcount++;
                    }
                }
            }

            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[1]
                        + " is not present in actual output");
                errorcount++;
            }

        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }
}
