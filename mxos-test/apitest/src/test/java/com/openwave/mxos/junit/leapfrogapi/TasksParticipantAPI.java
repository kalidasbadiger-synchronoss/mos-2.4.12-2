package com.openwave.mxos.junit.leapfrogapi;


import org.apache.log4j.Logger;
import java.util.List;
import java.util.Map;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksParticipantService;
import com.opwvmsg.mxos.task.pojos.Participant;

public class TasksParticipantAPI {
	
    static Logger alltestlog = Logger.getLogger(TasksParticipantAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case TasksParticipantService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("Create TasksParticipantService");
                        testStatus = createTasksParticipantService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("Read TasksParticipantService");
                        testStatus = readTasksParticipantService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("Update TasksParticipantService");
                        testStatus = updateTasksParticipantService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("Update TasksParticipantService");
                        testStatus = deleteTasksParticipantService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case LIST: {
                        CommonUtil.printstartAPI("List TasksParticipantService");
                        testStatus = listTasksParticipantService(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }

                    
                }
            }
          }
        return 1;
    }
    
    public static int readTasksParticipantService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        Participant data = null;
        int result;
        String method = "Read TasksParticipantService";
        try {
            CommonUtil.printOnBoth(method);
            ITasksParticipantService servcieObject = (ITasksParticipantService) context
                    .getService(service.name());
            data = servcieObject.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("TasksParticipantService Data is = " + data);
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && data != null) {
                result = CommonUtil.comparePayload(data.toString(),
                        expectedOutput);
            }
        }
        return result;
    }
    
    public static int createTasksParticipantService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "CREATE TasksParticipantService";
        try {
            CommonUtil.printOnBoth(method);
            ITasksParticipantService servcieObject = (ITasksParticipantService) context
                    .getService(service.name());
            servcieObject.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
    public static int deleteTasksParticipantService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
    	 String errorCode = LfConstants.HTTPSUCCESS;
         int result;
         String method = "DELETE ServiceName";
         try {
             CommonUtil.printOnBoth(method);
             ITasksParticipantService servcieObject = (ITasksParticipantService) context
                     .getService(service.name());
             servcieObject.delete(map.getMultivaluedMap());
             CommonUtil.printOnBoth(method + " ..done");
         } catch (MxOSException e) {
             errorCode = CommonUtil.getPrintErrorCode(method, e);
         }
         result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
         return result;
    	
    }
   
    public static int updateTasksParticipantService(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update TasksParticipantService";

        try {
            CommonUtil.printOnBoth(method);
            final ITasksParticipantService servcieObject = (ITasksParticipantService) context
                    .getService(service.name());
            servcieObject.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
    
   
    public static int listTasksParticipantService(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<Participant> dataList = null;

        String method = "LIST TasksParticipantService";
        Map<String, List<String>> multiMap = map.getMultivaluedMap();
        try {
            CommonUtil.printOnBoth(method);
            final ITasksParticipantService servcieObject = (ITasksParticipantService) context
                    .getService(service.name());
            dataList = servcieObject.list(multiMap);
            CommonUtil.printOnBoth("...TasksParticipantService data list == " + dataList);
            CommonUtil.printOnBoth(method + "...Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && dataList != null) {
                result = CommonUtil.comparePayload(dataList.toString(),
                        expectedOutput);
            }
        }
        return result;    	
    }
    

}
