package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosExternalStoreService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailStoreService;

public class MailStoreAPI {

    static Logger alltestlog = Logger.getLogger(MailStoreAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {
        System.out.println("MailStoreServiceAPI =");
        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
        case MailStoreService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET MAILSTORE");
                testStatus = read(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE MAILSTORE");
                testStatus = update(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        case CosMailStoreService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET COS MAILSTORE");
                testStatus = readCos(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE COS MAILSTORE");
                testStatus = updateCos(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        case ExternalStoreService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET EXTERNAL STORE");
                testStatus = readExternalStore(context, service, map,
                        expResult, expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE EXTERNAL STORE");
                testStatus = updateExternalStore(context, service, map,
                        expResult, expectedOutput, checkExpOutput);
                return testStatus;
            }
            }
        }
        case CosExternalStoreService: {
            switch (operation) {
            case GET: {
                CommonUtil.printstartAPI("GET COS EXTERNAL STORE");
                testStatus = readCosExternalStore(context, service, map,
                        expResult, expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE COS EXTERNAL STORE");
                testStatus = updateCosExternalStore(context, service, map,
                        expResult, expectedOutput, checkExpOutput);
                return testStatus;
            }
            }

        }
        }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read MailStore";
        MailStore mailstore = null;
        try {
            CommonUtil.printOnBoth(method);
            IMailStoreService MailStoreService = (IMailStoreService) context
                    .getService(service.name());
            mailstore = MailStoreService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailstore = " + mailstore);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailstore != null) {
                result = CommonUtil.comparePayload(mailstore.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update MailStore";
        try {
            CommonUtil.printOnBoth(method);
            IMailStoreService MailStoreService = (IMailStoreService) context
                    .getService(service.name());
            MailStoreService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    // ///////////

    public static int readCos(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read cos mail store";
        MailStore mailstore = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosMailStoreService cosMailStoreService = (ICosMailStoreService) context
                    .getService(service.name());
            mailstore = cosMailStoreService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("mailstore = " + mailstore);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && mailstore != null) {
                result = CommonUtil.comparePayload(mailstore.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCos(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update cos mail store";
        try {
            CommonUtil.printOnBoth(method);
            ICosMailStoreService cosMailStoreService = (ICosMailStoreService) context
                    .getService(service.name());
            cosMailStoreService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readExternalStore(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read External Store Service";
        ExternalStore externalstore = null;
        try {
            CommonUtil.printOnBoth(method);
            IExternalStoreService externalStoreService = (IExternalStoreService) context
                    .getService(service.name());
            externalstore = externalStoreService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("externalStoreService = "
                    + externalStoreService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            // alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && externalstore != null) {
                result = CommonUtil.comparePayload(externalstore.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateExternalStore(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update External Store";
        try {
            CommonUtil.printOnBoth(method);
            IExternalStoreService externalStoreService = (IExternalStoreService) context
                    .getService(service.name());
            externalStoreService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            // alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosExternalStore(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read Cos External Store Service";
        ExternalStore externalstore = null;
        try {
            CommonUtil.printOnBoth(method);
            ICosExternalStoreService cosExternalStoreService = (ICosExternalStoreService) context
                    .getService(service.name());
            externalstore = cosExternalStoreService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("cosExternalStoreService = "
                    + cosExternalStoreService);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            // alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && externalstore != null) {
                result = CommonUtil.comparePayload(externalstore.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int updateCosExternalStore(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "update Cos External Store";
        try {
            CommonUtil.printOnBoth(method);
            ICosExternalStoreService cosExternalStoreService = (ICosExternalStoreService) context
                    .getService(service.name());
            cosExternalStoreService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
            // alltestlog.info("mycatch  ...  = " + e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
