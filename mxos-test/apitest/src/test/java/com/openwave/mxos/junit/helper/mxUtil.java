package com.openwave.mxos.junit.helper;

import net.neoremind.sshxcute.core.ConnBean;
import net.neoremind.sshxcute.core.Result;
import net.neoremind.sshxcute.core.SSHExec;
import net.neoremind.sshxcute.exception.TaskExecFailException;
import net.neoremind.sshxcute.task.CustomTask;
import net.neoremind.sshxcute.task.impl.ExecCommand;

import org.apache.log4j.Logger;

public class mxUtil {

	static Logger alltestlog = Logger.getLogger(mxUtil.class);

	public static int execute(String host, String user, String pass,
			String Action, String cmd, String testCaseName,
			String expectedOutput, String expResult) {

		int testStatus = 0;

		int checkExpOutput = 1;
		if (null == expectedOutput || expectedOutput == "") {
			checkExpOutput = 0;
		}
		CommonUtil.printOnBoth("expectedOutput =" + expectedOutput);
		CommonUtil.printOnBoth("checkExpOutput =" + checkExpOutput);
		CommonUtil.printOnBoth("expHttpOut =" + expResult);

		if (Action.toString().equalsIgnoreCase(LfConstants.STARTSERVER)) {
			CommonUtil.printOnBoth("**** startServer *****");
			testStatus = startServer(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.STOPSERVER)) {
			CommonUtil.printOnBoth("**** stopServer *****");
			testStatus = stopServer(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.RESTARTSERVER)) {
			CommonUtil.printOnBoth("**** restartServer *****");
			testStatus = restartServer(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.KILLSERVER)) {
			CommonUtil.printOnBoth("**** killServer *****");
			testStatus = killServer(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.SETCONFIGKEY)) {
			CommonUtil.printOnBoth("**** setConfigKey *****");
			testStatus = setConfigkey(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.GETCONFIGKEY)) {
			CommonUtil.printOnBoth("**** getConfigKey *****");
			testStatus = getConfigkey(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		if (Action.toString().equalsIgnoreCase(LfConstants.DELETECONFIGKEY)) {
			CommonUtil.printOnBoth("**** deleteConfigKey *****");
			testStatus = deleteConfigkey(host, user, pass, cmd, expectedOutput);
			return testStatus;
		}
		return 1;
	}

	public static int startServer(String host, String user, String pass,
			String cmd, String expectedOutput) {
		SSHExec ssh = null;
		int allserver = 0;
		int errcnt = 0;
		int result = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
			allserver = 1;
		} else {
			cmd = cmd.replaceAll(",", " ");
		}

		System.out.println("cmd == " + cmd);
		try {
			ConnBean cb = new ConnBean(host, user, pass);
			ssh = SSHExec.getInstance(cb);
			CustomTask cmdexec = new ExecCommand("source .profile ",
					"lib/imservctrl start " + cmd + "", "sleep 20",
					"imservping");
			ssh.connect();
			Result res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("..Return code: " + res.rc);
				alltestlog.info("..sysout: " + res.sysout);
				result = checkserverUp(host, user, cmd, ssh, allserver);
				if (result == 0) {
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			} else {
				alltestlog.error("Error:Return code: " + res.rc);
				alltestlog.error("Error:error message: " + res.error_msg);
				errcnt++;
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int stopServer(String host, String user, String pass,
			String cmd, String expectedOutput) {
		SSHExec ssh = null;
		int allserver = 0;
		int errcnt = 0;
		int result = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
			allserver = 1;
		} else {
			cmd = cmd.replaceAll(",", " ");
		}

		System.out.println("cmd == " + cmd);
		try {
			ConnBean cb = new ConnBean(host, user, pass);
			ssh = SSHExec.getInstance(cb);
			CustomTask cmdexec = new ExecCommand("source .profile ",
					"lib/imservctrl stop " + cmd);
			ssh.connect();
			Result res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("..Return code: " + res.rc);
				alltestlog.info("..sysout: " + res.sysout);
				result = checkserverstop(host, user, cmd, ssh, allserver);
				if (result == 0) {
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			} else {
				alltestlog.error("Error:Return code: " + res.rc);
				alltestlog.error("Error:error message: " + res.error_msg);
				errcnt++;
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int restartServer(String host, String user, String pass,
			String cmd, String expectedOutput) {
		SSHExec ssh = null;
		int allserver = 0;
		int errcnt = 0;
		int result = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
			allserver = 1;
		} else {
			cmd = cmd.replaceAll(",", " ");
		}

		System.out.println("cmd == " + cmd);
		try {
			ConnBean cb = new ConnBean(host, user, pass);
			ssh = SSHExec.getInstance(cb);
			CustomTask cmdexec = new ExecCommand("source .profile ",
					"lib/imservctrl restart " + cmd + "", "sleep 20",
					"imservping");
			ssh.connect();
			Result res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("..Return code: " + res.rc);
				alltestlog.info("..sysout: " + res.sysout);
				result = checkserverUp(host, user, cmd, ssh, allserver);
				if (result == 0) {
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			} else {
				alltestlog.error("Error:Return code: " + res.rc);
				alltestlog.error("Error:error message: " + res.error_msg);
				errcnt++;
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int killServer(String host, String user, String pass,
			String cmd, String expectedOutput) {
		SSHExec ssh = null;
		int allserver = 0;
		int errcnt = 0;
		int result = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
			allserver = 1;
		} else {
			cmd = cmd.replaceAll(",", " ");
		}

		System.out.println("cmd == " + cmd);
		try {
			ConnBean cb = new ConnBean(host, user, pass);
			ssh = SSHExec.getInstance(cb);
			CustomTask cmdexec = new ExecCommand("source .profile ",
					"lib/imservctrl kill " + cmd);
			ssh.connect();
			Result res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("..Return code: " + res.rc);
				alltestlog.info("..sysout: " + res.sysout);
				result = checkserverstop(host, user, cmd, ssh, allserver);
				if (result == 0) {
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			} else {
				alltestlog.error("Error:Return code: " + res.rc);
				alltestlog.error("Error:error message: " + res.error_msg);
				errcnt++;
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int checkserverUp(String host, String user, String cmd,
			SSHExec ssh, int allserver) throws TaskExecFailException   {

		Result res;
		int errcnt = 0;
		if (allserver == 1) {
			String cmd2 = "ps -fu " + user
					+ " | grep -i unreachable | grep -v grep | wc -l";
			alltestlog.info("executing command : " + cmd2);
			CustomTask cmdexec = new ExecCommand("source .profile" + cmd2);
			res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("Return code: " + res.rc);
				String temp = res.sysout.replaceAll("\n", " ");
				temp = temp.replaceAll(".*" + user, "");
				temp = temp.replaceAll("\\s*", "");
				alltestlog.info("sysout: " + res.sysout.toString());
				if (temp.equalsIgnoreCase("0")) {
					System.out.println("sysout pass: " + temp);
				} else {
					alltestlog.error("sysout fail: " + temp);
					errcnt++;
				}
			} else {
				alltestlog.error("Return code: " + res.rc);
				alltestlog.error("error message: " + res.error_msg);
				errcnt++;
			}
		} else {
			String delimiter = " ";
			String[] servList;
			servList = cmd.split(delimiter);
			for (int i = 0; i < servList.length; i++) {

				String cmd2 = "ps -fu " + user + " | grep -i " + servList[i]
						+ " | grep -i unreachable | grep -v grep | wc -l";
				alltestlog.info("executing command : " + cmd2);
				CustomTask cmdexec = new ExecCommand("source .profile", cmd2);
				res = ssh.exec(cmdexec);
				if (res.isSuccess) {
					alltestlog.info("Return code: " + res.rc);
					String temp = res.sysout.replaceAll("\n", " ");
					temp = temp.replaceAll(".*" + user, "");
					temp = temp.replaceAll("\\s*", "");
					System.out.println("sysout: " + res.sysout.toString());
					if (temp.equalsIgnoreCase("0")) {
						alltestlog.info("sysout pass: " + temp);
					} else {
						alltestlog.error("sysout fail: " + temp);
						errcnt++;
					}
				} else {
					alltestlog.error("Return code: " + res.rc);
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			}
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int checkserverstop(String host, String user, String cmd,
			SSHExec ssh, int allserver) throws TaskExecFailException {

		Result res;
		int errcnt = 0;
		if (allserver == 1) {
			String cmd2 = "ps -fu " + user
					+ " | grep -i responded | grep -v grep | wc -l";
			alltestlog.info("executing command : " + cmd2);
			CustomTask cmdexec = new ExecCommand("source .profile" + cmd2);
			res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				alltestlog.info("Return code: " + res.rc);
				String temp = res.sysout.replaceAll("\n", " ");
				temp = temp.replaceAll(".*" + user, "");
				temp = temp.replaceAll("\\s*", "");
				alltestlog.info("sysout: " + res.sysout.toString());
				if (temp.equalsIgnoreCase("0")) {
					System.out.println("sysout pass: " + temp);
				} else {
					alltestlog.error("sysout fail: " + temp);
					errcnt++;
				}
			} else {
				alltestlog.error("Return code: " + res.rc);
				alltestlog.error("error message: " + res.error_msg);
				errcnt++;
			}
		} else {
			String delimiter = " ";
			String[] servList;
			servList = cmd.split(delimiter);
			for (int i = 0; i < servList.length; i++) {

				String cmd2 = "ps -fu " + user + " | grep -i " + servList[i]
						+ " | grep -i responded | grep -v grep | wc -l";
				alltestlog.info("executing command : " + cmd2);
				CustomTask cmdexec = new ExecCommand("source .profile", cmd2);
				res = ssh.exec(cmdexec);
				if (res.isSuccess) {
					alltestlog.info("Return code: " + res.rc);
					String temp = res.sysout.replaceAll("\n", " ");
					temp = temp.replaceAll(".*" + user, "");
					temp = temp.replaceAll("\\s*", "");
					System.out.println("sysout: " + res.sysout.toString());
					if (temp.equalsIgnoreCase("0")) {
						alltestlog.info("sysout pass: " + temp);
					} else {
						alltestlog.error("sysout fail: " + temp);
						errcnt++;
					}
				} else {
					alltestlog.error("Return code: " + res.rc);
					alltestlog.error("error message: " + res.error_msg);
					errcnt++;
				}
			}
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static int setConfigkey(String host, String user, String pass,
			String cmd, String expectedOutput) {
		SSHExec ssh = null;
		int errcnt = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
		}
		ConnBean cb = new ConnBean(host, user, pass);
		ssh = SSHExec.getInstance(cb);
		ssh.connect();
		System.out.println("..cmd: " + cmd);
		String delimiter = ",\"";
		String configkey;
		String[] confkeyList;
		confkeyList = cmd.split(delimiter);
		try {
			for (int i = 0; i < confkeyList.length; i++) {
				configkey = extractConfigkey(ssh, confkeyList[i]);
				alltestlog.info("..Return configkey: " + configkey);

				if (configkey.equalsIgnoreCase("Error")) {
					errcnt++;
				} else {
					CustomTask cmdexec = new ExecCommand("source .profile ",
							" imconfcontrol -install -key " + configkey);
					Result res = ssh.exec(cmdexec);
					if (res.isSuccess) {
						System.out.println("..Return code: " + res.rc);
						System.out.println("..sysout: " + res.sysout);
					} else {
						alltestlog.error("Error:Return code: " + res.rc);
						alltestlog.error("Error:error message: "
								+ res.error_msg);
						errcnt++;
					}
				}
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	
	public static int getConfigkey(String host, String user, String pass,
			String cmd,String expectedOutput) {
		SSHExec ssh = null;
		int errcnt = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
		}
		ConnBean cb = new ConnBean(host, user, pass);
		ssh = SSHExec.getInstance(cb);
		ssh.connect();
		System.out.println("..cmd: " + cmd);
		String delimiter = ",\"";
		String configkey;
		String configkeyVal = null;
		String[] confkeyList;
		confkeyList = cmd.split(delimiter);
		try {
			for (int i = 0; i < confkeyList.length; i++) {
				configkey = extractConfigkey(ssh, confkeyList[i]);
				System.out.println("..Return configkey: " + configkey);

				if (configkey.equalsIgnoreCase("Error")) {
					errcnt++;
				} else {
					CustomTask cmdexec = new ExecCommand("source .profile ",
							" imconfget -fullpath " + configkey);
					Result res = ssh.exec(cmdexec);
					if (res.isSuccess) {
						System.out.println("..Return code: " + res.rc);
						System.out.println("..sysout: " + res.sysout);
						configkeyVal = res.sysout.replaceAll("\n", "");
					} else {
						System.err.println("Error:Return code: " + res.rc);
						System.err.println("Error:error message: "
								+ res.error_msg);
						errcnt++;
					}
				}
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			
			alltestlog.error("Found " + errcnt + " Errors");
			return 0;
		} else if (expectedOutput == null || expectedOutput == ""){	
			alltestlog.info("getconfigkey success");
			return 1;
		} else if(!expectedOutput.equals(configkeyVal)){	
			alltestlog.error("Error : actual result " + configkeyVal);
			alltestlog.error("Error : expected result " + expectedOutput);
			return 0;
		} else {
		   alltestlog.info("actual result " + configkeyVal);
	       alltestlog.info("expected result " + expectedOutput);
		}
	return 1;
	}

	public static int deleteConfigkey(String host, String user, String pass,
			String cmd,String expectedOutput) {
		SSHExec ssh = null;
		int errcnt = 0;
		if (cmd == null || cmd == "") {
			cmd = "";
		}
		ConnBean cb = new ConnBean(host, user, pass);
		ssh = SSHExec.getInstance(cb);
		ssh.connect();
		System.out.println("..cmd: " + cmd);
		String delimiter = ",\"";
		String configkey;
		String[] confkeyList;
		confkeyList = cmd.split(delimiter);
		try {
			for (int i = 0; i < confkeyList.length; i++) {
				configkey = extractConfigkey(ssh, confkeyList[i]);
				System.out.println("..Return configkey: " + configkey);

				if (configkey.equalsIgnoreCase("Error")) {
					errcnt++;
				} else {
					CustomTask cmdexec = new ExecCommand("source .profile ",
							" imconfcontrol -install -key " + configkey);
					Result res = ssh.exec(cmdexec);
					if (res.isSuccess) {
						System.out.println("..Return code: " + res.rc);
						System.out.println("..sysout: " + res.sysout);
					} else {
						System.err.println("Error:Return code: " + res.rc);
						System.err.println("Error:error message: "
								+ res.error_msg);
						errcnt++;
					}
				}
			}
		} catch (TaskExecFailException e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} catch (Exception e) {
			alltestlog.error("Error Mesage" + e.getMessage());
			alltestlog.error("Error StackTrace" + e.getStackTrace());
			errcnt++;
		} finally {
			ssh.disconnect();
		}
		if (errcnt != 0) {
			System.err.println("Found " + errcnt + " Errors");
			return 0;
		}
		return 1;
	}

	public static String extractConfigkey(SSHExec ssh, String configkey)
			throws TaskExecFailException {

		String[] templist;
		configkey = configkey.replaceAll("^\"", "");
		configkey = configkey.replaceAll("\"$", "");

		System.out.println("configkey = " + configkey);

		String temp = configkey.replaceAll("/.*", "");

		String delimiter = "/";
		templist = configkey.split(delimiter);
		// System.out.println("..templist: " + templist[1]);
		// System.out.println("..templistlenght: " + templist.length);
		if (templist.length <= 3) {
			System.out.println("Invalid configkey : " + configkey);
			return "Error";
		}
		temp = templist[1].replaceAll("host", "");

		// System.out.println("..templist: " + templist[1]);
		if (temp.equalsIgnoreCase("*")) {
			System.out.println(".. server == : " + temp);
			// System.out.println(".. configkey == : " +configkey);
			return configkey;
		} else {
			System.out.println(".. server == : " + temp);
			// System.out.println(".. configkey == : " + configkey);

			CustomTask cmdexec = new ExecCommand("source .profile ",
					" imconfget -server " + temp);
			Result res = ssh.exec(cmdexec);
			if (res.isSuccess) {
				System.out.println("..Return code: " + res.rc);
				System.out.println("..sysout: " + res.sysout);
				res.sysout = res.sysout.replaceAll("\n", "");
				res.sysout = res.sysout.replaceAll("\\s*", "");
				configkey = configkey.replaceAll(temp + "host", res.sysout);
				// System.out.println(".. configkey == : " + configkey);
				return configkey;
			} else {
				System.err.println("Error:Return code: " + res.rc);
				System.err.println("Error:error message: " + res.error_msg);
			}

		}
		return "Error";
	}
}
