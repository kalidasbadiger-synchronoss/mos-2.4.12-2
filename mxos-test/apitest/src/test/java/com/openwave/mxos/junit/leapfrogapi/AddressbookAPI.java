package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IAddressBookService;

public class AddressbookAPI {

	   static Logger alltestlog = Logger.getLogger(AddressbookAPI.class);
	    static String service = "Addressbook API";

	    public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);
	        int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }

	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);
	        
	        switch (operation) {
	        case DELETE: {
	             CommonUtil.printstartAPI("DELETE ADDRESSBOOK");
	             testStatus = delete(context, service, map, expResult,
	                            expectedOutput, checkExpOutput);
	             return testStatus;
	                    }
                }
	        return 1;
	   }
	    

	    public static int delete(final IMxOSContext context, ServiceEnum service,
	            MxosRequestMap map, String expErrorCode, String expectedOutput,
	            int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        int result;
	        try {
	            final IAddressBookService addressBookService = (IAddressBookService) 
	            context.getService(ServiceEnum.AddressBookService.name());
	            CommonUtil.printOnBoth("delete " + service + "service");
	            addressBookService.delete(map.getMultivaluedMap());
	            CommonUtil.printOnBoth("delete " + service + "service ..done");

	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode("delete " + service
	                    + "service", e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        return result;
	    }

}
