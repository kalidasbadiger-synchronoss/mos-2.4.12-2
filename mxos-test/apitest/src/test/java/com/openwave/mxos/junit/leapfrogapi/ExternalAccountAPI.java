package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosExternalAccountsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalMailAccountService;

public class ExternalAccountAPI {
    static Logger alltestlog = Logger.getLogger("alltestlogs");
    static String service = "ExternalAccount";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {
        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (service) {
            case ExternalAccountService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET EXTERNAL ACCOUNT");
                        testStatus = readExtAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE EXTERNAL ACCOUNT");
                        testStatus = updateExtAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case CosExternalAccountService: {
                switch (operation) {
                    case GET: {
                        CommonUtil.printstartAPI("GET COS EXTERNAL ACCOUNT");
                        testStatus = readCosExtAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE COS EXTERNAL ACCOUNT");
                        testStatus = updateCosExtAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case ExternalMailAccountService: {
                switch (operation) {
                    case PUT: {
                        CommonUtil.printstartAPI("PUT EXTERNAL MAIL ACCOUNT");
                        testStatus = createExtMailAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case GET: {
                        CommonUtil.printstartAPI("GET EXTERNAL MAIL ACCOUNT");
                        testStatus = readExtMailAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case POST: {
                        CommonUtil.printstartAPI("UPDATE EXTERNAL MAIL ACCOUNT");
                        testStatus = updateExtMailAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                    case DELETE: {
                        CommonUtil.printstartAPI("DELETE EXTERNAL MAIL ACCOUNT");
                        testStatus = deleteExtMailAct(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;
    }

    public static int readExtAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        ExternalAccounts extAccount = null;
        String method = "read ExternalAccountsService";
        try {
            final IExternalAccountsService ExternalAccountsService =
                (IExternalAccountsService) context
                    .getService(service.name());

            CommonUtil.printOnBoth(method);
            extAccount = ExternalAccountsService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("extAccount = " + extAccount);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && extAccount != null) {
                result = CommonUtil.comparePayload(extAccount.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateExtAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update ExternalAccountsService";
        try {
            final IExternalAccountsService ExternalAccountsService =
                (IExternalAccountsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            ExternalAccountsService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readCosExtAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        ExternalAccounts extAccount = null;
        String method = "read CosExternalAccountsService";
        try {
            final ICosExternalAccountsService CosExternalAccountsService =
                (ICosExternalAccountsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            extAccount = CosExternalAccountsService.read(map
                    .getMultivaluedMap());
            CommonUtil.printOnBoth("extAccount = " + extAccount);
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && extAccount != null) {
                result = CommonUtil.comparePayload(extAccount.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateCosExtAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update CosExternalAccountsService";
        try {
            final ICosExternalAccountsService CosExternalAccountsService =
                (ICosExternalAccountsService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            CosExternalAccountsService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int readExtMailAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "read ExternalMailAccountService";
        List<MailAccount> accountList = null;
        try {
            final IExternalMailAccountService serviceImpl =
                (IExternalMailAccountService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            accountList = serviceImpl.read(map.getMultivaluedMap());
            for (MailAccount account : accountList) {
                CommonUtil.printOnBoth("extAccount = " + account);
            }
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && accountList != null) {
                result = CommonUtil.comparePayload(accountList.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int updateExtMailAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update ExternalMailAccountService";
        try {
            final IExternalMailAccountService serviceImpl =
                (IExternalMailAccountService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            serviceImpl.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int createExtMailAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Create ExternalMailAccountService";
        try {
            final IExternalMailAccountService serviceImpl =
                (IExternalMailAccountService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            serviceImpl.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int deleteExtMailAct(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "delete ExternalMailAccountService";
        try {
            final IExternalMailAccountService serviceImpl =
                (IExternalMailAccountService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            serviceImpl.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
