package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;

public class EmailAliasAPI {

    static Logger alltestlog = Logger.getLogger(MailboxAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
            case PUT: {
                CommonUtil.printstartAPI("ADD EMAIL-ALIAS");
                testStatus = create(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case GET: {
                CommonUtil.printstartAPI("GET EMAIL-ALIAS");
                testStatus = read(context, service, map, expResult, expectedOutput,
                        checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE EMAIL-ALIAS");
                testStatus = update(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case DELETE: {
                CommonUtil.printstartAPI("DELETE EMAIL-ALIAS");
                testStatus = delete(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
        }
        return 1;
    }

    public static int create(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Add emailalias";
        try {
            IEmailAliasService emailaliasService = (IEmailAliasService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            emailaliasService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        List<String> emailalias = null;
        String method = "Read emailalias";
        try {
            IEmailAliasService emailaliasService = (IEmailAliasService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            emailalias = emailaliasService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && emailalias != null) {
                result = CommonUtil.comparePayload(emailalias.toString(),
                        expectedOutput);

            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Update emailalias";
        try {
            IEmailAliasService emailaliasService = (IEmailAliasService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            emailaliasService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Delete emailalias";
        try {
            IEmailAliasService emailaliasService = (IEmailAliasService) context
                    .getService(service.name());
            CommonUtil.printOnBoth(method);
            emailaliasService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int verifyemailalias(List<String> emailalias,
            String expectedOutput) {

        System.out.println("Verifying emailalias Output ....");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs;
        int errorcount = 0;
        int paramexist = 0;
        alltestlog.info("paramKeyValPairs  ...  = " + expectKeyValPairs.length);

        // need to handle the case when expected output has non standard pattern
        // Would work it on at the time of tool enhancement phase

        if (expectKeyValPairs.length == 0) {
            return 1;
        }
        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";
            expectinterKeyValPairs = expectKeyValPairs[count].split(delimiter);
            if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.EMAILALIAS)) {
                paramexist = 1;
                String[] expectedlist = expectinterKeyValPairs[1].split("&");
                for (int count1 = 0; count1 < expectedlist.length; count1++) {
                    boolean yesorno = emailalias.contains(expectedlist[count]);
                    if (yesorno) {
                        alltestlog.info("Expected " + expectinterKeyValPairs[0]
                                + " = " + expectedlist[count] + " is found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + emailalias.toString());
                    } else {
                        alltestlog.info("ERROR :Expected "
                                + expectinterKeyValPairs[0] + " = "
                                + expectedlist[count] + " is NOT found in "
                                + " Actual " + expectinterKeyValPairs[0]
                                + " = " + emailalias.toString());
                        errorcount++;
                    }
                }
            }

            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[1]
                        + " is not present in actual output");
                errorcount++;
            }

        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }

}
