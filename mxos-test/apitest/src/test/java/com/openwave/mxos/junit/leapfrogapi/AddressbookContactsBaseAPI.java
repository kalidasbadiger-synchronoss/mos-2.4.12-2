package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;

public class AddressbookContactsBaseAPI {

    static Logger alltestlog = Logger.getLogger(AddressbookContactsBaseAPI.class);
    static String service = "Addressbook Contacts BASE API";

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);
       int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }

        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
        case GET: {
             CommonUtil.printstartAPI("GET ADDRESSBOOK CONTACTS BASE");
             testStatus = read(context, service, map, expResult,
                            expectedOutput, checkExpOutput);
             return testStatus;
                    }
        case POST: {
             CommonUtil.printstartAPI("UPDATE ADDRESSBOOK CONTACTS BASE");
             testStatus = update(context, service, map, expResult,
                                expectedOutput, checkExpOutput);
             return testStatus;
                    }
                }
        return 1;
    }

    public static int read(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        ContactBase contactBase = null;
        int result;
        try {
            final IContactsBaseService contactsBaseService =
                (IContactsBaseService) context.getService(service.name());
            CommonUtil.printOnBoth("read " + service + "service");
            contactBase = contactsBaseService.read(map.getMultivaluedMap());
            CommonUtil.printOnBoth("contactBase = " + contactBase);
            CommonUtil.printOnBoth("read " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("read " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        if (result == 1) {
            if (checkExpOutput == 1 && contactBase != null) {
                result = CommonUtil.comparePayload(contactBase.toString(),
                        expectedOutput);
            }
        } else {
            return 0;
        }
        return result;
    }

    public static int update(final IMxOSContext context, ServiceEnum service,
            MxosRequestMap map, String expErrorCode, String expectedOutput,
            int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            final IContactsBaseService contactsBaseService =
                (IContactsBaseService) context.getService(service.name());
            CommonUtil.printOnBoth("update " + service + "service");
            contactsBaseService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("update " + service + "service ..done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update " + service
                    + "service", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }
}
