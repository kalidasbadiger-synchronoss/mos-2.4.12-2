package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
//import com.opwvmsg.mxos.data.pojos.Message;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IHeaderService;
//import com.opwvmsg.mxos.interfaces.service.meta.IMessageHeaderService;
import com.opwvmsg.mxos.message.pojos.Header;


public class MessageHeaderAPI {
	static Logger alltestlog = Logger.getLogger(MessageHeaderAPI.class);

	 public static int execute(final IMxOSContext context, ServiceEnum service,
	            Operation operation, String inputstring, String testCaseName,
	            String expectedOutput, String expResult) {

	        int testStatus = 0;
	        MxosRequestMap map;
	        map = createInputParams.create(inputstring, operation);

	        int checkExpOutput = 1;
	        if (null == expectedOutput || expectedOutput == "") {
	            checkExpOutput = 0;
	        }
	        System.out.println("checkExpOutput =" + checkExpOutput);
	        System.out.println("expHttpOut =" + expResult);

	        switch (service) {
	        case HeaderService: {
	            switch (operation) {
	            case GET: {
	                CommonUtil.printstartAPI("GET FOLDER");
	                testStatus = readMessageHeader (context, service, map, expResult,
	                        expectedOutput, checkExpOutput);
	                return testStatus;
	            }
	            }
	        }
	        }
	     return 1;
	    }

	    public static int readMessageHeader(final IMxOSContext context,
	            ServiceEnum service, MxosRequestMap map, String expErrorCode,
	            String expectedOutput, int checkExpOutput) {
	        String errorCode = LfConstants.HTTPSUCCESS;
	        Header mHeader = null;
	        int result;
	        String method = "read MessageHeader";
      
	        Map<String, List<String>> multiMap = map.getMultivaluedMap();
	        CommonUtil.replcaePlaceholder(multiMap,
	                MessageProperty.messageId.name());
      
	        
	        try {
	            CommonUtil.printOnBoth(method);
	            IHeaderService messageHeaderService= (IHeaderService) context
          .getService(service.name());
	           mHeader = messageHeaderService.read(map.getMultivaluedMap());
	          
	            CommonUtil.printOnBoth("...MessageHeader  == " + mHeader);
	            CommonUtil.printOnBoth(method + "...Done");
	        } catch (MxOSException e) {
	            errorCode = CommonUtil.getPrintErrorCode(method, e);
	        }
	        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
	        if (result == 1) {
	            if (checkExpOutput == 1 && mHeader != null) {
	                result = CommonUtil.comparePayload(mHeader.toString(),
	                        expectedOutput);
	            }
	        }
	        return result;
	    }
	    
}	    

	
