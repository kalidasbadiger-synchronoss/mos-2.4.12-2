package com.openwave.mxos.junit.leapfrogapi;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnDeactivationService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnReactivationService;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnSwapService;

public class FionaMsisdnApi {

    static Logger alltestlog = Logger.getLogger(MailReceiptAPI.class);

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);

        switch (service) {
            case MsisdnDeactivationService: {
                switch (operation) {
                    case POST: {
                        CommonUtil.printstartAPI("MSISDN DEACTIVATION PROCESS");
                        testStatus = processMsisdnDeactivation(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MsisdnReactivationService: {
                switch (operation) {
                    case POST: {
                        CommonUtil.printstartAPI("MSISDN REACTIVATION PROCESS");
                        testStatus = processMsisdnReactivation(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
            case MsisdnSwapService: {
                switch (operation) {
                    case POST: {
                        CommonUtil.printstartAPI("MSISDN SWAP PROCESS");
                        testStatus = processMsisdnSwap(context, service, map,
                                expResult, expectedOutput, checkExpOutput);
                        return testStatus;
                    }
                }
            }
        }
        return 1;
    }

    public static int processMsisdnDeactivation(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Process MsisdnDeactivationService";
        try {
            CommonUtil.printOnBoth(method);
            IMsisdnDeactivationService MsisdnDeactivationService = (IMsisdnDeactivationService) context
                    .getService(service.name());
            MsisdnDeactivationService.process(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int processMsisdnReactivation(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Process MsisdnReactivationService";
        try {
            CommonUtil.printOnBoth(method);
            IMsisdnReactivationService MsisdnReactivationService = (IMsisdnReactivationService) context
                    .getService(service.name());
            MsisdnReactivationService.process(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int processMsisdnSwap(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map, String expErrorCode,
            String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        String method = "Process MsisdnSwapService";
        try {
            CommonUtil.printOnBoth(method);
            IMsisdnSwapService MsisdnSwapService = (IMsisdnSwapService) context
                    .getService(service.name());
            MsisdnSwapService.process(map.getMultivaluedMap());
            CommonUtil.printOnBoth(method + " ..done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode(method, e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

}
