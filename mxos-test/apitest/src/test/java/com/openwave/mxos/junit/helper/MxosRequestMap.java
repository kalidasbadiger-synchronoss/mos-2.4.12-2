package com.openwave.mxos.junit.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.core.MultivaluedMap;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.sun.jersey.core.util.MultivaluedMapImpl;

/**
 * @author bsatyam
 *
 */
public class MxosRequestMap {
    final MultivaluedMap<String, String> multivaluedMap;

    /**
     * Default constructor.
     */
    public MxosRequestMap() {
        multivaluedMap = new MultivaluedMapImpl();
    }

    /**
     * add value to a property.
     * 
     * @param property property
     * @param value value
     */
    public void add(final DataMap.Property property, final Object value) {
        add(property.name(), value.toString());
    }

    /**
     * add value to a property.
     * 
     * @param property property
     * @param value value
     */
    public void add(final String property, final Object value) {
        add(property, value.toString());
    }

    /**
     * add value to a property.
     * 
     * @param property property
     * @param value value
     */
    public void add(final DataMap.Property property, final String value) {
        add(property.name(), value);
    }

    /**
     * add value to a key.
     * 
     * @param key key
     * @param value value
     */
    public void add(final String key, final String value) {
        List<String> list = null;
        if (multivaluedMap.containsKey(key)) {
            list = multivaluedMap.get(key);
        } else {
            list = new ArrayList<String>();
        }
        list.add(value);
        multivaluedMap.put(key, list);
    }

    /**
     * method to get multivalued map.
     * 
     * @return multivalued map
     */
    public Map<String, List<String>> getMultivaluedMap() {
        return multivaluedMap;
    }

    /**
     * method to get String representation of Request Map.
     * 
     * @return String request map in string
     */
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for (Entry<String, List<String>> entry : multivaluedMap.entrySet()) {
            sb.append("\nKey = " + entry.getKey()).append(", Value = ");
            for (String value : entry.getValue()) {
                sb.append(value);
            }
        }
        return sb.toString();
    }
}
