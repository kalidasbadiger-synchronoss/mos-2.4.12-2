package com.openwave.mxos.junit.leapfrogapi;

import java.util.List;

import org.apache.log4j.Logger;

import com.openwave.mxos.junit.helper.CommonUtil;
import com.openwave.mxos.junit.helper.LfConstants;
import com.openwave.mxos.junit.helper.MxosRequestMap;
import com.openwave.mxos.junit.helper.createInputParams;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;

public class DomainAPI {

    static Logger alltestlog = Logger.getLogger("alltestlogs");

    public static int execute(final IMxOSContext context, ServiceEnum service,
            Operation operation, String inputstring, String testCaseName,
            String expectedOutput, String expResult) {

        int testStatus = 0;
        MxosRequestMap map;
        map = createInputParams.create(inputstring, operation);

        int checkExpOutput = 1;
        if (null == expectedOutput || expectedOutput == "") {
            checkExpOutput = 0;
        }
        System.out.println("checkExpOutput =" + checkExpOutput);
        System.out.println("expHttpOut =" + expResult);
        switch (operation) {
            case PUT: {
                CommonUtil.printstartAPI("PUT DOMAIN");
                testStatus = create(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case GET: {
                CommonUtil.printstartAPI("GET DOMAIN");
                testStatus = read(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case POST: {
                CommonUtil.printstartAPI("UPDATE DOMAIN");
                testStatus = update(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case DELETE: {
                CommonUtil.printstartAPI("DELETE DOMAIN");
                testStatus = delete(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
            case GETALL: {
                CommonUtil.printstartAPI("SEARCH DOMAINS");
                testStatus = search(context, service, map, expResult,
                        expectedOutput, checkExpOutput);
                return testStatus;
            }
        }
        return 1;
    }

    public static int create(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {

        String errorCode = LfConstants.HTTPSUCCESS;
        int result;
        try {
            CommonUtil.printOnBoth("create domain");
            IDomainService domainService = (IDomainService) context.getService(service.name());
            domainService.create(map.getMultivaluedMap());
            CommonUtil.printOnBoth("create domain .. Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("create domain", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int read(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        Domain domainpojo = null;
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;

        try {
            CommonUtil.printOnBoth("Reading Domain");
            IDomainService domainService = (IDomainService) context.getService(service.name());
            domainpojo = domainService.read(map.getMultivaluedMap());
            alltestlog.info("Domain = " + domainpojo);
            CommonUtil.printOnBoth("Reading Domain ... Done");

        } catch (MxOSException e) {

            errorCode = CommonUtil.getPrintErrorCode("read domain", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);

        if (result == 1) {
            if (checkExpOutput == 1 && domainpojo != null) {
                result = CommonUtil.comparePayload(domainpojo.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int update(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {

        String errorCode = LfConstants.HTTPSUCCESS;
        int result;

        try {
            CommonUtil.printOnBoth("updating Domain");
            IDomainService domainService = (IDomainService) context.getService(service.name());
            domainService.update(map.getMultivaluedMap());
            CommonUtil.printOnBoth("updating Domain .. Done");

        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("update domain", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int delete(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;

        try {
            CommonUtil.printOnBoth("Deleting Domain");
            IDomainService domainService = (IDomainService) context.getService(service.name());
            domainService.delete(map.getMultivaluedMap());
            CommonUtil.printOnBoth("Deleting Domain .. Done");
        } catch (MxOSException e) {
            errorCode = CommonUtil.getPrintErrorCode("delete domain", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);
        return result;
    }

    public static int search(final IMxOSContext context,
            ServiceEnum service, MxosRequestMap map,
            String expErrorCode, String expectedOutput, int checkExpOutput) {
        List<Domain> domianlist = null;
        String errorCode = LfConstants.HTTPSUCCESS;
        int result;

        try {
            CommonUtil.printOnBoth("Search Domain");
            IDomainService domainService = (IDomainService) context.getService(service.name());
            domianlist = domainService.search(map.getMultivaluedMap());
            alltestlog.info("Domain = " + domianlist);
            CommonUtil.printOnBoth("Search Domain ... Done");

        } catch (MxOSException e) {

            errorCode = CommonUtil.getPrintErrorCode("Search domain", e);
        }
        result = CommonUtil.compareErrorCode(errorCode, expErrorCode);

        if (result == 1) {
            if (checkExpOutput == 1 && domianlist != null) {
                result = CommonUtil.comparePayload(domianlist.toString(),
                        expectedOutput);
            }
        }
        return result;
    }

    public static int verifyDomainOutput(Domain domainpojo,
            String expectedOutput) {

        System.out.println("Verifying Domain Output ....");
        // alltestlog.info("------------------------------");
        alltestlog.info("----- Verifying Output -------");
        alltestlog.info("------------------------------");

        String delimiter = ",";
        String[] expectKeyValPairs = expectedOutput.split(delimiter);
        String[] expectinterKeyValPairs_temp = { null, null };
        String[] expectinterKeyValPairs = { null, null };
        int errorcount = 0;
        int paramexist = 0;
        int resultval = 0;
        // alltestlog.info("paramKeyValPairs  ...  = " +
        // expectKeyValPairs.length);

        if (expectKeyValPairs.length == 0) {
            return 1;
        }

        for (int count = 0; count < expectKeyValPairs.length; count++) {
            paramexist = 0;
            delimiter = "=";

            expectinterKeyValPairs_temp = expectKeyValPairs[count]
                    .split(delimiter);

            if (expectinterKeyValPairs_temp.length == 1) {
                expectinterKeyValPairs[0] = expectinterKeyValPairs_temp[0];
                expectinterKeyValPairs[1] = null;
            } else {
                expectinterKeyValPairs[0] = expectinterKeyValPairs_temp[0];
                expectinterKeyValPairs[1] = expectinterKeyValPairs_temp[1];
            }

            // alltestlog.info("expectinterKeyValPairs[0] " +
            // expectinterKeyValPairs[0] );
            // alltestlog.info("expectinterKeyValPairs[1] " +
            // expectinterKeyValPairs[1] );

            if (expectinterKeyValPairs[0].equalsIgnoreCase(LfConstants.DOMAIN)) {
                paramexist = 1;
                String actualVal = domainpojo.getDomain();
                resultval = CommonUtil.compOuputVal(actualVal,
                        expectinterKeyValPairs);
                errorcount = errorcount + resultval;
            }

            else if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.DOMAINTYPE)) {
                // paramexist = 1;

                // String actualVal = domainpojo.getType();
                // resultval =
                // CommonUtil.compOuputVal(actualVal,expectinterKeyValPairs);
                // errorcount = errorcount + resultval;
            }

            else if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.MAILRELAYHOST)) {
                paramexist = 1;
                String actualVal = domainpojo.getRelayHost();
                resultval = CommonUtil.compOuputVal(actualVal,
                        expectinterKeyValPairs);
                errorcount = errorcount + resultval;
            }

            else if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.MAILREWRITEDOMAIN)) {
                paramexist = 1;
                String actualVal = domainpojo.getAlternateDomain();
                resultval = CommonUtil.compOuputVal(actualVal,
                        expectinterKeyValPairs);
                errorcount = errorcount + resultval;
            } else if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.DEFAULTMAILBOX)) {
                paramexist = 1;
                String actualVal = domainpojo.getDefaultMailbox();
                resultval = CommonUtil.compOuputVal(actualVal,
                        expectinterKeyValPairs);
                errorcount = errorcount + resultval;
            } else if (expectinterKeyValPairs[0]
                    .equalsIgnoreCase(LfConstants.CUSTUMFIELD)) {
                paramexist = 1;
                String actualVal = domainpojo.getCustomFields();
                resultval = CommonUtil.compOuputVal(actualVal,
                        expectinterKeyValPairs);
                errorcount = errorcount + resultval;
            }
            if (paramexist == 0) {
                alltestlog.error("ERROR: paramer = "
                        + expectinterKeyValPairs[0]
                        + " is not present in actual output");
                errorcount++;
            }

        }
        if (errorcount == 0) {
            return 1;
        } else {
            System.err.println("ERROR: " + errorcount + " errors found");
            alltestlog.error("ERROR: " + errorcount + " errors found");
            return 0;
        }
    }

}
