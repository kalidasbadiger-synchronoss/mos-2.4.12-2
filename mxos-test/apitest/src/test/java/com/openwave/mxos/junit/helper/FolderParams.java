package com.openwave.mxos.junit.helper;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class FolderParams {


    Map<String,String> mailbox;

    public FolderParams(){
        mailbox = new HashMap<String,String>();
    }
    public void setFolderProperty(String propertyName, String propertyValue){
        mailbox.put(propertyName, propertyValue);
    }

    public String getFolderProperty(String propertyName){
        return mailbox.get(propertyName);
    }

    public String toString(){
        StringBuffer FolderAttributes = new StringBuffer();
        Set<String> keys = mailbox.keySet();

        Iterator<String> keyIterator = keys.iterator();
        while(keyIterator.hasNext()){
            String key = keyIterator.next();
            FolderAttributes.append(key);
            FolderAttributes.append("=");
            FolderAttributes.append(getFolderProperty(key));
            if(keyIterator.hasNext())
                FolderAttributes.append("&");
        }
        return FolderAttributes.toString();
    }

}

