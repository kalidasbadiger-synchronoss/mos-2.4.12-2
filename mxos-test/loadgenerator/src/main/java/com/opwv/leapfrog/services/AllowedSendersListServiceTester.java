package com.opwv.leapfrog.services;

import java.util.List;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedSendersListService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on AllowedSendersListService.
 * 
 * @author mxos-dev
 */
public class AllowedSendersListServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(AllowedSendersListServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public AllowedSendersListServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * AllowedSendersListService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createAllowedSendersList();
            }

            if (properties.isReadEnabled()) {
                readAllowedSendersList();
            }

            if (properties.isUpdateEnabled()) {
                updateAllowedSendersList();
            }

            if (properties.isDeleteEnabled()) {
                deleteAllowedSendersList();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for AllowedSendersListService.
     */
    private void createAllowedSendersList() {
        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.AllowedSendersListService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.allowedSender.name(),
                HelperFunctions.getRandomUser(properties));
        time = Stats.startTimer();
        try {
            IAllowedSendersListService testService = (IAllowedSendersListService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created Allowed Senders List for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for AllowedSendersListService.
     */
    private List<String> readAllowedSendersList() {
        List<String> allowedSendersList = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.AllowedSendersListService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IAllowedSendersListService testService = (IAllowedSendersListService) mxoscontext
                    .getService(serviceName.name());
            allowedSendersList = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                if (allowedSendersList != null && allowedSendersList.size() > 0) {
                    logger.debug("For user " + emailId + " AllowedSenders are "
                            + allowedSendersList.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " AllowedSendersList not set.");
                }
            }
            logger.info("Successfully read AllowedSendersList for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return allowedSendersList;
    }

    /**
     * Method for update operation for AllowedSendersListService.
     */

    private void updateAllowedSendersList() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.AllowedSendersListService;
        List<String> allowedSendersList = readAllowedSendersList();
        if (allowedSendersList != null && allowedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.oldAllowedSender.name(),
                    allowedSendersList.get(0));
            mxosRequestMap.add(MailboxProperty.newAllowedSender.name(),
                    HelperFunctions.getRandomUser(properties));
            time = Stats.startTimer();
            try {
                IAllowedSendersListService testService = (IAllowedSendersListService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated AllowedSendersList for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("AllowedSendersList is not set for user " + emailId);
        }
    }

    /**
     * Method for delete operation for AllowedSendersListService.
     */

    private void deleteAllowedSendersList() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.AllowedSendersListService;
        List<String> allowedSendersList = readAllowedSendersList();
        if (allowedSendersList != null && allowedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.allowedSender.name(),
                    allowedSendersList.get(0));
            time = Stats.startTimer();
            try {
                IAllowedSendersListService testService = (IAllowedSendersListService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address "
                        + allowedSendersList.get(0)
                        + " from AllowedSendersList for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("AllowedSendersList is not set for user " + emailId);
        }
    }
}
