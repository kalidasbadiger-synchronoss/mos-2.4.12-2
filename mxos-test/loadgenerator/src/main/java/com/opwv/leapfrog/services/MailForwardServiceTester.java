package com.opwv.leapfrog.services;

import java.util.List;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on MailForwardService.
 * 
 * @author mxos-dev
 */
public class MailForwardServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MailForwardServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public MailForwardServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for MailForwardService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createMailForward();
            }

            if (properties.isReadEnabled()) {
                readMailForward();
            }

            if (properties.isUpdateEnabled()) {
                updateMailForward();
            }

            if (properties.isDeleteEnabled()) {
                deleteMailForward();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for MailForwardService.
     */
    private void createMailForward() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.MailForwardService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.forwardingAddress.name(),
                HelperFunctions.getRandomUser(properties));
        time = Stats.startTimer();
        try {
            IMailForwardService testService = (IMailForwardService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            logger.info("Successfully created Mail Forwarding for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for MailForwardService.
     */
    private List<String> readMailForward() {

        List<String> mailForwardAddresses = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailForwardService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailForwardService testService = (IMailForwardService) mxoscontext
                    .getService(serviceName.name());
            mailForwardAddresses = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                if (mailForwardAddresses != null
                        && mailForwardAddresses.size() > 0) {
                    logger.debug("For user " + emailId
                            + " MailForwarding address are "
                            + mailForwardAddresses.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " MailForwarding address are not set.");
                }
            }
            logger.info("Successfully read Mail Forwarding for user " + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return mailForwardAddresses;
    }

    /**
     * Method for update operation for MailForwardService.
     */

    private void updateMailForward() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailForwardService;
        List<String> mailFwdAddresses = readMailForward();
        if (mailFwdAddresses != null && mailFwdAddresses.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.oldForwardingAddress.name(),
                    mailFwdAddresses.get(0));
            mxosRequestMap.add(MailboxProperty.newForwardingAddress.name(),
                    HelperFunctions.getRandomUser(properties));
            time = Stats.startTimer();
            try {
                IMailForwardService testService = (IMailForwardService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated Mail Forwarding for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("Mail forwarding is not set for user " + emailId);
        }
    }

    /**
     * Method for delete operation for MailForwardService.
     */

    private void deleteMailForward() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.MailForwardService;
        List<String> mailFwdAddresses = readMailForward();
        if (mailFwdAddresses != null && mailFwdAddresses.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.forwardingAddress.name(),
                    mailFwdAddresses.get(0));
            time = Stats.startTimer();
            try {
                IMailForwardService testService = (IMailForwardService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address "
                        + mailFwdAddresses.get(0)
                        + " from Mail Forwarding for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("Mail forwarding is not set for user " + emailId);
        }
    }
}
