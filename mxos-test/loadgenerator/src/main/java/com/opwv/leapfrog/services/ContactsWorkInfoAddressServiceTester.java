package com.opwv.leapfrog.services;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.LoadGeneratorConstants;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoAddressService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on ContactsWorkInfoAddressService.
 * 
 * @author mxos-dev
 */
public class ContactsWorkInfoAddressServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(ContactsWorkInfoAddressServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;
    private MxosRequestMap mxosRequestMap;
    private boolean createSuccessful;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public ContactsWorkInfoAddressServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * ContactsWorkInfoAddressService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            mxosRequestMap = new MxosRequestMap();
            createSuccessful = false;
            createContact();
            if (createSuccessful) {
                if (properties.isReadEnabled()) {
                    readContactsWorkInfoAddress();
                }

                if (properties.isUpdateEnabled()) {
                    updateContactsWorkInfoAddress();
                }

                deleteContact();
            }
            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for read operation for ContactsWorkInfoAddressService.
     */
    private void readContactsWorkInfoAddress() {

        currentOpr = Operation.GET;
        serviceName = ServiceEnum.ContactsWorkInfoAddressService;

        try {

            IContactsWorkInfoAddressService testService = (IContactsWorkInfoAddressService) mxoscontext
                    .getService(serviceName.name());
            time = Stats.startTimer();
            Address contactAddress = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully read contact work Info Address for user "
                    + emailId);
            if (properties.isDebug()) {
                logger.debug(contactAddress.toString());
            }
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for update operation for ContactsWorkInfoAddressService.
     */

    private void updateContactsWorkInfoAddress() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.ContactsWorkInfoAddressService;
        mxosRequestMap.add(LoadGeneratorConstants.CONTACTSTREETNAME,
                LoadGeneratorConstants.DEFAULTSTREETNAME);
        mxosRequestMap.add(LoadGeneratorConstants.CONTACTCITYNAME,
                LoadGeneratorConstants.DEFAULTCITYNAME);
        mxosRequestMap.add(LoadGeneratorConstants.CONTACTSTATE,
                LoadGeneratorConstants.DEFAULTSTATE);
        mxosRequestMap.add(LoadGeneratorConstants.CONTACTPOSTALCODE,
                LoadGeneratorConstants.DEFAULTPOSTALCODE);
        mxosRequestMap.add(LoadGeneratorConstants.CONTACTCOUNTRY,
                LoadGeneratorConstants.DEFAULTCOUNTRY);

        time = Stats.startTimer();
        try {
            IContactsWorkInfoAddressService testService = (IContactsWorkInfoAddressService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated contact work Info Address for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for create operation for ContactService.
     */
    private void createContact() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.ContactsService;
        mxosRequestMap = new MxosRequestMap();

        try {
            mxosRequestMap.add(AddressBookProperty.userId.name(), emailId);
            mxosRequestMap.add(AddressBookProperty.oxHttpURL.name(),
                    properties.getOxHttpURL());
            ExternalSession session = HelperFunctions.getSessionId(emailId,
                    properties.getOxHttpURL(), mxoscontext);
            logger.info("Successfully logged into Richmail for user " + emailId);
            mxosRequestMap.add(AddressBookProperty.sessionId.name(),
                    session.getSessionId());
            mxosRequestMap.add(AddressBookProperty.cookieString.name(),
                    session.getCookieString());
            final String firstName = properties.getUsernamePrefix()
                    + System.currentTimeMillis();
            mxosRequestMap.add(LoadGeneratorConstants.CONTACTFIRSTNAME,
                    firstName);
            mxosRequestMap.add(LoadGeneratorConstants.CONTACTNICKNAME,
                    firstName);
            mxosRequestMap.add(LoadGeneratorConstants.CONTACTDISPLAYNAME,
                    firstName);
            IContactsService testService = (IContactsService) mxoscontext
                    .getService(serviceName.name());
            time = Stats.startTimer();
            long contactId = testService.create(mxosRequestMap
                    .getMultivaluedMap());
            mxosRequestMap.add(AddressBookProperty.contactId.name(), contactId);
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created " + contactId
                    + " contact for user " + emailId);
            createSuccessful = true;
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for delete operation for ContactService.
     */

    private void deleteContact() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.ContactsService;
        time = Stats.startTimer();
        try {
            IContactsService testService = (IContactsService) mxoscontext
                    .getService(serviceName.name());
            testService.delete(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully deleted " + mxosRequestMap
                    + " contact for user " + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
