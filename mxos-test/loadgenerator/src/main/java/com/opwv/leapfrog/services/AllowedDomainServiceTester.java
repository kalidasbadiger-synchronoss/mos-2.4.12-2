package com.opwv.leapfrog.services;

import java.util.ArrayList;
import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on MailboxBaseService.
 * 
 * @author mxos-dev
 */
public class AllowedDomainServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(AllowedDomainServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public AllowedDomainServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for MailboxBaseService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            if (properties.isCreateEnabled()) {
                createAllowedDomains();
            }

            if (properties.isUpdateEnabled()) {
                updateAllowedDomains();
            }

            if (properties.isDeleteEnabled()) {
                deletAllowedDomains();
            }

            if (properties.isReadEnabled()) {
                readAllowedDomains();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for read operation for MailboxBaseService.
     */
    private void createAllowedDomains() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.AllowedDomainService;
        final String sampleallowdDomain = Long.toString(System
                .currentTimeMillis()) + ".com";
        final List<String> allowedDomainsList = new ArrayList<String>();
        allowedDomainsList.add(sampleallowdDomain);
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.allowedDomain.name(),
                allowedDomainsList);
        time = Stats.startTimer();
        try {
            IAllowedDomainService testService = (IAllowedDomainService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created allowd domain for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for search operation for MailboxBaseService.
     */
    private List<String> readAllowedDomains() {
        List<String> listOfDomains = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.AllowedDomainService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IAllowedDomainService testService = (IAllowedDomainService) mxoscontext
                    .getService(serviceName.name());
            listOfDomains = testService
                    .read(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                for (final String allowdDomain : listOfDomains) {
                    logger.debug("Domains allowed for user " + emailId + " "
                            + allowdDomain);
                }
            }
            logger.info("Successfully listed allowed Domains for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return listOfDomains;
    }

    /**
     * Method for update operation for AllowedDomainService.
     */

    private void updateAllowedDomains() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.AllowedDomainService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        List<String> listOfDomains = readAllowedDomains();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        if (listOfDomains != null && listOfDomains.size() > 0) {
            final String oldAllwedDomain = listOfDomains.get(0);
            final String newAllwedDomain = Long.toString(System
                    .currentTimeMillis()) + ".com";
            mxosRequestMap.add(MailboxProperty.oldAllowedDomain.name(),
                    oldAllwedDomain);
            mxosRequestMap.add(MailboxProperty.newAllowedDomain.name(),
                    newAllwedDomain);
            time = Stats.startTimer();
            try {
                IAllowedDomainService testService = (IAllowedDomainService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated allowed domain for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("No allowd domains to update for user " + emailId);
        }
    }

    /**
     * Method for delet operation for AllowedDomainService.
     */

    private void deletAllowedDomains() {

        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.AllowedDomainService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        List<String> listOfDomains = readAllowedDomains();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        if (listOfDomains != null && listOfDomains.size() > 0) {
            final String allowedDomain = listOfDomains.get(0);
            mxosRequestMap.add(MailboxProperty.allowedDomain.name(),
                    allowedDomain);
            time = Stats.startTimer();
            try {
                IAllowedDomainService testService = (IAllowedDomainService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted allowd domain "
                        + allowedDomain + " for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("No allowd domains to delete for user " + emailId);
        }
    }
}
