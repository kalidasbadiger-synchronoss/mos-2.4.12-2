/*
 *  * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *   *
 *    * The copyright to the computer software herein is the property of
 *     * Openwave Systems Inc. The software may be used and/or copied only
 *      * with the written permission of Openwave Systems Inc. or in accordance
 *       * with the terms and conditions stipulated in the agreement/contract
 *        * under which the software has been supplied.
 *         *
 *          * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/LoadGeneratorConstants.java#1 $
 *           */

/**
 *  * Constants used by Load Generator
 *   *
 *    * @author Shantanu
 *     */

package com.opwv.leapfrog.test;

import java.util.HashMap;
import java.util.Map;

/**
 * * Constants used by Load Generator. * * @author mxos-dev
 * */
public final class LoadGeneratorConstants {

    public static final String MXOSBASEURL = "mxosBaseUrl";
    public static final String MXOSCONNECTIONS = "mxosMaxConnections";
    public static final String BGCMXOSCONNECTIONS = "bgcMxosMaxConnections";
    public static final String BGCMXOSBASEURL = "bgcMxosBaseUrl";
    public static final String DEFAULTPASSWORD = "password";
    public static final String INBOX = "INBOX";
    public static final String FALSE = "false";
    public static final String TRUE = "true";
    public static final String NUMSTARTTHREADS = "numstartThreads";
    public static final String NUMENDTHREADS = "numendThreads";
    public static final String DEFAULTSEPERATOR = ",";
    public static final String TESTINTERVAL = "testInterval";
    public static final String OXHTTPURL = "oxHttpURL";
    public static final String OPRREAD = "read";
    public static final String OPRUPDATE = "update";
    public static final String OPRSEARCH = "search";
    public static final String OPRDELE = "delete";
    public static final String OPRCREATE = "create";
    public static final String OPRPROCESS = "process";
    public static final String OPRAUTHENTICATE = "authenticate";
    public static final String MBOXSTART = "mailboxIdStart";
    public static final String NUMMBOXES = "numMboxes";
    public static final String STATSINTERVAL = "statsInterval";
    public static final String DEBUG = "debug";
    public static final String TESTCOUNT = "testCount";
    public static final String REST = "rest";
    public static final String BACKEND = "backend";
    public static final String RUNTTOOLAS = "runToolAs";
    public static final String XMLFILE = "config/services.xml";
    public static final String UNLIMITEDQUOTA = "0";
    public static final String USERSRANDOM = "usersRandom";
    public static final String USRNAMEPREFIX = "usernamePrefix";
    public static final String DEFAULTDOMAIN = "defaultDomain";
    public static final long DEFAULTTHREADSLEEPTIME = 5000L;
    public static final String LOG4JPATH = "/config/log4j.xml";
    public static final String SERVICETOTEST = "serviceToTest";
    public static final String MANDATORYPROPERTY = "mandatory";
    public static final String OPTIONALPROPERTY = "optional";
    public static final String COUNTSEPERATOR = ":";
    public static final String MESSAGEFILES = "messageFiles";
    public static final int DEFAULTHSSIZE = 1024;
    public static final String DEFAULTPASSWORDTYPE = "clear";
    public static final String FOLDERNAMEPREFIX = "TestFolder";
    public static final String EQLTOOPERATOR = "==";
    public static final String[] SMSTYPES = { "smsonline", "basicnotification",
            "advancednotification", "passwordrecoverysms" };
    public static final String[] BMIFILTERACTIONS = { "Discard", "None",
            "Folder Deliver", "Add Header", "Subject Prefix" };
    public static final String[] COMMTOUCHACTIONS = { "Discard", "Allow",
            "Reject", "Tag" };
    public static final String[] MCAFEEACTIONS = { "Discard", "Allow",
            "Reject", "Repair", "Clean" };
    public static final String[] CLOUDMARKACTIONS = { "accept", "delete",
            "tag", "quarantine", "header" };
    public static final String[] AUTOREPLYMODE = { "none", "vacation", "reply",
            "echo" };
    public static final String[] BLOCKSENDERSACTION = { "none", "bounce",
            "reject", "toss", "sideline" };
    public static final String[] ACCESSTYPE = { "none", "all", "trusted" };
    public static final String[] CONFIRMATION = { "Yes", "No" };
    public static final String[] BOOLEAN = { "true", "false" };
    public static final String DEFAULTLOCALE = "en_US";
    public static final String XHEADER = "X-Header";
    public static final String RANDOMSTRING = "Hello, This is sample test Message to Use ";
    public static final String[] SOCIALNETWORKSITES = { "google+", "orkut",
            "facebook", "twitter", "ibibo" };
    public static final String ACTIVE = "activated";
    public static final String[] CUSTOMFIELDS = { "bgcEndOfContract:",
            "|bgcPending:", "|bgcMailboxMigrated:", "|bgcAntiSpamAllowed:" };
    public static final String DEFAULTCUSTOMFIELDS = "bgcMailboxMigrated:0|bgcFKey:test|bgcPSource:2|"
            + "bgcOptionsPriv:127|bgcOptionsPref:63|bgcNewsPref:1|"
            + "bgcExpireDaysPriv:1";
    public static final String[] USEREXPERIANCE = { "basic", "rich", "mobile",
            "tablet" };
    
    public static final String[] MAILBOXSTATUS = { "open", "closing", "locked",
            "closed", "pending", "inactive" };
    public static final String[] MARITALSTATUSVALUES = { "Married", "Single",
            "Divorcee" };
    public static final String MARITALSTATUS = "maritalStatus";
    public static final String CUSTOM = "custom";

    // Required for addressbook
    public static final String ISPRIVATE = "isPrivate";
    public static final String CONTACTNOTES = "notes";
    public static final String CONTACTFIRSTNAME = "firstName";
    public static final String CONTACTNICKNAME = "nickName";
    public static final String CONTACTDISPLAYNAME = "displayName";
    public static final String CONTACTCITYNAME = "city";
    public static final String CONTACTSTREETNAME = "street";
    public static final String CONTACTSTATE = "stateOrProvince";
    public static final String CONTACTPOSTALCODE = "postalCode";
    public static final String CONTACTCOUNTRY = "country";
    public static final String CONTACTMOBILE = "mobile";
    public static final String CONTACTPHONE = "phone1";
    public static final String CONTACTCOMPANYNAME = "company";
    public static final String DEFAULTCOMPANYNAME = "Openwave Systems Inc";
    public static final String CONTACTEMPLOYEEID = "employeeId";
    public static final String DEFAULTCITYNAME = "Redwood City";
    public static final String DEFAULTSTREETNAME = "Seaport Blvd";
    public static final String DEFAULTSTATE = "California";
    public static final String DEFAULTPOSTALCODE = "94063";
    public static final String DEFAULTCOUNTRY = "USA";

    public static final Map<String, String> TESTPARAMSMAP = new HashMap<String, String>() {
        private static final long serialVersionUID = 1L;
        {
            put(MXOSCONNECTIONS, " = 1");
            put(MXOSBASEURL, " = http://localhost:8080/mxos");
            put(BGCMXOSCONNECTIONS, " = 1");
            put(BGCMXOSBASEURL, " = http://localhost:8080/bgc-mxos");
            put(NUMSTARTTHREADS, " = 1");
            put(NUMENDTHREADS, " = 1");
            put(TESTCOUNT, " = 100");
            put(TESTINTERVAL, " = 100,86300");
            put(RUNTTOOLAS, " = rest or backend");
            put(OPRCREATE, " = true or false");
            put(OPRREAD, " = true or false");
            put(OPRUPDATE, " = true or false");
            put(OPRDELE, " = true or false");
            put(OPRSEARCH, " = true or false");
            put(OPRPROCESS, " = true or false");
            put(STATSINTERVAL, " = 20");
            put(USERSRANDOM, " = true or false");
            put(USRNAMEPREFIX, " = testuser%05d");
            put(DEFAULTDOMAIN, " = openwave.com");
            put(MESSAGEFILES, " = 1k:50,2k:50");
            put(MBOXSTART, " = 1000");
            put(NUMMBOXES, " = 1000");
            put(DEBUG, " = true or false");
        }
    };

    /* DELETE ME LATER */
    public static final String MAXMSGSIZE = "maxMsgSize";

    /**
     * * Constructor.
     * **/
    private LoadGeneratorConstants() {
    }
}
