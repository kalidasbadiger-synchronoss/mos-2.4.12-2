package com.opwv.leapfrog.services;

import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISieveBlockedSendersService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on SieveBlockedSendersService.
 * 
 * @author mxos-dev
 */
public class SieveBlockedSendersServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(SieveBlockedSendersServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public SieveBlockedSendersServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * SieveBlockedSendersService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createSieveBlockedSenders();
            }

            if (properties.isReadEnabled()) {
                readSieveBlockedSenders();
            }

            if (properties.isUpdateEnabled()) {
                updateSieveBlockedSenders();
            }

            if (properties.isDeleteEnabled()) {
                deleteSieveBlockedSenders();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for SieveBlockedSendersService.
     */
    private void createSieveBlockedSenders() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.SieveBlockedSendersService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.blockedSender.name(),
                HelperFunctions.getRandomUser(properties));
        time = Stats.startTimer();
        try {
            ISieveBlockedSendersService testService = (ISieveBlockedSendersService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            logger.info("Successfully created Sieve Blocked Senders for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for SieveBlockedSendersService.
     */
    private List<String> readSieveBlockedSenders() {

        List<String> sieveBlockedSendersList = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.SieveBlockedSendersService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            ISieveBlockedSendersService testService = (ISieveBlockedSendersService) mxoscontext
                    .getService(serviceName.name());
            sieveBlockedSendersList = testService.read(mxosRequestMap
                    .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                if (sieveBlockedSendersList != null
                        && sieveBlockedSendersList.size() > 0) {
                    logger.debug("For user " + emailId
                            + " SievecBlocked Senders address "
                            + sieveBlockedSendersList.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " ievecBlocked Senders not set.");
                }
            }
            logger.info("Successfully read BlockedSendersList for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return sieveBlockedSendersList;
    }

    /**
     * Method for update operation for SieveBlockedSendersService.
     */

    private void updateSieveBlockedSenders() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.SieveBlockedSendersService;
        List<String> sieveBlockedSendersList = readSieveBlockedSenders();
        if (sieveBlockedSendersList != null
                && sieveBlockedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.oldBlockedSender.name(),
                    sieveBlockedSendersList.get(0));
            mxosRequestMap.add(MailboxProperty.newBlockedSender.name(),
                    HelperFunctions.getRandomUser(properties));
            time = Stats.startTimer();
            try {
                ISieveBlockedSendersService testService = (ISieveBlockedSendersService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated SieveBlockedSenders for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("SieveBlockedSenders is not set for user " + emailId);
        }
    }

    /**
     * Method for delete operation for SieveBlockedSendersService.
     */

    private void deleteSieveBlockedSenders() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.SieveBlockedSendersService;
        List<String> sieveBlockedSendersList = readSieveBlockedSenders();
        if (sieveBlockedSendersList != null
                && sieveBlockedSendersList.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.blockedSender.name(),
                    sieveBlockedSendersList.get(0));
            time = Stats.startTimer();
            try {
                ISieveBlockedSendersService testService = (ISieveBlockedSendersService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address "
                        + sieveBlockedSendersList.get(0)
                        + " from SieveBlockedSenders for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("SieveBlockedSenders is not set for user " + emailId);
        }
    }
}
