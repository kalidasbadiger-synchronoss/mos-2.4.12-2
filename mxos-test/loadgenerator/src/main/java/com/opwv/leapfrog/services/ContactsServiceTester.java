package com.opwv.leapfrog.services;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on ContactService.
 * 
 * @author mxos-dev
 */
public class ContactsServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(ContactsServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;
    private MxosRequestMap mxosRequestMap;
    private boolean createSuccessful;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public ContactsServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for ContactService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }
            createSuccessful = false;
            mxosRequestMap = new MxosRequestMap();
            createContact();

            if (createSuccessful) {
                if (properties.isDeleteEnabled()) {
                    deleteContact();
                }
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for ContactService.
     */
    private void createContact() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.ContactsService;
        mxosRequestMap = new MxosRequestMap();

        try {

            mxosRequestMap.add(AddressBookProperty.userId.name(), emailId);
            mxosRequestMap.add(AddressBookProperty.oxHttpURL.name(),
                    properties.getOxHttpURL());
            ExternalSession session = HelperFunctions.getSessionId(emailId,
                    properties.getOxHttpURL(), mxoscontext);
            logger.info("Successfully logged into Richmail for user " + emailId);
            mxosRequestMap.add(AddressBookProperty.sessionId.name(),
                    session.getSessionId());
            mxosRequestMap.add(AddressBookProperty.cookieString.name(),
                    session.getCookieString());
            final String firstName = properties.getUsernamePrefix()
                    + System.currentTimeMillis();
            mxosRequestMap.add("firstName", firstName);
            mxosRequestMap.add("nickName", firstName);
            mxosRequestMap.add("displayName", firstName);
            IContactsService testService = (IContactsService) mxoscontext
                    .getService(serviceName.name());
            time = Stats.startTimer();
            long contactId = testService.create(mxosRequestMap
                    .getMultivaluedMap());
            mxosRequestMap.add(AddressBookProperty.contactId.name(), contactId);
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created " + contactId
                    + " contact for user " + emailId);
            createSuccessful = true;
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for delete operation for ContactService.
     */

    private void deleteContact() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.ContactsService;
        time = Stats.startTimer();
        try {
            IContactsService testService = (IContactsService) mxoscontext
                    .getService(serviceName.name());
            testService.delete(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully deleted " + mxosRequestMap
                    + " contact for user " + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
