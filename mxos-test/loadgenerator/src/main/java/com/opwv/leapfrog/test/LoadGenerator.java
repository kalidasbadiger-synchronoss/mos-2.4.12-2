/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/loadgenerator/src/main/java/com/openwave/db/LoadGenerator.java#1 $
 */

/**
 * LoadGenerator for Leapfrog
 *
 * @author mxos-dev
 */

package com.opwv.leapfrog.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.log4j.xml.DOMConfigurator;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * LoadGenerator class for testing MxOs2.0.
 * 
 * @author mxos-dev
 */
public final class LoadGenerator {

    private static void printHelp() {
        System.out
                .println("Usage: java LoadGenerator [-h|-c <servicename>|-l] <Test Property file>");
        System.exit(0);
    }

    /**
     * Main method for LoadGenerator.
     * 
     * @param args LoadGenerator class command line arguments
     * @throws IOException If any
     * @author mxos-dev
     */
    public static void main(String[] args) {

        String mycwd = System.getProperty("user.dir");
        String log4j = mycwd + LoadGeneratorConstants.LOG4JPATH;
        DOMConfigurator.configure(log4j);
        TestProperties properties = new TestProperties();
        Properties pFile = new Properties();
        ThreadBatchLauncher batch = null;
        long eachThreadOprCount = 0L;
        try {
            DefaultParams.readXML();
            processCommandLine(args);
            pFile.load(new FileInputStream(args[0]));
            validatePropertiesFile(pFile, properties);
            properties.setFlag(true);
            DefaultParams.createDefaultParams(properties);
        } catch (Exception e) {
            System.err.println("Exception occured while starting the Test.");
            e.printStackTrace();
            System.exit(1);
        }
        int numTotalBatches = (properties.getNumendThreads() / properties
                .getNumstartThreads()) + 1;
        int maxthreadpoolSize = properties.getNumendThreads() + numTotalBatches;
        ThreadPoolExecutor threadPool = new ThreadPoolExecutor(
                properties.getNumendThreads(), maxthreadpoolSize,
                LoadGeneratorConstants.DEFAULTTHREADSLEEPTIME,
                TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());

        try {
            batch = new ThreadBatchLauncher(properties);
        } catch (Exception e) {
            System.err.println("Exception occured during test initialization.");
            e.printStackTrace();
            System.exit(1);
        }

        if (properties.isReadEnabled() || properties.isCreateEnabled()
                || properties.isDeleteEnabled() || properties.isUpdateEnabled()
                || properties.isSearchEnabled()
                || properties.isProcessEnabled()
                || properties.isAuthenticateEnabled()) {
            if (!properties.isDurationRun()) {
                eachThreadOprCount = properties.getTestCount()
                        / properties.getNumendThreads();
                properties.setEachThreadOperationsCount(eachThreadOprCount);
            }

            for (int i = 0; i < numTotalBatches; i++) {
                try {
                    batch.startExecutors(threadPool);
                } catch (Exception e1) {
                    System.err.println("Exception occured while "
                            + "starting executor threads for the test.");
                    e1.printStackTrace();
                    System.exit(0);
                }
                try {
                    Thread.sleep(properties.getTestRampUpInterval() * 1000);
                } catch (InterruptedException e) {
                    System.err.println("Exception occured in "
                            + "thread sleep.");
                    System.err
                            .println("Running test without waiting for testRampUpInterval");
                    e.printStackTrace();
                }
            }

            try {
                Thread.sleep(properties.getTestDuration() * 1000);
            } catch (InterruptedException e) {
                System.err.println("Exception occured in thread sleep.");
                e.printStackTrace();
            }
            if (properties.isDurationRun()) {
                properties.setFlag(false);
            }

            do {
                try {
                    Thread.sleep(LoadGeneratorConstants.DEFAULTTHREADSLEEPTIME);
                } catch (InterruptedException ie) {
                    System.out.println("Error in Thread.sleep");
                }
            } while (threadPool.getActiveCount() > 0);

            System.out.println("Test Complete");
            System.out.println("Stopping all worker threads...");

            try {
                Thread.sleep(LoadGeneratorConstants.DEFAULTTHREADSLEEPTIME);
                threadPool.shutdown();
                while (!threadPool.isShutdown()) {
                    Thread.sleep(LoadGeneratorConstants.DEFAULTTHREADSLEEPTIME);
                }
            } catch (InterruptedException ie) {
                System.out.println("Error in Thread.sleep");
            }
            System.out.println("Done");
            System.exit(0);
        } else {
            System.out.println("No CRUD operation to test for component "
                    + properties.getServiceToTest());
            System.exit(0);
        }
    }

    private static void processCommandLine(String[] args) {
        Option toolHelp = new Option("h", false, "Prints help");
        Option listServices = new Option("l", false,
                "Lists the services available in tool for testing.");
        Option createDefaultParams = new Option("c", true,
                "Creates default test parameters for the given service.");
        Options options = new Options();
        options.addOption(toolHelp);
        options.addOption(listServices);
        options.addOption(createDefaultParams);

        CommandLineParser parser = new PosixParser();
        try {
            CommandLine cmdLine = parser.parse(options, args);

            if (args.length == 0) {
                printHelp();
            }
            if (cmdLine.hasOption("h")) {
                printHelp();
            } else if (cmdLine.hasOption("l")) {
                final List<String> listOfServices = DefaultParams
                        .getAvailableServices();
                System.out
                        .println("Following are the services availble to test");
                for (final String serviceName : listOfServices) {
                    System.out.println(serviceName + " - "
                            + DefaultParams.getServiceDescription(serviceName));
                }
                System.exit(0);
            } else if (cmdLine.hasOption("c")) {
                final String serviceName = cmdLine.getOptionValue("c");
                boolean validService = false;
                final List<String> listOfServices = DefaultParams
                        .getAvailableServices();
                for (final String availabeService : listOfServices) {
                    if (availabeService.equals(serviceName)) {
                        validService = true;
                        break;
                    }
                }
                if (validService) {

                    System.out.println(LoadGeneratorConstants.SERVICETOTEST
                            .concat(" = ").concat(serviceName));
                    Map<String, String> requiredParams = DefaultParams
                            .getRequiredParamsForService(serviceName);
                    final String[] allowedOpr = DefaultParams
                            .getOperationsAllowdInService(serviceName);
                    final String mandatoryParams = requiredParams
                            .get(LoadGeneratorConstants.MANDATORYPROPERTY);
                    final String optionalParams = requiredParams
                            .get(LoadGeneratorConstants.OPTIONALPROPERTY);

                    for (final String propertyName : mandatoryParams
                            .split(LoadGeneratorConstants.DEFAULTSEPERATOR)) {
                        if (!propertyName.equals("")) {
                            System.out.println(propertyName
                                    + LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(propertyName));
                        }
                    }
                    for (final String propertyName : optionalParams
                            .split(LoadGeneratorConstants.DEFAULTSEPERATOR)) {
                        if (!propertyName.equals("")) {
                            System.out.println(propertyName
                                    + LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(propertyName));
                        }
                    }

                    System.out
                            .println(LoadGeneratorConstants.MXOSCONNECTIONS
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.MXOSCONNECTIONS)));
                    System.out.println(LoadGeneratorConstants.MXOSBASEURL
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.MXOSBASEURL)));
                    System.out
                            .println(LoadGeneratorConstants.BGCMXOSCONNECTIONS
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.BGCMXOSCONNECTIONS)));
                    System.out
                            .println(LoadGeneratorConstants.BGCMXOSBASEURL
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.BGCMXOSBASEURL)));
                    System.out
                            .println(LoadGeneratorConstants.NUMSTARTTHREADS
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.NUMSTARTTHREADS)));
                    System.out
                            .println(LoadGeneratorConstants.NUMENDTHREADS
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.NUMENDTHREADS)));
                    System.out
                            .println("#Specify either testCount or testInterval");

                    System.out.println(LoadGeneratorConstants.TESTCOUNT
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.TESTCOUNT)));
                    System.out.println(LoadGeneratorConstants.TESTINTERVAL
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.TESTINTERVAL)));
                    System.out.println(LoadGeneratorConstants.RUNTTOOLAS
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.RUNTTOOLAS)));
                    System.out.println(LoadGeneratorConstants.USERSRANDOM
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.USERSRANDOM)));
                    System.out.println(LoadGeneratorConstants.DEBUG
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.DEBUG)));
                    System.out
                            .println("#Enable stats incase you are running the tool as rest client only");
                    System.out
                            .println(LoadGeneratorConstants.STATSINTERVAL
                                    .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                            .get(LoadGeneratorConstants.STATSINTERVAL)));
                    System.out
                            .println("#Enable debug incase you want to enable debug logs");
                    System.out.println(LoadGeneratorConstants.DEBUG
                            .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                    .get(LoadGeneratorConstants.DEBUG)));
                    System.out
                            .println("#Enable the required operations to test for the service");
                    for (final String oprName : allowedOpr) {
                        System.out.println(oprName
                                .concat(LoadGeneratorConstants.TESTPARAMSMAP
                                        .get(oprName)));
                    }
                } else {
                    System.out.println("Service " + serviceName
                            + " is not valid.");
                    System.exit(1);
                }
                System.exit(0);
            }
        } catch (Exception e) {
            printHelp();
        }
    }

    /**
     * Method for validating the input Property file for the test. It will
     * verify if the property file contains the mandatory parameters or not.
     * 
     * @param pfile Properties file
     * @param variablesFile The name for file containing the list of mandatory
     *            parameters for the tool.
     * @throws Exception is any.
     */

    public static void validatePropertiesFile(Properties pfile,
            TestProperties properties) throws InvalidDataException, IOException {

        properties.setCustomEnabled(pfile.getProperty(
                LoadGeneratorConstants.CUSTOM, LoadGeneratorConstants.TRUE));
        final String serviceName = pfile
                .getProperty(LoadGeneratorConstants.SERVICETOTEST);
        if (serviceName == null) {
            throw new InvalidDataException(
                    "Please specify valid service name to test: "
                            + LoadGeneratorConstants.SERVICETOTEST);
        } else {
            properties.setServiceToTest(serviceName);
        }

        final String runtoolAs = pfile
                .getProperty(LoadGeneratorConstants.RUNTTOOLAS);
        if (runtoolAs == null) {
            throw new InvalidDataException(
                    "Please specify valid service name to test: "
                            + LoadGeneratorConstants.RUNTTOOLAS);
        } else {
            properties.setRunToolAs(runtoolAs);
            if (properties.getRunToolAs().equalsIgnoreCase(
                    LoadGeneratorConstants.REST)) {
                final String bgcMxosUrl = pfile.getProperty(
                        LoadGeneratorConstants.BGCMXOSBASEURL, null);
                final String mxosbaseUrl = pfile.getProperty(
                        LoadGeneratorConstants.MXOSBASEURL, null);
                properties.setBgcMxosBaseUrl(bgcMxosUrl);
                properties.setMxosBaseUrl(mxosbaseUrl);
                final String statsInterval = pfile
                        .getProperty(LoadGeneratorConstants.STATSINTERVAL);
                if (statsInterval == null) {
                    throw new InvalidDataException(
                            "Please specify valid value for: "
                                    + LoadGeneratorConstants.STATSINTERVAL);
                } else {
                    properties.setStatsInterval(statsInterval);
                    startStatsLogger(properties.getStatsInterval());
                }

            }
        }

        final String numstartThreads = pfile
                .getProperty(LoadGeneratorConstants.NUMSTARTTHREADS);
        if (numstartThreads == null) {
            throw new InvalidDataException(
                    "Please specify valid number of threads for the test: "
                            + LoadGeneratorConstants.NUMSTARTTHREADS);
        } else {
            properties.setNumstartThreads(numstartThreads);
        }

        final String numendThreads = pfile
                .getProperty(LoadGeneratorConstants.NUMENDTHREADS);
        if (numendThreads == null) {
            throw new InvalidDataException(
                    "Please specify valid number of threads for the test: "
                            + LoadGeneratorConstants.NUMENDTHREADS);
        } else {
            properties.setNumendThreads(numendThreads);
        }

        final String testCount = pfile
                .getProperty(LoadGeneratorConstants.TESTCOUNT);
        final String testInterval = pfile
                .getProperty(LoadGeneratorConstants.TESTINTERVAL);

        if (testInterval == null && testCount == null) {
            System.out.println("Nothing to test since "
                    + LoadGeneratorConstants.TESTCOUNT + " and "
                    + LoadGeneratorConstants.TESTINTERVAL + " is undefined.");
            System.exit(1);
        }

        if (testInterval != null) {
            properties.setTestInterval(testInterval);
        } else if (testCount != null) {
            properties.setTestCount(testCount);
        }

        final String debugEnabled = pfile
                .getProperty(LoadGeneratorConstants.DEBUG);
        if (debugEnabled != null) {
            properties.setDebugEnabled(debugEnabled);
        }

        final String usersRandom = pfile
                .getProperty(LoadGeneratorConstants.USERSRANDOM);
        if (usersRandom != null) {
            properties.setUsersRandom(usersRandom);
        }

        Map<String, String> requiredParams = DefaultParams
                .getRequiredParamsForService(serviceName);

        final String[] mandatoryParams = requiredParams.get(
                LoadGeneratorConstants.MANDATORYPROPERTY).split(
                LoadGeneratorConstants.DEFAULTSEPERATOR);
        final String[] optionalParams = requiredParams.get(
                LoadGeneratorConstants.OPTIONALPROPERTY).split(
                LoadGeneratorConstants.DEFAULTSEPERATOR);

        for (final String paramName : mandatoryParams) {
            if (!paramName.equals("")) {
                properties.setMandatoryProperty(paramName,
                        pfile.getProperty(paramName));
            }
        }

        for (final String paramName : optionalParams) {
            if (!paramName.equals("")) {
                properties.setOptionalProperty(paramName,
                        pfile.getProperty(paramName));
            }
        }

        final String[] allowdOpr = DefaultParams
                .getOperationsAllowdInService(serviceName);
        properties.setServiceOperations(allowdOpr, pfile);

        if (!properties.isUsersRandom()) {
            long eachthreadMboxes = properties.getNumMboxes()
                    / properties.getNumstartThreads();
            properties.setEachthreadMboxes(eachthreadMboxes);
        }
    }

    /**
     * Method for initializing the test status logger.
     * 
     * @author mxos-dev
     */
    private static void startStatsLogger(int statsInterval) {
        Stats.init(statsInterval);
    }

    /**
     * Default Constructor.
     * 
     * @author mxos-dev
     */
    private LoadGenerator() {
    }
}
