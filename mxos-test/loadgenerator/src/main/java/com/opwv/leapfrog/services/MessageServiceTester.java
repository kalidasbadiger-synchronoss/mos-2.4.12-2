package com.opwv.leapfrog.services;

import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.List;
import java.util.ArrayList;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.LoadGeneratorConstants;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.MessageIDTerm;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

public class MessageServiceTester implements Runnable {

    private static Logger logger = Logger.getLogger(MessageServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;
    private int messagefileNum;
    private Map<String, Metadata> messageMetadata;

    public MessageServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isReadEnabled() || properties.isUpdateEnabled()
                    || properties.isDeleteEnabled()
                    || properties.isSearchEnabled()) {
                messageMetadata = listMessagesInFolder();
            }
            if (properties.isCreateEnabled()) {
                if (messagefileNum == properties.getMessageFiles().size()) {
                    messagefileNum = 0;
                }
                createMessage();
                messagefileNum++;
            }

            if (properties.isReadEnabled()) {
                readMessage();
            }

            if (properties.isUpdateEnabled()) {
                updateMessage();
            }

            if (properties.isSearchEnabled()) {
                searchMessage();
            }

            if (properties.isDeleteEnabled()) {
                deleteMessage();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }

            messageMetadata = null;
        }
    }

    /**
     * Method for create operation for MessageService.
     */
    private void createMessage() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.MessageService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(FolderProperty.folderName.name(),
                LoadGeneratorConstants.INBOX);
        mxosRequestMap.add(MxOSPOJOs.receivedFrom.name(), emailId);
        mxosRequestMap.add(MxOSPOJOs.message.name(), properties
                .getMessageFiles().get(messagefileNum));

        time = Stats.startTimer();
        try {
            IMessageService testService = (IMessageService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully created Blocked Senders List for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for MessageService.
     */
    private void readMessage() {

        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MessageService;
        if ((messageMetadata != null) & (messageMetadata.size() > 0)) {
            final String messageId = getRandomMessageId();
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(FolderProperty.folderName.name(),
                    LoadGeneratorConstants.INBOX);
            mxosRequestMap.add(MessageProperty.messageId.name(), messageId);
            time = Stats.startTimer();
            try {
                IMessageService testService = (IMessageService) mxoscontext
                        .getService(serviceName.name());
                testService.read(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully read message " + messageId
                        + " for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.error("Unable to read any message in INBOX folder for account "
                    + emailId);
        }
    }

    /**
     * Method for reading metadata for all messages in a folder operation for
     * MessageService.
     */
    private Map<String, Metadata> listMessagesInFolder() {

        currentOpr = Operation.GETALL;
        serviceName = ServiceEnum.MessageService;
        Map<String, Metadata> messageMetadataInFolder = null;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(FolderProperty.folderName.name(),
                LoadGeneratorConstants.INBOX);
        time = Stats.startTimer();
        try {
            IMetadataService testService = (IMetadataService) mxoscontext
                    .getService(ServiceEnum.MetadataService.name());
            messageMetadataInFolder = testService
                    .list(mxosRequestMap
                            .getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully read metadata for all the "
                    + "messages in INBOX folder for user " + emailId);
            if (properties.isDebug()) {
                Iterator<String> keyIterator = messageMetadataInFolder.keySet()
                        .iterator();
                while (keyIterator.hasNext()) {
                    String messageId = keyIterator.next();
                    logger.debug("Message ID: " + messageId
                            + " Message attributes "
                            + messageMetadataInFolder.get(messageId));
                }
            }
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return messageMetadataInFolder;
    }

    /**
     * Method for update operation for MessageService.
     */
    private void updateMessage() {

        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MessageService;
        if ((messageMetadata != null) & (messageMetadata.size() > 0)) {
            final String messageId = getRandomMessageId();
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(FolderProperty.folderName.name(),
                    LoadGeneratorConstants.INBOX);
            mxosRequestMap.add(MessageProperty.messageId.name(), messageId);
            mxosRequestMap.add(MessageProperty.flagSeen.name(),
                    HelperFunctions.getConfirmation());
            mxosRequestMap.add(MessageProperty.flagFlagged.name(),
                    HelperFunctions.getConfirmation());
            mxosRequestMap.add(MessageProperty.flagPriv.name(),
                    HelperFunctions.getConfirmation());
            mxosRequestMap.add(MessageProperty.flagRecent.name(),
                    HelperFunctions.getConfirmation());
            mxosRequestMap.add(MessageProperty.flagAns.name(),
                    HelperFunctions.getConfirmation());
            time = Stats.startTimer();
            try {
                IMetadataService testService = (IMetadataService) mxoscontext
                        .getService(ServiceEnum.MetadataService.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated message " + messageId
                        + " for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.error("Unable to update any message in INBOX folder for account "
                    + emailId);
        }
    }

    /**
     * Method for read operation for MessageService.
     */
    private void deleteMessage() {

        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.MessageService;
        if ((messageMetadata != null) & (messageMetadata.size() > 0)) {
            final String messageId = getRandomMessageId();
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(FolderProperty.folderName.name(),
                    LoadGeneratorConstants.INBOX);
            mxosRequestMap.add(MessageProperty.messageId.name(), messageId);
            time = Stats.startTimer();
            try {
                IMessageService testService = (IMessageService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted message " + messageId
                        + " for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.error("Unable to delete any message in INBOX folder for account "
                    + emailId);
        }
    }

    private String getRandomMessageId() {

        String msgId = null;
        List<String> listOfMsgIds = new ArrayList<String>(
                messageMetadata.keySet());
        int numMessages = listOfMsgIds.size();
        Random randomMsg = new Random();
        msgId = listOfMsgIds.get(randomMsg.nextInt(numMessages));
        return msgId;
    }

    /**
     * Method for read operation for MessageService.
     */
    private void searchMessage() {

        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MessageService;
        if ((messageMetadata != null) & (messageMetadata.size() > 0)) {
            final String messageId = getRandomMessageId();
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(FolderProperty.folderName.name(),
                    LoadGeneratorConstants.INBOX);
            
            SearchTerm searchTerm = new MessageIDTerm(messageId);
            time = Stats.startTimer();
            try {
                IMetadataService testService = (IMetadataService) mxoscontext
                        .getService(ServiceEnum.MetadataService.name());
                Map<String,Map<String,Metadata>> searchResults = testService
                        .search(mxosRequestMap.getMultivaluedMap(), searchTerm);
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully read message " + messageId
                        + " for user " + emailId);
                if (properties.isDebug()) {
                    Iterator<String> keyIterator = searchResults.keySet()
                            .iterator();
                    while (keyIterator.hasNext()) {
                        String messageFound = keyIterator.next();
                        logger.debug("Found Message ID: " + messageFound
                                + " Message attributes "
                                + searchResults.get(messageFound));
                    }
                }
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.error("Unable to read any message in INBOX folder for account "
                    + emailId);
        }
    }
}
