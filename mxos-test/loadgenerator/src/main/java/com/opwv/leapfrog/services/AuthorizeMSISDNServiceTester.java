package com.opwv.leapfrog.services;

import org.apache.log4j.Logger;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IAuthorizeMSISDNService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

public class AuthorizeMSISDNServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(AuthorizeMSISDNServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long time;
    private int threadId;
    private Operation currentOpr;
    private ServiceEnum serviceName;

    public AuthorizeMSISDNServiceTester(Integer tId, TestProperties properties,
            IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
    }

    @Override
    public void run() {
        while (myFlag) {
            if (properties.isProcessEnabled()) {
                authorizeMsisdn();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    private void authorizeMsisdn() {
        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.AuthorizeMsisdnService;
        final String phoneNumber = HelperFunctions.getPhoneNumber();
        final String smsType = HelperFunctions.getSmsType();
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(SmsProperty.smsType.name(), smsType);
        mxosRequestMap.add(SmsProperty.authorizeMsisdn.name(), phoneNumber);
        time = Stats.startTimer();
        try {
            IAuthorizeMSISDNService testService = (IAuthorizeMSISDNService) mxoscontext
                    .getService(serviceName.name());
            testService.process(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully Authorized MSISDN " + phoneNumber
                    + " for sms type " + smsType);
        } catch (Exception e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage(), e);
        }
    }
}
