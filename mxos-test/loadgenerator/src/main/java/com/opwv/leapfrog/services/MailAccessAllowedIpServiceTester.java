package com.opwv.leapfrog.services;

import java.util.List;
import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;

/**
 * Class for doing CRUD operations on MailAccessAllowedIpService.
 * 
 * @author mxos-dev
 */
public class MailAccessAllowedIpServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MailAccessAllowedIpServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public MailAccessAllowedIpServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * MailAccessAllowedIpService.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isCreateEnabled()) {
                createMailAccessAllowedIp();
            }

            if (properties.isReadEnabled()) {
                readMailAccessAllowedIp();
            }

            if (properties.isUpdateEnabled()) {
                updateMailAccessAllowedIp();
            }

            if (properties.isDeleteEnabled()) {
                deleteMailAccessAllowedIp();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for create operation for MailAccessAllowedIpService.
     */
    private void createMailAccessAllowedIp() {

        currentOpr = Operation.PUT;
        serviceName = ServiceEnum.MailAccessAllowedIpService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.allowedIP.name(),
                HelperFunctions.generateRandomIpAddress());
        time = Stats.startTimer();
        try {
            IMailAccessAllowedIpService testService = (IMailAccessAllowedIpService) mxoscontext
                    .getService(serviceName.name());
            testService.create(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            logger.info("Successfully created Mail Access Allowed Ips for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }

    /**
     * Method for read operation for MailAccessAllowedIpService.
     */
    private List<String> readMailAccessAllowedIp() {

        List<String> allowedIps = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailAccessAllowedIpService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailAccessAllowedIpService testService = (IMailAccessAllowedIpService) mxoscontext
                    .getService(serviceName.name());
            allowedIps = testService.read(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);

            if (properties.isDebug()) {
                if (allowedIps != null && allowedIps.size() > 0) {
                    logger.debug("For user " + emailId
                            + " Mail Access Allowed Ips are "
                            + allowedIps.toString());
                } else {
                    logger.debug("For user " + emailId
                            + " Mail Access Allowed Ips are not set.");
                }
            }
            logger.info("Successfully read Mail Access Allowed Ips for user "
                    + emailId);
        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return allowedIps;
    }

    /**
     * Method for update operation for MailAccessAllowedIpService.
     */

    private void updateMailAccessAllowedIp() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailAccessAllowedIpService;
        List<String> allowedIps = readMailAccessAllowedIp();
        if (allowedIps != null && allowedIps.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.oldAllowedIP.name(),
                    allowedIps.get(0));
            mxosRequestMap.add(MailboxProperty.newAllowedIP.name(),
                    HelperFunctions.generateRandomIpAddress());
            time = Stats.startTimer();
            try {
                IMailAccessAllowedIpService testService = (IMailAccessAllowedIpService) mxoscontext
                        .getService(serviceName.name());
                testService.update(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully updated MailAccessAllowedIp for user "
                        + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("MailAccessAllowedIp are not set for user " + emailId);
        }
    }

    /**
     * Method for delete operation for MailAccessAllowedIpService.
     */

    private void deleteMailAccessAllowedIp() {
        currentOpr = Operation.DELETE;
        serviceName = ServiceEnum.MailAccessAllowedIpService;
        List<String> allowedIps = readMailAccessAllowedIp();
        if (allowedIps != null && allowedIps.size() > 0) {
            MxosRequestMap mxosRequestMap = new MxosRequestMap();
            mxosRequestMap.add(MailboxProperty.email.name(), emailId);
            mxosRequestMap.add(MailboxProperty.allowedIP.name(),
                    allowedIps.get(0));
            time = Stats.startTimer();
            try {
                IMailAccessAllowedIpService testService = (IMailAccessAllowedIpService) mxoscontext
                        .getService(serviceName.name());
                testService.delete(mxosRequestMap.getMultivaluedMap());
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
                logger.info("Successfully deleted address " + allowedIps.get(0)
                        + " from MailAccessAllowedIp for user " + emailId);
            } catch (MxOSException e) {
                Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
                logger.error(
                        currentOpr.name() + " for service "
                                + serviceName.name() + " Failed with error: "
                                + e.getMessage() + " for user " + emailId, e);
            }
        } else {
            logger.warn("MailAccessAllowedIp are not set for user " + emailId);
        }
    }
}
