package com.opwv.leapfrog.services;

import com.opwv.leapfrog.test.HelperFunctions;
import com.opwv.leapfrog.test.MxosRequestMap;
import com.opwv.leapfrog.test.TestProperties;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.data.pojos.SieveFilters;

/**
 * Class for doing CRUD operations on MailSendService.
 * 
 * @author mxos-dev
 */
public class MailReceiptSieveFiltersServiceTester implements Runnable {

    private static Logger logger = Logger
            .getLogger(MailReceiptSieveFiltersServiceTester.class);
    private TestProperties properties;
    private IMxOSContext mxoscontext;
    private long myOprCount;
    private boolean myFlag;
    private long useridStart;
    private long useridEnd;
    private int threadId;
    private String emailId;
    private Operation currentOpr;
    private long time;
    private ServiceEnum serviceName;

    /**
     * Constructor.
     * 
     * @param tId Thread id
     * @param properties TestProperties used by LoadGenerator
     * @param mxoscontext IMxOSContext class object
     * 
     */
    public MailReceiptSieveFiltersServiceTester(Integer tId,
            TestProperties properties, IMxOSContext mxoscontext) {
        this.properties = properties;
        this.mxoscontext = mxoscontext;
        this.threadId = tId.intValue();
        myFlag = true;
        myOprCount = 0L;
        if (!properties.isUsersRandom()) {
            useridStart = (threadId * properties.getEachthreadMboxes())
                    + properties.getMboxIdStart();
            useridEnd = useridStart + properties.getEachthreadMboxes() - 1;
        }
    }

    /**
     * Thread run method for running CRUD operations for
     * MailReceiptSieveFilters.
     */
    @Override
    public void run() {
        while (myFlag) {

            if (!properties.isUsersRandom()) {
                emailId = String.format(properties.getUsernamePrefix(),
                        useridStart) + properties.getDefaultDomain();
                useridStart++;
                if (useridStart > useridEnd) {
                    useridStart = (threadId * properties.getEachthreadMboxes())
                            + properties.getMboxIdStart();
                }
            }
            if (properties.isUsersRandom()) {
                emailId = HelperFunctions.getRandomUser(properties);
            }

            if (properties.isReadEnabled()) {
                readMailReceiptSieveFilters();
            }

            if (properties.isUpdateEnabled()) {
                updateMailReceiptSieveFilters();
            }

            if (!properties.isDurationRun()) {
                myOprCount++;
                if (myOprCount == properties.getEachThreadOperationsCount()) {
                    myFlag = false;
                }
            } else {
                myFlag = properties.isflagSet();
            }
        }
    }

    /**
     * Method for read operation for MailReceiptSieveFiltersService.
     */
    private SieveFilters readMailReceiptSieveFilters() {
        SieveFilters sieveFilters = null;
        currentOpr = Operation.GET;
        serviceName = ServiceEnum.MailReceiptSieveFiltersService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        time = Stats.startTimer();
        try {
            IMailReceiptSieveFiltersService testService = (IMailReceiptSieveFiltersService) mxoscontext
                    .getService(serviceName.name());
            sieveFilters = testService.read(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            if (properties.isDebug()) {
                logger.debug("For user " + emailId
                        + " MailReceiptSieveFilters attributes are "
                        + sieveFilters.toString());
            }
            logger.info("Successfully read MailReceiptSieveFilters for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
        return sieveFilters;
    }

    /**
     * Method for update operation for MailReceiptSieveFiltersService.
     */
    private void updateMailReceiptSieveFilters() {
        currentOpr = Operation.POST;
        serviceName = ServiceEnum.MailReceiptSieveFiltersService;
        MxosRequestMap mxosRequestMap = new MxosRequestMap();
        mxosRequestMap.add(MailboxProperty.email.name(), emailId);
        mxosRequestMap.add(MailboxProperty.sieveFilteringEnabled.name(),
                HelperFunctions.getConfirmation());
        mxosRequestMap.add(MailboxProperty.blockedSenderAction.name(),
                HelperFunctions.getBlockSendersAction());
        mxosRequestMap.add(MailboxProperty.blockedSenderMessage.name(),
                HelperFunctions.getRandomString());
        mxosRequestMap.add(MailboxProperty.rejectBouncedMessage.name(),
                HelperFunctions.getConfirmation());
        time = Stats.startTimer();
        try {
            IMailReceiptSieveFiltersService testService = (IMailReceiptSieveFiltersService) mxoscontext
                    .getService(serviceName.name());
            testService.update(mxosRequestMap.getMultivaluedMap());
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.pass);
            logger.info("Successfully updated MailReceiptSieveFilters for user "
                    + emailId);

        } catch (MxOSException e) {
            Stats.stopTimer(serviceName, currentOpr, time, StatStatus.fail);
            logger.error(
                    currentOpr.name() + " for service " + serviceName.name()
                            + " Failed with error: " + e.getMessage()
                            + " for user " + emailId, e);
        }
    }
}
