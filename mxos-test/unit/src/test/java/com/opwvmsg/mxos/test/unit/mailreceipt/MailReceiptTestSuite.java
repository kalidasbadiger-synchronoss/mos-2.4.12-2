package com.opwvmsg.mxos.test.unit.mailreceipt;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailReceipt 
@SuiteClasses({
    MailReceiptGET.class,
    MailReceiptPOST.class,
    MailReceiptMcAfeeFiltersGET.class,
    MailReceiptMcAfeeFiltersPOST.class,
    MailReceiptSenderBlockingGET.class,
    MailReceiptSenderBlockingPOST.class,
    MailReceiptSieveFiltersGET.class,
    MailReceiptSieveFiltersPOST.class,
    MailReceiptBmiFiltersGET.class,
    MailReceiptBmiFiltersPOST.class,
    MailReceiptCloudmarkFiltersGET.class,
    MailReceiptCloudmarkFiltersPOST.class,
    MailReceiptCommtouchFiltersGET.class,
    MailReceiptCommtouchFiltersPOST.class,
    AddressForLocalDeliveryTest.class,
    AllowedSendersTest.class,
    BlockedSendersTest.class,
    SieveBlockedSendersTest.class,
    MailReceiptAdminControlGET.class,
    MailReceiptAdminControlPOST.class
})
public class MailReceiptTestSuite {
}
