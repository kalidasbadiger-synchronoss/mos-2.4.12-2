package com.opwvmsg.mxos.test.unit.cos.credentials;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosCredentialsService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;


/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosCredentialsTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosCredentialsService cosCredentialsService;
    private static ICosService cosService;
    private static IMxOSContext context;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosCredentialsTest....setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosCredentialsService = (ICosCredentialsService) context
                .getService(ServiceEnum.CosCredentialsService.name());
        cosService = (ICosService) context
                .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        
    }

 

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);

        try {
            cosService.create(inputParams);

        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static Credentials GetCosCredentialsParams(
            Map<String, List<String>> params) {

        return GetCosCredentialsParams(params, null);
    }

    private static Credentials GetCosCredentialsParams(
            Map<String, List<String>> params, String expectedError) {

        Credentials credentials = null;
        try {
            credentials = cosCredentialsService.read(params);
            if (null != expectedError) {
                fail("This should not have come!");
            } else {

                assertNotNull("Credentials object is null.", credentials);
            }
        } catch (MxOSException me) {
            if (null == expectedError) {
                fail("This should not have come!");
            } else {
                assertNotNull("MxOSError is not null", me);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, me.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return credentials;
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CredentialsTest...tearDownAfterClass");
        params.clear();
        deleteCos(COS_ID);
        cosCredentialsService = null;
        cosService = null;
    }

    @Test
    public void testGetCosCrdentialsWithoutCosId() {
        System.out
                .println("CosCredentialsTest.testGetCosCrdentialsWithoutCosId.");
        params.clear();
        GetCosCredentialsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
     
    }

    @Test
    public void testGetCosCrdentialsWithNullCosId() {
        System.out
                .println("CosCredentialsTest.testGetCosCrdentialsWithNullCosId.");
        params.clear();
        params.put(COSID_KEY, new ArrayList<String>());
        params.get(COSID_KEY).clear();
         
        GetCosCredentialsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetCosCrdentialsWithNonExistingCosId() {
        System.out
                .println("CosCredentialsTest.testGetCosCrdentialsWithNonExistingCosId.");

       
        params.clear();
        params.put(COSID_KEY, new ArrayList<String>());
        params.get(COSID_KEY).add("some_junk_cos124");
      
        GetCosCredentialsParams(params, CosError.COS_NOT_FOUND.name());
    }

    @Test
    public void testPasswordRecoveryAllowedSuccess() {
        System.out
                .println("CosCredentialsTest.testPasswordRecoveryAllowedSuccess.");
        params.clear();
        params.put(COSID_KEY, new ArrayList<String>());
        params.get(COSID_KEY).add(COS_ID);
        BooleanType pra = GetCosCredentialsParams(params)
                .getPasswordRecoveryAllowed();
        assertNotNull("PasswordRecoveryAllowed is null.", pra);
    }

    // not part of cos get, commenting code
    /* @Test
    public void testPasswordRecoveryPreferenceSuccess() {
        System.out
                .println("CosCredentialsTest.testPasswordRecoveryPreferenceSuccess.");
        params.clear();
        params.put(COSID_KEY, new ArrayList<String>());
        params.get(COSID_KEY).add(COS_ID);
       PasswordRecoveryPreference prp = GetCosCredentialsParams(params)
                .getPasswordRecoveryPreference();       
        assertNotNull("PasswordRecoveryPreference is null.", prp);
    } */
    
    @Test
    public void testMaxFailedLoginAttemptsSuccess() {
        System.out
                .println("CosCredentialsTest.testMaxFailedLoginAttemptsSuccess.");
        params.clear();
        params.put(COSID_KEY, new ArrayList<String>());
        params.get(COSID_KEY).add(COS_ID);
       Integer mfla = GetCosCredentialsParams(params)
                .getMaxFailedLoginAttempts();
        assertNotNull("testMaxFailedLoginAttemptsSuccess is null.", mfla);
    }

}
