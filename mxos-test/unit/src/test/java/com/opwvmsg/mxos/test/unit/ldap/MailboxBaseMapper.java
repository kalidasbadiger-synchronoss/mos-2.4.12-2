/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.test.unit.ldap;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.ldap.LDAPMapper;
import com.opwvmsg.mxos.data.pojos.Base;

/**
 * class
 * 
 * @author mxos-dev
 * 
 */
public class MailboxBaseMapper implements LDAPMapper<Base> {

    public Base mapping(Attributes attributes) {
        Base base = new Base();

        try {
            Attribute attr = attributes.get(LDAPMailboxProperty.mail.name());
            if (attr != null) {
                base.setEmail((String) attr.get());
            }
            attr = attributes.get(LDAPMailboxProperty.cn.name());
            if (attr != null) {
                base.setFirstName(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.sn.name());
            if (attr != null) {
                base.setLastName(attr.get().toString());
            }
            attr = attributes.get(LDAPMailboxProperty.mailboxid.name());
            if (attr != null) {
                base.setMailboxId(attr.get().toString());
            }
        } catch (NamingException e) {
            e.printStackTrace();
        }

        return base;
    }

}
