package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsListGET {

    private static final String TEST_NAME = "ContactsListGET";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static List<Contact> contactList = null;
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsService contactsService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsService = (IContactsService) ContextUtils.loadContext()
                .getService(ServiceEnum.ContactsService.name());
        login(USERID, PASSWORD);

        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // create post
    @Test
    public void testContactsList() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsList");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.sortKey.name(), new ArrayList<String>());
        params.get(AddressBookProperty.sortKey.name()).add("contactId");

        params.put(AddressBookProperty.sortOrder.name(), new ArrayList<String>());
        params.get(AddressBookProperty.sortOrder.name()).add("descending");

        params.put(AddressBookProperty.query.name(), new ArrayList<String>());
        params.get(AddressBookProperty.query.name()).add("contactId==" + CONTACTID);

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());
            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        contactList = contactsService.list(params);
        assertTrue(contactList != null);
        assertTrue("Contact list size is not greater than zero.",
                contactList.size() > 0);
        String contactId = contactList.get(0).getContactBase().getContactId();
        assertNotNull("ContactId is null.", contactId);
        assertTrue(Long.valueOf(contactId) == CONTACTID);
    }

}
