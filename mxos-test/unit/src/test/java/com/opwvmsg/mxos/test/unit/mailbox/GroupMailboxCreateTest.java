package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.DomainHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 */
public class GroupMailboxCreateTest {
    private static final String COSID = "default";
    private static final String DOMAIN_NAME = "openwave.com";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100001@" + DOMAIN_NAME;
    private static final String EMAIL2 = "mx100002@" + DOMAIN_NAME;
    private static final String EMAIL3 = "mx100003@" + DOMAIN_NAME;
    private static final String PWD_KEY = MailboxProperty.password.name();
    private static final String PWD = "test";
    private static final String GROUP_KEY = MailboxProperty.groupName.name();
    private static final String GROUP_VALUE = "testFamily.com";
    private static final String TYPE_KEY = MailboxProperty.type.name();
    private static final String TYPE_VALUE = "groupAdmin";
    private static final String TYPE_VALUE1 = "groupMailbox";
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("FamilyMailboxCreateTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        inputParams.clear();
        try {
            CosHelper.createCos(COSID);
            DomainHelper.createDomain(DOMAIN_NAME);
        } catch (Exception e) {
        }
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("FamilyMailboxCreateTest.setUp...");
    }
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("FamilyMailboxCreateTest.tearDown...");
    }
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("FamilyMailboxCreateTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        try {
            MailboxHelper.deleteMailbox(EMAIL);
            MailboxHelper.deleteMailbox(EMAIL2);
            MailboxHelper.deleteMailbox(EMAIL3);
            CosHelper.deleteCos(COSID);
            DomainHelper.deleteDomain(DOMAIN_NAME);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
        mailboxService = null;
    }
    
       
    @Test
    public void testCreateFamilyMailboxHOHWithNullGroup() {
        System.out.println("testCreateFamilyMailboxHOHWithNullGroup...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(null);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(TYPE_VALUE);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testCreateFamilyMailboxHOHWithNullType() {
        System.out.println("testCreateFamilyMailboxHOHWithNullType...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(null);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(null);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testCreateFamilyMailboxHOH() {
        System.out.println("testCreateFamilyMailboxHOH...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(GROUP_VALUE);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(TYPE_VALUE);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
            e.printStackTrace();
        }
    }
    
    @Test
    public void testCreateFamilyMailboxChildWithNullType(){
        System.out.println("testCreateFamilyMailboxChildWithNullType...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(null);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testCreateFamilyMailboxChild() {
        System.out.println("testCreateFamilyMailboxChild...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(GROUP_VALUE);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(TYPE_VALUE1);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateFamilyMailboxChild2() {
        System.out.println("testCreateFamilyMailboxChild2...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL3);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(GROUP_VALUE);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(TYPE_VALUE1);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
}