package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.PersonalInfo;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.MaritalStatus;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsPersonalInfoPOST {

    private static final String TEST_NAME = "ContactPersonalInfoPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsPersonalInfoService contactsPersonalInfoService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;
    }

    private static PersonalInfo getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static PersonalInfo getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        PersonalInfo info = null;
        try {
            info = contactsPersonalInfoService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Personal Info object is null.", info);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return info;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setEmptyParams(Map<String, List<String>> params) {
        try {
            contactsPersonalInfoService.update(params);

            Map<String, List<String>> inputParam = getBasicParams(params);

            PersonalInfo info = getParams(inputParam);

            assertNull("spouse name is not null", info.getSpouseName());

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertNotNull("MxOSException is null", e);
        }
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsPersonalInfoService.update(params);

            if (expectedError != null)
                fail("This should not have been come!!!");

            Map<String, List<String>> inputParam = getBasicParams(params);

            PersonalInfo info = getParams(inputParam);

            MaritalStatus status = info.getMaritalStatus();
            assertNotNull("MaritalStatus is null.", status);
            assertEquals("Updated MaritalStatus value is not equal.",
                    MaritalStatus.SINGLE, status);

            String spouseName = info.getSpouseName();
            assertNotNull("SpouseName is null.", spouseName);
            assertEquals("Updated SpouseName value is not equal.", "Bob",
                    spouseName);

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertNotNull("MxOSException is null", e);
        }
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsPersonalInfoService = (IContactsPersonalInfoService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsPersonalInfoService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsPersonalInfoService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    @Test
    public void testContactsPersonalInfo() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsPersonalInfo");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("maritalStatus", new ArrayList<String>());
        inputParam.get("maritalStatus").add(MaritalStatus.SINGLE.toString());

        inputParam.put("spouseName", new ArrayList<String>());
        inputParam.get("spouseName").add("Bob");

        setParams(inputParam);
    }

    @Test
    public void testContactsPersonalInfoEmptyParams() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoEmptyParams");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("spouseName", new ArrayList<String>());
        inputParam.get("spouseName").add("");

        setEmptyParams(inputParam);
    }

    @Test
    public void testContactsPersonalInfoNegTest() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoNegTest");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("maritalStatus", new ArrayList<String>());
        inputParam.get("maritalStatus").add("##########");

        setParams(params, new AddressBookException(
                AddressBookError.ABS_INVALID_SPOUSE_NAME.toString()));

    }

}
