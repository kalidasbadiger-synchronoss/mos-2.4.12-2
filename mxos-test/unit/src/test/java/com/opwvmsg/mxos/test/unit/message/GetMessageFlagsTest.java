package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetMessageFlagsTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String MSG_ID_KEY = MessageProperty.messageId.name();
    private static final String EMAIL = "zz123457@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static final String FOLDER = "INBOX";
    private static final String MSG_ID = "1062d0c4-512c-11e2-a0d7-108a47a179ce";
    private static IMetadataService service;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetMessageFlagsTest.setUpBeforeClass...");
        service = (IMetadataService) ContextUtils.loadContext().getService(
                ServiceEnum.MetadataService.name());
        //long mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        //assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.put(FOLDER_KEY, new ArrayList<String>());
        params.put(MSG_ID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetMessageFlagsTest.setUp...");
        assertNotNull("Service object is null.", service);

        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());

        assertNotNull("Input Param:folderName is null.", params.get(FOLDER_KEY));
        params.get(FOLDER_KEY).add(FOLDER);
        assertTrue("Input Param:folderName is empty.", !params.get(FOLDER_KEY)
                .isEmpty());

        assertNotNull("Input Param:messageId is null.", params.get(MSG_ID_KEY));
        params.get(MSG_ID_KEY).add(MSG_ID);
        assertTrue("Input Param:messageId is empty.", !params.get(MSG_ID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetMessageFlagsTest.tearDown...");
        params.get(EMAIL_KEY).clear();
        params.get(FOLDER_KEY).clear();
        params.get(MSG_ID_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetMessageFlagsTest.tearDownAfterClass...");
        //MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        service = null;
    }

    private static Flags getMessageFlagsParams(
            Map<String, List<String>> params) {
        return getMessageFlagsParams(params, null);
    }

    private static Flags getMessageFlagsParams(
            Map<String, List<String>> params, String expectedError) {
        Flags flags = null;
        try {
            // flags = 
            service.read(params);
            // TODO - need to map flags
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("Flags object is null.", flags);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (null == params.get(EMAIL_KEY)) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
            if (null == params.get(FOLDER_KEY)) {
                params.put(FOLDER_KEY, new ArrayList<String>());
            }
            if (null == params.get(MSG_ID_KEY)) {
                params.put(MSG_ID_KEY, new ArrayList<String>());
            }
        }
        return flags;
    }

    @Test
    public void testGetFlagsWithNullEmail() {
        System.out
                .println("GetMessageFlagsTest.testGetFlagsWithNullEmail...");
        params.get(EMAIL_KEY).clear();
        getMessageFlagsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetFlagsWithNullFolderName() {
        System.out
                .println("GetMessageFlagsTest.testGetFlagsWithNullFolderName...");
        params.get(FOLDER_KEY).clear();
        getMessageFlagsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetFlagsWithNullMessageId() {
        System.out
                .println("GetMessageFlagsTest.testGetFlagsWithNullMessageId...");
        params.get(MSG_ID_KEY).clear();
        getMessageFlagsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testFlagRecent() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagRecent...");
        Boolean value = getMessageFlagsParams(params).getFlagRecent();
        assertNotNull("flagRecent is null.", value);
    }

    @Test
    public void testFlagSeen() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagSeen...");
        Boolean value = getMessageFlagsParams(params).getFlagSeen();
        assertNotNull("flagSeen is null.", value);
    }

    @Test
    public void testFlagUnread() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagUnread...");
        Boolean value = getMessageFlagsParams(params).getFlagUnread();
        assertNotNull("flagUnread is null.", value);
    }

    @Test
    public void testFlagAns() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagRecent...");
        Boolean value = getMessageFlagsParams(params).getFlagAns();
        assertNotNull("flagAns is null.", value);
    }

    @Test
    public void testFlagFlagged() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagFlagged...");
        Boolean value = getMessageFlagsParams(params).getFlagFlagged();
        assertNotNull("flagFlagged is null.", value);
    }

    @Test
    public void testFlagDel() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagRecent...");
        Boolean value = getMessageFlagsParams(params).getFlagDel();
        assertNotNull("flagDel is null.", value);
    }

    @Test
    public void testFlagDraft() throws Exception {
        System.out.println("GetMessageFlagsTest.testFlagDraft...");
        Boolean value = getMessageFlagsParams(params).getFlagDraft();
        assertNotNull("flagDraft is null.", value);
    }

}
