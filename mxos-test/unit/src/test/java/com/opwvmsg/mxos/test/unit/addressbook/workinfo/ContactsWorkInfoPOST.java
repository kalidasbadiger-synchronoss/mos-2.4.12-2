package com.opwvmsg.mxos.test.unit.addressbook.workinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.WorkInfo;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsWorkInfoPOST {

    private static final String TEST_NAME = "ContactWorkInfoPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsWorkInfoService contactsWorkInfoService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;

    }

    private static WorkInfo getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static WorkInfo getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        WorkInfo info = null;
        try {
            info = contactsWorkInfoService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Work Info object is null.", info);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return info;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            MxOSException expectedError) {

        try {
            contactsWorkInfoService.update(params);

            Map<String, List<String>> inputParam = getBasicParams(params);

            WorkInfo info = getParams(inputParam);

            assertNotNull("Company Name Is null.", info.getCompanyName());
            assertNotNull("Department Is null.", info.getDepartment());
            assertNotNull("Title Is null.", info.getTitle());
            assertNotNull("WebPage Is null.", info.getWebPage());
            assertNotNull("Manager Is null.", info.getManager());
            assertNotNull("Assistant Is null.", info.getAssistant());
            assertNotNull("AssistantPhone Is null.", info.getAssistantPhone());
            assertNotNull("EmployeeId Is null.", info.getEmployeeId());

            assertEquals("Updated Company Name value is not equal.",
                    "TESTCMPNY", info.getCompanyName());
            assertEquals("Updated Department value is not equal.", "TESTDEPT",
                    info.getDepartment());
            assertEquals("Updated Title value is not equal.", "TESTTITLE",
                    info.getTitle());
            assertEquals("Updated WebPage value is not equal.", "TESTURL",
                    info.getWebPage());
            assertEquals("Updated Manager value is not equal.", "TESTMANAGER",
                    info.getManager());
            assertEquals("Updated Assistant value is not equal.",
                    "TESTASSISTANTNAME", info.getAssistant());
            assertEquals("Updated AssistantPhone value is not equal.",
                    "TESTPHONE", info.getAssistantPhone());
            assertEquals("Updated EmployeeId value is not equal.",
                    "TESTEMPLYID", info.getEmployeeId());

        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
                System.out.println(e.getCode());
            }
        }

    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsWorkInfoService = (IContactsWorkInfoService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsWorkInfoService.name());
        login(USERID, PASSWORD);
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsWorkInfoService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    @Test
    public void testContactsWorkInfo() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsWorkInfo");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        params.put("companyName", new ArrayList<String>());
        params.get("companyName").add("TESTCMPNY");

        params.put("department", new ArrayList<String>());
        params.get("department").add("TESTDEPT");

        params.put("manager", new ArrayList<String>());
        params.get("manager").add("TESTMANAGER");

        params.put("assistant", new ArrayList<String>());
        params.get("assistant").add("TESTASSISTANTNAME");

        params.put("assistantPhone", new ArrayList<String>());
        params.get("assistantPhone").add("TESTPHONE");

        params.put("title", new ArrayList<String>());
        params.get("title").add("TESTTITLE");

        params.put("employeeId", new ArrayList<String>());
        params.get("employeeId").add("TESTEMPLYID");

        params.put("webPage", new ArrayList<String>());
        params.get("webPage").add("TESTURL");

        setParams(params);

    }

    @Test
    public void testContactsWorkInfoNegTest() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsWorkInfoNegTest");
        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("companyName", new ArrayList<String>());
        inputParam
                .get("companyName")
                .add("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");

        setParams(params, new InvalidRequestException(
                AddressBookError.ABS_INVALID_COMPANY.toString()));

    }
}
