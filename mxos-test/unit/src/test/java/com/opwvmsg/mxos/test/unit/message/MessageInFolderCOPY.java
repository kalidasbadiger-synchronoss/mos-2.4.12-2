package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 *
 * @author mxos-dev
 * 
 */
public class MessageInFolderCOPY {

    private static final String TEST_NAME = "MessageInFolderCOPY";
    private static final String ARROW_SEP = " --> ";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "message.rest444@openwave.com";
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String PASSWORD = "test5";
    private static final String COS_ID_KEY = MailboxProperty.cosId.name();
    private static final String COS_VALUE = "default";

    private static final String RECEIVED_FROM_KEY = MxOSPOJOs.receivedFrom
            .name();
    private static final String RECEIVED_FROM_VALUE = "message.rest555@openwave.com";
    private static final String MESSAGE_KEY = MxOSPOJOs.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDER_KEY = FolderProperty.folderName.name();
    private static final String SRC_FOLDER_KEY = FolderProperty.srcFolderName
            .name();
    private static final String TO_FOLDER_KEY = FolderProperty.toFolderName
            .name();
    private static final String MESSAGE_ID_KEY = MessageProperty.messageId.name();
    private static final String FOLDER_INBOX = "INBOX";
    private static final String FOLDER_TO = "SentMail";
    private static final String FOLDER_TO_TRASH = "Trash";
    private static final String OPTION_FOLDER_IS_HINT_KEY = MessageProperty.optionFolderIsHint
            .name();
    private static final String OPTION_FOLDER_IS_HINT_VALUE = "true";
    private static final String OPTION_MULTIPLE_OK_KEY = MessageProperty.optionMultipleOk
            .name();
    private static final String OPTION_MULTIPLE_OK_VALUE = "true";
    private static final String MESSAGE_STORE_HOST_KEY = MailboxProperty.messageStoreHost.name();
    private static final String MAILBOX_ID_KEY = MailboxProperty.mailboxId.name();
    private static final String FOLDERNAME_VALUE_11_22 = "1111111111111111111111111111111111111111111111%2f22222222222222222222222222222222222222222222222" +
    "2222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222" +
    "222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222";
    private static final String FOLDERNAME_VALUE_10 = "a%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2fta%2fs%2fa%2ft";

    
    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IMetadataService metadataService;
    private static IMailboxService mailboxService;
    private static IInternalInfoService internalInfoService;
    private static IMailboxBaseService mailboxBaseService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        createMailBox(EMAIL, PASSWORD);
        createMailBox(RECEIVED_FROM_VALUE, PASSWORD);
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        internalInfoService = (IInternalInfoService) context
                .getService(ServiceEnum.InternalInfoService.name());
        mailboxBaseService = (IMailboxBaseService) context
                .getService(ServiceEnum.MailboxBaseService.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("messageService object is null.", messageService);
        assertNotNull("MailBoxService object is null.", mailboxService);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteMailBox(EMAIL);
        deleteMailBox(RECEIVED_FROM_VALUE);
        params.clear();
        params = null;
        messageService = null;
        mailboxService = null;
    }

    private static void createMailBox(String email, String password) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        inputParams.get(PASSWORD_KEY).add(password);
        inputParams.put(COS_ID_KEY, new ArrayList<String>());
        inputParams.get(COS_ID_KEY).add(COS_VALUE);
        
        String mailBoxId = null;
        try {
            mailBoxId = mailboxService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Unable to create Mailbox...", mailBoxId != null);
    }

    private static void deleteMailBox(String email) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(email);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static String createMessage() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, RECEIVED_FROM_KEY, RECEIVED_FROM_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            return messageService.create(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
        return null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testCopyMessageInFolderNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderNoEmail");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyMessageInFolderNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderNoFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = ErrorCode.GEN_BAD_REQUEST.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testCopyMessageInFolderNonExistingFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, "asdfadsf");
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_NOT_FOUND.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testCopyMessageInFolderInvalidFolderName() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDERNAME_VALUE_11_22);
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_NOT_FOUND.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testCopyMessageInFolderInvalidFolderDepth() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderNonExistingFolder");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDERNAME_VALUE_10);
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = FolderError.FLD_INVALID_TO_FOLDERNAME.name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testCopyMessageSourceDestFolderSame() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageSourceDestFolderSame");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            String msgId = createMessage();
            
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, "' '");
            addParams(inputParams, TO_FOLDER_KEY, "' '");
            addParams(inputParams, MESSAGE_ID_KEY, msgId);
            
            messageService.copy(inputParams);
        } catch (MxOSException e) {
            String expectedError = MessageError
                .MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME
                    .name();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyAllMessageInFolderSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copyAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            assertFalse("Message not copied", messages.size() == 0);
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyMessageInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            String messageId = null;
            for (String msg : messages.keySet()) {
                messageId = msg;
            }

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, MESSAGE_ID_KEY, messageId);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copy(inputParams);
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messages = metadataService
                    .list(inputParams);
        } catch (Exception e) {
            fail();
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyMultipleMessagesInFolderWithMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMultipleMessagesInFolderWithMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            inputParams.put(MESSAGE_ID_KEY, new ArrayList<String>());
            for (String msg : messages.keySet()) {
                inputParams.get(MESSAGE_ID_KEY).add(msg);
            }
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copy(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messagesInFromFolder = metadataService
                    .list(inputParams);
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            Map<String, Metadata> messagesInToFolder = metadataService
                    .list(inputParams);

            for (String msg : messagesInFromFolder.keySet()) {
                String msgIdInFromFolder = msg;
                if (msgIdInFromFolder != null
                        && !messagesInToFolder.containsKey(msgIdInFromFolder)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyMessageWithMultipleSameMessageIdSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageWithMultipleSameMessageIdSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copyAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_TO);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copyAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);

            Map<String, Metadata> messages = metadataService
                    .list(inputParams);
            System.out.println("size"+messages.size());
            if (messages.size() < 0) {
                fail();
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    
    @Test
    public void testCopyMessageWithHostandMailboxId_Success() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageWithHostandMailboxId_Success");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            InternalInfo internalInfo = internalInfoService.read(inputParams);
            String messageStoreHost = internalInfo.getMessageStoreHosts()
                    .get(0);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            Base base = mailboxBaseService.read(inputParams);
            String mailboxId = base.getMailboxId();
            String mId = "" + mailboxId;

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, MESSAGE_ID_KEY, uid);
            addParams(inputParams, MAILBOX_ID_KEY, mId);
            addParams(inputParams, MESSAGE_STORE_HOST_KEY, messageStoreHost);
            messageService.copy(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, MESSAGE_ID_KEY, uid);

            Map<String, Metadata> messages = metadataService
                    .list(inputParams);

            if (messages.size() != 1)
                fail();
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testCopyMessageWithHostandMailboxId_InvalidHost() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageWithHostandMailboxId_InvalidHost");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            InternalInfo internalInfo = internalInfoService.read(inputParams);
            String messageStoreHost = internalInfo.getMessageStoreHosts()
                    .get(0);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            Base base = mailboxBaseService.read(inputParams);
            String mailboxId = base.getMailboxId();
            String mId = "" + mailboxId;

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, MESSAGE_ID_KEY, uid);
            addParams(inputParams, MAILBOX_ID_KEY, mId);
            addParams(inputParams, MESSAGE_STORE_HOST_KEY, "junk");
            messageService.copy(inputParams);
            fail();
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), e.getCode());
        }
    }

    @Test
    public void testCopyMessageWithHostandMailboxId_InvalidMsgId() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMessageWithHostandMailboxId_InvalidMsgId");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            String uid = createMessage();

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            InternalInfo internalInfo = internalInfoService.read(inputParams);
            String messageStoreHost = internalInfo.getMessageStoreHosts()
                    .get(0);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            Base base = mailboxBaseService.read(inputParams);
            String mailboxId = base.getMailboxId();
            String mId = "" + mailboxId;

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO_TRASH);
            addParams(inputParams, MESSAGE_ID_KEY, uid);
            addParams(inputParams, MAILBOX_ID_KEY, "123456");
            addParams(inputParams, MESSAGE_STORE_HOST_KEY, messageStoreHost);
            messageService.copy(inputParams);
            fail();
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_NOT_FOUND.name(), e.getCode());
        }
    }
    
    @Test
    public void testCopyMultipleMessages_Success() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCopyMultipleMessages_Success");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            messageService.deleteAll(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            createMessage();
            createMessage();
            createMessage();
            Map<String, Metadata> messages = metadataService
                    .list(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, SRC_FOLDER_KEY, FOLDER_INBOX);
            addParams(inputParams, TO_FOLDER_KEY, FOLDER_TO);
            inputParams.put(MESSAGE_ID_KEY, new ArrayList<String>());
            for (String msg : messages.keySet()) {
                inputParams.get(MESSAGE_ID_KEY).add(msg);
            }
            addParams(inputParams, OPTION_FOLDER_IS_HINT_KEY,
                    OPTION_FOLDER_IS_HINT_VALUE);
            addParams(inputParams, OPTION_MULTIPLE_OK_KEY,
                    OPTION_MULTIPLE_OK_VALUE);
            messageService.copyMulti(inputParams);

            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_INBOX);
            Map<String, Metadata> messagesInFromFolder = metadataService
                    .list(inputParams);
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDER_KEY, FOLDER_TO);
            Map<String, Metadata> messagesInToFolder = metadataService
                    .list(inputParams);

            for (String msg : messagesInFromFolder.keySet()) {
                String msgIdInFromFolder = msg;
                if (msgIdInFromFolder != null
                        && !messagesInToFolder.containsKey(msgIdInFromFolder)) {
                    fail();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
}
