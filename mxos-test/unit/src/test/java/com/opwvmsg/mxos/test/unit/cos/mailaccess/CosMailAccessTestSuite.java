package com.opwvmsg.mxos.test.unit.cos.mailaccess;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    CosMailAccessTest.class
})
public class CosMailAccessTestSuite {
}
