package com.opwvmsg.mxos.test.unit.mailbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.DomainHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class MailboxStatusTest {
    private static final String COSID = "cos_2.0_ut200";
    private static final String DOMAIN_NAME = "domain2ut200.com";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100005@" + DOMAIN_NAME;
    private static final String PWD = "test1";
    private static IMxOSContext context;
    private static IMailboxBaseService mailboxBaseService;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailboxStatusTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxBaseService = (IMailboxBaseService) context
                .getService(ServiceEnum.MailboxBaseService.name());
        assertNotNull("mailboxBaseService object is null.", mailboxBaseService);
        CosHelper.createCos(COSID);
        DomainHelper.createDomain(DOMAIN_NAME);
        MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        // assertTrue("Mailbox NOT Created", (MAILBOX_ID > 0));
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailboxStatusTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailboxStatusTest.tearDown...");
        inputParams.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailboxStatusTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        try {
            MailboxHelper.deleteMailbox(EMAIL);
            DomainHelper.deleteDomain(DOMAIN_NAME);
            CosHelper.deleteCos(COSID);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
    }

    @Test
    public void testMailboxStatusUpdate2() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxStatusUpdate2..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("locked");
        inputParams.put(MailboxProperty.msisdn.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdn.name()).add("+91987654321");
        try {
            mailboxBaseService.update(inputParams);
            assertTrue("Success ", true);
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }

    @Test
    public void testMailboxStatusUpdateNeg1() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxStatusUpdateNeg1..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add(null);
        inputParams.put(MailboxProperty.maxNumAliases.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAliases.name()).add("10");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (Exception e) {
            assertTrue("Exception Happened ", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testMailboxStatusUpdateNeg2() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxStatusUpdateNeg2..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("Junk");
        inputParams.put(MailboxProperty.maxNumAliases.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAliases.name()).add("10");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (Exception e) {
            assertTrue("Exception Happened ", true);
            e.printStackTrace();
        }
    }

    // ---------------------Read Status -----------------------------//

    @Test
    public void testMailboxStatusRead() throws Exception {
        System.out.println("MailboxStatusTest...testMailboxStatusRead");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            Base base = mailboxBaseService.read(inputParams);
            assertNotNull(base.getStatus());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMailboxStatusReadNeg() throws Exception {
        System.out.println("MailboxStatusTest...testMailboxStatusReadNeg..");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            Base base = mailboxBaseService.read(inputParams);
            assertNotNull(base.getStatus());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMailboxCustomFieldRead() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxCustomFieldRead..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            Base base = mailboxBaseService.read(inputParams);
            assertNotNull(base.getCustomFields());
            System.out.println(base.getCustomFields());
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMailboxStatusUpdate1() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxStatusUpdate1..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("active");
        try {
            mailboxBaseService.update(inputParams);
            assertTrue("Success ", true);
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }

    @Test
    public void testMailboxCustomStatusUpdate1() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxCustomStatusUpdate1..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("proxy");
        try {
            mailboxBaseService.update(inputParams);
            assertTrue("Success ", true);
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }

    @Test
    public void testMailboxCustomStatusUpdate2() throws Exception {
        System.out
                .println("MailboxStatusTest...testMailboxCustomStatusUpdate2..started");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("maintenance");
        try {
            mailboxBaseService.update(inputParams);
            assertTrue("Success ", true);
        } catch (Exception e) {
            fail();
            e.printStackTrace();
        }
    }

}
