package com.opwvmsg.mxos.test.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;

public class CosHelper {

    private static final String COS_ID_KEY = MailboxProperty.cosId.name();

    private static ICosService cos;

    static {
        try {
            cos = (ICosService) ContextUtils.loadContext().getService(
                    ServiceEnum.CosService.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean createCos(String cosId) {
        boolean flag = true;
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(COS_ID_KEY, new ArrayList<String>());
        params.get(COS_ID_KEY).add(cosId);
        try {
            cos.create(params);
            flag = true;
        } catch (Exception e) {
            flag = false;
            e.printStackTrace();
        }
        return flag;
    }

    public static void deleteCos(String cosId) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(COS_ID_KEY, new ArrayList<String>());
        params.get(COS_ID_KEY).add(cosId);
        try {
            cos.delete(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
}
