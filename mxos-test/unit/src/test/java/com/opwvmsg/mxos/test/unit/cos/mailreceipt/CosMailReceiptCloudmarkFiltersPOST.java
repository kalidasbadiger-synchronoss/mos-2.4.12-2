package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.CloudmarkActionType;
import com.opwvmsg.mxos.data.enums.MxosEnums.SpamPolicy;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailReceiptCloudmarkFiltersPOST {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID = "default_cos";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "CosMailReceiptCloudmarkFiltersPOST";
    private static final String ARROW_SEP = " --> ";
    private static ICosMailReceiptCloudmarkFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (ICosMailReceiptCloudmarkFiltersService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosMailReceiptCloudmarkFiltersService.name());
        CosHelper.createCos(COSID);
        addToParams(updateParams, COSID_KEY, COSID);
        addToParams(getParams, COSID_KEY, COSID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        CosHelper.createCos(COSID);
        assertNotNull("Input Param:cosId is null.", updateParams.get(COSID_KEY));
        assertTrue("Input Param:cosId is empty.", !updateParams.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        CosHelper.deleteCos(COSID);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static CloudmarkFilters getParams() {
        CloudmarkFilters object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                addToParams(getParams, COSID_KEY, COSID);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                addToParams(updateParams, COSID_KEY, COSID);
            }
        }
    }

    @Test
    public void testUpdateWithoutCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutCOS");
        // Clear the params.
        updateParams.remove(COSID_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, COSID_KEY, COSID);
    }

    @Test
    public void testUpdateWithNullCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullCOS");
        updateParams.remove(COSID_KEY);
        addToParams(updateParams, COSID_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(COSID_KEY);
        addToParams(updateParams, COSID_KEY, COSID);
    }

    @Test
    public void testUpdateWithEmptyCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyCOS");
        updateParams.remove(COSID_KEY);
        addToParams(updateParams, COSID_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(COSID_KEY);
        addToParams(updateParams, COSID_KEY, COSID);
    }

   
    @Test
    public void testSpamfilterEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledNullParam");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamfilterEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledEmptyParam");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamfilterEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledInvalidParam");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, "asdfasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamfilterEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledSplCharsInParam");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamfilterEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledSuccess");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSpamfilterEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamfilterEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamfilterEnabledSuccess1");
        String key = MailboxProperty.spamfilterEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSpamfilterEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    //    spamPolicy
    @Test
    public void testSpamPolicyNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicyNullParam");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamPolicyEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicyEmptyParam");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamPolicyInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicyInvalidParam");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, "asdasdasd");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_POLICY.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamPolicySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicySplCharsInParam");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_POLICY.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamPolicySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicySuccess");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, SpamPolicy.MILD.name());
        updateParams(updateParams);
        SpamPolicy value = getParams().getSpamPolicy();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, SpamPolicy.MILD);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamPolicySuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicySuccess1");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, SpamPolicy.MODERATE.name());
        updateParams(updateParams);
        SpamPolicy value = getParams().getSpamPolicy();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, SpamPolicy.MODERATE);
        updateParams.remove(key);
    }

    @Test
    public void testSpamPolicySuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamPolicySuccess2");
        String key = MailboxProperty.spamPolicy.name();
        addToParams(updateParams, key, SpamPolicy.AGGRESSIVE.name());
        updateParams(updateParams);
        SpamPolicy value = getParams().getSpamPolicy();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, SpamPolicy.AGGRESSIVE);
        updateParams.remove(key);
    }

    //    spamAction
    @Test
    public void testSpamActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionNullParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionEmptyParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSpamActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionInvalidParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSplCharsInParam");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SPAM_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSuccess");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CloudmarkActionType.ACCEPT.name());
        updateParams(updateParams);
        String value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }
    
    
    @Test
    public void testSpamActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSuccess1");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CloudmarkActionType.DELETE.name());
        updateParams(updateParams);
        String value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }
    
    @Test
    public void testSpamActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSuccess2");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CloudmarkActionType.QUARANTINE.name());
        String key1= MailboxProperty.folderName.name();
        addToParams(updateParams, key1, "DirtyMails");
        updateParams(updateParams);
        String value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testSpamActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSuccess3");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CloudmarkActionType.TAG.name());
        String key1= MailboxProperty.tagText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testSpamActionSuccess4() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSpamActionSuccess4");
        String key = MailboxProperty.spamAction.name();
        addToParams(updateParams, key, CloudmarkActionType.HEADER.name());
        String key1= MailboxProperty.headerText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getSpamAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    //    cleanAction
    @Test
    public void testCleanActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionNullParam");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testCleanActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionEmptyParam");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testCleanActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionInvalidParam");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_CLEAN_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testCleanActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSplCharsInParam");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_CLEAN_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testCleanActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSuccess");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, CloudmarkActionType.ACCEPT.name());
        updateParams(updateParams);
        String value = getParams().getCleanAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }
    
    @Test
    public void testCleanActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSuccess1");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, CloudmarkActionType.DELETE.name());
        updateParams(updateParams);
        String value = getParams().getCleanAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }
    
    @Test
    public void testCleanActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSuccess2");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, CloudmarkActionType.HEADER.name());
        String key1= MailboxProperty.headerText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getCleanAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testCleanActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSuccess3");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, CloudmarkActionType.QUARANTINE.name());
        String key1= MailboxProperty.folderName.name();
        addToParams(updateParams, key1, "DirtyMails");
        updateParams(updateParams);
        String value = getParams().getCleanAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testCleanActionSuccess4() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testCleanActionSuccess4");
        String key = MailboxProperty.cleanAction.name();
        addToParams(updateParams, key, CloudmarkActionType.TAG.name());
        String key1= MailboxProperty.tagText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getCleanAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    //  suspectAction
    @Test
    public void testSuspectActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionNullParam");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }
    
    @Test
    public void testSuspectActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionEmptyParam");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSuspectActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionInvalidParam");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, "asdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SUSPECT_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSuspectActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionSplCharsInParam");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_CLOUDMARK_FILTERS_SUSPECT_ACTION.name());
        updateParams.remove(key);
    }
    
    @Test
    public void testSuspectActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionSuccess");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, CloudmarkActionType.ACCEPT.name());
        updateParams(updateParams);
        String value = getParams().getSuspectAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }

    @Test
    public void testSuspectActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionSuccess1");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, CloudmarkActionType.DELETE.name());
        updateParams(updateParams);
        String value = getParams().getSuspectAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
    }
    
    @Test
    public void testSuspectActionSuccess2() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionSuccess2");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, CloudmarkActionType.QUARANTINE.name());
        String key1= MailboxProperty.folderName.name();
        addToParams(updateParams, key1, "DirtyMails");
        updateParams(updateParams);
        String value = getParams().getSuspectAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testSuspectActionSuccess3() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSuspectActionSuccess3");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, CloudmarkActionType.TAG.name());
        String key1= MailboxProperty.tagText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getSuspectAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }

    @Test
    public void testSuspectActionSuccess4() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSuspectActionSuccess4");
        String key = MailboxProperty.suspectAction.name();
        addToParams(updateParams, key, CloudmarkActionType.HEADER.name());
        String key1= MailboxProperty.headerText.name();
        addToParams(updateParams, key1, "SPAM");
        updateParams(updateParams);
        String value = getParams().getSuspectAction();
        assertNotNull("Is null.", value);
        updateParams.remove(key);
        updateParams.remove(key1);
    }
}

