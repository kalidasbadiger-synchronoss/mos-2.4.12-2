package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ILogToExternalEntityService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MaaLogToExternalEntityTest {

    private static final String APICALL_VALUE = "Ragiluh";
    private static final String TIMESTAMP_VALUE = "2012-12-12-12:12:12:122";
    private static final String PAYLOAD_VALUE = "payload";
    private static final String RESULTCODE_VALUE = "resultCode";
    private static final String CRUD_VALUE = "c";

    private static final String KEY_VALUE = "test_success@openwave.com";
    private static final String KEY_VALUE_ERROR = "test_error@openwave.com";
    private static final String KEY_VALUE_INVALID_XML = "test_invalidXML@openwave.com";
    private static final String KEY_VALUE_RESULT_CODE_NOT_PRESENT = "test_noresult@openwave.com";
    
    private static ILogToExternalEntityService logToExternalEntityService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MaaLogToExternalEntityTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null ||
                System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        logToExternalEntityService = (ILogToExternalEntityService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.LogToExternalEntityService.name());
        assertNotNull("MailboxService object is null.", logToExternalEntityService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MaaLogToExternalEntityTest.tearDownAfterClass...");
        params.clear();
        params = null;
        logToExternalEntityService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    
    @Test
    public void MaaLogToExternalEntity_apicall_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.apicall_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_timeStamp_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.timeStamp_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    @Test
    public void MaaLogToExternalEntity_key_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.key_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    @Test
    public void MaaLogToExternalEntity_payload_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.payload_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_resultCode_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.resultCode_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_crud_NotPresent() {
        System.out.println("MaaLogToExternalEntityTest.crud_NotPresent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_key_InvalidFormat() {
        System.out.println("MaaLogToExternalEntityTest.key_InvalidFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), "Openwave");
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_INVALID_KEY.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_timestamp_InvalidFormat() {
        System.out.println("MaaLogToExternalEntityTest.timestamp_InvalidFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), "Openwave");
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_INVALID_TIMESTAMP.name(), e.getCode());
        }
    }
    
    @Test
    public void MaaLogToExternalEntity_crud_InvalidFormat() {
        System.out.println("MaaLogToExternalEntityTest.crud_InvalidFormat...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), "Openwave");
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_INVALID_CRUD.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_Success() {
        System.out.println("MaaLogToExternalEntityTest.Success...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Happened", true);
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_ErrorFromMaa() {
        System.out.println("MaaLogToExternalEntityTest.ErrorFromMaa...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE_ERROR);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(), e.getCode());
        }
    }


    @Test
    public void MaaLogToExternalEntity_InvalidXMLFromMaa() {
        System.out.println("MaaLogToExternalEntityTest.InvalidXMLFromMaa...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE_INVALID_XML);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(), e.getCode());
        }
    }
    
    
    @Test
    public void MaaLogToExternalEntity_ResultCodeNotPresentinMAAResponse() {
        System.out.println("MaaLogToExternalEntityTest.ResultCodeNotPresentinMAAResponse...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            
            addParams(inputParams, SmsProperty.apicall.name(), APICALL_VALUE);
            addParams(inputParams, SmsProperty.timestamp.name(), TIMESTAMP_VALUE);
            addParams(inputParams, SmsProperty.key.name(), KEY_VALUE_RESULT_CODE_NOT_PRESENT);
            addParams(inputParams, SmsProperty.payload.name(), PAYLOAD_VALUE);
            addParams(inputParams, SmsProperty.resultcode.name(), RESULTCODE_VALUE);
            addParams(inputParams, SmsProperty.crud.name(), CRUD_VALUE);
            logToExternalEntityService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(), e.getCode());
        }
    }
    
    
}
