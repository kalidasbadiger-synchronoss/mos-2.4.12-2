package com.opwvmsg.mxos.test.unit.cos.credentials;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test Suite for test cases related to Credentials for Cos service.
 * 
 * @author mxos-dev
 *
 */

@RunWith(Suite.class)
// Include all the test case classes related to COS Credentials
@SuiteClasses({
    GetCosCredentialsTest.class,
    UpdateCosCredentialsTest.class
})
public class CosCredentialsSuite {
}
