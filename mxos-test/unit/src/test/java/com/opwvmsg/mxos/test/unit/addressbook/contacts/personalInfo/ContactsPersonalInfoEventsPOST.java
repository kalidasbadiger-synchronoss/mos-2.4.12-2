package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoEventsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsPersonalInfoEventsPOST {

    private static final String TEST_NAME = "ContactsPersonalInfoEventsPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsPersonalInfoEventsService contactsPersonalInfoEventsService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Map<String, List<String>> getBasicParams(
            Map<String, List<String>> params) {

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        return params;
    }

    private static List<Event> getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static List<Event> getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        List<Event> eventList = null;
        try {
            eventList = contactsPersonalInfoEventsService.readAll(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                for (Event e : eventList) {
                    assertNotNull("Type is null", e.getType());
                    assertNotNull("Date is null", e.getDate());
                }
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return eventList;
    }

    private static Event getEmptyParams(Map<String, List<String>> params) {
        Event event = null;
        try {
            params.put(AddressBookProperty.type.name(), new ArrayList<String>());
            params.get(AddressBookProperty.type.name()).add("Birthday");

            event = contactsPersonalInfoEventsService.read(params);

            assertNull("Event Is not null", event);
        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertNotNull("MxOSError is not null", e);

        }
        return event;
    }

    private static void setEmptyParams(Map<String, List<String>> params) {

        try {
            contactsPersonalInfoEventsService.update(params);

            params.clear();
            Map<String, List<String>> inputParam = getBasicParams(params);

            Event event = getEmptyParams(inputParam);

            assertNull("Event Is not null", event);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is  null", e);

        }
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsPersonalInfoEventsService.update(params);

            if (expectedError != null)
                fail("This should not have been come!!!");

            params.clear();
            Map<String, List<String>> inputParam = getBasicParams(params);

            List<Event> eventList = getParams(inputParam);

            assertNotNull("Event is null.", eventList.get(0));
            assertNotNull("Event Type is null.", eventList.get(0).getType());
            assertNotNull("Event Date is null.", eventList.get(0).getDate());
            assertEquals("Updated Type value is not equal.",
                    Event.Type.BIRTHDAY, eventList.get(0).getType());
            assertEquals("Updated Date value is not equal.",
                    "1980-06-11T00:00:00Z", eventList.get(0).getDate());

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertNotNull("MxOSException is null", e);
        }
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsPersonalInfoEventsService = (IContactsPersonalInfoEventsService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsPersonalInfoEventsService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsPersonalInfoEventsService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    @Test
    public void testContactsPersonalInfoEvent() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoEvent");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("type", new ArrayList<String>());
        inputParam.get("type").add("Birthday");

        inputParam.put("date", new ArrayList<String>());
        inputParam.get("date").add("1980-06-11T00:00:00Z");

        setParams(inputParam);

    }

    @Test
    public void testContactsPersonalInfoEventEmptyParams() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoEventEmptyParams");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("type", new ArrayList<String>());
        inputParam.get("type").add("Birthday");

        inputParam.put("date", new ArrayList<String>());
        inputParam.get("date").add("");

        setEmptyParams(inputParam);

    }

    @Test
    public void testContactsPersonalInfoEventInvalidTypeParams()
            throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoEventInvalidTypeParams");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("type", new ArrayList<String>());
        inputParam.get("type").add("HappyBirthday");

        setParams(
                inputParam,
                new AddressBookException(
                        AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_TYPE
                                .toString()));

    }

    @Test
    public void testContactsPersonalInfoEventInvalidDateParams()
            throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsPersonalInfoEventInvalidTypeParams");

        Map<String, List<String>> inputParam = getBasicParams(params);

        inputParam.put("type", new ArrayList<String>());
        inputParam.get("type").add("Birthday");

        inputParam.put("date", new ArrayList<String>());
        inputParam.get("date").add("11-11-11");

        setParams(
                inputParam,
                new AddressBookException(
                        AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_DATE
                                .toString()));

    }

}
