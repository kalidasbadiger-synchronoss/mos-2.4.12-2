package com.opwvmsg.mxos.test.unit.externalAccount;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    ExternalAccountsTest.class,
    MailAccountsTest.class
})

public class ExternalAccountSuite {

}

