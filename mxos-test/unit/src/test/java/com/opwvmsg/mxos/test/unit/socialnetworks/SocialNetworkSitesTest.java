package com.opwvmsg.mxos.test.unit.socialnetworks;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworkSiteService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class SocialNetworkSitesTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static ISocialNetworkSiteService snss = null;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("SocialNetworkSitesTest.setUpBeforeClass...");
        snss = (ISocialNetworkSiteService) ContextUtils.loadContext()
                .getService(ServiceEnum.SocialNetworkSiteService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("SocialNetworkSitesTest.setUp...");
        assertNotNull("SocialNetworkSiteService object is null.", snss);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("SocialNetworkSitesTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("SocialNetworkSitesTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        params.clear();
        params = null;
        snss = null;
    }

    private static List<SocialNetworkSite> getSocialNetworkSites() {
        List<SocialNetworkSite> sites = null;
        try {
            sites = snss.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("sites object is null.", sites);
        return sites;
    }

    private static void createSocialNetworkSites(
            Map<String, List<String>> params) {
        createSocialNetworkSites(params, null);
    }

    private static void createSocialNetworkSites(
            Map<String, List<String>> params, String expectedError) {
        try {
            snss.create(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void updateSocialNetworkSites(
            Map<String, List<String>> params) {
        updateSocialNetworkSites(params, null);
    }

    private static void updateSocialNetworkSites(
            Map<String, List<String>> params, String expectedError) {
        try {
            snss.update(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void deleteSocialNetworkSites(
            Map<String, List<String>> params) {
        deleteSocialNetworkSites(params, null);
    }

    private static void deleteSocialNetworkSites(
            Map<String, List<String>> params, String expectedError) {
        try {
            snss.delete(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testCreateSocialNetworkSitesWithoutEmail() throws Exception {
        params.clear();
        System.out
                .println("SocialNetworkSitesTest.testCreateSocialNetworkSitesWithoutEmail...");
        createSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testCreateSocialNetworkSitesWithEmptyEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add("");
        System.out
                .println("SocialNetworkSitesTest.testCreateSocialNetworkSitesWithEmptyEmail...");
        createSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testCreateSocialNetworkSitesWithNullEmail() throws Exception {
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(null);
        System.out
                .println("SocialNetworkSitesTest.testCreateSocialNetworkSitesWithNullEmail...");
        createSocialNetworkSites(params, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testCreateSocialNetworkSitesWithoutAnyParam() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testCreateSocialNetworkSitesWithoutAnyParam...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        createSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testAddWithNoSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddWithNoSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        createSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
    }

    @Test
    public void testAddEmptySite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddEmptySite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        createSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddSiteWithSplChars() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddSiteWithSplChars...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*");
        createSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddInvalidSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddInvalidSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("facebook");
        createSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
        params.remove(key);
    }

    // Commented because of hang issue: refer LEAPFROG-251.
    /*@Test
    public void testAddBigSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddBigSite...");
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(
                "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite");
        createSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
        params.remove(key);
    }*/

    @Test
    public void testAddSiteSuccess() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testAddSiteSuccess...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);
        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);
        params.remove(key);
    }

    @Test
    public void testDeleteWithNoSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteWithNoSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        deleteSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testDeleteEmptySite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteEmptySite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        deleteSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(key);
    }

    @Test
    public void testDeleteSiteWithSplChars() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteSiteWithSplChars...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*");
        deleteSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
    }

    @Test
    public void testDeleteInvalidSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteInvalidSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("facebook");
        deleteSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
    }

    // Commented because of hang issue: refer LEAPFROG-251.
    /*@Test
    public void testDeleteBigSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteBigSite...");
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(
                "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite");
        deleteSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
    }*/

    @Test
    public void testDeleteNonExistingSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteSiteSuccess...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("twitter.com");
        deleteSocialNetworkSites(params,
                MailboxError.MBX_SOCIALNETWORK_SNS_NOTFOUND.name());

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("sites still empty.",
                site.equals(sites.get(0).getSocialNetworkSite()));
        params.remove(key);
    }

    @Test
    public void testDeleteSiteSuccess() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testDeleteSiteSuccess...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still non-empty.", sites.isEmpty());
        params.remove(key);
    }

    @Test
    public void testUpdateWithNoSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateWithNoSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        updateSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateEmptySite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateEmptySite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        deleteSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(key);
        params.remove(key1);
    }

    @Test
    public void testUpdateSiteWithSplChars() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateSiteWithSplChars...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*");
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        deleteSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
        params.remove(key1);
    }

    @Test
    public void testUpdateInvalidSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateInvalidSite...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("facebook");
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        deleteSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
        params.remove(key1);
    }

    // Commented because of hang issue: refer LEAPFROG-251.
    /*@Test
    public void testUpdateBigSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateBigSite...");
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.get(key).add(
                "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite."
                + "ThisIsAVeryBigSocialNetworkSite");
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        updateSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
        params.remove(key);
        params.remove(key1);
    }*/

    @Test
    public void testUpdateNonExistingSite() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateSiteSuccess...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add("twitter.com");
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        updateSocialNetworkSites(params,
                MailboxError.MBX_SOCIALNETWORK_SNS_NOTFOUND.name());

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("sites still empty.",
                site.equals(sites.get(0).getSocialNetworkSite()));
        params.remove(key);
        params.remove(key1);
    }

    @Test
    public void testUpdateNoSNSAEnabled() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateNoSNSAEnabled...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        updateSocialNetworkSites(params, ErrorCode.GEN_BAD_REQUEST.name());

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
    }

    @Test
    public void testUpdateNullSNSAEnabled() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateNullSNSAEnabled...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "twitter.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(null);
        updateSocialNetworkSites(params);

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));
        assertTrue("socialNetworkSiteAccessEnabled was not updated.",
                        BooleanType.NO.name().equalsIgnoreCase(sites.get(0)
                                        .getSocialNetworkSiteAccessEnabled()));

        params.remove(key);
        params.remove(key1);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
    }

    @Test
    public void testUpdateEmptySNSAEnabled() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateEmptySNSAEnabled...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add("");
        updateSocialNetworkSites(params);

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));
        assertTrue("socialNetworkSiteAccessEnabled was not updated.",
                        BooleanType.NO.name().equalsIgnoreCase(sites.get(0)
                                        .getSocialNetworkSiteAccessEnabled()));

        params.remove(key);
        params.remove(key1);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
    }

    @Test
    public void testUpdateInvalidSNSAEnabled() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateInvalidSNSAEnabled...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add("xyz");
        updateSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SNSITEACCESSENABLED.name());

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));

        params.remove(key);
        params.remove(key1);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
    }

    @Test
    public void testUpdateSplCharsSNSAEnabled() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateSplCharsSNSAEnabled...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add("!@#$%^&*");
        updateSocialNetworkSites(params,
                MailboxError.MBX_INVALID_SNSITEACCESSENABLED.name());

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));

        params.remove(key);
        params.remove(key1);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
    }

    @Test
    public void testUpdateSiteSuccess() throws Exception {
        System.out
                .println("SocialNetworkSitesTest.testUpdateSiteSuccess...");
        params.clear();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        String site = "facebook.com";
        String key = MailboxProperty.socialNetworkSite.name();
        String key1 = MailboxProperty.socialNetworkSiteAccessEnabled.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        createSocialNetworkSites(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(BooleanType.YES.name());
        updateSocialNetworkSites(params);

        // Get the list and compare
        List<SocialNetworkSite> sites = getSocialNetworkSites();
        assertNotNull("sites is null.", sites);
        assertTrue("sites still empty.", !sites.isEmpty());
        assertTrue("site not found.",
                site.equals(sites.get(0).getSocialNetworkSite()));
		assertTrue("socialNetworkSiteAccessEnabled was not updated.",
				BooleanType.YES.name().equalsIgnoreCase(sites.get(0)
						.getSocialNetworkSiteAccessEnabled()));

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(site);
        deleteSocialNetworkSites(params);

        params.remove(key);
        params.remove(key1);
    }

}
