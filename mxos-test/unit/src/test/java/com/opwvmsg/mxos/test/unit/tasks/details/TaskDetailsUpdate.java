/**
 * 
 */
package com.opwvmsg.mxos.test.unit.tasks.details;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksDetailsService;

/**
 * @author bgh21604
 * 
 */
public class TaskDetailsUpdate {
    private static IMxOSContext context;
    private static ExternalSession oxSession;
    private static final String baseURL = "http://localhost:8080/mxos";
    private static final String userName = "vinayaka@openwave.com";
    private static final String password = "test";
    private String taskId = "50";

    private static final String actualCost = "40000";
    private static final String actualWork = "2350";
    private static final String billing = "1247";
    private static final String targetCost = "10000";
    private static final String targetWork = "2000";
    private static final String currency = "USD";
    private static final String mileage = "500";
    private static final String companies = "aricent-group";

    private static void createContext() throws MxOSException {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.REST.name());
        p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Tasks");
        p.setProperty(ContextProperty.MXOS_BASE_URL, baseURL);
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "5");
        p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
        p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
        context = MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO",
                p);

    }

    private static void login() throws MxOSException {
        System.out.println("Login() called");
        assertNotNull("context is null", context);
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(),
                Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.password.name(), Arrays.asList(password));
        oxSession = service.login(requestMap);
        System.out.println("Login suceeded." + oxSession.getSessionId());
        assertTrue("Login suceeded.", oxSession.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("This is setUpBeforeClass");
        try {
            createContext();
            login();
            System.out.println("Afte login method");
        } catch (MxOSException e) {
            assertTrue("Login Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("This is tearDownAfterClass");
    }

    @Test
    public void testTaskDetailsUpdate() {
        System.out.println("testTaskDetailsUpdate() started");
        ITasksDetailsService service = null;

        try {
            service = (ITasksDetailsService) context
                    .getService(ServiceEnum.TasksDetailsService.name());

        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Task Details Update Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertNotNull("oxSession is null", oxSession);

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(), Arrays.asList(taskId));

        requestMap.put(TasksProperty.actualCost.name(),
                Arrays.asList(actualCost));
        requestMap.put(TasksProperty.actualWork.name(),
                Arrays.asList(actualWork));
        requestMap.put(TasksProperty.billing.name(), Arrays.asList(billing));
        requestMap.put(TasksProperty.targetCost.name(),
                Arrays.asList(targetCost));
        requestMap.put(TasksProperty.targetWork.name(),
                Arrays.asList(targetWork));
        requestMap.put(TasksProperty.currency.name(), Arrays.asList(currency));
        requestMap.put(TasksProperty.mileage.name(), Arrays.asList(mileage));
        requestMap
                .put(TasksProperty.companies.name(), Arrays.asList(companies));

        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        try {
            System.out.println("Before calling Update method" + requestMap);
            service.update(requestMap);

        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Task Details Update Failure. " + e.getMessage(),
                    e.getMessage() != null);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            assertTrue("Task Details Update Failure. " + e.getMessage(),
                    e.getMessage() != null);
            e.printStackTrace();
        }
    }
}