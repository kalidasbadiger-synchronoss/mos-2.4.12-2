package com.opwvmsg.mxos.test.unit.addressbook.workinfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to AddressBook Login
@SuiteClasses({ ContactsWorkInfoGET.class, ContactsWorkInfoPOST.class,
        ContactsWorkInfoCommunicationGET.class,
        ContactsWorkInfoCommunicationPOST.class,
        ContactsWorkInfoAddressGET.class, ContactsWorkInfoAddressPOST.class })
public class ContactsWorkInfoTestSuite {
}
