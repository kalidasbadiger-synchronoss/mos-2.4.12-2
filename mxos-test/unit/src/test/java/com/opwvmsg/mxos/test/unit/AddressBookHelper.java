package com.opwvmsg.mxos.test.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;

public class AddressBookHelper {

    private static IContactsService cs;
    private static IGroupsService gs;
    
    static {
        try {
            cs = (IContactsService) ContextUtils.loadContext().getService(
                    ServiceEnum.ContactsService.name());
            gs = (IGroupsService) ContextUtils.loadContext().getService(
                    ServiceEnum.GroupsService.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long createContact(String userId) {
        return createContact(userId, null);
    }

    public static long createContact(String userId, ExternalSession session) {

        Map<String, List<String>> params = new HashMap<String, List<String>>();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(userId);

        params.put("firstName", new ArrayList<String>());
        params.get("firstName").add("firstName1");

        params.put("middleName", new ArrayList<String>());
        params.get("middleName").add("middleName1");

        params.put("lastName", new ArrayList<String>());
        params.get("lastName").add("lastName1");

        params.put("displayName", new ArrayList<String>());
        params.get("displayName").add("displayName1");

        params.put("nickName", new ArrayList<String>());
        params.get("nickName").add("nickName1");

        params.put("suffix", new ArrayList<String>());
        params.get("suffix").add("suffix1");

        params.put("maritalStatus", new ArrayList<String>());
        params.get("maritalStatus").add("Married");

        params.put("spouseName", new ArrayList<String>());
        params.get("spouseName").add("spouseName1");

        params.put("companyName", new ArrayList<String>());
        params.get("companyName").add("companyName1");

        params.put("department", new ArrayList<String>());
        params.get("department").add("department1");

        params.put("manager", new ArrayList<String>());
        params.get("manager").add("manager1");

        params.put("assistant", new ArrayList<String>());
        params.get("assistant").add("assistant1");

        params.put("assistantPhone", new ArrayList<String>());
        params.get("assistantPhone").add("assistantPhone1");

        params.put("employeeId", new ArrayList<String>());
        params.get("employeeId").add("employeeId1");

        params.put("personalInfoCity", new ArrayList<String>());
        params.get("personalInfoCity").add("personalInfoCity1");

        params.put("personalInfoStateOrProvince", new ArrayList<String>());
        params.get("personalInfoStateOrProvince").add(
                "personalInfoStateOrProvince1");

        params.put("personalInfoPostalCode", new ArrayList<String>());
        params.get("personalInfoPostalCode").add("personalInfoPostalCode1");

        params.put("personalInfoCountry", new ArrayList<String>());
        params.get("personalInfoCountry").add("personalInfoCountry1");

        params.put("personalInfoPhone1", new ArrayList<String>());
        params.get("personalInfoPhone1").add("personalInfoPhone11");

        params.put("personalInfoPhone2", new ArrayList<String>());
        params.get("personalInfoPhone2").add("personalInfoPhone2");

        params.put("personalInfoMobile", new ArrayList<String>());
        params.get("personalInfoMobile").add("personalInfoMobile");

        params.put("personalInfoFax", new ArrayList<String>());
        params.get("personalInfoFax").add("personalInfoFax");

        params.put("personalInfoEmail", new ArrayList<String>());
        params.get("personalInfoEmail").add("personalInfoEmail1@test.com");

        params.put("personalInfoImAddress", new ArrayList<String>());
        params.get("personalInfoImAddress").add(
                "personalInfoImAddress1@test.com");

        params.put("workInfoCity", new ArrayList<String>());
        params.get("workInfoCity").add("workInfoCity1");

        params.put("workInfoStateOrProvince", new ArrayList<String>());
        params.get("workInfoStateOrProvince").add("workInfoStateOrProvince1");

        params.put("workInfoPostalCode", new ArrayList<String>());
        params.get("workInfoPostalCode").add("workInfoPostalCode1");

        params.put("workInfoCountry", new ArrayList<String>());
        params.get("workInfoCountry").add("workInfoCountry1");

        params.put("workInfoPhone1", new ArrayList<String>());
        params.get("workInfoPhone1").add("workInfoPhone11");

        params.put("workInfoPhone2", new ArrayList<String>());
        params.get("workInfoPhone2").add("workInfoPhone2");

        params.put("workInfoMobile", new ArrayList<String>());
        params.get("workInfoMobile").add("workInfoMobile");

        params.put("workInfoFax", new ArrayList<String>());
        params.get("workInfoFax").add("workInfoFax");

        params.put("workInfoEmail", new ArrayList<String>());
        params.get("workInfoEmail").add("workInfoEmail1@test.com");

        params.put("workInfoImAddress", new ArrayList<String>());
        params.get("workInfoImAddress").add("workInfoImAddress1@test.com");
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        try {

            long contactId = cs.create(params);
            return contactId;
        } catch (MxOSException e) {
            e.printStackTrace();
        }

        return 0;

    }

    public static long createGroup(String userId) {
        return createGroup(userId);
    }

    public static long createGroup(String userId, ExternalSession session) {

        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(userId);

        params.put("groupName", new ArrayList<String>());
        params.get("groupName").add("TestGroup");

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        try {
            return gs.create(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }

        return 0;

    }

    public static void deleteContact(String userId, long contactId) {
        deleteContact(userId, contactId, null);
    }

    public static void deleteContact(String userId, long contactId,
            ExternalSession session) {

        Map<String, List<String>> params = new HashMap<String, List<String>>();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(userId);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(contactId));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        try {
            cs.delete(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }

    }

    public static void deleteGroup(String userId, long groupId, ExternalSession session) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(userId);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(groupId));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        try {
            gs.delete(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }   
}
