package com.opwvmsg.mxos.test.unit.backend.crud.mss;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.opwvmsg.mxos.backend.crud.mss.MssSlMetaCRUD;

public class MssSlMetaCRUDTest {

    @Test
    public void testSortByFlagUnread() {
        Map<String, Boolean> map = new HashMap<String, Boolean>();
        map.put("a", true);
        map.put("b", false);
        map.put("c", false);
        map.put("d", true);
        List<String> list = MssSlMetaCRUD.sortByFlagUnread(map, true);
        assertEquals("b", list.get(0));
        assertEquals("c", list.get(1));
        assertEquals("a", list.get(2));
        assertEquals("d", list.get(3));
        
        list = MssSlMetaCRUD.sortByFlagUnread(map, false);
        assertEquals("a", list.get(0));
        assertEquals("d", list.get(1));
        assertEquals("b", list.get(2));
        assertEquals("c", list.get(3));
    }
}
