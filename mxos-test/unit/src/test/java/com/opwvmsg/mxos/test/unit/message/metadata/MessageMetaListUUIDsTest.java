package com.opwvmsg.mxos.test.unit.message.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MessageMetaListUUIDsTest {

    private static final String TEST_NAME = "MessageMetaListUUIDsTest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "gappsuser_1976@mytrial2.co.uk";
    private static final String PASSWORD = "test";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageMetaDataListUidFrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";
    private static final String FOLDERNAME_VALUE_1 = "SentMail";
    private static final String FOLDERNAME_VALUE_INVALID = "INBOX1";    

    private static IMxOSContext context;
    private static IMetadataService metadataService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static final String COS_ID = "default";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        // params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("message metadata object is null.", metadataService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        metadataService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testUidsListMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);

            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            System.out.println("message.size() : " + uuids.size());
            assertTrue("Unable to read uuids",
                    (uuids.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testUidsListMessageMetaDataNoMessageSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataNoMessageSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1);
            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            System.out.println("message.size() : " + uuids.size());
            assertTrue("Unable to read message metadata",
                    (uuids.size() == 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testUidsListMessageMetaDataNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testUidsListMessageMetaDataInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "krishan@@@domain1.co.in");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }
    
    @Test
    public void testUidsListMessageMetaDataNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testUidsListMessageMetaDataInvalidFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUidsListMessageMetaDataInvalidFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INVALID);
            List<String> uuids = metadataService
                    .listUUIDs(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    FolderError.FLD_NOT_FOUND.name(), e.getCode());
        }
    }
}
