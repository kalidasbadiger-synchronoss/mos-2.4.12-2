package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailReceiptFilter {

    private static ICosMailReceiptFiltersService cosmrfs = null;
    private static ICosService cosService = null;
    private static Map <String, List<String> >inputParams =
        new HashMap<String, List<String>>();
    private static String test = "test";
    private static String cosIdKey = "cosId";
    
    private static ICosMailReceiptSieveFiltersService cossfs = null;
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.put("loadDefaultCos", new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.get("loadDefaultCos").add("yes");
        try {
            cosmrfs = (ICosMailReceiptFiltersService) ContextUtils
                    .loadContext().getService(
                            ServiceEnum.CosMailReceiptFiltersService.name());

            cossfs = (ICosMailReceiptSieveFiltersService) ContextUtils
                    .loadContext().getService(
                            ServiceEnum.CosMailReceiptSieveFiltersService
                                    .name());
            cosService = (ICosService) ContextUtils.loadContext().getService(
                    ServiceEnum.CosService.name());
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNotNull(cosmrfs);
    }
    
    
    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:cos starting......");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosService.delete(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptFiltersGet() {
        System.out.println("testMailReceiptFiltersGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            Filters f = cosmrfs.read(inputParams);
            assertNotNull(f);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptSieveFiltersGet() {
        System.out.println("testMailReceiptSieveFiltersGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            SieveFilters sf  = cossfs.read(inputParams);
            assertNotNull(sf);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptSieveFilterPost1() {
        System.out.println("testMailReceiptSieveFilterPost1:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("blockedSenderAction", new ArrayList<String>());
        inputParams.get("blockedSenderAction").add(MxosEnums.BlockedSenderAction.SIDELINE.name());
        try {
            cossfs.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            SieveFilters sf  = cossfs.read(inputParams);
            assertEquals("Passed: ", MxosEnums.BlockedSenderAction.SIDELINE, sf.getBlockedSenderAction());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptSieveFilterPost2() {
        System.out.println("testMailReceiptSieveFilterPost2:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("rejectBouncedMessage", new ArrayList<String>());
        inputParams.get("rejectBouncedMessage").add(MxosEnums.BooleanType.YES.name());
        try {
            cossfs.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            SieveFilters sf  = cossfs.read(inputParams);
            assertEquals("Passed: ", MxosEnums.BooleanType.YES, sf.getRejectBouncedMessage());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptSieveFilterPost3() {
        System.out.println("testMailReceiptSieveFilterPost3:cos ....");
        String message ="You are blocked";
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("blockedSenderMessage", new ArrayList<String>());
        inputParams.get("blockedSenderMessage").add(message);
        try {
            cossfs.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            SieveFilters sf  = cossfs.read(inputParams);
            assertEquals("Passed: ", message, sf.getBlockedSenderMessage());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptSieveFiltersPostNeg1() {
        System.out.println("testMailReceiptSieveFiltersPostNeg1:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cossfs.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", ErrorCode.GEN_BAD_REQUEST.name() ,me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptSieveFiltersPostNeg2() {
        System.out.println("testMailReceiptSieveFiltersPostNeg2:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("sieveFilteringEnabled", new ArrayList<String>());
        inputParams.get("sieveFilteringEnabled").add("illegal");
        try {
            cossfs.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", MailboxError.MBX_INVALID_SIEVE_FILTERING_ENABLED.name(), me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptSieveFiltersPostNeg3() {
        System.out.println("testMailReceiptSieveFiltersPostNeg3:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("blockedSenderAction", new ArrayList<String>());
        inputParams.get("blockedSenderAction").add("Maybe");
        try {
            cossfs.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", me.getCode(), MailboxError.MBX_INVALID_SIEVE_FILTERS_BLOCKED_SENDER_ACTION.name());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptFilterGet() {
        System.out.println("testMailReceiptFilterGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            Filters f  = cosmrfs.read(inputParams);
            assertNotNull(f);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }

}