package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedDomainService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IEmailAliasService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.DomainHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class AliasTest {
    private static final String COSID = "cos_2.0_ut200";
    private static final String DOMAIN_NAME = "domain2ut200.com";
    private static String MAILBOX_ID;
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100005@" + DOMAIN_NAME;
    private static final String PWD = "test1";
    private static IMxOSContext context;
    private static IEmailAliasService aliasService;
    private static IAllowedDomainService adService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();

    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("AliasTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        aliasService = (IEmailAliasService) context
                .getService(ServiceEnum.MailAliasService.name());
        adService = (IAllowedDomainService) context
                .getService(ServiceEnum.AllowedDomainService.name());
        assertNotNull("AliasService object is null.", aliasService);
        assertNotNull("AllowedDomainService object is null.", adService);
        CosHelper.createCos(COSID);
        DomainHelper.createDomain(DOMAIN_NAME);
        MAILBOX_ID = MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        //assertTrue("Mailbox NOT Created", (MAILBOX_ID > 0));
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("AliasTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("AliasTest.tearDown...");
        inputParams.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("AliasTest.tearDownAfterClass...");
        inputParams.clear();
        // Delete the mailbox if already exists
        try {
            MailboxHelper.deleteMailbox(EMAIL);
            DomainHelper.deleteDomain(DOMAIN_NAME);
            CosHelper.deleteCos(COSID);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
        aliasService = null;
        adService = null;
    }

    @Test
    public void testReadAlisWithNullEmail() throws Exception {
        System.out.println("AliasTest.testReadAlis...");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, null);
            aliasService.read(inputParams);
            fail();
        } catch (MxOSException e) {
            e.printStackTrace();
            assertTrue(true);
        }
    }
    
    @Test
    public void testAddAlisWithOutAllowedDomain() throws Exception {
        System.out.println("AliasTest.testAddAlisWithOutAllowedDomain...");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
            String alias1 = "bstestuser100a@test123.com";
            inputParams.get(MailboxProperty.emailAlias.name()).add(alias1);
            aliasService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_ALIAS_NOT_EXIST_IN_ALLOWED_DOMAINS",
                    e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_ALIAS_NOT_EXIST_IN_ALLOWED_DOMAINS.name()));
        }
    }
    @Test
    public void testAddAliasWithAllowedDomain() throws Exception {
        System.out.println("AliasTest.testAddAliasWithAllowedDomain...");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(MailboxProperty.allowedDomain.name(), new ArrayList<String>());
            inputParams.get(MailboxProperty.allowedDomain.name()).add("test.com");
            adService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
            String alias1 = "bstestuser100a@test.com";
            inputParams.get(MailboxProperty.emailAlias.name()).add(alias1);
            aliasService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String> aliases = aliasService.read(inputParams);
            System.out.println(aliases);
            assertNotNull(aliases);
            assertTrue(aliases.contains(alias1));
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testAddAliasWithAllowedDomainCaseSensitivity() throws Exception {
        System.out.println("AliasTest.testAddAliasWithAllowedDomainCaseSensitivity...");
        try {
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(MailboxProperty.allowedDomain.name(), new ArrayList<String>());
            inputParams.get(MailboxProperty.allowedDomain.name()).add("test.com");
            adService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            inputParams.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
            String alias1 = "bstestuser101a@TEST.com";
            inputParams.get(MailboxProperty.emailAlias.name()).add(alias1);
            aliasService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String> aliases = aliasService.read(inputParams);
            System.out.println(aliases);
            assertNotNull(aliases);
            assertTrue(aliases.contains(alias1.toLowerCase()));
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testAddAlisWithInvalidAlias() throws Exception {
        System.out.println("AliasTest.testAddAlisWithInvalidAlias...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
        String alias1 = "bstestuser100a@test1.com";
        inputParams.get(MailboxProperty.emailAlias.name()).add(alias1);
        try {
            aliasService.create(inputParams);
        } catch (MxOSException e) {
            assertTrue(true);
        }
    }
    
    @Test
    public void testAddAlisWithMultipleAliasesSuccess() throws Exception {
        System.out.println("AliasTest.testAddAlisWithMultipleAliasesSuccess...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.emailAlias.name(), new ArrayList<String>());
        String alias1 = "bstestuser100a@" + DOMAIN_NAME;
        String alias2 = "bstestuser101a@" + DOMAIN_NAME;
        String alias3 = "bstestuser102a@" + DOMAIN_NAME;
        inputParams.get(MailboxProperty.emailAlias.name()).add(alias1);
        inputParams.get(MailboxProperty.emailAlias.name()).add(alias2);
        inputParams.get(MailboxProperty.emailAlias.name()).add(alias3);
        try {
            aliasService.create(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            List<String>aliases=aliasService.read(inputParams);
            if(aliases.size() < 3){
                fail();
            }
        } catch (MxOSException e) {
            assertTrue(true);
        }
    }

}
