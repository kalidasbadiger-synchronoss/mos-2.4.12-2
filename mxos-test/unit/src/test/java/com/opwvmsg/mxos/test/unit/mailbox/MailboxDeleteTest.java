package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 */
public class MailboxDeleteTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "bstestuser1001@openwave.com";
    private static final String EMAIL2 = "deleteInMss@openwave.com";
    private static final String PWD_KEY = MailboxProperty.password.name();
    private static final String PWD = "test";
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID = "cos_2.0_ut204";
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailboxDeleteTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        MailboxHelper.createMailbox(EMAIL2, PWD, COSID);
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailboxDeleteTest.setUp...");
    }
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailboxDeleteTest.tearDown...");
    }
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailboxDeleteTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(EMAIL2);
        CosHelper.deleteCos(COSID);
        inputParams = null;
        mailboxService = null;
    }
    @Test
    public void testDeleteMailboxBadRequest() {
        System.out.println("MailboxDeleteTest.testDeleteMailboxBadRequest...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        //inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.delete(inputParams);
            fail();
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }
    @Test
    public void testDeleteMailbox() {
        System.out.println("MailboxDeleteTest.testDeleteMailbox...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testDeleteMailboxInMss() {
        System.out.println("MailboxDeleteTest.testDeleteMailboxInMss...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.deleteMSS(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }
}
