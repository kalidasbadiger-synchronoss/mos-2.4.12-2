package com.opwvmsg.mxos.test.unit.message.header;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    MessageHeaderGETTest.class
})
public class MessageHeaderTestSuite {
}
