/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.test.unit.ldap;

import javax.naming.directory.Attributes;

import com.opwvmsg.mxos.backend.ldap.LDAPMapper;
import com.opwvmsg.mxos.data.pojos.Mailbox;

/**
 * class
 *
 * @author mxos-dev
 *
 */
public class MailboxMapper implements LDAPMapper<Mailbox> {

    public Mailbox mapping(Attributes attrs) {
        Mailbox mailbox = new Mailbox();
        MailboxBaseMapper baseMapper = new MailboxBaseMapper();
        mailbox.setBase(baseMapper.mapping(attrs));
        return mailbox;
    }

}
