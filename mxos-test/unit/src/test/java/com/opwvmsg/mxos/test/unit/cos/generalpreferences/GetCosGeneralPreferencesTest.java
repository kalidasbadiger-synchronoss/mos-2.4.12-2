package com.opwvmsg.mxos.test.unit.cos.generalpreferences;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.PreferredUserExperience;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosGeneralPreferencesService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosGeneralPreferencesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosGeneralPreferencesService cosGeneralPreferencesService;;
    private static ICosService cosService;
    private static IMxOSContext context;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosGeneralPreferencesTest....setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosGeneralPreferencesService = (ICosGeneralPreferencesService) context
                .getService(ServiceEnum.CosGeneralPreferencesService.name());
        cosService = (ICosService) context
                .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosInternalInfoTest.setUp...");
        params.get(COSID_KEY).add(COS_ID);
        assertNotNull("CosGeneralPreferencesService object is null.",
                cosGeneralPreferencesService);
        assertNotNull("cosService object is null.", cosService);
    }
    
    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);

        try {
            cosService.create(inputParams);

        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static GeneralPreferences GetCosGeneralPreferencesParams(
            Map<String, List<String>> params) {

        return GetCosGeneralPreferencesParams(params, null);
    }

    private static GeneralPreferences GetCosGeneralPreferencesParams(
            Map<String, List<String>> params, String expectedError) {

        GeneralPreferences generalPreferences = null;
        try {
            generalPreferences = cosGeneralPreferencesService.read(params);
            if (null != expectedError) {
                fail("This should not have come!");
            } else {

                assertNotNull("GeneralPreferences object is null.",
                        generalPreferences);
            }
        } catch (MxOSException me) {
            if (null == expectedError) {
                fail("This should not have come!");
            } else {
                assertNotNull("MxOSError is not null", me);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, me.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return generalPreferences;
    }
    
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.tearDown...");
        if (null != params.get(COSID_KEY)) {
            params.get(COSID_KEY).clear();
        }
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GeneralPreferencesTest...tearDownAfterClass");
        params.clear();
        deleteCos(COS_ID);
        cosGeneralPreferencesService = null;
        cosService = null;
    }

    @Test
    public void testGetCosGeneralPreferencesWithoutCosId() {
        System.out
                .println("CosGeneralPreferencesTest.testGetCosCrdentialsWithoutCosId.");
        params.clear();
        GetCosGeneralPreferencesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(COSID_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetCosGeneralPreferencesWithNullCosId() {
        System.out
                .println("CosGeneralPreferencesTest.testGetCosCrdentialsWithNullCosId.");
        params.get(COSID_KEY).clear();
        GetCosGeneralPreferencesParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetCosGeneralPreferencesWithNonExistingCosId() {
        System.out
                .println("CosGeneralPreferencesTest.testGetCosCrdentialsWithNonExistingCosId.");
        params.get(COSID_KEY).clear();
        params.get(COSID_KEY).add("some_junk_cos124");
        GetCosGeneralPreferencesParams(params, CosError.COS_NOT_FOUND.name());
    }

    @Test
    public void testParentalControlEnabledSuccess() {
        System.out
                .println("CosGeneralPreferencesTest.testParentalControlEnabledSuccess.");
        params.get(COSID_KEY).clear();
        params.get(COSID_KEY).add(COS_ID);
        BooleanType pce = GetCosGeneralPreferencesParams(params)
                .getParentalControlEnabled();
        assertNotNull("ParentalControlEnabled is null.", pce);
    }

    @Test
    public void testRecycleBinEnabledSuccess() {
        System.out
                .println("CosGeneralPreferencesTest.testRecycleBinEnabledSuccess");
        BooleanType rbe = GetCosGeneralPreferencesParams(params)
                .getRecycleBinEnabled();
        assertNotNull("RecycleBinEnabled is null.", rbe);
    }

    @Test
    public void testPreferredThemeSuccess() {
        System.out
                .println("CosGeneralPreferencesTest.testPreferredThemeSuccess.");
        String pt = GetCosGeneralPreferencesParams(params).getPreferredTheme();
        assertNotNull("PreferredTheme is null.", pt);
    }

    @Test
    public void testTimezoneSuccess() {
        System.out.println("CosGeneralPreferencesTest.testTimezoneSuccess.");
        String timeZone = GetCosGeneralPreferencesParams(params).getTimezone();
        assertNotNull("Timezone is null.", timeZone);
    }

    @Test
    public void testCharsetSuccess() {
        System.out.println("CosGeneralPreferencesTest.testCharsetSuccess.");
        String charset = GetCosGeneralPreferencesParams(params).getCharset();
        assertNotNull("Charset is null.", charset);
    }

    @Test
    public void testPreferredUserExperience() {
        System.out
                .println("CosGeneralPreferencesTest.testPreferredUserExperience.");
        PreferredUserExperience pue = GetCosGeneralPreferencesParams(params)
                .getPreferredUserExperience();
        assertNotNull("PreferredUserExperience is null.", pue);
    }

    @Test
    public void testWebMailMTA() {
        System.out
                .println("CosGeneralPreferencesTest.testPreferredThemeSuccess.");
        String wm = GetCosGeneralPreferencesParams(params).getWebmailMTA();
        assertNotNull("WebMailMTA is null.", wm);
    }
}
