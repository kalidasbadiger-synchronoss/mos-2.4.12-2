package com.opwvmsg.mxos.test.unit.smsservices;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    SmsServicesPOST.class,
    SmsServicesGET.class,
    SmsNotificationsPOST.class,
    SmsNotificationsGET.class,
    SmsOnlinePOST.class,
    SmsOnlineGET.class
})
public class SmsServicesTestSuite {
}
