package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    CosMailReceipt.class,
    CosMailReceiptFilter.class,
    CosMailReceiptSenderBlocking.class,
    CosMailReceiptCloudmarkFiltersGET.class,
    CosMailReceiptCloudmarkFiltersPOST.class,
    CosMailReceiptMaxForwardAddresses.class
    
})
public class CosMailReceiptTestSuite {
}
