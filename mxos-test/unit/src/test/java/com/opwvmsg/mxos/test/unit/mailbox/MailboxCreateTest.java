package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.adventnet.afp.log.Logger;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailReceipt.AckReturnReceiptReq;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.DomainHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 *
 * @author mxos-dev
 */
public class MailboxCreateTest {
    private static final String COSID = "default23";
    private static final String FIRSTNAME="Foooo";
    private static final String LASTNAME="Barrrr";
    private static final String DOMAIN_NAME = "openwave.com";
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100005@" + DOMAIN_NAME;
    private static final String EMAIL2 = "mx100006@" + DOMAIN_NAME;
    private static final String EMAIL3 = "mx100007@" + DOMAIN_NAME;
    private static final String EMAIL4 = "mx100009@" + DOMAIN_NAME;
    private static final String EMAIL5 = "mx100010@" + DOMAIN_NAME;
    private static final String PWD_KEY = MailboxProperty.password.name();
    private static final String PWD = "test";
    private static final String BCRYPT_PWD = "$2a$10$Ro0CUfOqk6cXEKf3dyaM7Ox3VKMOUI7atskEN0jUadLjfwdMfJU7S";
    private static final String PWD_STORE_TYPE_KEY = MailboxProperty.passwordStoreType.name();
    private static final String PWD_STORE_TYPE_BCRYPT = "bcrypt";
    private static final String PWD_STORE_TYPE = "sha1";
    private static final String PRE_ENCYRPT_PWD_KEY = MailboxProperty.preEncryptedPasswordAllowed.name();
    private static final String PRE_ENCYRPT_PWD = "true";
    private static final String PRE_ENCYRPT_FALSE_PWD = "false";
    private static final String GROUP_KEY = MailboxProperty.groupName.name();
    private static final String GROUP_VALUE = "family2.com";
    private static final String TYPE_KEY = MailboxProperty.type.name();
    private static final String TYPE_VALUE = "groupMailbox";
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String FIRSTNAME_KEY = MailboxProperty.firstName.name();
    private static final String LASTNAME_KEY = MailboxProperty.lastName.name();
    private static final String EMAIL_ADDRESS_KEY = MailboxProperty.emailAlias.name();
    private static final String ALIAS1 = "mx1000061Alias1@" + DOMAIN_NAME;
    private static final String ALIAS2 = "mx1000061Alias2@" + DOMAIN_NAME;
    private static final String ALIAS3 = "mx1000061Alias3@" + DOMAIN_NAME;
    private static final String FROM_ADDRESS_KEY = MailboxProperty.fromAddress.name();
    private static final String FROM_ADDRESS = "mx1000061from@" + DOMAIN_NAME;
    private static final String NUM_DELAYED_DELIVARY_MESSAGES_PENDING_KEY = MailboxProperty.numDelayedDeliveryMessagesPending.name();
    private static final String NUM_DELAYED_DELIVARY_MESSAGES_PENDING1 = "2011-06-28T17:26:54Z,MessageId1";
    private static final String NUM_DELAYED_DELIVARY_MESSAGES_PENDING2 = "2012-07-28T17:26:55Z,MessageId2";
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailboxCreateTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        // Delete the mailbox if already exists
        inputParams.clear();
        try {
            CosHelper.createCos(COSID);
            DomainHelper.createDomain(DOMAIN_NAME);
            MailboxHelper.deleteMailbox(EMAIL);
            MailboxHelper.deleteMailbox(EMAIL2);
            MailboxHelper.deleteMailbox(EMAIL3);
            MailboxHelper.deleteMailbox(EMAIL4);
            MailboxHelper.deleteMailbox(EMAIL5);
        } catch (Exception e) {
        }
    }
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailboxCreateTest.setUp...");
    }
    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailboxCreateTest.tearDown...");
    }
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailboxCreateTest.tearDownAfterClass...");
        // Delete the mailbox if already exists
        try {
            MailboxHelper.deleteMailbox(EMAIL);
            MailboxHelper.deleteMailbox(EMAIL2);
            MailboxHelper.deleteMailbox(EMAIL3);
            MailboxHelper.deleteMailbox(EMAIL4);
            MailboxHelper.deleteMailbox(EMAIL5);
            DomainHelper.deleteDomain(DOMAIN_NAME);
            CosHelper.deleteCos(COSID);
        } catch (Exception e) {
            fail();
        }
        inputParams = null;
        mailboxService = null;
    }
    @Test
    public void testCreateMailbox() {
        System.out.println("testCreateMailbox...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(FIRSTNAME_KEY, new ArrayList<String>());
        inputParams.get(FIRSTNAME_KEY).add(FIRSTNAME);
        inputParams.put(LASTNAME_KEY, new ArrayList<String>());
        inputParams.get(LASTNAME_KEY).add(LASTNAME);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    @Test
    public void testCreateMailbox2() {
        System.out.println("testCreateMailbox2...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(EMAIL_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS1);
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS2);
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS3);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        // HK attrib
       inputParams.put(MailboxProperty.notificationMailSentStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add(
                "notSent");
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastSuccessfulLoginDate.name()).add(
                "2013-02-27T07:32:33Z");
        inputParams.put(MailboxProperty.signature.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.signature.name()).add(
                "Best Regards : OPWV");
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateMailbox3() {
        System.out.println("testCreateMailbox3...");
        MailboxHelper.deleteMailbox(EMAIL3);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL3);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(BCRYPT_PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add(PWD_STORE_TYPE_BCRYPT);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);        
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateMailbox4() {
        System.out.println("testCreateMailbox4...");
        MailboxHelper.deleteMailbox(EMAIL4);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL4);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(BCRYPT_PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add(PWD_STORE_TYPE_BCRYPT);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_FALSE_PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);        
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            assertEquals("MBX_ENCRYPTION_NOT_SUPPORTED", e.getCode());
        }
    }

    @Test
    public void testCreateGroupMailbox() {
        System.out.println("testCreateGroupMailbox...");
        MailboxHelper.deleteMailbox(EMAIL5);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL5);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(BCRYPT_PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add(PWD_STORE_TYPE_BCRYPT);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_FALSE_PWD);
        inputParams.put(GROUP_KEY, new ArrayList<String>());
        inputParams.get(GROUP_KEY).add(GROUP_VALUE);
        inputParams.put(TYPE_KEY, new ArrayList<String>());
        inputParams.get(TYPE_KEY).add(TYPE_VALUE);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            assertEquals("MBX_ENCRYPTION_NOT_SUPPORTED", e.getCode());
        }
    }

    @Test
    public void testCreateMailboxWithAllOptionalParameters() {
        System.out.println("testCreateMailboxWithAllOptionalParameters...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();

        // Mandatory attributes
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);

        // Optional attributes
        inputParams.put(MailboxProperty.userName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.userName.name()).add("foooo");

        inputParams.put(MailboxProperty.firstName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.firstName.name()).add("bar");

        inputParams.put(MailboxProperty.lastName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add("bar");

        inputParams.put(MailboxProperty.status.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.status.name()).add("active");

        inputParams.put(MailboxProperty.lastStatusChangeDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastStatusChangeDate.name()).add(
                "2013-02-14T07:32:33Z");
        // HK attrib
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add(
                "notSent");

        inputParams.put(MailboxProperty.msisdn.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdn.name()).add("+919000000000");

        inputParams.put(MailboxProperty.msisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.msisdnStatus.name()).add(
                MxosEnums.MsisdnStatus.ACTIVATED.name());

        inputParams.put(MailboxProperty.lastMsisdnStatusChangeDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastMsisdnStatusChangeDate.name()).add(
                "2013-02-14T07:32:33Z");

        inputParams.put(MailboxProperty.maxNumAliases.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAliases.name()).add("10");

        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add("10");
        
        inputParams.put(EMAIL_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS1);
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS2);
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS3);

        // Allowed domain multi
        inputParams.put(MailboxProperty.allowedDomain.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.allowedDomain.name()).add(DOMAIN_NAME);

        // Web mail features
        inputParams.put(MailboxProperty.businessFeaturesEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.businessFeaturesEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.fullFeaturesEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.fullFeaturesEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.lastFullFeaturesConnectionDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastFullFeaturesConnectionDate.name())
                .add("2013-02-14T07:32:33Z");

        // Credentials
        inputParams.put(MailboxProperty.passwordStoreType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordStoreType.name()).add(
                MxosEnums.PasswordStoreType.CLEAR.name());

        inputParams.put(MailboxProperty.passwordRecoveryAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.passwordRecoveryPreference.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryPreference.name()).add(
                MxosEnums.PasswordRecoveryPreference.ALL.name());

        inputParams.put(MailboxProperty.passwordRecoveryMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdn.name()).add(
                "+919000000000");

        inputParams.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryMsisdnStatus.name())
                .add(MxosEnums.MsisdnStatus.ACTIVATED.name());

        inputParams.put(
                MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate
                        .name(), new ArrayList<String>());
        inputParams.get(
                MailboxProperty.lastPasswordRecoveryMsisdnStatusChangeDate
                        .name()).add("2013-02-14T07:32:33Z");

        inputParams.put(MailboxProperty.passwordRecoveryEmail.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryEmail.name()).add(
                "abc@foo.com");

        inputParams.put(MailboxProperty.passwordRecoveryEmailStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordRecoveryEmailStatus.name())
                .add(MxosEnums.MsisdnStatus.ACTIVATED.name());

        inputParams.put(MailboxProperty.failedLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.failedLoginAttempts.name()).add("1");

        inputParams.put(MailboxProperty.maxFailedLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxFailedLoginAttempts.name()).add("5");

        inputParams.put(MailboxProperty.failedCaptchaLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.failedCaptchaLoginAttempts.name()).add("1");

        inputParams.put(MailboxProperty.maxFailedCaptchaLoginAttempts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxFailedCaptchaLoginAttempts.name()).add("5");
        
        inputParams.put(MailboxProperty.lastLoginAttemptDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastLoginAttemptDate.name()).add(
                "2013-02-14T07:32:33Z");
        // HK attrib
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastSuccessfulLoginDate.name()).add(
                "2013-02-27T07:32:33Z");

        // General Preferences
        inputParams.put(MailboxProperty.locale.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.locale.name()).add("de_DE");

        inputParams
                .put(MailboxProperty.charset.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.charset.name()).add("ISO-8859-1");

        inputParams.put(MailboxProperty.parentalControlEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.parentalControlEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.preferredTheme.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.preferredTheme.name()).add("orange");

        inputParams.put(MailboxProperty.preferredUserExperience.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.preferredUserExperience.name()).add(
                MxosEnums.PreferredUserExperience.BASIC.name());

        inputParams.put(MailboxProperty.recycleBinEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.recycleBinEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.timezone.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.timezone.name()).add("PST8PDT");

        /*inputParams.put(MailboxProperty.userThemes.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.userThemes.name()).add("orange");*/

        inputParams.put(MailboxProperty.webmailMTA.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailMTA.name()).add("localhost");

        // MailSend
        inputParams.put(MailboxProperty.fromAddress.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.fromAddress.name()).add("abc@foo.com");

        inputParams.put(MailboxProperty.futureDeliveryEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.futureDeliveryEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.maxFutureDeliveryDaysAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxFutureDeliveryDaysAllowed.name())
                .add("100");

        inputParams.put(
                MailboxProperty.maxFutureDeliveryMessagesAllowed.name(),
                new ArrayList<String>());
        inputParams
                .get(MailboxProperty.maxFutureDeliveryMessagesAllowed.name())
                .add("100");

        inputParams.put(MailboxProperty.alternateFromAddress.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.alternateFromAddress.name()).add(
                "abc@foo.com");

        inputParams.put(MailboxProperty.replyToAddress.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.replyToAddress.name()).add(
                "abc@foo.com");

        inputParams.put(MailboxProperty.useRichTextEditor.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.useRichTextEditor.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.includeOriginalMailInReply.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.includeOriginalMailInReply.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.originalMailSeperatorCharacter.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.originalMailSeperatorCharacter.name())
                .add(">");

        inputParams.put(MailboxProperty.autoSaveSentMessages.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.autoSaveSentMessages.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.addSignatureForNewMails.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.addSignatureForNewMails.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.addSignatureInReplyType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.addSignatureInReplyType.name()).add(
                "afterOriginalMessage");

        inputParams.put(MailboxProperty.autoSpellCheckEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.autoSpellCheckEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.confirmPromptOnDelete.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.confirmPromptOnDelete.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.includeReturnReceiptReq.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.includeReturnReceiptReq.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(NUM_DELAYED_DELIVARY_MESSAGES_PENDING_KEY,
                new ArrayList<String>());
        inputParams.get(NUM_DELAYED_DELIVARY_MESSAGES_PENDING_KEY).add(
                NUM_DELAYED_DELIVARY_MESSAGES_PENDING1);
        inputParams.get(NUM_DELAYED_DELIVARY_MESSAGES_PENDING_KEY).add(
                NUM_DELAYED_DELIVARY_MESSAGES_PENDING2);
        
        inputParams.put(MailboxProperty.maxSendMessageSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxSendMessageSizeKB.name())
                .add("1000");

        inputParams.put(MailboxProperty.maxAttachmentSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxAttachmentSizeKB.name()).add("1000");

        inputParams.put(MailboxProperty.maxAttachmentsInSession.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxAttachmentsInSession.name()).add("10");

        
        inputParams.put(MailboxProperty.maxAttachmentsToMessage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxAttachmentsToMessage.name()).add("5");
        
        inputParams.put(MailboxProperty.maxCharactersPerPage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxCharactersPerPage.name()).add(
                "10000");

        // MailSend BMIFilters
        inputParams.put(MailboxProperty.mailSendBmiSpamAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailSendBmiSpamAction.name()).add(
                MxosEnums.BmiSpamType.DISCARD.name());

        inputParams.put(MailboxProperty.spamMessageIndicator.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.spamMessageIndicator.name()).add(
                "unknown");

        // MailSend CommtouchFilters
        inputParams.put(MailboxProperty.mailSendCommtouchSpamAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailSendCommtouchSpamAction.name())
                .add(MxosEnums.CommtouchActionType.ALLOW.name());

        inputParams.put(MailboxProperty.mailSendCommtouchVirusAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailSendCommtouchVirusAction.name())
                .add(MxosEnums.CommtouchActionType.DISCARD.name());

        // MailSend McAfeeFilters
        inputParams.put(MailboxProperty.mailSendMcAfeeVirusAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailSendMcAfeeVirusAction.name()).add(
                MxosEnums.McAfeeVirusActionType.DISCARD.name());

        // MailReceipt
        inputParams.put(MailboxProperty.receiveMessages.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.receiveMessages.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.forwardingEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.forwardingEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "12");

        inputParams.put(MailboxProperty.copyOnForward.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.copyOnForward.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.filterHTMLContent.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.filterHTMLContent.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.displayHeaders.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.displayHeaders.name()).add(
                MxosEnums.DisplayHeadersType.HTML.name());

        inputParams.put(MailboxProperty.webmailDisplayWidth.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailDisplayWidth.name()).add("80");

        inputParams.put(MailboxProperty.webmailDisplayFields.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailDisplayFields.name()).add(
                "from,subj,size,date");

        inputParams.put(MailboxProperty.maxMailsPerPage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxMailsPerPage.name()).add("100");

        /*inputParams.put(MailboxProperty.previewPaneEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.previewPaneEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());*/

        inputParams.put(MailboxProperty.ackReturnReceiptReq.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.ackReturnReceiptReq.name()).add(
                AckReturnReceiptReq.ALWAYS.name());

        inputParams.put(MailboxProperty.deliveryScope.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.deliveryScope.name()).add(
                MxosEnums.DeliveryScope.GLOBAL.name());

        inputParams.put(MailboxProperty.autoReplyMode.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.autoReplyMode.name()).add(
                MxosEnums.AutoReplyMode.VACATION.name());

        inputParams.put(MailboxProperty.autoReplyMessage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.autoReplyMessage.name()).add("OOO");

        inputParams.put(MailboxProperty.maxReceiveMessageSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxReceiveMessageSizeKB.name()).add(
                "100");
        
        inputParams.put(MailboxProperty.mobileMaxReceiveMessageSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMaxReceiveMessageSizeKB.name()).add(
                "100");

        // SenderBlocking
        inputParams.put(MailboxProperty.senderBlockingAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.senderBlockingAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.senderBlockingEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.senderBlockingEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.rejectAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.rejectAction.name()).add(
                MxosEnums.RejectActionType.DROP.name());

        inputParams.put(MailboxProperty.rejectFolder.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.rejectFolder.name()).add("junk");

        inputParams.put(MailboxProperty.blockSendersPABActive.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.blockSendersPABActive.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.blockSendersPABAccess.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.blockSendersPABAccess.name()).add(
                MxosEnums.BooleanType.YES.name());

        // MailReceipt SieveFilters
        inputParams.put(MailboxProperty.sieveFilteringEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.sieveFilteringEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.mtaFilter.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mtaFilter.name())
                .add("Test MTA Filter");

        inputParams.put(MailboxProperty.rmFilter.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.rmFilter.name()).add("Test RM Filter");

        inputParams.put(MailboxProperty.blockedSenderAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.blockedSenderAction.name()).add(
                MxosEnums.BlockedSenderAction.BOUNCE.name());

        inputParams.put(MailboxProperty.blockedSenderMessage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.blockedSenderMessage.name()).add(
                "Message bounced");

        inputParams.put(MailboxProperty.rejectBouncedMessage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.rejectBouncedMessage.name()).add(
                MxosEnums.BooleanType.YES.name());

        // MailReceipt BMIFilters
        inputParams.put(MailboxProperty.mailReceiptBmiSpamAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailReceiptBmiSpamAction.name()).add(
                MxosEnums.BmiSpamType.DISCARD.name());

        inputParams.put(MailboxProperty.spamMessageIndicator.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.spamMessageIndicator.name()).add(
                "X-Header");

        // MailReceipt CommtouchFilters
        inputParams.put(MailboxProperty.mailReceiptCommtouchSpamAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailReceiptCommtouchSpamAction.name())
                .add(MxosEnums.CommtouchActionType.ALLOW.name());

        inputParams.put(MailboxProperty.mailReceiptCommtouchVirusAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailReceiptCommtouchVirusAction.name())
                .add(MxosEnums.CommtouchActionType.DISCARD.name());

        // MailReceipt CloudmarkFilters
        inputParams.put(MailboxProperty.spamfilterEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.spamfilterEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.spamPolicy.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.spamPolicy.name()).add(
                MxosEnums.SpamPolicy.AGGRESSIVE.name());

        inputParams.put(MailboxProperty.mailReceiptCloudmarkSpamAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailReceiptCloudmarkSpamAction.name())
                .add(MxosEnums.CloudmarkActionType.DELETE.name());

        inputParams.put(MailboxProperty.cleanAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.cleanAction.name()).add(
                MxosEnums.CloudmarkActionType.DELETE.name());

        inputParams.put(MailboxProperty.suspectAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.suspectAction.name()).add(
                MxosEnums.CloudmarkActionType.DELETE.name());

        // MailReceipt McAfeeFilters
        inputParams.put(MailboxProperty.mailReceiptMcAfeeVirusAction.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mailReceiptMcAfeeVirusAction.name())
                .add(MxosEnums.McAfeeVirusActionType.DISCARD.name());

        // MailForward Multi
        inputParams.put(MailboxProperty.forwardingAddress.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.forwardingAddress.name()).add(
                "bar@foo.com");
        inputParams.get(MailboxProperty.forwardingAddress.name()).add(
                "xyz@foo.com");

        // AllowedSendersList Multi
        inputParams.put(MailboxProperty.allowedSender.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.allowedSender.name())
                .add("bar@foo.com");
        inputParams.get(MailboxProperty.allowedSender.name())
                .add("xyz@foo.com");

        // BlockedSendersList Multi
        inputParams.put(MailboxProperty.blockedSender.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.blockedSender.name())
                .add("xyz@bar.com");
        inputParams.get(MailboxProperty.blockedSender.name())
                .add("foo@bar.com");

        // AddressForLocalDelivery Multi
        inputParams.put(MailboxProperty.addressForLocalDelivery.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.addressForLocalDelivery.name()).add(
                "xyz@bar.com");
        inputParams.get(MailboxProperty.addressForLocalDelivery.name()).add(
                "foo@bar.com");

        // SieveBlockedSenders Multi
        inputParams.put(MailboxProperty.sieveFilterBlockedSender.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.sieveFilterBlockedSender.name())
                .add("xyz@bar.com");
        inputParams.get(MailboxProperty.sieveFilterBlockedSender.name())
                .add("foo@bar.com");

        // MailAccess
        inputParams.put(MailboxProperty.popAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.popAccessType.name()).add(
                MxosEnums.AccessType.TRUSTED.name());

        inputParams.put(MailboxProperty.popAuthenticationType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.popAuthenticationType.name()).add("0");

        inputParams.put(MailboxProperty.popSSLAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.popSSLAccessType.name()).add(
                MxosEnums.AccessType.TRUSTED.name());

        inputParams.put(MailboxProperty.imapAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.imapAccessType.name()).add(
                MxosEnums.AccessType.TRUSTED.name());

        inputParams.put(MailboxProperty.imapSSLAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.imapSSLAccessType.name()).add(
                MxosEnums.AccessType.TRUSTED.name());

        inputParams.put(MailboxProperty.smtpAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smtpAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.smtpSSLAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smtpSSLAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.smtpAuthenticationEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smtpAuthenticationEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.webmailAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailAccessType.name()).add(
                MxosEnums.AccessType.ALL.name());

        inputParams.put(MailboxProperty.webmailSSLAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailSSLAccessType.name()).add(
                MxosEnums.AccessType.ALL.name());

        inputParams.put(MailboxProperty.mobileMailAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMailAccessType.name()).add(
                MxosEnums.AccessType.ALL.name());

        inputParams.put(MailboxProperty.mobileActiveSyncAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileActiveSyncAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        // MailAccess AllowedIp Multi
        inputParams.put(MailboxProperty.allowedIP.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.allowedIP.name()).add("10.10.10.10");
        inputParams.get(MailboxProperty.allowedIP.name()).add("10.10.10.11");

        // MailStore
        inputParams.put(MailboxProperty.maxMessages.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxMessages.name()).add("100");

        inputParams.put(MailboxProperty.maxStorageSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxStorageSizeKB.name()).add("100");
        
        inputParams.put(MailboxProperty.maxMessagesSoftLimit.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxMessagesSoftLimit.name()).add("100");

        inputParams.put(MailboxProperty.maxStorageSizeKBSoftLimit.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxStorageSizeKBSoftLimit.name()).add("100");
        
        inputParams.put(MailboxProperty.mobileMaxMessages.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMaxMessages.name()).add("100");

        inputParams.put(MailboxProperty.mobileMaxStorageSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMaxStorageSizeKB.name()).add("100");
        
        inputParams.put(MailboxProperty.mobileMaxMessagesSoftLimit.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMaxMessagesSoftLimit.name()).add("100");

        inputParams.put(MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name()).add("100");
                
        inputParams.put(MailboxProperty.quotaWarningThreshold.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.quotaWarningThreshold.name()).add("50");

        inputParams.put(MailboxProperty.quotaBounceNotify.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.quotaBounceNotify.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.folderQuota.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.folderQuota.name()).add("read");

        // ExternalStore
        inputParams.put(MailboxProperty.externalStoreAccessAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.externalStoreAccessAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.maxExternalStoreSizeMB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxExternalStoreSizeMB.name()).add(
                "1000");

        // SmsServices
        inputParams.put(MailboxProperty.smsServicesAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.smsServicesMsisdn.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdn.name()).add(
                "+919000000000");

        inputParams.put(MailboxProperty.smsServicesMsisdnStatus.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsServicesMsisdnStatus.name()).add(
                MxosEnums.MsisdnStatus.ACTIVATED.name());

        inputParams.put(
                MailboxProperty.lastSmsServicesMsisdnStatusChangeDate.name(),
                new ArrayList<String>());
        inputParams.get(
                MailboxProperty.lastSmsServicesMsisdnStatusChangeDate.name())
                .add("2013-02-14T07:32:33Z");

        // SmsOnline
        inputParams.put(MailboxProperty.smsOnlineEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsOnlineEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.internationalSMSAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.internationalSMSAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.internationalSMSEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.internationalSMSEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.maxSMSPerDay.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxSMSPerDay.name()).add("100");

        inputParams.put(MailboxProperty.concatenatedSMSAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.concatenatedSMSAllowed.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.concatenatedSMSEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.concatenatedSMSEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.maxConcatenatedSMSSegments.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxConcatenatedSMSSegments.name()).add(
                "20");

        inputParams.put(MailboxProperty.maxPerCaptchaSMS.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxPerCaptchaSMS.name()).add("20");

        inputParams.put(MailboxProperty.maxPerCaptchaDurationMins.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxPerCaptchaDurationMins.name()).add(
                "20");

        // SmsNotifications
        inputParams.put(MailboxProperty.smsBasicNotificationsEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsBasicNotificationsEnabled.name())
                .add(MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.smsAdvancedNotificationsEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smsAdvancedNotificationsEnabled.name())
                .add(MxosEnums.BooleanType.YES.name());

        // InternalInfo
        inputParams.put(MailboxProperty.webmailVersion.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.webmailVersion.name()).add("5");

        inputParams.put(MailboxProperty.selfCareAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.selfCareAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.selfCareSSLAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.selfCareSSLAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.messageStoreHost.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.messageStoreHost.name()).add(
                "rwcvmx83c0100");

        inputParams.put(MailboxProperty.smtpProxyHost.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.smtpProxyHost.name()).add("localhost");

        inputParams.put(MailboxProperty.imapProxyHost.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.imapProxyHost.name()).add("localhost");

        inputParams.put(MailboxProperty.popProxyHost.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.popProxyHost.name()).add("localhost");

        inputParams.put(MailboxProperty.autoReplyHost.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.autoReplyHost.name()).add("localhost");
        
        inputParams.put(MailboxProperty.realm.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.realm.name()).add("mailRealm");

        inputParams.put(MailboxProperty.imapProxyAuthenticationEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.imapProxyAuthenticationEnabled.name())
                .add(MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.remoteCallTracingEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.remoteCallTracingEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.interManagerAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.interManagerAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.interManagerSSLAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.interManagerSSLAccessEnabled.name())
                .add(MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.voiceMailAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.voiceMailAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.faxAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.faxAccessEnabled.name()).add(
                MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.ldapUtilitiesAccessType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.ldapUtilitiesAccessType.name()).add(
                MxosEnums.AccessType.ALL.name());

        inputParams.put(MailboxProperty.addressBookProvider.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.addressBookProvider.name()).add(
                MxosEnums.AddressBookProviderType.PLAXO.name());

        // MessageEventRecords
        inputParams.put(MailboxProperty.eventRecordingEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.eventRecordingEnabled.name()).add(
                MxosEnums.EventRecordingType.ALWAYS.name());

        inputParams.put(MailboxProperty.maxHeaderLength.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxHeaderLength.name()).add("20");

        inputParams.put(MailboxProperty.maxFilesSizeKB.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxFilesSizeKB.name()).add("100");

        // SocialNetworks
        inputParams.put(MailboxProperty.socialNetworkIntegrationAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.socialNetworkIntegrationAllowed.name())
                .add(MxosEnums.BooleanType.YES.name());

        // SocialNetworkSite
        inputParams.put(MailboxProperty.socialNetworkSiteAccessEnabled.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.socialNetworkSiteAccessEnabled.name())
                .add(MxosEnums.BooleanType.YES.name());

        // ExternalAccount
        inputParams.put(MailboxProperty.externalMailAccountsAllowed.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.externalMailAccountsAllowed.name())
                .add(MxosEnums.BooleanType.YES.name());

        inputParams.put(MailboxProperty.promptForExternalAccountSync.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.promptForExternalAccountSync.name())
                .add(MxosEnums.BooleanType.NO.name());
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxBadRequest() {
        System.out.println("MailboxCreateTest.testCreateMailboxBadRequest...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        //inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(FIRSTNAME_KEY, new ArrayList<String>());
        inputParams.get(FIRSTNAME_KEY).add(FIRSTNAME);
        inputParams.put(LASTNAME_KEY, new ArrayList<String>());
        inputParams.get(LASTNAME_KEY).add(LASTNAME);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }
    @Test
    public void testCreateMailboxWithPreEncryptPwd() {
        System.out.println("testCreateMailboxWithPreEncryptPwd...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add(PWD_STORE_TYPE);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    @Test
    public void testCreateMailboxWithPreEncryptPwdNoPwdStoreType() {
        System.out.println("testCreateMailboxWithPreEncryptPwdNoPwdStoreType...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    @Test
    public void testCreateMailboxWithPreEncryptPwdNeg() {
        System.out.println("testCreateMailboxWithPreEncryptPwdNeg...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add("unix");
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add(PRE_ENCYRPT_PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Exception Happened ", true);
        }
    }
    @Test
    public void testCreateMailboxWithPreEncryptPwdAsFalse() {
        System.out.println("testCreateMailboxWithPreEncryptPwdAsFalse...");
        MailboxHelper.deleteMailbox(EMAIL);
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(PWD_STORE_TYPE_KEY, new ArrayList<String>());
        inputParams.get(PWD_STORE_TYPE_KEY).add("unix");
        inputParams.put(PRE_ENCYRPT_PWD_KEY, new ArrayList<String>());
        inputParams.get(PRE_ENCYRPT_PWD_KEY).add("false");
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testInvalidNotificationMailSentStatus() {
        System.out.println("testInvalidNotificationMailSentStatus...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(),
                new ArrayList<String>());
        // inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add("");
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add(
                null);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
    @Test
    public void testInvalidLastSuccessfulLoginDate() {
        System.out.println("testWrongLastSuccessfulLoginDate...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        inputParams.put(MailboxProperty.lastSuccessfulLoginDate.name(),
                new ArrayList<String>());
        //inputParams.get(MailboxProperty.lastSuccessfulLoginDate.name()).add(null);
        inputParams.get(MailboxProperty.lastSuccessfulLoginDate.name()).add("abcd");
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
    
    @Test
    public void testEmptyAddressBookValues() {
        System.out.println("testEmptyAddressBookValues...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        inputParams.put(MailboxProperty.maxContacts.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxContacts.name()).add(null);
        inputParams.put(MailboxProperty.maxGroups.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxGroups.name()).add(null);
        inputParams.put(MailboxProperty.maxContactsPerGroup.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxContactsPerGroup.name()).add(null);
        inputParams.put(MailboxProperty.maxContactsPerPage.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxContactsPerPage.name()).add(null);
        inputParams.put(
                MailboxProperty.createContactsFromOutgoingEmails.name(),
                new ArrayList<String>());
        inputParams
                .get(MailboxProperty.createContactsFromOutgoingEmails.name())
                .add(null);
        try {
            mailboxService.create(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println("Message : " + e.getCode());
        }
    }
    
    @Test
    public void testCreateMailboxWithExtensionAttributesSuccess() {
        System.out.println("testCreateMailboxWithExtensionAttributesSuccess...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(EMAIL_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS1);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        inputParams.put(MailboxProperty.extensionAttributes.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.extensionAttributes.name()).add(
                "postalAddress:openwave|telephoneNumber:9886123456");
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithExtensionAttributesFailure() {
        System.out.println("testCreateMailboxWithExtensionAttributesFailure...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        inputParams.put(EMAIL_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_ADDRESS_KEY).add(ALIAS1);
        inputParams.put(FROM_ADDRESS_KEY, new ArrayList<String>());
        inputParams.get(FROM_ADDRESS_KEY).add(FROM_ADDRESS);
        inputParams.put(MailboxProperty.extensionAttributes.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.extensionAttributes.name()).add(
                "postalAddress-openwave,telephoneNumber-9886123456");
        try {
            mailboxService.create(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesNullValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                null);
        String mailboxid ;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesEmptyValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "");
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesAlphaNumericValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "12a");
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesSpecialCharsValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "&*)#");
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesMinLimitValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "-1");
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesMaxLimitValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                "2147483648");
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            fail() ;
        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
                assertNotNull("Invalid value for maxforwardingaddresses",e);
            }
            else {
                fail() ;
            }
            e.printStackTrace();
            
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testCreateMailboxWithMaxForwardAddressesCorrectValue() {
        System.out.println("testCreateMailboxWithMaxForwardAddressesNullValue...");
        try {
            MailboxHelper.deleteMailbox(EMAIL2);
        } catch (Exception e) {
           
        }
        
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL2);
        inputParams.put(PWD_KEY, new ArrayList<String>());
        inputParams.get(PWD_KEY).add(PWD);
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(COSID);
        Integer value = 7 ;
        inputParams.put(MailboxProperty.maxNumForwardingAddresses.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumForwardingAddresses.name()).add(
                value.toString());
        String mailboxid;
        try {
            mailboxid = mailboxService.create(inputParams);
            assertNotNull("Mailbox id should not be null", mailboxid);
            System.out.println("mailbox id is " + mailboxid.toString());
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL2);
            IMailReceiptService mailReceiptService = (IMailReceiptService) context
            .getService(ServiceEnum.MailReceiptService.name());
            MailReceipt mailreceipt = mailReceiptService.read(inputParams);
            Integer val = mailreceipt.getMaxNumForwardingAddresses();
            assertEquals("Has a wrong value.", value, val);

        } catch (MxOSException e) {
            if (e.getCode().equalsIgnoreCase(
                    MailboxError.MBX_INVALID_MAXNUM_OF_FWDADDRSS.name())) {
                System.out.println("Invalid value for maxforwardingaddresses");
            }
            fail();
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
            fail();
        } finally {
            try {
                MailboxHelper.deleteMailbox(EMAIL2);
            } catch (Exception e) {

            }
        }
        
    }
}
