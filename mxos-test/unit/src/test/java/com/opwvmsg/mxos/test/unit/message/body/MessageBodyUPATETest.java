package com.opwvmsg.mxos.test.unit.message.body;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IBodyService;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.test.unit.ContextUtils;

public class MessageBodyUPATETest {

    private static final String TEST_NAME = "MessageBodyUPATETest";
    private static final String ARROW_SEP = " --> ";

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "user1@rwc.nis";
    private static final String RECEIVED_FROM_EMAIL_KEY = MxOSPOJOs.receivedFrom
            .name();
    private static final String RECEIVED_FROM_EMAIL = "user2@rwc.nis";
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "INBOX";
    private static final String EMAIL_MESSAGE_KEY = MessageProperty.message
            .name();
    private static final String MESSAGE_VALUE = "Subject:Test Message 25 apr - 1\r\n"
            + "this is " + "first message 25 apr - 1";
    private static final String UPDATE_MESSAGE_VALUE = "Subject: hi\r\n\r\nhello agn 11";
    private static final String MESSAGEID_KEY = MessageProperty.messageId
            .name();
    private static IMxOSContext context;
    private static IMessageService messageService;
    private static IBodyService bodyService;
    private static IMetadataService metaDataService;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        String mxosHome = System.getenv("MXOS_HOME");
        System.setProperty("MXOS_HOME", mxosHome);
        context = ContextUtils.loadContext();
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        bodyService = (IBodyService) context.getService(ServiceEnum.BodyService
                .name());
        metaDataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        messageService = null;
        bodyService = null;
        System.clearProperty("MXOS_HOME");
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testUpdateMessageid() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateMessageid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, messageId);
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);
            
            

        } catch (MxOSException e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateMessageInvalidMessageId() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageInvalidMessageId");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);
          
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, "asd");
            bodyService.update(inputParams2);
            
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }

    @Test
    public void testUpdateMessageIdNoEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testUpdateMessageIdNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, messageId);
            bodyService.update(inputParams2);

            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testUpdateMessageIdNoFloder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageIdNoFloder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, messageId);
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        } catch (Exception e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e);

        }
    }

    @Test
    public void testUpdateMessageNoMessageId() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageNoMessageId");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }

    @Test
    public void testUpdateMessageUid() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateMessageUid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGEID_KEY, messageId);
            Metadata metaData = metaDataService.read(inputParams1);
            Long uid = metaData.getUid();

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, uid.toString());
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    @Test
    public void testUpdateMessageInvalidUid() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageInvalidUid");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGEID_KEY, messageId);
            Metadata metaData = metaDataService.read(inputParams1);

            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, "asd");
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }

    @Test
    public void testUpdateMessageUidNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageUidNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGEID_KEY, messageId);
            Metadata metaData = metaDataService.read(inputParams1);
            Long uid = metaData.getUid();
           
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, uid.toString());
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testUpdateMessageUidNoFloder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateMessageUidNoFloder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGEID_KEY, messageId);
            Metadata metaData = metaDataService.read(inputParams1);
            Long uid = metaData.getUid();
            
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            addParams(inputParams2, MESSAGEID_KEY, uid.toString());
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        } catch (Exception e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e);

        }
    }

    @Test
    public void testUpdateMessageNoUId() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateMessageNoUId");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            inputParams.clear();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, EMAIL_MESSAGE_KEY, MESSAGE_VALUE);
            addParams(inputParams, RECEIVED_FROM_EMAIL_KEY, RECEIVED_FROM_EMAIL);
            String messageId = messageService.create(inputParams);

            Map<String, List<String>> inputParams1 = new HashMap<String, List<String>>();
            addParams(inputParams1, EMAIL_KEY, EMAIL);
            addParams(inputParams1, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams1, MESSAGEID_KEY, messageId);
            Metadata metaData = metaDataService.read(inputParams1);
           
            Map<String, List<String>> inputParams2 = new HashMap<String, List<String>>();
            addParams(inputParams2, EMAIL_KEY, EMAIL);
            addParams(inputParams2, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams2, EMAIL_MESSAGE_KEY, UPDATE_MESSAGE_VALUE);
            bodyService.update(inputParams2);
            
            Map<String, List<String>> inputParams3 = new HashMap<String, List<String>>();
            addParams(inputParams3, EMAIL_KEY, EMAIL);
            addParams(inputParams3, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams3, MESSAGEID_KEY, messageId);
            messageService.delete(inputParams3);

        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
    }

}
