package com.opwvmsg.mxos.test.unit.externalAccount;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

public class ExternalAccountsTest {
    private static IExternalAccountsService ieas = null;
    private static Map <String, List<String> >inputParams = new HashMap<String, List<String>>();
   
    private static final String EMAIL = "testExtAcct@openwave.com";
    private static final String PASSWORD = "Password1";
    private static final String COSID = "cos_2.0_ut200";
   
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp starting......");
        ieas = (IExternalAccountsService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalAccountService.name());
        assertNotNull(ieas);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COSID);
        //assertTrue("Mailbox was not created.", id != -1);
       
    }
    
    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("tearDownAfterClass...........");
        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);
        ieas = null;
        
    }
   
    @Test
    public void testExternalAccounts() {
    	
    	//1) READ
    	System.out.println("testExternalAccountsRead starting ...");
    	testExternalAccountsRead();
    	System.out.println("testExternalAccountsRead end ...");
    	
    	//1) UPDATE
    	System.out.println("testExternalAccountsAllParamUpdate starting ...");
    	testExternalAccountsAllParamUpdate();
    	System.out.println("testExternalAccountsAllParamUpdate end ...");
    	
    	
    	System.out.println("testExternalAccountsPartialParamUpdate starting ...");
    	testExternalAccountsPartialParamUpdate();
    	System.out.println("testExternalAccountsPartialParamUpdate end ...");
    	
    	System.out.println("testExternalAccounts2 starting ...");    	
    	testExternalAccounts2();
    	System.out.println("testExternalAccounts2 end ...");
    	
    	System.out.println("testExternalUpdateNeg starting ...");
    	testExternalUpdateNeg();
    	System.out.println("testExternalUpdateNeg end ...");
    }

    public void testExternalAccountsRead() {
        System.out.println("testExternalAccountsRead starting ...");
        inputParams.clear();
        inputParams.put("email",new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        try {
        ExternalAccounts ea = ieas.read(inputParams);
        assertNotNull(ea);
        } catch (Exception e) {
            System.out.println("Exception: "+e);
            fail();
        }
    }
    
 
    public void testExternalAccountsAllParamUpdate() {
        System.out.println("testExternalAccountsAllParamUpdate starting");
        inputParams.clear();
        inputParams.put("email",new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("externalMailAccountsAllowed",new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("yes");
        inputParams.put("promptForExternalAccountSync",new ArrayList<String>());
        inputParams.get("promptForExternalAccountSync").add("no");
        
        try {
            ieas.update(inputParams);
            inputParams.clear();
            inputParams.put("email",new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            ExternalAccounts ea  = ieas.read(inputParams);
            assertNotNull(ea.getExternalMailAccountsAllowed());
            assertNotNull( ea.getPromptForExternalAccountSync());
        } catch(Exception e) {
            System.out.println("Exception: "+ e);
            fail();
        }
    }
  
    public void testExternalAccountsPartialParamUpdate() {
        System.out.println("testExternalAccountsPartialParamUpdate starting");
        inputParams.clear();
        inputParams.put("email",new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("externalMailAccountsAllowed",new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("no");
        
        try {
            ieas.update(inputParams);
            inputParams.clear();
            inputParams.put("email",new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            ExternalAccounts ea  = ieas.read(inputParams);
            assertNotNull(ea.getExternalMailAccountsAllowed());
            } catch(Exception e) {
            System.out.println("Exception: "+ e);
            fail();
        }
    }
    
    public void testExternalAccounts2() {
        System.out.println("testExternalAccounts2 starting ...");
        inputParams.clear();
        inputParams.put("email",new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("externalMailAccountsAllowed",new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("no");
        inputParams.put("promptForExternalAccountSync",new ArrayList<String>());
        inputParams.get("promptForExternalAccountSync").add("yes");
      try {
            ieas.update(inputParams);
            inputParams.clear();
            inputParams.put("email",new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            ExternalAccounts ea  = ieas.read(inputParams);
            System.out.println("ex ma acc all"+ea.getExternalMailAccountsAllowed());
            assertNotNull("externalMailAccountsAllowed is null", ea.getExternalMailAccountsAllowed());
            assertNotNull("promptForExternalAccountSync is null", ea.getPromptForExternalAccountSync());  
      } catch(Exception e) {
            System.out.println("Exception: "+ e);
            fail();
        }
    }

   
    public void testExternalUpdateNeg() {
        System.out.println("testExternalUpdateNeg starting");
        inputParams.clear();
        inputParams.put("email",new ArrayList<String>());
        inputParams.get("email").add(EMAIL);
        inputParams.put("externalMailAccountsAllowed",new ArrayList<String>());
        inputParams.get("externalMailAccountsAllowed").add("INVALID");
        try {
            ieas.update(inputParams);
            inputParams.clear();
            inputParams.put("email",new ArrayList<String>());
            inputParams.get("email").add(EMAIL);
            ieas.read(inputParams);
            fail();
        } catch(MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),MailboxError.MBX_INVALID_EXTERNAL_ACCOUNTS_ALLOWED.toString());
        }
    }
}
