package com.opwvmsg.mxos.test.unit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.io.IOUtils;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContextUtils {
    private static IMxOSContext context;

    public static IMxOSContext loadContext() throws Exception {
        final String contextId = "UNIT-MXOS-2.0";
        if (context == null) {
            // To get MxOS Context object
            Properties p = new Properties();
            // for BACKEND
            /*
            p.setProperty(MxOSContextFactory.MXOS_CONTEXT_TYPE,
                    ContextEnum.BACKEND.name());
            */
            // for STUB
            /*
            p.setProperty(MxOSContextFactory.MXOS_CONTEXT_TYPE,
                    ContextEnum.STUB.name());
            */
            // for REST context, add two more below properties.
            p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                    ContextEnum.REST.name());
            p.setProperty(ContextProperty.MXOS_BASE_URL,
                    "http://localhost:8080/mxos");
            p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "200");
            p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
            p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "120000");
            loadMxOSProperties();
            // for logging APIs
            // p.setProperty("custom", "true");
            // get context object
            context = MxOSContextFactory.getInstance().getContext(contextId, p);
        }
        return context;
    }

    /**
     * Method to load mxos.properties.
     * <i>${MXOS_HOME}/config/${app.name}.properties</i>.
     * 
     * @throws MxOSException MxOSException
     */
    private static void loadMxOSProperties() throws MxOSException {

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        Properties config = null;
        InputStream in = null;
        try {
            in = new FileInputStream(home + "/config/mxos.properties");
            config = new Properties();
            try {
                config.load(in);
            } catch (IOException e) {
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        } catch (FileNotFoundException e) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        try {
            Iterator<Object> keys = config.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                System.setProperty(key, config.getProperty(key));
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

}
