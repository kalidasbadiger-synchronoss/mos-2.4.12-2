package com.opwvmsg.mxos.test.unit.message;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SystemFolder;
import com.opwvmsg.mxos.message.search.AndTerm;
import com.opwvmsg.mxos.message.search.ArrivalTimeTerm;
import com.opwvmsg.mxos.message.search.CcTerm;
import com.opwvmsg.mxos.message.search.FromTerm;
import com.opwvmsg.mxos.message.search.NotTerm;
import com.opwvmsg.mxos.message.search.OrTerm;
import com.opwvmsg.mxos.message.search.SearchOperator;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.message.search.SearchTermParser;
import com.opwvmsg.mxos.message.search.SentDateTerm;
import com.opwvmsg.mxos.message.search.SubjectTerm;
import com.opwvmsg.mxos.message.search.ToTerm;
import com.opwvmsg.mxos.message.search.UIDTerm;

public class SearchTermParserTest {

	private static final String FOLDER = SystemFolder.SentMail.name();
	
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testSubjectTerm() {
        try {
            String query = "(subject=hello)";
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new SubjectTerm('=', "hello");
            assertTrue(ref.equals(st));
            
            query = "(subject~hello)";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('~', "hello");
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testToTerm() {
        String query = "(to=test@example.com)";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new ToTerm(SearchOperator.Equals, "test@example.com");
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testFromTerm() {
        String query = "(from=test@example.com)";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new FromTerm(SearchOperator.Equals, "test@example.com");
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testCcTerm() {
        String query = "(cc=test@example.com)";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new CcTerm(SearchOperator.Equals, "test@example.com");
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testAndTerm() {
        String query = "(&(subject=hello)(to=test@example.com))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SubjectTerm subject = new SubjectTerm('=', "hello");
            ToTerm to = new ToTerm(SearchOperator.Equals, "test@example.com");
            SearchTerm ref = new AndTerm(subject, to);
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testNotTerm() {
        String query = "(!(subject=hello))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SubjectTerm subject = new SubjectTerm('=', "hello");
            SearchTerm ref = new NotTerm(subject);
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testOrTerm() {
        String query = "(|(subject=hello)(to=test@example.com))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SubjectTerm subject = new SubjectTerm('=', "hello");
            ToTerm to = new ToTerm(SearchOperator.Equals, "test@example.com");
            SearchTerm ref = new OrTerm(subject, to);
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }

    @Test
    public void testMultipleAndTerm() {
        String query = "(&(subject=hello)(to=to@example.com)(from=from@example.com))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm[] terms = new SearchTerm[3];
            terms[0] = new SubjectTerm('=', "hello");
            terms[1] = new ToTerm(SearchOperator.Equals, "to@example.com");
            terms[2] = new FromTerm(SearchOperator.Equals, "from@example.com");
            SearchTerm ref = new AndTerm(terms);
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testAndOrTerm() {
        String query = "(&(subject=hello)(|(to=to@example.com)(cc=cc@example.com)))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm[] terms = new SearchTerm[2];
            terms[0] = new SubjectTerm('=', "hello");
            terms[1] = new OrTerm(new ToTerm(SearchOperator.Equals,
                    "to@example.com"), new CcTerm(SearchOperator.Equals,
                    "cc@example.com"));
            SearchTerm ref = new AndTerm(terms);
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testNotAndOrTerm() {
        String query = "(!(&(subject=hello)(|(to=to@example.com)(cc=cc@example.com))))";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm[] terms = new SearchTerm[2];
            terms[0] = new SubjectTerm('=', "hello");
            terms[1] = new OrTerm(new ToTerm(SearchOperator.Equals, "to@example.com"), new CcTerm(
                    SearchOperator.Equals, "cc@example.com"));
            SearchTerm ref = new NotTerm(new AndTerm(terms));
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testNoParenthesis() {
        String query = "subject=hello";
        try {
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new SubjectTerm('=', "hello");
            assertTrue(ref.equals(st));
        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testSpecialCharacters() {
        String query;
        try {
            query = "subject=(a\\\\bc\\)";
            SearchTerm st = SearchTermParser.parse(query);
            SearchTerm ref = new SubjectTerm('=', "(a\\bc)");
            assertTrue(query, ref.equals(st));
            
            query = "subject=\\\\abc";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "\\abc");
            assertTrue(query, ref.equals(st));
            
            query = "subject=\\)abc\\\\";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', ")abc\\");
            assertTrue(query, ref.equals(st));

            query = "subject==";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "=");
            assertTrue(query, ref.equals(st));

            query = "subject=~";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "~");
            assertTrue(query, ref.equals(st));

            query = "subject=&";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "&");
            assertTrue(query, ref.equals(st));

            query = "subject=|";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "|");
            assertTrue(query, ref.equals(st));

            query = "subject=!";
            st = SearchTermParser.parse(query);
            ref = new SubjectTerm('=', "!");
            assertTrue(query, ref.equals(st));

        } catch (Exception e) {
            e.printStackTrace();
            fail("exception");
        }
    }
    
    @Test
    public void testArrivalTimeTerm() throws Exception {
    	String query = "(arrivalTime=1411376339)";
    	SearchTerm st = SearchTermParser.parse(query);
        SearchTerm ref = new ArrivalTimeTerm('=', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(arrivalTime>1411376339)";
    	st = SearchTermParser.parse(query);
        ref = new ArrivalTimeTerm('>', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(arrivalTime<1411376339)";
    	st = SearchTermParser.parse(query);
        ref = new ArrivalTimeTerm('<', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(arrivalTime=0)";
    	st = SearchTermParser.parse(query);
        ref = new ArrivalTimeTerm('=', "0");
        assertTrue(ref.equals(st));
        
        query = "(arrivalTime<-1411376339)";
    	st = SearchTermParser.parse(query);
        ref = new ArrivalTimeTerm('<', "-1411376339");
        assertTrue(ref.equals(st));
    }

    @Test
    public void testSentDateTerm() throws Exception {
    	String query = "(sentDate=1411376339)";
    	SearchTerm st = SearchTermParser.parse(query, FOLDER);
        SearchTerm ref = new SentDateTerm('=', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(sentDate=1411376339)";
        assertNull("Should be null if the folder parameter is not SentMail", SearchTermParser.parse(query));
        
        query = "(sentDate>1411376339)";
    	st = SearchTermParser.parse(query, FOLDER);
        ref = new SentDateTerm('>', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(sentDate<1411376339)";
    	st = SearchTermParser.parse(query, FOLDER);
        ref = new SentDateTerm('<', "1411376339");
        assertTrue(ref.equals(st));
        
        query = "(sentDate=0)";
    	st = SearchTermParser.parse(query, FOLDER);
        ref = new SentDateTerm('=', "0");
        assertTrue(ref.equals(st));
        
        query = "(sentDate<-1411376339)";
    	st = SearchTermParser.parse(query, FOLDER);
        ref = new SentDateTerm('<', "-1411376339");
        assertTrue(ref.equals(st));
    }
    
    @Test(expected = NumberFormatException.class)
    public void testSentDateTermWithNonDigitChars() throws Exception {
    	String query = "(sentDate=1411376339a)";
    	SearchTermParser.parse(query, FOLDER);
    }
    
    @Test
    public void testGreaterThanOrEqualOperator() throws Exception {
    	String query = "(sentDate>=1411376339)";
    	SearchTerm st = SearchTermParser.parse(query, FOLDER);
        SearchTerm ref = new SentDateTerm('}', "1411376339");
        assertTrue(ref.equals(st));
    }
    
    @Test
    public void testLessThanOrEqualOperator() throws Exception {
    	String query = "(subject<=)";
    	SearchTerm st = SearchTermParser.parse(query);
        SearchTerm ref = new SubjectTerm('{', "");
        assertTrue(ref.equals(st));
    }
    
    @Test
    public void testUIDTerm() throws Exception {
    	String query = "(uid=1000)";
    	SearchTerm st = SearchTermParser.parse(query);
        SearchTerm ref = new UIDTerm('=', "1000");
        assertTrue(ref.equals(st));
        
        query = "(uid^\\\\d{4})";
    	st = SearchTermParser.parse(query);
        ref = new UIDTerm('^', "\\d{4}");
        assertTrue(ref.equals(st));
        
        query = "(uid^^\\\\d{4}$)";
    	st = SearchTermParser.parse(query);
        ref = new UIDTerm('^', "^\\d{4}$");
        assertTrue(ref.equals(st));
    }

}
