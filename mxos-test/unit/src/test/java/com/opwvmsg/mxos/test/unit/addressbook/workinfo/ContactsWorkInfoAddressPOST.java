package com.opwvmsg.mxos.test.unit.addressbook.workinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.Address;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoAddressService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsWorkInfoAddressPOST {

    private static final String TEST_NAME = "ContactWorkInfoAddressPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsWorkInfoAddressService contactsWorkInfoAddressService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static Address getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static Address getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        Address info = null;
        try {
            info = contactsWorkInfoAddressService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("Work Info Address object is null.", info);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
            }
        }
        return info;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    private static void setParams(Map<String, List<String>> params) {
        setParams(params, null);
    }

    private static void setParams(Map<String, List<String>> params,
            AddressBookException expectedError) {

        try {
            contactsWorkInfoAddressService.update(params);

            params.clear();
            params.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.userId.name()).add(USERID);

            params.put(AddressBookProperty.contactId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.contactId.name()).add(
                    String.valueOf(CONTACTID));

            if (session != null) {
                params.put(AddressBookProperty.sessionId.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.sessionId.name()).add(
                        session.getSessionId());

                params.put(AddressBookProperty.cookieString.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.cookieString.name()).add(
                        session.getCookieString());
            }
            Address address = getParams(params);

            assertNotNull("Street Is null.", address.getStreet());
            assertNotNull("Country Is null.", address.getCountry());
            assertNotNull("Postalcode Is null.", address.getPostalCode());
            assertNotNull("City Is null.", address.getCity());
            assertNotNull("StateOrProvince Is null.",
                    address.getStateOrProvince());

            assertEquals("Updated Street value is not equal.", "TESTSTREET",
                    address.getStreet());
            assertEquals("Updated Country value is not equal.", "TESTCOUNTRY",
                    address.getCountry());
            assertEquals("Updated Postalcode value is not equal.",
                    "TESTPOSTALCODE", address.getPostalCode());
            assertEquals("Updated City value is not equal.", "TESTCITY",
                    address.getCity());
            assertEquals("Updated StateOrProvince value is not equal.",
                    "TESTSTATE", address.getStateOrProvince());

        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }

    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsWorkInfoAddressService = (IContactsWorkInfoAddressService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.ContactsWorkInfoAddressService.name());
        login(USERID, PASSWORD);
        
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsWorkInfoAddressService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // base post
    @Test
    public void testContactsWorkInfoAddress() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP
                + "testContactsWorkInfoAddress");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        params.put("street", new ArrayList<String>());
        params.get("street").add("TESTSTREET");

        params.put("city", new ArrayList<String>());
        params.get("city").add("TESTCITY");

        params.put("stateOrProvince", new ArrayList<String>());
        params.get("stateOrProvince").add("TESTSTATE");

        params.put("postalCode", new ArrayList<String>());
        params.get("postalCode").add("TESTPOSTALCODE");

        params.put("country", new ArrayList<String>());
        params.get("country").add("TESTCOUNTRY");

        setParams(params);

    }
}
