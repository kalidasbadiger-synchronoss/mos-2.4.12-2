package com.opwvmsg.mxos.test.unit.process;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
    DeactivateMSISDNServiceTest.class,
    ReactivateMSISDNServiceTest.class,
    SwapMSISDNServiceTest.class
})
public class FionaProcessTestSuite {
}
