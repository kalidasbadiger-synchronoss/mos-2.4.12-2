package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to Contacts
@SuiteClasses({ ContactsBaseGET.class, ContactsBaseGETALL.class,
        ContactsBasePOST.class, ContactsCreateAndDelete.class,
        ContactsCreateAndMoveMultiple.class, ContactsImageGET.class,
        ContactsImagePOST.class, ContactsListGET.class, ContactsNameGET.class,
        ContactsNamePOST.class })
public class ContactsTestSuite {
}
