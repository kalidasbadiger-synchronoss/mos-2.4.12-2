package com.opwvmsg.mxos.test.unit.message.metadata;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IMessageService;
import com.opwvmsg.mxos.interfaces.service.message.IMetadataService;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ListMessageMetaDataTest {

    private static final String TEST_NAME = "ListMessageMetaDataTest";
    private static final String ARROW_SEP = " --> ";

    private static final String SORT_KEY = MailboxProperty.sortKey.name();
    private static final String SORT_KEY_MESSAGE_ID = "messageId";
    private static final String SORT_KEY_HAS_ATTACHMENTS = "hasAttachments";
    private static final String SORT_KEY_RICHMAIL_FLAG = "richMailFlag";
    private static final String SORT_KEY_PRIORITY = "priority";
    private static final String SORT_KEY_UID = "uid";
    private static final String SORT_KEY_SIZE = "size";
    private static final String SORT_KEY_SUBJECT = "subject";
    
    private static final String SORT_ORDER_KEY = MailboxProperty.sortOrder.name();
    private static final String SORT_ORDER_ASC = "ascending";
    private static final String SORT_ORDER_DESC = "descending";
    
    private static final String POP_DELETED_FLAG_KEY = MessageProperty.popDeletedFlag.name();
    private static final String POP_DELETED_FLAG_TRUE = "true";
    private static final String POP_DELETED_FLAG_FALSE = "false";
    
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "shankar@openwave.com";
    
    
    private static final String PASSWORD = "test123";
    private static final String FROMADDRESS_KEY = MxOSPOJOs.receivedFrom.name();
    private static final String FROMADDRESS = "messageMetaDataListFrom@openwave.com";

    private static final String MESSAGE_KEY = SmsProperty.message.name();
    private static final String MESSAGE_VALUE = "Hi, Sending test mails";
    private static final String FOLDERNAME_KEY = FolderProperty.folderName
            .name();
    private static final String FOLDERNAME_VALUE = "SentMail";
    private static final String FOLDERNAME_VALUE_1 = "Trash";
    private static final String FOLDERNAME_VALUE_INVALID = "INBOX1";    

    private static IMxOSContext context;
    private static IMetadataService metadataService;
    private static IMessageService messageService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static final String COS_ID = "default";

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        MailboxHelper.createMailbox(EMAIL, PASSWORD, COS_ID);
        MailboxHelper.createMailbox(FROMADDRESS, PASSWORD, COS_ID);
        metadataService = (IMetadataService) context
                .getService(ServiceEnum.MetadataService.name());
        messageService = (IMessageService) context
                .getService(ServiceEnum.MessageService.name());
        sendMail();
        sendMail();
        sendMail();
        sendMail();
    }

    private static void sendMail() {
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, FROMADDRESS_KEY, FROMADDRESS);
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, MESSAGE_KEY, MESSAGE_VALUE);
            messageService.create(inputParams);
        } catch (Exception e) {
            assertFalse("Exception Occured...", true);
            e.printStackTrace();
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("message metadata object is null.", metadataService);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL);
        MailboxHelper.deleteMailbox(FROMADDRESS);
        params.clear();
        params = null;
        metadataService = null;
        messageService = null;
    }

    public static void addParams(Map<String, List<String>> inputParams,
            String Key, String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testListMessageMetaDataNoEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataNoEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testListMessageMetaDataInvalidEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataInvalidEmail");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, "krishan@@@domain1.co.in");
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MailboxError.MBX_INVALID_EMAIL.name(), e.getCode());
        }
    }
    
    @Test
    public void testListMessageMetaDataNoFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataNoFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testListMessageMetaDataInvalidFolder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataInvalidFolder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_INVALID);
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    FolderError.FLD_NOT_FOUND.name(), e.getCode());
        }
    }
    
    @Test
    public void testListMessageMetaDataInvalidSortKey() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataInvalidSortKey");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, "junk");
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);            
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_SORT_KEY.name(), e.getCode());
        }
    }
    
    @Test
    public void testListMessageMetaDataInvalidSortOrder() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataInvalidSortOrder");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_MESSAGE_ID);
            addParams(inputParams, SORT_ORDER_KEY, "junk");            
            Map<String, Metadata> message = metadataService
                    .list(inputParams);
            fail("This should not have been come!!!");
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    MessageError.MSG_INVALID_SORT_ORDER.name(), e.getCode());
        }
    }
    
    @Test
    public void testListMessageMetaDataSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataSuccess");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);

            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testListMessageMetaDataNoMessageSuccess() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataNoMessageSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        try {
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE_1);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Message metadata found",
                    (metadataList.size() == 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }

    @Test
    public void testListMessageMetaDataWithOptionalParameters() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_MESSAGE_ID);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters1() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters1");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_UID);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_ASC);
            addParams(inputParams, POP_DELETED_FLAG_KEY, POP_DELETED_FLAG_FALSE);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters2() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters2");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_SIZE);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters3() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters3");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_HAS_ATTACHMENTS);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters4() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters4");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_PRIORITY);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters5() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters5");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_SUBJECT);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_ASC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters6() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters6");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_RICHMAIL_FLAG);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_ASC);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters7() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters7");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_MESSAGE_ID);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_DESC);
            addParams(inputParams, POP_DELETED_FLAG_KEY, POP_DELETED_FLAG_TRUE);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
    
    @Test
    public void testListMessageMetaDataWithOptionalParameters8() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testListMessageMetaDataWithOptionalParameters8");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, EMAIL_KEY, EMAIL);
            addParams(inputParams, FOLDERNAME_KEY, FOLDERNAME_VALUE);
            addParams(inputParams, SORT_KEY, SORT_KEY_MESSAGE_ID);
            addParams(inputParams, SORT_ORDER_KEY, SORT_ORDER_ASC);
            addParams(inputParams, POP_DELETED_FLAG_KEY, POP_DELETED_FLAG_TRUE);
            Map<String, Metadata> metadataList = metadataService
                    .list(inputParams);
            System.out.println("message.size() : " + metadataList.size());
            assertTrue("Unable to read message metadata",
                    (metadataList.size() != 0));
        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Occured...", true);
        }
    }
}
