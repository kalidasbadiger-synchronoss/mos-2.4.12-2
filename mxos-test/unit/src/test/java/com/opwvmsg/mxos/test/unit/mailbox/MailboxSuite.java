package com.opwvmsg.mxos.test.unit.mailbox;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailAccess 
@SuiteClasses({
    MailboxCreateTest.class,
    MailboxReadTest.class,
    MailboxDeleteTest.class,
    MailboxBaseTest.class,
    AliasTest.class,
    AllowedDomainsTest.class,
    MailboxStatusTest.class,
    GroupMailboxCreateTest.class
})
public class MailboxSuite {
}
