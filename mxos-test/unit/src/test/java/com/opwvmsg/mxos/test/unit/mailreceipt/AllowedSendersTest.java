package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAllowedSendersListService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class AllowedSendersTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static final String COSID="default_cos";
    private static IAllowedSendersListService service;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("AllowedSendersTest.setUpBeforeClass...");
        service = (IAllowedSendersListService) ContextUtils.loadContext()
                .getService(ServiceEnum.AllowedSendersListService.name());
        CosHelper.createCos(COSID);
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, COSID);
        //assertTrue("MailBox was not created.", mailboxId != -1);
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(EMAIL);
        getParams.put(EMAIL_KEY, new ArrayList<String>());
        getParams.get(EMAIL_KEY).add(EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("AllowedSendersTest.setUp...");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("AllowedSendersTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("AllowedSendersTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);
        getParams.clear();
        getParams = null;
        params.clear();
        params = null;
        service = null;
    }

    private static List<String> read() {
        List<String> allowedIP = null;
        try {
            allowedIP = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                getParams.put(EMAIL_KEY, new ArrayList<String>());
                getParams.get(EMAIL_KEY).add(EMAIL);
            }
        }
        assertNotNull("allowedIP object is null.", allowedIP);
        return allowedIP;
    }

    private static void create(Map<String, List<String>> params) {
        create(params, null);
    }

    private static void create(Map<String, List<String>> params,
            String expectedError) {
        try {
            service.create(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void update(Map<String, List<String>> params) {
        update(params, null);
    }

    private static void update(Map<String, List<String>> params,
            String expectedError) {
        try {
            service.update(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    private static void delete(Map<String, List<String>> params) {
        delete(params, null);
    }

    private static void delete(Map<String, List<String>> params,
            String expectedError) {
        try {
            service.delete(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
                params.get(EMAIL_KEY).add(EMAIL);
            }
        }
    }

    @Test
    public void testAddWithNoParam() throws Exception {
        System.out.println("AllowedSendersTest --> testAddWithNoParam");
        String key = MailboxProperty.allowedSender.name();
        params.put(key, new ArrayList<String>());
        create(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender still non-empty.", list.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddWithNull() throws Exception {
        System.out.println("AllowedSendersTest --> testAddWithNull");
        String key = MailboxProperty.allowedSender.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add(null);
        create(params, ErrorCode.MXS_INPUT_ERROR.name());
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender still non-empty.", list.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddWithEmpty() throws Exception {
        System.out.println("AllowedSendersTest --> testAddWithEmpty");
        String key = MailboxProperty.allowedSender.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("");
        create(params, ErrorCode.GEN_BAD_REQUEST.name());
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender still non-empty.", list.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddWithSplChars() throws Exception {
        System.out.println("AllowedSendersTest --> testAddWithSplChars");
        String key = MailboxProperty.allowedSender.name();
        params.put(key, new ArrayList<String>());
        params.get(key).add("!@#$%^&*()");
        create(params, MailboxError.MBX_INVALID_ALLOWED_SENDER.name());
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender still non-empty.", list.isEmpty());
        params.remove(key);
    }

    @Test
    public void testAddSuccess() throws Exception {
        System.out.println("AllowedSendersTest --> testAddSuccess");
        String key = MailboxProperty.allowedSender.name();
        String sender = "foo@bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender does not contain " + sender + ".",
                list.contains(sender));
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);
        params.remove(key);
    }

    @Test
    public void testAddSuccessForDomainName() throws Exception {
        System.out.println("AllowedSendersTest --> testAddSuccessForDomainName");
        String key = MailboxProperty.allowedSender.name();
        String sender = "bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender does not contain " + sender + ".",
                list.contains(sender));
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);
        params.remove(key);
    }
    @Test
    public void testGetSuccessForDomainName() throws Exception {
        System.out.println("AllowedSendersTest --> testGetSuccessForDomainName");
        String key = MailboxProperty.allowedSender.name();
        String sender = "bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender does not contain " + sender + ".",
                list.contains(sender));
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);
        params.remove(key);
    }

    @Test
    public void testGetSuccess() throws Exception {
        System.out.println("AllowedSendersTest --> testGetSuccess");
        String key = MailboxProperty.allowedSender.name();
        String sender = "foo1@bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertTrue("sender does not contain " + sender + ".",
                list.contains(sender));
        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);
        params.remove(key);
    }
    @Test
    public void testUpdateSuccess() throws Exception {
        System.out.println("AllowedSendersTest --> testUpdateSuccess");
        String key = MailboxProperty.allowedSender.name();
        String sender1 = "foo1@bar.com";
        String sender2 = "foo2@bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender1);
        create(params);

        String key1 = MailboxProperty.oldAllowedSender.name();
        String key2 = MailboxProperty.newAllowedSender.name();
        params.remove(key);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(sender1);
        params.put(key2, new ArrayList<String>());
        params.get(key2).add(sender2);
        update(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertFalse("sender contains " + sender1 + ".", list.contains(sender1));
        assertTrue("sender does not contain " + sender2 + ".",
                list.contains(sender2));
        params.remove(key1);
        params.remove(key2);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender2);
        delete(params);
        params.remove(key);
    }

    @Test
    public void testUpdateSuccessForDomainName() throws Exception {
        System.out.println("AllowedSendersTest --> testUpdateSuccessForDomainName");
        String key = MailboxProperty.allowedSender.name();
        String sender1 = "bar.com";
        String sender2 = "openwave.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender1);
        create(params);

        String key1 = MailboxProperty.oldAllowedSender.name();
        String key2 = MailboxProperty.newAllowedSender.name();
        params.remove(key);
        params.put(key1, new ArrayList<String>());
        params.get(key1).add(sender1);
        params.put(key2, new ArrayList<String>());
        params.get(key2).add(sender2);
        update(params);
        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertFalse("sender contains " + sender1 + ".", list.contains(sender1));
        assertTrue("sender does not contain " + sender2 + ".",
                list.contains(sender2));
        params.remove(key1);
        params.remove(key2);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender2);
        delete(params);
        params.remove(key);
    }
    @Test
    public void testDeleteSuccess() throws Exception {
        System.out.println("AllowedSendersTest --> testDeleteSuccess");
        String key = MailboxProperty.allowedSender.name();
        String sender = "foo1@bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);

        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertFalse("sender contains " + sender + ".", list.contains(sender));
        params.remove(key);
    }
    @Test
    public void testDeleteSuccessForDomainName() throws Exception {
        System.out.println("AllowedSendersTest --> testDeleteSuccessForDomainName");
        String key = MailboxProperty.allowedSender.name();
        String sender = "bar.com";
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        create(params);

        params.remove(key);
        params.put(key, new ArrayList<String>());
        params.get(key).add(sender);
        delete(params);

        // Get the list and compare
        List<String> list = read();
        assertNotNull("sender is null.", list);
        assertFalse("sender contains " + sender + ".", list.contains(sender));
        params.remove(key);
    }
}
