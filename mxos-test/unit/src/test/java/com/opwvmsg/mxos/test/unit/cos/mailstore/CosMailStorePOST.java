package com.opwvmsg.mxos.test.unit.cos.mailstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailStoreService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailStorePOST {

    private static final String TEST_NAME = "CosMailStorePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String cosId = "default_mxos.2.0";
    private static IMxOSContext context;
    private static ICosMailStoreService cosMailStoreService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static MailStore getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static MailStore getParams(Map<String, List<String>> params,
            MailboxError expectedError) {
        MailStore mailStore = null;
        try {
            mailStore = cosMailStoreService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SmsServices object is null.", mailStore);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return mailStore;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosMailStoreService = (ICosMailStoreService) context
                .getService(ServiceEnum.CosMailStoreService.name());
        assertNotNull("CosBaseService object is null.", cosMailStoreService);
        CosHelper.createCos(cosId);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CosTest.tearDownAfterClass...");
        params = null;
        cosMailStoreService = null;
        CosHelper.deleteCos(cosId);
    }
    

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            CosError expectedError) {
        try {
            cosMailStoreService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("CosTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("CosTest.tearDown...");
        if (params.get(COSID_KEY) != null)
            params.get(COSID_KEY).clear();
    }

    @Test
    public void testMaxMessagesEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessagesEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessages.name(), "");
        updateParams(updateParams, CosError.COS_INVALID_MAX_MESSAGES);
    }

    @Test
    public void testMaxMessagesInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMessagesInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessages.name(), "sadfsadf");
        updateParams(updateParams, CosError.COS_INVALID_MAX_MESSAGES);
    }

    // maxMessages
    @Test
    public void testMaxMessagesNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessagesNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessages.name(), null);
        updateParams(updateParams, CosError.COS_INVALID_MAX_MESSAGES);
    }

    @Test
    public void testMaxMessagesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMessagesSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessages.name(), "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MAX_MESSAGES);
    }

    @Test
    public void testMaxMessagesSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessagesSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessages.name(), "123456");
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        Integer value = getParams(updateParams).getMaxMessages();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456));
    }

    @Test
    public void testMaxStorageSizeKBEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxStorageSizeKB.name(), "");
        updateParams(updateParams, CosError.COS_INVALID_MAX_STORAGE_SIZEKB);
    }

    @Test
    public void testMaxStorageSizeKBInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxStorageSizeKB.name(),
                "sadfsadf");
        updateParams(updateParams, CosError.COS_INVALID_MAX_STORAGE_SIZEKB);
    }

    // maxStorageSizeKB
    @Test
    public void testMaxStorageSizeKBNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxStorageSizeKB.name(), null);
        updateParams(updateParams, CosError.COS_INVALID_MAX_STORAGE_SIZEKB);
    }

    @Test
    public void testMaxStorageSizeKBSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxStorageSizeKB.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MAX_STORAGE_SIZEKB);
    }

    @Test
    public void testMaxStorageSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.maxStorageSizeKB.name(),
                "123456");
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        Long value = getParams(updateParams).getMaxStorageSizeKB();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456L));
    }

    @Test
    public void testQuotaBounceNotifyEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifyEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(), "");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_BOUNCE_NOTIFY);
    }

    @Test
    public void testQuotaBounceNotifyInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifyInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(),
                "sadfsadf");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_BOUNCE_NOTIFY);
    }

    // quotaBounceNotify
    @Test
    public void testQuotaBounceNotifyNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifyNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(), null);
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_BOUNCE_NOTIFY);
    }

    @Test
    public void testQuotaBounceNotifySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifySplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_BOUNCE_NOTIFY);
    }

    @Test
    public void testQuotaBounceNotifySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifySuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        String value = getParams(updateParams).getQuotaBounceNotify();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testQuotaBounceNotifySuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifySuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.quotaBounceNotify.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        String value = getParams(updateParams).getQuotaBounceNotify();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }

    @Test
    public void testQuotaWarningThresholdEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaWarningThreshold.name(),
                "");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_WARNING_THRESHOLD);
    }

    @Test
    public void testQuotaWarningThresholdInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaWarningThreshold.name(),
                "sadfsadf");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_WARNING_THRESHOLD);
    }

    // quotaWarningThreshold
    @Test
    public void testQuotaWarningThresholdNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaWarningThreshold.name(),
                null);
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_WARNING_THRESHOLD);
    }

    @Test
    public void testQuotaWarningThresholdSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.quotaWarningThreshold.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_QUOTA_WARNING_THRESHOLD);
    }

    @Test
    public void testQuotaWarningThresholdSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaWarningThresholdSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.quotaWarningThreshold.name(),
                "1");
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        Integer value = getParams(updateParams).getQuotaWarningThreshold();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value == 1);
    }

    
    @Test
    public void testMobileMaxMessagesSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxMessagesSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.mobileMaxMessages.name(), "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MOBILE_MAX_MESSAGES);
    }

    @Test
    public void testMobileMaxMessagesSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileMaxMessagesSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.mobileMaxMessages.name(), "123456");
        updateParams(updateParams);
        Integer value = Integer.parseInt(updateParams.get(
                MailboxProperty.mobileMaxMessages.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456));
    }

    @Test
    public void testMobileStorageSizeKBSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileStorageSizeKBSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.mobileMaxStorageSizeKB.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MOBILE_MAX_STORAGE_SIZEKB);
    }

    @Test
    public void testMobileStorageSizeKBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileStorageSizeKBSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.mobileMaxStorageSizeKB.name(),
                "123456");
        updateParams(updateParams);
        Long value = Long.parseLong(updateParams.get(
                MailboxProperty.mobileMaxStorageSizeKB.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456L));
    }
    
    @Test
    public void testMaxMessagesSoftLimitSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxMessagesSoftLimitSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxMessagesSoftLimit.name(), "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MAX_MESSAGES_SOFT_LIMIT);
    }

    @Test
    public void testMaxMessagesSoftLimitSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxMessagesSoftLimitSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.maxMessagesSoftLimit.name(), "123456");
        updateParams(updateParams);
        Integer value = Integer.parseInt(updateParams.get(
                MailboxProperty.maxMessagesSoftLimit.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456));
    }

    @Test
    public void testMaxStorageSizeKBSoftLimitSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBSoftLimitSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxStorageSizeKBSoftLimit.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MAX_STORAGE_SIZEKB_SOFT_LIMIT);
    }

    @Test
    public void testMaxStorageSizeKBSoftLimitSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxStorageSizeKBSoftLimitSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.maxStorageSizeKBSoftLimit.name(),
                "123456");
        updateParams(updateParams);
        Long value = Long.parseLong(updateParams.get(
                MailboxProperty.maxStorageSizeKBSoftLimit.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456L));
    }
    
    @Test
    public void testMobileMaxMessagesSoftLimitSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxMessagesSoftLimitSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.mobileMaxMessagesSoftLimit.name(), "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MOBILE_MAX_MESSAGES_SOFT_LIMIT);
    }

    @Test
    public void testMobileMaxMessagesSoftLimitSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMobileMaxMessagesSoftLimitSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.mobileMaxMessagesSoftLimit.name(), "123456");
        updateParams(updateParams);
        Integer value = Integer.parseInt(updateParams.get(
                MailboxProperty.mobileMaxMessagesSoftLimit.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456));
    }

    @Test
    public void testMobileMaxStorageSizeKBSoftLimitSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxStorageSizeKBSoftLimitSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_MOBILE_MAX_STORAGE_SIZEKB_SOFT_LIMIT);
    }

    @Test
    public void testMobileMaxStorageSizeKBSoftLimitSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMobileMaxStorageSizeKBSoftLimitSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name(),
                "123456");
        updateParams(updateParams);
        Long value = Long.parseLong(updateParams.get(
                MailboxProperty.mobileMaxStorageSizeKBSoftLimit.name()).get(0));
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.", value.equals(123456L));
    }
    
    // largeMailboxPlatformEnabled
    @Test
    public void testLargeMailboxPlatformEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLargeMailboxPlatformEnabledNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.largeMailboxPlatformEnabled.name(), null);
        updateParams(updateParams, CosError.COS_INVALID_LARGE_MAILBOX_PLATFORM_ENABLED);
    }

    @Test
    public void testLargeMailboxPlatformEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLargeMailboxPlatformEnabledSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.largeMailboxPlatformEnabled.name(),
                "!@#$%^&");
        updateParams(updateParams, CosError.COS_INVALID_LARGE_MAILBOX_PLATFORM_ENABLED);
    }

    @Test
    public void testLargeMailboxPlatformEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testLargeMailboxPlatformEnabledSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.largeMailboxPlatformEnabled.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        String value = getParams(updateParams).getLargeMailboxPlatformEnabled();
        assertNotNull("Is null.", value);
        assertTrue("Has a wrong value.",
                value.equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

}
