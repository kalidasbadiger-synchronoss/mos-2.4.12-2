package com.opwvmsg.mxos.test.unit.addressbook.login;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

public class LoginPOST {

    private static final String TEST_NAME = "LoginPOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID_KEY = AddressBookProperty.userId.name();
    private static final String PASSWORD_KEY = AddressBookProperty.password
            .name();
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static IExternalLoginService externalLoginService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
    }

    @Test
    public void login() {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(USERID_KEY, new ArrayList<String>());
        inputParams.put(PASSWORD_KEY, new ArrayList<String>());
        inputParams.get(USERID_KEY).add(USERID);
        inputParams.get(PASSWORD_KEY).add(PASSWORD);
        ExternalSession sessionId = null;
        try {
            sessionId = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", sessionId.getSessionId() != null);
    }
}
