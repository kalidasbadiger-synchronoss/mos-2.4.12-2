package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSenderBlockingService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailReceiptSenderBlocking {

    private static ICosSenderBlockingService cossb = null;
    private static ICosService cosService = null;
    private static Map <String, List<String> >inputParams =
        new HashMap<String, List<String>>();
    private static String test = "test";
    private static String cosIdKey = "cosId";
    
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.put("loadDefaultCos", new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.get("loadDefaultCos").add("yes");
        try {
            cossb = (ICosSenderBlockingService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosSenderBlockingService.name());
            cosService = (ICosService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosService.name());
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNotNull(cossb);
    }
    
    
    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:cos starting......");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosService.delete(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testSenderBlockingGet() {
        System.out.println("testSenderBlockingGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            SenderBlocking sb = cossb.read(inputParams);
            assertNotNull(sb);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testSenderBlockingPost() {
        System.out.println("testSenderBlockingPost:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("senderBlockingAllowed", new ArrayList<String>());
        inputParams.get("senderBlockingAllowed").add("yes");
        try {
            cossb.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            SenderBlocking sb  = cossb.read(inputParams);
            assertEquals("Passed: ", MxosEnums.BooleanType.YES, sb.getSenderBlockingAllowed());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    
    @Test
    public void testSenderBlockingPost3() {
        System.out.println("testSenderBlockingPost3:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("blockSendersPABActive", new ArrayList<String>());
        inputParams.get("blockSendersPABActive").add("yes");
        try {
            cossb.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            SenderBlocking sb  = cossb.read(inputParams);
            assertEquals("Passed: ", MxosEnums.BooleanType.YES, sb.getBlockSendersPABActive());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testSenderBlockingPostNeg1() {
        System.out.println("testSenderBlockingPostNeg1:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cossb.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", ErrorCode.GEN_BAD_REQUEST.name(), me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testSenderBlockingPostNeg2() {
        System.out.println("testSenderBlockingPostNeg2:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("senderBlockingEnabled", new ArrayList<String>());
        inputParams.get("senderBlockingEnabled").add("illegal");
        try {
            cossb.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", MailboxError.MBX_INVALID_SENDER_BLOCKING_ENABLED.name(), me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}