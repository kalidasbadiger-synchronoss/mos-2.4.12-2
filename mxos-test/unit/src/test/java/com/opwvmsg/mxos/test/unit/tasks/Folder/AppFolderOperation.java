/**
 * 
 */
package com.opwvmsg.mxos.test.unit.tasks.Folder;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.task.pojos.Folder;

/**
 * @author bgh21604
 * 
 */
public class AppFolderOperation {
    private static IMxOSContext context;
    private static ExternalSession oxSession;
    private static final String baseURL = "http://localhost:8080/mxos";
    private static final String userName = "test21234567@openwave.com";
    private static final String password = "test";
    private String folderName = "Test10000";
    private String notes = "Notes1000";

    private static void createContext() throws MxOSException {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.REST.name());
        p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Tasks");
        p.setProperty(ContextProperty.MXOS_BASE_URL, baseURL);
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "5");
        p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
        p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
        context = MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO",
                p);
    }

    private static void login() throws MxOSException {
        System.out.println("Login() called");
        assertNotNull("context is null", context);
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(),
                Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.password.name(), Arrays.asList(password));
        oxSession = service.login(requestMap);
        assertTrue("Login suceeded.", oxSession.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("This is setUpBeforeClass");
        try {
            createContext();
            login();
        } catch (MxOSException e) {
            assertTrue("Login Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("This is tearDownAfterClass");
    }

    @Test
    public void testFolderCreateAB() {
        System.out.println("testFolderCreate() started");
        IAppsFolderService service = null;

        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Folder Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertNotNull("oxSession is null", oxSession);

        System.out.println("userName:" + userName + ", folderName:" + folderName
                + ", notes:" + notes + ", sessionId:"
                + oxSession.getSessionId() + ", cookies:"
                + oxSession.getCookieString());
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.folderName.name(), Arrays.asList(folderName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("addressBook"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        long taskId = 0;
        try {
            taskId = service.create(requestMap);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertTrue(taskId != 0);

        System.out.println("Task Created :" + taskId);
    }

    
    @Test
    public void testFolderCreateTask() {
        System.out.println("testFolderCreateTask() started");
        IAppsFolderService service = null;

        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Folder Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertNotNull("oxSession is null", oxSession);

        System.out.println("userName:" + userName + ", folderName:" + folderName
                + ", notes:" + notes + ", sessionId:"
                + oxSession.getSessionId() + ", cookies:"
                + oxSession.getCookieString());
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.folderName.name(), Arrays.asList(folderName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("task"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        long taskId = 0;
        try {
            taskId = service.create(requestMap);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertTrue(taskId != 0);

        System.out.println("Task Created :" + taskId);
    }

    @Test
    public void testListFolderTask() {
        System.out.println("testListFolderAB() started...");
        IAppsFolderService service = null;
        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            assertTrue("Folder list Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("task"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        List<Folder> tasks = null;
        try {
            tasks = service.list(requestMap);
        } catch (MxOSException e) {
            assertTrue("Folder list Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        System.out.println("Folder List size:" + tasks.size());

        for (Folder task : tasks) {
            System.out.println("Folder Name :" + task.getTitle());
        }
    }
    
    @Test
    public void testListFolderAB() {
        System.out.println("testListFolderAB() started...");
        IAppsFolderService service = null;
        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            assertTrue("Folder list Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("addressBook"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        List<Folder> tasks = null;
        try {
            tasks = service.list(requestMap);
        } catch (MxOSException e) {
            assertTrue("Folder list Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        System.out.println("Folder List size:" + tasks.size());

        for (Folder task : tasks) {
            System.out.println("Folder Name :" + task.getTitle());
        }
    }
    
    @Test
    public void testdeleteFolderAB() {
        System.out.println("testListTask() started...");
        IAppsFolderService service = null;
        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            assertTrue("Folder deletion Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("addressBook"));
        requestMap.put(TasksProperty.folderId.name(), Arrays.asList("40"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

       
        try {
          service.deleteAll(requestMap);
        } catch (MxOSException e) {
            assertTrue("Folder Deletion Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        
    }
    
    @Test
    public void testdeleteFolderTask() {
        System.out.println("testdeleteFolderTask() started...");
        IAppsFolderService service = null;
        try {
            service = (IAppsFolderService) context
                    .getService(ServiceEnum.AppsFolderService.name());
        } catch (MxOSException e) {
            assertTrue("Folder deletion Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.module.name(), Arrays.asList("task"));
        requestMap.put(TasksProperty.folderId.name(), Arrays.asList("40"));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

       
        try {
          service.deleteAll(requestMap);
        } catch (MxOSException e) {
            assertTrue("Folder Deletion Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        
    }
} 