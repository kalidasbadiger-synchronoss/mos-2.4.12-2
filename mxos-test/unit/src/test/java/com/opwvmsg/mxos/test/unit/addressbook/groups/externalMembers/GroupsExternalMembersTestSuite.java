package com.opwvmsg.mxos.test.unit.addressbook.groups.externalMembers;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailAccess
@SuiteClasses({ GroupsExternalMembersCreateAndDelete.class,
        GroupsExternalMembersGET.class, GroupsExternalMembersDeleteAll.class

})
public class GroupsExternalMembersTestSuite {

}
