package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.CloudmarkActionType;
import com.opwvmsg.mxos.data.enums.MxosEnums.SpamPolicy;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptCloudmarkFiltersService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailReceiptCloudmarkFiltersGET {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID = "default_cos";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "CosMailReceiptCloudmarkFiltersGET";
    private static final String ARROW_SEP = " --> ";
    private static ICosMailReceiptCloudmarkFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (ICosMailReceiptCloudmarkFiltersService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.CosMailReceiptCloudmarkFiltersService.name());
        CosHelper.createCos(COSID);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", params.get(COSID_KEY));
        params.get(COSID_KEY).add(COSID);
        assertTrue("Input Param:email is empty.", !params.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(COSID_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        CosHelper.deleteCos(COSID);
        params.clear();
        params = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static CloudmarkFilters getParams() {
        return getParams(null);
    }

    private static CloudmarkFilters getParams(String expectedError) {
        CloudmarkFilters filters = null;
        try {
            filters = service.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(COSID_KEY)
                    || params.get(COSID_KEY).isEmpty()) {
                addToParams(params, COSID_KEY, COSID);
            }
        }
        return filters;
    }

    @Test
    public void testGetWithoutCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithoutCOS");
        // Clear the params.
        params.remove(COSID_KEY);
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(params, COSID_KEY, COSID);
    }

    @Test
    public void testGetWithNullCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithNullCOS");
        // Remove the cosId from the map to have a null cosId.
        params.remove(COSID_KEY);
        addToParams(params, COSID_KEY, null);
        getParams(ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(COSID_KEY);
        addToParams(params, COSID_KEY, COSID);
    }

    @Test
    public void testGetWithEmptyCOS() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithEmptycosId");
        // Remove the cosId from the map to have a null cosId.
        params.remove(COSID_KEY);
        addToParams(params, COSID_KEY, "");
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(COSID_KEY);
        addToParams(params, COSID_KEY, COSID);
    }

   
    @Test
    public void testSpamFilterEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamFilterEnabled");
        BooleanType spamfilterEnabled = getParams().getSpamfilterEnabled();
        assertNull("Is not null.", spamfilterEnabled);
    }

    @Test
    public void testSpamPolicy() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamPolicy");
        SpamPolicy spamPolicy = getParams().getSpamPolicy();
        assertNull("Is not null.", spamPolicy);
    }

    @Test
    public void testSpamAction() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSpamAction");
        String spamAction = getParams().getSpamAction();
        assertNull("Is not null.", spamAction);
    }

    @Test
    public void testCleanAction() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testCleanAction");
        String cleanAction = getParams().getCleanAction();
        assertNull("Is not null.", cleanAction);
    }

    @Test
    public void testSuspectAction() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSuspectAction");
        String suspectAction = getParams().getSuspectAction();
        assertNull("Is not null.", suspectAction);
    }
}
