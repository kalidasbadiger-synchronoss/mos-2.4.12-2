/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminApprovedSendersListService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

public class AdminApprovedSendersPOST {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String ADMIN_EMAIL = "head2@openwave.com";
    private static final String CHILD_EMAIL = "child005@openwave.com";
    private static final String INVALID_EMAIL = "alphabeta@openwave.com";
    private static final String PASSWORD = "test";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "AdminApprovedSendersPOST";
    private static final String ARROW_SEP = " --> ";
    private static IAdminApprovedSendersListService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IAdminApprovedSendersListService) ContextUtils.loadContext()
                .getService(ServiceEnum.AdminApprovedSendersListService.name());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        service = null;
    }

    private static void addToParams(String key, String value) {
        List<String> values = null;
        if (params.containsKey(key)) {
            values = params.get(key);
        } else {
            values = new ArrayList<String>();
            params.put(key, values);
        }
        values.add(values.size(), value);
    }

    private static void executeService() {
        executeService(null);
        return;
    }

    private static void executeService(String expectedError) {
        try {
            service.update(params);
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
            }
        }
        return;
    }

    @Test
    public void testAdminApprovedSendersInvalidOldApprovedSender()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersInvalidOldApprovedSender");
        addToParams(EMAIL_KEY, ADMIN_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(), "foo@barcom");
        addToParams(MailboxProperty.newAdminApprovedSender.name(), "foo.com");
        executeService("Invalid old blocked sender");
    }

    @Test
    public void testAdminApprovedSendersOldApprovedSenderNotExist()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersOldApprovedSenderNotExist");
        addToParams(EMAIL_KEY, ADMIN_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(),
                "doesnotexist@bar.com");
        addToParams(MailboxProperty.newAdminApprovedSender.name(), "foo.com");
        executeService("Old blocked sender does not exist");
    }

    @Test
    public void testAdminApprovedSendersInvalidNewApprovedSender()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersInvalidNewApprovedSender");
        addToParams(EMAIL_KEY, ADMIN_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(),
                "foo@bar.com");
        addToParams(MailboxProperty.newAdminApprovedSender.name(), "foo-com");
        executeService("Invalid new admin blocked sender");
    }

    @Test
    public void testAdminApprovedSendersUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersUpdateSuccess");
        addToParams(EMAIL_KEY, ADMIN_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(), "x1.com");
        addToParams(MailboxProperty.newAdminApprovedSender.name(),
                "update@x1.com");
        executeService();
    }

    @Test
    public void testAdminApprovedSendersInvalidEmail() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersInvalidEmail");
        addToParams(EMAIL_KEY, INVALID_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(), "xyz.com");
        addToParams(MailboxProperty.newAdminApprovedSender.name(),
                "update@foo.com");
        executeService("Error : Invalid email provided");
    }

    @Test
    public void testAdminApprovedSendersChildEmail() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminApprovedSendersChildEmail");
        addToParams(EMAIL_KEY, CHILD_EMAIL);
        addToParams(MailboxProperty.oldAdminApprovedSender.name(), "xyz.com");
        addToParams(MailboxProperty.newAdminApprovedSender.name(),
                "update@foo.com");
        executeService("Error : Child email provided");
    }

}
