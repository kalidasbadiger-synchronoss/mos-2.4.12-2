package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISenderBlockingService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author Aricent
 * 
 */
public class MailReceiptSieveFiltersGET {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptSieveFiltersGET";
    private static final String ARROW_SEP = " --> ";
    private static IMailReceiptSieveFiltersService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IMailReceiptSieveFiltersService) ContextUtils.loadContext()
                .getService(ServiceEnum.MailReceiptSieveFiltersService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SieveFilters getParams() {
        return getParams(null);
    }

    private static SieveFilters getParams(String expectedError) {
        SieveFilters filters = null;
        try {
            filters = service.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty() || null == params.get(EMAIL_KEY)
                    || params.get(EMAIL_KEY).isEmpty()) {
                addToParams(params, EMAIL_KEY, EMAIL);
            }
        }
        return filters;
    }

    @Test
    public void testGetWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithoutEmail");
        // Clear the params.
        params.remove(EMAIL_KEY);
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithNullEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, null);
        getParams(ErrorCode.MXS_INPUT_ERROR.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testGetWithEmptyEmail");
        // Remove the email from the map to have a null email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "");
        getParams(ErrorCode.GEN_BAD_REQUEST.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "something.junk@foobar.com");
        getParams(MailboxError.MBX_NOT_FOUND.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInEmail() {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testGetWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "!#$%^&*()+-=.junk@foobar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testGetWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testGetWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, "junk@$&*^-bar.com");
        getParams(MailboxError.MBX_INVALID_EMAIL.name());
        params.remove(EMAIL_KEY);
        addToParams(params, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testSieveFilteringEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSieveFilteringEnabled");
        String sieveFilteringEnabled = getParams()
                .getSieveFilteringEnabled().name();
        assertNotNull("Is null.", sieveFilteringEnabled);
    }

    @Test
    public void testMtaFilter() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMtaFilter");
        String filter = getParams().getMtaFilter();
        assertNull("Is not null.", filter);
    }

    @Test
    public void testRmFilter() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testRmFilter");
        String filter = getParams().getRmFilter();
        assertNull("Is not null.", filter);
    }

    @Test
    public void testBlockedSenderAction() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testBlockedSenderAction");
        String blockedSenderAction = getParams().getBlockedSenderAction()
                .name();
        assertNotNull("Is null.", blockedSenderAction);
    }

    @Test
    public void testBlockedSenderMessage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testBlockedSenderMessage");
        String blockedSenderMessage = getParams()
                .getBlockedSenderMessage();
        assertNotNull("Is null.", blockedSenderMessage);
    }

    @Test
    public void testRejectBouncedMessage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testRejectBouncedMessage");
        String rejectBouncedMessage = getParams()
                .getRejectBouncedMessage().name();
        assertNotNull("Is null.", rejectBouncedMessage);
    }
}
