package com.opwvmsg.mxos.test.unit.webmailfeatures.addressbook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IWebMailFeaturesAddressBookService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetWebMailAddressBookFeaturesTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100013@openwave.com";
    private static final String PASSWORD = "test1";
    private static IWebMailFeaturesAddressBookService wmfabs;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.setUpBeforeClass...");
        wmfabs = (IWebMailFeaturesAddressBookService) ContextUtils.loadContext().getService(
                ServiceEnum.WebMailFeaturesAddressBookService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.setUp...");
        assertNotNull("GetWebMailAddressBookFeaturesTest object is null.", wmfabs);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.tearDown...");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        wmfabs = null;
    }

    private static AddressBookFeatures getAddressBookFeaturesAddressBookParams(
            Map<String, List<String>> params) {
        return getAddressBookFeaturesAddressBookParams(params, null);
    }

    private static AddressBookFeatures getAddressBookFeaturesAddressBookParams(
            Map<String, List<String>> params, String expectedError) {
        AddressBookFeatures ab = null;
        try {
            ab = wmfabs.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("GetWebMailAddressBookFeaturesTest object is null.", wmfabs);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
        }
        return ab;
    }

   @Test
    public void testGetAddressBookFeaturesParamsWithoutEmail() {
        System.out
                .println("GetWebMailAddressBookFeaturesTest.testGetAddressBookFeaturesParamsWithoutEmail...");
        // Clear the params.
        params.clear();
        getAddressBookFeaturesAddressBookParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetAddressBookFeaturesWithNullEmail() {
        System.out
                .println("GetWebMailAddressBookFeaturesTest.testGetAddressBookFeaturesWithNullEmail...");
        // Remove the email from the map to have a null email.
        params.get(EMAIL_KEY).clear();
        getAddressBookFeaturesAddressBookParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetAddressBookFeaturesWithNonExistingEmail() {
        System.out
                .println("GetWebMailAddressBookFeaturesTest.testGetAddressBookFeaturesWithNonExistingEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("something.junk@foobar.com");
        getAddressBookFeaturesAddressBookParams(params, MailboxError.MBX_NOT_FOUND.name());
    }

    @Test
    public void testSetMaxContacts() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.testSetMaxContacts...");
        int maxContacts = getAddressBookFeaturesAddressBookParams(params).getMaxContacts();
        System.out.println("MaxContacts : "+maxContacts);
    }

    @Test
    public void testSetMaxGroups() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.testSetMaxContacts...");
        int maxContacts = getAddressBookFeaturesAddressBookParams(params).getMaxGroups();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetMaxContactsPerGroup() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.testSetMaxContacts...");
        int maxContacts = getAddressBookFeaturesAddressBookParams(params).getMaxContactsPerGroup();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetMaxContactsPerPage() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.testSetMaxContacts...");
        int maxContacts = getAddressBookFeaturesAddressBookParams(params).getMaxContactsPerPage();
        System.out.println("MaxContacts : "+maxContacts);
    }
    
    @Test
    public void testSetCreateContactsFromOutgoingEmails() throws Exception {
        System.out.println("GetWebMailAddressBookFeaturesTest.createContactsFromOutgoingEmails...");
        String foe = getAddressBookFeaturesAddressBookParams(params).getCreateContactsFromOutgoingEmails().toString();
        assertNotNull("CreateContactsFromOutgoingEmails is null.", foe);
        System.out.println("CreateContactsFromOutgoingEmails : "+foe);
    }
}
