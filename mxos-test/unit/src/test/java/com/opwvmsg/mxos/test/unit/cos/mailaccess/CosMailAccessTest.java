package com.opwvmsg.mxos.test.unit.cos.mailaccess;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.IntAccessType;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailAccessService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailAccessTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID_VALUE = "CosMailAccessTestCos";

    private static final String POPACCESSTYPE_KEY = MailboxProperty.popAccessType
            .name();
    private static final String POPSSLACCESSTYPE_KEY = MailboxProperty.popSSLAccessType
            .name();
    private static final String POPAUTHENTICATIONTYPE_KEY = MailboxProperty.popAuthenticationType
            .name();
    private static final String IMAPACCESSTYPE_KEY = MailboxProperty.imapAccessType
            .name();
    private static final String IMAPSSLACCESSTYPE_KEY = MailboxProperty.imapSSLAccessType
            .name();
    private static final String SMTPACCESSTYPEENABLED_KEY = MailboxProperty.smtpAccessEnabled
            .name();
    private static final String SMTPSSLACCESSTYPEENABLED_KEY = MailboxProperty.smtpSSLAccessEnabled
            .name();
    private static final String SMTPAUTHENABLED_KEY = MailboxProperty.smtpAuthenticationEnabled
            .name();
    private static final String WEBMAILACCESSTYPE_KEY = MailboxProperty.webmailAccessType
            .name();
    private static final String WEBMAILSSLACCESSTYPE_KEY = MailboxProperty.webmailSSLAccessType
            .name();
    private static final String MOBILEMAILACCESS_KEY = MailboxProperty.mobileMailAccessType
            .name();
    private static final String MOBILEACTIVESYNCALLOWED_KEY = MailboxProperty.mobileActiveSyncAllowed
            .name();

    private static ICosMailAccessService cosMailAccessService;
    private static ICosService cs;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosMailAccessTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        cs = (ICosService) ContextUtils.loadContext().getService(
                ServiceEnum.CosService.name());
        cosMailAccessService = (ICosMailAccessService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.CosMailAccessService.name());
        createCos(COSID_VALUE);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("CosMailAccessTest.setUp...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        assertNotNull("MailAccessService object is null.", cosMailAccessService);
        assertNotNull("MailBoxService object is null.", cosMailAccessService);
        assertNotNull("Input Param:email is null.", inputParams.get(COSID_KEY));
        assertTrue("Input Param:email is empty.", !inputParams.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("CosMailAccessTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CosMailAccessTest.tearDownAfterClass...");
        deleteCos(COSID_VALUE);
        cosMailAccessService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static MailAccess getMailAccessParams(
            Map<String, List<String>> params) {
        MailAccess mao = null;
        try {
            mao = cosMailAccessService.read(params);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("MailAccess object is null.", mao);
        return mao;
    }

    private static void updateMailAccessParams(
            Map<String, List<String>> updateParams) {
        updateMailAccessParams(updateParams, null);
    }

    private static void updateMailAccessParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            cosMailAccessService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        }
    }

    private static void createCos(String cos) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cos);
        try {
            cs.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }

    }

    private static void deleteCos(String cos) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cos);
        try {
            cs.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testPopAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAccessTypeNullParam...");

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPACCESSTYPE_KEY, null);

        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_ACCESS.name());

    }

    @Test
    public void testPopAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_ACCESS.name());

    }

    @Test
    public void testPopAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPACCESSTYPE_KEY, "#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_ACCESS.name());

    }

    @Test
    public void testPopAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_ACCESS.name());

    }

    @Test
    public void testPopAccessTypeSuccess() throws Exception {
        System.out.println("CosMailAccessTest.testPopAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPACCESSTYPE_KEY, AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams).getPopAccessType();
        assertNotNull("PopAccessType is null.", pa);
        assertTrue("PopAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testPopSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopSSLAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPSSLACCESSTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_SSL_ACCESS.name());

    }

    @Test
    public void testPopSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopSSLAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPSSLACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_SSL_ACCESS.name());

    }

    @Test
    public void testPopSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopSSLAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPSSLACCESSTYPE_KEY, "#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_SSL_ACCESS.name());

    }

    @Test
    public void testPopSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopSSLAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPSSLACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_SSL_ACCESS.name());

    }

    @Test
    public void testPopSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopSSLAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPSSLACCESSTYPE_KEY, AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams).getPopSSLAccessType();
        assertNotNull("PopSSLAccessType is null.", pa);
        assertTrue("PopSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testPopAuthenticationTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAuthenticationTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPAUTHENTICATIONTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_AUTH_TYPE.name());

    }

    @Test
    public void testPopAuthenticationTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAuthenticationTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPAUTHENTICATIONTYPE_KEY, "5");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_AUTH_TYPE.name());

    }

    @Test
    public void testPopAuthenticationTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAuthenticationTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPAUTHENTICATIONTYPE_KEY, "@#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_AUTH_TYPE.name());

    }

    @Test
    public void testPopAuthenticationTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAuthenticationTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPAUTHENTICATIONTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_POP_AUTH_TYPE.name());

    }

    @Test
    public void testPopAuthenticationTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testPopAuthenticationTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, POPAUTHENTICATIONTYPE_KEY,
                IntAccessType._0.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        IntAccessType pa = getMailAccessParams(inputParams)
                .getPopAuthenticationType();
        assertNotNull("PopAuthenticationType is null.", pa);
        assertTrue("PopAuthenticationType has a wrong value.",
                pa.equals(IntAccessType._0));

    }

    @Test
    public void testImapAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPACCESSTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_ACCESS.name());

    }

    @Test
    public void testImapAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_ACCESS.name());

    }

    @Test
    public void testImapAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPACCESSTYPE_KEY, "@#$%^&*");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_ACCESS.name());

    }

    @Test
    public void testImapAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_ACCESS.name());

    }

    @Test
    public void testImapAccessTypeSuccess() throws Exception {
        System.out.println("CosMailAccessTest.testImapAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPACCESSTYPE_KEY, AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams).getImapAccessType();
        assertNotNull("ImapAccessType is null.", pa);
        assertTrue("ImapAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testImapSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapSSLAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPSSLACCESSTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_SSL_ACCESS.name());

    }

    @Test
    public void testImapSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapSSLAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPSSLACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_SSL_ACCESS.name());

    }

    @Test
    public void testImapSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapSSLAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPSSLACCESSTYPE_KEY, "#$^&*");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_SSL_ACCESS.name());

    }

    @Test
    public void testImapSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapSSLAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPSSLACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_IMAP_SSL_ACCESS.name());

    }

    @Test
    public void testImapSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testImapSSLAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, IMAPSSLACCESSTYPE_KEY,
                AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams).getImapSSLAccessType();
        assertNotNull("ImapSSLAccessType is null.", pa);
        assertTrue("ImapSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testSmtpAccessEnabledNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAccessEnabledNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPACCESSTYPEENABLED_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAccessEnabledInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPACCESSTYPEENABLED_KEY, "6");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAccessEnabledSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPACCESSTYPEENABLED_KEY, "#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAccessEnabledEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPACCESSTYPEENABLED_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpAccessEnabledSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAccessEnabledSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPACCESSTYPEENABLED_KEY,
                BooleanType.YES.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        BooleanType pa = getMailAccessParams(inputParams)
                .getSmtpAccessEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertTrue("SmtpAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));

    }

    @Test
    public void testSmtpSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpSSLAccessEnabledNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPSSLACCESSTYPEENABLED_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_SSL_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpSSLAccessEnabledInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPSSLACCESSTYPEENABLED_KEY, "6");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_SSL_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpSSLAccessEnabledSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPSSLACCESSTYPEENABLED_KEY, "$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_SSL_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpSSLAccessEnabledEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPSSLACCESSTYPEENABLED_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_SSL_ACCESS_ENABLED.name());

    }

    @Test
    public void testSmtpSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpSSLAccessEnabledSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPSSLACCESSTYPEENABLED_KEY,
                BooleanType.YES.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        BooleanType pa = getMailAccessParams(inputParams)
                .getSmtpSSLAccessEnabled();
        assertNotNull("SmtpSSLAccessEnabled is null.", pa);
        assertTrue("SmtpSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));

    }

    @Test
    public void testSmtpAuthenticationEnabledNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAuthenticationEnabledNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPAUTHENABLED_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_AUTH_ENABLED.name());

    }

    @Test
    public void testSmtpAuthenticationEnabledInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAuthenticationEnabledInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPAUTHENABLED_KEY, "6");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_AUTH_ENABLED.name());

    }

    @Test
    public void testSmtpAuthenticationEnabledSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAuthenticationEnabledSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPAUTHENABLED_KEY, "%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_AUTH_ENABLED.name());

    }

    @Test
    public void testSmtpAuthenticationEnabledEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAuthenticationEnabledEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPAUTHENABLED_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_SMTP_AUTH_ENABLED.name());

    }

    @Test
    public void testSmtpAuthenticationEnabledSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testSmtpAuthenticationEnabledSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMTPAUTHENABLED_KEY, BooleanType.YES.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        BooleanType pa = getMailAccessParams(inputParams)
                .getSmtpAuthenticationEnabled();
        assertNotNull("SmtpAuthenticationEnabled is null.", pa);
        assertTrue("SmtpAuthenticationEnabled has a wrong value.",
                pa.equals(BooleanType.YES));

    }

    @Test
    public void testWebmailAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILACCESSTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_ACCESS.name());

    }

    @Test
    public void testWebmailAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_ACCESS.name());
    }

    @Test
    public void testWebmailAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILACCESSTYPE_KEY, "$%&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_ACCESS.name());

    }

    @Test
    public void testWebmailAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_ACCESS.name());

    }

    @Test
    public void testWebmailAccessTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILACCESSTYPE_KEY,
                AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams).getWebmailAccessType();
        assertNotNull("WebmailAccessType is null.", pa);
        assertTrue("WebmailAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testWebmailSSLAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailSSLAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILSSLACCESSTYPE_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_SSL_ACCESS.name());

    }

    @Test
    public void testWebmailSSLAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailSSLAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILSSLACCESSTYPE_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_SSL_ACCESS.name());

    }

    @Test
    public void testWebmailSSLAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailSSLAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILSSLACCESSTYPE_KEY, "@#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_SSL_ACCESS.name());

    }

    @Test
    public void testWebmailSSLAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailSSLAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILSSLACCESSTYPE_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_WEBMAIL_SSL_ACCESS.name());

    }

    @Test
    public void testWebmailSSLAccessTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testWebmailSSLAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, WEBMAILSSLACCESSTYPE_KEY,
                AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams)
                .getWebmailSSLAccessType();
        assertNotNull("WebmailSSLAccessType is null.", pa);
        assertTrue("WebmailSSLAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testMobileMailAccessTypeNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileMailAccessTypeNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEMAILACCESS_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_MAIL_ACCESS.name());

    }

    @Test
    public void testMobileMailAccessTypeInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileMailAccessTypeInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEMAILACCESS_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_MAIL_ACCESS.name());

    }

    @Test
    public void testMobileMailAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileMailAccessTypeSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEMAILACCESS_KEY, "$%^&*");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_MAIL_ACCESS.name());

    }

    @Test
    public void testMobileMailAccessTypeEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileMailAccessTypeEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEMAILACCESS_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_MAIL_ACCESS.name());

    }

    @Test
    public void testMobileMailAccessTypeSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileMailAccessTypeSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEMAILACCESS_KEY, AccessType.NONE.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        AccessType pa = getMailAccessParams(inputParams)
                .getMobileMailAccessType();
        assertNotNull("MobileMailAccessType is null.", pa);
        assertTrue("MobileMailAccessType has a wrong value.",
                pa.equals(AccessType.NONE));

    }

    @Test
    public void testMobileActiveSyncAllowedNullParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileActiveSyncAllowedNullParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEACTIVESYNCALLOWED_KEY, null);
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED.name());

    }

    @Test
    public void testMobileActiveSyncAllowedInvalidParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileActiveSyncAllowedInvalidParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEACTIVESYNCALLOWED_KEY, "Junk");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED.name());

    }

    @Test
    public void testMobileActiveSyncAllowedSplCharsInParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileActiveSyncAllowedSplCharsInParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEACTIVESYNCALLOWED_KEY, "#$%^&*(");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED.name());

    }

    @Test
    public void testMobileActiveSyncAllowedEmptyParam() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileActiveSyncAllowedEmptyParam...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEACTIVESYNCALLOWED_KEY, "");
        updateMailAccessParams(inputParams,
                CosError.COS_INVALID_MOBILE_ACTIVE_SYNC_ALLOWED.name());

    }

    @Test
    public void testMobileActiveSyncAllowedSuccess() throws Exception {
        System.out
                .println("CosMailAccessTest.testMobileActiveSyncAllowedSuccess...");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MOBILEACTIVESYNCALLOWED_KEY,
                BooleanType.YES.toString());
        updateMailAccessParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        BooleanType pa = getMailAccessParams(inputParams)
                .getMobileActiveSyncAllowed();
        assertNotNull("MobileActiveSyncAllowed is null.", pa);
        assertTrue("MobileActiveSyncAllowed has a wrong value.",
                pa.equals(BooleanType.YES));

    }

}
