package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.RejectActionType;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISenderBlockingService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailReceiptSenderBlockingPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptSenderBlockingGET";
    private static final String ARROW_SEP = " --> ";
    private static ISenderBlockingService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (ISenderBlockingService) ContextUtils.loadContext()
                .getService(ServiceEnum.SenderBlockingService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static SenderBlocking getParams() {
        SenderBlocking object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    // senderBlockingAllowed
    @Test
    public void testSenderBlockingAllowedNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedNullParam");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlockingAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedEmptyParam");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlockingAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedInvalidParam");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SENDER_BLOCKING_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlockingAllowedSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedSplCharsInParam");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SENDER_BLOCKING_ALLOWED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlockingAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedSuccess");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSenderBlockingAllowed();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlockingAllowedSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlockingAllowedSuccess1");
        String key = MailboxProperty.senderBlockingAllowed.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSenderBlockingAllowed();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // senderBlokingEnabled
    @Test
    public void testSenderBlokingEnabledNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledNullParam");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlokingEnabledEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledEmptyParam");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlokingEnabledInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledInvalidParam");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, "sadfasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SENDER_BLOCKING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlokingEnabledSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledSplCharsInParam");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_SENDER_BLOCKING_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlokingEnabledSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledSuccess");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSenderBlockingEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testSenderBlokingEnabledSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSenderBlokingEnabledSuccess1");
        String key = MailboxProperty.senderBlockingEnabled.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getSenderBlockingEnabled();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // rejectAction
    @Test
    public void testRejectActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionNullParam");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionEmptyParam");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionInvalidParam");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, "sadfasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_REJECT_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testRejectActionSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionSplCharsInParam");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_REJECT_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testRejectActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionSuccess");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, RejectActionType.DROP.name());
        updateParams(updateParams);
        RejectActionType value = getParams().getRejectAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, RejectActionType.DROP);
        updateParams.remove(key);
    }

    @Test
    public void testRejectActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectActionSuccess1");
        String key = MailboxProperty.rejectAction.name();
        addToParams(updateParams, key, RejectActionType.FORWARD.name());
        updateParams(updateParams);
        RejectActionType value = getParams().getRejectAction();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, RejectActionType.FORWARD);
        updateParams.remove(key);
    }

    // rejectFolder
    @Test
    public void testRejectFolderNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectFolderNullParam");
        String key = MailboxProperty.rejectFolder.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectFolderEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectFolderEmptyParam");
        String key = MailboxProperty.rejectFolder.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectFolderInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectFolderInvalidParam");
        String key = MailboxProperty.rejectFolder.name();
        addToParams(updateParams, key, "sadfasf");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectFolderSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectFolderSplCharsInParam");
        String key = MailboxProperty.rejectFolder.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testRejectFolderSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testRejectFolderSuccess");
        String key = MailboxProperty.rejectFolder.name();
        String val = "FOLDER1";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getRejectFolder();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }

    // blockedSendersPABActive
    @Test
    public void testBlockedSendersPABActiveNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveNullParam");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSendersPABActiveEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveEmptyParam");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSendersPABActiveInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveInvalidParam");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, "asdasdaasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BLOCK_SENDERS_PAB_ACTIVE.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSendersPABActiveSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveSplCharsInParam");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BLOCK_SENDERS_PAB_ACTIVE.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSendersPABActiveSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveSuccess");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getBlockSendersPABActive();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testBlockedSendersPABActiveSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockedSendersPABActiveSuccess1");
        String key = MailboxProperty.blockSendersPABActive.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getBlockSendersPABActive();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }

    // blockedSendersPABAccess
    @Test
    public void testBlockSendersPABAccessNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessNullParam");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockSendersPABAccessEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessEmptyParam");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testBlockSendersPABAccessInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessInvalidParam");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, "dasdasd");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BLOCK_SENDERS_PAB_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockSendersPABAccessSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessSplCharsInParam");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, "!@#$%^&");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_BLOCK_SENDERS_PAB_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testBlockSendersPABAccessSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessSuccess");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, BooleanType.NO.name());
        updateParams(updateParams);
        BooleanType value = getParams().getBlockSendersPABAccess();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.NO);
        updateParams.remove(key);
    }

    @Test
    public void testBlockSendersPABAccessSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testBlockSendersPABAccessSuccess1");
        String key = MailboxProperty.blockSendersPABAccess.name();
        addToParams(updateParams, key, BooleanType.YES.name());
        updateParams(updateParams);
        BooleanType value = getParams().getBlockSendersPABAccess();
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, BooleanType.YES);
        updateParams.remove(key);
    }
}
