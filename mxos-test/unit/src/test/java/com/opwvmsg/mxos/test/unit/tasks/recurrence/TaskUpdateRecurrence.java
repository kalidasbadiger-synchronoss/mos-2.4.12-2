/**
 * 
 */
package com.opwvmsg.mxos.test.unit.tasks.recurrence;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksRecurrenceService;

/**
 * @author bgh21604
 * 
 */
public class TaskUpdateRecurrence {
    private static IMxOSContext context;
    private static ExternalSession oxSession;
    long taskId;
    private static final String baseURL = "http://localhost:8080/mxos";
    private static final String userName = "vinayaka@openwave.com";
    private static final String password = "test";

    // Recurrence attributes
    // type=Yearly&startDate=2013-10-30T17:26:54Z&dueDate=2013-11-30T17:26:54Z
    // &recurAfterInterval=1&daysInWeek=Sunday&dayInMonth=5&monthInYear=9&occurrences=3&notification=true
    private static String type = "Weekely";
    private static final String startDate = "2013-10-30T17:26:54Z";
    private static final String dueDate = "2013-11-30T17:26:54Z";
    private static final List<String> daysIneekList = new ArrayList<String>();

    private static final String dayInMonth = "2";
    private static final String monthInYear = "9";
    private static final String recurAfterInterval = "2";
    private static final String endDate = "2013-11-30T17:26:54Z";
    private static final String notification = "true";
    private static final String occurrences = "9";

    private static void createContext() throws MxOSException {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.REST.name());
        p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Tasks");
        p.setProperty(ContextProperty.MXOS_BASE_URL, baseURL);
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "5");
        p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
        p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
        context = MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO",
                p);
    }

    private static void login() throws MxOSException {
        System.out.println("Login() called");
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(),
                Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.password.name(), Arrays.asList(password));
        oxSession = service.login(requestMap);
        assertTrue("Login suceeded.", oxSession.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("This is setUpBeforeClass");
        try {
            createContext();
            login();
        } catch (MxOSException e) {
            assertTrue("Login Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("This is tearDownAfterClass");
    }

    @Test
    public void taskUpdateRecurrenceDaily() {

        daysIneekList.add("Wednesday");
        daysIneekList.add("Monday");
        daysIneekList.add("Sunday");

        type = "Daily";
        taskId = 2;

        ITasksRecurrenceService service = null;
        try {
            service = (ITasksRecurrenceService) context
                    .getService(ServiceEnum.TasksRecurrenceService.name());
        } catch (MxOSException e) {
            assertTrue("taskRecurrenceService Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(),
                Arrays.asList(String.valueOf(taskId)));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        requestMap.put(TasksProperty.type.name(), Arrays.asList(type));
        requestMap
                .put(TasksProperty.startDate.name(), Arrays.asList(startDate));
        requestMap.put(TasksProperty.dueDate.name(), Arrays.asList(dueDate));
        requestMap.put(TasksProperty.daysInWeek.name(), daysIneekList);

        requestMap.put(TasksProperty.dayInMonth.name(),
                Arrays.asList(dayInMonth));
        requestMap.put(TasksProperty.recurAfterInterval.name(),
                Arrays.asList(recurAfterInterval));
        requestMap.put(TasksProperty.endDate.name(), Arrays.asList(endDate));
        requestMap.put(TasksProperty.notification.name(),
                Arrays.asList(notification));
        requestMap.put(TasksProperty.occurrences.name(),
                Arrays.asList(occurrences));

        try {
            service.update(requestMap);
        } catch (MxOSException e) {
            // assertTrue("taskRecurrenceRead Failure. "+e.getMessage(),
            // e.getMessage() != null);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void taskUpdateRecurrenceWeekely() {

        daysIneekList.add("Friday");
        daysIneekList.add("Monday");
        daysIneekList.add("Sunday");

        type = "Weekely";
        taskId = 28;

        ITasksRecurrenceService service = null;
        try {
            service = (ITasksRecurrenceService) context
                    .getService(ServiceEnum.TasksRecurrenceService.name());
        } catch (MxOSException e) {
            assertTrue("taskRecurrenceService Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(),
                Arrays.asList(String.valueOf(taskId)));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        requestMap.put(TasksProperty.type.name(), Arrays.asList(type));
        requestMap
                .put(TasksProperty.startDate.name(), Arrays.asList(startDate));
        requestMap.put(TasksProperty.dueDate.name(), Arrays.asList(dueDate));
        requestMap.put(TasksProperty.daysInWeek.name(), daysIneekList);

        requestMap.put(TasksProperty.dayInMonth.name(),
                Arrays.asList(dayInMonth));
        requestMap.put(TasksProperty.recurAfterInterval.name(),
                Arrays.asList(recurAfterInterval));
        requestMap.put(TasksProperty.endDate.name(), Arrays.asList(endDate));
        requestMap.put(TasksProperty.notification.name(),
                Arrays.asList(notification));
        requestMap.put(TasksProperty.occurrences.name(),
                Arrays.asList(occurrences));

        try {
            service.update(requestMap);
        } catch (MxOSException e) {
            // assertTrue("taskRecurrenceRead Failure. "+e.getMessage(),
            // e.getMessage() != null);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void taskUpdateRecurrenceMonthly() {

        daysIneekList.add("Tuesday");
        daysIneekList.add("Monday");
        daysIneekList.add("Sunday");

        type = "Monthly";
        taskId = 33;

        ITasksRecurrenceService service = null;
        try {
            service = (ITasksRecurrenceService) context
                    .getService(ServiceEnum.TasksRecurrenceService.name());
        } catch (MxOSException e) {
            assertTrue("taskRecurrenceService Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(),
                Arrays.asList(String.valueOf(taskId)));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        requestMap.put(TasksProperty.type.name(), Arrays.asList(type));
        requestMap
                .put(TasksProperty.startDate.name(), Arrays.asList(startDate));
        requestMap.put(TasksProperty.dueDate.name(), Arrays.asList(dueDate));
        requestMap.put(TasksProperty.daysInWeek.name(), daysIneekList);

        requestMap.put(TasksProperty.dayInMonth.name(),
                Arrays.asList(dayInMonth));
        requestMap.put(TasksProperty.recurAfterInterval.name(),
                Arrays.asList(recurAfterInterval));
        requestMap.put(TasksProperty.endDate.name(), Arrays.asList(endDate));
        requestMap.put(TasksProperty.notification.name(),
                Arrays.asList(notification));
        requestMap.put(TasksProperty.occurrences.name(),
                Arrays.asList(occurrences));

        try {
            service.update(requestMap);
        } catch (MxOSException e) {
            // assertTrue("taskRecurrenceRead Failure. "+e.getMessage(),
            // e.getMessage() != null);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    @Test
    public void taskUpdateRecurrenceYearly() {

        daysIneekList.add("Tuesday");
        daysIneekList.add("Thursday");
        daysIneekList.add("Sunday");

        type = "Yearly";
        taskId = 36;

        ITasksRecurrenceService service = null;
        try {
            service = (ITasksRecurrenceService) context
                    .getService(ServiceEnum.TasksRecurrenceService.name());
        } catch (MxOSException e) {
            assertTrue("taskRecurrenceService Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(),
                Arrays.asList(String.valueOf(taskId)));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        requestMap.put(TasksProperty.type.name(), Arrays.asList(type));
        requestMap
                .put(TasksProperty.startDate.name(), Arrays.asList(startDate));
        requestMap.put(TasksProperty.dueDate.name(), Arrays.asList(dueDate));
        requestMap.put(TasksProperty.daysInWeek.name(), daysIneekList);

        requestMap.put(TasksProperty.dayInMonth.name(),
                Arrays.asList(dayInMonth));
        requestMap.put(TasksProperty.monthInYear.name(),
                Arrays.asList(monthInYear));
        requestMap.put(TasksProperty.recurAfterInterval.name(),
                Arrays.asList(recurAfterInterval));
        requestMap.put(TasksProperty.endDate.name(), Arrays.asList(endDate));
        requestMap.put(TasksProperty.notification.name(),
                Arrays.asList(notification));
        requestMap.put(TasksProperty.occurrences.name(),
                Arrays.asList(occurrences));

        try {
            service.update(requestMap);
        } catch (MxOSException e) {
            // assertTrue("taskRecurrenceRead Failure. "+e.getMessage(),
            // e.getMessage() != null);
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}