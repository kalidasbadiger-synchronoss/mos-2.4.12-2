package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class VampSendSmsTest {

    private static final String SMSTYPE_SMSONLINE = "smsonline";
    private static final String SMSTYPE_BASICNOTIFICATION = "basicnotification";

    private static final String FROM_ADDRESS_VALUE = "+19886576790";

    private static final String TO_ADDRESS_VALUE_SUCCESS = "+19886576790";
    private static final String TO_ADDRESS_VALUE_TEMPERROR = "+19916923750";
    private static final String TO_ADDRESS_VALUE_PERMERROR = "+18008805766";
    private static final String TO_ADDRESS_VALUE_INVALIDXML = "+19393828866";
    private static final String TO_ADDRESS_VALUE_INVALIDERRCODE = "+08041067791";
    
    private static final String TO_ADDRESS_VALUE_SNMP = "+19986600571";

    private static final String MESSAGE_VALUE = "Hi Message";

    private static ISendSMSService sendSmsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("VampSendSmsTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        sendSmsService = (ISendSMSService) ContextUtils.loadContext()
                .getService(ServiceEnum.SendSmsService.name());
        assertNotNull("MailboxService object is null.", sendSmsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("VampSendSmsTest.tearDownAfterClass...");
        params.clear();
        params = null;
        sendSmsService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testSendSms_Success() {
        System.out.println("VampSendSmsTest.Success...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (Exception e) {
            e.printStackTrace();
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_VampTempError() {
        System.out.println("VampSendSmsTest.VampTempError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_TEMPERROR);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_VampPermanentError() {
        System.out.println("VampSendSmsTest.VampPermanentError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_PERMERROR);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }
    
    
    @Test
    public void testSendSms_VampPermanentSNMPError() {
        System.out.println("VampSendSmsTest.VampPermanentSNMPError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SNMP);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERM_SNMP_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_InValidXMLFromVamp() {
        System.out.println("VampSendSmsTest.InValidXMLFromVamp...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_INVALIDXML);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_inValidStatusFromVamp() {
        System.out.println("VampSendSmsTest.inValidStatusFromVamp...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_INVALIDERRCODE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_SmsTypeNotSent() {
        System.out.println("VampSendSmsTest.SmsTypeNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_fromAddressNotSent() {
        System.out.println("VampSendSmsTest.fromAddressNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressNotSent() {
        System.out.println("VampSendSmsTest.toAddressNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_messageNotSent() {
        System.out.println("VampSendSmsTest.messageNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidSmsTypeSent() {
        System.out.println("VampSendSmsTest.invalidSmsTypeSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(), "sdfsdfs");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
        }
    }


    @Test
    public void testSendSms_invalidCharactersinsmsType() {
        System.out.println("VampSendSmsTest.invalidCharactersinsmsType...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(), "!@#$%^&*()");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidfromAddress() {
        System.out.println("VampSendSmsTest.invalidfromAddress...");

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, SmsProperty.smsType.name(), SMSTYPE_SMSONLINE);
        addParams(inputParams, SmsProperty.fromAddress.name(), "sdfsdf");
        addParams(inputParams, SmsProperty.toAddress.name(),
                TO_ADDRESS_VALUE_SUCCESS);
        addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
        try {
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }

    }

    @Test
    public void testSendSms_fromAddressisnull() {
        System.out.println("VampSendSmsTest.fromAddressisnull...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(), "");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(),
                    e.getCode());
        }
    }

    @Test
    public void testSendSms_fromAddressContainsInvalidCharacters() {
        System.out
                .println("VampSendSmsTest.fromAddressCntainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(), "!@#$%^&*(");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidtoAddress() {
        System.out.println("VampSendSmsTest.invalidtoAddress...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(), "sdfsdf");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_TO_ADDRESS.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressisnull() {
        System.out.println("VampSendSmsTest.toAddressisnull...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(), "");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressContainsInvalidCharacters() {
        System.out
                .println("VampSendSmsTest.fromAddressCntainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(), "!@#$%^&*(");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_TO_ADDRESS.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_messageIsNUll() {
        System.out.println("VampSendSmsTest.messageIsNUll...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "");
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_messageContainsInvalidCharacters() {
        System.out
                .println("VampSendSmsTest.messageContainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "@!#$%^&*(");
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_optionalParamsSent() {
        System.out.println("VampSendSmsTest.optionalParamsSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, SmsProperty.pid.name(), "23424");
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_additionalParamsSent() {
        System.out.println("VampSendSmsTest.additionalParamsSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, "sdfsdf", "sdfsdf");
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

}
