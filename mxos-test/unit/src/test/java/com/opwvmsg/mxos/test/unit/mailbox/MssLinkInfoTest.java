package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMssLinkInfoService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MssLinkInfoTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100005@openwave.com";
    private static final String PWD = "test1";
    private static final String COSID = "cos_2.0_ut202";
    private static IMxOSContext context;
    private static IMssLinkInfoService mssLinkInfoService;

    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
  
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MssLinkInfoTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mssLinkInfoService = (IMssLinkInfoService) context
                .getService(ServiceEnum.MssLinkInfoService.name());
        assertNotNull("MssLinkInfoService object is null.", mssLinkInfoService);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PWD, COSID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MssLinkInfoTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MssLinkInfoTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MssLinkInfoTest.tearDownAfterClass...");
        inputParams.clear();
        // Delete the mailbox if already exists
        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);
        inputParams = null;
        mssLinkInfoService = null;
    }

    @Test
    public void testGetMssLinkInfo() throws Exception {
        System.out.println("MssLinkInfoTest.testGetMssLinkInfo...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        MssLinkInfo info = null;
        try {
            info = mssLinkInfoService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("MssLinkInfo object is null.", info);
        System.out.println("MssLinkInfo = " + info);
    }

    @Test
    public void testGetMssLinkInfoWithoutEmail() throws Exception {
        System.out.println("MssLinkInfoTest.testGetMssLinkInfoWithoutEmail...");
        inputParams.clear();
        MssLinkInfo info = null;
        try {
            info = mssLinkInfoService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_GET_MISSING_PARAMS",
                    e.getCode().equalsIgnoreCase(
                            ErrorCode.GEN_BAD_REQUEST.name()));
        }
        assertNull(info);
    }

    @Test
    public void testGetMssLinkInfoInvalidEmail() throws Exception {
        System.out.println("MssLinkInfoTest.testGetMssLinkInfoInvalidEmail...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add("temp.comskdjf");
        MssLinkInfo info = null;
        try {
            info = mssLinkInfoService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_INVALID_EMAIL",
                    e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_INVALID_EMAIL.name()));
        }
        assertNull(info);
    }

}
