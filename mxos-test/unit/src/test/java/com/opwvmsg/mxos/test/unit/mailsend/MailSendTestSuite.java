package com.opwvmsg.mxos.test.unit.mailsend;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
//Include all the test case classes related to MailSend 
@SuiteClasses({
    MailSendGET.class,
    MailSendPOST.class,
    MailSendBmiFiltersGET.class,
    MailSendBmiFiltersPOST.class,
    MailSendCommtouchFiltersGET.class,
    MailSendCommtouchFiltersPOST.class,
    MailSendMcAfeeFiltersGET.class,
    MailSendMcAfeeFiltersPOST.class,
    MailSendNumDelayedDeliveryMessagesPendingTest.class
})
public class MailSendTestSuite {
}
