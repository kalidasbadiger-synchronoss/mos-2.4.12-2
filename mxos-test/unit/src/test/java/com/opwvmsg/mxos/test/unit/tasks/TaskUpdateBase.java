/**
 * 
 */
package com.opwvmsg.mxos.test.unit.tasks;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksBaseService;

/**
 * @author bgh21604
 * 
 */
public class TaskUpdateBase {
    private static IMxOSContext context;
    private static ExternalSession oxSession;
    private static final String baseURL = "http://localhost:8080/mxos";
    private static final String userName = "vinayaka@openwave.com";
    private static final String taskId = "1";
    private static final String password = "test";

    private static final String isPrivate = "no";
    private static final String updatename = "vinay";
    private static final String updateowner = "Vin";
    private static final String updatePriority = "medium";
    private static final String updateStatus = "done";
    private static final String updateColorLabel = "6";
    private static final String updateCategories = "xyz";
    private static final String updateNotes = "Updated from JUnit";
    private static final String updateStartDate = "2013-10-16T09:24:15Z";
    private static final String updateDueDate = "2013-10-16T09:24:15Z";
    private static final String updateReminderDate = "2013-10-16T09:24:15Z";
    private static final String updateProgress = "10";

    private static void createContext() throws MxOSException {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.REST.name());
        p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Tasks");
        p.setProperty(ContextProperty.MXOS_BASE_URL, baseURL);
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "5");
        p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
        p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
        context = MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO",
                p);
    }

    private static void login() throws MxOSException {
        System.out.println("Login() called");
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(),
                Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.password.name(), Arrays.asList(password));
        oxSession = service.login(requestMap);
        assertTrue("Login suceeded.", oxSession.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("This is setUpBeforeClass");
        try {
            createContext();
            login();
        } catch (MxOSException e) {
            assertTrue("Login Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("This is tearDownAfterClass");
    }

    @Test
    public void testUpdateTask() {
        System.out.println("testUpdateTask() started...");
        ITasksBaseService service = null;
        try {
            service = (ITasksBaseService) context
                    .getService(ServiceEnum.TasksBaseService.name());
        } catch (MxOSException e) {
            assertTrue("Task Update Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.taskId.name(), Arrays.asList(taskId));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        requestMap
                .put(TasksProperty.isPrivate.name(), Arrays.asList(isPrivate));
        requestMap.put(TasksProperty.name.name(), Arrays.asList(updatename));
        requestMap
                .put(TasksProperty.status.name(), Arrays.asList(updateStatus));
        requestMap.put(TasksProperty.owner.name(), Arrays.asList(updateowner));
        requestMap.put(TasksProperty.categories.name(),
                Arrays.asList(updateCategories));
        requestMap.put(TasksProperty.priority.name(),
                Arrays.asList(updatePriority));
        requestMap.put(TasksProperty.colorLabel.name(),
                Arrays.asList(updateColorLabel));
        requestMap.put(TasksProperty.notes.name(), Arrays.asList(updateNotes));
        requestMap.put(TasksProperty.progress.name(),
                Arrays.asList(updateProgress));
        requestMap.put(TasksProperty.startDate.name(),
                Arrays.asList(updateStartDate));
        requestMap.put(TasksProperty.dueDate.name(),
                Arrays.asList(updateDueDate));
        requestMap.put(TasksProperty.reminderDate.name(),
                Arrays.asList(updateReminderDate));

        try {
            service.update(requestMap);
        } catch (MxOSException e) {
            assertTrue("Task update Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        System.out.println("Task updated ");
    }
}