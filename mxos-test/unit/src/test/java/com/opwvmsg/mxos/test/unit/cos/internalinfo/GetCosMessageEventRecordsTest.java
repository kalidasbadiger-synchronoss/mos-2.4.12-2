package com.opwvmsg.mxos.test.unit.cos.internalinfo;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.EventRecordingType;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMessageEventRecordsService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetCosMessageEventRecordsTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosMessageEventRecordsService mers;
    private static ICosService cs;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.setUpBeforeClass...");
        mers = (ICosMessageEventRecordsService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosMessageEventRecordsService.name());
        cs = (ICosService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        params.put(COSID_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.setUp...");
        assertNotNull("MessageEventRecords object is null.", mers);
        assertNotNull("Cos object is null.", cs);
        assertNotNull("Input Param:cosId is null.", params.get(COSID_KEY));
        params.get(COSID_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !params.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.tearDown...");
        params.get(COSID_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.tearDownAfterClass...");
        deleteCos(COS_ID);
        params.clear();
        params = null;
        mers = null;
        cs = null;
    }

    private static MessageEventRecords getCosMsgEventRecordsParams(
            Map<String, List<String>> params) {
        return getCosMsgEventRecordsParams(params, null);
    }

    private static MessageEventRecords getCosMsgEventRecordsParams(
            Map<String, List<String>> params, String expectedError) {
        MessageEventRecords mer = null;
        try {
            mer = mers.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MessageEventRecords object is null.", mer);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(COSID_KEY, new ArrayList<String>());
            }
        }
        return mer;
    }

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cs.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cs.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testGetMsgEventRecordsWithoutCosId() {
        System.out
                .println("GetCosMessageEventRecordsTest.testGetMsgEventRecordsWithoutCosId...");
        // Clear the params.
        params.clear();
        getCosMsgEventRecordsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(COSID_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetMsgEventRecordsWithNullCosId() {
        System.out
                .println("GetCosMessageEventRecordsTest.testGetMsgEventRecordsWithNullCosId...");
        params.get(COSID_KEY).clear();
        getCosMsgEventRecordsParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetMsgEventRecordsWithNonExistingCosId() {
        System.out
                .println("GetCosMessageEventRecordsTest.testGetMsgEventRecordsWithNonExistingCosId...");
        params.get(COSID_KEY).clear();
        params.get(COSID_KEY).add("something_junk_cos_1234");
        getCosMsgEventRecordsParams(params, CosError.COS_NOT_FOUND.name());
    }

    @Test
    public void testEventRecordingEnabledSuccess() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.testEventRecordingEnabledSuccess...");
        EventRecordingType ert = getCosMsgEventRecordsParams(params).getEventRecordingEnabled();
        assertNotNull("EventRecordingEnabled is null.", ert);
        assertTrue("EventRecordingEnabled has a wrong value.",
                ert.equals(EventRecordingType.NEVER));
    }

    @Test
    public void testMaxHeaderLengthSuccess() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.testMaxHeaderLengthSuccess...");
        Integer headerLength = getCosMsgEventRecordsParams(params).getMaxHeaderLength();
        assertNotNull("MaxHeaderLength is null.", headerLength);
        assertTrue("MaxHeaderLength has a wrong value.", headerLength == 0);
    }

    @Test
    public void testMaxFilesSizeKBSuccess() throws Exception {
        System.out.println("GetCosMessageEventRecordsTest.testMaxFilesSizeKBSuccess...");
        Integer filesSize = getCosMsgEventRecordsParams(params).getMaxFilesSizeKB();
        assertNotNull("MaxFilesSizeKB is null.", filesSize);
        assertTrue("MaxFilesSizeKB has a wrong value.", filesSize == 0);
    }

}
