package com.opwvmsg.mxos.test.unit.mailbox;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailboxBaseTest {
    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "mx100005@openwave.com";
    private static final String PWD = "test1";
    private static final String COSID = "cos_2.0_ut202";
    private static IMxOSContext context;
    private static IMailboxService mailboxService;
    private static IMailboxBaseService mailboxBaseService;
    private static Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
    String maxAllowedDomain = "20";
    String firstName="Foooo";
    String lastName="Barrrrrr";   
    /**
     *
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("MailboxBaseTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxService = (IMailboxService) context
                .getService(ServiceEnum.MailboxService.name());
        mailboxBaseService = (IMailboxBaseService) context
                .getService(ServiceEnum.MailboxBaseService.name());
        assertNotNull("MailboxService object is null.", mailboxService);
        assertNotNull("MailboxBaseService object is null.", mailboxBaseService);
        CosHelper.createCos(COSID);
        MailboxHelper.createMailbox(EMAIL, PWD, COSID);
        //assertTrue("Mailbox NOT Created", (MAILBOX_ID > 0));
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("MailboxBaseTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("MailboxBaseTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("MailboxBaseTest.tearDownAfterClass...");
        inputParams.clear();
        // Delete the mailbox if already exists
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        try {
            mailboxService.delete(inputParams);
        } catch (MxOSException e) {
            fail();
        }
        MailboxHelper.deleteMailbox(EMAIL);
        CosHelper.deleteCos(COSID);
        inputParams = null;
        mailboxService = null;
        mailboxBaseService = null;
    }

    @Test
    public void testGetMailboxBase() throws Exception {
        System.out.println("MailboxBaseTest.testGetMailboxBase...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Mailbox Base object is null.", base);
        System.out.println("Mailbox Base = " + base);
    }

    @Test
    public void testGetMailboxBaseWithoutEmail() throws Exception {
        System.out.println("MailboxBaseTest.testGetMailboxBaseWithoutEmail...");
        inputParams.clear();
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_GET_MISSING_PARAMS",
                    e.getCode().equalsIgnoreCase(
                            ErrorCode.GEN_BAD_REQUEST.name()));
        }
        assertNull(base);
    }

    @Test
    public void testGetMailboxBaseInvalidEmail() throws Exception {
        System.out.println("MailboxBaseTest.testGetMailboxBaseInvalidEmail...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add("temp.comskdjf");
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_INVALID_EMAIL",
                    e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_INVALID_EMAIL.name()));
        }
        assertNull(base);
    }
    @Test
    public void testGetMailboxBaseInvalidLongEmail() throws Exception {
        System.out.println("MailboxBaseTest.testGetMailboxBaseInvalidLongEmail...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams
                .get(EMAIL_KEY)
                .add("abababababababababababababababababababababababababababababababab");
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_INVALID_EMAIL",
                    e.getCode().equalsIgnoreCase(
                            MailboxError.MBX_INVALID_EMAIL.name()));
        }
        assertNull(base);
    }

    @Test
    public void testUpdateUserName() throws Exception {
        System.out.println("MailboxBaseTest.testUpdateUserName...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.userName.name(), new ArrayList<String>());
        String newUserName = "testuser201new";
        inputParams.get(MailboxProperty.userName.name()).add(newUserName);
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getUserName().contains(newUserName));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Exception Occured",true);
        }
    }
    
    @Test
    public void testMaxNumAllowedDomainUpdate() throws Exception {
        System.out.println("testMaxNumAllowedDomainUpdate starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add("basemailbox1@openwave.com");
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add(maxAllowedDomain);
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add("basemailbox1@openwave.com");
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            int intMaxAllowedDomain = Integer.parseInt(maxAllowedDomain);
            assertTrue(base.getMaxNumAllowedDomains().equals(intMaxAllowedDomain));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Exception Occured",true);
        }
    }
    
   /* @Test
    public void testPasswordStoreTypeUpdateLowerCase() throws Exception {
        System.out.println("testPasswordStoreTypeUpdateLowerCase starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.password.name()).add("test");
        inputParams.put(MailboxProperty.passwordStoreType.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordStoreType.name()).add(
                MxosEnums.PasswordStoreType.SHA_1.toString().toLowerCase());
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail("This should not have come.");
        }
    }

    @Test
    public void testPasswordStoreTypeUpdateUpperCase() throws Exception {
        System.out.println("testPasswordStoreTypeUpdateUpperCase starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.password.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.password.name()).add("test");
        inputParams.put(MailboxProperty.passwordStoreType.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.passwordStoreType.name()).add(
                                MxosEnums.PasswordStoreType.CLEAR.name().toUpperCase());
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail("This should not have come.");
        }
    }*/

    @Test
    public void testMaxNumAllowedDomainRead() {
        System.out.println("testMaxNumAllowedDomainRead starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getMaxNumAllowedDomains() >= 0);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMaxNumAllowedDomainUpdateNull() {
        System.out.println("testMaxNumAllowedDomainUpdateNull starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add(null);
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }

    @Test
    public void testMaxNumAllowedDomainUpdateNeg() {
        System.out.println("testMaxNumAllowedDomainUpdateNeg starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add("");
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testMaxNumAllowedDomainUpdateNeg2() {
        System.out.println("testMaxNumAllowedDomainUpdateNeg2 starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add("basemailbox1@openwave.com");
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add("Illegal");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: "+e);
        }
    }
    @Test
    public void testFirstNameUpdate() throws Exception {
        System.out.println("testFirstNameUpdate starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.firstName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.firstName.name()).add(firstName);
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getFirstName().equals(firstName));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Exception Occured",true);
        }
    }
    
    @Test
    public void testFirstNameRead() {
        System.out.println("testFirstNameRead starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getFirstName()!= null);
        } catch (Exception e) {
            fail();
        }
    }
    
    @Test
    public void testNotifStatusUpdate() throws Exception {
        System.out.println("testNotifStatusUpdate starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add("2");
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getNotificationMailSentStatus().equals("2"));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS",
                 e.getCode().equalsIgnoreCase(
                       MailboxError.MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS.name()));
        }
    }
    
    @Test
    public void testNotifStatusReset() throws Exception {
        System.out.println("testNotifStatusReset starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add("0");
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getNotificationMailSentStatus().equals("0"));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue(
                    "Error is MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS",
                    e.getCode()
                            .equalsIgnoreCase(
                                    MailboxError.MBX_UNABLE_TO_SET_MAIL_NOTIFICATION_SENT_STATUS
                                            .name()));
        }
    }
     

    @Test
    public void testFirstNameUpdateNull() {
        System.out.println("testFirstNameUpdateNull starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.firstName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.firstName.name()).add(null);
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
        	assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: "+e);
        }
    }

    @Test
    public void testFirstNameUpdateNeg() {
        System.out.println("testFirstNameUpdateNeg starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.firstName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.firstName.name()).add("");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
        	assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: "+e);
        }
    }
    
    @Test
    public void testFirstNameUpdateNeg2() {
        System.out.println("testFirstNameUpdateNeg2 starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.firstName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.firstName.name()).add("abc");
        try {
            mailboxBaseService.update(inputParams);
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
            fail();
        }
    }
    
    @Test
    public void testFirstNameUpdateNeg3() {
        System.out.println("testFirstNameUpdateNeg3 starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add("abc@xyz.com");
        try {
            mailboxBaseService.update(inputParams);
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
            fail();
        }
    }

   @Test
    public void testLastNameUpdate() throws Exception {
        System.out.println("testLastNameUpdate starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add(lastName);
        Base base = null;
        try {
            mailboxBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(EMAIL_KEY, new ArrayList<String>());
            inputParams.get(EMAIL_KEY).add(EMAIL);
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getLastName().equals(lastName));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Exception Occured",true);
        }
    }
    
    @Test
    public void testLastNameRead() {
        System.out.println("testLastNameRead starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        Base base = null;
        try {
            base = mailboxBaseService.read(inputParams);
            assertNotNull(base);
            assertTrue(base.getLastName()!= null);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testLastNameUpdateNull() {
        System.out.println("testLastNameUpdateNull starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add(null);
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
        	assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: "+e);
        }
    }

    @Test
    public void testLastNameUpdateNeg() {
        System.out.println("testLastNameUpdateNeg starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add("");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
        	assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: "+e);
        }
    }
    
    @Test
    public void testLastNameUpdateNeg2() {
        System.out.println("testLastNameUpdateNeg2 starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add("abc");
        try {
            mailboxBaseService.update(inputParams);
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
            fail();
        }
    }
    @Test
    public void testLastNameUpdateNeg3() {
        System.out.println("testLastNameUpdateNeg3 starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.lastName.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.lastName.name()).add("abc@xyz.com");
        try {
            mailboxBaseService.update(inputParams);
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
            fail();
        }
    }
    
    @Test
    public void testNotificationMailSentStatus() throws Exception {
        System.out.println("testNotificationMailSentStatus starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add("sentForInactiveStatus");
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            fail();
        }
    }
    
    @Test
    public void testInvalidNotificationMailSentStatus() throws Exception {
        System.out.println("testInvalidNotificationMailSentStatus starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.notificationMailSentStatus.name(), new ArrayList<String>());
        //inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add(null);
        inputParams.get(MailboxProperty.notificationMailSentStatus.name()).add("Sent");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println("Error: "+e.getCode());
        }
    }
    
    @Test
    public void testExtensionAttributes() throws Exception {
        System.out.println("testExtensionAttributes starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.extensionAttributes.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.extensionAttributes.name()).add("postalAddress:openwave|telephoneNumber:9886123456");
        try {
            mailboxBaseService.update(inputParams);
        } catch (MxOSException e) {
            System.out.println(e.getMessage());
            fail();
        }
    }
    
    @Test
    public void testExtensionAttributesFailWithoutKey() throws Exception {
        System.out.println("testExtensionAttributesFailWithoutKey starting...");
        inputParams.clear();
        inputParams.put(EMAIL_KEY, new ArrayList<String>());
        inputParams.get(EMAIL_KEY).add(EMAIL);
        inputParams.put(MailboxProperty.extensionAttributes.name(), new ArrayList<String>());
        inputParams.get(MailboxProperty.extensionAttributes.name()).add(":openwave|telephoneNumber:9886123456");
        try {
            mailboxBaseService.update(inputParams);
            fail();
        } catch (MxOSException e) {
            System.out.println(e.getCode());
        }
    }
}
