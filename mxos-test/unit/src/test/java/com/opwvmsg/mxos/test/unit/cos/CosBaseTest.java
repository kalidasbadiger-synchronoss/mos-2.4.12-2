package com.opwvmsg.mxos.test.unit.cos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosBaseService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * @author mxos-dev
 */
public class CosBaseTest {
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String cosId = "cos_2.0_ut100";
    private static IMxOSContext context;
    private static ICosService cosService;
    private static ICosBaseService cosBaseService;
    private static Map<String, List<String>> inputParams =
        new HashMap<String, List<String>>();

    private static String maxAllowedDomain = "10";

    /**
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosBaseTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosService = (ICosService) context.getService(ServiceEnum.CosService
                .name());
        cosBaseService = (ICosBaseService) context
                .getService(ServiceEnum.CosBaseService.name());
        assertNotNull("CosService object is null.", cosService);
        assertNotNull("CosBaseService object is null.", cosBaseService);

    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("setUp...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.create(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("tearDown...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CosBaseTest.tearDownAfterClass...");
        inputParams = null;
        cosService = null;
        cosBaseService = null;
    }

    @Test
    public void testGetCosBase() throws Exception {
        System.out.println("testGetCosBase...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        Base base = null;
        try {
            base = cosBaseService.read(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("Mailbox Base object is null.", base);
        System.out.println("Mailbox Base = " + base);
    }

    @Test
    public void testGetCosBaseWithoutEmail() throws Exception {
        System.out.println("testGetCosBaseWithoutEmail...");
        inputParams.clear();
        Base base = null;
        try {
            base = cosBaseService.read(inputParams);
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not GEN_BAD_REQUEST", e.getCode()
                    .equalsIgnoreCase(ErrorCode.GEN_BAD_REQUEST.name()));
        }
        assertNull(base);
    }

    @Test
    public void testGetCosBaseInvalidEmail() throws Exception {
        System.out.println("testGetCosBaseInvalidEmail...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add("temp.+$@#KSDcomskdjf");
        Base base = null;
        try {
            base = cosBaseService.read(inputParams);
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not COS_INVALID_COSID", e.getCode()
                    .equalsIgnoreCase(CosError.COS_INVALID_COSID.name()));
        }
        assertNull(base);
    }

    @Test
    public void testGetCosBaseNotFound() throws Exception {
        System.out.println("testGetCosBaseNotFound...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add("temp.comskdjf");
        Base base = null;
        try {
            base = cosBaseService.read(inputParams);
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not COS_NOT_FOUND", e.getCode()
                    .equalsIgnoreCase(CosError.COS_NOT_FOUND.name()));
        }
        assertNull(base);
    }

    @Test
    public void testMaxNumAllowedDomainUpdate() throws Exception {
        System.out.println("testMaxNumAllowedDomainUpdate starting...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add(
                maxAllowedDomain);
        Base base = null;
        try {
            cosBaseService.update(inputParams);
            inputParams.clear();
            inputParams.put(COSID_KEY, new ArrayList<String>());
            inputParams.get(COSID_KEY).add(cosId);
            base = cosBaseService.read(inputParams);
            assertNotNull(base);
            int intMaxAllowedDomain = Integer.parseInt(maxAllowedDomain);
            assertTrue(base.getMaxNumAllowedDomains().equals(
                    intMaxAllowedDomain));
        } catch (MxOSException e) {
            e.printStackTrace();
            assertNotNull("MxOSError is not null", e);
            assertTrue("Error is Not MBX_INVALID_cosId", e.getCode()
                    .equalsIgnoreCase(CosError.COS_INVALID_COSID.name()));
        }
    }

    @Test
    public void testMaxNumAllowedDomainRead() {
        System.out.println("testMaxNumAllowedDomainRead starting...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        Base base = null;
        try {
            base = cosBaseService.read(inputParams);
            assertNotNull(base);
            int intMaxAllowedDomain = Integer.parseInt(maxAllowedDomain);
            assertTrue(base.getMaxNumAllowedDomains().equals(
                    intMaxAllowedDomain));
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testMaxNumAllowedDomainUpdateNeg() {
        System.out.println("testMaxNumAllowedDomainUpdateNeg starting...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add(null);
        try {
            cosBaseService.update(inputParams);
            fail();
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
        }
    }

    @Test
    public void testMaxNumAllowedDomainUpdateNeg2() {
        System.out.println("testMaxNumAllowedDomainUpdateNeg2 starting...");
        inputParams.clear();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosId);
        inputParams.put(MailboxProperty.maxNumAllowedDomains.name(),
                new ArrayList<String>());
        inputParams.get(MailboxProperty.maxNumAllowedDomains.name()).add(
                "Illegal");
        try {
            cosBaseService.update(inputParams);
            fail();
        } catch (Exception e) {
            assertTrue("Exception Happened", true);
            System.out.println("Expected exception occured: " + e);
        }
    }
}
