package com.opwvmsg.mxos.test.unit.cos.mailstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosExternalStoreService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosExternalStorePOST {

    private static final String TEST_NAME = "CosExternalStorePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String cosId = "default_mxos.2.0";
    private static IMxOSContext context;
    private static ICosExternalStoreService cosExternalStoreService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static ExternalStore getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static ExternalStore getParams(Map<String, List<String>> params,
            MailboxError expectedError) {
        ExternalStore externalStore = null;
        try {
            externalStore = cosExternalStoreService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SmsServices object is null.", externalStore);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return externalStore;
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("CosTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        cosExternalStoreService = (ICosExternalStoreService) context
                .getService(ServiceEnum.CosExternalStoreService.name());
        assertNotNull("CosBaseService object is null.", cosExternalStoreService);
        CosHelper.createCos(cosId);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("CosTest.tearDownAfterClass...");
        params = null;
        cosExternalStoreService = null;
        CosHelper.deleteCos(cosId);
        
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            CosError expectedError) {
        try {
            cosExternalStoreService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("CosTest.setUp...");
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("CosTest.tearDown...");
        if (params.get(COSID_KEY) != null)
            params.get(COSID_KEY).clear();
    }

    @Test
    public void testExternalStoreAccessAllowedEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "");
        updateParams(updateParams,
                CosError.COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    @Test
    public void testExternalStoreAccessAllowedInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "sadfsadf");
        updateParams(updateParams,
                CosError.COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    // externalStoreAccessAllowed
    @Test
    public void testExternalStoreAccessAllowedNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifyNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), null);
        updateParams(updateParams,
                CosError.COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    @Test
    public void testExternalStoreAccessAllowedSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testQuotaBounceNotifySplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(), "!@#$%^&");
        updateParams(updateParams,
                CosError.COS_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED);
    }

    @Test
    public void testExternalStoreAccessAllowedSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(),
                MxosEnums.BooleanType.YES.name());
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        ExternalStore es = getParams(updateParams);
        if (es != null) {
            String externalStoreAccessAllowed = es
                    .getExternalStoreAccessAllowed();
            assertNotNull("Is null.", externalStoreAccessAllowed);
            assertTrue("Has a wrong value.",
                    externalStoreAccessAllowed
                            .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
        }
    }

    @Test
    public void testExternalStoreAccessAllowedSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testExternalStoreAccessAllowedSuccess1");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams,
                MailboxProperty.externalStoreAccessAllowed.name(),
                MxosEnums.BooleanType.NO.name());
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        ExternalStore es = getParams(updateParams);
        if (es != null) {
            String externalStoreAccessAllowed = es
                    .getExternalStoreAccessAllowed();
            assertNotNull("Is null.", externalStoreAccessAllowed);
            assertTrue("Has a wrong value.",
                    externalStoreAccessAllowed
                            .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
        }
    }

    @Test
    public void testMaxExternalStoreSizeMBEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBEmptyParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "");
        updateParams(updateParams,
                CosError.COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    @Test
    public void testMaxExternalStoreSizeMBInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBInvalidParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "sadfsadf");
        updateParams(updateParams,
                CosError.COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    // maxExternalStoreSizeMB
    @Test
    public void testMaxExternalStoreSizeMBNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBNullParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                null);
        updateParams(updateParams,
                CosError.COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    @Test
    public void testMaxExternalStoreSizeMBSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBSplCharsInParam");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);
        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "!@#$%^&");
        updateParams(updateParams,
                CosError.COS_INVALID_MAX_EXTERNAL_STORE_SIZEMB);
    }

    @Test
    public void testMaxExternalStoreSizeMBSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxExternalStoreSizeMBSuccess");

        Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
        updateParams.clear();
        addParams(updateParams, COSID_KEY, cosId);

        addParams(updateParams, MailboxProperty.maxExternalStoreSizeMB.name(),
                "1");
        updateParams(updateParams);
        addParams(updateParams, COSID_KEY, cosId);
        ExternalStore es = getParams(updateParams);
        if (es != null) {
            Integer maxExternalStoreSizeMB = es.getMaxExternalStoreSizeMB();
            assertNotNull("Is null.", maxExternalStoreSizeMB);
            assertTrue("Has a wrong value.", maxExternalStoreSizeMB == 1);
        }
    }
}
