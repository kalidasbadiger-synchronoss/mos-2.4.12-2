/**
 * 
 */
package com.opwvmsg.mxos.test.unit.tasks;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.ContextProperty;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.task.pojos.Task;

/**
 * @author bgh21604
 * 
 */
public class TaskCreate {
    private static IMxOSContext context;
    private static ExternalSession oxSession;
    private static final String baseURL = "http://localhost:8080/mxos";
    private static final String userName = "vinayaka@openwave.com";
    private static final String password = "test";
    private String taskName = "Test10000";
    private String notes = "Notes1000";

    private static void createContext() throws MxOSException {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE,
                ContextEnum.REST.name());
        p.setProperty(ContextProperty.LOAD_SERVICES, "Mailbox,Tasks");
        p.setProperty(ContextProperty.MXOS_BASE_URL, baseURL);
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "5");
        p.setProperty(ContextProperty.MXOS_CONNECTION_TIMEOUT, "120000");
        p.setProperty(ContextProperty.MXOS_READ_TIMEOUT, "60000");
        context = MxOSContextFactory.getInstance().getContext("MXOS-REST-DEMO",
                p);
    }

    private static void login() throws MxOSException {
        System.out.println("Login() called");
        assertNotNull("context is null", context);
        IExternalLoginService service = (IExternalLoginService) context
                .getService(ServiceEnum.ExternalLoginService.name());
        // then call the service methods as below
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(ExternalProperty.entity.name(),
                Arrays.asList(Entity.TASKS.toString()));
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.password.name(), Arrays.asList(password));
        oxSession = service.login(requestMap);
        assertTrue("Login suceeded.", oxSession.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() {
        System.out.println("This is setUpBeforeClass");
        try {
            createContext();
            login();
        } catch (MxOSException e) {
            assertTrue("Login Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("This is tearDownAfterClass");
    }

    @Test
    public void testTaskCreate() {
        System.out.println("testTaskCreate() started");
        ITasksService service = null;

        try {
            service = (ITasksService) context
                    .getService(ServiceEnum.TasksService.name());
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertNotNull("oxSession is null", oxSession);

        System.out.println("userName:" + userName + ", taskName:" + taskName
                + ", notes:" + notes + ", sessionId:"
                + oxSession.getSessionId() + ", cookies:"
                + oxSession.getCookieString());
        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.name.name(), Arrays.asList(taskName));
        requestMap.put(TasksProperty.notes.name(), Arrays.asList(notes));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        long taskId = 0;
        try {
            taskId = service.create(requestMap);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        assertTrue(taskId != 0);

        System.out.println("Task Created :" + taskId);
    }

    @Test
    public void testListTask() {
        System.out.println("testListTask() started...");
        ITasksService service = null;
        try {
            service = (ITasksService) context
                    .getService(ServiceEnum.TasksService.name());
        } catch (MxOSException e) {
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        final Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(TasksProperty.userId.name(), Arrays.asList(userName));
        requestMap.put(TasksProperty.sessionId.name(),
                Arrays.asList(oxSession.getSessionId()));
        requestMap.put(TasksProperty.cookieString.name(),
                Arrays.asList(oxSession.getCookieString()));

        List<Task> tasks = null;
        try {
            tasks = service.list(requestMap);
        } catch (MxOSException e) {
            assertTrue("Task Creation Failure. " + e.getMessage(),
                    e.getMessage() != null);
        }

        System.out.println("Task List size:" + tasks.size());

        for (Task task : tasks) {
            System.out.println("Task Base :" + task.getTaskBase());
        }
    }
}