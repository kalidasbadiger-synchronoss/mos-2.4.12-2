package com.opwvmsg.mxos.test.unit.cos.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosInternalInfoService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosInternalInfoTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosInternalInfoService info;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateCosInternalInfoTest.setUpBeforeClass...");
        info = (ICosInternalInfoService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosInternalInfoService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("UpdateCosInternalInfoTest.setUp...");
        assertNotNull("InternalInfoService object is null.", info);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateCosInternalInfoTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateCosInternalInfoTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        info = null;
    }

    private static InternalInfo getCosInternalInfoParams() {
        InternalInfo mao = null;
        try {
            mao = info.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("InternalInfo object is null.", mao);
        return mao;
    }

    private static void updateCosInternalInfoParams(
            Map<String, List<String>> updateParams) {
        updateCosInternalInfoParams(updateParams, null);
    }

    private static void updateCosInternalInfoParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            info.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    @Test
    public void testUpdateInternalInfoWithoutCosId() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithoutCosId...");
        updateCosInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateInternalInfoWithEmptyCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add("");
        System.out
                .println("UpdateCosInternalInfoTest.testUpdateInternalInfoWithEmptyCosId...");
        updateCosInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateInternalInfoWithNullCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(null);
        System.out
                .println("UpdateInternalInfoTest.testUpdateInternalInfoWithNullCosId...");
        updateCosInternalInfoParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateInternalInfoWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testUpdateInternalInfoWithoutAnyParam...");
        updateParams.get(COSID_KEY).clear();
        updateCosInternalInfoParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testWebmailVersionNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testWebmailVersionNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_VERSION.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testWebmailVersionSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_VERSION.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testWebmailVersionEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_WEBMAIL_VERSION.name());
        updateParams.remove(key);
    }

    @Test
    public void testWebmailVersionSuccess() throws Exception {
        System.out.println("UpdateCosInternalInfoTest.testWebmailVersionSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.webmailVersion.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("1.5");
        updateCosInternalInfoParams(updateParams);
        String version = getCosInternalInfoParams().getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
        assertTrue("WebmailVersion has a wrong value.",
                version.equals("1.5"));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertTrue("SelfCareAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareSSLAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareSSLAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareSSLAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareSSLAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_SELFCARE_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testSelfCareSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testSelfCareSSLAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.selfCareSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertTrue("SelfCareSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testImapProxyAuthEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testImapProxyAuthEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testImapProxyAuthEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testImapProxyAuthEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_IMAP_PROXY_AUTH.name());
        updateParams.remove(key);
    }

    @Test
    public void testImapProxyAuthEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testImapProxyAuthEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.imapProxyAuthenticationEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthEnabled is null.", pa);
        assertTrue("ImapProxyAuthEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testRemoteCallTracingEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testRemoteCallTracingEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testRemoteCallTracingEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testRemoteCallTracingEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_REMOTE_CALL_TRACING.name());
        updateParams.remove(key);
    }

    @Test
    public void testRemoteCallTracingEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testRemoteCallTracingEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.remoteCallTracingEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertTrue("RemoteCallTracingEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertTrue("InterManagerAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerSSLAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerSSLAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerSSLAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerSSLAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_INTER_MANAGER_SSL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testInterManagerSSLAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testInterManagerSSLAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.interManagerSSLAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.NO.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertTrue("InterManagerSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testVoiceMailAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testVoiceMailAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testVoiceMailAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testVoiceMailAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_VOICE_MAIL_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testVoiceMailAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testVoiceMailAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.voiceMailAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertTrue("VoiceMailAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testFaxAccessEnabledNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testFaxAccessEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("6");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testFaxAccessEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testFaxAccessEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_FAX_ACCESS.name());
        updateParams.remove(key);
    }

    @Test
    public void testFaxAccessEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testFaxAccessEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.faxAccessEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosInternalInfoParams(updateParams);
        BooleanType pa = getCosInternalInfoParams().getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertTrue("FaxAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testLdapUtilitiesAccessTypeNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testLdapUtilitiesAccessTypeInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testLdapUtilitiesAccessTypeSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testLdapUtilitiesAccessTypeEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE.name());
        updateParams.remove(key);
    }

    @Test
    public void testLdapUtilitiesAccessTypeSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testLdapUtilitiesAccessTypeSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.ldapUtilitiesAccessType.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AccessType.TRUSTED.toString());
        updateCosInternalInfoParams(updateParams);
        AccessType pa = getCosInternalInfoParams().getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertTrue("LdapUtilitiesAccessType has a wrong value.",
                pa.equals(AccessType.TRUSTED));
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderNullParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testAddressBookProviderNullParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(null);
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderInvalidParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testAddressBookProviderInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testAddressBookProviderSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderEmptyParam() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testAddressBookProviderEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,
                MailboxError.MBX_INVALID_ADDRESS_BOOK_PROVIDER.name());
        updateParams.remove(key);
    }

    @Test
    public void testAddressBookProviderSuccess() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testAddressBookProviderSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.addressBookProvider.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(AddressBookProviderType.PLAXO.toString());
        updateCosInternalInfoParams(updateParams);
        AddressBookProviderType pa = getCosInternalInfoParams().getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertTrue("AddressBookProvider has a wrong value.",
                pa.equals(AddressBookProviderType.PLAXO));
        updateParams.remove(key);
    }
    @Test
    public void testCosRealm() throws Exception {
        System.out
                .println("UpdateCosInternalInfoTest.testCosRealm...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.realm.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosInternalInfoParams(updateParams,MailboxError.MBX_INVALID_MAIL_REALM.name());
        updateParams.remove(key);
    }
}
