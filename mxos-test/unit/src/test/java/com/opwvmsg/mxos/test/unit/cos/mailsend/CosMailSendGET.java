package com.opwvmsg.mxos.test.unit.cos.mailsend;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.ServiceLoader;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailSendService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailSendGET {

    private static final String TEST_NAME = "CosMailSendRestGET";
    private static final String ARROW_SEP = " --> ";
    private static final String COS_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "mail_send_cos";

    private static IMxOSContext context;
    private static ICosMailSendService cosMailSendService;
    private static ICosService cosService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        context = ContextUtils.loadContext();
        cosMailSendService = (ICosMailSendService)context
                .getService(ServiceEnum.CosMailSendService.name());
        cosService = (ICosService) context
        .getService(ServiceEnum.CosService.name());
        createCos(COS_ID);
        params.put(COS_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("cosMailSendService object is null.", cosMailSendService);
        assertNotNull("cosService object is null.", cosService);
        assertNotNull("Input Param:cosId is null.", params.get(COS_KEY));
        params.get(COS_KEY).add(COS_ID);
        assertTrue("Input Param:cosId is empty.", !params.get(COS_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
        //params.get(COS_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteCos(COS_ID);
        params.clear();
        params = null;
        cosMailSendService = null;
        cosService = null;
    }

    private static MailSend getParams(Map<String, List<String>> params) {
        params.put(COS_KEY, new ArrayList<String>());
        params.get(COS_KEY).add(COS_ID);
        MailSend mailSend = getParams(params, null);;
        params.put(COS_KEY, new ArrayList<String>());
        params.get(COS_KEY).add(COS_ID);
        return mailSend;
    }

    private static MailSend getParams(Map<String, List<String>> params,
            MailboxError expectedError) {
        MailSend mailSend = null;
        try {
            mailSend = cosMailSendService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("SmsServices object is null.", mailSend);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return mailSend;
    }

    private static void createCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COS_KEY, new ArrayList<String>());
        inputParams.get(COS_KEY).add(cosId);
        try {
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosId) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COS_KEY, new ArrayList<String>());
        inputParams.get(COS_KEY).add(cosId);
        try {
            cosService.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    // futureDeliveryEnabled
    @Test
    public void testFutureDeliveryEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testFutureDeliveryEnabled");
        String futureDeliveryEnabled = getParams(params)
                .getFutureDeliveryEnabled().name();
        assertNotNull("Is null.", futureDeliveryEnabled);
    }

    // maxFutureDeliveryDaysAllowed
    @Test
    public void testMaxFutureDeliveryDaysAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxFutureDeliveryDaysAllowed");
        Integer maxFutureDeliveryDaysAllowed = getParams(params)
                .getMaxFutureDeliveryDaysAllowed();
        assertNotNull("Is null.", maxFutureDeliveryDaysAllowed);
    }

    // maxFutureDeliveryMessagesAllowed
    @Test
    public void testMaxFutureDeliveryMessagesAllowed() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxFutureDeliveryMessagesAllowed");
        int maxFutureDeliveryMessagesAllowed = getParams(params)
                .getMaxFutureDeliveryMessagesAllowed();
        assertNotNull("Is null.", maxFutureDeliveryMessagesAllowed);
    }

    // useRichTextEditor
    @Test
    public void testUseRichTextEditor() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testUseRichTextEditor");
        String useRichTextEditor = getParams(params).getUseRichTextEditor()
                .name();
        assertNotNull("Is null.", useRichTextEditor);
    }

    // includeOriginalMailInReply
    @Test
    public void testIncludeOriginalMailInReply() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeOriginalMailInReply");
        String includeOriginalMailInReply = getParams(params)
                .getIncludeOriginalMailInReply().name();
        assertNotNull("Is null.", includeOriginalMailInReply);
    }

    // originalMailSeperatorCharacter
    @Test
    public void testOriginalMailSeperatorCharacter() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testOriginalMailSeperatorCharacter");
        String originalMailSeperatorCharacter = getParams(params)
                .getOriginalMailSeperatorCharacter();
        assertNotNull("Is null.", originalMailSeperatorCharacter);
    }

    // autoSaveSentMessages
    @Test
    public void testAutoSaveSentMessages() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoSaveSentMessages");
        String autoSaveSentMessages = getParams(params)
                .getAutoSaveSentMessages().name();
        assertNotNull("Is null.", autoSaveSentMessages);
    }

    // addSignatureForNewMails
    @Test
    public void testAddSignatureForNewMails() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureForNewMails");
        String addSignatureForNewMails = getParams(params)
                .getAddSignatureForNewMails().name();
        assertNotNull("Is null.", addSignatureForNewMails);
    }

    // addSignatureInReplyType
    @Test
    public void testAddSignatureInReplyType() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAddSignatureInReplyType");
        String addSignatureInReplyType;
            addSignatureInReplyType = getParams(params)
                    .getAddSignatureInReplyType().name();
            System.out.println("addSignatureInReplyType : "
                    + addSignatureInReplyType);
        assertNotNull("Is null.", addSignatureInReplyType);
    }

    // autoSpellCheckEnabled
    @Test
    public void testAutoSpellCheckEnabled() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testAutoSpellCheckEnabled");
        String autoSpellCheckEnabled = getParams(params)
                .getAutoSpellCheckEnabled().name();
        assertNotNull("Is null.", autoSpellCheckEnabled);
    }

    // confirmPromptOnDelete
    @Test
    public void testConfirmPromptOnDelete() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testConfirmPromptOnDelete");
        String confirmPromptOnDelete = getParams(params)
                .getConfirmPromptOnDelete().name();
        assertNotNull("Is null.", confirmPromptOnDelete);
    }

    // includeReturnReceiptReq
    @Test
    public void testIncludeReturnReceiptReq() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testIncludeReturnReceiptReq");
        String includeReturnReceiptReq = getParams(params)
                .getIncludeReturnReceiptReq().name();
        assertNotNull("Is null.", includeReturnReceiptReq);
    }

    // maxSendMessageSizeKB
    @Test
    public void testMaxSendMessageSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxSendMessageSizeKB");
        int maxSendMessageSizeKB = getParams(params).getMaxSendMessageSizeKB();
        assertNotNull("Is null.", maxSendMessageSizeKB);
    }

    // maxAttachmentSizeKB
    @Test
    public void testMaxAttachmentSizeKB() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxAttachmentSizeKB");
        int maxAttachmentSizeKB = getParams(params).getMaxAttachmentSizeKB();
        assertNotNull("Is null.", maxAttachmentSizeKB);
    }

    // maxAttachments
    @Test
    public void testMaxAttachments() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxAttachments");
        int maxAttachments = getParams(params).getMaxAttachmentsInSession();
        assertNotNull("Is null.", maxAttachments);
    }
    
    // maxCharactersPerPage
    @Test
    public void testMaxCharactersPerPage() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxCharactersPerPage");
        int maxCharactersPerPage = getParams(params).getMaxCharactersPerPage();
        assertNotNull("Is null.", maxCharactersPerPage);
    }
}
