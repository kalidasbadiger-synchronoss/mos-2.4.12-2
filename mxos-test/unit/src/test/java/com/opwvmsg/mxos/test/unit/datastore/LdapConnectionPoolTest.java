package com.opwvmsg.mxos.test.unit.datastore;


import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.test.unit.ContextUtils;

public class LdapConnectionPoolTest {

    private static IMxOSContext context;
    private static ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
    private static IMailboxCRUD mailboxCRUD = null;
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("LdapConnectionPoolTest.setUpBeforeClass...");
        context = ContextUtils.loadContext();
        mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        mailboxCRUDPool = null ;
    }

    @Before
    public void setUp() throws Exception {
        
    }

    @After
    public void tearDown() throws Exception {
        if(mailboxCRUD != null && mailboxCRUDPool != null)
            mailboxCRUDPool.returnObject(mailboxCRUD);
    }
    
    @Test
    public void testLdapConnection() {
        System.out.println("testLdapConnection");
        try {
            mailboxCRUD = mailboxCRUDPool.borrowObject() ;
            assertNotNull(mailboxCRUD) ;
            System.out.println("Valiid ldap connection object found" + mailboxCRUD.toString());
        } catch (MxOSException e) {
           e.printStackTrace();
           fail() ;
        } 
    }

}