package com.opwvmsg.mxos.test.unit.internalinfo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AccessType;
import com.opwvmsg.mxos.data.enums.MxosEnums.AddressBookProviderType;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IInternalInfoService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GetInternalInfoTest {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "go123321@openwave.com";
    private static final String PASSWORD = "pAsSw0rD";
    private static IInternalInfoService info;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("GetInternalInfoTest.setUpBeforeClass...");
        info = (IInternalInfoService) ContextUtils.loadContext()
                .getService(ServiceEnum.InternalInfoService.name());
        String id = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("Mailbox was not created.", id != null);
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetInternalInfoTest.setUp...");
        assertNotNull("InternalInfoService object is null.", info);
        assertNotNull("Input Param:email is null.", params.get(EMAIL_KEY));
        params.get(EMAIL_KEY).add(EMAIL);
        assertTrue("Input Param:email is empty.", !params.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("GetInternalInfoTest.tearDown...");
        params.get(EMAIL_KEY).clear();
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("GetInternalInfoTest.tearDownAfterClass...");
        MailboxHelper.deleteMailbox(EMAIL, true);
        params.clear();
        params = null;
        info = null;
    }

    private static InternalInfo getInternalInfoParams(
            Map<String, List<String>> params) {
        return getInternalInfoParams(params, null);
    }

    private static InternalInfo getInternalInfoParams(
            Map<String, List<String>> params, String expectedError) {
        InternalInfo mao = null;
        try {
            mao = info.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("InternalInfo object is null.", mao);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (params.isEmpty()) {
                params.put(EMAIL_KEY, new ArrayList<String>());
            }
        }
        return mao;
    }

    @Test
    public void testGetInternalInfoWithoutEmail() {
        System.out
                .println("GetInternalInfoTest.testGetInternalInfoWithoutEmail...");
        // Clear the params.
        params.clear();
        getInternalInfoParams(params, ErrorCode.GEN_BAD_REQUEST.name());
        params.put(EMAIL_KEY, new ArrayList<String>());
    }

    @Test
    public void testGetInternalInfoWithNullEmail() {
        System.out
                .println("GetInternalInfoTest.testGetInternalInfoWithNullEmail...");
        // Remove the email from the map to have a null email.
        params.get(EMAIL_KEY).clear();
        getInternalInfoParams(params, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testGetInternalInfoWithNonExistingEmail() {
        System.out
                .println("GetInternalInfoTest.testGetInternalInfoWithNonExistingEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("something.junk@foobar.com");
        getInternalInfoParams(params, MailboxError.MBX_NOT_FOUND.name());
    }

    @Test
    public void testGetInternalInfoWithSplCharsInEmail() {
        System.out
                .println("GetInternalInfoTest.testGetInternalInfoWithSplCharsInEmail...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("!#$%^&*()+-=.junk@foobar.com");
        getInternalInfoParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testGetInternalInfoWithSplCharsInDomain() {
        System.out
                .println("GetInternalInfoTest.testGetInternalInfoWithSplCharsInDomain...");
        // Replace openwave with a junk value to set an invalid email.
        params.get(EMAIL_KEY).clear();
        params.get(EMAIL_KEY).add("junk@foo!#+()$&*^-bar.com");
        getInternalInfoParams(params, MailboxError.MBX_INVALID_EMAIL.name());
    }

    @Test
    public void testWebmailVersionSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testWebmailVersionSuccess...");
        String version = getInternalInfoParams(params).getWebmailVersion();
        assertNotNull("WebmailVersion is null.", version);
    }

    @Test
    public void testSelfCareAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testSelfCareAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getSelfCareAccessEnabled();
        assertNotNull("SelfCareAccessEnabled is null.", pa);
        assertTrue("SelfCareAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testSelfCareSSLAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testSelfCareSSLAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getSelfCareSSLAccessEnabled();
        assertNotNull("SelfCareSSLAccessEnabled is null.", pa);
        assertTrue("SelfCareSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testMessageStoreHostSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testMessageStoreHostSuccess...");
        List<String> msgStoreHost = getInternalInfoParams(params).getMessageStoreHosts();
        assertNotNull("MessageStoreHost is null.", msgStoreHost);
        assertFalse("MessageStoreHost is empty.", msgStoreHost.isEmpty());
    }

    @Test
    public void testAutoReplyHostSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testAutoReplyHostSuccess...");
        String autoReplyHost = getInternalInfoParams(params).getAutoReplyHost();
        assertNotNull("AutoReplyHost is not null.", autoReplyHost);
    }

    @Test
    public void testImapProxyAuthenticationEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testImapProxyAuthenticationEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getImapProxyAuthenticationEnabled();
        assertNotNull("ImapProxyAuthenticationEnabled is null.", pa);
        assertTrue("ImapProxyAuthenticationEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testRemoteCallTracingEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testRemoteCallTracingEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getRemoteCallTracingEnabled();
        assertNotNull("RemoteCallTracingEnabled is null.", pa);
        assertTrue("RemoteCallTracingEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testInterManagerAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testInterManagerAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getInterManagerAccessEnabled();
        assertNotNull("InterManagerAccessEnabled is null.", pa);
        assertTrue("InterManagerAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testInterManagerSSLAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testInterManagerSSLAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getInterManagerSSLAccessEnabled();
        assertNotNull("InterManagerSSLAccessEnabled is null.", pa);
        assertTrue("InterManagerSSLAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testVoiceMailAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testVoiceMailAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getVoiceMailAccessEnabled();
        assertNotNull("VoiceMailAccessEnabled is null.", pa);
        assertTrue("VoiceMailAccessEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
    }

    @Test
    public void testFaxAccessEnabledSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testFaxAccessEnabledSuccess...");
        BooleanType pa = getInternalInfoParams(params).getFaxAccessEnabled();
        assertNotNull("FaxAccessEnabled is null.", pa);
        assertTrue("FaxAccessEnabled has a wrong value.",
                pa.equals(BooleanType.NO));
    }

    @Test
    public void testLdapUtilitiesAccessTypeSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testLdapUtilitiesAccessTypeSuccess...");
        AccessType pa = getInternalInfoParams(params).getLdapUtilitiesAccessType();
        assertNotNull("LdapUtilitiesAccessType is null.", pa);
        assertTrue("LdapUtilitiesAccessType has a wrong value.",
                pa.equals(AccessType.ALL));
    }

    @Test
    public void testAddressBookProviderSuccess() throws Exception {
        System.out.println("GetInternalInfoTest.testAddressBookProviderSuccess...");
        AddressBookProviderType pa = getInternalInfoParams(params).getAddressBookProvider();
        assertNotNull("AddressBookProvider is null.", pa);
        assertTrue("AddressBookProvider has a wrong value.",
                pa.equals(AddressBookProviderType.MSS));
    }

}
