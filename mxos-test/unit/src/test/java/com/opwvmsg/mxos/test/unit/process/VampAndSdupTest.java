package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class VampAndSdupTest {

    private static final String SMSTYPE_SMSONLINE = "smsonline";
    private static final String AUTHORIZE_MSISDN = "+19886576790";

    private static final String TO_ADDRESS_VALUE_SUCCESS = "+19886576790";
    private static final String TO_ADDRESS_VALUE_TEMPERROR = "+19886180101";
    private static final String TO_ADDRESS_VALUE_PERMERROR = "+18008805766";

    private static final String FROM_ADDRESS_VALUE = "+19886576790";

    private static final String MESSAGE_VALUE = "Hi Message";

    private static ISendSMSService sendSmsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("VampAndSdupTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null ||
                System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        sendSmsService = (ISendSMSService) ContextUtils.loadContext()
                .getService(ServiceEnum.SendSmsService.name());
        assertNotNull("MailboxService object is null.", sendSmsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("VampAndSdupTest.tearDownAfterClass...");
        params.clear();
        params = null;
        sendSmsService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testSendSmsWithAuthorize_Success() {
        System.out.println("VampAndSdupTest.testSuccess_SendSmsWithAuthorize...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertFalse("MxOSException Happened", true);
        }
    }

    @Test
    public void testSendSmsWithAuthorize_SmsTempError() {
        System.out
                .println("VampAndSdupTest.testSendSmsWithAuthorize_SmsTempError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_TEMPERROR);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_TEMPORARY_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSmsWithAuthorize_SmsPermError() {
        System.out
                .println("VampAndSdupTest.testSendSmsWithAuthorize_SmsPermError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_PERMERROR);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, SmsProperty.authorizeMsisdn.name(),
                    AUTHORIZE_MSISDN);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

}
