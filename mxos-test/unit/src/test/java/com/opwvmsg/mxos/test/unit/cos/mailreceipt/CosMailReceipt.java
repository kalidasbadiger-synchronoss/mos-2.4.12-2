package com.opwvmsg.mxos.test.unit.cos.mailreceipt;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosMailReceipt {

    private static ICosMailReceiptService cosmr = null;
    private static ICosService cosService = null;
    private static Map <String, List<String> >inputParams =
        new HashMap<String, List<String>>();
    private static String test = "test";
    private static String cosIdKey = "cosId";
    
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.put("loadDefaultCos", new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.get("loadDefaultCos").add("yes");
        try {
            cosmr = (ICosMailReceiptService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosMailReceiptService.name());
            cosService = (ICosService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosService.name());
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNotNull(cosmr);
    }
    
    
    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:cos starting......");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosService.delete(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptGet() {
        System.out.println("testMailReceiptGet:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            MailReceipt mr = cosmr.read(inputParams);
            assertNotNull(mr);
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptPost() {
        System.out.println("testMailReceiptPost:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("copyOnForward", new ArrayList<String>());
        inputParams.get("copyOnForward").add("yes");
        try {
            cosmr.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            MailReceipt mr  = cosmr.read(inputParams);
            assertEquals("Passed: ", MxosEnums.BooleanType.YES, mr.getCopyOnForward());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptPost2() {
        System.out.println("testMailReceiptPost2:cos ....");
        inputParams.clear();
        String count = "30";
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("maxMailsPerPage", new ArrayList<String>());
        inputParams.get("maxMailsPerPage").add(count);
        try {
            cosmr.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            MailReceipt mr  = cosmr.read(inputParams);
            assertEquals("Passed: ", count, mr.getMaxMailsPerPage().toString());
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptPost3() {
        System.out.println("testMailReceiptPost3:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("ackReturnReceiptReq", new ArrayList<String>());
        inputParams.get("ackReturnReceiptReq").add(MailReceipt.AckReturnReceiptReq.ALWAYS.name());
        try {
            cosmr.update(inputParams);
            inputParams.clear();
            inputParams.put("cosId", new ArrayList<String>());
            inputParams.get("cosId").add(test);
            MailReceipt mr  = cosmr.read(inputParams);
            assertEquals("Passed: ", MailReceipt.AckReturnReceiptReq.ALWAYS, mr.getAckReturnReceiptReq() );
        } catch(MxOSException me) {
            me.printStackTrace();
            fail();
        } catch(Exception e) {
            e.printStackTrace();
            fail();
        }
    }
    
    @Test
    public void testMailReceiptPostNeg1() {
        System.out.println("testMailReceiptPostNeg1:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosmr.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", ErrorCode.GEN_BAD_REQUEST.name(), me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptPostNeg2() {
        System.out.println("testMailReceiptPostNeg2:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("forwardingEnabled", new ArrayList<String>());
        inputParams.get("forwardingEnabled").add("illegal");
        try {
            cosmr.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ",
                    MailboxError.MBX_INVALID_FORWARDING_ENABLED.name(),
                    me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testMailReceiptPostNeg3() {
        System.out.println("testMailReceiptPostNeg3:cos ....");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        inputParams.put("deliveryScope", new ArrayList<String>());
        inputParams.get("deliveryScope").add("illegal");
        try {
            cosmr.update(inputParams);
            fail();
        } catch(MxOSException me) {
            me.printStackTrace();
            assertEquals("Expected Exception: ", MailboxError.MBX_INVALID_DELIVERY_SCOPE.name(), me.getCode());
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}