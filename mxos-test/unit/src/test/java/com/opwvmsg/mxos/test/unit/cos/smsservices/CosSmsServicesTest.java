package com.opwvmsg.mxos.test.unit.cos.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsServicesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosSmsServicesTest {

    private static final String TEST_NAME = "CosSmsServicesTest";
    private static final String ARROW_SEP = " --> ";

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID_VALUE = "CosSmsServicesCos";

    private static final String SMSSERVICESALLOWED_KEY = MailboxProperty.smsServicesAllowed
            .name();
    private static final String SMSSERVICESALLOWED_VALUE_YES = MxosEnums.BooleanType.YES
            .name();
    private static final String SMSSERVICESALLOWED_VALUE_NO = MxosEnums.BooleanType.NO
            .name();

    private static ICosSmsServicesService cosSmsServicesService;
    private static ICosService cs;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        cs = (ICosService) ContextUtils.loadContext().getService(
                ServiceEnum.CosService.name());
        cosSmsServicesService = (ICosSmsServicesService) ContextUtils
                .loadContext().getService(
                        ServiceEnum.CosSmsServicesService.name());
        createCos(COSID_VALUE);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("cosSmsServicesService object is null.",
                cosSmsServicesService);
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        assertNotNull("Input Param:email is null.", inputParams.get(COSID_KEY));
        inputParams.get(COSID_KEY).add(COSID_VALUE);
        assertTrue("Input Param:email is empty.", !inputParams.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteCos(COSID_VALUE);
        cosSmsServicesService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static SmsServices getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static SmsServices getParams(Map<String, List<String>> params,
            MailboxError expectedError) {
        SmsServices smsServices = null;
        try {
            smsServices = cosSmsServicesService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("SmsServices object is null.", smsServices);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return smsServices;
    }

    private static SmsServices getWithoutParams(
            Map<String, List<String>> params, ErrorCode expectedError) {
        SmsServices smsServices = null;
        try {
            smsServices = cosSmsServicesService.read(params);
            if (null != expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("SmsServices object is null.", smsServices);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
        return smsServices;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            MailboxError expectedError) {
        try {
            cosSmsServicesService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private static void updateWithoutParams(
            Map<String, List<String>> updateParams, ErrorCode expectedError) {
        try {
            cosSmsServicesService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private static void createCos(String CosValue) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(CosValue);
        try {
            cs.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String CosValue) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(CosValue);
        try {
            cs.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testSmsServicesAllowedGet() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testSmsServicesAllowedGet");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        String smsServicesAllowed = getParams(inputParams)
                .getSmsServicesAllowed().name();
        assertNotNull("smsServicesAllowed is null.", smsServicesAllowed);
        System.out.println("smsServicesAllowed : " + smsServicesAllowed);
    }

    @Test
    public void testSmsServicesAllowedUpdate_NullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdate_NullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY, null);
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED);
    }

    @Test
    public void testSmsServicesAllowedUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY, "");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED);
    }

    @Test
    public void testSmsServicesAllowedUpdateInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY, "Junk");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED);
    }

    @Test
    public void testSmsServicesAllowedUpdateSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY, "#$%^&*(");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_SERVICES_ALLOWED);
    }

    @Test
    public void testSmsServicesAllowedUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY,
                SMSSERVICESALLOWED_VALUE_YES);
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        String smsServicesAllowed = getParams(inputParams)
                .getSmsServicesAllowed().name();
        assertNotNull("Is null.", smsServicesAllowed);
        assertTrue("Has a wrong value.",
                smsServicesAllowed.equalsIgnoreCase(MxosEnums.BooleanType.YES
                        .name()));

    }

    @Test
    public void testSmsServicesAllowedUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsServicesAllowedUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSSERVICESALLOWED_KEY,
                SMSSERVICESALLOWED_VALUE_NO);
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        String smsServicesAllowed = getParams(inputParams)
                .getSmsServicesAllowed().name();
        assertNotNull("Is null.", smsServicesAllowed);
        assertTrue("Has a wrong value.",
                smsServicesAllowed.equalsIgnoreCase(MxosEnums.BooleanType.NO
                        .name()));
    }

}
