package com.opwvmsg.mxos.test.unit.cos.webmailfeatures;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosWebMailFeaturesService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.CosHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class UpdateCosWebMailFeaturesTest {

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COS_ID = "default_cos_1234";
    private static ICosWebMailFeaturesService wmfs;
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("UpdateCosWebMailFeaturesTest.setUpBeforeClass...");
        wmfs = (ICosWebMailFeaturesService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosWebMailFeaturesService.name());
        boolean flag = CosHelper.createCos(COS_ID);
        assertTrue("Cos was not created.", flag);
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        getParams.put(COSID_KEY, new ArrayList<String>());
        getParams.get(COSID_KEY).add(COS_ID);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println("GetCosWebMailFeaturesTest.setUp...");
        assertNotNull("CosWebMailFeaturesService object is null.", wmfs);
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println("UpdateCosWebMailFeaturesTest.tearDown...");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("UpdateCosWebMailFeaturesTest.tearDownAfterClass...");
        CosHelper.deleteCos(COS_ID);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        wmfs = null;
    }

    private static WebMailFeatures getCosWebMailFeaturesParams() {
        WebMailFeatures wmf = null;
        try {
            wmf = wmfs.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(COSID_KEY)
                    || getParams.get(COSID_KEY).isEmpty()) {
                getParams.put(COSID_KEY, new ArrayList<String>());
                getParams.get(COSID_KEY).add(COS_ID);
            }
        }
        assertNotNull("WebMailFeatures object is null.", wmf);
        return wmf;
    }

    private static void updateCosWebMailFeaturesParams(
            Map<String, List<String>> updateParams) {
        updateCosWebMailFeaturesParams(updateParams, null);
    }

    private static void updateCosWebMailFeaturesParams(
            Map<String, List<String>> updateParams, String expectedError) {
        try {
            wmfs.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail("This should not have come!!!");
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(COSID_KEY)
                    || updateParams.get(COSID_KEY).isEmpty()) {
                updateParams.put(COSID_KEY, new ArrayList<String>());
                updateParams.get(COSID_KEY).add(COS_ID);
            }
        }
    }

    @Test
    public void testUpdateWebMailFeaturesWithoutCosId() throws Exception {
        updateParams.clear();
        System.out
                .println("UpdateCosWebMailFeaturesTest.testUpdateWebMailFeaturesWithoutCosId...");
        updateCosWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithEmptyCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add("");
        System.out
                .println("UpdateCosInternalInfoTest.testUpdateWebMailFeaturesWithEmptyCosId...");
        updateCosWebMailFeaturesParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithNullCosId() throws Exception {
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(null);
        System.out
                .println("UpdateCosWebMailFeaturesTest.testUpdateWebMailFeaturesWithNullCosId...");
        updateCosWebMailFeaturesParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
    }

    @Test
    public void testUpdateWebMailFeaturesWithoutAnyParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testUpdateMailAccessWithoutAnyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        updateCosWebMailFeaturesParams(updateParams,
                ErrorCode.GEN_BAD_REQUEST.name());
    }

    @Test
    public void testBusinessFeaturesEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testBusinessFeaturesEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testBusinessFeaturesEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testBusinessFeaturesEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testBusinessFeaturesEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testBusinessFeaturesEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.businessFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosWebMailFeaturesParams(updateParams);
        BooleanType pa = getCosWebMailFeaturesParams()
                .getBusinessFeaturesEnabled();
        assertNotNull("BusinessFeaturesEnabled is null.", pa);
        assertTrue("BusinessFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledEmptyParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testFullFeaturesEnabledEmptyParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledInvalidParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testFullFeaturesEnabledInvalidParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("SomethingJunk");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledSplCharsInParam() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testFullFeaturesEnabledSplCharsInParam...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("!@#$%^&*()_+-=");
        updateCosWebMailFeaturesParams(updateParams,
                MailboxError.MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED.name());
        updateParams.remove(key);
    }

    @Test
    public void testFullFeaturesEnabledSuccess() throws Exception {
        System.out
                .println("UpdateCosWebMailFeaturesTest.testFullFeaturesEnabledSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.fullFeaturesEnabled.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosWebMailFeaturesParams(updateParams);
        BooleanType pa = getCosWebMailFeaturesParams().getFullFeaturesEnabled();
        assertNotNull("FullFeaturesEnabled is null.", pa);
        assertTrue("FullFeaturesEnabled has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }
    
    @Test
    public void testAllowPasswordChangeSuccess() throws Exception {
        System.out
                .println("testAllowPasswordChangeSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.allowPasswordChange.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add(BooleanType.YES.toString());
        updateCosWebMailFeaturesParams(updateParams);
        BooleanType pa = getCosWebMailFeaturesParams().getAllowPasswordChange();
        assertNotNull("allowPasswordChange is null.", pa);
        assertTrue("allowPasswordChange has a wrong value.",
                pa.equals(BooleanType.YES));
        updateParams.remove(key);
    }
    
    @Test
    public void testAllowPasswordChangeFailure() throws Exception {
        System.out
                .println("testAllowPasswordChangeSuccess...");
        updateParams.clear();
        updateParams.put(COSID_KEY, new ArrayList<String>());
        updateParams.get(COSID_KEY).add(COS_ID);
        String key = MailboxProperty.allowPasswordChange.name();
        updateParams.put(key, new ArrayList<String>());
        updateParams.get(key).add("abcd");
        try{
        	wmfs.update(updateParams);
        	fail();
        }catch (MxOSException nEx){
        	System.out.println("Message : "+nEx.getCode());
        }
        
    }
}
