package com.opwvmsg.mxos.test.unit.cos.smsservices;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSmsOnlineService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class CosSmsOnlineTest {

    private static final String TEST_NAME = "CosSmsOnlineTest";
    private static final String ARROW_SEP = " --> ";

    private static final String COSID_KEY = MailboxProperty.cosId.name();
    private static final String COSID_VALUE = "CosSmsOnlineCos";

    private static ICosService cs;
    private static ICosSmsOnlineService cosSmsOnlineService;

    private static final String SMSONLINEENABLED_KEY = MailboxProperty.smsOnlineEnabled
            .name();
    private static final String INTERNATIONALSMSALLOWED_KEY = MailboxProperty.internationalSMSAllowed
            .name();
    private static final String INTERNATIONALSMSENABLED_KEY = MailboxProperty.internationalSMSEnabled
            .name();
    private static final String CONCATENATEDSMSALLOWED_KEY = MailboxProperty.concatenatedSMSAllowed
            .name();
    private static final String CONCATENATEDSMSENABLED_KEY = MailboxProperty.concatenatedSMSEnabled
            .name();
    private static final String MAX_SMS_PERDAY_KEY = MailboxProperty.maxSMSPerDay
            .name();
    private static final String MAX_CONCATENATED_SMS_SEGMENTS_KEY = MailboxProperty.maxConcatenatedSMSSegments
            .name();
    private static final String MAX_PERCAPTCHA_SMS_KEY = MailboxProperty.maxPerCaptchaSMS
            .name();
    private static final String MAX_PERCAPTCHA_DURATION_MINS = MailboxProperty.maxPerCaptchaDurationMins
            .name();

    private static final String VALUE_YES = MxosEnums.BooleanType.YES.name();
    private static final String VALUE_NO = MxosEnums.BooleanType.NO.name();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        cs = (ICosService) ContextUtils.loadContext().getService(
                ServiceEnum.CosService.name());
        cosSmsOnlineService = (ICosSmsOnlineService) ContextUtils.loadContext()
                .getService(ServiceEnum.CosSmsOnlineService.name());
        createCos(COSID_VALUE);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        assertNotNull("Object is null.", cosSmsOnlineService);
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        assertNotNull("Input Param:email is null.", inputParams.get(COSID_KEY));
        inputParams.get(COSID_KEY).add(COSID_VALUE);
        assertTrue("Input Param:email is empty.", !inputParams.get(COSID_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        deleteCos(COSID_VALUE);
        cosSmsOnlineService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    private static SmsOnline getParams(Map<String, List<String>> params) {
        SmsOnline smsOnline = null;
        try {
            smsOnline = cosSmsOnlineService.read(params);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertNotNull("SmsServices object is null.", smsOnline);
        return smsOnline;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            MailboxError expectedError) {
        try {
            cosSmsOnlineService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private static void updateWithoutParams(
            Map<String, List<String>> updateParams, ErrorCode expectedError) {
        try {
            cosSmsOnlineService.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError.name(), e.getCode());
            }
        }
    }

    private static void createCos(String cosValue) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosValue);
        try {
            cs.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static void deleteCos(String cosValue) {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put(COSID_KEY, new ArrayList<String>());
        inputParams.get(COSID_KEY).add(cosValue);
        try {
            cs.delete(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testSmsOnlineEnabledUpdateNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY, null);
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED);
    }

    @Test
    public void testSmsOnlineEnabledUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY, "");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED);
    }

    @Test
    public void testSmsOnlineEnabledUpdateInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY, "Junk");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED);
    }

    @Test
    public void testSmsOnlineEnabledUpdateSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY, "#$%^&*");
        updateParams(inputParams, MailboxError.MBX_INVALID_SMS_ONLINE_ENABLED);
    }

    @Test
    public void testSmsOnlineEnabledUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY,
                MxosEnums.BooleanType.YES.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String smsServicesEnabled = getParams(inputParams)
                .getSmsOnlineEnabled().name();
        assertNotNull("Is null.", smsServicesEnabled);
        assertTrue("Has a wrong value.",
                smsServicesEnabled.equalsIgnoreCase(MxosEnums.BooleanType.YES
                        .name()));
    }

    @Test
    public void testSmsOnlineEnabledUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testSmsOnlineEnabledUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, SMSONLINEENABLED_KEY,
                MxosEnums.BooleanType.NO.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String smsServicesEnabled = getParams(inputParams)
                .getSmsOnlineEnabled().name();
        assertNotNull("Is null.", smsServicesEnabled);
        assertTrue("Has a wrong value.",
                smsServicesEnabled.equalsIgnoreCase(MxosEnums.BooleanType.NO
                        .name()));
    }

    // Internation SMS Allowed Tests

    @Test
    public void testInternationSMSAllowedUpdateNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY, null);
        updateParams(inputParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED);
    }

    @Test
    public void testInternationSMSAllowedUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY, "");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED);
    }

    @Test
    public void testInternationSMSAllowedUpdateInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY, "Junk");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED);
    }

    @Test
    public void testInternationSMSAllowedUpdateSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY, "#$%^&*");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_INTERNATIONAL_SMS_ALLOWED);
    }

    @Test
    public void testInternationSMSAllowedUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY,
                MxosEnums.BooleanType.YES.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String smsServicesEnabled = getParams(inputParams)
                .getInternationalSMSAllowed().name();
        assertNotNull("Is null.", smsServicesEnabled);
        assertTrue("Has a wrong value.",
                smsServicesEnabled.equalsIgnoreCase(MxosEnums.BooleanType.YES
                        .name()));
    }

    @Test
    public void testInternationSMSAllowedUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSAllowedUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSALLOWED_KEY,
                MxosEnums.BooleanType.NO.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String smsServicesEnabled = getParams(inputParams)
                .getInternationalSMSAllowed().name();
        assertNotNull("Is null.", smsServicesEnabled);
        assertTrue("Has a wrong value.",
                smsServicesEnabled.equalsIgnoreCase(MxosEnums.BooleanType.NO
                        .name()));
    }

    // Internation SMS Enabled Tests
    // commenting this part of code as this is not part of COSSmsOnline POST
    /* @Test
    public void testInternationSMSEnabledUpdateNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY, null);
        updateWithoutParams(inputParams, ErrorCode.GEN_BAD_REQUEST);
        //updateParams(inputParams,
          //      MailboxError.MBX_INVALID_INTERNALTONAL_SMS_ENABLED);
    }

    @Test
    public void testInternationSMSEnabledUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY, "");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testInternationSMSEnabledUpdateInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY, "Junk");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testInternationSMSEnabledUpdateSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY, "#$%^&*");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testInternationSMSEnabledUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY,
                MxosEnums.BooleanType.YES.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String internationalSmsEnabled = getParams(inputParams)
                .getInternationalSMSEnabled().name();
        assertNotNull("Is null.", internationalSmsEnabled);
        assertTrue("Has a wrong value.",
                internationalSmsEnabled
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testInternationSMSEnabledUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testInternationSMSEnabledUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, INTERNATIONALSMSENABLED_KEY,
                MxosEnums.BooleanType.NO.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String internationalSmsEnabled = getParams(inputParams)
                .getInternationalSMSEnabled().name();
        assertNotNull("Is null.", internationalSmsEnabled);
        assertTrue("Has a wrong value.",
                internationalSmsEnabled
                        .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }
   */
    // Concatenated SMS Allowed Tests

    @Test
    public void testConcatenationSMSAllowedUpdateNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY, null);
        updateParams(inputParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED);
    }

    @Test
    public void testConcatenationSMSAllowedUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY, "");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED);
    }

    @Test
    public void testConcatenationSMSAllowedUpdateInvalidParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY, "Junk");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED);
    }

    @Test
    public void testConcatenationSMSAllowedUpdateSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY, "#$%^&*");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_CONCATENATED_SMS_ALLOWED);
    }

    @Test
    public void testConcatenationSMSAllowedUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY,
                MxosEnums.BooleanType.YES.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String concatenatedSmsAllowed = getParams(inputParams)
                .getConcatenatedSMSAllowed().name();
        assertNotNull("Is null.", concatenatedSmsAllowed);
        assertTrue("Has a wrong value.",
                concatenatedSmsAllowed
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testConcatenationSMSAllowedUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSAllowedUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSALLOWED_KEY,
                MxosEnums.BooleanType.NO.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String concatenatedSmsAllowed = getParams(inputParams)
                .getConcatenatedSMSAllowed().name();
        assertNotNull("Is null.", concatenatedSmsAllowed);
        assertTrue("Has a wrong value.",
                concatenatedSmsAllowed
                        .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    }

    // commenting this part of code as this is not part of COSSmsOnline POST
    // Concatenated SMS Enabled Tests

    /* @Test
    public void testConcatenationSMSEnabledUpdateNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY, null);
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testConcatenationSMSEnabledUpdateEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY, "");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testConcatenationSMSEnabledUpdateInvalidParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY, "Junk");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testConcatenationSMSEnabledUpdateSplCharsInParam()
            throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY, "#$%^&*");
        updateWithoutParams(inputParams,
                ErrorCode.GEN_BAD_REQUEST);
    }

    @Test
    public void testConcatenationSMSEnabledUpdateSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY,
                MxosEnums.BooleanType.YES.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String concatenatedSmsEnabled = getParams(inputParams)
                .getConcatenatedSMSEnabled().name();
        assertNotNull("Is null.", concatenatedSmsEnabled);
        assertTrue("Has a wrong value.",
                concatenatedSmsEnabled
                        .equalsIgnoreCase(MxosEnums.BooleanType.YES.name()));
    }

    @Test
    public void testConcatenationSMSEnabledUpdateSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testConcatenationSMSEnabledUpdateSuccess1");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, CONCATENATEDSMSENABLED_KEY,
                MxosEnums.BooleanType.NO.name());
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        String concatenatedSmsEnabled = getParams(inputParams)
                .getConcatenatedSMSEnabled().name();
        assertNotNull("Is null.", concatenatedSmsEnabled);
        assertTrue("Has a wrong value.",
                concatenatedSmsEnabled
                        .equalsIgnoreCase(MxosEnums.BooleanType.NO.name()));
    } */

    // MaxSMSPerday tests
    @Test
    public void testMaxSMSPerDayNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxSMSPerDayNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_SMS_PERDAY_KEY, null);

        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY);
    }

    @Test
    public void testMaxSMSPerDayEmptyParam() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testMaxSMSPerDayEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_SMS_PERDAY_KEY, "");

        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY);
    }

    @Test
    public void testMaxSMSPerDayInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDayInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_SMS_PERDAY_KEY, "Junk");
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY);
    }

    @Test
    public void testMaxSMSPerDaySplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxSMSPerDaySplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_SMS_PERDAY_KEY, "#$%^&*");
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_SMS_PER_DAY);
    }

    @Test
    public void testMaxSMSPerDaySuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "testMaxSMSPerDaySuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_SMS_PERDAY_KEY, "123");
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);

        Integer integerValue = getParams(inputParams).getMaxSMSPerDay();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 123));
    }

    // Max Concatenated SMS Segments
    @Test
    public void testMaxConcatenatedSMSSegmentsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, null);
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "Junk");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsMaxBoundary() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "256");
        updateParams(inputParams);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsNegValue() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "-1");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsOutOfRange() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "257");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS);
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsZero() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "0");
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        Integer integerValue = getParams(inputParams)
                .getMaxConcatenatedSMSSegments();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 0));
    }

    @Test
    public void testMaxConcatenatedSMSSegmentsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxConcatenatedSMSSegmentsSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_CONCATENATED_SMS_SEGMENTS_KEY, "256");
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        Integer integerValue = getParams(inputParams)
                .getMaxConcatenatedSMSSegments();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 256));
    }

    @Test
    public void testMaxPerCaptchaSMSNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_SMS_KEY, null);
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS);
    }

    @Test
    public void testMaxPerCaptchaSMSEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_SMS_KEY, "");
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS);
    }

    @Test
    public void testMaxPerCaptchaSMSInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_SMS_KEY, "Junk");
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS);
    }

    @Test
    public void testMaxPerCaptchaSMSSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_SMS_KEY, "#$%^&*(");
        updateParams(inputParams, MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_SMS);
    }

    @Test
    public void testMaxPerCaptchaSMSSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaSMSSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_SMS_KEY, "12345");
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        Integer integerValue = getParams(inputParams).getMaxPerCaptchaSMS();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 12345));
    }

    @Test
    public void testMaxPerCaptchaDurationMinsNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsNullParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_DURATION_MINS, null);
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsEmptyParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_DURATION_MINS, "");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsInvalidParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_DURATION_MINS, "Junk");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsSplCharsInParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsSplCharsInParam");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_DURATION_MINS, "#$%^&*(");
        updateParams(inputParams,
                MailboxError.MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS);
    }

    @Test
    public void testMaxPerCaptchaDurationMinsSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testMaxPerCaptchaDurationMinsSuccess");
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        addParams(inputParams, MAX_PERCAPTCHA_DURATION_MINS, "12345");
        updateParams(inputParams);
        inputParams.clear();
        addParams(inputParams, COSID_KEY, COSID_VALUE);
        Integer integerValue = getParams(inputParams)
                .getMaxPerCaptchaDurationMins();
        assertNotNull("Is null.", integerValue);
        assertTrue("Has a wrong value.", (integerValue == 12345));
    }
}
