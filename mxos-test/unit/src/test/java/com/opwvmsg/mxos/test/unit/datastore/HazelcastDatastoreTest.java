package com.opwvmsg.mxos.test.unit.datastore;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.MultiMap;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;
import com.opwvmsg.mxos.utils.datastore.IDataStoreProvider;
import com.opwvmsg.mxos.utils.datastore.hazelcast.HazelcastDataStoreProvider;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static java.util.concurrent.TimeUnit.SECONDS;

public class HazelcastDatastoreTest {


    private static final String TEST_NAME = HazelcastDatastoreTest.class.getSimpleName();
    private static final String ARROW_SEP = " --> ";
    
    private static IDataStoreProvider<String, String> dsProvider;
    private static IDataStoreMultiMap<String, String> dsMultiMap;
    private static IDataStoreMap<String, String> dsMap;
    
    private static HazelcastInstance otherHzInstance;
    private static IMap<String, String> otherMap;
    private static MultiMap<String, String> otherMultiMap;
    
    private static final String MAP_NAME = "testHZmap";
    private static final String KEY = "TestKey";
    private static final String VAL = "TestValue";
    
    private static final String MMAP_NAME = "testHZmultiMap";
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");

        /* for loading mxos.properties */
        ContextUtils.loadContext();
        
        /* set mxos hz config */
        String hzConfig = System.getProperty("hazelcast.config");
        System.out.println(hzConfig);
        File f = new File(hzConfig);
        assertTrue(f.exists());
        
        /* create the provider instance this will start a new Hz Instance thru
         * mxos code */
        try {
            dsProvider = new HazelcastDataStoreProvider<String, String>();
        } catch (MxOSException e) {
            System.out.println(e.getMessage());
            fail();
        }
        
        dsMultiMap = dsProvider.getMultiMapDataStore();
        dsMap = dsProvider.getMapDataStore();
        
        /* create one more test instance of Hz for evaluating the values */
        otherHzInstance = Hazelcast.newHazelcastInstance();
        otherMap = otherHzInstance.getMap(MAP_NAME);
        otherMultiMap = otherHzInstance.getMultiMap(MMAP_NAME);
        
        assertTrue(otherHzInstance.getName().contains("mxos"));
    }
    
    /**
     * Value is put into the HzInstance running in mxos and same value fetched
     * thru test instance "otherHzInstance".
     */
    @Test
    public void mapPutTest() {
        System.out.println(TEST_NAME + ARROW_SEP + "mapPutTest");
        dsMap.put(MAP_NAME, KEY, VAL);

        assertTrue(otherMap.get(KEY).equals(VAL));
    }

    /**
     * Put data with TTL = 3 sec, fetch same data after 5sec -> the value should
     * be null, and should return valid value before 3 sec.
     * 
     * @throws InterruptedException
     */
    @Test
    public void mapPutTtlTest() throws InterruptedException{
        System.out.println(TEST_NAME + ARROW_SEP + "mapPutTtlTest");
        dsMap.put(MAP_NAME, KEY, "mapPutTtlTest");
        
        String pervious = dsMap.put(MAP_NAME, KEY, VAL, 3, SECONDS);
        assertEquals("mapPutTtlTest", pervious);
        
        Thread.currentThread().sleep(1500);
        /* same value should be available */
        assertTrue(otherMap.get(KEY).equals(VAL));
        
        Thread.currentThread().sleep(4500);
        /* value should be evited by now */
        assertNull(otherMap.get(KEY));
    }
    
    /**
     * Putting a whole map into HzInstance and verifying thru other instance.
     * Test also for getAll, size.
     */
    @Test
    public void mapPutAllSizeGetAllTest() {
        System.out.println(TEST_NAME + ARROW_SEP + "mapPutAllSizeGetAllTest");
        dsMap.clear(MAP_NAME);
        assertEquals(0, dsMap.size(MAP_NAME));
        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put(KEY + 1, VAL + 1);
        testMap.put(KEY + 2, VAL + 2);
        testMap.put(KEY + 3, VAL + 3);

        dsMap.putAll(MAP_NAME, testMap);

        assertEquals(testMap.size(), otherMap.size());
        assertEquals(testMap.get(KEY + 3), otherMap
                .get(KEY + 3));
        
        Set<String> kSet = new HashSet<String>();
        kSet.add(KEY+1);
        kSet.add(KEY+2);
        kSet.add(KEY+3);
        
        Map<String, String> resultMap = otherMap.getAll(kSet);
        
        assertEquals(testMap.get(KEY+1), resultMap.get(KEY+1));
        assertEquals(testMap.get(KEY+2), resultMap.get(KEY+2));
        assertEquals(testMap.get(KEY+3), resultMap.get(KEY+3));
    }
    
    /**
     * Test with Set-ttl
     * @throws InterruptedException
     */
    @Test
    public void mapSetTtlTest() throws InterruptedException{
        System.out.println(TEST_NAME + ARROW_SEP + "mapSetTtlTest");
        
        dsMap.set(MAP_NAME, KEY, VAL, 3, SECONDS);
        
        Thread.currentThread().sleep(1500);
        /* same value should be available */
        assertTrue(otherMap.get(KEY).equals(VAL));
        
        Thread.currentThread().sleep(4500);
        /* value should be evited by now */
        assertNull(otherMap.get(KEY));
    }
    
    /**
     * Test remove, evict and clear. 
     */
    @Test
    public void mapRemoveEvictClear(){
        System.out.println(TEST_NAME + ARROW_SEP + "mapRemoveEvictClear");
        dsMap.clear(MAP_NAME);
        assertEquals(0, dsMap.size(MAP_NAME));
        Map<String, String> testMap = new HashMap<String, String>();
        testMap.put(KEY + 1, VAL + 1);
        testMap.put(KEY + 2, VAL + 2);
        testMap.put(KEY + 3, VAL + 3);
        
        dsMap.putAll(MAP_NAME, testMap);
        
        dsMap.evict(MAP_NAME, KEY+1);
        assertEquals(2, otherMap.size());
        
        dsMap.remove(MAP_NAME, KEY+2);
        assertEquals(1, otherMap.size());
    }
    
    /**
     * Test MultiMap putEntry and Get
     */
    @Test
    public void multiMapPutEntryGet(){
        System.out.println(TEST_NAME + ARROW_SEP + "multiMapPutEntryGet");
        fillMultiMap("");
        
        assertEquals(otherMultiMap.size(), dsMultiMap.size(MMAP_NAME));
        
        assertFalse(dsMultiMap.containsEntry(MMAP_NAME, KEY, VAL+"x"));
        assertTrue(dsMultiMap.containsEntry(MMAP_NAME, KEY, VAL+2));
        
        otherMultiMap.clear();
        assertEquals(0, dsMultiMap.size(MMAP_NAME));
    }
    
    /**
     * Test for remove key, remove entry and contains.
     */
    @Test
    public void multiMapRemove(){
        System.out.println(TEST_NAME + ARROW_SEP + "multiMapRemove");
        fillMultiMap("");
        
        dsMultiMap.remove(MMAP_NAME, KEY);
        assertEquals(0, dsMultiMap.size(MMAP_NAME));
        
        String key = "NewKey";
        fillMultiMap(key);
        
        dsMultiMap.removeEntry(MMAP_NAME, key, VAL);
        assertFalse(otherMultiMap.containsEntry(key, VAL));
        assertTrue(dsMultiMap.containsEntry(MMAP_NAME, key, VAL+1));        
    }
    
    private void fillMultiMap(String aKey){
        String key = aKey.isEmpty() ? KEY : aKey;
        dsMultiMap.putEntry(MMAP_NAME, key, VAL);
        dsMultiMap.putEntry(MMAP_NAME, key, VAL+1);
        dsMultiMap.putEntry(MMAP_NAME, key, VAL+2);
        dsMultiMap.putEntry(MMAP_NAME, key, VAL+3);
    }
}
