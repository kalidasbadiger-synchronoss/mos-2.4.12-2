package com.opwvmsg.mxos.test.unit.process;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class FBISendSmsTest {

    private static final String SMSTYPE_SMSONLINE = "advancednotification";
    private static final String SMSTYPE_ADVANCEDNOTIFICATION = "advancednotification";

    private static final String FROM_ADDRESS_VALUE = "+19886576790";

    private static final String TO_ADDRESS_VALUE_SUCCESS = "+19886576790";
    private static final String TO_ADDRESS_VALUE_TEMPERROR = "+19886180101";
    private static final String TO_ADDRESS_VALUE_PERMERROR = "+19986600570";
    private static final String TO_ADDRESS_VALUE_INVALIDXML = "+19393828866";
    private static final String TO_ADDRESS_VALUE_INVALIDERRCODE = "+08041067791";
    private static final String TO_ADDRESS_VALUE_SNMP = "+19986600571";

    private static final String MESSAGE_VALUE = "Hi Message";

    private static ISendSMSService sendSmsService;
    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("FBISendSmsTest.setUpBeforeClass...");
        if (System.getProperty("MXOS_HOME") == null
                || System.getProperty("MXOS_HOME").equals("")) {
            System.setProperty("MXOS_HOME", "D:\\LeapFrog\\workspace\\mxos");
        }
        sendSmsService = (ISendSMSService) ContextUtils.loadContext()
                .getService(ServiceEnum.SendSmsService.name());
        assertNotNull("MailboxService object is null.", sendSmsService);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("FBISendSmsTest.tearDownAfterClass...");
        params.clear();
        params = null;
        sendSmsService = null;
    }

    void addParams(Map<String, List<String>> inputParams, String Key,
            String Value) {
        List<String> p = new ArrayList<String>();
        p.add(Value);
        inputParams.put(Key, p);
    }

    @Test
    public void testSendSms_Success() {
        System.out.println("FBISendSmsTest.Success...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (Exception e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_FBITempError() {
        System.out.println("FBISendSmsTest.FBITempError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_TEMPERROR);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_TEMPORARY_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_FBIPermanentError() {
        System.out.println("FBISendSmsTest.FBIPermanentError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_PERMERROR);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }
    
    @Test
    public void testSendSms_FBIPermanentSNMPError() {
        System.out.println("FBISendSmsTest.FBIPermanentSNMPError...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_SMSONLINE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SNMP);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERM_SNMP_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_InValidXMLFromFBI() {
        System.out.println("FBISendSmsTest.InValidXMLFromFBI...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_INVALIDXML);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_inValidStatusFromFBI() {
        System.out.println("FBISendSmsTest.inValidStatusFromFBI...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_INVALIDERRCODE);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_PERMANENT_ERROR.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_SmsTypeNotSent() {
        System.out.println("FBISendSmsTest.SmsTypeNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_fromAddressNotSent() {
        System.out.println("FBISendSmsTest.fromAddressNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressNotSent() {
        System.out.println("FBISendSmsTest.toAddressNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_messageNotSent() {
        System.out.println("FBISendSmsTest.messageNotSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidSmsTypeSent() {
        System.out.println("FBISendSmsTest.invalidSmsTypeSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(), "sdfsdfs");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
        }
    }


    @Test
    public void testSendSms_invalidCharactersinsmsType() {
        System.out
                .println("FBISendSmsTest.invalidCharactersinsmsType...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(), "!@#$%^&*()");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SMS_INVALID_SMS_TYPE.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidfromAddress() {
        System.out.println("FBISendSmsTest.invalidfromAddress...");

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        addParams(inputParams, SmsProperty.smsType.name(), SMSTYPE_ADVANCEDNOTIFICATION);
        addParams(inputParams, SmsProperty.fromAddress.name(), "sdfsdf");
        addParams(inputParams, SmsProperty.toAddress.name(),
                TO_ADDRESS_VALUE_SUCCESS);
        addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
        try {
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }

    }

    @Test
    public void testSendSms_fromAddressisnull() {
        System.out.println("FBISendSmsTest.fromAddressisnull...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(), "");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(),
                    e.getCode());
        }
    }

    @Test
    public void testSendSms_fromAddressContainsInvalidCharacters() {
        System.out
                .println("FBISendSmsTest.fromAddressCntainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.fromAddress.name(), "!@#$%^&*(");
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_FROM_ADDRESS.name(),
                    e.getCode());
        }
    }

    @Test
    public void testSendSms_invalidtoAddress() {
        System.out.println("FBISendSmsTest.invalidtoAddress...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(), "sdfsdf");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_TO_ADDRESS.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressisnull() {
        System.out.println("FBISendSmsTest.toAddressisnull...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(), "");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    ErrorCode.GEN_BAD_REQUEST.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_toAddressContainsInvalidCharacters() {
        System.out
                .println("FBISendSmsTest.fromAddressCntainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(), "!@#$%^&*(");
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {

            assertEquals("Some unexpected error code.",
                    CustomError.SEND_SMS_INVALID_TO_ADDRESS.name(), e.getCode());
        }
    }

    @Test
    public void testSendSms_messageIsNUll() {
        System.out.println("FBISendSmsTest.messageIsNUll...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "");
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_messageContainsInvalidCharacters() {
        System.out
                .println("FBISendSmsTest.messageContainsInvalidCharacters...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), "@!#$%^&*(");
            sendSmsService.process(inputParams);

        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_optionalParamsSent() {
        System.out.println("FBISendSmsTest.optionalParamsSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, SmsProperty.pid.name(), "23424");
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

    @Test
    public void testSendSms_additionalParamsSent() {
        System.out.println("FBISendSmsTest.additionalParamsSent...");
        try {
            Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
            addParams(inputParams, SmsProperty.smsType.name(),
                    SMSTYPE_ADVANCEDNOTIFICATION);
            addParams(inputParams, SmsProperty.toAddress.name(),
                    TO_ADDRESS_VALUE_SUCCESS);
            addParams(inputParams, SmsProperty.fromAddress.name(),
                    FROM_ADDRESS_VALUE);
            addParams(inputParams, SmsProperty.message.name(), MESSAGE_VALUE);
            addParams(inputParams, "sdfsdf", "sdfsdf");
            sendSmsService.process(inputParams);
        } catch (MxOSException e) {
            assertFalse("Exception Happened", true);
        }
    }

}
