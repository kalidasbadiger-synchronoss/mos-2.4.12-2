package com.opwvmsg.mxos.test.unit.mailstore;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailStore
@SuiteClasses({ 
    MailStoreGET.class, 
    MailStorePOST.class,
    ExternalStorePOST.class,
    GroupAdminAllocationsGETAndPOST.class
    })
public class MailStoreTestSuite {
}
