package com.opwvmsg.mxos.test.unit.addressbook;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to AddressBook
@SuiteClasses({ AddressBookDelete.class })
public class AddressBookTestSuite {
}
