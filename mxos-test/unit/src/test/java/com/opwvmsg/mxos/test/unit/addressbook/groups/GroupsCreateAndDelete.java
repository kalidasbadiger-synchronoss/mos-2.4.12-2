package com.opwvmsg.mxos.test.unit.addressbook.groups;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class GroupsCreateAndDelete {

    private static final String TEST_NAME = "GroupsCreateAndDelete";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long GROUPID;

    private static IGroupsService groupsService;
    private static IExternalLoginService externalLoginService;
    private static IGroupsBaseService groupsBaseService;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();
    private static ExternalSession session = null;

    /**
     * 
     * @throws Exception
     */

    private static GroupBase getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static GroupBase getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        GroupBase groupBase = null;
        try {
            groupBase = groupsBaseService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            }
        } catch (MxOSException e) {
            assertNotNull("MxOSError is not null", e);
        }
        return groupBase;
    }

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        groupsService = (IGroupsService) ContextUtils.loadContext().getService(
                ServiceEnum.GroupsService.name());
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        groupsBaseService = (IGroupsBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.GroupsBaseService.name());
        login(USERID, PASSWORD);
        

    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        groupsService = null;
        externalLoginService = null;
    }

    // create post
    @Test
    public void testGroupsCreate() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsCreate");

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put("groupName", new ArrayList<String>());
        params.get("groupName").add("TestGroup");
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        GROUPID = groupsService.create(params);
        assertNotNull("GroupId Is null", GROUPID);

    }

    // base post
    @Test
    public void testNegGroups() throws Exception {
        try {
            System.out.println(TEST_NAME + ARROW_SEP + "testNegGroups");
            params.clear();
            params.put(AddressBookProperty.userId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.userId.name()).add(USERID);

            params.put("groupName", new ArrayList<String>());
            params.get("groupName")
                    .add("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
            if (session != null) {
                params.put(AddressBookProperty.sessionId.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.sessionId.name()).add(
                        session.getSessionId());

                params.put(AddressBookProperty.cookieString.name(),
                        new ArrayList<String>());
                params.get(AddressBookProperty.cookieString.name()).add(
                        session.getCookieString());
            }
            GROUPID = groupsService.create(params);

        } catch (MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(AddressBookError.ABS_INVALID_GROUP_NAME.name(),
                    e.getCode());
        }
    }

    // delete post
    @Test
    public void testGroupsDelete() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testGroupsDelete");

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.groupId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.groupId.name()).add(
                String.valueOf(GROUPID));
        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }
        groupsService.delete(params);

        GroupBase base = getParams(params);
        assertNull("GroupBase Is not null", base);
    }
}
