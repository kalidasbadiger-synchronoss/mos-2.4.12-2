package com.opwvmsg.mxos.test.unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;

public class MailboxHelper {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String PASSWORD_KEY = MailboxProperty.password.name();
    private static final String COS_ID_KEY = MailboxProperty.cosId.name();
    private static final String DEFAULT_COS_ID = "ut_default12314";

    private static IMailboxService mb;

    static {
        try {
            mb = (IMailboxService) ContextUtils.loadContext().getService(
                    ServiceEnum.MailboxService.name());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String createMailbox(String email, String password) {
        return createMailbox(email, password, null);
    }

    public static String createMailbox(String email, String password,
            boolean createCos) {
        boolean cosCreated = false;
        if (createCos) {
            cosCreated = CosHelper.createCos(DEFAULT_COS_ID);
        }
        return cosCreated ? createMailbox(email, password, DEFAULT_COS_ID) : null;
    }

    public static String createMailbox(String email, String password, String cosId) {
        String id = null;
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.put(PASSWORD_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(email);
        params.get(PASSWORD_KEY).add(password);
        if (null != cosId) {
            params.put(COS_ID_KEY, new ArrayList<String>());
            params.get(COS_ID_KEY).add(cosId);
        }
        try {
            id = mb.create(params);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public static void deleteMailbox(String email, boolean cosCreated) {
        deleteMailbox(email);
        CosHelper.deleteCos(DEFAULT_COS_ID);
    }

    public static void deleteMailbox(String email) {
        Map<String, List<String>> params = new HashMap<String, List<String>>();
        params.put(EMAIL_KEY, new ArrayList<String>());
        params.get(EMAIL_KEY).add(email);
        try {
            mb.delete(params);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
    }
}
