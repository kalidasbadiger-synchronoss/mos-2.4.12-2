package com.opwvmsg.mxos.test.unit.cos.socialnetworks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;

import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSocialNetworksService;
import com.opwvmsg.mxos.test.unit.ContextUtils;

public class CosSocialNetworksTest {
    private static ICosSocialNetworksService cossn = null;
    private static ICosService cosService = null;
    private static Map <String, List<String> >inputParams =
        new HashMap<String, List<String>>();
    private static String test = "ut_default";
    private static String cosIdKey = "cosId";
    
    
    @BeforeClass
    public static void BeforeClassSetUp() throws Exception {
        System.out.println("BeforeClassSetUp:cos starting......");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.put("loadDefaultCos", new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.get("loadDefaultCos").add("yes");
        try {
            /*cossn = (ICosSocialNetworksService) ContextUtils.loadContext()
                .getContext()
                    .getService(ServiceEnum.CosSocialNetworksService.name());*/
            
            cossn = (ICosSocialNetworksService) ContextUtils.loadContext()
                    .getService(ServiceEnum.CosSocialNetworksService.name());
            cosService = (ICosService) ContextUtils.loadContext().getService(
                    ServiceEnum.CosService.name());
            cosService.create(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
        }
        assertNotNull(cossn);
    }
    
    @Test
    public void testSocialNetworksRead() {
        System.out.println("testSocialNetworksRead:cos starting ...");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        try {
        SocialNetworks sn = cossn.read(inputParams);
        assertNotNull(sn);
        } catch (Exception e) {
            System.out.println("Exception: "+e);
            fail();
        }
    }
    
    @AfterClass
    public static void TearDown() throws Exception {
        System.out.println("TearDown:cos starting......");
        inputParams.clear();
        inputParams.put("cosId", new ArrayList<String>());
        inputParams.get("cosId").add(test);
        try {
            cosService.delete(inputParams);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testSocialNetworkUpdate() {
        System.out.println("testSocialNetworkUpdate:cos starting");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.put("socialNetworkIntegrationAllowed",new ArrayList<String>());
        inputParams.get("socialNetworkIntegrationAllowed").add("yes");
        try {
            cossn.update(inputParams);
            inputParams.clear();
            inputParams.put(cosIdKey, new ArrayList<String>());
            inputParams.get(cosIdKey).add(test);
            SocialNetworks sn  = cossn.read(inputParams);
            assertEquals("They are equals", MxosEnums.BooleanType.YES, sn.getSocialNetworkIntegrationAllowed());
        } catch(Exception e) {
            System.out.println("Exception: "+ e);
            fail();
        }
    }
    
    @Test
    public void testSocialNetworkUpdate2() {
        System.out.println("testSocialNetworkUpdate2:cos starting ...");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.put("socialNetworkIntegrationAllowed",new ArrayList<String>());
        inputParams.get("socialNetworkIntegrationAllowed").add("");
        try {
            cossn.update(inputParams);
            inputParams.clear();
            inputParams.put(cosIdKey, new ArrayList<String>());
            inputParams.get(cosIdKey).add(test);
            SocialNetworks sn  = cossn.read(inputParams);
            assertNull("socialNetworkIntegrationAllowed is null", sn.getSocialNetworkIntegrationAllowed());
        } catch(Exception e) {
            System.out.println("Exception: "+ e);
            fail();
        }
    }

    @Test
    public void testSocialNetworkUpdateNeg() {
        System.out.println("testSocialNetworkUpdateNeg:cos starting");
        inputParams.clear();
        inputParams.put(cosIdKey, new ArrayList<String>());
        inputParams.get(cosIdKey).add(test);
        inputParams.put("socialNetworkIntegrationAllowed",new ArrayList<String>());
        inputParams.get("socialNetworkIntegrationAllowed").add("Invalid");
        try {
            cossn.update(inputParams);
            inputParams.clear();
            inputParams.put(cosIdKey, new ArrayList<String>());
            inputParams.get(cosIdKey).add(test);
            cossn.read(inputParams);
            fail();
        } catch(MxOSException e) {
            System.out.println(e.getCode());
            assertEquals(e.getCode(),MailboxError.MBX_INVALID_SNINTEGRATIONALLOWED.toString());
            
        }
    }
}
