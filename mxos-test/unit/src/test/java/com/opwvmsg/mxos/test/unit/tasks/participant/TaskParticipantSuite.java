package com.opwvmsg.mxos.test.unit.tasks.participant;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to MailAccess
@SuiteClasses({ TaskParticipantCreate.class })
public class TaskParticipantSuite {
}
