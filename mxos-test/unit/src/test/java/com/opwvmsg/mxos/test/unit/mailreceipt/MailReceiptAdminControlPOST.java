package com.opwvmsg.mxos.test.unit.mailreceipt;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.AdminDefaultDispositionAction;
import com.opwvmsg.mxos.data.enums.MxosEnums.AdminRejectActionFlag;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IAdminControlService;
import com.opwvmsg.mxos.test.unit.ContextUtils;
import com.opwvmsg.mxos.test.unit.MailboxHelper;

/**
 * 
 * @author mxos-dev
 * 
 */
public class MailReceiptAdminControlPOST {

    private static final String EMAIL_KEY = MailboxProperty.email.name();
    private static final String EMAIL = "head2@openwave.com";
    private static final String PASSWORD = "password";
    private static Map<String, List<String>> getParams = new HashMap<String, List<String>>();
    private static Map<String, List<String>> updateParams = new HashMap<String, List<String>>();
    private static final String TEST_NAME = "MailReceiptAdminControlPOST";
    private static final String ARROW_SEP = " --> ";
    private static IAdminControlService service;

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        service = (IAdminControlService) ContextUtils.loadContext().getService(
                ServiceEnum.AdminControlService.name());
        String mailboxId = MailboxHelper.createMailbox(EMAIL, PASSWORD, true);
        assertTrue("MailBox was not created.", mailboxId != null);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
        addToParams(getParams, EMAIL_KEY, EMAIL);
    }

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUp");
        assertNotNull("service object is null.", service);
        assertNotNull("Input Param:email is null.", updateParams.get(EMAIL_KEY));
        assertTrue("Input Param:email is empty.", !updateParams.get(EMAIL_KEY)
                .isEmpty());
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDown");
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        MailboxHelper.deleteMailbox(EMAIL, true);
        getParams.clear();
        getParams = null;
        updateParams.clear();
        updateParams = null;
        service = null;
    }

    private static void addToParams(Map<String, List<String>> params,
            String key, String value) {
        params.put(key, new ArrayList<String>());
        params.get(key).add(value);
    }

    private static AdminControl getParams() {
        AdminControl object = null;
        try {
            object = service.read(getParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        } finally {
            if (getParams.isEmpty() || null == getParams.get(EMAIL_KEY)
                    || getParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(getParams, EMAIL_KEY, EMAIL);
            }
        }
        return object;
    }

    private static void updateParams(Map<String, List<String>> updateParams) {
        updateParams(updateParams, null);
    }

    private static void updateParams(Map<String, List<String>> updateParams,
            String expectedError) {
        try {
            service.update(updateParams);
            if (null != expectedError) {
                fail("This should not have come!!!");
            }
        } catch (MxOSException e) {
            e.printStackTrace();
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSException is not null", e);
                assertEquals("Error is not " + expectedError + ".",
                        expectedError, e.getCode());
            }
        } finally {
            if (updateParams.isEmpty() || null == updateParams.get(EMAIL_KEY)
                    || updateParams.get(EMAIL_KEY).isEmpty()) {
                addToParams(updateParams, EMAIL_KEY, EMAIL);
            }
        }
    }

    @Test
    public void testUpdateWithoutEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithoutEmail");
        // Clear the params.
        updateParams.remove(EMAIL_KEY);
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNullEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithNullEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, null);
        updateParams(updateParams, ErrorCode.MXS_INPUT_ERROR.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithEmptyEmail() {
        System.out.println(TEST_NAME + ARROW_SEP + "testUpdateWithEmptyEmail");
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithNonExistingEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithNonExistingEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInEmail() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInEmail");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "some.!@#$%^&.junk@foobar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    @Test
    public void testUpdateWithSplCharsInDomain() {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testUpdateWithSplCharsInDomain");
        // Replace openwave with a junk value to set an invalid email.
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, "something.junk@$&*^-bar.com");
        updateParams(updateParams, ErrorCode.GEN_BAD_REQUEST.name());
        updateParams.remove(EMAIL_KEY);
        addToParams(updateParams, EMAIL_KEY, EMAIL);
    }

    // adminDefaultDisposition
    @Test
    public void testAdminDefaultDispositionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminDefaultDispositionNullParam");
        String key = MailboxProperty.adminDefaultDisposition.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_DEFAULT_DISPOSITION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminDefaultDispositionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminDefaultDispositionEmptyParam");
        String key = MailboxProperty.adminDefaultDisposition.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_DEFAULT_DISPOSITION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminDefaultDispositionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminDefaultDispositionInvalidParam");
        String key = MailboxProperty.adminDefaultDisposition.name();
        addToParams(updateParams, key, "fasdfasdf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_DEFAULT_DISPOSITION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminDefaultDispositionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminDefaultDispositionSuccess");
        String key = MailboxProperty.adminDefaultDisposition.name();
        addToParams(updateParams, key,
                AdminDefaultDispositionAction.REJECT.name());
        updateParams(updateParams);
        AdminDefaultDispositionAction value = getParams()
                .getAdminDefaultDisposition();
        System.out.println("value == " + value.toString());
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value,
                AdminDefaultDispositionAction.REJECT);
        updateParams.remove(key);
    }

    @Test
    public void testAdminDefaultDispositionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminDefaultDispositionSuccess1");
        String key = MailboxProperty.adminDefaultDisposition.name();
        addToParams(updateParams, key,
                AdminDefaultDispositionAction.ACCEPT.name());
        updateParams(updateParams);
        AdminDefaultDispositionAction value = getParams()
                .getAdminDefaultDisposition();
        System.out.println("value == " + value.toString());
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value,
                AdminDefaultDispositionAction.ACCEPT);
        updateParams.remove(key);
    }

    // adminRejectAction
    @Test
    public void testAdminRejectActionNullParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectActionNullParam");
        String key = MailboxProperty.adminRejectAction.name();
        addToParams(updateParams, key, null);
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_REJECT_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminRejectActionEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectActionEmptyParam");
        String key = MailboxProperty.adminRejectAction.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_REJECT_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminRejectActionInvalidParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectActionInvalidParam");
        String key = MailboxProperty.adminRejectAction.name();
        addToParams(updateParams, key, "sadfasf");
        updateParams(updateParams,
                MailboxError.MBX_INVALID_ADMIN_REJECT_ACTION.name());
        updateParams.remove(key);
    }

    @Test
    public void testAdminRejectActionSuccess() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectActionSuccess");
        String key = MailboxProperty.adminRejectAction.name();
        addToParams(updateParams, key, AdminRejectActionFlag.DROP.name());
        updateParams(updateParams);
        AdminRejectActionFlag value = getParams().getAdminRejectAction();
        System.out.println("value == " + value.toString());
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AdminRejectActionFlag.DROP);
        updateParams.remove(key);
    }

    @Test
    public void testAdminRejectActionSuccess1() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectActionSuccess1");
        String key = MailboxProperty.adminRejectAction.name();
        addToParams(updateParams, key, AdminRejectActionFlag.FORWARD.name());
        updateParams(updateParams);
        AdminRejectActionFlag value = getParams().getAdminRejectAction();
        System.out.println("value == " + value.toString());
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, AdminRejectActionFlag.FORWARD);
        updateParams.remove(key);
    }

    // adminRejectInfo
    @Test
    public void testAdminRejectInfoEmptyParam() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP
                + "testAdminRejectInfoEmptyParam");
        String key = MailboxProperty.adminRejectInfo.name();
        addToParams(updateParams, key, "");
        updateParams(updateParams);
        updateParams.remove(key);
    }

    @Test
    public void testAdminRejectInfoSuccess() throws Exception {
        System.out
                .println(TEST_NAME + ARROW_SEP + "testAdminRejectInfoSuccess");
        String key = MailboxProperty.adminRejectInfo.name();
        String val = "Trash";
        addToParams(updateParams, key, val);
        updateParams(updateParams);
        String value = getParams().getAdminRejectInfo();
        System.out.println("value == " + value);
        assertNotNull("Is null.", value);
        assertEquals("Has a wrong value.", value, val);
        updateParams.remove(key);
    }
}
