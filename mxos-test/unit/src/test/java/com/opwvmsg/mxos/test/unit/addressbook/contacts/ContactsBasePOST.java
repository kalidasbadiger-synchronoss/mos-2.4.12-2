package com.opwvmsg.mxos.test.unit.addressbook.contacts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.test.unit.AddressBookHelper;
import com.opwvmsg.mxos.test.unit.ContextUtils;

/**
 * 
 * @author mxos-dev
 * 
 */
public class ContactsBasePOST {

    private static final String TEST_NAME = "ContactBasePOST";
    private static final String ARROW_SEP = " --> ";
    private static final String USERID = "test";
    private static final String PASSWORD = "test";
    private static long CONTACTID;

    private static IExternalLoginService externalLoginService;
    private static IContactsBaseService contactsBaseService;
    private static ExternalSession session = null;

    private static Map<String, List<String>> params = new HashMap<String, List<String>>();

    private static ContactBase getParams(Map<String, List<String>> params) {
        return getParams(params, null);
    }

    private static ContactBase getParams(Map<String, List<String>> params,
            AddressBookException expectedError) {
        ContactBase base = null;
        try {
            base = contactsBaseService.read(params);
            if (null != expectedError) {
                fail("This should not have been come!!!");
            } else {
                assertNotNull("Contacts name object is null.", base);
            }
        } catch (MxOSException e) {
            if (null == expectedError) {
                fail();
            } else {
                assertNotNull("MxOSError is null", e);
            }
        }
        return base;
    }

    private static void login(String userId, String password) {

        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();

        inputParams.put(AddressBookProperty.userId.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.userId.name()).add(userId);

        inputParams.put(AddressBookProperty.password.name(),
                new ArrayList<String>());
        inputParams.get(AddressBookProperty.password.name()).add(password);

        try {
            session = externalLoginService.login(inputParams);
        } catch (MxOSException e) {
            e.printStackTrace();
            fail();
        }
        assertTrue("Login failed.", session.getSessionId() != null);
    }

    /**
     * 
     * @throws Exception
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "setUpBeforeClass");
        externalLoginService = (IExternalLoginService) ContextUtils.loadContext()
                .getService(ServiceEnum.ExternalLoginService.name());
        contactsBaseService = (IContactsBaseService) ContextUtils.loadContext()
                .getService(ServiceEnum.ContactsBaseService.name());
        login(USERID, PASSWORD);
        CONTACTID = AddressBookHelper.createContact(USERID, session);
    }

    /**
     * @throws java.lang.Exception
     */
    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println(TEST_NAME + ARROW_SEP + "tearDownAfterClass");
        params.clear();
        params = null;
        externalLoginService = null;
        contactsBaseService = null;
        AddressBookHelper.deleteContact(USERID, CONTACTID, session);
    }

    // base post
    @Test
    public void testContactsBase() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testContactsBase");
        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        params.put(AddressBookProperty.notes.name(), new ArrayList<String>());
        params.get(AddressBookProperty.notes.name()).add("notes1");

        params.put(AddressBookProperty.pager.name(), new ArrayList<String>());
        params.get(AddressBookProperty.pager.name()).add("123456");

        params.put(AddressBookProperty.yomiFirstName.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.yomiFirstName.name()).add(
                "yomiFirstName");

        params.put(AddressBookProperty.yomiLastName.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.yomiLastName.name()).add("yomiLastName");

        params.put(AddressBookProperty.yomiCompany.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.yomiCompany.name()).add("yomiCompany");
        
		params.put(AddressBookProperty.otherPhone.name(),
				new ArrayList<String>());
		params.get(AddressBookProperty.otherPhone.name()).add("1234567890");

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        contactsBaseService.update(params);

        params.clear();

        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        ContactBase base = getParams(params);

        String contactId = base.getContactId();
        assertNotNull("ContactId is null.", contactId);
        String notes = base.getNotes();
        assertEquals("updated notes value is not equal.", "notes1", notes);
        String customField = base.getCustomFields();
        assertEquals(
                "updated custom fields value is not equal.",
                "telephone_pager=123456,yomiFirstName=yomiFirstName,yomiLastName=yomiLastName,yomiCompany=yomiCompany",
                customField);
    }

    // base post
    @Test
    public void testNegativeContactsBase() throws Exception {

        System.out.println(TEST_NAME + ARROW_SEP + "testNegativeContactsBase");

        params.clear();
        params.put(AddressBookProperty.userId.name(), new ArrayList<String>());
        params.get(AddressBookProperty.userId.name()).add(USERID);

        params.put(AddressBookProperty.contactId.name(),
                new ArrayList<String>());
        params.get(AddressBookProperty.contactId.name()).add(
                String.valueOf(CONTACTID));

        params.put("isPrivate", new ArrayList<String>());
        params.get("isPrivate").add("aaaaaaa");

        if (session != null) {
            params.put(AddressBookProperty.sessionId.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.sessionId.name()).add(
                    session.getSessionId());

            params.put(AddressBookProperty.cookieString.name(),
                    new ArrayList<String>());
            params.get(AddressBookProperty.cookieString.name()).add(
                    session.getCookieString());
        }

        try {
            contactsBaseService.update(params);
        } catch (Exception e) {
            if (e instanceof MxOSException) {
                // this is expected
            } else {
                fail();
            }
        }

    }
}
