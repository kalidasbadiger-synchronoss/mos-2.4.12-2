package com.opwvmsg.mxos.test.unit.addressbook.contacts.personalInfo;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
// Include all the test case classes related to Contacts Personal Info Events
@SuiteClasses({ ContactsPersonalInfoEventsPUT.class,
        ContactsPersonalInfoEventsPOST.class,
        ContactsPersonalInfoEventsGET.class,
        ContactsPersonalInfoEventsDELETE.class })
public class ContactsPersonalInfoEventsTestSuite {

}
