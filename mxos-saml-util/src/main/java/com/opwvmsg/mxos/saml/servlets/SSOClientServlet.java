/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.saml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.saml.service.SAMLConfig;
import com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider.VmSSOClient;

/**
 * Servlet implementation class SSOClientServlet
 */
public class SSOClientServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private static Logger logger = Logger.getLogger(SSOClientServlet.class);
    private static final String samlIDMSURL = "https://myssotesta.virginmedia.com/vm_sso/idp/requestAssertion.action";
    private static final String samlACSURL = "http://localhost:8080/mxos/v2/saml/acs";
    private static final String samlIssuer = "VMPortal";
    private static final String samlProviderName = "VMPortal";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SSOClientServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        if (logger.isInfoEnabled()) {
            logger.info("SSOClientServlet - GET request received.");
        }
        try {
            SAMLConfig.getInstance(System
                    .getProperty(SystemProperty.samlProvider.name()));
        } catch (Exception e) {
            logger.warn("Unable to create saml provider");
        }
        // the end point of the identity provider
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SAML IMDS url: ").append(samlIDMSURL));
        }
        // the end point of the service provider's assertion consumer service
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SAML ACS url: ").append(samlACSURL));
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SAML provider: ")
                    .append(samlProviderName));
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SAML issuer: ").append(samlIssuer));
        }
        // Create sso client
        VmSSOClient ssoClient = new VmSSOClient(samlIDMSURL, samlProviderName, samlACSURL,
                samlIssuer);
        String relayState = request.getParameter("targetURL");
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SAML target url in request: ")
                    .append(relayState));
        }
        // send authentication request
        ssoClient.sendAuthenticationRequest(relayState, response);
        String subject = null;// leave null to ignore the email validation
        final String samlRequest = ssoClient.createSAMLRequest(relayState,
                response, subject);
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Generated SAML request: ")
                    .append(samlRequest));
        }
        if (logger.isInfoEnabled()) {
            logger.info("SSOClientServlet - GET request ended.");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        if (logger.isInfoEnabled()) {
            logger.info("SSOClientServlet - POST request received.");
        }
        doGet(request, response);
        if (logger.isInfoEnabled()) {
            logger.info("SSOClientServlet - POST request ended.");
        }
    }
}
