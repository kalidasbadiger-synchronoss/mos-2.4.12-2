/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.opensaml.Configuration;
import org.opensaml.DefaultBootstrap;
import org.opensaml.xml.ConfigurationException;

/**
 * Base class of other objects that use the OpenSAML engine. Ensures that the
 * engine only gets bootstrapped once.
 * 
 * @author mxos-dev
 * 
 */
public class VmSAMLObject {

    private static Logger logger = LoggerFactory.getLogger(VmSAMLObject.class);

    // bootstrapping the OpenSAML Engine is expensive...only do it once
    static {
        try {
            logger.debug("Bootstrapping the OpenSAML engine");
            DefaultBootstrap.bootstrap();
            Configuration.setSAMLDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

        } catch (ConfigurationException ex) {
            logger.error("The OpenSAML system was unable to configure itself",
                    ex);
        }
    }

}
