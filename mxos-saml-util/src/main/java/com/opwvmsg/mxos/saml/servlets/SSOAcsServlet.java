/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.saml.exception.SAMLException;
import com.opwvmsg.mxos.saml.service.SAMLConfig;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLUtils;
import com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider.VmAssertionConsumerService;
import com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider.VmAssertionSummary;

/**
 * Servlet implementation class SSOAcsServlet
 * 
 * @author mxos-dev
 */
public class SSOAcsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    public static final String CLEAR = "clear";
    public static final String BASE64_ENC = "base64Compressed";
    public static final String PUBLIC_KEY_ENC = "publicKeyEncryption";
    public static final String MXOS_HOME = "MXOS_HOME";
    public static final String COMMA = ",";
    public static final boolean samlCookies=true;
    public static final String samlAttributeEncryption ="clear";
    public static final String samlAcsExceptionURL="https://myssotesta.virginmedia.com/vm_sso/idp/logout.action";
    public static final String requiredAttrs="vmLoginID";
    public static final String samlPublicKey="saml/openwave.com.key";

    private static Logger logger = Logger.getLogger(SSOAcsServlet.class);

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SSOAcsServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        if (logger.isInfoEnabled()) {
            logger.info("SSOAcsServlet - POST request received.");
        }
        try {
            SAMLConfig.getInstance(System
                    .getProperty(SystemProperty.samlProvider.name()));
        } catch (Exception e) {
            logger.warn("Unable to create saml provider");
        }
        String xmlString = request.getParameter("SAMLResponse");
        String publicKeyPath = System.getProperty(MXOS_HOME) + "/config/"
                + samlPublicKey;
        if (logger.isDebugEnabled()) {
            logger.debug("SAMLResponse - " + xmlString);
            logger.debug("ACS public key path - " + publicKeyPath);
        }

        try {
            VmAssertionConsumerService acs = new VmAssertionConsumerService(
                    publicKeyPath);
            VmAssertionSummary accersionSummary = acs.consumeSAMLResponse(
                    request, response);

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("Assertion summary - ")
                        .append(accersionSummary));
            }
            List<String> configuredAttributeList = new ArrayList<String>();
            if (null != requiredAttrs) {
                String[] samlAttributes = requiredAttrs.split(COMMA);
                if (samlAttributes.length > 0) {
                    for (int i = 0; i < samlAttributes.length; i++) {
                        configuredAttributeList.add(samlAttributes[i]);
                    }
                }
            }

            Map<String, String> assersionAttributeMap = accersionSummary
                    .getAttributeMap();
            Map<String, String> queryAttributeMap = new HashMap<String, String>();
            for (String attribute : configuredAttributeList) {
                if(assersionAttributeMap.containsKey(attribute)){
                    queryAttributeMap.put(attribute,
                            assersionAttributeMap.get(attribute));
                }
            }

            // Check if sending cookie information in query parameters is
            // enabled and add it to query parameters
            if (samlCookies) {
                Cookie cookies[] = request.getCookies();
                if ((cookies == null) || (cookies.length == 0)) {
                        logger.warn("Cookies not found.");
                } else {
                    if (logger.isDebugEnabled()) {
                        logger.debug("Cookies found, sending cookie information"
                                + " in query params is enabled.");
                        logger.debug("cookies length : " + cookies.length);
                    }                    
                    for (int i = 0; i < cookies.length; i++) {
                        Cookie c = cookies[i];
                        if (logger.isDebugEnabled()) {
                            logger.debug(new StringBuffer("Cookie name: ")
                                    .append(c.getName()).append(" value: ")
                                    .append(c.getValue()).append(" maxage: ")
                                    .append(c.getMaxAge()).append(" comment: ")
                                    .append(c.getComment()));
                        }
                        queryAttributeMap.put(c.getName(), c.getValue());
                    }
                }
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug("Sending cookie information"
                            + " in query params is disabled.");
                }
            }

            String queryString = "";
            if (samlAttributeEncryption.equalsIgnoreCase(BASE64_ENC)) {
                queryString = VmSAMLUtils
                        .getBase64CompressedQueryString(queryAttributeMap);
            } else if (samlAttributeEncryption.equalsIgnoreCase(PUBLIC_KEY_ENC)) {
                queryString = VmSAMLUtils
                        .getPublicKeyEncryptedQueryString(queryAttributeMap);
            } else if (samlAttributeEncryption.equalsIgnoreCase(CLEAR)) {
                queryString = VmSAMLUtils
                        .getNonEncryptedQueryString(queryAttributeMap);
            }

            String targetUrl = accersionSummary.getRelayState();
            response.setStatus(HttpServletResponse.SC_SEE_OTHER);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("Redirecing request - ").append(
                        targetUrl).append(queryString));
            }
            response.sendRedirect(targetUrl + queryString);

        } catch (SAMLException ex) {
            logger.error("SAML exception occured"
                    + " while consuming assertion service: " + ex);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SAML logout url: ")
                        .append(samlAcsExceptionURL));
            }
            response.sendRedirect(samlAcsExceptionURL);
        } catch (Exception e) {
            logger.error("General exception occured"
                    + " while consuming assertion service: " + e);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SAML logout url: ")
                        .append(samlAcsExceptionURL));
            }
            response.sendRedirect(samlAcsExceptionURL);
        }
        if (logger.isInfoEnabled()) {
            logger.info("SSOAcsServlet - POST request ended.");
        }
    }
}
