/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.interfaces.DSAPublicKey;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.joda.time.DateTime;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.Issuer;
import org.opensaml.saml2.core.Response;
import org.opensaml.saml2.core.Status;
import org.opensaml.security.SAMLSignatureProfileValidator;
import org.opensaml.xml.XMLObject;
import org.opensaml.xml.security.credential.BasicCredential;
import org.opensaml.xml.security.credential.Credential;
import org.opensaml.xml.signature.SignatureValidator;
import org.opensaml.xml.validation.ValidationException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.opwvmsg.mxos.error.SamlError;
import com.opwvmsg.mxos.saml.exception.AuthenticationException;
import com.opwvmsg.mxos.saml.exception.SAMLException;
import com.opwvmsg.mxos.saml.service.SamlProperty;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLConstants;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLReader;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLUtils;

/**
 * Service Provider component that validates responses returned
 * from the identity provider. Please note that namespaces are
 * inherited from parent elements in the XML document.
 * 
 * @author mxos-dev
 *
 */
public class VmAssertionConsumerService {

    public final static String SAML_ISSUER = System
            .getProperty(SamlProperty.samlAssertionIssuer.name());
    private static DSAPublicKey publicKey;

    private org.jdom.Namespace nsAssertion = org.jdom.Namespace
            .getNamespace("urn:oasis:names:tc:SAML:2.0:assertion");
    private org.jdom.Namespace nsProtocol = org.jdom.Namespace
            .getNamespace("urn:oasis:names:tc:SAML:2.0:protocol");
    private org.jdom.Namespace nsSignature = org.jdom.Namespace
            .getNamespace("http://www.w3.org/2000/09/xmldsig#");
    private org.jdom.Namespace nsStatusSuccess = org.jdom.Namespace
            .getNamespace("urn:oasis:names:tc:SAML:2.0:status:Success");

    private static Logger logger = LoggerFactory
            .getLogger(VmAssertionConsumerService.class);

    // this property is required by the MemberVerifier
    static {
        System.setProperty("virgin.cookieDomain",
                VmSAMLConstants.VIRGINMEDIA_DOMAIN);
    }

    /**
     * Constructor.
     * 
     * @param publicKeyPath
     * @param privateKeyPath
     */
    public VmAssertionConsumerService(String publicKeyPath)
            throws SAMLException {
        if (publicKey == null) {
            publicKey = (DSAPublicKey) VmSAMLUtils.getPublicKey(publicKeyPath,
                    "DSA");
        }
    }

    /**
     * Consumes a SAML response from an Identity Provider. Interrogates the
     * response to determine validity.
     * 
     * @param response
     * @throws SAMLException
     */
    public VmAssertionSummary consumeSAMLResponse(String samlAssertion)
            throws SAMLException {

        String emailAddress = "";
        VmAssertionSummary result = new VmAssertionSummary();

        Response samlResponse = VmSAMLReader.getResponse(samlAssertion);
        
        try {
            validateStatus(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(ex.getMessage());
        }
        try {
            validateDigitalSignature(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(ex.getMessage());
        }
        try {
            validateAssertions(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(ex.getMessage());
        }

        List<Assertion> assertions = samlResponse.getAssertions();
        Assertion assertion = assertions.get(0);
        emailAddress = assertion.getSubject().getNameID().getValue().trim();
        String username = emailAddress.split("@")[0];
        String domain = emailAddress.split("@")[1];

        logger.debug("User name from assertion: {}", username);
        result.setUsername(username);
        result.setDomain(domain);
        result.setAttributeMap(buildAttributeMap(assertion));
        return result;
    }
    
    /**
     * Consumes a SAML response from an Identity Provider. Interrogates the
     * response to determine validity.
     * 
     * @param response
     * @throws AuthenticationException
     */
    public VmAssertionSummary consumeSAMLResponse(
            HttpServletRequest httpRequest, HttpServletResponse httpResponse)
            throws SAMLException {

        String emailAddress = "";
        String urlDecodedRelayState = "";
        VmAssertionSummary result = new VmAssertionSummary();
        String xmlString = httpRequest.getParameter("SAMLResponse");
        String relayState = httpRequest.getParameter("RelayState");

        try {
            urlDecodedRelayState = URLDecoder.decode(relayState, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException("Unable to URL decode the relay state");
        }

        Response samlResponse = VmSAMLReader.getResponse(xmlString);
        logger.debug("Beginning validation of the SAML response");

        try {
            validateStatus(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(
                    SamlError.SAML_ASSERTION_AUTHENTICATION_FAILURE.name(),
                    ex.getMessage());
        }
        try {
            validateDigitalSignature(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(
                    SamlError.SAML_ASSERTION_INVALID_DIGITAL_SIGNATURE.name(),
                    ex.getMessage());
        }
        try {
            validateAssertions(samlResponse);
        } catch (ValidationException ex) {
            logger.error("The SAML response failed validation");
            throw new SAMLException(ex.getMessage());
        }

        List<Assertion> assertions = samlResponse.getAssertions();

        // we've already validated the assertions so this should not error
        Assertion assertion = assertions.get(0);
        emailAddress = assertion.getSubject().getNameID().getValue().trim();
        String username = emailAddress.split("@")[0];
        String domain = emailAddress.split("@")[1];

        logger.debug("User name from assertion: {}", username);

        // passed validation, so create the cookies
        login(username, httpRequest, httpResponse);

        // return the authentication information to the service provider
        result.setUsername(username);
        result.setDomain(domain);
        result.setRelayState(urlDecodedRelayState);
        result.setAttributeMap(buildAttributeMap(assertion));

        logger.debug("The SAML response passed validation");
        return result;
    }

    private Map<String, String> buildAttributeMap(Assertion assertion) {

        logger.debug("about to build attribute map from assertion");

        Map<String, String> attributeMap = new HashMap<String, String>();

        if (assertion == null)
            return null;

        // do logging enabled check outside loop for performance
        boolean doLogging = logger.isDebugEnabled();
        for (AttributeStatement attributeStatement : assertion
                .getAttributeStatements()) {

            List<Attribute> attributes = attributeStatement.getAttributes();
            for (Attribute attribute : attributes) {

                String name = attribute.getName();

                String attributeValue = obtainAttributeValue(name,
                        attribute.getAttributeValues());
                if (doLogging) {
                    logger.debug("found attribute name:{}, value:{}", name,
                            attributeValue);
                }
                if (attributeValue != null) {
                    attributeMap.put(name, attributeValue);
                } else {
                    attributeMap.put(name, "");
                }
            }
        }
        return attributeMap;
    }

    private String obtainAttributeValue(String name,
            List<XMLObject> attributeValues) {

        if (attributeValues == null)
            return null;

        // do logging enabled check outside loop for performance
        boolean doLogging = logger.isDebugEnabled();
        for (XMLObject value : attributeValues) {
            NodeList childNodes = value.getDOM().getChildNodes();

            if (childNodes == null || childNodes.getLength() == 0)
                continue;

            for (int i = 0; i < childNodes.getLength(); i++) {
                Node node = childNodes.item(i);

                if (node == null)
                    continue;

                if (doLogging) {
                    logger.debug("found attribute value:{}",
                            node.getNodeValue());
                }
                return node.getNodeValue();
            }
        }

        return null;
    }

    /**
     * Validates the digital signature from the SAML response. We must ensure
     * that the assertion has not been engineered by a malicious third party.
     * 
     * @param signature
     * @throws ValidationException
     */
    private void validateDigitalSignature(Response response)
            throws ValidationException {

        SAMLSignatureProfileValidator profileValidator = new SAMLSignatureProfileValidator();

        try {
            profileValidator.validate(response.getSignature());

            Credential verificationCredential = getVerificationCredential();
            SignatureValidator sigValidator = new SignatureValidator(
                    verificationCredential);

            sigValidator.validate(response.getSignature());

        } catch (ValidationException ex) {
            logger.error(
                    "Unable to validate the digital signature or its profile: {}",
                    ex.getMessage());
            throw ex;
        }
    }

    /**
     * Validates the assertions from the SAML Response.
     * 
     * @param samlResponse
     * @throws ValidationException
     */
    private void validateAssertions(Response samlResponse)
            throws ValidationException, SAMLException {

        List<Assertion> assertions = samlResponse.getAssertions();

        if (assertions == null) {
            throw new SAMLException(SamlError.SAML_ASSERTIONS_NOT_FOUND.name(),
                    "The SAML response contains no assertions");
        } else {
            // there should only be 1 assertion
            Assertion assertion = (Assertion) assertions.get(0);
            if (assertion == null) {
                throw new SAMLException(SamlError.SAML_ASSERTIONS_NOT_FOUND.name(),
                        "The SAML response contains no assertions");
            }
            DateTime now = new DateTime();
            DateTime notBefore = assertion.getConditions().getNotBefore();
            DateTime notAfter = assertion.getConditions().getNotOnOrAfter();
            if (logger.isDebugEnabled()) {
                logger.debug("Current time = {}", now);
                logger.debug("Not before = {}", notBefore);
                logger.debug("Not after = {}", notAfter);
            }

            if (notBefore.isAfterNow()) {
                throw new SAMLException(
                        SamlError.SAML_ASSERTION_NOT_YET_VALID.name(),
                        "The SAML assertion is not yet valid");
            }
            if (notAfter.isBeforeNow()) {
                throw new SAMLException(
                        SamlError.SAML_ASSERTION_EXPIRED.name(),
                        "The SAML assertion has expired");
            }

            Issuer issuer = assertion.getIssuer();

            if (logger.isDebugEnabled()) {
                logger.debug("Issuer from assertion: {}", issuer.getValue());
                logger.debug("Issuer from ACS: {}", SAML_ISSUER);
            }

            if (!issuer.getValue().equalsIgnoreCase(SAML_ISSUER)) {
                throw new SAMLException(
                        SamlError.SAML_ASSERTION_INVALID_ISSUER.name(),
                        "The Virgin Media assertion consumer service cannot validate SAML issued by another authority");
            }
            // TODO: some more validation (but what?)
        }
    }

    /**
     * Validates the Status from the SAML response.
     * 
     * @param samlResponse
     * @throws ValidationException
     */
    private void validateStatus(Response samlResponse)
            throws ValidationException {

        Status status = samlResponse.getStatus();

        if (status == null)
            throw new ValidationException(
                    "The SAML response contains no status");

        if (!status.getStatusCode().getValue()
                .equalsIgnoreCase(nsStatusSuccess.getURI())) {
            throw new ValidationException(
                    "SAML authentication failed: assertion did not contain the status code for success");
        }
    }
	
    /* *//**
     * Do not use this method. It is a legacy method that parses the SAML
     * response using JDOM (a low level XML parsing API). Use the OpenSAML API
     * instead.
     * 
     * @param response
     * @deprecated
     * *//*
    private void processSAML(String response) throws SAMLException {

        org.jdom.Document doc = SAMLUtils.createJdomDoc(response);

        org.jdom.Element signature = doc.getRootElement().getChild("Signature",
                nsSignature);
        org.jdom.Element assertion = doc.getRootElement().getChild("Assertion",
                nsAssertion);
        org.jdom.Element status = doc.getRootElement().getChild("Status",
                nsProtocol);

        // validate existence of at least one assertion
        if (assertion == null) {
            throw new SAMLException(
                    "SAML Authentication Failed: No assertions returned by the Identity Provider");
        }

        org.jdom.Element issuer = assertion.getChild("Issuer", nsAssertion);

        // validate issuer
        if (!issuer.getTextTrim().equalsIgnoreCase(SAML_ISSUER)) {
            throw new SAMLException(
                    "SAML Authentication Failed: Unexpected SAML issuer (this issuer: "
                            + issuer.getTextTrim() + ", expected issuer: "
                            + SAML_ISSUER + ")");
        }

        org.jdom.Element statusCode = status.getChild("StatusCode", nsProtocol);

        // validate status
        if (!statusCode.getAttributeValue("Value").equalsIgnoreCase(
                nsStatusSuccess.getURI())) {
            throw new SAMLException(
                    "SAML Authentication Failed: Assertion did not return the status code for success");
        }

        org.jdom.Element conditions = assertion.getChild("Conditions",
                nsAssertion);
        String notBeforeString = conditions.getAttributeValue("NotBefore");
        String notOnOrAfterString = conditions
                .getAttributeValue("NotOnOrAfter");
        Date now = new Date();
        Date notBefore = VmSAMLUtils.toSAMLDate(notBeforeString);
        Date notOnOrAfter = VmSAMLUtils.toSAMLDate(notOnOrAfterString);

        logger.debug("Current time = {}", now);

        // validate timestamps
        if (now.before(notBefore)) {
            throw new SAMLException(
                    "SAML Authentication Failed: Assertion conditions breached (assertion not yet valid)");
        }
        if (now.after(notOnOrAfter)) {
            throw new SAMLException(
                    "SAML Authentication Failed: Assertion conditions breached (assertion expired)");
        }

    }*/

    /**
     * Builds a credential from the public and private key files.
     * 
     * @return
     * @throws SAMLException
     */
    private Credential getVerificationCredential() {

        logger.debug("Building verification credential from Virgin Media public key");

        BasicCredential credential = new BasicCredential();
        credential.setPublicKey(publicKey);

        return credential;
    }

    /**
     * Create the cookies so that the servlet filter (or other SSO mechanism)
     * will permit access to the requested resource.
     * 
     * @param username
     * @param httpRequest
     * @param httpResponse
     */
    private void login(String username, HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) {

        // MCS - 28/11/08
        // REMOVED MEMBER VERIFIER KEEP ALIVE - causing session timeouts in MV
        // cookie
        // SEE MERCURY DEFECT 359
        /*
         * // get new or existing MemberVerifier MemberVerifier memberVerifier =
         * MemberVerifier.getMemberVerifier(httpRequest, httpResponse);
         * memberVerifier.fetchValues();
         * Utils.debugMemberVerifier(memberVerifier); // refresh the cookie
         * memberVerifier.keepAlive();
         */

        httpRequest.getSession().setAttribute("isLoggedIn", Boolean.TRUE);

    }

}
