/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;
import java.util.zip.DeflaterOutputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.opensaml.saml2.core.Attribute;

import com.opwvmsg.mxos.saml.exception.SAMLException;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLFactory;
import com.opwvmsg.mxos.saml.service.vm.impl.VmSAMLReader;

/**
 * The SSO client is responsible for sending a SAML formatted 
 * authentication request to the specified identity provider (IDP) using the
 * specified HTTP response. The SSO client contains information about itself
 * that is passed to the IDP as a form of identification.
 *  
 * @author mxos-dev
 *
 */
public class VmSSOClient {
        
    private String endpoint;
    private String providerName;
    private String acsUrl;
    private String samlIssuer;
    
    public static final String METHOD_GET = "get";
    public static final String METHOD_POST = "post";
    
    private static Logger logger = LoggerFactory.getLogger(VmSSOClient.class);
       
    /**
     * Constructor.
     * 
     * @param endpoint - of the identity provider
     * @param providerName - of the service provider
     * @param acsUrl - of the service provider
     * @param samlIssuer - of the service provider
     */
    public VmSSOClient(
    		String endpoint, 
    		String providerName,
    		String acsUrl,
    		String samlIssuer) {
    	
    	this.endpoint = endpoint;
    	this.acsUrl = acsUrl;
    	this.providerName = providerName;
    	this.samlIssuer = samlIssuer;    	
    }
    
    /**
     * Constructor.
     * 
     * Use this method if there will be no assertion to consume. 
     * 
     * @param endpoint - of the identity provider
     * @param providerName - of the service provider
     * @param samlIssuer - of the service provider
     */
    public VmSSOClient(
    		String endpoint, 
    		String providerName,    		
    		String samlIssuer) {
    	
    	this.endpoint = endpoint;
    	this.providerName = providerName;
    	this.samlIssuer = samlIssuer;    	
    }
    
    /**
     * Sends an authentication request to the endpoint defined when the SSO client
     * was created. The target URL is the resource that the user originally requested
     * (although he will be redirected now to the IDP before he is permitted to access
     * the resource). 
     * 
     * @param targetUrl
     * @param response
     */    
	public void sendAuthenticationRequest(String relayState, HttpServletResponse response) {
	
		sendAuthenticationRequest(relayState, response, null);		
	}
	
	/**
	 * Sends an authentication request to the endpoint defined when the SSO client
     * was created. The relayState URL is the resource that the user originally requested
     * (although he will be redirected now to the IDP before he is permitted to access
     * the resource). The subject is the email address that needs to be authenticated.
     * 
	 * @param relayState
	 * @param response
	 * @param subject
	 */
	public void sendAuthenticationRequest(String relayState, HttpServletResponse response, String subject) {		
		
		logger.debug("RelayState : {}", relayState);
		String authnRequest = "";
		String urlEncodedRelayState = "";
		    
		try {
			urlEncodedRelayState = URLEncoder.encode(relayState, "UTF-8");
			
		} catch(UnsupportedEncodingException ex) {
			throw new RuntimeException("Unable to URL encode the relay state");
		}
		
		try {
			
			logger.debug("SERVICE PROVIDER: BUILDING SAML AUTHENTICATION REQUEST USING OPENSAML TOOLKIT");
						
			authnRequest = VmSAMLFactory.createAuthenticationRequest(providerName, acsUrl, samlIssuer, subject);
									
			if (logger.isDebugEnabled()) {
			    logger.debug("SAML authentication request:");
			    logger.debug(authnRequest);
			}
		
		} catch(SAMLException ex) {
			
			logger.error("Unable to build SAML authentication request - problems with OpenSAML");
			throw new RuntimeException(ex);			
		} catch(Exception ex) {
			logger.error("Unable to build SAML authentication request due to: {}", ex.getMessage());
			throw new RuntimeException(ex);		
		}
		
		String encodedMessage = getEncodedMessage(authnRequest);
		
		redirect(urlEncodedRelayState, encodedMessage, response);
		
	}
		
	/**
	 * Builds a SAML attribute query and sends it to the identity provider.
	 * 
	 * @param username
	 * @param requestedAttributes
	 * @param response
	 * @param method
	 * @return
	 */
	public List<Attribute> sendAttributeQuery(String username, 
			List<Attribute> requestedAttributes, HttpServletResponse response, String method) {
		
		List<Attribute> returnedAttributes;
		
		try {
			logger.debug("SERVICE PROVIDER: BUILDING SAML ATTRIBUTE QUERY");
						
			String attrQuery = VmSAMLFactory.createAttributeQuery(username, requestedAttributes, samlIssuer);
						
			if (logger.isDebugEnabled()) {
			    logger.debug("SAML attribute query:");
			    logger.debug(attrQuery);
			}
		    
		    String urlEncodedMessage = getEncodedMessage(attrQuery);
			
			StringBuffer sb = new StringBuffer();
			sb.append(endpoint);
			sb.append("?SAMLRequest=");
			sb.append(urlEncodedMessage);
			
			if (logger.isDebugEnabled()) {
			    logger.debug("Attribute query URL:");
                logger.debug(sb.toString());
            }
            String samlResponse = sendSAMLRequestGET(sb.toString());
			returnedAttributes = VmSAMLReader.toAttributeList(samlResponse); 
					
		} catch(SAMLException ex) {
			
			logger.error("Unable to build SAML attribute query - problems with OpenSAML");
			throw new RuntimeException(ex);			
		}		
		
		return returnedAttributes;
		
	}
	
	/**
	 * Encodes and deflates the authentication request.
	 * 
	 * @param authnRequest
	 */
	private String getEncodedMessage(String authnRequest) {
		
		String urlEncodedMessage = "";
		
		try {			
			byte[] xmlBytes = authnRequest.getBytes("UTF-8");
			ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
			DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteOutputStream);
			
			try {
				deflaterOutputStream.write(xmlBytes, 0, xmlBytes.length);
				deflaterOutputStream.close();								
			}  catch(IOException ex) {
				logger.error("Unable to deflate the SAML authentication request");
				throw new RuntimeException(ex);
			}
			
			// Base64 encode the deflated string
			Base64 base64Encoder = new Base64();
			byte[] base64EncodedByteArray = base64Encoder.encode(byteOutputStream.toByteArray());
			String base64EncodedMessage = new String(base64EncodedByteArray);
			
			// URL-encode the deflated, Base64-encoded string
		    urlEncodedMessage = URLEncoder.encode(base64EncodedMessage, "UTF-8");
			
		} catch(UnsupportedEncodingException ex) {
			
			// this should never happen as UTF-8 will always be supported
			logger.error("The URL encoder was unable to handle the UTF-8 encoding format");
			throw new RuntimeException(ex);			
		}
		
		return urlEncodedMessage;
						
	}
	
	/**
	 * Redirects the browser to the identity provider.
	 * 
	 * @param targetUrl
	 * @param urlEncodedMessage
	 * @param response
	 */	
	private void redirect(String targetUrl, String urlEncodedMessage, HttpServletResponse response) {
		
		String relayState = targetUrl;
		
		StringBuffer sb = new StringBuffer();
		sb.append(endpoint);
		sb.append("?SAMLRequest=" + urlEncodedMessage);
		
		// only handle the relay state if a target resource is required
		if(relayState != null) {
			sb.append("&RelayState=" + relayState);			
		}	
		
		String url = sb.toString();
		
		if (logger.isDebugEnabled()) {
		    logger.debug("SERVICE PROVIDER: REDIRECTING BROWSER TO IDP WITH AUTHENTICATION REQUEST");
		    logger.debug("Authentication request URL:");
		    logger.debug(url);
		}
		
		// Redirect the browser to the identity provider URL
		response.setStatus(HttpServletResponse.SC_SEE_OTHER);						
		response.addHeader("location", url);
	}
	
	/**
	 * This method sends an encoded request direct to the URL provided and returns
	 * the SAML response from the IDP. No re-directing takes place.
	 * 
	 * @param idpUrl
	 * @return
	 */
	private String sendSAMLRequestGET(String idpUrl) {
				
		StringBuffer sb = new StringBuffer();		
		URL url = null; 
		HttpURLConnection httpConnection = null;
		
		try  {
			url = new URL (idpUrl) ;
			
			logger.debug("SERVICE PROVIDER: SENDING ATTRIBUTE QUERY TO IDP (METHOD=GET)");
						
			logger.debug("Opening URL connection");
			httpConnection = (HttpURLConnection)url.openConnection();			
			httpConnection.setRequestMethod("GET");			
			httpConnection.setDoInput(true);			
			httpConnection.connect();
						
            if (logger.isDebugEnabled()) {
                logger.debug("Response Code: {}", Integer.valueOf(httpConnection.getResponseCode()));
                logger.debug("Response Message: {}", httpConnection.getResponseMessage());
                logger.debug("Fetching the HTML response");
            }
            BufferedReader inStream = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
			String inputLine;
            //do logging enabled check outside loop
			boolean doLogging = logger.isDebugEnabled();
			while ((inputLine = inStream.readLine()) != null) {
            	sb.append(inputLine);
            	if (doLogging) {
            	    logger.debug(inputLine);
            	}
            }
            inStream.close();
         	
		} catch(IOException ex) {
			
			logger.error("Unable to read from the HTTP connection to the identity provider");
			throw new RuntimeException(ex);
			
		} finally {
 
    		if(httpConnection != null){
    			httpConnection.disconnect();	
    		}
		}
		
		String samlResponse = getSAMLResponseFromHTML(sb.toString());
				
		return samlResponse;
		
	}
	
	
	/**
	 * Parses the HTML and fetches the SAML.
	 * 
	 * @param htmlContent
	 * @return
	 */
	private String getSAMLResponseFromHTML(String htmlContent) {
		
		int beginIndex = htmlContent.indexOf("<samlp:Response");
		int endIndex = htmlContent.indexOf("</samlp:Response>") + 17;		
		String samlResponse = htmlContent.substring(beginIndex, endIndex);
		
		if (logger.isDebugEnabled()) {
		    logger.debug("SAML response from HTML:");
		    logger.debug(samlResponse);
		}
		
		return samlResponse;
		
	}
	
	/**
	 * This method URL encodes(using the UTF-8 encoding scheme) the relayState 
     * only to ensure that the URL can be encoded otherwise a RuntimeException is thrown.
     * 
     * The SAMLRequest is created and returned. 
     * 
	 * @param relayState the resource that the user originally requested.
	 * @param response servlet response.
	 * @param subject the email address that needs to be authenticated.
	 * 
	 * @return SAMLRequest which is deflated, base64 encoded and URL encoded. 
	 */
	public String createSAMLRequest(String relayState,
			HttpServletResponse response, String subject) {
		String authnRequest = "";

		
		try {
			URLEncoder.encode(relayState, "UTF-8");
			
		} catch(UnsupportedEncodingException ex) {
			throw new RuntimeException("Unable to URL encode the relay state");
		}
		
		try {
			
			logger.debug("SERVICE PROVIDER: BUILDING SAML AUTHENTICATION REQUEST USING OPENSAML TOOLKIT");
						
			authnRequest = VmSAMLFactory.createAuthenticationRequest(providerName, acsUrl, samlIssuer, subject);
									
			if (logger.isDebugEnabled()) {
			    logger.debug("SAML authentication request:");
			    logger.debug(authnRequest);
			}
		
		} catch(SAMLException ex) {
			
			logger.error("Unable to build SAML authentication request - problems with OpenSAML");
			throw new RuntimeException(ex);			
		} catch(Exception ex) {
			logger.error("Unable to build SAML authentication request due to: '" + ex);
			throw new RuntimeException(ex);		
		}
		
		String encodedMessage = getEncodedMessage(authnRequest);
		return encodedMessage;
	}		

}
