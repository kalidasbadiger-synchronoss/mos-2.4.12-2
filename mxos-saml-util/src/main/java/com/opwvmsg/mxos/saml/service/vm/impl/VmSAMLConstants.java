/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl;

import com.opwvmsg.mxos.saml.service.SamlProperty;

/**
 * Constants used by SSO.
 * 
 * @author mxos-dev
 * 
 */
public interface VmSAMLConstants {

    public static final String SAML_REQUEST = "SAMLRequest";

    public static final String EMAIL_PARAMETER = "email";

    public static final String VIRGINMEDIA_DOMAIN = "virginmedia.com";
    public static final String BLUEYONDER_DOMAIN = "blueyonder.co.uk";
    public static final String NTLWORLD_DOMAIN = "ntlworld.com";
    public static final String VIRGINNET_DOMAIN = "virgin.net";

    public static final String XML_NAMESPACE = "samlp";

    public static final String RIPA_ATTRIBUTE_NAME = "vmxinf";

    public static final String COOKIE_TIMEOUT_IN_SECOUNDS = "cookie.timeout.in.secounds";

    public static final String SAML_ISSUER_IDP = System
            .getProperty(SamlProperty.samlAssertionIssuer.name());

    public interface AttributeNames {

        public static final String ASSERTION_CONSUMER_SERVICE_URL = "AssertionConsumerServiceURL";

    }

    public interface Namespaces {

        public static final String SAML2_PROTOCOL = "urn:oasis:names:tc:SAML:2.0:protocol";
        public static final String REDIRECT_PROTOCOL_BINDING = "urn:oasis:names.tc:SAML:2.0:bindings:HTTP-Redirect";

    }

    public interface DomainFilters {

        public static final String ACS_URL_PARSER = "AssertionConsumerServiceEndpointParser";
    }

    public interface ClientTypes {

        public static final Integer PC_BROWSER = Integer.valueOf(1);
        public static final Integer MICRO_BROWSER = Integer.valueOf(2);
    }

}
