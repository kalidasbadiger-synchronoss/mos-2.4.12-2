/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service.vm.impl;

import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.codec.binary.Base64;
import org.opensaml.Configuration;
import org.opensaml.saml2.core.Attribute;
import org.opensaml.saml2.core.AttributeQuery;
import org.opensaml.saml2.core.AttributeStatement;
import org.opensaml.saml2.core.AuthnRequest;
import org.opensaml.saml2.core.Response;
import org.opensaml.xml.io.Unmarshaller;
import org.opensaml.xml.io.UnmarshallerFactory;
import org.opensaml.xml.io.UnmarshallingException;
import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.XMLParserException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.opwvmsg.mxos.saml.exception.SAMLException;
import com.opwvmsg.mxos.saml.service.SamlProperty;

/**
 * Reads XML into memory using the OpenSAML API.
 * 
 * @author masmith
 *
 */
public class VmSAMLReader extends VmSAMLObject {
	
	private static Logger logger = LoggerFactory.getLogger(VmSAMLReader.class);

    /**
     * Reads an XML document into memory from a String.Additionaly this method
     * checks the XML string is Base64 encoded ,validates true then Decode the
     * String before parsing.
     * 
     * @param xml
     *            to parse
     * @return parsed document.
     * @throws SAMLException
     *             raised by this method.
     */
    public static Document getDocument(String xml) throws SAMLException {
	Document doc = null;
	logger.trace("Parsing the XML string");
	try {
	    BasicParserPool ppMgr = new BasicParserPool();
	    ppMgr.setNamespaceAware(true);
            String encryption = System
                    .getProperty(SamlProperty.samlAssertionBase64Encoded.name());
            if (Boolean.parseBoolean(encryption)) {
                xml = validateBase64(xml);
            }
	    Reader reader = new StringReader(xml);
	    doc = ppMgr.parse(reader);
	} catch (XMLParserException ex) {
	    logger.error("Unable to parse the XML: ");
	    logger.error(xml);
	    throw new SAMLException(ex);
	}
	return doc;
    }
	
	/**
	 * Convenience method for parsing a SAML response string.
	 * 
	 * @return
	 */
	public static Response getResponse(String xmlString)  throws SAMLException {
				
		Document doc = getDocument(xmlString);
		
		return getResponse(doc);
	}
	
	/**
	 * Parses a SAML response document.
	 * 
	 * @param doc
	 * @return
	 * @throws SAMLException
	 */
	public static Response getResponse(Document doc) throws SAMLException {
		
		Response response = null;
		Element rootElement = doc.getDocumentElement();
		
		try {			 				
			UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
			Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(rootElement);
			response = (Response)unmarshaller.unmarshall(rootElement);
						
		} catch(UnmarshallingException ex) {
			
			logger.error("Unable to load the SAML XML response into memory");
			throw new SAMLException(ex);
		}
		
		return response;
	}
	
	/**
	 * Parses a SAML authentication request string.
	 * 
	 * @param xmlRequest
	 * @return
	 */
	public static AuthnRequest getAuthnRequest(String xmlString) throws SAMLException {
		
		Document doc = getDocument(xmlString);
		
		return getAuthnRequest(doc);
	}
	
	/**
	 * Parses a SAML authentication request document.
	 * 
	 * @param doc
	 * @return
	 * @throws SAMLException
	 */
	public static AuthnRequest getAuthnRequest(Document doc) throws SAMLException {
		
		AuthnRequest authnRequest = null;		
		Element rootElement = doc.getDocumentElement();
		
		try {				
			UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
			Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(rootElement);
			authnRequest = (AuthnRequest)unmarshaller.unmarshall(rootElement);
						
		} catch(UnmarshallingException ex) {
			
			logger.error("Unable to load the SAML XML authentication request into memory");
			throw new SAMLException(ex);
		}
		
		return authnRequest;
		
	}
	
	/**
	 * Parses a SAML attribute query document.
	 * 
	 * @param doc
	 * @return
	 */
	public static AttributeQuery getAttributeQuery(Document doc) throws SAMLException {
		
		AttributeQuery attrQuery = null;
		Element rootElement = doc.getDocumentElement();		
		
		try {					
			UnmarshallerFactory unmarshallerFactory = Configuration.getUnmarshallerFactory();
			Unmarshaller unmarshaller = unmarshallerFactory.getUnmarshaller(rootElement);
			attrQuery = (AttributeQuery)unmarshaller.unmarshall(rootElement);
						
		} catch(UnmarshallingException ex) {
			
			logger.error("Unable to load the SAML attribute query into memory");
			throw new SAMLException(ex);
		}
		
		return attrQuery;	
		
	}
	
	
	/**
	 * Parses the attributes from a SAML response string.
	 * 
	 * @param samlResponse
	 * @return
	 */
	public static List<Attribute> toAttributeList(String samlResponse) throws SAMLException {
		
		Response response = getResponse(samlResponse);
		
		AttributeStatement statement = response.getAssertions().get(0).getAttributeStatements().get(0);
		
		return statement.getAttributes();
	}
	
    /**
     * Checks the string is Base64 encoded if true then return the decoded
     * String,otherwise return the same String.
     * 
     * @param strXML
     *            String to validate is it base64 encoded.
     * @return decoded string if it is encoded or return the same string
     * @throws SAMLException
     *             raised by this method.
     */
    public static String validateBase64(String strXML) throws SAMLException {
	logger.debug("Inside method : {}, XML String to Validate : {}",
		"validateBase64", strXML);
	final boolean isEncoded = Base64.isArrayByteBase64(strXML.getBytes());
	logger.debug("Encoded String ? : {}", isEncoded);
	if (isEncoded) {
	    try {
		final Base64 base64 = new Base64();
		final byte[] base64Decoded = base64.decode(strXML.getBytes());
		strXML = new String(base64Decoded, "UTF-8");
	    } catch (UnsupportedEncodingException uEE) {
		logger.error("Unable to Decode the String: {}",
			uEE.getMessage());
		throw new SAMLException(uEE.toString());
	    }
	}
	return strXML;
    }

}
