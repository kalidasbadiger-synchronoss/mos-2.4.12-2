/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.exception;

/**
*
* @author mxos-dev
*/
public class UnknownEmailDomainException extends AuthenticationException {

    /**
     *
     */
    private static final long serialVersionUID = 2926039668692808336L;

    /**
     * Constructor.
     * 
     * @param emailDomain
     */
    public UnknownEmailDomainException(String emailDomain) {
        super("The " + emailDomain + " email domain is not recognised");
    }

}
