/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.saml.service;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.saml.service.vm.impl.serviceprovider.VmSAMLProvider;

/**
 * Provider class register and retrieve SAML provider based on configuration.
 *
 * @author mxos-dev
 * 
 */
public class SAMLConfig {

    private static Logger logger = Logger.getLogger(SAMLConfig.class);

    private static final Map<String, SAMLProvider> providers
        = new ConcurrentHashMap<String, SAMLProvider>();
    public static String SAML_CONFIG_FILE_PREFIX = "/config/saml/saml-";
    public static String CONFIG_FILE_SUFFIX = ".properties";
    
    private static SAMLConfig samlServices = null;

    /**
     * Defaul construtor.
     *
     * @throws MxOSException
     */
    private SAMLConfig() throws MxOSException {
        // Read the source provider loaded from configuration file
        String provider = System.getProperty(SystemProperty.samlProvider.name());
        SamlProvidersEnum samlProvider = SamlProvidersEnum.valueOf(provider);
        switch (samlProvider) {
        case VM:
            registerProvider(samlProvider.name(), new VmSAMLProvider());
            loadSAMLProperties(samlProvider.name());
            logger.info("SAML properties loaded for provider "
                    + samlProvider.name());
            break;
        default:
            throw new IllegalArgumentException(
                    "No provider registered ");
        }
    }

    /**
     * Method to register SAML provider.
     * @param name provider name
     * @param p provider instance
     */
    public static void registerProvider(String name, SAMLProvider p) {
        providers.put(name, p);
    }

    /**
     * Method to get SAML provider.
     * 
     * @param name provider name
     * @return
     * @throws MxOSException in case any exceptions
     */
    public static SAMLService getInstance(String name) throws MxOSException {
        if (samlServices == null) {
            samlServices = new SAMLConfig();
        }
        SAMLProvider p = providers.get(name);
        if (p == null) {
            logger.error("No SAML provider registered with name: " + name);
            throw new IllegalArgumentException(
                    "No provider registered with name: " + name);
        }
        return p.createSamlService();
    }

    /**
     * Method to load saml.properties.
     * <i>${MXOS_HOME}/config/saml/saml.properties</i>.
     * 
     * @throws MxOSException MxOSException
     */
    public static void loadSAMLProperties(String providerName) throws MxOSException {

        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }

        Properties samlProps = null;
        InputStream in = null;
        try {
            String providerFile = SAML_CONFIG_FILE_PREFIX
                    + providerName
                    + CONFIG_FILE_SUFFIX;
            in = new FileInputStream(home + providerFile);
            samlProps = new Properties();
            try {
                samlProps.load(in);
            } catch (IOException e) {
                logger.error("Unable to load SAML configurations", e);
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        } catch (FileNotFoundException e1) {
            logger.error("SAML configurations file not found", e1);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e1);
        } catch (Exception e) {
            logger.error("Error while loading SAML configurations", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        try {
            Iterator<Object> keys = samlProps.keySet().iterator();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                System.setProperty(key.trim(), samlProps.getProperty(key)
                        .trim());
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

}
