/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailForwardService;

import org.apache.log4j.Logger;

/**
 * MailForwardingController class that controls all the requests for Mailbox
 * forwarding addresses.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/mailReceipt/forwardingAddress")
public class MailForwardingController {
    private static Logger logger = Logger
            .getLogger(MailForwardingController.class);

    /**
     * Add MailForwardingAddress.
     *
     * @param email String
     * @param frdAdrs String
     * @param inputParams MultivaluedMap<String, String>
     * @return ForwardingAddress object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response addMultipleMailForwdingAddress(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailForwardService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Add MailForwardingAddress.
     *
     * @param email String
     * @param frdAdrs String
     * @param inputParams MultivaluedMap<String, String>
     * @return ForwardingAddress object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{forwardingAddress}")
    public Response addMailForwdingAddress(
            @PathParam("email") final String email,
            @PathParam("forwardingAddress") final String frdAdrs,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.forwardingAddress.name(), frdAdrs);
            ((IMailForwardService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailForwardingAddress.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return ForwardingAddress object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailForwdingAddresses(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> forwardingAddresses =
                    ((IMailForwardService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, forwardingAddresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailForwardingAddress.
     *
     * @param email String
     * @param oldFrdAdrs String
     * @param newFrdAdrs String
     * @param inputParams MultivaluedMap<String, String>
     * @return ForwardingAddress object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("{oldForwardingAddress}/{newForwardingAddress}")
    public Response updateMailForwdingAddress(
            @PathParam("email") final String email,
            @PathParam("oldForwardingAddress") final String oldFrdAdrs,
            @PathParam("newForwardingAddress") final String newFrdAdrs,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldForwardingAddress.name(),
                    oldFrdAdrs);
            inputParams.add(MailboxProperty.newForwardingAddress.name(),
                    newFrdAdrs);
            ((IMailForwardService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailForwardingAddress.
     *
     * @param email String
     * @param frdAdrs String
     * @param uriInfo UriInfo
     * @return ForwardingAddress object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/{forwardingAddress}")
    public Response deleteMailForwdingAddress(
            @PathParam("email") final String email,
            @PathParam("forwardingAddress") final String frdAdrs,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(
                    MailboxProperty.forwardingAddress.name(), frdAdrs);
            ((IMailForwardService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of MailForwardingAddresses.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return ForwardingAddress object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public Response listMailForwdingAddresses(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "List ForwardingAddress Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> forwardingAddresses =
                    ((IMailForwardService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailForwardService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, forwardingAddresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
