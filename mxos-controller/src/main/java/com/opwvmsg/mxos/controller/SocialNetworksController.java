/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.DELETE;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworkSiteService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISocialNetworksService;

import org.apache.log4j.Logger;

/**
 * VoiceMailController class that controls all the requests for VoiceMail.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/socialNetworks")
public class SocialNetworksController {
    private static Logger logger = Logger
            .getLogger(SmsServicesController.class);

    /**
     * Retrieve SocialNetworks.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SocialNetworks object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSocialNetworks(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get SocialNetworks Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            ISocialNetworksService socialNetworksService =
                (ISocialNetworksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworksService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final SocialNetworks socialNetworks = socialNetworksService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, socialNetworks);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SocialNetworks.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSocialNetworks(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update SocialNetworks Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ISocialNetworksService socialNetworksService =
                (ISocialNetworksService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworksService.name());
            socialNetworksService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create SocialNetworkSite.
     *
     * @param email String
     * @param socialNetworkSite String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/socialNetworkSites/{socialNetworkSite}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createSocialNetworkSite(
            @PathParam("email") final String email,
            @PathParam("socialNetworkSite") final String socialNetworkSite,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Add SocialNetworks Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.socialNetworkSite.name(),
                    socialNetworkSite);
            ISocialNetworkSiteService socialNetworksService =
                (ISocialNetworkSiteService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            socialNetworksService.create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Create Multiple SocialNetworkSite.
     *
     * @param email String
     * @param socialNetworkSite String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/socialNetworkSites")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleSocialNetworkSite(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Add SocialNetworks Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ISocialNetworkSiteService socialNetworksService = (ISocialNetworkSiteService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            socialNetworksService.create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }


    /**
     * Retrieve SocialNetworkSite.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SocialNetworks object
     */
    @Path("/socialNetworkSites")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSocialNetworkSite(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get SocialNetworks Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            ISocialNetworkSiteService socialNetworksService =
                (ISocialNetworkSiteService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<SocialNetworkSite> socialNetworks = socialNetworksService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, socialNetworks);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update SocialNetworkSite.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @Path("/socialNetworkSites/{socialNetworkSite}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateSocialNetworkSite(
            @PathParam("email") final String email,
            @PathParam("socialNetworkSite") final String socialNetworkSite,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update SocialNetworks Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.socialNetworkSite.name(),
                    socialNetworkSite);
            ISocialNetworkSiteService socialNetworksService =
                (ISocialNetworkSiteService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            socialNetworksService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete SocialNetworkSite.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SocialNetworks object
     */
    @Path("/socialNetworkSites/{socialNetworkSite}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSocialNetworkSite(
            @PathParam("email") final String email,
            @PathParam("socialNetworkSite") final String socialNetworkSite,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete SocialNetworks Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            ISocialNetworkSiteService socialNetworksService =
                (ISocialNetworkSiteService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.SocialNetworkSiteService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters()
                    .add(MailboxProperty.socialNetworkSite.name(),
                            socialNetworkSite);
            socialNetworksService.delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
