/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.ExternalProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.Entity;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IAddressBookService;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;

/**
 * addressBookController class that controls all the requests for AddressBook.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}")
public class AddressBookController {
    private static Logger logger = Logger
            .getLogger(AddressBookController.class);
    /**
     * Login user.
     * 
     * @param userId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - contactId object
     * @throws Exception Exception
     */
    @Path("/login")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response loginUser(
            @PathParam("userId") final String userId,
            final MultivaluedMap<String, String> inputParams) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Login Request Received for userId (").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(ExternalProperty.entity.name(),
                    Entity.ADDRESS_BOOK.toString());
            inputParams.add(ExternalProperty.userId.name(), userId);
            final ExternalSession session = ((IExternalLoginService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.ExternalLoginService.name()))
                    .login(inputParams);
            return ControllerUtils.generateResponseWithCookies(
                    MediaType.APPLICATION_JSON, Response.Status.OK, session,
                    session.getCookieString());
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Logout user.
     * 
     * @param userId String
     * @param uriInfo
     * @return Response
     * @throws Exception Exception
     */
    @Path("/logout")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response logoutUser(
            @PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Logout Request Received for userId (").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(ExternalProperty.entity.name(),
                    Entity.ADDRESS_BOOK.toString());
            uriInfo.getQueryParameters().add(ExternalProperty.userId.name(),
                    userId);
            ((IExternalLoginService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ExternalLoginService.name()))
                    .logout(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Delete AddressBook.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Consumes("application/x-www-form-urlencoded")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response delete(@PathParam("userId") final String userId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete address book Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            ((IAddressBookService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AddressBookService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
