/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessAllowedIpService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailAccessService;

import org.apache.log4j.Logger;

/**
 * MailAccessController class that controls all the requests for MailAccess.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/mailAccess")
public class MailAccessController {
    private static Logger logger = Logger.getLogger(MailAccessController.class);

    /**
     * Retrieve MailAccess.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return MailAccess object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailAccess(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailAccess Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MailAccess mailAccess = ((IMailAccessService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailAccessService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailAccess);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailAccess.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailAccess(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailAccess Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailAccessService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAccessService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailAccess Multiple AllowedIP.
     *
     * @param email String
     * @param allowedIP String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/allowedIPs")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createMultipleAllowedIP(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create allowedIP Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailAccessAllowedIpService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAccessAllowedIpService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create MailAccess AllowedIP.
     *
     * @param email String
     * @param allowedIP String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Path("/allowedIPs/{allowedIP}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createAllowedIP(@PathParam("email") final String email,
            @PathParam("allowedIP") final String allowedIP,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create allowedIP Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.allowedIP.name(), allowedIP);
            ((IMailAccessAllowedIpService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAccessAllowedIpService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailAccess AllowedIP.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return AllowedSender objects
     */
    @GET
    @Path("/allowedIPs")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllowedIPs(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get AllowedIP Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<String> addresses = ((IMailAccessAllowedIpService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailAccessAllowedIpService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, addresses);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailAccess AllowedIP.
     *
     * @param email String
     * @param oldAllowedIP String
     * @param newAllowedIP String
     * @param inputParams MultivaluedMap<String, String>
     */
    @POST
    @Path("/allowedIPs/{oldAllowedIP}/{newAllowedIP}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateAllowedIP(@PathParam("email") final String email,
            @PathParam("oldAllowedIP") final String oldAllowedIP,
            @PathParam("newAllowedIP") final String newAllowedIP,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update AllowedIP Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.oldAllowedIP.name(), oldAllowedIP);
            inputParams.add(MailboxProperty.newAllowedIP.name(), newAllowedIP);
            ((IMailAccessAllowedIpService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAccessAllowedIpService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete MailAccess AllowedIP.
     *
     * @param email String
     * @param allowedIP String
     * @param uriInfo UriInfo
     */
    @DELETE
    @Path("/allowedIPs/{allowedIP}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllowedIP(@PathParam("email") final String email,
            @PathParam("allowedIP") final String allowedIP,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete AllowedIP Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(MailboxProperty.allowedIP.name(),
                    allowedIP);
            ((IMailAccessAllowedIpService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailAccessAllowedIpService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
