/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.controller.cos;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.data.pojos.Filters;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.SenderBlocking;
import com.opwvmsg.mxos.data.pojos.SieveFilters;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptSieveFiltersService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosSenderBlockingService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosMailReceiptCloudmarkFiltersService;

import org.apache.log4j.Logger;

/**
 * MailReceiptController class that controls all the requests for Mailbox
 * MailReceipt.
 *
 * @author mxos-dev
 */
@Path("/cos/v2/{cosId}/mailReceipt")
public class CosMailReceiptController {
    private static Logger logger = Logger
            .getLogger(CosMailReceiptController.class);

    /**
     * Retrieve MailReceipt.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return MailReceipt object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceipt(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final MailReceipt mailReceipt = ((ICosMailReceiptService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosMailReceiptService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailReceipt);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceipt(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosMailReceiptService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosMailReceiptService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt SenderBlocking.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return SenderBlocking object
     */
    @GET
    @Path("/senderBlocking")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptSenderBlocking(
            @PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt SenderBlocking Request Received "
                    + "for cosId(").append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final SenderBlocking senderBlocking = ((ICosSenderBlockingService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosSenderBlockingService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, senderBlocking);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt SenderBlocking.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return MailReceipt SenderBlocking object
     */
    @POST
    @Path("/senderBlocking")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptSenderBlocking(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosSenderBlockingService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosSenderBlockingService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt Filters.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return Filters object
     */
    @GET
    @Path("/filters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptFilters(
            @PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt Filters Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final Filters filters = ((ICosMailReceiptFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                    ServiceEnum.CosMailReceiptFiltersService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, filters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve MailReceipt CloudmarkFilters.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return CloudmarkFilters object
     */
    @GET
    @Path("/filters/cloudmarkFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptCloudmarkFilters(
            @PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt CloudmarkFilters Request Received"
                    + " for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final CloudmarkFilters cloudmarkFilters =
                ((ICosMailReceiptCloudmarkFiltersService) MxOSApp.getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMailReceiptCloudmarkFiltersService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cloudmarkFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt CloudmarkFilters.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return CloudmarkFilters object
     */
    @POST
    @Path("/filters/cloudmarkFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptCloudmarkFilters(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosMailReceiptCloudmarkFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMailReceiptCloudmarkFiltersService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Retrieve MailReceipt SieveFilters.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return SieveFilters object
     */
    @GET
    @Path("/filters/sieveFilters")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailReceiptSieveFilters(
            @PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailReceipt SieveFilters Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final SieveFilters sieveFilters =
                    ((ICosMailReceiptSieveFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMailReceiptSieveFiltersService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, sieveFilters);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailReceipt SieveFilters.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return SieveFilters object
     */
    @POST
    @Path("/filters/sieveFilters")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailReceiptSieveFilters(
            @PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailReceipt Request Received for cosId(").append(
                    cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosMailReceiptSieveFiltersService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.CosMailReceiptSieveFiltersService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
