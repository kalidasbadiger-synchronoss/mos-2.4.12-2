/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.task.pojos.Folder;

/**
 * TasksFolderController class that creating all folders for a tasks.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}/folders")
public class AddressBookFolderController {
    private static Logger logger = Logger
            .getLogger(AddressBookFolderController.class);

    /**
     * Create Tasks folder.
     * 
     * @param userId folderName
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @Path("/{folderName}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createTasksFolder(@PathParam("userId")
    final String userId,@PathParam("module")
    final String module, @PathParam("folderName")
    final String folderName, final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Task Folder Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(TasksProperty.module.name(), "addressBook");
            inputParams.add(TasksProperty.folderName.name(), folderName);
            final long fId = ((IAppsFolderService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, fId);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve folders.
     * 
     * @param folderId String
     * @param uriInfo UriInfo
     * 
     * @return Response - folder object
     */
    @Path("/{folderId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTasksFolder(@PathParam("userId")
    final String userId,@PathParam("module")
    final String module, @PathParam("folderId")
    final String folderId, @Context
    final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Task Folder Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(TasksProperty.module.name(), "addressBook");
            uriInfo.getQueryParameters().add(TasksProperty.folderId.name(),
                    folderId);
            final Folder folder = ((IAppsFolderService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, folder);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Folder.
     * 
     * @param folderId String
     * @param inputParams MultivaluedMap<String, String>
     * 
     * @return Response - Empty object
     */
    @Path("/{folderId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateTasksFolder(@PathParam("userId")
    final String userId,@PathParam("module")
    final String module, @PathParam("folderId")
    final String folderId, final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Task Folder Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(TasksProperty.module.name(), "addressBook");
            inputParams.add(TasksProperty.folderId.name(), folderId);
            ((IAppsFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete folder.
     * 
     * @param folderId String
     * @param uriInfo UriInfo
     * 
     * @return Response - Empty Object
     */
    @Path("/{folderId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTasksFolder(@PathParam("userId")
    final String userId,@PathParam("module")
    final String module, @PathParam("folderId")
    final String folderId, @Context
    final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Task Folder Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(TasksProperty.module.name(), "addressBook");
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.folderId.name(), folderId);
            ((IAppsFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of task folders.
     * 
     * @param uriInfo UriInfo
     * 
     * @return Response - participant object
     */
    @Path("/list")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listTasksFolder(@PathParam("userId")
    final String userId,@PathParam("module")
    final String module, @Context
    final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "List Tasks folders Request Received for userId(").append(
                    userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(TasksProperty.module.name(), "addressBook");
            final List<Folder> list = ((IAppsFolderService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, list);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Delete Task.
     * 
     * @param userId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Consumes("application/x-www-form-urlencoded")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Response deleteAll(@PathParam("userId")
    final String userId, @PathParam("module")
    final String module, @Context
    final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete task Request Received for userId(").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(TasksProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(TasksProperty.module.name(),
                    "addressBook");
            ((IAppsFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.AppsFolderService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
