/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMssLinkInfoService;


/**
 * MailboxController class that controls all the requests for Mailbox.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2")
public class MailboxController {
    private static Logger logger = Logger.getLogger(MailboxController.class);

    /**
     * Authenticate user.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, authentication is "Success" if successful else "Failed"
     *         with proper response code
     */
    @Path("/{email}/authenticate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authenticate(@PathParam("email") final String emailOrUsername,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Authenticate Mailbox Request Received for email(").append(
                            emailOrUsername).append(")");
            logger.info(sb);
        }
        try {//check if email is used for authentication
            if (emailOrUsername.contains(MxOSConstants.AT_THE_RATE))
                inputParams.add(MailboxProperty.email.name(), emailOrUsername);
            else
                inputParams.add(MailboxProperty.userName.name(), emailOrUsername);
            ((ICredentialService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CredentialService.name()))
                    .authenticate(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    /**
     * Authenticate user and return mailbox pojo.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, authentication is "Success" if successful else "Failed"
     *         with proper response code
     */
    @Path("/{email}/authenticateAndGetMailbox")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response authAndGetMailbox(@PathParam("email") final String emailOrUsername,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Authenticate Mailbox Request Received for email(").append(
                            emailOrUsername).append(")");
            logger.info(sb);
        }
        try {//check if email is used for authentication
            if (emailOrUsername.contains(MxOSConstants.AT_THE_RATE))
                inputParams.add(MailboxProperty.email.name(), emailOrUsername);
            else
                inputParams.add(MailboxProperty.userName.name(), emailOrUsername);
            final Mailbox mailbox = ((ICredentialService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CredentialService.name()))
                    .authenticateAndGetMailbox(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailbox);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create Mailbox.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - MailboxId object
     * @throws Exception Exception
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}")
    public Response createMailbox(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) throws Exception {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Mailbox Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            final String mailboxId = ((IMailboxService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MailboxService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailboxId);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Mailbox Info.
     * for single mail box.
     *
     * @param uid can be email or mailboxId or other uid for mailbox
     * @param uriInfo UriInfo
     * @return Response - Mailbox object
     */
    @Path("/{uid}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailboxInfo(@PathParam("uid") final String uid,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Mailbox Request Received for uid(").append(uid)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.uid.name(),
                    uid);
            //GetMSSLinkInfo action need to read the email address name. 
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    uid);
            
            final Mailbox mailbox = ((IMailboxService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MailboxService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailbox);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve Mailbox Info.
     * for multiple mail boxes.
     *
     * @param uriInfo UriInfo
     * @return Response - List of Mailbox object
     */
    @Path("/mailboxes")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailboxesInfo(@Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Mailboxes Request Received(").append(
                    uriInfo.getQueryParameters()).append(")");
            logger.info(sb);
        }
        try {
            final List<Mailbox> mailboxes = ((IMailboxService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailboxesService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailboxes);

        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Mailbox Base.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Mailbox Base object
     */
    @Path("/{email}/base")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailboxBase(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Mailbox Base Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final Base base = ((IMailboxBaseService) MxOSApp.getInstance()
                    .getContext()
                    .getService(ServiceEnum.MailboxBaseService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, base);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Mailbox Base.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @Path("/{email}/base")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailboxBase(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Mailbox Base Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailboxBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailboxBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Mailbox.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/{email}")
    public Response deleteMailbox(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Mailbox Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            ((IMailboxService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailboxService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Delete Mailbox in Mss.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{email}/mss")
    public Response deleteMailboxInMSS(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Mailbox In MSS Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            ((IMailboxService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailboxService.name()))
                    .deleteMSS(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of Mailbox Base objects.
     *
     * @param uriInfo UriInfo
     * @return Response - List of Mailbox Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/base/search")
    public Response listMailbox(@Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer("List Mailbox Request Received(")
                    .append(uriInfo.getQueryParameters()).append(")");
            logger.info(sb);
        }
        try {
            final List<Base> mailboxBaseObjects = ((IMailboxBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailboxBaseService.name()))
                    .search(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailboxBaseObjects);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve MsslinkInfo.
     * for single mail box.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - MssLinkInfo object
     */
    @Path("/{email}/mssLinkInfo")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMssLinkInfo(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get msslinkinfo Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MssLinkInfo linkInfo = ((IMssLinkInfoService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.MssLinkInfoService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, linkInfo);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
