/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.process;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IExternalLdapAttributesService;

@Path("/externalLdap")
public class ExternalLdapController {
    private static Logger logger = Logger.getLogger(ExternalLdapController.class);

    /**
     * Authorize the MSISDN.
     *
     * @param filterValue String
     * @return Response - Ldap Attributes Map
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getAttributes/{filterValue}")
    public Response getAttributes(@PathParam("filterValue") final String filterValue) {

        if(logger.isDebugEnabled())
            logger.debug("ExternalLdapController getAttributes Attribute request received with filterValue:" + filterValue);
        HashMap <String, List<String>>inputParams=new HashMap<String, List<String>>();
        try {
            List <String> filterValueList=new LinkedList<String>();
            filterValueList.add(filterValue);
            inputParams.put("filterValue",filterValueList);
            IExternalLdapAttributesService externalLdapAttributesRetrieveService =
                    (IExternalLdapAttributesService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ExternalLdapAttributesService.name());
            Map<String, List<String>> attributeMap=externalLdapAttributesRetrieveService.readAttributes(inputParams);

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, attributeMap);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
