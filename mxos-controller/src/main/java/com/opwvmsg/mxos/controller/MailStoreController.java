/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailStoreService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IGroupAdminAllocationsService;

/**
 * AliasController class that controls all the requests for Mailbox Alias.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/mailStore")
public class MailStoreController {
    private static Logger logger = Logger.getLogger(MailStoreController.class);

    /**
     * Retrieve MailStore.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return MailStore object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMailStore(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get MailStore Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final MailStore mailStore = ((IMailStoreService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.MailStoreService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, mailStore);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update MailStore.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateMailStore(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update MailStore Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IMailStoreService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.MailStoreService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve External MailStore.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return MailStore object
     */
    @Path("/externalStore")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExternalStore(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get External Store Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final ExternalStore externalStore = ((IExternalStoreService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalStoreService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, externalStore);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update External MailStore.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, change password is success or failure
     */
    @Path("/externalStore")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateExternalStore(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update External Store Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IExternalStoreService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.ExternalStoreService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Group Admin Allocations MailStore.
     * 
     * @param email String
     * @param uriInfo UriInfo
     * @return GroupAdminAllocations object
     */
    @Path("/groupAdminAllocations")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getGroupAdminAllocations(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Group Admin Allocations Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final GroupAdminAllocations groupAdminAllocations = ((IGroupAdminAllocationsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(ServiceEnum.GroupAdminAllocationsService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, groupAdminAllocations);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Group Admin Allocations MailStore.
     * 
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String, update group admin allocations is success or failure
     */
    @Path("/groupAdminAllocations")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateGroupAdminAllocations(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Group Admin Allocations Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            ((IGroupAdminAllocationsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(ServiceEnum.GroupAdminAllocationsService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
