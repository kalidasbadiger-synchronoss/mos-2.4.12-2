/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.message;

import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;

/**
 * FolderController class that controls all the requests for Folders.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/folders")
public class FolderController {
    private static Logger logger = Logger.getLogger(FolderController.class);


    /**
     * Create Folder
     * 
     * @param email: email id
     * @param inputParams
     * @return folderUUID of newly created folder
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{folderName}")
    public Response createFolder(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create Folder Request Received for Email Id(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);
            final String folderId = ((IFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.FolderService.name()))
                    .create(inputParams);
            if(folderId !=null && !folderId.equals(""))
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, UUID.fromString(folderId));
            else
                return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                        Response.Status.OK, null);  
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Folder Information.
     * 
     * @param email String
     * @param folderName String
     * @param uriInfo UriInfo
     * @return Response - Folder object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{folderName}")
    public Response readFolder(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Folder Request Received for email(").append(email)
                    .append(") folder(").append(folderName).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            final Folder folder = ((IFolderService) MxOSApp.getInstance()
                    .getContext().getService(ServiceEnum.FolderService.name()))
                    .read(uriInfo.getQueryParameters());
            Response response = ControllerUtils.generateResponse(MediaType.APPLICATION_JSON, Response.Status.OK, folder);
            if (logger.isDebugEnabled()) {
                logger.debug("done reading");
            }
            return response;
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Folder.
     *
     * @param email String
     * @param folderName String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{folderName}")
    public Response updateFolder(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Folder Request Received for Mailbox(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(FolderProperty.folderName.name(), folderName);
            ((IFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.FolderService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Folder.
     *
     * @param email String
     * @param folderName String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    @Path("/{folderName}")
    public Response deleteFolder(@PathParam("email") final String email,
            @PathParam("folderName") final String folderName,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Folder Request Received for Mailbox(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(FolderProperty.folderName.name(),
                    folderName);
            ((IFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.FolderService.name()))
                    .delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    
    /**
     * Delete All Folder.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON })
    public Response deleteAllFolder(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete All Folder Request Received for Mailbox(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            ((IFolderService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.FolderService.name()))
                    .deleteAll(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
    
    /**
     * Retrieve Folders.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - List of Folder objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/list")
    public Response listFolders(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "List Folder Request Received for Mailbox(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<Folder> folders = ((IFolderService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.FolderService.name()))
                    .list(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, folders);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
