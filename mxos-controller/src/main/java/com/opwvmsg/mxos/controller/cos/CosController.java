/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.cos;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.cos.ICosService;
import com.opwvmsg.mxos.interfaces.service.cos.ICosBaseService;

/**
 * CosController class that controls all the requests for COS main and base
 * objects.
 *
 * @author mxos-dev
 */
@Path("/cos/v2")
public class CosController {
    private static Logger logger = Logger.getLogger(CosController.class);

    /**
     * Create Cos.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{cosId}")
    public Response createCos(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create COS Request Received for cosId(").append(cosId)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosService.name()))
                    .create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve Cos Base.
     *
     * @param cosId String
     * @param uriInfo UriInfo
     * @return Response - Cos Base object
     */
    @Path("/{cosId}/base")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCosBase(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Cos Base Request Received for cosId(").append(cosId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            final Base base = ((ICosBaseService) MxOSApp.getInstance()
                    .getContext().getService(
                    ServiceEnum.CosBaseService.name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, base);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update Cos Base.
     *
     * @param cosId String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @Path("/{cosId}/base")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateCosBase(@PathParam("cosId") final String cosId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update Cos Base Request Received for cosId(")
                    .append(cosId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.cosId.name(), cosId);
            ((ICosBaseService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosBaseService.name()))
                    .update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Cos.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    @Path("/{cosId}")
    public Response deleteCos(@PathParam("cosId") final String cosId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete Cos Request Received for cosId(").append(cosId)
                    .append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(MailboxProperty.cosId.name(),
                    cosId);
            ((ICosService) MxOSApp.getInstance().getContext()
                    .getService(ServiceEnum.CosService.name())).delete(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve List of Cos Base objects.
     *
     * @param uriInfo UriInfo
     * @return Response - List of Cos Base objects
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/base/search")
    public Response listCos(@Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            logger.info(new StringBuffer("List Cos Request Received"));
        }
        try {
            final List<Base> cosBaseObjects = ((ICosBaseService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.CosBaseService.name()))
                    .search(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, cosBaseObjects);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
