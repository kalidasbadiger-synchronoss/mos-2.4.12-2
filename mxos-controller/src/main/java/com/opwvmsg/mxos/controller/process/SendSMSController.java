/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.controller.process;

import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.ISendSMSService;

/**
 * SendSMSController class that sends SMS / mail via the external Interfaces
 * like VAMP/FBI/MMG.
 *
 * @author mxos-dev
 */
@Path("/sendSMS")
public class SendSMSController {
    private static Logger logger = Logger.getLogger(SendSMSController.class);

    /**
     * Sends SMS via VAMP/FBI/MMG.
     *
     * @param smsType String
     * @param fromAddress String
     * @param toAddress String
     * @param inputParams String
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{smsType}/{fromAddress}/{toAddress}")
    public Response sendSMS(@PathParam("smsType") final String smsType,
            @PathParam("fromAddress") final String fromAddress,
            @PathParam("toAddress") final String toAddress,
            final MultivaluedMap<String, String> inputParams) {

        logger.info("Send SMS Request Received. Request Details:");
        logger.info("smsType:" + smsType);
        logger.info("fromAddress:" + fromAddress);
        logger.info("toAddress:" + toAddress);

        Iterator<String> keys = inputParams.keySet().iterator();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            logger.info(key + ":" + inputParams.getFirst(key));
        }

        try {

            inputParams.add(SmsProperty.smsType.name(), smsType);
            inputParams.add(SmsProperty.fromAddress.name(), fromAddress);
            inputParams.add(SmsProperty.toAddress.name(), toAddress);

            ISendSMSService sendSMSService = (ISendSMSService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.SendSmsService.name());

            sendSMSService.process(inputParams);

            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
