/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * ProcessMonitorController class is used to health check of the system.
 * It just sends 200 OK for every request it receives.
 *
 * @author mxos-dev
 */
@Path("/monitor")
public class ProcessMonitorController {
    /**
     * Method to just return 200 OK.
     *
     * @param uriInfo UriInfo
     * @return http status
     */
    @GET
    public Response monitor() {
        ResponseBuilder r = Response.status(Response.Status.OK);
        return r.build();
    }
}
