/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller.addressbook;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.controller.ControllerUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoEventsService;

/**
 * ContactsPersonalInfoEventsController class that controls all the requests for
 * contact's Personal Info event.
 * 
 * @author mxos-dev
 */
@Path("/addressBook/v2/{userId}/contacts/{contactId}/personalInfo/events")
public class ContactsPersonalInfoEventsController {
    private static Logger logger = Logger
            .getLogger(ContactsPersonalInfoEventsController.class);

    /**
     * Create contacts Personal Info Event.
     * 
     * @param userId String
     * @param contactId String
     * @param type String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{type}")
    public Response createContactsPersonalInfoEvent(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @PathParam("type") final String type,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Create contact personal info event Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.contactId.name(), contactId);
            inputParams.add(AddressBookProperty.type.name(), type.toString());

            String eventType = inputParams.get(AddressBookProperty.type.name())
                    .get(0);

            if (null != inputParams.get(AddressBookProperty.date.name())) {
                if (eventType.equalsIgnoreCase(Event.Type.BIRTHDAY.name())) {
                    String date = inputParams.get(
                            AddressBookProperty.date.name()).get(0);
                    inputParams.add(AddressBookProperty.birthDate.name(), date);

                } else if (eventType.equalsIgnoreCase(Event.Type.ANNIVERSARY
                        .name())) {
                    String date = inputParams.get(
                            AddressBookProperty.date.name()).get(0);
                    inputParams.add(AddressBookProperty.anniversaryDate.name(),
                            date);

                } else {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_TYPE
                                    .name());
                }
            } else {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }
            ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name())).create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException ex) {
            if (ex.getCode() == null || ex.getCode().equals("")) {
                ex.setCode(ErrorCode.GEN_INTERNAL_ERROR.name());
            }
            ex.setRequestParams(inputParams.toString());
            ex.setOperationType(ServiceEnum.ContactsPersonalInfoEventsService
                    .name() + ":" + Operation.PUT.name());
            ExceptionUtils.prepareMxOSErrorObject(ex);
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(ex), ex);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve contact PersonalInfo event.
     * 
     * @param userId String
     * @param contactId String
     * @param type String
     * @param uriInfo UriInfo
     * @return Response - contact Personal Info events object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{type}")
    public Response getContactsPersonalInfoEvents(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @PathParam("type") final String type, @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get Contacts Personal Info Events Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            uriInfo.getQueryParameters().add(AddressBookProperty.type.name(),
                    type);

            final Event event = ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name()))
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, event);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve contact PersonalInfo event.
     * 
     * @param userId String
     * @param contactId String
     * @param uriInfo UriInfo
     * @return Response - contact Personal Info events object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllContactsPersonalInfoEvents(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get All Contacts Personal Info Events Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            final List<Event> event = ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name())).readAll(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, event);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update contacts Personal Info event.
     * 
     * @param userId String
     * @param contactId String
     * @param type String
     * @param inputParams MultivaluedMap<String, String>
     * @return Response - Empty object
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    @Path("/{type}")
    public Response updateContactsPersonalInfoEvent(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @PathParam("type") final String type,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update contact personal info event "
                            + "Request Received for userId(").append(userId)
                    .append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(AddressBookProperty.userId.name(), userId);
            inputParams.add(AddressBookProperty.contactId.name(), contactId);
            inputParams.add(AddressBookProperty.type.name(), type.toString());

            String eventType = inputParams.get(AddressBookProperty.type.name())
                    .get(0);
            if (null != inputParams.get(AddressBookProperty.date.name())) {
                if (eventType.equalsIgnoreCase(Event.Type.BIRTHDAY.name())) {
                    String date = inputParams.get(
                            AddressBookProperty.date.name()).get(0);
                    inputParams.add(AddressBookProperty.birthDate.name(), date);

                } else if (eventType.equalsIgnoreCase(Event.Type.ANNIVERSARY
                        .name())) {
                    String date = inputParams.get(
                            AddressBookProperty.date.name()).get(0);
                    inputParams.add(AddressBookProperty.anniversaryDate.name(),
                            date);

                } else {
                    throw new InvalidRequestException(
                            AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_TYPE
                                    .name());
                }
            } else {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }

            ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name())).update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException ex) {
            if (ex.getCode() == null || ex.getCode().equals("")) {
                ex.setCode(ErrorCode.GEN_INTERNAL_ERROR.name());
            }
            ex.setRequestParams(inputParams.toString());
            ex.setOperationType(ServiceEnum.ContactsPersonalInfoEventsService
                    .name() + ":" + Operation.POST.name());
            ExceptionUtils.prepareMxOSErrorObject(ex);

            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(ex), ex);

        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Contacts Personal Info events.
     * 
     * @param userId String
     * @param contactId String
     * @param type String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{type}")
    public Response deleteContactsPersonalInfoEvents(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @PathParam("type") final String type, @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete contact personal info event Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            uriInfo.getQueryParameters().add(AddressBookProperty.type.name(),
                    type);

            String eventType = uriInfo.getQueryParameters()
                    .get(AddressBookProperty.type.name()).get(0);

            if (eventType.equalsIgnoreCase(Event.Type.BIRTHDAY.name())) {
                uriInfo.getQueryParameters().add(
                        AddressBookProperty.birthDate.name(), null);
            } else if (eventType
                    .equalsIgnoreCase(Event.Type.ANNIVERSARY.name())) {
                uriInfo.getQueryParameters().add(
                        AddressBookProperty.anniversaryDate.name(), null);
            } else {
                throw new InvalidRequestException(
                        AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_TYPE
                                .name());
            }
            ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name())).delete(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete Contacts all personal info events.
     * 
     * @param userId String
     * @param contactId String
     * @param uriInfo UriInfo
     * @return Response - Empty object
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAllContactsPersonalInfoEvents(
            @PathParam("userId") final String userId,
            @PathParam("contactId") final String contactId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Delete contacts all the personal info events Request Received for userId(")
                    .append(userId).append(")");
            logger.info(sb);
        }
        try {
            uriInfo.getQueryParameters().add(AddressBookProperty.userId.name(),
                    userId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.contactId.name(), contactId);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.birthDate.name(), null);
            uriInfo.getQueryParameters().add(
                    AddressBookProperty.anniversaryDate.name(), null);

            ((IContactsPersonalInfoEventsService) MxOSApp
                    .getInstance()
                    .getContext()
                    .getService(
                            ServiceEnum.ContactsPersonalInfoEventsService
                                    .name())).deleteAll(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final Exception e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

}
