/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.VoiceMail;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IVoiceMailService;

import org.apache.log4j.Logger;

/**
 * VoiceMailController class that controls all the requests for VoiceMail.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/voiceMail")
public class VoiceMailController {
    private static Logger logger = Logger
            .getLogger(SmsServicesController.class);

    /**
     * Retrieve VoiceMail.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return SmsServices object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVoiceMail(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get VoiceMail Request Received for email(").append(email)
                    .append(")");
            logger.info(sb);
        }
        try {
            IVoiceMailService voiceMailService = (IVoiceMailService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.VoiceMailService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final VoiceMail voiceMail = voiceMailService.read(uriInfo
                    .getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, voiceMail);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update VoiceMail.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateVoiceMail(@PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update VoiceMail Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            inputParams.add(MailboxProperty.email.name(), email);
            IVoiceMailService voiceMailService = (IVoiceMailService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.VoiceMailService.name());
            voiceMailService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
