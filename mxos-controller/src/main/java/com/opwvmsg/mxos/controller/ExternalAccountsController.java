/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Path;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalAccountsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IExternalMailAccountService;

import org.apache.log4j.Logger;

/**
 * ExternalAccountsController class that controls all the requests for
 * ExternalAccounts.
 *
 * @author mxos-dev
 */
@Path("/mailbox/v2/{email}/externalAccounts")
public class ExternalAccountsController {
    private static Logger logger = Logger
            .getLogger(ExternalAccountsController.class);

    /**
     * Retrieve ExternalAccounts.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return ExternalAccounts object
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExternalAccounts(@PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get ExternalAccounts Request Received for email(").append(
                    email).append(")");
            logger.info(sb);
        }
        try {
            IExternalAccountsService externalAccountsService =
                    (IExternalAccountsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalAccountService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final ExternalAccounts externalAccounts = externalAccountsService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, externalAccounts);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update ExternalAccounts.
     *
     * @param email String
     * @param inputParams MultivaluedMap<String, String>
     * @return String,
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateExternalAccounts(
            @PathParam("email") final String email,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update ExternalAccounts Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IExternalAccountsService externalAccountsService =
                    (IExternalAccountsService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalAccountService.name());
            inputParams.add(MailboxProperty.email.name(), email);
            externalAccountsService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Create External Mail Account.
     *
     * @param email String
     * @param accountId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String,
     */
    @Path("/mailAccounts/{accountId}")
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response createExternalMailAccounts(
            @PathParam("email") final String email,
            @PathParam("accountId") final String accountId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "create External Mail Accounts Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IExternalMailAccountService externalAccountsService =
                    (IExternalMailAccountService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalMailAccountService.name());
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.accountId.name(), accountId);
            externalAccountsService.create(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Retrieve External Mail Accounts.
     *
     * @param email String
     * @param uriInfo UriInfo
     * @return ExternalAccounts object
     */
    @Path("/mailAccounts")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getExternalMailAccounts(
            @PathParam("email") final String email,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get External Mail Accounts Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IExternalMailAccountService externalAccountsService =
                    (IExternalMailAccountService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalMailAccountService.name());
            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            final List<MailAccount> externalAccounts = externalAccountsService
                    .read(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, externalAccounts);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Update External Mail Account.
     *
     * @param email String
     * @param accountId String
     * @param inputParams MultivaluedMap<String, String>
     * @return String,
     */
    @Path("/mailAccounts/{accountId}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/x-www-form-urlencoded")
    public Response updateExternalMailAccounts(
            @PathParam("email") final String email,
            @PathParam("accountId") final String accountId,
            final MultivaluedMap<String, String> inputParams) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Update External Mail Accounts Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IExternalMailAccountService externalAccountsService =
                    (IExternalMailAccountService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalMailAccountService.name());
            inputParams.add(MailboxProperty.email.name(), email);
            inputParams.add(MailboxProperty.accountId.name(), accountId);
            externalAccountsService.update(inputParams);
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }

    /**
     * Delete External Mail Accounts.
     *
     * @param email String
     * @param accountId String
     * @param uriInfo UriInfo
     * @return ExternalAccounts object
     */
    @Path("/mailAccounts/{accountId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteExternalMailAccounts(
            @PathParam("email") final String email,
            @PathParam("accountId") final String accountId,
            @Context final UriInfo uriInfo) {
        if (logger.isInfoEnabled()) {
            StringBuffer sb = new StringBuffer(
                    "Get External Mail Accounts Request Received for email(")
                    .append(email).append(")");
            logger.info(sb);
        }
        try {
            IExternalMailAccountService externalAccountsService =
                    (IExternalMailAccountService) MxOSApp
                    .getInstance().getContext()
                    .getService(ServiceEnum.ExternalMailAccountService.name());

            uriInfo.getQueryParameters().add(MailboxProperty.email.name(),
                    email);
            uriInfo.getQueryParameters().add(MailboxProperty.accountId.name(),
                    accountId);
            externalAccountsService.delete(uriInfo.getQueryParameters());
            return ControllerUtils.generateResponse(MediaType.APPLICATION_JSON,
                    Response.Status.OK, null);
        } catch (final MxOSException e) {
            return ControllerUtils.generateErrorResponse(
                    MediaType.APPLICATION_JSON,
                    ControllerUtils.generateHTTPStatus(e), e);
        }
    }
}
