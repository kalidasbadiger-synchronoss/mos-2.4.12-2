/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.notify;

import java.util.List;
import java.util.Map;
import com.opwvmsg.mxos.notify.pojos.Notify;

import com.opwvmsg.mxos.exception.MxOSException;


/**
 * Notify operations interface which will be exposed to the client. This
 * interface is responsible for doing notification service related operations 
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface INotifyService {
    
    /**
     * This operation is not supported. 
     * For creation of Notify-Subscription entry use {@link ISubscriptionsService.create(...)}
     * 
     * @param inputParams Parameters given by user.
     */
    void create(final Map<String, List<String>> inputParams)
    throws MxOSException;

    /**
     * This operation is responsible to read all the subscriptions for the given
     * topic. It uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * 
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     * </pre>
     * 
     * @return returns Notify POJO.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * NTF_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NTF_UNABLE_TO_READ - <i>Unable to perform Notify READ operation.</i>
     * NTF_TOPIC_NOT_FOUND - <i>No subscription mapping found of given topic.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * 
     * </pre>
     * 
     */
    Notify read(final Map<String, List<String>> inputParams)
    throws MxOSException;
    
    /**
     * This operation is not supported 
     * 
     * @param inputParams
     *     Parameters given by user.
     */
    void update(final Map<String, List<String>> inputParams)
    throws MxOSException;

    /**
     * This operation is responsible for deleting all subscriptions for the
     * topic. It uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * 
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     * 
     * NTF_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NTF_TOPIC_NOT_FOUND - <i>No subscription mapping found of given topic.</i>
     * NTF_UNABLE_TO_DELETE - <i>Unable to perform Notify DELETE operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * 
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
    throws MxOSException;

    /**
     * This operation is responsible to get all the subscriptions corresponding
     * to given topic and publish the message received as input to respective
     * callback URL.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * 
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>topic</b>
     *     <i>Topic for which subscriptions are required. This can be mailboxId
     *     and folderId or any other arbitrarily formated String. IMAP creates a topic and subscriptions
     *     associated with it. Topic acts as key for multiple values(subscriptions).</i>
     *     Type: String, Maximum Length: 255
     *     Sample Format:- &lt;folder-id&gt;.&lt;notification-class&gt;.&lt;notification-type&gt; 
     *     Where:- folder-id=folder-UUID, notification-class="mss",notification-type: "message",  
     *     "add", "remove", or "flagschanged".
     *     Example: - imap.any.7001.33852bcf-2987-3644-898a-c9a45510dc96
     * <b>Message</b>
     *     <i>JSON object for message. Should not be null</i>
     *     Example:- 
     *     {
     *        "Type" : "MessageAdded",
     *        "Message-Id" : "37477552-7c63-11e2-b18f-55432c0edab3",
     *        "Imap-UID" : 1002,
     *        "Message-Flags" : "TFFFFFF",
     *        "Keywords" : ["call_received", "migrated"]
     *     }
     *        
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     * 
     * NTF_INVALID_TOPIC - <i>Invalid topic format.</i>
     * NTF_TOPIC_NOT_FOUND - <i>No subscription mapping found for given topic.</i>
     * NTF_INVALID_PUBLISH_MESSAGE - <i>Empty or invalid formated publish message received.</i>
     * NTF_UNABLE_TO_PUBLISH - <i>Unable to perform Notify POST operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * 
     * </pre>
     */
    void publish(final Map<String, List<String>> inputParams)
    throws MxOSException;
}
