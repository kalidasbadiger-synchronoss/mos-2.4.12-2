/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Message operations interface which will be exposed to the client. This
 * interface is responsible for doing message related operations (like Create,
 * Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMessageService {

    /**
     * This operation is responsible for creating message. It uses one
     * or more actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * <b>folderName</b>
     *      <i>Name of the folder in which the message has to be created</i>
     *      Type: String, Maximum Length: 255
     * <b>message</b>
     *      <i>message body</i>
     *      Type: String
     * <b>receivedFrom</b>
     *      <i>Sender email-id</i>
     *      Allowed values: email
     * </pre> 
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *        Type: String; Enum: false, true; EmptyAllowed: no
     * <b>mailboxId</b>
     *      <i>Subscriber mailbox's ID. It's a mandatory parameter if messageStoreHost is present.</i>
     *      Type: Long; EmptyAllowed: no
     * <b>messageStoreHost</b>
     *      <i>Specifies the name of the host or service cluster. It's a mandatory parameter if mailboxId is present</i>
     *      Type: String, Maximum Length: 255
     * <b>realm</b>
     *      <i>Specifies mailbox mail realm of the user. It's a mandatory parameter if mailboxId is present</i>
     *      Type: String, Maximum Length: 255
     * <b>flagSeen</b>
     *     <i>Specifies whether the flag is seen, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagAns</b>
     *     <i>Specifies whether the flag is answer, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagFlagged</b>
     *     <i>Specifies whether the flag is flagged, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDel</b>
     *     <i>Specifies whether the flag is delete, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagRecent</b>
     *     <i>Specifies whether the the flag is recent, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDraft</b>
     *     <i>Specifies whether the the flag is draft, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>isPrivate</b>
     *     <i>Specifies whether this message is private, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>arrivalTime</b>
     *     <i>Epoch time specifying when this message was created</i>
     *     Type: Long; EmptyAllowed: no
     * <b>expireTime</b>
     *     <i>Epoch time specifying when this message expires</i>
     *     Type: Long; EmptyAllowed: no
     * <b>keywords</b>
     *     <i>Keywords</i>
     *     Type: String, Maximum Length: 255; EmptyAllowed: no
     * <b>uid</b>
     *     <i>UID of the message to be created with. Allowed Values: Integer. Min: 0, Max: 2147483647</i>
     *     Type: Integer, Min: 0, Max: 2147483647
     * <b>oldMsgId</b>
     *     <i>Old msg Id. Supported with Mx 9.0 and later.</i>
     *     Type: String, Maximum Length: 257
     * <b>popDeletedFlag</b>
     *     <i>Pop deleted flag, allowed values are true/false. Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Sets the message as popDeleted.
     *     false - Sets the message as normal.
     * <b>msgBase64Encoded</b>
     *     <i>Message is base 64 encoded, allowed values are true/false, if true then message is base 64 encoded</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *        
     * </pre>
     * 
     * @return returns MessageId for newly deposited message.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     *      MSG_INVALID_MESSAGE_FROM - <i>Invalid from value.</i>
     *      MSG_INVALID_MESSAGE_BODY - <i>Invalid message body value.</i>
     *      MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     *      MSG_INVALID_MESSAGE_STORE_HOST - <i>Invalid Message Store Host.</i>
     *      MBX_INVALID_MAIL_REALM - <i>Invalid mail realm.</i>
     *      MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *      MSG_INVALID_FLAG_SEEN - <i>Invalid flagSeen.</i>
     *      MSG_INVALID_FLAG_ANS - <i>Invalid flagAns.</i>
     *      MSG_INVALID_FLAG_FLAGGED - <i>Invalid flagFlagged.</i>
     *      MSG_INVALID_FLAG_DEL - <i>Invalid flagDel.</i>
     *      MSG_INVALID_FLAG_RECENT - <i>Invalid flagRecent.</i>
     *      MSG_INVALID_FLAG_DRAFT - <i>Invalid flagDraft.</i>
     *      MSG_INVALID_IS_PRIVATE - <i>Invalid Private Flag.</i>
     *      MSG_INVALID_ARRIVAL_TIME - <i>Invalid Message Arrival Time.</i>
     *      MSG_INVALID_EXPIRY_TIME - <i>Invalid Message Expiry Time.</i>
     *      MSG_INVALID_KEYWORDS - <i>Invalid Keywords.</i>
     *      MSG_INVALID_OLD_MESSAGE_ID - <i>Invalid Old Message Id.</i>
     *      MSG_INVALID_UID - <i>Invalid Uid.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_MISSING_MAILBOXID_OR_MESSAGESTOREHOST_PARAM - <i>Unable to perform operation, messageStoreHost and mailboxId both are mandatory parameters if either is present.</i>
     *      MSG_SIZE_EXCEED_MAX_LIMIT - <i>Message not created, message size exceeds the maximum limit.</i>
     *      MBX_QUOTA_LIMIT_ERROR - <i>Message not created, message size exceeds the maximum quota.</i>
     *      INVALID_MSG_BASE64_ENCODED_VALUE - <i>Invalid message base64 encoded value.</i>
     *      MSG_INVALID_THREAD_ID - <i>Invalid Thread Id.</i>
     *   
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    String create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading message.
     *
     * @param inputParams Parameters given by user.
     * @return returns Mailbox POJO.
     * @throws MxOSException MxOSException.
     */
    /**
     * This operation is responsible for reading message.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name of the folder where the message resides</i>
     *     Type: String, Maximum Length: 255
     *     Example: openwave
     * <b>messageId</b>
     *     <i>Message Id of the message to be retrieved. This is unique for the system. max-length(excluding angular brackets)=255</i>
     *     Type: String, Maximum Length: 128
     *     Example: openwave123
     *     
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: trueOrFalse, NonEmpty: true
     *     Example: false
     * <b>offset</b>
     *     <i>Offset for reading the message. Allowed Values: Integer. Min: 0, Max: 2147483647. Only for Mx 8.4.x and earlier.</i>
     *     Type: Integer, Min: 0, Max: 2147483647
     * <b>length</b>
     *     <i>Length of the message to be read. Allowed Values: Integer. Min: 0, Max: 2147483647. Only for Mx 8.4.x and earlier.</i>
     *     Type: Integer, Min: 0, Max: 2147483647
     * <b>popDeletedFlag</b>
     *     <i>Pop deleted flag, allowed values are true/false. Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - The messages that are marked as pop deleted are returned as deleted messages.
     *     false - The messages that are marked as pop deleted are not returned as deleted.
     * </pre>
     * 
     * @return returns MetaData POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * 
     * <b>Error Codes:</b>
     *     MBX_INVALID_EMAIL - <i>Invalid email</i>
     *     FLD_INVALID_FOLDERNAME - <i>Invalid folder name</i>
     *     MSG_INVALID_MESSAGE_ID - <i>Invalid message Id</i>
     *     MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value</i>
     *     MSG_INVALID_OFFSET - <i>Invalid message offset value.</i>
     *     MSG_INVALID_LENGTH - <i>Invalid message length value.</i>
     *     GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters</i>
     *     MSG_UNABLE_TO_PERFORM_SEARCH - <i>Unable to perform Get Message operation.</i>
     *     FLD_NOT_FOUND - <i>Given folder NOT found.</i>
     *     MSG_NOT_FOUND - <i>Given message(id) NOT found.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Message read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading message by UID.
     *
     * @param inputParams Parameters given by user.
     * @return returns Mailbox POJO.
     * @throws MxOSException MxOSException.
     */
    Message readByUID(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for update message by UID.
     *
     * @param inputParams Parameters given by user.
     * @return returns Mailbox POJO.
     * @throws MxOSException MxOSException.
     */
    void updateByUID(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting message.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * <b>folderName</b>
     *     <i>Folder Name which contains message to be deleted</i>
     *     Type: String, Maximum Length: 255
     * <b>messageId</b>
     *     <i>One or more MessageIDs which need to be copied</i>
     *     Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_DELETE - <i>Unable to perform delete message operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting multiple messages. It uses one
     * or more actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>folderName</b>
     *      <i>Folder Name which contains message to be deleted</i>
     *      Type: String, Maximum Length: 255
     * <b>messageId</b>
     *      <i>One or more MessageIDs which need to be deleted</i>
     *      Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *        Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *      <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *      <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *      <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_DELETE - <i>Unable to perform delete message operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void deleteMulti(final Map<String, List<String>> inputParams)
        throws MxOSException;
    
    /**
     * This operation is responsible for deleting all messages. It uses one or
     * more actions to do this activity.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>folderName</b>
     *      <i>Folder Name which contains message to be deleted</i>
     *      Type: String, Maximum Length: 255
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *        Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *      <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *      <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *      <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *      Type: String; Enum: false, true; EmptyAllowed: no
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_FOLDERNAME - <i>Invalid folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_DELETE - <i>Unable to perform delete message operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void deleteAll(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for copying a message from one
     * folder to another folder.
     *
     * @param inputParams 
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *      <i>Folder Name which contains the message to be copied</i>
     *      Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *      <i>Folder Name to which the message to be copied</i>
     *      Type: String, Maximum Length: 255
     * <b>messageId</b>
     *      <i>One or more MessageIDs which need to be copied</i>
     *      Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>mailboxId</b>
     *     <i>Subscriber mailbox's ID. It's a mandatory parameter if messageStoreHost is present.</i>
     *     Type: Long; EmptyAllowed: no
     * <b>messageStoreHost</b>
     *     <i>Specifies the name of the host or service cluster. It's a mandatory parameter if mailboxId is present</i>
     *     Type: String, Maximum Length: 255
     * <b>realm</b>
     *     <i>Specifies mailbox mail realm of the user. It's a mandatory parameter if mailboxId is present</i>
     *     Type: String, Maximum Length: 255
     * <b>flagSeen</b>
     *     <i>Specifies whether the flag is seen, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagAns</b>
     *     <i>Specifies whether the flag is answer, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagFlagged</b>
     *     <i>Specifies whether the flag is flagged, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDel</b>
     *     <i>Specifies whether the flag is delete, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagRecent</b>
     *     <i>Specifies whether the the flag is recent, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDraft</b>
     *     <i>Specifies whether the the flag is draft, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>uid</b>
     *     <i>UID of the message to be created with. Allowed Values: Integer. Min: 0, Max: 2147483647</i>
     *     Type: Integer, Min: 0, Max: 2147483647
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MBX_INVALID_MAILBOXID - <i>Invalid mailboxid.</i>
     *      MSG_INVALID_MESSAGE_STORE_HOST - <i>Invalid Message Store Host.</i>
     *      MBX_INVALID_MAIL_REALM - <i>Invalid mail realm.</i>
     *      MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *      MSG_INVALID_FLAG_SEEN - <i>Invalid flagSeen.</i>
     *      MSG_INVALID_FLAG_ANS - <i>Invalid flagAns.</i>
     *      MSG_INVALID_FLAG_FLAGGED - <i>Invalid flagFlagged.</i>
     *      MSG_INVALID_FLAG_DEL - <i>Invalid flagDel.</i>
     *      MSG_INVALID_FLAG_RECENT - <i>Invalid flagRecent.</i>
     *      MSG_INVALID_FLAG_DRAFT - <i>Invalid flagDraft.</i>
     *      MSG_INVALID_UID - <i>Invalid Uid.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_COPY - <i>Unable to perform copy message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to copy messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void copy(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for copying multiple messages from one
     * folder to another folder.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *     <i>Folder Name which contains the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *     <i>Folder Name to which the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>messageId</b>
     *     <i>One or more MessageIDs which need to be copied</i>
     *     Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>mailboxId</b>
     *     <i>Mailbox Id of the user. Allowed values: Long</i>
     *     Type: Long
     * <b>messageStoreHost</b>
     *     <i>Mailbox Message Store host of the user</i>
     *     Type: String, Maximum Length: 255, EmptyAllowed: yes
     * <b>realm</b>
     *     <i>Specifies mailbox mail realm of the user. It's a mandatory parameter if mailboxId is present</i>
     *     Type: String, Maximum Length: 255
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_INVALID_MESSAGE_STORE_HOST - <i>Invalid Message Store Host.</i>
     *      MBX_INVALID_MAIL_REALM - <i>Invalid mail realm.</i>
     *      MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *      MSG_INVALID_UID - <i>Invalid Uid.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_COPY - <i>Unable to perform copy message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to copy messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void copyMulti(final Map<String, List<String>> inputParams)
        throws MxOSException;
    
    /**
     * This operation is responsible for copying all the messages from one
     * folder to another folder.
     *
     * @param inputParams 
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *     <i>Folder Name which contains the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *     <i>Folder Name to which the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_COPY - <i>Unable to perform copy message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to copy messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void copyAll(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for moving a message from one
     * folder to another folder.
     *
     * @param inputParams 
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *      <i>Folder Name which contains the message to be copied</i>
     *      Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *      <i>Folder Name to which the message to be copied</i>
     *      Type: String, Maximum Length: 255
     * <b>messageId</b>
     *      <i>One or more MessageIDs which need to be copied</i>
     *      Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagSeen</b>
     *     <i>Specifies whether the flag is seen, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagAns</b>
     *     <i>Specifies whether the flag is answer, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagFlagged</b>
     *     <i>Specifies whether the flag is flagged, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDel</b>
     *     <i>Specifies whether the flag is delete, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagRecent</b>
     *     <i>Specifies whether the the flag is recent, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>flagDraft</b>
     *     <i>Specifies whether the the flag is draft, allowed values are true/false</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>uid</b>
     *     <i>UID of the message to be created with. Allowed Values: Integer. Min: 0, Max: 2147483647</i>
     *     Type: Integer, Min: 0, Max: 2147483647
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_INVALID_IS_ADMIN - <i>Invalid isAdmin value.</i>
     *      MSG_INVALID_FLAG_SEEN - <i>Invalid flagSeen.</i>
     *      MSG_INVALID_FLAG_ANS - <i>Invalid flagAns.</i>
     *      MSG_INVALID_FLAG_FLAGGED - <i>Invalid flagFlagged.</i>
     *      MSG_INVALID_FLAG_DEL - <i>Invalid flagDel.</i>
     *      MSG_INVALID_FLAG_RECENT - <i>Invalid flagRecent.</i>
     *      MSG_INVALID_FLAG_DRAFT - <i>Invalid flagDraft.</i>
     *      MSG_INVALID_UID - <i>Invalid Uid.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_COPY - <i>Unable to perform copy message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to copy messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void move(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for moving multiple messages from one
     * folder to another folder.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *     <i>Folder Name which contains the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *     <i>Folder Name to which the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>messageId</b>
     *     <i>One or more MessageIDs which need to be copied</i>
     *     Type: String, Maximum Length: 128
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_INVALID_MESSAGE_ID - <i>Invalid messageId value.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_MOVE - <i>Unable to perform move message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to move messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void moveMulti(final Map<String, List<String>> inputParams)
        throws MxOSException;
    
    /**
     * This operation is responsible for moving all the messages from one
     * folder to another folder.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * <b>srcFolderName</b>
     *     <i>Folder Name which contains the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * <b>toFolderName</b>
     *     <i>Folder Name to which the message to be copied</i>
     *     Type: String, Maximum Length: 255
     * </pre>
     * 
     * <pre>
     * <b>inputParams-Optional:</b>
     * 
     * <b>isAdmin</b>
     *     <i>Specifies whether the operation is performed by admin user or end user, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionFolderIsHint</b>
     *     <i>Specifies that the message may not be in specified src folder, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionMultipleOk</b>
     *     <i>Specifies to look in multiple folders, multiple uid matches are ok, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>optionSupressMers</b>
     *     <i>Specifies to suppress MERS events for this operation, allowed values are true/false. Only for Mx 8.4.x and earlier.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     * <b>disableNotificationFlag</b>
     *     <i>Disable notification flag, allowed values are true/false - Supported with Mx 9.0 and later.</i>
     *     Type: String; Enum: false, true; EmptyAllowed: no
     *     true - Send notification to the IMAP client.
     *     false - Do not send notification to the IMAP client.
     * </pre>
     * 
     * @return Status - 200 OK.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *      MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     *      GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *      FLD_INVALID_SRC_FOLDERNAME - <i>Invalid source folderName value.</i>
     *      FLD_INVALID_DEST_FOLDERNAME - <i>Invalid destination folderName value.</i>
     *      MSG_UID_ALREADY_EXISTS - <i>Given message UID already exists in given folder.</i>
     *      MSG_INVALID_DISABLE_NOTIFICATION_FLAG - <i>Invalid Disable Notification Flag.</i>
     *      MSG_UNABLE_TO_PERFORM_MOVE - <i>Unable to perform move message operation.</i>
     *      MSG_ERROR_COPY_SOURCE_DEST_CANNOT_BE_SAME - <i>Unable to move messages, source and destination folders cannot be same.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void moveAll(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
