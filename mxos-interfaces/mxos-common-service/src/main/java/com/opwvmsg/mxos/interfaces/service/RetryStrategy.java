/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service;

public class RetryStrategy {

    private int numRetries;
    private long sleepBetweenRetries;

    public RetryStrategy() {
        this(1, 0);
    }

    public RetryStrategy(int numRetries) {
        this(numRetries, 0);
    }

    public RetryStrategy(int numRetries, long sleepBetweenRetries) {
        super();
        this.numRetries = numRetries;
        this.sleepBetweenRetries = sleepBetweenRetries;
    }

    public int getNumRetries() {
        return numRetries;
    }

    public void setNumRetries(int numRetries) {
        this.numRetries = numRetries;
    }

    public long getSleepBetweenRetries() {
        return sleepBetweenRetries;
    }

    public void setSleepBetweenRetries(long sleepBetweenRetries) {
        this.sleepBetweenRetries = sleepBetweenRetries;
    }

}