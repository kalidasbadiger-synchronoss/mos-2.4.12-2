/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.interfaces.service;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Factory class to get MxOS Context instance based.
 *
 * @author mxos-dev
 */
public final class MxOSContextFactory {
    private static Logger logger = Logger.getLogger(MxOSContextFactory.class);
    private static final Map<String, IMxOSContext> contexts =
        new ConcurrentHashMap<String, IMxOSContext>();

    private static class MxOSContextHolder {
        public static final MxOSContextFactory instance = new MxOSContextFactory();
    }

    /**
     * Method to get Instance of MxOSContextFactory object.
     * 
     * @return MxOSContextFactory object
     * @throws Exception Exception
     */
    public static MxOSContextFactory getInstance() {
        return MxOSContextHolder.instance;
    }

    /**
     * Default Constructor.
     */
    private MxOSContextFactory() {
        logger.info("MxOSContextFactory instance created");
    }

    /**
     * Method to get MxOSContext instance.
     * 
     * @param contextId - Id of the Context
     * @param p - properties
     * @return IMxOSContext MxOSContext instance
     * @throws MxOSException Exception
     */
    public final synchronized IMxOSContext getContext(final String contextId,
            final Properties p) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("start");
        }
        if (contextId == null || "".equals(contextId)) {
            logger.error("Invalid Context Name.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Invalid Context Id.");
        }
        if (contexts.containsKey(contextId)) {
            logger.warn("MxOSContext instance of (" + contextId
                    + ") is already exists");
            return contexts.get(contextId);
        }
        if (p == null) {
            logger.error("Properties have not been set to get the context.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Properties have not been set to get the context.");
        }
        try {
            // This can used across the system later on.
            // For example: RestMxOSContext need the below params
            // mxos-host - url of the mxos (Ex: http://localhost:8080/mxos)
            // max-connections - number of max connections required (Ex: 10)
            // System.getProperties().putAll(p);
            if (p.containsKey(ContextProperty.MXOS_CONTEXT_TYPE)) {
                IMxOSContext mxosContext = null;
                String contextType = p
                        .getProperty(ContextProperty.MXOS_CONTEXT_TYPE);
                if (contextType != null && !contextType.equals("")) {
                    mxosContext = ServiceLoader.loadContext(ContextEnum
                            .find(contextType));
                    mxosContext.init(contextId, p);
                }
                if (mxosContext != null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("end");
                    }
                    contexts.put(contextId, mxosContext);
                    return mxosContext;
                } else {
                    logger.error("Not able to create MxOS Context.");
                    throw new Exception("Not able to create MxOS Context.");
                }
            } else {
                logger.error("MXOS_CONTEXT_CLASS not provided.");
                throw new Exception("MXOS_CONTEXT_CLASS not provided.");
            }
        } catch (MxOSException e) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } catch (Exception e) {
            logger.error("Exception encountered  while loading MxOS Context", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }
}
