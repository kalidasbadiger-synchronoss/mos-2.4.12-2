/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.common;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Folder;

/**
 * Interface to Tasks Folder level operations like Create, Read, Update and List Folder.
 * 
 * @author mxos-dev
 */
public interface IAppsFolderService {

    /**
     * This operation is responsible for creating folder. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>userId</b>
     *     <i>Subscriber's email address or userId</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderName</b>
     *     <i>Name of the folder to be created, folder name is case sensitive.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     *     Example: TEST
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>module</b>
     *     <i>Name of the module which implements this folder</i>
     *     Type: String, Enum: "tasks", "calendar", "contacts", "infostore", "mail"
     * 
     * <b>folderId</b>
     *     <i>Open Xchange folder where the task should be created, default is private folder (23)</i>
     *     Type: long
     * </pre>
     * 
     * @return folderId of the newly created folder
     * @throws TasksException TasksException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * TSK_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_SESSION - <i>Invalid session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDER_NAME - <i>Invalid task folder name.</i>
     * TSK_TASKFOLDER_UNABLE_TO_CREATE - <i>Unable to create a task folder.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    
    /**
     * This operation is responsible for reading a folder data. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderId</b>
     *     <i>folderId of the created folder.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     *     Example: TEST
     * 
     * @return folderPojo of the folder
     * @throws TasksException TasksException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * TSK_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_SESSION - <i>Invalid session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDERID - <i>Invalid Task Folder Id.</i>
     * TSK_TASKFOLDER_UNABLE_TO_GET - <i>Unable to get task folder data.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Folder read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting a folder. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * <b>folderId</b>
     *     <i>folderId of the created folder.</i>
     *     Type: String, folderNameLength: 255, folderDepth: 30
     *     Example: TEST
     * 
     * @return
     * @throws TasksException TasksException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * TSK_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_SESSION - <i>Invalid session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDERID - <i>Invalid Task Folder Id.</i>
     * TSK_UNABLE_TO_TASKFOLDER_DELETE - <i>Unable to delete a task folder.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    List<Folder> list(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all folders except the default folder. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     *     
     * @return 
     * @throws TasksException TasksException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * TSK_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_SESSION - <i>Invalid session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDERID - <i>Invalid Task Folder Id.</i>
     * TSK_UNABLE_TO_TASKFOLDER_DELETE - <i>Unable to delete a task folder.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void deleteAll(Map<String, List<String>> inputParams) throws MxOSException;

}
