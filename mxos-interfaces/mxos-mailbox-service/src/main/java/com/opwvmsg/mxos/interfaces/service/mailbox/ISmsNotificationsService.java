/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SmsNotifications;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox SMS Notifications Service operations interface which will be exposed
 * to the client. This interface is responsible for doing mailbox SMS
 * Notification Service related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ISmsNotificationsService {

    /**
     * This operation is responsible for reading SMS Notifications Service.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 *  
	 * @return returns Returns Mailbox smsNotification POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    SmsNotifications read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating SMS Notifications Service.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>smsBasicNotificationsEnabled</b>
	 *     <i>Allowed value- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>smsAdvancedNotificationsEnabled</b>
	 *     <i>Allowed value- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_INVALID_SMS_ADVANCED_NOTIFICATIONS_ENABLED - <i>Invalid sms advanced notifications enabled</i>
	 * MBX_UNABLE_TO_SET_SMS_ADVANCED_NOTIFICATIONS_ENABLED - <i>Unable to set smsAdvancedNotificationsEnabled</i>
	 * MBX_INVALID_SMS_BASIC_NOTICATIONS_ENABLED - <i>Invalid sms basic notifications enabled</i>
	 * MBX_UNABLE_TO_SET_SMS_BASIC_NOTICATIONS_ENABLED - <i>Unable to set smsBasicNotificationsEnabled</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
