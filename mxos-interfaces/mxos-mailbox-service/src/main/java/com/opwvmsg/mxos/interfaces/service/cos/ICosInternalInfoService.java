/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS for InternalInfo Object level operations like Read, Update.
 *
 * @author mxos-dev
 */
public interface ICosInternalInfoService {

    /**
     * This operation is responsible for reading InternalInfo
     * for a COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * </pre>
	 *  
	 * @return returns Base POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_CREATE - <i>Unable to perform Mailbox Create operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    InternalInfo read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating InternalInfo
     * attributes for a COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>webmailVersion</b>
	 *     <i>Webmail Version</i>
	 *     Type: String
	 *     Regex Pattern: "^[a-zA-Z0-9.]+$"
	 *     Example: 0
	 *     
	 * <b>selfCareAccessEnabled</b>
	 *     <i>Self Care Access Enabled</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>selfCareSSLAccessEnabled</b>
	 *     <i>Self Care SSL Access Enabled</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 *  <b>imapProxyHost</b>
	 *     <i>Imap Proxy host for the migration. It is case-sensitive</i>
	 *     Type: String, Maximum Length: 255, Empty Allowed: No
	 *     Regex Pattern: 
	 *     	"^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$"
	 *     Example: host
	 *     
	 * <b>imapProxyAuthenticationEnabled</b>
	 *     <i>IMAP Proxy Authentication</i>
	 *     TType: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>remoteCallTracingEnabled</b>
	 *     <i>Remote Call Tracing Enabled</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>interManagerAccessEnabled</b>
	 *     <i>Enables account access to the InterManager for the Openwave Email application</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>interManagerSSLAccessEnabled</b>
	 *     <i>Enables account access to the SSL-encrypted InterManager for the Openwave Email application.</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>voiceMailAccessEnabled</b>
	 *     <i>Specifies whether the subscriber account has access to voice message features from the web interface</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>faxMailAccessEnabled</b>
	 *     <i>Specifies whether the subscriber account has access to fax features from the web interface</i>
	 *     Type: String, ENUM ["no", "yes"], Empty Allowed: No
	 *     Example: yes
	 *     
	 * <b>ldapUtilitiesAccessType</b>
	 *     <i>SControls account access to standard LDAP services such as imldapsh, ldapadd, ldapmodify, and other LDAP utilities</i>
	 *     Type: String, ENUM ["all", "trusted", "none"], Empty Allowed: No
	 *     Example: mss
	 *     
	 * <b>addressBookProvider</b>
	 *     <i>Specifies the subscriber-preferred address book provider</i>
	 *     Type: String, ENUM ["mss", "plaxo", "sun", "vox"], Empty Allowed: No
	 *     Example: mss
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 * MBX_INVALID_WEBMAIL_VERSION - <i>Invalid webmailVersion</i>
	 * MBX_INVALID_SELFCARE_ACCESS - <i>Invalid selfcare access</i>
	 * MBX_INVALID_SELFCARE_SSL_ACCESS - <i>Invalid selfcare SSL access</i>
	 * MBX_INVALID_IMAP_PROXY_AUTH - <i>Invalid imapProxyAuthenticationEnabled</i>
	 * MBX_INVALID_REMOTE_CALL_TRACING - <i>Invalid remoteCallTracingEnabled</i>
	 * MBX_INVALID_INTER_MANAGER_ACCESS - <i>Invalid interManagerAccessEnabled" />
	 * MBX_INVALID_INTER_MANAGER_SSL_ACCESS - <i>Invalid interManagerSSLAccessEnabled" />
	 * MBX_INVALID_VOICE_MAIL_ACCESS - <i>Invalid voiceMailAccessEnabled</i>
	 * MBX_INVALID_FAX_ACCESS - <i>Invalid faxAccessEnabled</i>
	 * MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE - <i>Invalid ldapUtilitiesAccessType</i>
	 * MBX_INVALID_ADDRESS_BOOK_PROVIDER - <i>Invalid addressBookProvider</i>
	 * MBX_UNABLE_TO_INTERNALINFO_GET - <i>Unable to perform InternalInfo GET operation</i>
	 * MBX_UNABLE_TO_SET_WEBMAIL_VERSION - <i>Unable to set webmailVersion</i>
	 * MBX_UNABLE_TO_SET_SELFCARE_ACCESS - <i>Unable to set selfcare access</i>
	 * MBX_UNABLE_TO_SET_SELFCARE_SSL_ACCESS - <i>Unable to set selfcare access</i>
	 * MBX_UNABLE_TO_SET_IMAP_PROXY_AUTH - <i>Unable to set imapProxyAuthenticationEnabled</i>
	 * MBX_UNABLE_TO_SET_REMOTE_CALL_TRACING - <i>Unable to set remote call tracing</i>
	 * MBX_UNABLE_TO_SET_INTER_MANAGER_ACCESS - <i>Unable to set interManagerAccessEnabled</i>
	 * MBX_UNABLE_TO_SET_INTER_MANAGER_SSL_ACCESS - <i>Unable to set interManagerSSLAccessEnabled</i>
	 * MBX_UNABLE_TO_SET_VOICE_MAIL_ACCESS - <i>Unable to set voiceMailAccessEnabled</i>
	 * MBX_UNABLE_TO_SET_FAX_ACCESS - <i>Unable to set faxAccessEnabled</i>
	 * MBX_UNABLE_TO_SET_LDAP_UTILITIES_ACCESS_TYPE - <i>Unable to set ldapUtilitiesAccessType</i>
	 * MBX_UNABLE_TO_SET_ADDRESS_BOOK_PROVIDER - <i>Unable to set addressBookProvider</i>
	 * MBX_UNABLE_TO_SET_IMAP_PROXY_HOST - <i>Unable to set IMAP proxy host</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
