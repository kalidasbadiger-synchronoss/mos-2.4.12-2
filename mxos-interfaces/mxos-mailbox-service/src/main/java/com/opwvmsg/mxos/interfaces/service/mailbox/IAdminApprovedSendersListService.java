/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Admin Approved Senders interface which will be exposed to the client. This interface
 * is responsible for doing Approved Senders related operations (like Create,
 * Read, Update, Delete, etc.) for family administrators.
 * 
 * @author mxos-dev
 */
public interface IAdminApprovedSendersListService {

    /**
     * This operation is responsible for creating Admin Approved Senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>adminApprovedSenders</b>
     *     <i>Approved sender email address to be added by family administrator</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}|[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: bar.com, foo@abc.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_INVALID_ADMIN_APPROVED_SENDER - <i>Invalid admin approved sender provided</i>
     * MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_CREATE - <i>Unable to create admin approved senders</i>
     * MBX_ADMIN_APPROVED_SENDERS_REACHED_MAX_LIMIT - <i>Admin blocked senders maximum limit reached</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading Admin Approved Senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of Admin Approved Senders of MailReceipt
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_GET - <i>Unable to get admin approved senders</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating admin approved senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldAdminApprovedSender</b>
     *     <i>Old admin approved sender email address/domain to be replaced</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * <b>newAdminApprovedSender</b>
     *     <i>New admin approved sender email address</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_INVALID_ADMIN_OLD_APPROVED_SENDER - <i>Invalid admin old approved sender provided</i>
     * MBX_ADMIN_APPROVED_SENDER_NOT_EXIST - <i>Admin old approved sender does not exist</i>
     * MBX_INVALID_ADMIN_NEW_APPROVED_SENDER - <i>Invalid admin new approved sender provided</i>
     * MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_POST - <i>Unable to update admin old approved sender list with new blocked senders list</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting admin approved sender.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>adminApprovedSender</b>
     *     <i>Admin blocked sender email address/domain to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_INVALID_ADMIN_APPROVED_SENDER - <i>Invalid admin approved sender provided</i>
     * MBX_ADMIN_APPROVED_SENDER_NOT_EXIST - <i>Given admin approved sender does not exist</i>
     * MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_DELETE - <i>Unable to delete admin approved senders list</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
