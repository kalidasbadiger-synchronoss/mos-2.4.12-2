/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Admin Control operations interface which will be exposed to the client. This
 * interface is responsible for doing Admin Control related operations (like
 * Read, Update, etc.).
 * 
 * @author mxos-dev
 */
public interface IAdminControlService {

    /**
     * This operation is responsible for reading Admin Control.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns AdminControl POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ADMINCONTROL_GET - <i>Unable to perform AdminControl GET operation</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Mailbox is not a group admin account.</i>
     * MBX_NOT_GROUP_ACCOUNT - <i>Mailbox is not a group account.</i>
     * GROUP_MAILBOX_FEATURE_IS_DISABLED - <i>Group Mailbox feature is disabled.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    AdminControl read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating Admin Control.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     *    
     * <b>adminDefaultDisposition</b>
     *     <i>Specifies the default action taken on messages sent to submailboxes that are from senders who are on neither the approved nor blocked list of senders - accept|reject</i>
     *     Type: String, ENUM ["accept", "reject"]
     *     Example: yes
     *     
     * <b>adminRejectAction</b>
     *     <i>Admin rejection action - drop|forward</i>
     *     Type: String, ENUM ["drop", "forward"]
     *     Example: D
     *     
     * <b>adminRejectInfo</b>
     *     <i>Additional information associated with the admin reject action attribute</i>
     *     Type: String, Maximum Length: 255
     *     Example: Trash
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_INVALID_ADMIN_REJECT_ACTION - <i>Invalid admin reject action</i>
     *     MBX_UNABLE_TO_SET_ADMIN_REJECT_ACTION - <i>Unable to set adminRejectAction</i>
     *     MBX_INVALID_ADMIN_REJECT_INFO - <i>Invalid admin reject info</i>
     *     MBX_UNABLE_TO_SET_ADMIN_REJECT_INFO - <i>Unable to set adminRejectInfo</i>
     *     MBX_INVALID_ADMIN_DEFAULT_DISPOSITION - <i>Invalid admin default disposition</i>
     *     MBX_UNABLE_TO_SET_ADMIN_DEFAULT_DISPOSITION - <i>Unable to set adminDefaultDisposition</i>
     *     MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Mailbox is not a group admin account.</i>
     *     MBX_NOT_GROUP_ACCOUNT - <i>Mailbox is not a group account.</i>
     *     GROUP_MAILBOX_FEATURE_IS_DISABLED - <i>Group Mailbox feature is disabled.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
