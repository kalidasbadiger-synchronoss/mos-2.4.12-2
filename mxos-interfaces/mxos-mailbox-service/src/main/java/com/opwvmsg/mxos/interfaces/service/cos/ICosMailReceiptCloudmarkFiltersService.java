/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.CloudmarkFilters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Cos mailReceipt cloudmark filter operations interface which will be
 * exposed to the client. This interface is responsible for doing mailReceipt
 * cloudmark filter related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailReceiptCloudmarkFiltersService {

    /**
     * This operation is responsible for reading mailReceipt cloudmark filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: 
     *      "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * 
     * </pre>
     * @return CloudmarkFilters Object of MailReceipt.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    CloudmarkFilters read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailReceipt cloudmark filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: 
     *      "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     *
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>spamfilterEnabled</b>
     *     <i>Spam Filter Enabled.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>spamPolicy</b>
     *     <i>Spam Policy Type.</i>
     *     Type: String, Enum: "mild", "moderate", "aggressive"
     * <b>spamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * <b>cleanAction</b>
     *     <i>Clean Action.</i>
     *     Type: String, Enum: "accept", "delete", "quarantine", "tag", "header"
     * <b>suspectAction</b>
     *     <i>Suspect Action.</i>
     *     Type: String, Enum: "accept", "delete", "quarantine", "tag", "header"
     * <b>folderName</b>
     *     <i>Folder Name.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern:
     *      "^[a-z, A-Z, 0-9]*$"
     * <b>tagText</b>
     *     <i>Tag Text.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: 
     *      "^[\\w \\!,\\#-\\/,\\:-\\@,\\[-\\`,\\{-\\~]*$"
     * <b>headerText</b>
     *     <i>Header Text.</i>
     *     Type: String, Maximum Length: 255 
     *     Regex Pattern: 
     *      "^[\\w \\!,\\#-\\/,\\:-\\@,\\[-\\`,\\{-\\~]*$"            
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_CLOUDMARK_FILTERS_CLEAN_ACTION - <i>Invalid cloudmark filters clean action.</i>
     * MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_CLEAN_ACTION - <i>Unable to set cleanAction.</i>
     * MBX_INVALID_CLOUDMARK_FILTERS_SPAM_ACTION - <i>Invalid cloudmark filters spam action.</i>
     * MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_ACTION - <i>Unable to set spamAction.</i>
     * MBX_INVALID_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED - <i>Invalid cloudmark filters spam filtering enabled.</i>
     * MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_FILTER_ENABLED - <i>Unable to set spamfilterEnabled.</i>
     * MBX_INVALID_CLOUDMARK_FILTERS_SPAM_POLICY - <i>Invalid cloudmark filters spam policy.</i>
     * MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_POLICY - <i>Unable to set spamPolicy.</i>
     * MBX_INVALID_CLOUDMARK_FILTERS_SUSPECT_ACTION - <i>Invalid cloudmark filters spam action.</i>
     * MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SUSPECT_ACTION - <i>Unable to set suspectAction.</i>
     * MBX_INVALID_FOLDER_NAME - <i>Invalid folder name.</i>
     * MBX_INVALID_HEADER_TEXT - <i>Invalid header text.</i>
     * MBX_INVALID_TAG_TEXT - <i>Invalid tag text.</i> 
     * FOLDER_NAME_REQUIRED <i>Folder name is required for action quarantine. </i>
     * TAG_TEXT_REQUIRED <i>Tag text is required for action tag. </i>
     * HEADER_TEXT_REQUIRED <i>message="Header text is required for action header. </i> 
     *  
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
