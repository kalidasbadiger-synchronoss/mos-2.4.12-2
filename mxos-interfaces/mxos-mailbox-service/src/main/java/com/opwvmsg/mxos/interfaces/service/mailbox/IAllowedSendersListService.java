/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Allowed Senders Service operations interface which will be exposed to
 * the client. This interface is responsible for doing User Themes related
 * operations (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IAllowedSendersListService {

    /**
     * This operation is responsible for creating Allowed Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>allowedSender</b>
     *     <i>Subscriber's allowed sender email address to be added</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}|[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com OR openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ALLOWED_SENDERS_CREATE - <i>Unable to perform Allowed Senders Create operation.</i>
     * MBX_ALLOWED_SENDERS_REACHED_MAX_LIMIT - <i>Number of allowed senders reached the maximum limit.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading Allowed Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of allowed Senders of MailReceipt
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ALLOWED_SENDERS_GET - <i>Unable to perform Allowed Senders GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Allowed Sender.
     *    
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldAllowedSender</b>
     *     <i>Subscriber's old allowed sender email address to be replaced</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * <b>newAllowedSender</b>
     *     <i>Subscriber's new allowed sender email address</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ALLOWED_SENDERS_POST - <i>Unable to perform Allowed Senders POST operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting Allowed Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>allowedSender</b>
     *     <i>Subscriber's allowed sender email address to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ALLOWED_SENDERS_DELETE - <i>Unable to perform Allowed Senders DELETE operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
