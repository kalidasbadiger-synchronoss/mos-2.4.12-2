/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * MailSend Signature interface which will be exposed to the client. This
 * interface is responsible for signature related operations
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailSendSignatureService {

    /**
     * This operation is responsible for creating mail send signatures.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>signature</b>
     *     <i>Signature text of the mail user</i>
     *     Type: String, MaxLength: 1000
     *     Example: Signature 1
     * 
     * <b>inputParams-Optional:</b>
     * <b>signatureName</b>
     *     <i>Specifies the name of the signature.</i>
     *     Type: String, Maximum Length:: 129
     * <b>isDefault</b>
     *     <i>Specifies a boolean value whether signature is default or not.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>addSignatureInReplyType</b>
     *     <i>Specifies the subscriber-preferred option to place the personal signature, allowed Value - one of beforeOriginalMessage, afterOriginalMessage</i>
     *     Type: String, Enum: "afterOriginalMessage", "beforeOriginalMessage"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * MBX_SIGNATURE_ALREADY_EXISTS - <i>Signature already exists</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_INVALID_SIGNATURE - <i>Invalid signature.</i>
     * MBX_INVALID_ADD_SIGNATURE_IN_REPLY_TYPE - <i>Invalid add signature in reply.</i>
     * MBX_INVALID_SIGNATURE_DEFAULT - <i>Invalid signature isDefault flag.</i>
     * MBX_INVALID_SIGNATURE_NAME - <i>Invalid signature name.</i>
     * MBX_UNABLE_TO_PUT_MAIL_SEND_SIGNATURE - <i>Unable to add signature</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading mailSend signatures.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>.
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of Signature objects of MailSend.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_MAILSEND_SIGNATURE_GET - <i>Unable to perform MailSend Signature GET operation</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Signature> read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading a single mailSend signatures.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>.
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>signatureId</b>
     *     <i>signatureId of the Signature</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * </pre>
     * 
     * @return List of Signature objects of MailSend.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_MAILSEND_SIGNATURE_GET - <i>Unable to perform MailSend Signature GET operation</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Signature readSignature(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating signatures of MailSend.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>signatureId</b>
     *     <i>signatureId of the Signature</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>signature</b>
     *     <i>Signature text of the mail user</i>
     *     Type: String, MaxLength: 1000
     *     Example: Signature 1
     * <b>signatureName</b>
     *     <i>Specifies the name of the signature.</i>
     *     Type: String, Maximum Length:: 129
     * <b>isDefault</b>
     *     <i>Specifies a boolean value whether signature is default or not.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>addSignatureInReplyType</b>
     *     <i>Specifies the subscriber-preferred option to place the personal signature, allowed Value - one of beforeOriginalMessage, afterOriginalMessage</i>
     *     Type: String, Enum: "afterOriginalMessage", "beforeOriginalMessage"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_SIGNATURE_ALREADY_EXISTS - <i>Signature already exists</i>
     * MBX_UNABLE_TO_SET_MAIL_SEND_SIGNATURE - <i>Unable to set mail send signature</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_INVALID_SIGNATURE - <i>Invalid signature.</i>
     * MBX_INVALID_ADD_SIGNATURE_IN_REPLY_TYPE - <i>Invalid add signature in reply.</i>
     * MBX_INVALID_SIGNATURE_DEFAULT - <i>Invalid signature isDefault flag.</i>
     * MBX_INVALID_SIGNATURE_NAME - <i>Invalid signature name.</i>
     * MBX_INVALID_SIGNATURE_ID - <i>Invalid signature Id.</i>
     * MBX_MAIL_SEND_SIGNATURE_NOT_FOUND - <i>MailSend signature not found.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting signature.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>signatureId</b>
     *     <i>SignatureId of the signature</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format</i>
     * MBX_INVALID_SIGNATURE_ID - <i>Invalid signature Id.</i>
     * MBX_MAIL_SEND_SIGNATURE_NOT_FOUND - <i>MailSend signature not found.</i>
     * MBX_UNABLE_TO_DELETE_MAIL_SEND_SIGNATURE - <i>Unable to delete mail send signature.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
