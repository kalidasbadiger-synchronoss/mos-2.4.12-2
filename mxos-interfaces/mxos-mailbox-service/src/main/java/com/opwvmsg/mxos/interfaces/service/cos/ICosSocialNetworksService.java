/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * SocialNetworkCos interface which will be exposed to the client. This
 * interface is responsible for doing Social Network related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosSocialNetworksService {

    /**
     * This operation is responsible for reading Social Network.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @return returns SocialNetworks POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_COSID - <i>Invalid cosId.</i>
     * MBX_INVALID_NETMAIL_SOCIALNETWORKSITE - <i>Invalid netMailSocialNetworkSite value in Ldap.</i>
     * MBX_INVALID_SNINTEGRATIONALLOWED - <i>Invalid SocialNetworkIntegrationAllowed value.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    SocialNetworks read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Social Network.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * <b>socialNetworkIntegrationAllowed</b>
     *     <i>Specifies if Social Network integration is allowed.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_COSID - <i>Invalid cosId.</i>
     * MBX_INVALID_SNINTEGRATIONALLOWED - <i>Invalid SocialNetworkIntegrationAllowed value.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
