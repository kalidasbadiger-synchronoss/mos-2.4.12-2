/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Blocked Senders interface which will be exposed to the client.
 * This interface is responsible for doing Blocked Senders related operations
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IBlockedSendersListService {

    /**
     * This operation is responsible for creating Blocked Sender.
     *
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>blockedSender</b>
     *     <i>Subscriber's blocked sender email address to be added</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}|[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com OR openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_ALLOWED_SENDERS_CREATE - <i>Unable to perform Allowed Senders Create operation.</i>
     * MBX_ALLOWED_SENDERS_REACHED_MAX_LIMIT - <i>Number of allowed senders reached the maximum limit.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading Blocked Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of Blocked Senders of MailReceipt
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_BLOCKED_SENDERS_GET - <i>Unable to perform Blocked Senders GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Blocked Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldBlockedSender</b>
     *     <i>Subscriber's old blocked sender email address to be replaced</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * <b>newAllowedSender</b>
     *     <i>Subscriber's new blocked sender email address</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_BLOCKED_SENDERS_POST - <i>Unable to perform Blocked Senders POST operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting Blocked Sender.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>allowedSender</b>
     *     <i>Subscriber's blocked sender email address to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_BLOCKED_SENDERS_DELETE - <i>Unable to perform Blocked Senders DELETE operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
