/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Admin Blocked Senders interface which will be exposed to the client. This
 * interface is responsible for doing Blocked Senders related operations (like
 * Create, Read, Update, Delete, etc.) for family administrators.
 * 
 * @author mxos-dev
 */
public interface IAdminBlockedSendersListService {

    /**
     * This operation is responsible for creating Admin Blocked Senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>adminBlockedSenders</b>
     *     <i>Blocked sender email address to be added by family administrator</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}|[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: bar.com, foo@abc.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_INVALID_ADMIN_BLOCKED_SENDER - <i>Invalid admin blocked sender provided</i>
     * MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_CREATE - <i>Unable to create admin blocked senders</i>
     * MBX_ADMIN_BLOCKED_SENDERS_REACHED_MAX_LIMIT - <i>Admin blocked senders maximum limit reached</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading Admin Blocked Senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of Admin Blocked Senders of MailReceipt
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_GET - <i>Unable to get admin blocked senders</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating admin blocked senders list.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldAdminBlockedSender</b>
     *     <i>Old admin blocked sender email address/domain to be replaced</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com     
     * <b>newAdminBlockedSender</b>
     *     <i>New admin blocked sender email address</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_INVALID_ADMIN_OLD_BLOCKED_SENDER - <i>Invalid admin old blocked sender provided</i>
     * MBX_ADMIN_BLOCKED_SENDER_NOT_EXIST - <i>Admin old blocked sender does not exist</i>
     * MBX_INVALID_ADMIN_NEW_BLOCKED_SENDER - <i>Given email not a valid family administrator</i>
     * MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_POST - <i>Unable to update admin old blocked sender list with new blocked senders list</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting admin blocked sender.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>adminBlockedSender</b>
     *     <i>Admin blocked sender email address/domain to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_NOT_GROUP_ADMIN_ACCOUNT - <i>Given email not a valid family administrator</i>
     * MBX_ADMIN_BLOCKED_SENDER_NOT_EXIST - <i>Given admin blocked sender does not exist</i>
     * MBX_INVALID_ADMIN_BLOCKED_SENDER - <i>Invalid admin blocked sender provided</i>
     * MBX_UNABLE_TO_ADMIN_BLOCKED_SENDERS_DELETE - <i>Unable to delete admin blocked senders list</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
