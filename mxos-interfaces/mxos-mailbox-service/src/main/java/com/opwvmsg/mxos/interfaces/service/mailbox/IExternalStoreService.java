/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.ExternalStore;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * External store operations interface which will be exposed to the client.
 * This interface is responsible for doing External store related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IExternalStoreService {

    /**
     * This operation is responsible for reading External mail store.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return ExternalStore POJO of MailStore.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED - <i>Invalid externalStoreAccessAllowed.</i>
     * MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB - <i>Invalid maxExternalStoreSizeMB.</i>
     * MBX_UNABLE_TO_EXTERNALSTORE_GET - <i>Unable to perform External Store GET operation.</i> 
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters."/>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    ExternalStore read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating External mail store.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>maxExternalStoreSizeMB</b>
     *     <i>External storage size quota.</i>
     *     Type: Long, Minimum: 0, Maximum: 1073741823
     * <b>externalStoreAccessAllowed</b>
     *     <i>Specifies whether RichMail allows access to external store | yes/no</i>
     *     Type: String, Enum: "no", "yes"
     * </pre>
     *     
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters."/>
     * MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED - <i>Invalid externalStoreAccessAllowed.</i>
     * MBX_INVALID_EXTERNAL_STORE_ACCESS_ALLOWED - <i>Invalid externalStoreAccessAllowed.</i>
     * MBX_UNABLE_TO_SET_EXTERNAL_STORE_ACCESS_ALLOWED - <i>Unable to set external store access allowed.</i>
     * MBX_INVALID_MAX_EXTERNAL_STORE_SIZEMB - <i>Invalid maxExternalStoreSizeMB.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
