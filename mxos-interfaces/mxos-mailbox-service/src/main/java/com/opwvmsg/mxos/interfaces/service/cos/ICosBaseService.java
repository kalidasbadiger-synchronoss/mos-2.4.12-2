/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to COS Base Object level operations like Read, Update and List.
 *
 * @author mxos-dev
 */
public interface ICosBaseService {

    /**
     * This operation is responsible for reading COS.
     *
	 * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * </pre>
	 *  
	 * @return returns Base POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_UNABLE_TO_GET - <i>Unable to perform Cos GET operation</i>
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * COS_NOT_FOUND - <i>The given cosId does not exist</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
	 *
     */
    Base read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating COS.
     *
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: 
	 *     	"^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>maxNumAliases</b>
	 *     <i>Maximum number of aliases allowed for the user</i>
	 *     Type: integer, Minimum Val: 1, Maximum Val: 1000, Empty Allowed: No 
	 * 	   Example: 4
	 *     
	 * <b>maxNumAllowedDomains</b>
	 *     <i>Maximum number of allowed Domain for the user</i>
	 *     Type: integer, Minimum Val: 1, Maximum Val: 1000, Empty Allowed: No
	 * 	   Example: 4
	 *     
	 * <b>customFields</b>
	 *     <i>Custom Fields : customer specific data </i>
	 *     Type: String, Empty Allowed: No, Minimum Length: 1, Maximum Length: 255, BLOB Separator: :|
	 *     Example: 1 :| 2
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_INVALID_COSID - <i>Invalid cosId</i>
	 * MBX_INVALID_MSISDN_STATUS - <i>Invalid MSISDN Status</i>
	 * MBX_INVALID_MAX_ALLOWEDDOMAIN - <i>Invalid maxAllowedDomain value</i>
	 * MBX_INVALID_MAXNUM_OF_ALIASES - <i>Invalid maximum number of email aliases</i>
	 * MBX_UNABLE_TO_SET_MAX_ALLOWEDDOMAIN - <i>Unable to perform set maxNumAllowedDomain</i>
	 * MBX_UNABLE_TO_SET_MSISDN - <i>Unable to perform set account msisdn</i>
	 * MBX_UNABLE_TO_SET_MAX_NUM_ALIAS - <i>Unable to perform set maxNumAliases</i>
	 * MBX_UNABLE_TO_UPDATE - <i>Unable to perform Cos POST operation</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for searching COS.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>query</b>
	 *     <i>query to search, can be any valid LDAP search query</i>
	 *     Type: String, Maximum Length: 255
	 *     Example: bs000123
	 * </pre>
	 * 
	 * * <b>inputParams-Optional:</b>
	 * 
	 * <b>sortKey</b>
	 *     <i>Sorting Key</i>
	 *     Type: String, Maximum Length: 20
	 *     Example: bs
	 *     
	 * <b>sortOrder</b>
	 *     <i>Sorting Order</i>
	 *     Type: String, ENUM ["ascending", "descending"]
	 *     Example: ascending
	 *     
	 * <b>maxRows</b>
	 *     <i>Maximum Rows </i>
	 *     Type: integer, Minimum Val: 1, Maximum Val: 1000 
	 * 	   Example: 4
	 * </pre>
	 *  
	 * @return returns list containing COS Base POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * COS_UNABLE_TO_SEARCH - <i>Unable to perform Cos SEARCH operation</i>
	 * GEN_INVALID_SEARCH_QUERY - <i>Invalid search query.</i>
	 * GEN_INVALID_SORT_KEY - <i>Invalid sort key.</i>
	 * GEN_INVALID_SORT_ORDER - <i>Invalid sort order.</i>
	 * GEN_INVALID_SEARCH_MAXROWS - <i>Invalid search maxRows.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    List<Base> search(final Map<String, List<String>> inputParams)
        throws MxOSException;

}
