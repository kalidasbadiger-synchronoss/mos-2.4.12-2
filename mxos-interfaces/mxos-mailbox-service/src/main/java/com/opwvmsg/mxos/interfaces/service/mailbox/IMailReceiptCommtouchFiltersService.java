/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.CommtouchFilters;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox mailReceipt commtouch filter operations interface which will be
 * exposed to the client. This interface is responsible for doing mailReceipt
 * commtouch filter related operations (like Create, Read, Update, Delete,
 * etc.).
 *
 * @author mxos-dev
 */
public interface IMailReceiptCommtouchFiltersService {

    /**
     * This operation is responsible for reading mailReceipt commtouch
     * filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return CommontouchFilters Object of MailReceipt.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    CommtouchFilters read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mailReceipt commtouch
     * filters.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     *      
     * <b>spamAction</b>
     *     <i>Spam Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * <b>virusAction</b>
     *     <i>Virus Action.</i>
     *     Type: String, Enum: "discard", "none", "folder deliver", "add header", "subject prefix"
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     *     MBX_INVALID_MS_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Invalid spam action .</i>
     *     MBX_UNABLE_TO_SET_MS_COMMTOUCH_FILTERS_SPAM_ACTION - <i>Unable to perform set spamAction.</i>
     *     MBX_INVALID_MS_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Invalid virus action.</i>
     *     MBX_UNABLE_TO_SET_MS_COMMTOUCH_FILTERS_VIRUS_ACTION - <i>Unable to perform set virusAction.</i>
     *      
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
