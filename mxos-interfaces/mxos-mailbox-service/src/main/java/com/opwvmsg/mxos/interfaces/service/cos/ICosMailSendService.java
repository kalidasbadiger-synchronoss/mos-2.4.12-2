/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail send operations interface which will be exposed to the client. This
 * interface is responsible for doing mail send related operations (like Read,
 * Update, etc.).
 *
 * @author mxos-dev
 */
public interface ICosMailSendService {

    /**
     * This operation is responsible for reading mail send.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>cosId</b>
	 *     <i>Specifies Distinguished name of the class of service</i>
	 *     Type: String, Maximum Length: 30
	 *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
	 *     Example: class1
     * </pre>
	 * 
	 * @return returns MailSend POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation.</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MailSend read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail send.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>futureDeliveryEnabled</b>
     *     <i>Enables the delayed delivery feature for the current class of service (COS) or user.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>maxFutureDeliveryDaysAllowed</b>
     *     <i>The maximum number of days that a user can choose to delay a message before it is sent.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * <b>maxFutureDeliveryMessagesAllowed</b>
     *     <i>The maximum number of delayed delivery messages a user can have at one time.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>useRichTextEditor</b>
     *     <i>Specifies the subscriber-preferred default editor for composing messages in the web interface.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>includeOriginalMailInReply</b>
     *     <i>Include the original message with a reply message.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>originalMailSeperatorCharacter</b>
     *     <i>Specifies the subscriber-preferred format of original message text when subscribers reply to a message.</i>
     *     Type: String, Empty Allowed: No, Maximum Length: 225
     * <b>autoSaveSentMessages</b>
     *     <i>Specifies whether copies of outgoing messages are saved.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>addSignatureForNewMails</b>
     *     <i>Enables automatic insertion of signature text at the end of a newly composed message.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>addSignatureInReplyType</b>
     *     <i>Specifies the subscriber-preferred option to place the personal signature.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>autoSpellCheckEnabled</b>
     *     <i>Specifies whether the application automatically performs a spell check on out bound messages.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>confirmPromptOnDelete</b>
     *     <i>Specifies whether WebEdge prompts for confirmation before deleting a message.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>includeReturnReceiptReq</b>
     *     <i>Specifies the subscriber-preferred option for whether the application includes a return receipt request in all out bound messages.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>maxSendMessageSizeKB</b>
     *     <i>Specifies the maximum send message size.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2097151
     * <b>maxAttachmentSizeKB</b>
     *     <i>Specifies the maximum attachment size.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * <b>maxAttachmentsInSession</b>
     *     <i>Specifies the maximum number of  attachments as a total that a subscriber can hold during simultaneous message compositions</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     * <b>maxAttachmentsToMessage</b>
     *     <i>Specifies the maximum number of files that can be attached to a single message using the WebEdge Web interface</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     * <b>maxCharactersPerPage</b>
     *     <i>Specifies the maximum number of characters per page that the Web interface uses when displaying a message.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_MAILSEND_GET - <i>Unable to perform MailSend GET operation.</i>
     * MBX_INVALID_FUTURE_DELIVERY_ENABLED - <i>Invalid future delivery enabled.</i>
     * MBX_UNABLE_TO_SET_FUTURE_DELIVERY_ENABLED - <i>Unable to perform set futureDeliveryEnabled.</i>
     * MBX_INVALID_FUTURE_DELIVERY_DAYS - <i>Invalid future delivery days.</i>
     * MBX_UNABLE_TO_SET_FUTURE_DELIVERY_DAYS - <i>Unable to perform set futureDeliveryDays.</i>
     * MBX_INVALID_MAX_FUTURE_DELIVERY_MESSAGES - <i>Invalid max future delivery messages.</i>
     * MBX_UNABLE_TO_SET_MAX_FUTURE_DELIVERY_MESSAGES - <i>Unable to perform set maxFutureDeliveryMessages</i>
     * MBX_INVALID_USE_RICH_TEXT_EDITOR - <i>Invalid use rich text editor.</i>
     * MBX_UNABLE_TO_SET_USE_RICH_TEXT_EDITOR - <i>Unable to perform set useRichTextEditor</i>
     * MBX_INVALID_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Invalid include original mail in reply.</i>
     * MBX_UNABLE_TO_SET_INCLUDE_ORIGINAL_MAIL_IN_REPLY - <i>Unable to perform set includeOriginalMailInReply.</i>
     * MBX_INVALID_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Invalid original mail seperator character.</i>
     * MBX_UNABLE_TO_SET_ORIGINAL_MAIL_SEPERATOR_CHARACTER - <i>Unable to perform set maxSendMessageSizeKB.</i>
     * MBX_INVALID_AUTO_SAVE_MESSAGES - <i>Invalid auto save messages.</i>
     * MBX_UNABLE_TO_SET_AUTO_SAVE_MESSAGES - <i>Unable to perform set autoSaveMessages.</i>
     * MBX_INVALID_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Invalid add signature for new mails.</i>
     * MBX_UNABLE_TO_SET_ADD_SIGNATURE_FOR_NEW_MAILS - <i>Unable to perform set addSignatureForNewMails.</i>
     * MBX_INVALID_ADD_SIGNATURE_IN_REPLY - <i>Invalid add signature in reply.</i>
     * MBX_UNABLE_TO_SET_ADD_SIGNATURE_IN_REPLY - <i>Unable to perform set addSignatureInReply.</i>
     * MBX_INVALID_AUTO_SPELL_CHECK_ENABLED - <i>Invalid auto spell check enabled.</i>
     * MBX_UNABLE_TO_SET_AUTO_SPELL_CHECK_ENABLED - <i>Unable to perform set autoSpellCheckEnabled.</i>
     * MBX_INVALID_CONFIRM_PROMT_ON_DELETE - <i>Invalid confirn promt on delete.</i>
     * MBX_UNABLE_TO_SET_CONFIRM_PROMT_ON_DELETE - <i>Unable to perform set confirmPromtOnDelete.</i>
     * MBX_INVALID_INCLUDE_RETURN_RECEIPT_REQ - <i>Invalid inlcude return receipt req.</i>
     * MBX_UNABLE_TO_SET_INCLUDE_RETURN_RECEIPT_REQ - <i>Unable to perform set includeReturnReceiptReq.</i>
     * MBX_INVALID_MAX_SEND_MESSAGE_SIZE_KB - <i>Invalid max send message size kb.</i>
     * MBX_UNABLE_TO_SET_MAX_SEND_MESSAGE_SIZE_KB - <i>Unable to perform set maxSendMessageSizeKB.</i>
     * MBX_INVALID_MAX_ATTACHEMENT_SIZE_KB - <i>Invalid max attachement size kb.</i>
     * MBX_UNABLE_TO_SET_MAX_ATTACHEMENT_SIZE_KB - <i>Unable to perform set maxAttachmentSizeKB.</i>
     * MBX_INVALID_MAX_ATTACHMENTS_IN_SESSION - <i>Invalid max attachments in session</i>
     * MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_IN_SESSION - <i>Unable to perform set maxAttachments in session</i>
     * MBX_INVALID_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Invalid max attachments to message.</i>
     * MBX_UNABLE_TO_SET_MAX_ATTACHMENTS_TO_MESSAGE" - <i>Unable to perform set maxAttachmentsToMessage.</i>
     * MBX_INVALID_MAX_CHARACTERS_PER_PAGE - <i>Invalid max characters per page.</i>
     * MBX_UNABLE_TO_SET_MAX_CHARACTERS_PER_PAGE - <i>Unable to perform set maxCharactersPerPage.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
