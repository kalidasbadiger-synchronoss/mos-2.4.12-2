/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.domain;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Domain operations interface which will be exposed to the client. This
 * interface is responsible for doing domain related operations (like Create,
 * Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IDomainService {

    /**
     * This operation is responsible for creating domain. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>domain</b>
     *     <i>Complete domain name associated with a mailDomain object</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     * <b>domainType</b>
     *     <i>To create domain type as Local, value should be "local".</i>
     *     Type: String, Enum: "local", "nonauth", "rewrite"
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>relayHost</b>
     *     <i>Another mail host that accepts messages for a nonauthoritative domain if the primary host does not recognize the recipient.</i>
     *     Type: String, Maximum Length: 225
     * <b>alternateDomain</b>
     *     <i>Alternate domain.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * DMN_ALREADY_EXISTS - <i>The given domain already exists</i>
     * DMN_MISSING_ALTERNATEDOMAIN - <i>Missing mailRewriteDomain attribute. Should be present for type=rewrite</i>
     * DMN_MISSING_RELAYHOST - <i>Missing mailRelayHost attribute. Should be present for type=nonauth</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> userParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading domain. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>domain</b>
     *     <i>Complete domain name associated with a mailDomain object</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     * </pre>
     *  
     * @return returns Domain POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * DMN_INVALID_NAME - <i>Invalid domain, not RFC compliant, maxLength supported is 255.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Domain read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for searching domain. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>query</b>
     *     <i>LDAP Search Query</i>
     *     Type: String, Maximum Length: 255
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>sortKey</b>
     *     <i>Key for sorting.</i>
     *     Type: String, Maximum Length: 255
     * <b>sortOrder</b>
     *     <i>Sorting order.</i>
     *     Type: String, Enum: "ascending", "descending"
     * <b>maxRows</b>
     *     <i>Maximum number of search results.</i>
     *     Type: Integer, Minimum: 0, Maximum: 1000
     * </pre>
     *  
     * @return returns list containing Domain POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Domain> search(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating domain. It uses one or more
     * actions to do this activity.
     * 
     * * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>domain</b>
     *     <i>Complete domain name associated with a mailDomain object</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     
     * <b>inputParams-Optional:</b>
     * 
     * <b>type</b>
     *     <i>Domain type.</i>
     *     Type: String, Enum: "local", "nonauth", "rewrite"
     * <b>defaultMailbox</b>
     *     <i>Email account within a mail domain that is to receive all mail sent to nonexistent addresses within that domain.</i>
     *     Type: String, Empty Allowed: Yes, Maximum Length: 129
     * <b>relayHost</b>
     *     <i>Another mail host that accepts messages for a non-authoritative domain if the primary host does not recognize the recipient.</i>
     *     Type: String, Maximum Length: 225
     * <b>alternateDomain</b>
     *     <i>Alternate domain.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}$
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * DMN_INVALID_NAME - <i>Invalid domain, not RFC compliant, maxLength supported is 255</i>
     * DMN_INVALID_TYPE - <i>Invalid type, maxLength supported is 20</i>
     * DMN_INVALID_RELAYHOST - <i>Invalid mailRelayHost, maxLength supported is 255</i>
     * DMN_INVALID_REWRITTENDOMAIN - <i>Invalid mailRewriteDomain, not RFC compliant, please refer attribute domain</i>
     * DMN_INVALID_DEFAULT_MAILBOX - <i>Invalid defaultMailbox, not RFC compliant, please refer attribute email</i>
     * DMN_INVALID_CUSTOM_FEILDS - <i>Invalid Domain related customFields, maxLength supported is 255</i>
     * GEN_BAD_REQUEST, Message:Bad request, please check the request and parameters</i>
     * DMN_MISSING_ALTERNATEDOMAIN - <i>Missing mailRewriteDomain attribute. Should be present for type=Rewrite</i>
     * DMN_MISSING_RELAYHOST - <i>Missing mailRelayHost attribute. Should be present for type=Non-Authoritative</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting domain. It uses one or more
     * actions to do this activity.
     * 
     * * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>domain</b>
     *     <i>Complete domain name associated with a mailDomain object</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4}$
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * DMN_INVALID_NAME - <i>Invalid domain, not RFC compliant, maxLength supported is 255.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
