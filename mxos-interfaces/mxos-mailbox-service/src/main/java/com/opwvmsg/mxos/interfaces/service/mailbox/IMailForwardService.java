/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mail forward interface which will be exposed to the client. This interface is
 * responsible for doing mail forward related operations (like Create, Read,
 * Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IMailForwardService {

    /**
     * This operation is responsible for creating mail forward address.
     * 
     * @param inputParams
     * 
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>forwardingAddress</b>
     *     <i>Subscriber's local delivery email address to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com  
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_FORWARD_ADDRESS_CREATE - <i>Unable to perform MailForward Create operation.</i>
     * MBX_FORWARD_ADDRESSES_REACHED_MAX_LIMIT - <i>Number of mail forwarding addresses reached the maximum limit.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN - <i>Invalid forwarding address.</i>
     * MBX_INVALID_FORWARDING_ADDRESS - <i>Invalid forwarding address.</i>
     * MBX_UNABLE_TO_MAILRECEIPT_GET - <i>Unable to perform MailReceipt GET operation.</i>
     * MBX_UNABLE_TO_FORWARD_ADDRESS_GET - <i>Unable to perform MailForward GET operation.</i>
     * MBX_FORWARD_ADDRESSES_AND_EMAIL_SAME - <i>Forwarding address cannot be same as email.</i>
     * MBX_UNABLE_TO_MSSLINKINFO_GET - <i>Unable to perform MssLinkInfo GET operation.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading mail forward address.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of forward addresses.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_FORWARD_ADDRESS_GET - <i>Unable to perform MailForward GET operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating mail forward address.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldForwardingAddress</b>
     *     <i>Subscriber's old forwarding address to be replaced</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com     
     * <b>newForwardingAddress</b>
     *     <i>Subscriber's new forwarding address</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: 123@openwave.com
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_FORWARD_ADDRESS_POST - <i>Unable to perform MailForward POST operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN - <i>Invalid forwarding address.</i>
     * MBX_UNABLE_TO_FORWARD_ADDRESS_GET - <i>Unable to perform MailForward GET operation.</i>
     * MBX_FORWARD_ADDRESSES_AND_EMAIL_SAME - <i>Forwarding address cannot be same as email.</i>
     * MBX_INVALID_OLD_FORWARDING_ADDRESS - <i>Invalid old forwarding address.</i>
     * MBX_INVALID_OLD_FORWARDING_ADDRESS_NOT_EXIST - <i>Old forwarding address does not exist.</i>
     * MBX_INVALID_NEW_FORWARDING_ADDRESS - <i>Invalid new forwarding address.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting mail forward address.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>forwardingAddress</b>
     *     <i>Subscriber's local delivery email address to be deleted</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com     
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>     
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_FORWARD_ADDRESS_DELETE - <i>Unable to perform MailForward DELETE operation.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * MBX_UNABLE_TO_GET_GROUP_MALIBOX_DN - <i>Invalid forwarding address.</i>
     * MBX_INVALID_FORWARDING_ADDRESS - <i>Invalid forwarding address.</i>
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
