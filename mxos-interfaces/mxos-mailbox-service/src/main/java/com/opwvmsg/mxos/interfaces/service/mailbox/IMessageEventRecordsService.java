/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * MessageEventRecords object of mail internal info operations interface which
 * will be exposed to the client. This interface is responsible for doing
 * MessageEventRecords related operations (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IMessageEventRecordsService {

    /**
     * This operation is responsible for reading mail internal info.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * </pre>
	 * 
	 * @return returns MessageEventRecords POJO.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 * <b>Error Codes:</b>
	 *
	 * MBX_UNABLE_TO_GET_MSG_EVENT_RECORDS - <i>Unable to perform MessageEventRecords GET operation</i>
	 * MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 * MBX_NOT_FOUND - <i>The given email does not exist</i>
	 * 
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    MessageEventRecords read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating message event records.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 *
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * <b>inputParams-Optional:</b>
	 *
	 * <b>eventRecordingEnabled</b>
	 *     <i>Specifies whether the Message Event Recording System (MERS) used by the Openwave Notification Manager application is enabled. Allowed values: Always|True|False|Never</i>
	 *     Type: String, ENUM ["always", "true", "false", "never"]
	 *     Example: always
	 * 
	 * <b>maxHeaderLength</b>
	 *     <i>Specifies the maximum header size of event messages in the Message Event Recording System (MERS)</i>
	 *     Type: integer, Minimum : 0, Maximum : 100
	 *     Example: 1
	 *     
	 * <b>maxFilesSizeKB</b>
	 *     <i>Specifies the size, in kilobytes, of the Message Event Recording System (MERS) file</i>
	 *     Type: integer, Minimum : 0, Maximum : 100
	 *     Example: 1
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>
	 * <b>Error Codes:</b>
	 *
	 *     MBX_INVALID_EMAIL - <i>Invalid email format</i>
	 *     MBX_NOT_FOUND - <i>The given email does not exist</i>
	 *     MBX_INVALID_EVENT_RECORDING_ENABLED - <i>Invalid eventRecordingEnabled</i>
	 *     MBX_INVALID_MAX_HEADER_LENGTH - <i>Invalid maxHeaderLength</i>
	 *     MBX_INVALID_MAX_FILES_SIZE - <i>Invalid maxFilesSizeKB</i>
	 *     MBX_UNABLE_TO_SET_EVENT_RECORDING_ENABLED - <i>Unable to set eventRecordingEnabled</i>
	 *     MBX_UNABLE_TO_SET_MAX_HEADER_LENGTH - <i>Unable to set maxHeaderLength</i>
	 *     MBX_UNABLE_TO_SET_MAX_FILES_SIZE - <i>Unable to set maxFilesSizeKB</i>
	 * 
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
