/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * COS SMS Online Service operations interface which will be exposed to the
 * client. This interface is responsible for doing cos SMS Online Service
 * related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ICosSmsOnlineService {

    /**
     * This operation is responsible for reading SMS Online Service COS.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     * </pre>
     *  
     * @return returns SmsOnline object of COS.
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * COS_INVALID_COSID - <i>Invalid cosId.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    SmsOnline read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating SMS Online Service.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>cosId</b>
     *     <i>Specifies Distinguished name of the class of service.</i>
     *     Type: String, Maximum Length: 30
     *     Regex Pattern: "^([a-zA-Z0-9._,=]+)$"
     *     Example: class1
     *     
     * <b>inputParams-Optional:</b>
     * 
     * <b>internationalSMSAllowed</b>
     *     <i>Specifies if international SMS is allowed.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>maxSMSPerDay</b>
     *     <i>Max number of Online SMS Allowed per user.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 1, Maximum: 2147483647
     * <b>concatenatedSMSAllowed</b>
     *     <i>Specifies if concatenated SMS allowed.</i>
     *     Type: String, Empty Allowed: No, Enum: "no", "yes"
     * <b>maxConcatenatedSMSSegments</b>
     *     <i>Max number of concatenated SMS segments per SMS.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 0, Maximum: 256
     * <b>maxPerCaptchaSMS</b>
     *     <i>Max number of messages before CAPTCHA is presented.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 0, Maximum: 2147483647
     * <b>maxPerCaptchaDurationMins</b>
     *     <i>Max duration (minutes) before CAPTCHA is presented.</i>
     *     Type: Integer, Empty Allowed: No, Minimum: 0, Maximum: 256
     * </pre>
     *  
     * @throws MxOSException MxOSException.
     * 
     * <pre>    
     * <b>Error Codes:</b>
     * 
     * "MBX_SMSSERVICES_UPDATE_MISSING_PARAMS - <i>One or more mandatory parameters are missing for SmsServices POST operation.</i>
     * "MBX_INVALID_INTERNATIONAL_SMS_ALLOWED - <i>Invalid international sms allowed.</i>
     * "MBX_UNABLE_TO_SET_INTERNATIONAL_SMS_ALLOWED - <i>Unable to set internationalSMSAllowed.</i>
     * "MBX_INVALID_MAX_SMS_PER_DAY - <i>Invalid maximum sms per day.</i>
     * "MBX_UNABLE_TO_SET_MAX_SMS_PER_DAY - <i>Unable to set maxSMSPerDay.</i>
     * "MBX_INVALID_CONCATENATED_SMS_ALLOWED - <i>Invalid concatenated sms allowed.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
