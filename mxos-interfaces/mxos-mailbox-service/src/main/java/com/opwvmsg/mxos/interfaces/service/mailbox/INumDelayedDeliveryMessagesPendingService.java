/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Delivery messages pending operations interface which will be exposed to the
 * client. This interface is responsible for doing delayed delivery messages
 * pending related operations (like Create, Read, Update, Delete, etc.).
 * 
 * @author mxos-dev
 */
public interface INumDelayedDeliveryMessagesPendingService {

    /**
     * This operation is responsible for creating delayed delivery messages
     * pending.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>numDelayedDeliveryMessagesPending</b>
     *     <i>The Number of messages awaiting future delivery for a user, Format - Time,MessageId</i>
     *     Type: string, Maximum Length: 255
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid delayed delivery messages pending.</i>
     * MBX_INVALID_EMAIL - <i>Invalid allowed format.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading delayed delivery messages
     * pending.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>.
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of delayed delivery messages pending.
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid allowed format.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<String> read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating allowed domain.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>oldNumDelayedDeliveryMessagesPending</b>
     *     <i>Old Number of messages awaiting future delivery for a user, Format - Time,MessageId</i>
     *     Type: string, Maximum Length: 255 
     * <b>newNumDelayedDeliveryMessagesPending</b>
     *     <i>New Number of messages awaiting future delivery for a user, Format - Time,MessageId</i>
     *     Type: string, Maximum Length: 255
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_OLD_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid old delayed delivery messages pending.</i>
     * MBX_INVALID_NEW_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid new delayed delivery messages pending.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting allowed domain.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>numDelayedDeliveryMessagesPending</b>
     *     <i>The Number of messages awaiting future delivery for a user, Format - Time,MessageId</i>
     *     Type: string, Maximum Length: 255
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_NUM_DELAYED_DELIVERY_MESSAGES_PENDING - <i>Invalid delayed delivery messages pending.</i>
     * MBX_INVALID_EMAIL - <i>Invalid email format.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
