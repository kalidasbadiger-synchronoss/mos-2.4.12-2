/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * External MailAccount interface which will be exposed to the client. This
 * interface is responsible for doing External MailAccount related operations
 * (like Create, Read, Update, Delete, etc.).
 *
 * @author mxos-dev
 */
public interface IExternalMailAccountService {

    /**
     * This operation is responsible for creating External MailAccount.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>accountId</b>
     *     <i>AccountId of the mailAccount</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * 
     * <b>inputParams-Optional:</b>
     * <b>accountName</b>
     *     <i>Specifies the text description of the account associated with the external server.</i>
     *     Type: String, Maximum Length:: 129
     * <b>accountType</b>
     *     <i>Specifies the type of external mail server.</i>
     *     Type: String, Enum: "pop3", "pop3ssl", "imap"
     * <b>serverAddress</b>
     *     <i>Specifies the name of the external mail server that contains the account.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>serverPort</b>
     *     <i>Specifies the port number of the external mail server.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>timeoutSecs</b>
     *     <i>Specifies the subscriber-preferred amount of time, in seconds, that the application waits for a response from the external mail server.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>emailAddress</b>
     *     <i>Specifies the SMTP address of the external mail server account.</i>
     * <b>accountUserName</b>
     *     <i>Specifies the name of the account on the external mail server.</i>
     *     Type: String, Empty Allowed: yes, Maximum Length: 129
     * <b>accountPassword</b>
     *     <i>Specifies the password, in the format identified by the password encryption type, for the external mail server account.</i>
     *     Type: String
     *     Regex Pattern: ^({0,})|[a-zA-Z0-9+/=-]{1,64}$
     * <b>displayImage</b>
     *     <i>Specifies a number corresponding to the subscriber-preferred image that the interface displays to indicate that the message was fetched from a particular external account.</i>
     *     Type: String, Maximum Length: 225
     * <b>fetchMailsEnabled</b>
     *     <i>Specifies, using a boolean value, whether mail in the remote account is downloaded (true) or viewed (false).(Rich Mail only).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>autoFetchMails</b>
     *     <i>Specifies the subscriber-preferred option as to whether (true) or not (false).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>fetchedMailsFolderName</b>
     *     <i>Specifies the subscriber-preferred folder in which messages from the external mail server account are placed after being downloaded.</i>
     *     Type: String, Maximum Length: 225
     * <b>leaveEmailsOnServer</b>
     *     <i>Specifies the subscriber-preferred option of whether messages in the external mail server account are deleted (true) or saved (false) after they are retrieved.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>fromAddressInReply</b>
     *     <i>Specifies the subscriber-preferred email address placed in the From message header when replying to messages retrieved from the external mail.</i>
     *     Type: String, Maximum Length: 225
     *     Regex Pattern:^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$
     * <b>mailSendEnabled</b>
     *     <i>Specifies, using a boolean value, whether (true) or not (false) outgoing mail is enabled from this account.(Rich Mail only)</i>
     *     Type: String, Enum: "no", "yes"
     * <b>mailSendSMTPAddress</b>
     *     <i>Specifies the hostname of the external SMTP server.(Rich Mail only)</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>mailSendSMTPPort</b>
     *     <i>Specifies the port number of external SMTP server.(Rich Mail only)</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>smtpSSLEnabled</b>
     *     <i>Specifies whether the external SMTP server uses the SSL protocol (SMTPS) or not (SMTP).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>useSmtpLogin</b>
     *     <i>Does outgoing server require authentication (login and password).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>smtpLogin</b>
     *     <i>SMTP Login to be used for authentication.</i>
     *     Type: String, Empty Allowed: yes, Maximum Length: 129
     * <b>smtpPassword</b>
     *     <i>SMTP Password to be used for authentication.</i>
     *     Type: String
     *     Regex Pattern: ^({0,})|[a-zA-Z0-9+/=-]{1,64}$
     * <b>sendFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>draftsFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>trashFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>spamFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225                        
     * </pre> 
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_SET_MAIL_ACCOUNTS - <i>Unable to set MailAccounts</i>
     * MBX_INVALID_ACCOUNT_ID - <i>Invalid Account Id of MailAccount</i>
     * MBX_MAIL_ACCOUNT_NOT_FOUND - <i>The given MailAccount does not exist</i>
     * MBX_UNABLE_TO_PUT_MAIL_ACCOUNT - <i>Unable to put MailAccounts</i>
     * MBX_INVALID_ACCOUNT_NAME - <i>Invalid Account Name</i>
     * MBX_INVALID_ACCOUNT_TYPE - <i>Invalid Account Type</i>
     * MBX_INVALID_SERVER_ADDRESS - <i>Invalid Server Address</i>
     * MBX_INVALID_SERVER_PORT - <i>Invalid Server Port</i>
     * MBX_INVALID_TIMEOUT_SEC - <i>Invalid TimeOut in Seconds</i>
     * MBX_INVALID_EMAIL_ADDRESS - <i>Invalid Email Address</i>
     * MBX_INVALID_ACCOUNT_USERNAME - <i>Invalid Account Username</i>
     * MBX_INVALID_ACCOUNT_PASSWORD - <i>Invalid Account Password</i>
     * MBX_INVALID_DISPLAY_IMAGE - <i>Invalid Display Image</i>
     * MBX_INVALID_FETCH_MAILS_ENABLED - <i>Invalid Fetch Mails Enabled</i>
     * MBX_INVALID_AUTO_FETCH_MAILS - <i>Invalid Auto Fetch Mails</i>
     * MBX_INVALID_FETCHED_MAILS_FOLDER_NAME - <i>Invalid Fetched Folders Name</i>
     * MBX_INVALID_LEAVE_EMAIL_ON_SERVER - <i>Invalid Leave Email on Server</i>
     * MBX_INVALID_FROM_ADDRESS_IN_REPLY - <i>Invalid From Address in Reply</i>
     * MBX_INVALID_MAIL_SEND_ENABLED - <i>Invalid Mail Send Enabled</i>
     * MBX_INVALID_MAIL_SEND_SMTP_ADDRESS - <i>Invalid Mail Send SMTP Address</i>
     * MBX_INVALID_MAIL_SEND_SMTP_PORT - <i>Invalid Mail Send SMTP Port</i>
     * MBX_INVALID_SMTP_SSL_ENABLED - <i>Invalid SMTP SSL Enabled</i>
     * MBX_INVALID_USE_SMTP_LOGIN - <i>Invalid Use SMTP Login</i>
     * MBX_INVALID_SMTP_LOGIN - <i>Invalid SMTP Login</i>
     * MBX_INVALID_SMTP_PASSWORD - <i>Invalid SMTP Password</i>
     * MBX_INVALID_SEND_FOLDER_NAME - <i>Invalid Send Folder Name</i>
     * MBX_INVALID_DRAFTS_FOLDER_NAME - <i>Invalid Drafts Folder Name</i>
     * MBX_INVALID_TRASH_FOLDER_NAME - <i>Invalid Trash Folder Name</i>
     * MBX_INVALID_SPAM_FOLDER_NAME - <i>Invalid Spam Folder Name</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void create(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for reading External MailAccount.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>.
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return List of MailAccount objects of ExternalAccounts.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * MBX_UNABLE_TO_GET_EXTERNAL_ACCOUNTS - <i>Unable to get Externalaccounts</i>
     * MBX_INVALID_ACCOUNT_ID - <i>Invalid Account Id of MailAccount</i>
     * MBX_MAIL_ACCOUNT_NOT_FOUND - <i>The given MailAccount does not exist</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<MailAccount> read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating Social Network Site.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>accountId</b>
     *     <i>AccountId of the mailAccount</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>accountName</b>
     *     <i>Specifies the text description of the account associated with the external server.</i>
     *     Type: String, Maximum Length:: 129
     * <b>accountType</b>
     *     <i>Specifies the type of external mail server.</i>
     *     Type: String, Enum: "pop3", "pop3ssl", "imap"
     * <b>serverAddress</b>
     *     <i>Specifies the name of the external mail server that contains the account.</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>serverPort</b>
     *     <i>Specifies the port number of the external mail server.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>timeoutSecs</b>
     *     <i>Specifies the subscriber-preferred amount of time, in seconds, that the application waits for a response from the external mail server.</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>emailAddress</b>
     *     <i>Specifies the SMTP address of the external mail server account.</i>
     * <b>accountUserName</b>
     *     <i>Specifies the name of the account on the external mail server.</i>
     *     Type: String, Empty Allowed: yes, Maximum Length: 129
     * <b>accountPassword</b>
     *     <i>Specifies the password, in the format identified by the password encryption type, for the external mail server account.</i>
     *     Type: String
     *     Regex Pattern: ^({0,})|[a-zA-Z0-9+/=-]{1,64}$
     * <b>displayImage</b>
     *     <i>Specifies a number corresponding to the subscriber-preferred image that the interface displays to indicate that the message was fetched from a particular external account.</i>
     *     Type: String, Maximum Length: 225
     * <b>fetchMailsEnabled</b>
     *     <i>Specifies, using a boolean value, whether mail in the remote account is downloaded (true) or viewed (false).(Rich Mail only).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>autoFetchMails</b>
     *     <i>Specifies the subscriber-preferred option as to whether (true) or not (false).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>fetchedMailsFolderName</b>
     *     <i>Specifies the subscriber-preferred folder in which messages from the external mail server account are placed after being downloaded.</i>
     *     Type: String, Maximum Length: 225
     * <b>leaveEmailsOnServer</b>
     *     <i>Specifies the subscriber-preferred option of whether messages in the external mail server account are deleted (true) or saved (false) after they are retrieved.</i>
     *     Type: String, Enum: "no", "yes"
     * <b>fromAddressInReply</b>
     *     <i>Specifies the subscriber-preferred email address placed in the From message header when replying to messages retrieved from the external mail.</i>
     *     Type: String, Maximum Length: 225
     *     Regex Pattern:^({0,})|(([a-zA-Z0-9]+[._-]?){1,}@(([a-zA-Z0-9]+[-]?){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{2,4})$
     * <b>mailSendEnabled</b>
     *     <i>Specifies, using a boolean value, whether (true) or not (false) outgoing mail is enabled from this account.(Rich Mail only)</i>
     *     Type: String, Enum: "no", "yes"
     * <b>mailSendSMTPAddress</b>
     *     <i>Specifies the hostname of the external SMTP server.(Rich Mail only)</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: ^|([a-zA-Z]+[a-zA-Z0-9\\.-]*)|((([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(/([0-9]|[1-2][0-9]|3[0-2]))?)|((([0-9]|[a-f]|[A-F]){1,4}\\:){7}([0-9]|[a-f]|[A-F]){1,4})$
     * <b>mailSendSMTPPort</b>
     *     <i>Specifies the port number of external SMTP server.(Rich Mail only)</i>
     *     Type: Integer, Minimum: 0, maximum: 2147483647
     * <b>smtpSSLEnabled</b>
     *     <i>Specifies whether the external SMTP server uses the SSL protocol (SMTPS) or not (SMTP).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>useSmtpLogin</b>
     *     <i>Does outgoing server require authentication (login and password).</i>
     *     Type: String, Enum: "no", "yes"
     * <b>smtpLogin</b>
     *     <i>SMTP Login to be used for authentication.</i>
     *     Type: String, Empty Allowed: yes, Maximum Length: 129
     * <b>smtpPassword</b>
     *     <i>SMTP Password to be used for authentication.</i>
     *     Type: String
     *     Regex Pattern: ^({0,})|[a-zA-Z0-9+/=-]{1,64}$
     * <b>sendFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>draftsFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>trashFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225
     * <b>spamFolderName</b>
     *     <i></i>
     *     Type: String, Maximum Length: 225    
     * </pre> 
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_UNABLE_TO_SET_MAIL_ACCOUNTS - <i>Unable to set MailAccounts</i>
     * MBX_INVALID_ACCOUNT_ID - <i>Invalid Account Id of MailAccount</i>
     * MBX_MAIL_ACCOUNT_NOT_FOUND - <i>The given MailAccount does not exist</i>
     * MBX_UNABLE_TO_PUT_MAIL_ACCOUNT - <i>Unable to put MailAccounts</i>
     * MBX_INVALID_ACCOUNT_NAME - <i>Invalid Account Name</i>
     * MBX_INVALID_ACCOUNT_TYPE - <i>Invalid Account Type</i>
     * MBX_INVALID_SERVER_ADDRESS - <i>Invalid Server Address</i>
     * MBX_INVALID_SERVER_PORT - <i>Invalid Server Port</i>
     * MBX_INVALID_TIMEOUT_SEC - <i>Invalid TimeOut in Seconds</i>
     * MBX_INVALID_EMAIL_ADDRESS - <i>Invalid Email Address</i>
     * MBX_INVALID_ACCOUNT_USERNAME - <i>Invalid Account Username</i>
     * MBX_INVALID_ACCOUNT_PASSWORD - <i>Invalid Account Password</i>
     * MBX_INVALID_DISPLAY_IMAGE - <i>Invalid Display Image</i>
     * MBX_INVALID_FETCH_MAILS_ENABLED - <i>Invalid Fetch Mails Enabled</i>
     * MBX_INVALID_AUTO_FETCH_MAILS - <i>Invalid Auto Fetch Mails</i>
     * MBX_INVALID_FETCHED_MAILS_FOLDER_NAME - <i>Invalid Fetched Folders Name</i>
     * MBX_INVALID_LEAVE_EMAIL_ON_SERVER - <i>Invalid Leave Email on Server</i>
     * MBX_INVALID_FROM_ADDRESS_IN_REPLY - <i>Invalid From Address in Reply</i>
     * MBX_INVALID_MAIL_SEND_ENABLED - <i>Invalid Mail Send Enabled</i>
     * MBX_INVALID_MAIL_SEND_SMTP_ADDRESS - <i>Invalid Mail Send SMTP Address</i>
     * MBX_INVALID_MAIL_SEND_SMTP_PORT - <i>Invalid Mail Send SMTP Port</i>
     * MBX_INVALID_SMTP_SSL_ENABLED - <i>Invalid SMTP SSL Enabled</i>
     * MBX_INVALID_USE_SMTP_LOGIN - <i>Invalid Use SMTP Login</i>
     * MBX_INVALID_SMTP_LOGIN - <i>Invalid SMTP Login</i>
     * MBX_INVALID_SMTP_PASSWORD - <i>Invalid SMTP Password</i>
     * MBX_INVALID_SEND_FOLDER_NAME - <i>Invalid Send Folder Name</i>
     * MBX_INVALID_DRAFTS_FOLDER_NAME - <i>Invalid Drafts Folder Name</i>
     * MBX_INVALID_TRASH_FOLDER_NAME - <i>Invalid Trash Folder Name</i>
     * MBX_INVALID_SPAM_FOLDER_NAME - <i>Invalid Spam Folder Name</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for deleting Social Network Site.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * <b>accountId</b>
     *     <i>AccountId of the mailAccount</i>
     *     Type: Integer, Minimum: 0, Maximum: 2147483647
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format</i>
     * MBX_INVALID_ACCOUNT_ID - <i>Invalid Account Id of MailAccount</i>
     * MBX_MAIL_ACCOUNT_NOT_FOUND - <i>The given MailAccount does not exist</i>
     * MBX_UNABLE_TO_DELETE_MAIL_ACCOUNT - <i>Unable to delete MailAccounts</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void delete(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
