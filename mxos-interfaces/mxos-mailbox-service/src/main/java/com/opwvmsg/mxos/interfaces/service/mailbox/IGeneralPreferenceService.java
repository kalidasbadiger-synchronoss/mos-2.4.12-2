/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * GeneralPreference interface which will be exposed to the client. This
 * interface is responsible for doing general preference related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IGeneralPreferenceService {

    /**
     * This operation is responsible for reading general preferences.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * </pre>
     * 
     * @return GeneralPreferences POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_GENERAL_PREFERENCES_GET_MISSING_PARAMS - <i>One or more mandatory parameters are missing for GeneralPreferences GET operation.</i>
     * MBX_GENERAL_PREFERENCES_UNABLE_TO_GET - <i>Unable to perform GeneralPreferences GET operation.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    GeneralPreferences read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating general preferences.
     * 
     * @param inputParams
     *     Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: "^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: abc@openwave.com
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>locale</b>
     *     <i>Valid supported locale.</i>
     *     Type: String, Maximum Length: 6
     * <b>charset</b>
     *     <i>Valid international Character Set.</i>
     *     Type: String, Maximum Length: 257
     * <b>parentalControlEnabled</b>
     *     <i>Enables parental control for an account, allowed value- yes or no.</i>
     *     Type: String
     * <b>preferredTheme</b>
     *     <i>Specifies the subscriber-preferred application variant.</i>
     *     Type: String, Maximum Length: 256
     * <b>preferredUserExperience</b>
     *     <i>Specifies the preferred Web mail application.</i>
     *     Type: String, Enum: "basic", "rich", "mobile", "tablet"
     * <b>recycleBinEnabled</b>
     *     <i>Indicates whether to places deleted messages into the Trash folder 
    or permanently deletes them immediately, allowed value - yes or no</i>
           Type: String
     * <b>timezone</b>
     *     <i>Valid timezone in unix format</i>
     *     Type: String, Maximum Length: 255
     * <b>userThemesAvailable</b>
     *     <i>Specifies application variant.</i>
     *     Type: String, Maximum Length: 256
     * <b>webmailMTA</b>
     *     <i>Specifies the name of the MTA the system uses for the subscriber mail transport-related operations, max-length=20</i>
     *     Type: String
     * </pre>
     *     
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * MBX_GENERAL_PREFERENCES_UPDATE_MISSING_PARAMS - <i>One or more mandatory parameters are missing for GeneralPreferences POST operation.</i>
     * MBX_UNABLE_TO_SET_LOCALE - <i>Unable to perform set locale.</i>
     * MBX_INVALID_CHARSET - <i>Unable to perform set charset, May occur due to Invalid charset in request.</i>
     * MBX_INVALID_TIMEZONE - <i>Unable to perform set timezone, May occur due to Invalid timezone in request.</i>
     * MBX_INVALID_PARENTALCONTROL - <i>Unable to perform set parental control,Invalid parentalControl value, supports either yes or no.</i>
     * MBX_INVALID_PREFERRED_THEME - <i>Unable to perform set preferred Theme, May occur due to Invalid preferredTheme in request.</i>
     * MBX_INVALID_PREFERRED_USER_EXPERIENCE - <i>Unable to perform set preferred UserExperience, May occur due to Invalid preferred UserExperience in request.</i>
     * MBX_INVALID_RECYCLEBIN_ENABLED - <i>Unable to perform set recycleBinTheme, Invalid recycleBinTheme, supports either yes or no.</i>
     * MBX_INVALID_WEBMAILMTA - <i>Unable to perform set webmailMTA, May occur due to Invalid webmailMTA in request.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
