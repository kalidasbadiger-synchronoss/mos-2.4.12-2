/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.SmsOnline;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox SMS Online Service operations interface which will be exposed to the
 * client. This interface is responsible for doing mailbox SMS Online Service
 * related operations (like Read, Update etc.).
 *
 * @author mxos-dev
 */
public interface ISmsOnlineService {

    /**
     * This operation is responsible for reading SMS Online Service.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * </pre>
	 *  
	 * @return returns SmsOnline object of Mailbox.
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_INVALID_EMAIL - <i>nvalid email format</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    SmsOnline read(final Map<String, List<String>> inputParams)
        throws MxOSException;

    /**
     * This operation is responsible for updating SMS Online Service.
     * 
     * @param inputParams
	 *     Parameters given by user.
	 * 
	 * <pre>
	 * <b>inputParams-Mandatory:</b>
	 * 
	 * <b>email</b>
	 *     <i>Subscriber's email address or emailAlias</i>
	 *     Type: String, Maximum Length: 255
	 *     Regex Pattern: 
	 *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
	 *     Example: bs000123@openwave.com
	 * 
	 * 
	 * <b>inputParams-Optional:</b>
	 * 
	 * <b>smsOnlineEnabled</b>
	 *     <i>Allowed values- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * 
	 * <b>internationalSMSAllowed</b>
	 *     <i>Allowed values- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * 
	 * <b>internationalSMSEnabled</b>
	 *     <i>Allowed values- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 *     
	 * <b>maxSMSPerDay</b>
	 *     <i>Max number of Online SMS Allowed per user</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 2
	 *     
	 * <b>concatenatedSMSAllowed</b>
	 *     <i>Allowed values- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * 
	 * <b>concatenatedSMSEnabled</b>
	 *     <i>Allowed values- yes or no</i>
	 *     Type: String, ENUM ["no", "yes"]
	 *     Example: yes
	 * 
	 * <b>maxConcatenatedSMSSegments</b>
	 *     <i>Max number of concatenated SMS segments per SMS</i>
	 *     Type: integer, Minimum : 0, Maximum : 256
	 *     Example: 2
	 *     
	 * <b>maxPerCaptchaSMS</b>
	 *     <i>Max number of messages before CAPTCHA is presented</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 2
	 * 
	 * <b>maxPerCaptchaDurationMins</b>
	 *     <i>Max duration (minutes) before CAPTCHA is presented</i>
	 *     Type: integer, Minimum : 0, Maximum : 2147483647
	 *     Example: 2
	 * </pre>
	 *  
	 * @throws MxOSException MxOSException.
	 * 
	 * <pre>	
	 * <b>Error Codes:</b>
	 * 
	 * MBX_SMSSERVICES_UPDATE_MISSING_PARAMS - <i>One or more mandatory parameters are missing for SmsServices POST operation</i>
	 * MBX_INVALID_SMS_ONLINE_ENABLED - <i>Invalid sms online enabled</i>
	 * MBX_UNABLE_TO_SET_SMS_ONLINE_ENABLED - <i>Unable to set smsOnlineEnabled</i>
	 * MBX_INVALID_INTERNATIONAL_SMS_ALLOWED - <i>Invalid international sms allowed</i>
	 * MBX_UNABLE_TO_SET_INTERNATIONAL_SMS_ALLOWED - <i>Unable to set internationalSMSAllowed</i>
	 * MBX_INVALID_INTERNALTONAL_SMS_ENABLED - <i>Invalid international sms enabled</i>
	 * MBX_UNABLE_TO_SET_INTERNALTONAL_SMS_ENABLED - <i>Unable to set internationalSMSEnabled</i>
	 * MBX_INVALID_MAX_SMS_PER_DAY - <i>Invalid maximum sms per day</i>
	 * MBX_UNABLE_TO_SET_MAX_SMS_PER_DAY - <i>Unable to set maxSMSPerDay</i>
	 * MBX_INVALID_CONCATENATED_SMS_ALLOWED - <i>Invalid concatenated sms allowed</i>
	 * MBX_UNABLE_TO_SET_CONCATENATED_SMS_ALLOWED - <i>Unable to set concatenatedSMSAllowed</i>
	 * MBX_INVALID_CONCATENATED_SMS_ENABLED - <i>Invalid concatenated sms enabled</i>
	 * MBX_UNABLE_TO_SET_CONCATENATED_SMS_ENABLED - <i>Unable to set concatenatedSMSEnabled</i>
	 * MBX_INVALID_MAX_CONCATENATED_SMS_SEGMENTS - <i>Invalid maximum concatenated sms segments</i>
	 * MBX_UNABLE_TO_SET_MAX_CONCATENATED_SMS_SEGMENTS - <i>Unable to set maxConcatenatedSMSSegments</i>
	 * MBX_INVALID_MAX_PER_CAPTCHA_SMS - <i>Invalid maximum per capcha sms</i>
	 * MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_SMS - <i>Unable to set maxPerCaptchaSMS</i>
	 * MBX_INVALID_MAX_PER_CAPTCHA_DURATION_MINS - <i>Invalid maximum per captcha duration minutes</i>
	 * MBX_UNABLE_TO_SET_MAX_PER_CAPTCHA_DURATION_MINS - <i>Unable to set maxPerCaptchaDurationMins</i>
	 *  
	 * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
	 * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
