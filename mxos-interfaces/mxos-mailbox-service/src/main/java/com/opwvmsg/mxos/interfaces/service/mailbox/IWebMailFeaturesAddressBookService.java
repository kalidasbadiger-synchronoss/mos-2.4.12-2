/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.AddressBookFeatures;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * AddressBook interface which will be exposed to the client. This
 * interface is responsible for doing AddressBook preferences related operations
 * (like Read, Update, etc.).
 *
 * @author mxos-dev
 */
public interface IWebMailFeaturesAddressBookService {

    /**
     * This operation is responsible for reading AddressBookFeatures preferences.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: 
     *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns AddressBookFeatures POJO.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_INVALID_EMAIL - <i>Invalid email format</i>
     * MBX_NOT_FOUND - <i>The given email does not exist</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    AddressBookFeatures read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating AddressBookFeatures preferences.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>email</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 255
     *     Regex Pattern: 
     *     	"^[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)*@(([a-zA-Z0-9]*[-]*){1,}[a-zA-Z0-9]+\\.){1,}[a-zA-Z]{1,4}$"
     *     Example: bs000123@openwave.com
     * 
     * 
     * <b>inputParams-Optional:</b>
     * 
     * <b>maxContacts</b>
     *     <i>Max number of address book entries for a user</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 200
     *     
     * <b>maxGroups</b>
     *     <i>Specifies the maximum number of address book groups that a subscriber may define. </i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 20
     *     
     * <b>maxContactsPerGroup</b>
     *     <i>Specifies the maximum number of email addresses allowed in a single address book entry.</i>
     *     Type: integer, Minimum : 0, Maximum : 2147483647
     *     Example: 20
     *     
     * <b>maxContactsPerPage</b>
     *     <i>Specifies the subscriber-preferred number of address book entries that the web interface displays on a page.</i>
     *     Type: integer, Minimum : 1, Maximum : 2147483647
     *     Example: 50
     *     
     * <b>createContactsFromOutgoingEmails</b>
     *     <i>Specifies the subscriber-preferred method by which the web and WAP interfaces collects recipient email addresses from outgoing messages and adds them to the personal address book.</i>
     *     Type: String, ENUM ["disabled", "forAllAddresses", "onlyForAddressesInToField"]
     *     Example: disabled
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * 
     * MBX_NOT_FOUND - <i>The given email does not exist</i>
     * MBX_UNABLE_TO_SET_MAX_CONTACTS - <i>Unable to update maxContacts</i>
     * MBX_UNABLE_TO_SET_MAX_GROUPS - <i>Unable to update maxGroups</i>
     * MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_GROUP - <i>Unable to update maxContactsPerGroup</i>
     * MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_PAGE - <i>Unable to update maxContactsPerPage</i>
     * MBX_UNABLE_TO_SET_CREATE_CONTACTS_FROM_OUTGOING_EMAILS - <i>Unable to update createContactsFromOutgoingEmails</i>
     * MBX_INVALID_MAX_CONTACTS - <i>Invalid value for maxContacts</i>
     * MBX_INVALID_MAX_GROUPS - <i>Invalid value for maxGroups</i>
     * MBX_INVALID_MAX_CONTACTS_PER_GROUP - <i>Invalid value for maxContactsPerGroup</i>
     * MBX_INVALID_MAX_CONTACTS_PER_PAGE - <i>Invalid value for maxContactsPerPage</i>
     * MBX_INVALID_CREATE_CONTACTS_FROM_OUTGOING_EMAILS - <i>Invalid value for createContactsFromOutgoingEmails</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void update(final Map<String, List<String>> inputParams)
        throws MxOSException;
}
