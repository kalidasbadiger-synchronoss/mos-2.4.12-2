/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.saml;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Saml operations interface which will be exposed to the client. This
 * interface is responsible for saml related operations.
 * 
 * @author mxos-dev
 */

public interface ISamlService {

    /**
     * This operation is responsible to get SAML request.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>acsURL</b>
     *     <i>assertion consumer service URL</i>
     *     Type: String
     * </pre>
     * 
     * @return SAMLRequest which is base64Compressed or clear.
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes:</b>
     * SAML_INVALID_ACS_URL - <i>Invalid SAML assertion consumer service URL.</i>
     * SAML_UNABLE_TO_CREATE_AUTHENTICATION_REQUEST - <i>Unable to create authentication request.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    String getSAMLRequest(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for decoding SAML response from an Identity Provider and check for validity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams-Mandatory:</b>
     * 
     * <b>samlAssertion</b>
     *     <i>SAML assertion received from IDP</i>
     *     Type: String
     * </pre>
     * 
     * @return Map<String, String> map of saml attributes as configured.
     * @throws MxOSException MxOSException.
     * 
     * <pre> 
     * <b>Error Codes:</b>
     * SAML_INVALID_ASSERTION - <i>Invalid SAML Assertion.</i>
     * SAML_UNABLE_TO_DECODE_SAML_ASSERTION - <i>Unable to decode SAML Assertion.</i>
     * SAML_ASSERTION_AUTHENTICATION_FAILURE - <i>SAML authentication failed, assertion did not contain the status code for success.</i>
     * SAML_ASSERTIONS_NOT_FOUND - <i>The SAML response contains no assertions.</i>
     * SAML_ASSERTION_EXPIRED - <i>The SAML assertion has expired.</i>
     * SAML_ASSERTION_NOT_YET_VALID - <i>The SAML assertion is not yet valid.</i>
     * SAML_ASSERTION_INVALID_ISSUER - <i>The Virgin Media assertion consumer service cannot validate SAML issued by another authority.</i>
     * SAML_ASSERTION_INVALID_DIGITAL_SIGNATURE - <i>Unable to validate the digital signature or its profile.</i>
     * GEN_BAD_REQUEST - <i>Bad request, please check the request and parameters.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    Map<String, String> decodeSAMLAssertion(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
