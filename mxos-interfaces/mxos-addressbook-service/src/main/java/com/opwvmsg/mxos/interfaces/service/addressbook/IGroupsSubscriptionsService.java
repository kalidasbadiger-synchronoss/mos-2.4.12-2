/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.SubscriptionLink;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Groups Subscriptions Object level operations like Create, Read
 * and Delete.
 * 
 * @author mxos-dev
 */
public interface IGroupsSubscriptionsService {

    /**
     * This operation is responsible for creating group subscriptions. It uses
     * one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @return returns unique identifier for group subscriptions i.e.
     *         subscriptionId.
     * @throws MxOSException MxOSException.
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting group subscriptions. It uses
     * one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all group subscriptions. It
     * uses one or more actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading group subscription.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
	 * 
     * @return returns Group External Member POJO.
     * @throws MxOSException MxOSException.
     */
    SubscriptionLink read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading all the group subscription.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
	 * <b>To be supported in future release</b> 
	 * </pre>
	 * 
     * @return returns Group External Member POJO.
     * @throws MxOSException MxOSException.
     */
    List<SubscriptionLink> readAll(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
