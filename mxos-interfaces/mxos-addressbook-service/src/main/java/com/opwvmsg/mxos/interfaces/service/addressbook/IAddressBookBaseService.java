/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.AddressBookBase;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to AddressBook Base Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface IAddressBookBaseService {

    /**
     * This operation is responsible for searching AddressBook Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>To be supported in future release</b>
     * </pre>
     * 
     * @return returns list containing AddressBook Base POJO.
     * @throws MxOSException MxOSException.
     */
    List<AddressBookBase> list(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for reading AddressBook Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>To be supported in future release</b>
     * </pre>
     * 
     * @return returns AddressBookBase POJO.
     * @throws MxOSException MxOSException.
     */
    AddressBookBase read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating AddressBook Base.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>To be supported in future release</b>
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
