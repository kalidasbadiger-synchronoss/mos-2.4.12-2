/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.List;
import java.util.Map;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Group Object level operations like Create and Delete.
 * 
 * @author mxos-dev
 */
public interface IGroupsService {

    /**
     * This operation is responsible for creating a Group. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupName</b>
     *     <i>Group name</i>
     *     Type: String, Maximum Length: 128
     *     Example: John        
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc    
     * <b>folder</b> 
     *     <i>Open Xchange folder where the user should be created, default is private folder (22)</i>
     *     Type: integer, Minimum Val:0, Maximum Val:2147483647
     *     Example: 37
     * <b>displayName</b> 
     *     <i>Display name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: John
     * 
     * </pre>
     * 
     * @return returns unique identifier for group i.e. groupId.
     * @throws MxOSException MxOSException. *
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_DISPLAY_NAME - <i>Invalid group display name.</i>
     * ABS_INVALID_GROUP_NAME - <i>Invalid group name.</i>
     * ABS_INVALID_FIRST_NAME - <i>Invalid contact first name.</i>         * 
     * ABS_GROUP_UNABLE_TO_CREATE - <i>Error in creating a group.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     **/

    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for creating multiple Group. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupName</b>
     *     <i>Group name</i>
     *     Type: String, Maximum Length: 128
     *     Example: John        
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc    
     * <b>folder</b> 
     *     <i>Open Xchange folder where the user should be created, default is private folder (22)</i>
     *     Type: integer, Minimum Val:0, Maximum Val:2147483647
     *     Example: 37
     * <b>displayName</b> 
     *     <i>Display name as string value</i>
     *     Type: String, Maximum Length: 128 
     *     Example: John
     * 
     * </pre>
     * 
     * @return returns unique identifier for group i.e. groupId.
     * @throws MxOSException MxOSException. *
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_INVALID_DISPLAY_NAME - <i>Invalid group display name.</i>
     * ABS_INVALID_GROUP_NAME - <i>Invalid group name.</i>
     * ABS_INVALID_MULTIPLE_GROUPs - <i>Invalid multiple groups.</i>
     * ABS_INVALID_FIRST_NAME - <i>Invalid contact first name.</i>         * 
     * ABS_GROUP_UNABLE_TO_CREATE - <i>Error in creating a group.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     **/

    List<Long> createMultiple(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting group. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>groupId</b>
     *     <i>Group Id, which is returned after calling create group API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7  
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException. *
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_GROUP_UNABLE_TO_DELETE - <i>Error in deleting the group.</i>
     * ABS_GROUP_NOT_FOUND - <i>Group not found in the addressbook.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     **/

    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting all groups. It uses one or
     * more actions to do this activity.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *              *  
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException. *
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_GROUP_UNABLE_TO_DELETE - <i>Error in deleting the group.</i>
     *  
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     **/

    void deleteAll(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for moving all contacts to a folder. It
     * uses one or more actions to do this activity. This operation is only
     * specific for Open-Xchange backend.
     * 
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     * <b>contactId</b>
     *     <i>Contact Id, which is returned after calling create contact API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 7
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_CONTACT - <i>Invalid contact id.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * ABS_CONTACTS_MULTIPLE_UNABLE_TO_MOVE - <i>Error in moving multiple Contacts.</i>
     * ABS_INVALID_MULTIPLE_MOVE_CONTACTS - <i>Invalid contact ids to move.</i>
     * ABS_OX_ERROR - <i>Open-Xchange error.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void moveMultiple(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
