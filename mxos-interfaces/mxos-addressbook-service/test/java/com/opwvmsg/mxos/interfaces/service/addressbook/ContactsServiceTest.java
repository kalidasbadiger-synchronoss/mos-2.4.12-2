/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.addressbook;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.easymock.EasyMock;
import org.junit.Test;

import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Contact Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public class ContactsServiceTest {

    @Test
    public void testCreateContact() {
        Map<String, List<String>> inputParams = new HashMap<String, List<String>>();
        inputParams.put("test", new ArrayList<String>());
        inputParams.get("test").add("test");
        IContactsService contactsService = EasyMock.createMock(IContactsService.class);
        try {
            contactsService.create(inputParams);
        } catch (MxOSException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
