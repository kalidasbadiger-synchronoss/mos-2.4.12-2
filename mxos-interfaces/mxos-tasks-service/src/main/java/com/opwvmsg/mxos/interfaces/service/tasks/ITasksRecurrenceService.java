/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Recurrence;

/**
 * Interface to Tasks Base Object level operations like Read, Update.
 * 
 * @author mxos-dev
 */
public interface ITasksRecurrenceService {

	/**
     * This operation is responsible for read the task recurrence.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * @throws MxOSException MxOSException.
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * </pre>
     * 
     */
	
    Recurrence read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating Task Recurrence.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>type</b> 
     *     <i>Recurrence Type value - Enum values can be  (0- Nothing, 1 - daily, 2 - monthly, 3 - yearly)</i>
     *     Type: String
     *     
     * <b>daysInWeek</b> 
     *     <i>String array - Array of Enum with values Monday-Sunday</i>
     *     Type: String array
     *     Example: Monday-unday
     * <b>dayInMonth</b> 
     *     <i>Day in month to repeat the task</i>
     *     Type: Integer
     *     
     * <b>monthInYear</b> 
     *     <i>Month value in year to repeat the task</i>
     *     Type: Integer 
     *     
     * <b>recurAfterInterval</b> 
     *     <i>delay after which a new task is regenerated</i>
     *     Type: Integer
     *     Example: 5
     *     
     * <b>endDate</b> 
     *     <i>Date to indicate the Task end date</i>
     *     Type: Date
     *     Example: 2013-07-15T17:26:54Z
     *     
     * <b>notification</b> 
     *     <i>Boolean value to enable the notification</i>
     *     Type: Boolean
     *     Example: Yes/No
     * <b>occurrences</b> 
     *     <i>Number of times the task occurs</i>
     *     Type: Integer
     *     Example: 5
     *       
     * @throws MxOSException MxOSException.
     * 
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * TSK_TASKRECURRENCE_UNABLE_TO_UPDATE - <i>Error in updating task recurrence.</i>
     * TSK_INVALID_RECURRENCETYPE - <i>Error in updating task recurrence type.</i>
	 * TSK_INVALID_DAYSINWEEK - <i>Error in updating task recurrence days in week.</i>
	 * TSK_INVALID_DAYSINMONTH - <i>Error in updating task recurrence days in month.</i>
	 * TSK_INVALID_MONTHINYEAR - <i>Error in updating task recurrence month in year.</i>
	 * TSK_INVALID_RECURAFTERINTERVAL - <i>Error in updating task recurrence interval.</i>
	 * TSK_INVALID_ENDDATE - <i>Error in updating task recurrence end date.</i>
	 * TSK_INVALID_NOTIFICATION - <i>Error in updating task recurrence notification.</i>
	 * TSK_INVALID_OCCURRENCES - <i>Error in updating task recurrence occurrences.</i>
     * </pre>
     * 
     */
    
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
