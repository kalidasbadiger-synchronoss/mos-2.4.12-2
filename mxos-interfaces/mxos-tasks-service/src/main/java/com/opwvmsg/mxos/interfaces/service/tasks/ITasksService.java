/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Task;

/**
 * Interface to Tasks Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface ITasksService {

    /**
     * This operation is responsible for creating Tasks. It uses one or more
     * actions to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>name</b> 
     *     <i>Allowed Values - String values</i>
     *     Type: String
     *     Example: someTask
     *     
     * <b>owner</b> 
     *     <i>Allowed Values - Email address of the user who created the task</i>
     *     Type: Email Address
     *     Example: bs000123@openwave.com
     * <b>priority</b> 
     *     <i>Priority value which defines the task priority</i>
     *     Type: Enum - (0 - low, 1 - medium, 2 -high)
     *     
     * <b>status</b> 
     *     <i>Status value which defines the task status</i>
     *     Type: Enum - (0 - not started, 1 - in progress, 2 - done, 3 - waiting, 4 - deferred) 
     *     
     * <b>isPrivate</b> 
     *     <i>Allowed Values - "true", false"</i>
     *     Type: String
     *     Example: yes
     *     
     * <b>colorLabel</b> 
     *     <i>Any integer value which corresponds to color number(Can not be Empty)</i>
     *     Type: integer, Minimum Val: 1, Maximum Val: 10
     *     Example: 4
     *     
     * <b>categories</b> 
     *     <i>String containing comma separated categories. Order is preserved.</i>
     *     Type: String, Maximum Length: 128
     *     Example: Business
     * <b>notes</b> 
     *     <i>Notes for the contact as text value</i>
     *     Type: String, Maximum Length: 128
     *     Example: Sample Notes
     *
     * <b>startDate</b> 
     *     <i>Start date for task</i>
     *     Type: Date
     *     Example: 2013-07-15T17:26:54Z
     *     
     * <b>dueDate</b> 
     *     <i>Due date for task</i>
     *     Type: Date
     *     Example: 2013-07-15T17:26:54Z  
     *    
     * <b>reminderDate</b> 
     *     <i>Reminder date for task</i>
     *     Type: Date
     *     Example: 2013-07-15T17:26:54Z  
     *     
     * <b>progress</b> 
     *     <i>Progress value of task</i>
     *     Type: int
     *     Example: 50%
     *     
     * <b>folderId</b>
     *     <i>Folder ID - the id of the Folder.
     *     Default folder would be used on this missing of this param.</i>
     *     Type: integer
     *     Example: 1,2,3 et...
     * </pre>
     * 
     * @return returns unique identifier for tasks i.e. userId.
     * @throws MxOSException MxOSException.
     *
     * <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_NAME - <i>Error in updating task name.</i>
     * TSK_INVALID_OWNER - <i>Error in updating task owner.</i>
     * TSK_INVALID_PRIORITY - <i>Error in updating task priority.</i>
     * TSK_INVALID_STATUS - <i>Error in updating task status.</i>
     * TSK_INVALID_PRIVATE_FLAG - <i>Error in updating task private flag.</i>
     * TSK_INVALID_COLOR_LABEL - <i>Error in updating task color label.</i>
     * TSK_INVALID_CATEGORIES - <i>Error in updating task categories.</i>
     * TSK_INVALID_NOTES - <i>Error in updating task notes.</i>
     * TSK_INVALID_START_DATE - <i>Error in updating task start date.</i>
     * TSK_INVALID_DUE_DATE - <i>Error in updating task due date.</i>
     * TSK_INVALID_REMINDER_DATE - <i>Error in updating task reminder date.</i>
     * TSK_INVALID_PROGRESS - <i>Error in updating task progress.</i>
     * TSK_INVALID_FOLDER_ID - <i>Invalid folder Id.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    long create(final Map<String, List<String>> inputParams)
            throws MxOSException;

    List<Long> createMultiple(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting tasks. It uses one or more actions
     * to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>folderId</b>
     *     <i>Folder ID - the id of the Folder.
     *     Default folder would be used on this missing of this param.</i>
     *     Type: integer
     *     Example: 1,2,3 et...
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDER_ID - <i>Invalid folder Id.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    void deleteAll(Map<String, List<String>> inputParams) 
            throws MxOSException;


    /**
     *  This operation is responsible for list task objects.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>folderId</b>
     *     <i>Folder ID - the id of the Folder.
     *     Default folder would be used on this missing of this param.</i>
     *     Type: integer
     *     Example: 1,2,3 et...
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDER_ID - <i>Invalid folder Id.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * <pre>
     * This API is deprecated. Please use ITasksBaseService.list(...) instead.
     * </pre>
     */
    @Deprecated
    List<Task> list(final Map<String, List<String>> inputParams)
            throws MxOSException;


    /**
     *  This operation is to read the entire Task object.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>folderId</b>
     *     <i>Folder ID - the id of the Folder.
     *     Default folder would be used on this missing of this param.</i>
     *     Type: integer
     *     Example: 1,2,3 et...
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_FOLDER_ID - <i>Invalid folder Id.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     * <pre>
     * This API is deprecated. Please use ITasksBaseService.list(...) instead.
     * </pre>
     */
    
    Task read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     *  This operation is responsible for confirm task.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>taskId</b>
     *     <i>taskId</i>
     *     Type: integer - Valid taskId
     *     Example: 123 
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_FOLDER_ID - <i>Invalid folder Id.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    void confirm(final Map<String, List<String>> inputParams) throws MxOSException;
}
