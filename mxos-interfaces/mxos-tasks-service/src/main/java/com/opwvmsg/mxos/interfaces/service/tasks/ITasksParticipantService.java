/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.interfaces.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Participant;

/**
 * Interface to Tasks Base Object level operations like Read, Update and List.
 * 
 * @author mxos-dev
 */
public interface ITasksParticipantService {

	/**
     * This operation is responsible for creating the task participants.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>participantEmail</b>
     * 		<i>Participants's email address or emailAlias</i>
     * 		Type: String, Maximum Length: 128
     * 		Example: bs000123@openwave.com
     * <b>participantType</b>
     * 		<i>Participants's type</i>
     * 		Type: Integer, Valid Values [1 - user, 2 - user group, 3 - resource, 4 - resource group, 5 - external user]
     * 		Example: 1
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>participantName</b>
     *     <i>Participant Name</i>
     *     Type: String, Maximum Length: 60
     *     Example: something
     * @throws MxOSException MxOSException.
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * TSK_INVALID_EMAIL - <i>Invalid email.</i>
     * TSK_INVALID_PARTIIPANT_TYPE - <i>Invalid participant type.</i>
     * TSK_INVALID_PARTIIPANT_NAME - <i>Invalid participant name.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void create(final Map<String, List<String>> inputParams)
            throws MxOSException;
    
    /**
     * This operation is responsible for read the task participants.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>participantEmail</b>
     * 		<i>Participants's email address or emailAlias</i>
     * 		Type: String, Maximum Length: 128
     * 		Example: bs000123@openwave.com
     *      
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * @throws MxOSException MxOSException.
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * TSK_INVALID_EMAIL - <i>Invalid email.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    Participant read(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for updating Task participants.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>participantEmail</b>
     * 		<i>Participants's email address or emailAlias</i>
     * 		Type: String, Maximum Length: 128
     * 		Example: bs000123@openwave.com
     * 
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * <b>participantName</b>
     *     <i>Participant Name</i>
     *     Type: String, Maximum Length: 60
     *     Example: something     
     * @throws MxOSException MxOSException.
     * 
     *  <pre>
     * <b>Error Codes</b>
     * TSK_INVALID_USERNAME - <i>Invalid user name.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * TSK_INVALID_SESSION - <i>Invalid Session.</i>
     * TSK_INVALID_COOKIE - <i>Invalid cookie.</i> 
     * TSK_INVALID_EMAIL - <i>Invalid email.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void update(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     * This operation is responsible for deleting tasks participant. It uses one or more actions
     * to do this activity.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * <b>participantEmail</b>
     * 		<i>Participants's email address or emailAlias</i>
     * 		Type: String, Maximum Length: 128
     * 		Example: bs000123@openwave.com
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     * <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>
     * TSK_INVALID_EMAIL - <i>Invalid email.</i>
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     * 
     */
    void delete(final Map<String, List<String>> inputParams)
            throws MxOSException;

    /**
     *  This operation is responsible for list task participant objects.
     * 
     * @param inputParams Parameters given by user.
     * 
     * <pre>
     * <b>inputParams - Mandatory</b>
     * <b>taskId</b>
     *     <i>Task ID, which is returned after calling create task API</i>
     *     Type: String, Maximum Length: 128
     *     Example: 1,2,3 et...
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com 
     *         
     * <b>inputParams - Optional</b>
     * <b>sessionId</b>
     *     <i>Open Xchange session Id returned after calling Login API</i>
     *     Type: String, Maximum Length: 60
     *     Example: c62ad453fa354001b1bd0c3ed938065a
     * <b>cookieString</b>
     *     <i>Open Xchange cookie, returned after calling Login API</i>
     *     Type: String, Maximum Length: 255
     *     Example: open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=7843dd3a6da543e0aa26200c0a297bcc
     * 
     * </pre>
     * 
     * @throws MxOSException MxOSException.
     * 
     *             <pre>
     * <b>Error Codes</b>
     * ABS_INVALID_USERNAME - <i>Invalid username.</i>
     * TSK_INVALID_TASKID - <i>Invalid TaskId.</i>
     * ABS_INVALID_SESSION - <i>Invalid Session.</i>
     * ABS_INVALID_COOKIE - <i>Invalid cookie.</i>  
     * 
     * <b>See Also: </b><a href="http://home.openwave.com/~devbuild/mxos/v2/mxos-doc/json/errors/common-errors.html">Common errors</a>
     * </pre>
     */
    List<Participant> list(final Map<String, List<String>> inputParams)
            throws MxOSException;
}
