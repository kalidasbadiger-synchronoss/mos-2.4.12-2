/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave
 * Messaging Systems Inc. The software may be used and/or copied only with the
 * written permission of Openwave MessagingSystems Inc. or in accordance with
 * the terms and conditions stipulated in the agreement/contract under which the
 * software has been supplied. $Id: $
 */
package com.opwvmsg.mxos.snapshot;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.Group;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.snapshot.ISnapshot;
import com.opwvmsg.mxos.snapshot.SnapshotWorkerThread;
import com.opwvmsg.mxos.snapshot.commons.ToolKeys;
import com.opwvmsg.mxos.snapshot.mos.MosSnapshot;
import com.opwvmsg.mxos.snapshot.utils.ToolUtils;

/**
 * The LegalInterceptorTool code which responsible for taking the snpahsot of the user account.
 * 
 * @author mxos-dev
 */
public class LegalInterceptorTool {
    private static Logger logger = Logger.getLogger(LegalInterceptorTool.class);
    private static ExecutorService executor = null;
    private static String userId = null;
    private static ISnapshot mosSnapshot;
    private static ConcurrentSkipListSet<String> messageList;

    public static void main(String[] args) throws Exception {

       // Add a shutdown hook
        try {
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    logger.info("Snapshot Tool is shutting down...");
                    System.out.println("Snapshot Tool is shutting down...");
                    ToolUtils.setShuttingDown(true);
                    try {
                        for (Thread t : Thread.getAllStackTraces().keySet()) {
                            if (null != t.getThreadGroup()
                                    && t.getThreadGroup().getName()
                                            .equalsIgnoreCase("main")
                                    && !t.getName().equalsIgnoreCase(
                                            "DestroyJavaVM")
                                    && !t.equals(Thread.currentThread())
                                    && t.isAlive()) {
                                t.join();
                            }
                        }
                    } catch (Exception e) {
                        System.out.println("Shutdown hook failed. Reason: ");
                        e.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            System.out.println("Shutdown hook was not registered. Reason: ");
            e.printStackTrace();
        }

        try {
            String msg = "Snapshot started...";
            System.out.println(msg + "Command line argument: " + args);
            
            // User Id can be given in the command line arguments or in the
            // snapshot.properties
            if (args != null && args.length > 0) {
                userId = args[0];
            }
            
            ToolUtils.loadContext();
            if (userId == null) {
                // If user Id is not provided as commnad line argument it should
                // be provided in snapshot.properties
                userId = System.getProperty(ToolKeys.userId.name());
                System.out.println("userId: " + userId);
                
                if(userId == null) {
                    System.out
                            .println("Usage: java SnapshotTool user id whose snapshot has to be taken is not provided");
                    System.exit(-1);

                }
            } 

            // Threads not required if non of migration enabled.
            if (ToolUtils.isSnapshotAddressBookEnabled()
                    || ToolUtils.isSnapshotMailboxEnabled()
                    || ToolUtils.isSnapshotMessagesEnabled()) {
                mosSnapshot = new MosSnapshot();
                messageList = new ConcurrentSkipListSet<String>();
                startSnapshotTask(userId);
            }
            msg = "Snapshot task completed...!";
            logger.info(msg);        
        } catch (Exception e) {
            String msg = "Exception in migration.";
            logger.error(msg, e);
            System.out.println(msg);
            System.exit(-1);
        } finally {
            ToolUtils.destroy();
        }
    }

    /**
     * This operation is responsible for shutting down the multithreaded
     * activity
     * 
     * @param executor
     * 
     */
    private static void shutdownAndAwaitTermination(ExecutorService executor) {
        if (null == executor || executor.isTerminated()) {
            return;
        }
        // Disable new tasks from being submitted
        executor.shutdown();
        long waitTime = 10;// default wait time 10 secs;
        try {
            // Wait a while for existing tasks to terminate
            while (!executor.awaitTermination(waitTime, TimeUnit.SECONDS)) {
            }
        } catch (InterruptedException e) {
            logger.warn("ExecutorService waiting thread interrupted!", e);
        }
    }

    /**
     * This operation is responsible for starting the activity of taking
     * snapshot of the user. This APi takes snapshot of mailbox, contacts,
     * groups, folders, messages. The snapshot of messages is a multithreaded
     * activity for better performance
     * 
     * @param email is the userId whose snapshot has to be taken.
     * 
     */
    private static void startSnapshotTask(final String email) throws Exception {
        try {

            // Snapshot Mailbox
            if (ToolUtils.isSnapshotMailboxEnabled()) {
                snapshotMailbox(email);
            }

            // Snapshot AddressBook
            if (ToolUtils.isSnapshotAddressBookEnabled()) {
                snapshotAddressBook(email);
            }

            // Snapshot Messages
            if (ToolUtils.isSnapshotMessagesEnabled()) {
                List<Folder> folderList = snapshotFolder(email);

                executor = Executors.newFixedThreadPool(ToolUtils
                        .getMaxWorkerThreadCount());

                for (Folder fld : folderList) {

                    if (!fld.getFolderName().equals("/")
                            && fld.getNumMessages().intValue() > 0) {

                        Map<String, Metadata> messageMetaDataMap = mosSnapshot
                                .readMessageMetaData(email, fld.getFolderName());

                        // Reading the messages of the user is a multithreaded
                        // activity
                        for (int i = 0; i < ToolUtils.getMaxWorkerThreadCount(); i++) {
                            executor.execute(new SnapshotWorkerThread(
                                    messageList, email, fld.getFolderName(),
                                    messageMetaDataMap));
                        }

                    }
                }
                shutdownAndAwaitTermination(executor);
            }

            final String msg = "Snapshot completed for batch file: ";
            System.out.println(msg);
            logger.info(msg);

        } catch (Exception e) {
            logger.error("Error processing batch file: ");
        }

    }

    /**
     * This operation is responsible for writing the account details to files at
     * userdefined location.
     * 
     * @param data which needs to be stored.
     * @param dataType is the type of data thats being stored
     * 
     */
    public static final void writeToFile(String data, String dataType) {
        try {
            File dir = new File(System.getProperty(ToolKeys.outputFolderName
                    .name()));
            File file = new File(dir, dataType);

            // if file doesnt exists, then create it
            if (!dir.exists()) {
                dir.mkdir();
            }
            if (!file.exists()) {
                file.createNewFile();
            }

            BufferedWriter bw = new BufferedWriter(new FileWriter(file, true));

            bw.write(data);
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
            final String erroMsg = "Error generating final failure report.";
            System.out.println(erroMsg);
            logger.error(erroMsg, e);
        }
    }

    /**
     * This operation is responsible for taking the snapshot of the users
     * addressBook.
     * 
     * @param email is the userId whose addressBook has to read.
     * @throws Exception is thrown for any exception thrown during the operation
     *             of taking the snapshot of addressBook.
     */
    public static void snapshotAddressBook(final String email) throws Exception {

        try {
            if (ToolUtils.isSnapshotAddressBookEnabled()) {

                List<Contact> contactList = mosSnapshot.readContacts(email);
                logger.info("contactList details: " + contactList);
                writeToFile(new ObjectMapper().writeValueAsString(contactList),
                        "Contact-" + email + ".dat");
                List<Group> groupList = mosSnapshot.readGroups(email);
                logger.info("groupList details: " + groupList);
                writeToFile(new ObjectMapper().writeValueAsString(groupList),
                        "Group-" + email + ".dat");
            }
        } catch (Exception e) {
            logger.error("Error reading mailbox: " + email
                    + " on destination system", e);
            throw e;
        }
    }

    /**
     * This operation is responsible for taking the snapshot of the users
     * mailBox.
     * 
     * @param email is the userId whose mailBox has to read.
     * @throws Exception is thrown for any exception thrown during the operation
     *             of taking the snapshot of mailBox.
     */
    public static void snapshotMailbox(final String email) throws Exception {

        try {
            Mailbox mbx = mosSnapshot.readMailBox(email);
            logger.info("Mailbox details: " + mbx.toString());
            writeToFile(new ObjectMapper().writeValueAsString(mbx), "Mailbox-"
                    + email + ".dat");
        } catch (Exception e) {
            logger.error("Error reading mailbox: " + email
                    + " on destination system", e);
            throw e;
        }
    }

    /**
     * This operation is responsible for taking the snapshot of the users
     * folder.
     * 
     * @param email is the userId whose folder has to read.
     * @throws Exception is thrown for any exception thrown during the operation
     *             of taking the snapshot of folder.
     */
    public static List<Folder> snapshotFolder(final String email)
            throws Exception {

        try {
            if (ToolUtils.isSnapshotMessagesEnabled()) {
                List<Folder> folderList = mosSnapshot.readFolder(email);

                writeToFile(new ObjectMapper().writeValueAsString(folderList),
                        "Folder-" + email + ".dat");

                return folderList;
            }
        } catch (Exception e) {
            logger.error("Error reading mailbox: " + email
                    + " on destination system", e);
            throw e;
        }

        return null;
    }

}
