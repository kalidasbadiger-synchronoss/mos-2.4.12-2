/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave MessagingSystems Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.snapshot.utils;

import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.MxOSContextFactory;
import com.opwvmsg.mxos.snapshot.commons.ToolKeys;
import com.opwvmsg.mxos.snapshot.stats.Stats;

/**
 * ToolUtils class has all the operations based on the properties given in the snapshot.properties file.
 * @author mxos-dev
 */
public class ToolUtils {
    private static Logger logger;
    // MxOS Context instance
    private static IMxOSContext context = null;
    private static boolean snapshotMailbox;
    private static boolean snapshotMessages;
    private static boolean snapshotAddressBook;
    private static boolean statsEnabled;
    private static boolean shuttingDown;

    /**
     * Method to cleanup all resources.
     * 
     */
    public static void destroy() {
        try {
            ToolUtils.getContext().destroy();
        } catch (Exception e) {
            logger.warn("Error while closing mOS Context", e);
        }
        Stats.stopStatlogger();
    }

    /**
     * Get Context Instance.
     * 
     * @return IMxOSContext IMxOSContext
     */
    public static IMxOSContext getContext() {
        return context;
    }

    /**
     * Method to get properties value of maxWorkerThreadCount.
     * 
     * @return integer
     */
    public static Integer getMaxWorkerThreadCount() {
        return Integer.parseInt(System
                .getProperty(ToolKeys.maxWorkerThreadCount.name()));
    }

    /**
     * Method to know statsInterval in seconds.
     * 
     * @return boolean statsInterval
     */
    public static Integer getStatsInterval() {
        return Integer.parseInt(System.getProperty(ToolKeys.statsInterval
                .name()));
    }

    /**
     * Method to get properties value of workerThreadRetryDelay.
     * 
     * @return integer integer in ms
     */
    public static Integer getWorkerThreadRetryDelay() {
        return Integer.parseInt(System
                .getProperty(ToolKeys.workerThreadRetryDelay.name()));
    }

    /**
     * Method to know shuttingDown is enabled or not.
     * 
     * @return boolean shuttingDown
     */
    public static boolean isShuttingDown() {
        return ToolUtils.shuttingDown;
    }

    /**
     * Method to know snapshotAddressBook is enabled or not.
     * 
     * @return boolean snapshotAddressBook
     */
    public static boolean isSnapshotAddressBookEnabled() {
        return snapshotAddressBook;
    }

    /**
     * Method to know snapshotMailbox is enabled or not.
     * 
     * @return boolean snapshotMailbox
     */
    public static boolean isSnapshotMailboxEnabled() {
        return snapshotMailbox;
    }

    /**
     * Method to know snapshotMessages is enabled or not.
     * 
     * @return boolean snapshotMessages
     */
    public static boolean isSnapshotMessagesEnabled() {
        return snapshotMessages;
    }

    /**
     * Method to know statsEnabled is enabled or not.
     * 
     * @return boolean statsEnabled
     */
    public static boolean isStatsEnabled() {
        return statsEnabled;
    }

    /**
     * Method to load all the boolean properties from the properties file.
     * 
     */
    private static void loadAllBooleanConfigs() {
        snapshotMailbox = loadAllBooleanConfigs(ToolKeys.snapshotMailbox);
        snapshotMessages = loadAllBooleanConfigs(ToolKeys.snapshotMessages);
        snapshotAddressBook = loadAllBooleanConfigs(ToolKeys.snapshotAddressBook);
        statsEnabled = loadAllBooleanConfigs(ToolKeys.statsEnabled);
    }

    /**
     * Method to load a specific property from snapshot.properties file.
     * 
     * @param key is the field in snapshot.propertoes whose value has to be
     *            retrieved.
     * @return boolean the value of specified property
     */
    private static boolean loadAllBooleanConfigs(final ToolKeys key) {
        final boolean value = Boolean.parseBoolean(System.getProperty(key
                .name()));
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(key.name()).append(" is = ").append(
                    value));
        }
        return value;
    }

    /**
     * Method to load a the snapshot.properties file.
     */
    private static Properties loadConfig() {
        final Properties p = new Properties();

        // load snapshot.properties
        InputStream in = null;
        try {
            String fileName = "/config/snapshot.properties";
            in = ToolUtils.class.getResourceAsStream(fileName);
            p.load(in);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Problem while loading snapshot.properties", e);
            System.exit(-1);
        } finally {
            IOUtils.closeQuietly(in);
        }
        return p;
    }

    /**
     * Load Context for first time.
     */
    public static void loadContext() throws Exception {
        // load mxos.properties
        final Properties p = loadConfig();
        loadLog4j();
        logger.info("Snapshot Tool Started");
        context = MxOSContextFactory.getInstance().getContext("MOS-SNAPSHOT", p);
        logger.info(new StringBuffer("Loaded mOS ").append(
                p.getProperty("contextId")).append(" context"));
        // load all boolean flags
        loadAllBooleanConfigs();
        // loadStats
        loadStats();
        logger.info("Snapshot Context loaded");
    }

    /**
     * Method to load log4j configurations.
     * 
     * @throws Exception Exception
     */
    private static void loadLog4j() throws Exception {
        String log4jFile = "/config/log4j.xml";
        // init log4j configuration from property file
        DOMConfigurator.configure(ToolUtils.class.getResource(log4jFile)
                .getFile());
        logger = Logger.getLogger(ToolUtils.class);
        logger.debug("Snapshot Tool Started");
    }

    /**
     * Method to load Stats.
     * 
     * @throws Exception Exception
     */
    public static void loadStats() throws Exception {
        // start stats thread
        // Generate stats log for every configured duration (in seconds)
        boolean isStatsEnabled = ToolUtils.isStatsEnabled();
        if (logger.isDebugEnabled()) {
            logger.debug("isStatsEnabled: " + isStatsEnabled);
        }
        if (isStatsEnabled) {
            int timeInterval = ToolUtils.getStatsInterval();
            Stats.init(timeInterval);
        }
    }

    /**
     * loadValidateContext for first time.
     */
    public static void loadValidateContext() throws Exception {
        // load mxos.properties
        final Properties p = loadConfig();
        try {
            loadLog4j();
        } catch (Exception e) {
            System.out.println("Unable to load log4j in Validate Context");
            e.printStackTrace();
        }
        logger.info("Validation Tool Started");
        context = MxOSContextFactory.getInstance().getContext("MOS-SNAPSHOT", p);
        logger.info(new StringBuffer("Loaded mOS ").append(
                p.getProperty("contextId")).append(" context"));
        // load all boolean flags
        loadAllBooleanConfigs();
        logger.info("Validate context loaded");
    }

    /**
     * Method to set shuttingDown value.
     * 
     * @param boolean shuttingDown
     */
    public static void setShuttingDown(boolean shuttingDown) {
        ToolUtils.shuttingDown = shuttingDown;
    }
}
