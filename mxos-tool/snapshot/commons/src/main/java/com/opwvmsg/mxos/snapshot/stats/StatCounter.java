/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.snapshot.stats;

import java.util.EnumMap;
import java.util.Iterator;

/**
 * Class for storing and logging various response times and counts for any
 * statEvent. The class also has getter methods for getting the statistics.
 * 
 * @author mxos-dev
 **/
public class StatCounter {
    private EnumMap<SnapshotOperation, Statistics> serviceOprMap;
    private SnapshotServiceEnum statName;

    /**
     * Constructor.
     * 
     * @param statName StatName for which this counter will be used.
     */
    public StatCounter(SnapshotServiceEnum statName) {
        this.setStatName(statName);
        serviceOprMap = new EnumMap<SnapshotOperation, Statistics>(
                SnapshotOperation.class);
    }

    /**
     * Method to get stat name
     * 
     * @return the StatName for which this counter will be used.
     */
    public SnapshotServiceEnum getStatName() {
        return statName;
    }

    /**
     * Method to print and reset the Counters after each statsIntervel in
     * seconds
     */
    public void printAndResetCounters() {
        Iterator<SnapshotOperation> keySetIterator = serviceOprMap.keySet()
                .iterator();
        while (keySetIterator.hasNext()) {
            SnapshotOperation tmpOpr = keySetIterator.next();
            serviceOprMap.get(tmpOpr).printAndResetStats();
        }
    }

    /**
     * Method to set stat name
     * 
     * @param statName StatName for which this counter will be used.
     */
    public void setStatName(SnapshotServiceEnum statName) {
        this.statName = statName;
    }

    /**
     * Method to update the Counter.
     * 
     * @param statEvent StatEvent containing the statName, responce time and
     *            status.The existing counter will be updated with this new
     *            statEvent.
     */
    public void updateCounter(StatEvent statEvent) {
        setStatName(statEvent.getName());
        if (!serviceOprMap.containsKey(statEvent.getOperation())) {
            serviceOprMap.put(statEvent.getOperation(), new Statistics(
                    statEvent));
        } else {
            serviceOprMap.get(statEvent.getOperation()).updateStats(statEvent);
        }
    }
}
