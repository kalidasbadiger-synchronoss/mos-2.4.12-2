/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.snapshot.commons;

/**
 * Base Exception that can occur for any error across Snapshot project.
 * 
 * @author mxos-dev
 */

public class LegalInterceptorException extends Exception {

    private String message;

    /**
     * Constructor with Message.
     * 
     * @param message - error message
     */
    public LegalInterceptorException(final String message) {
        super(message);
        this.message = message;
    }

    /**
     * Constructor with ErrorCode and Message.
     * 
     * @param message - error message
     * @param e - actual exception which is being thrown
     */
    public LegalInterceptorException(final String message, final Exception e) {
        super(message, e);
        this.message = message;
    }

    /**
     * Get Error Message.
     * 
     * @return the error message
     */
    @Override
    public String getMessage() {
        return this.message;
    }

    /**
     * Method to get String representation of this Error.
     * 
     * @return String - error message
     */
    @Override
    public String toString() {
        return getMessage();
    }
}
