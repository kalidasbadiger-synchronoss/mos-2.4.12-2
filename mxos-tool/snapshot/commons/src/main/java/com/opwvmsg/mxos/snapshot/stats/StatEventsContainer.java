/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.snapshot.stats;

import java.util.EnumMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Class for storing the stat events. This class uses two concurrent queues for
 * storing the stats. The stat events will be stored in first concurrent queue
 * while each thread in the application add the stat. Later while processing the
 * stats, the events from first queue will be removed and then will be placed in
 * the other concurrent queue.
 *
 * @author mxos-dev
 **/
public class StatEventsContainer {

    private ConcurrentLinkedQueue<StatEvent> stateventqueueMain;
    private ConcurrentLinkedQueue<StatEvent> stateventqueueBkup;
    private EnumMap<SnapshotServiceEnum, StatCounter> statCounts;

    /**
     * Constructor.
     * 
     */
    public StatEventsContainer() {

        stateventqueueMain = new ConcurrentLinkedQueue<StatEvent>();
        stateventqueueBkup = new ConcurrentLinkedQueue<StatEvent>();
        statCounts = new EnumMap<SnapshotServiceEnum, StatCounter>(
                SnapshotServiceEnum.class);
        SnapshotServiceEnum[] availableStats = SnapshotServiceEnum.values();
        for (SnapshotServiceEnum statName : availableStats) {
            StatCounter statCounter = new StatCounter(statName);
            statCounts.put(statName, statCounter);
        }
    }

    /**
     * Method to add the statEvent to the container. This method will create a
     * statEvent object if needed or it will use the existing event from the
     * queue and update it with the latest information.
     * 
     * @param statName name of the stat.
     * @param serviceOperation name of the operation.
     * @param totalTime time taken by the application for the operation.
     * @param statStatus stat status such as valid or invalid.
     * 
     */
    public void addEvent(SnapshotServiceEnum statName,
            SnapshotOperation serviceOperation, long totalTime,
            StatStatus statStatus) {
        if (stateventqueueMain.isEmpty()) {
            StatEvent statEvent = new StatEvent(statName, serviceOperation,
                    totalTime, statStatus, StatValidity.valid);
            stateventqueueBkup.offer(statEvent);
        } else {
            StatEvent statEvent = stateventqueueMain.poll();
            statEvent.updateEvent(statName, serviceOperation, totalTime,
                    statStatus, StatValidity.valid);
            stateventqueueBkup.offer(statEvent);
        }
    }

    /**
     * Method to process the event in the container. Once the processing is
     * complete this method will print the stats, invalidate the stat events and
     * add them to the Main queue.
     */
    public void processEvents() {

        StatCounter statCounter = null;
        while (!stateventqueueBkup.isEmpty()) {
            StatEvent statEvent = stateventqueueBkup.poll();
            if (statEvent.isValidEvent()) {
                statCounter = statCounts.get(statEvent.getName());
                statCounter.updateCounter(statEvent);
                statEvent.invalidate();
            }
            stateventqueueMain.offer(statEvent);
        }

        Iterator<SnapshotServiceEnum> keySetIterator = statCounts.keySet()
                .iterator();
        while (keySetIterator.hasNext()) {
            SnapshotServiceEnum stmp = keySetIterator.next();
            statCounts.get(stmp).printAndResetCounters();
        }
    }
}
