/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave MessagingSystems Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.snapshot.commons;

/**
 * This enum contains all the keys present in the snapshot.properties file
 * 
 * @author mxos-dev
 */
public enum ToolKeys {
    /* Keys in snapshot.properties */

    // mOS SDK Context, Allowed Values: REST or STUB
    contextId,
    // mOS Base URL, applicable only for REST Context
    mosBaseUrl,
    // max thread count to connect mOS, applicable only for REST Context
    mosMaxConnections,
    // mos Connection Timeout in ms
    mosConnectionTimeout,
    // mos Read Timeout in ms
    mosReadTimeout,
    // Snapshot of mailbox
    snapshotMailbox,
    // Snapshot of all Folder and Messages objects on target.
    snapshotMessages,
    // Snapshot of all AddressBook objects on target.
    snapshotAddressBook,
    // Folder where the snapshot details will be stored
    outputFolderName,
    // Statistics to be supported
    statsEnabled,
    // Stats time duration
    statsInterval,
    // max thread count to connect mOS, applicable only for REST Context
    mxosMaxConnections,
    // Max worker thread count
    maxWorkerThreadCount,
    // Waiting time to retry, when WorkerThread Count reached max, in ms.
    workerThreadRetryDelay,
    // User Id whose snapshot has to be taken.
    userId;
}
