/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.snapshot.stats;

/**
 * Class containing stat event details like statName, time taken by the
 * application for executing the operation described by the stat, stat status
 * (e.g. pass, fail or timeout) and stat validity such as valid or invalid.
 * 
 * @author mxos-dev
 **/
public class StatEvent {
    private SnapshotServiceEnum statName;
    private long time;
    private StatStatus statStatus;
    private StatValidity statValidity;
    private SnapshotOperation operationName;

    /**
     * Constructor.
     * 
     * @param statName StatName for this statEvent.
     * @param operationName the name of the opertaion for this statEvent
     * @param time time taken by the application for executing the operation
     *            described by the stat.
     * @param statStatus stat status (e.g. pass, fail or timeout).
     * @param statValidity such as valid or invalid.
     */

    public StatEvent(SnapshotServiceEnum statName,
            SnapshotOperation operationName, long time, StatStatus statStatus,
            StatValidity statValidity) {
        this.statName = statName;
        this.operationName = operationName;
        this.time = time;
        this.statStatus = statStatus;
        this.statValidity = statValidity;
    }

    /**
     * Method for getting stats name for the statEvent.
     * 
     * @return statName stats name for the statEvent.
     */
    public SnapshotServiceEnum getName() {
        return this.statName;
    }

    /**
     * Method for getting operation name for the statEvent.
     * 
     * @return operationName operation name for the statEvent.
     */
    public SnapshotOperation getOperation() {
        return this.operationName;
    }

    /**
     * Method for getting status for the statEvent.
     * 
     * @return statStatus stat status (e.g. pass, fail and timeout).
     */
    public StatStatus getStatus() {
        return this.statStatus;
    }

    /**
     * Method for getting response time for the statEvent.
     * 
     * @return time response time for the statEvent.
     */
    public long getTime() {
        return this.time;
    }

    /**
     * Method for invalidating the stat event.
     */
    public void invalidate() {
        this.statValidity = StatValidity.invalid;
    }

    /**
     * Method for getting stats validity such as it is a valid or invalid stat.
     * 
     * @return true if the stat is valid else false.
     */
    public boolean isValidEvent() {
        return this.statValidity == StatValidity.valid;
    }

    /**
     * Method for updating the existing statEvent with new details.
     * 
     * @param statName StatName for this statEvent.
     * @return operationName operation name for the statEvent.
     * @param time time taken by the application for executing the operation
     *            described by the stat.
     * @param statStatus stat status (e.g. pass, fail or timeout).
     * @param statValidity such as valid or invalid.
     */
    public void updateEvent(SnapshotServiceEnum statName,
            SnapshotOperation operationName, long time, StatStatus statStatus,
            StatValidity statValidity) {
        this.statName = statName;
        this.operationName = operationName;
        this.time = time;
        this.statStatus = statStatus;
        this.statValidity = statValidity;
    }
}
