ECHO OFF
SET CLASSPATH=""
FOR /R lib/ %%a in (*.jar) DO CALL :AddToPath %%a

java -cp %CLASSPATH% com.opwvmsg.mxos.snapshot.LegalInterceptorTool

:AddToPath
SET CLASSPATH=%1;%CLASSPATH%;.;
GOTO :EOF

ECHO ON
