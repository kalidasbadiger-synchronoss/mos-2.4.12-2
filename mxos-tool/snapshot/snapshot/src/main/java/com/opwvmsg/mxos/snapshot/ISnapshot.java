package com.opwvmsg.mxos.snapshot;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Contact;
import com.opwvmsg.mxos.addressbook.pojos.Group;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.snapshot.commons.LegalInterceptorException;

/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave
 * Messaging Systems Inc. The software may be used and/or copied only with the
 * written permission of Openwave MessagingSystems Inc. or in accordance with
 * the terms and conditions stipulated in the agreement/contract under which the
 * software has been supplied. $Id: $
 */

/**
 * Interface for all Snapshot Operations.
 * 
 * @author mxos-dev
 */
public interface ISnapshot {

    /**
     * This operation is responsible for reading all the Contacts of the
     * account.
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns Contact.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the contacts.
     * 
     */
    public List<Contact> readContacts(String userId)
            throws LegalInterceptorException;

    /**
     * This operation is responsible for reading all the Folders of the account.
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns List<Folder>.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the folders.
     * 
     */
    public List<Folder> readFolder(String userId)
            throws LegalInterceptorException;

    /**
     * This operation is responsible for reading all the Groups of the account.
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns Group.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the groups.
     * 
     */
    public List<Group> readGroups(String userId)
            throws LegalInterceptorException;

    /**
     * This operation is responsible for reading the Mailbox of the account.
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns Mailbox.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the mailBox.
     * 
     */
    public Mailbox readMailBox(String userId) throws LegalInterceptorException;

    /**
     * This operation is responsible for reading all the MessageMetaData of the
     * account.
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns map of messageId as key and the message meta data as the
     *         value
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the messageMetaData.
     * 
     */
    public Map<String, Metadata> readMessageMetaData(String userId,
            String folderName) throws LegalInterceptorException;

    /**
     * This operation is responsible for reading a particular message
     * 
     * @param userId Parameters given by user.
     * 
     *            <pre>
     * <b>inputParams - Mandatory</b>
     * <b>userId</b>
     *     <i>Subscriber's email address or emailAlias</i>
     *     Type: String, Maximum Length: 128
     *     Example: bs000123@openwave.com
     * </pre>
     * 
     * @return returns MessageBody.
     * @throws LegalInterceptorException is thrown for any exception thrown
     *             during the operation of reading the message.
     * 
     */
    public Message readMessage(String userId, String folderName,
            String messageId) throws LegalInterceptorException;

}
