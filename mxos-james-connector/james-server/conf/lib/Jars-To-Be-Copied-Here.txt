Common Jars:
========================================
commons-io-2.0.1.jar
commons-lang-2.6.jar
jackson-core-asl-1.9.2.jar
jackson-jaxrs-1.9.2.jar
jackson-mapper-asl-1.9.2.jar
jackson-xc-1.9.2.jar
jersey-client-1.11.jar
jersey-core-1.11.jar
jersey-json-1.11.jar
jsr311-api-1.1.jar


MxOS Jars:
========================================
mxos-data-2.0.0-SNAPSHOT.jar
mxos-exception-2.0.0-SNAPSHOT.jar
mxos-interfaces-2.0.0-SNAPSHOT.jar
mxos-james-connector-2.0.0-SNAPSHOT.jar
mxos-rest-2.0.0-SNAPSHOT.jar
mxos-util-2.0.0-SNAPSHOT.jar