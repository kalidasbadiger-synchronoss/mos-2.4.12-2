/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.james.mailbox.store.streaming.ConfigurableMimeTokenStream;
import org.apache.james.mime4j.parser.Field;
import org.apache.james.mime4j.parser.MimeEntityConfig;
import org.apache.james.mime4j.parser.MimeTokenStream;

/**
 * Mxos Utility Class for James-Mxos Integration.
 *
 * @author mxos-dev
 */
public final class MxosJamesUtil {
    private MxosJamesUtil() {
    }
    /**
     * Method to convert InputStream to String object.
     *
     * @param is inputstream
     * @return string object
     */
    public static String convertStreamToString(InputStream is) {
        try {
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream buf = new ByteArrayOutputStream();
            int result = bis.read();
            while (result != -1) {
                byte b = (byte) result;
                buf.write(b);
                result = bis.read();
            }
            return buf.toString();
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Method to getHeaders.
     *
     * @param header InputStream
     * @return headers Map<String, String>
     */
    public static Map<String, String> getHeaders(final InputStream header) {
        final Map<String, String> headers = new HashMap<String, String>();
        try {
            MimeEntityConfig config = new MimeEntityConfig();
            config.setMaximalBodyDescriptor(true);
            config.setMaxLineLen(-1);
            final ConfigurableMimeTokenStream parser =
                new ConfigurableMimeTokenStream(config);

            parser.setRecursionMode(MimeTokenStream.M_NO_RECURSE);
            // parser.parse(tmpMsgIn.newStream(0, -1));
            parser.parse(header);
            int next = parser.next();
            while (next != MimeTokenStream.T_BODY
                    && next != MimeTokenStream.T_END_OF_STREAM
                    && next != MimeTokenStream.T_START_MULTIPART) {
                if (next == MimeTokenStream.T_FIELD) {
                    Field field = parser.getField();
                    if (field != null) {
                        headers.put(field.getName(), field.getBody());
                    }
                }
                next = parser.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return headers;
    }

    /**
     * Class for Mxos James Constants.
     *
     * @author mxos-dev
     */
    public final class HeaderKey {
        public static final String FROM = "From";
        public static final String TO = "To";
        public static final String SUBJECT = "Subject";
        public static final String DATE = "Date";
        public static final String MESSAGE_ID = "Message-ID";
        private HeaderKey() {
        }
    }

    /**
     * Method to get proper email from given composite email.
     *
     * @param email email
     * @return String email
     */
    public static String getAsEmail(String email) {
        if (email == null || email.equals("")) {
            return "";
        }
        final Pattern pattern = Pattern.compile("<(.+?)>");
        Matcher m = pattern.matcher(email);
        if (m.find()) {
            return m.group(1).trim();
        } else {
            return email.trim();
        }
    }
}
