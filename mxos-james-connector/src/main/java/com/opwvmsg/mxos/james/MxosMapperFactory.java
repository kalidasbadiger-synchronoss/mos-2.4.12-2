/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james;

import javax.annotation.Resource;

import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.SubscriptionException;
import org.apache.james.mailbox.store.MailboxSessionMapperFactory;
import org.apache.james.mailbox.store.mail.MailboxMapper;
import org.apache.james.mailbox.store.mail.MessageMapper;
import org.apache.james.mailbox.store.user.SubscriptionMapper;

import com.opwvmsg.mxos.interfaces.service.meta.IFolderService;
import com.opwvmsg.mxos.interfaces.service.meta.IMessageService;
import com.opwvmsg.mxos.james.folder.MxosFolderMapper;
import com.opwvmsg.mxos.james.message.MxosMessageMapper;
import com.opwvmsg.mxos.james.message.MxosModSeqProvider;
import com.opwvmsg.mxos.james.message.MxosUidProvider;
import com.opwvmsg.mxos.james.user.MxosSubscriptionMapper;

/**
 * Class for James Mxos Mapper factory.
 *
 * @author mxos-dev
 */
public class MxosMapperFactory extends MailboxSessionMapperFactory<Integer> {
    @Resource(name = "folderService")
    protected IFolderService folderService;
    @Resource(name = "messageService")
    protected IMessageService messageService;
    protected MailboxMapper<Integer> mailboxMapper;

    /**
     * Default constructor.
     */
    public MxosMapperFactory() {

    }

    @Override
    protected MailboxMapper<Integer> createMailboxMapper(
            MailboxSession session) throws MailboxException {
        mailboxMapper = new MxosFolderMapper(folderService, session);
        return mailboxMapper;
    }

    @Override
    protected MessageMapper<Integer> createMessageMapper(
            MailboxSession session) throws MailboxException {
        return new MxosMessageMapper(messageService, session,
                (MxosFolderMapper) mailboxMapper, new MxosUidProvider(),
                new MxosModSeqProvider());
    }

    @Override
    protected SubscriptionMapper createSubscriptionMapper(
            MailboxSession session) throws SubscriptionException {
        return new MxosSubscriptionMapper(folderService);
    }
}
