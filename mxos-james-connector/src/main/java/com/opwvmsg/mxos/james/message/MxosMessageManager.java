/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.message;

import javax.mail.Flags;
import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.store.MailboxEventDispatcher;
import org.apache.james.mailbox.store.MailboxSessionMapperFactory;
import org.apache.james.mailbox.store.StoreMessageManager;
import org.apache.james.mailbox.store.mail.model.Mailbox;
import org.apache.james.mailbox.store.search.MessageSearchIndex;

/**
 * Abstract base class which should be used from MxOS implementation.
 * @author mxos-dev
 */
public class MxosMessageManager extends StoreMessageManager<Integer> {
    /**
     * Default constructor.
     * @param mapperFactory mapperFactory
     * @param index index
     * @param dispatcher dispatcher
     * @param mailbox mailbox
     * @throws MailboxException MailboxException
     */
    public MxosMessageManager(
            final MailboxSessionMapperFactory<Integer> mapperFactory,
            final MessageSearchIndex<Integer> index,
            final MailboxEventDispatcher<Integer> dispatcher,
            final Mailbox<Integer> mailbox) throws MailboxException {
        super(mapperFactory, index, dispatcher, mailbox);
    }

    @Override
    protected Flags getPermanentFlags(final MailboxSession session) {
        final Flags flags = super.getPermanentFlags(session);
        flags.add(Flags.Flag.USER);
        return flags;
    }
}
