/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.pojos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.opwvmsg.mxos.data.pojos.*;

/**
 * Utility class for Mxos Request Map.
 * @author bsatyam
 */
public class MxosRequestMap {
    final Map<String, List<String>> multivaluedMap;
    /**
     * Default constructor.
     */
    public MxosRequestMap() {
        multivaluedMap = new HashMap<String, List<String>>();
    }
    /**
     * add value to a property.
     * @param property property
     * @param value value
     */
    public void add(final DataMap.Property property, final Object value) {
        add(property.name(), value.toString());
    }

    /**
     * add value to a property.
     * @param property property
     * @param value value
     */
    public void add(final String property, final Object value) {
        add(property, value.toString());
    }

    /**
     * add value to a property.
     * @param property property
     * @param value value
     */
    public void add(final DataMap.Property property, final String value) {
        add(property.name(), value);
    }

    /**
     * add value to a key.
     * @param key key
     * @param value value
     */
    public void add(final String key, final String value) {
        final List<String> list = new ArrayList<String>();
        list.add(value);
        multivaluedMap.put(key, list);
    }
    /**
     * method to get multivalued map.
     * @return multivalued map
     */
    public Map<String, List<String>> getMultivaluedMap() {
        return multivaluedMap;
    }
    /**
     * method to get String representation of Request Map.
     * @return String request map in string
     */
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        for (Entry<String, List<String>> entry : multivaluedMap.entrySet()) {
            sb.append("\nKey = " + entry.getKey()).append(", Value = ");
            for (String value : entry.getValue()) {
                sb.append(value);
            }
        }
        return sb.toString();
    }
}
