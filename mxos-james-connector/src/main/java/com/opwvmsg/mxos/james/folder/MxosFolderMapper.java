/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.james.folder;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import java.util.List;

import org.apache.james.mailbox.MailboxConstants;
import org.apache.james.mailbox.MailboxException;
import org.apache.james.mailbox.MailboxNotFoundException;
import org.apache.james.mailbox.MailboxPath;
import org.apache.james.mailbox.MailboxSession;
import org.apache.james.mailbox.store.mail.MailboxMapper;
import org.apache.james.mailbox.store.mail.model.Mailbox;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.meta.IFolderService;
import com.opwvmsg.mxos.james.pojos.JamesMailbox;
import com.opwvmsg.mxos.james.pojos.MxosRequestMap;
import com.opwvmsg.mxos.james.pojos.JamesSession;

/**
 * MxosMailboxManager class to manager the Mxos Folders.
 *
 * @author mxos-dev
 */
public class MxosFolderMapper implements MailboxMapper<Integer> {
    private IFolderService folderService = null;
    private final JamesSession session;

    /**
     * Default constructor.
     *
     * @param folderService folderService
     * @param session session
     */
    public MxosFolderMapper(final IFolderService folderService,
            final MailboxSession session) {
        this.folderService = folderService;
        this.session = (JamesSession) session;
    }

    @Override
    public void delete(final Mailbox<Integer> mailbox) throws MailboxException {
        // Mailbox Delete API
        if (folderService != null) {
            try {
                final String folderToBeDeleted = mailbox.getName();
                final Long userId = session.getMailboxId();
                final Integer folderIdToBeDeleted = session.getFolderId(
                        folderToBeDeleted);
                final MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.mailboxId, userId);
                map.add(FolderProperty.folderid, folderIdToBeDeleted);
                map.add(FolderProperty.name, folderToBeDeleted);
                folderService.delete(map.getMultivaluedMap());
                session.removeFolder(folderIdToBeDeleted);
                list();
            } catch (MxOSException e) {
                e.printStackTrace();
                throw new MailboxNotFoundException(e.getMessage());
            }
        } else {
            throw new MailboxNotFoundException("FolderService not found");
        }
    }

    /**
     * Method to delete all folders.
     *
     * @throws MailboxException MailboxException
     */
    public void deleteAll() throws MailboxException {
        // mailboxesById.clear();
        // TODO: Need to check how to get username here.
    }

    @Override
    public synchronized Mailbox<Integer> findMailboxByPath(MailboxPath path)
        throws MailboxException, MailboxNotFoundException {
        if (folderService != null) {
            if (!session.isFoldersLoadded()) {
                list();
            }
            try {
                Map<Integer, JamesMailbox> folders = session.getFolders();
                if (folders != null) {
                    for (Entry<Integer, JamesMailbox> entry : folders
                            .entrySet()) {
                        Mailbox<Integer> folder = entry.getValue();
                        if (folder.getName().equalsIgnoreCase(path.getName())) {
                            session.setMessagesLoadded(false);
                            return folder;
                        }
                    }
                }
            } catch (Exception e) {
                throw new MailboxNotFoundException(path);
            }
        } else {
            throw new MailboxNotFoundException(path);
        }
        throw new MailboxNotFoundException(path);
    }

    @Override
    public List<Mailbox<Integer>> findMailboxWithPathLike(
            final MailboxPath path)
        throws MailboxException {
        if (folderService != null) {
            final List<Mailbox<Integer>> mailboxList =
                new ArrayList<Mailbox<Integer>>();
            if (!session.isFoldersLoadded()) {
                list();
            }
            try {
                final String regex = path.getName().replace("%", ".*");
                Map<Integer, JamesMailbox> folders = session.getFolders();
                if (folders != null) {
                    for (Entry<Integer, JamesMailbox> entry
                            : folders.entrySet()) {
                        Mailbox<Integer> folder = entry.getValue();
                        if (folder.getName().matches(regex)) {
                            session.setMessagesLoadded(false);
                            mailboxList.add(folder);
                        }
                    }
                }
            } catch (Exception e) {
                throw new MailboxNotFoundException(path);
            }
            return mailboxList;
        } else {
            throw new MailboxNotFoundException(path);
        }
    }

    @Override
    public void save(final Mailbox<Integer> mailbox) throws MailboxException {
        if (folderService != null) {
            try {
                final Long userId = session.getMailboxId();
                final String folderToBeCreated = mailbox.getName();
                int parentFolderId = 0;
                if (folderToBeCreated.indexOf('/') != -1) {
                    parentFolderId = session.getFolderId(folderToBeCreated
                            .substring(0, folderToBeCreated.lastIndexOf('/')));
                }
                final MxosRequestMap map = new MxosRequestMap();
                map.add(MailboxProperty.mailboxId, userId);
                map.add(FolderProperty.name, folderToBeCreated);
                map.add(FolderProperty.parentfolderid, parentFolderId);
                folderService.create(map.getMultivaluedMap());
                list();
            } catch (MxOSException e) {
                e.printStackTrace();
                throw new MailboxNotFoundException(e.getMessage());
            }
        } else {
            throw new MailboxNotFoundException("FolderService not found");
        }
    }

    @Override
    public void endRequest() {
        // Do nothing
    }

    @Override
    public boolean hasChildren(Mailbox<Integer> mailbox, char delimiter)
        throws MailboxException, MailboxNotFoundException {
        // TODO:
        /*
         * String mailboxName = mailbox.getName() + delimiter; for (final
         * Mailbox<Integer> box:mailboxesById.values()) { if
         * (box.getName().startsWith(mailboxName)) { return true; } }
         */
        return false;
    }

    /*
     * (non-Javadoc)
     * @see org.apache.james.mailbox.store.mail.MailboxMapper#list()
     */
    @Override
    public List<Mailbox<Integer>> list() throws MailboxException {
        if (folderService != null) {
            final List<Mailbox<Integer>> mailboxList =
                new ArrayList<Mailbox<Integer>>();
            final Long userId = session.getMailboxId();
            final MxosRequestMap map = new MxosRequestMap();
            map.add(MailboxProperty.mailboxId, userId);
            try {
                System.out.println("List Folders Request : " + map);
                List<Folder> folders =
                    folderService.readAll(map.getMultivaluedMap());
                if (folders != null) {
                    for (Folder folder : folders) {
                        final JamesMailbox mxosFolder = new JamesMailbox(
                                new MailboxPath(session.getPersonalSpace(),
                                        this.session.getUser().getUserName(),
                                        folder.getFolderName()), folder);
                        if (folder.getFolderName().equals(
                                MailboxConstants.INBOX)) {
                            mailboxList.add(0, mxosFolder);
                        } else {
                            mailboxList.add(mxosFolder);
                        }
                        session.setFolder(folder.getFolderId(), mxosFolder);
                    }
                    session.setFoldersLoadded(true);
                    session.setMessagesLoadded(false);
                }
            } catch (MxOSException e) {
                e.printStackTrace();
                throw new MailboxNotFoundException("Folder not found");
            }
            return mailboxList;
        } else {
            throw new MailboxNotFoundException("FolderService not found");
        }
    }

    @Override
    public <T> T execute(Transaction<T> transaction) throws MailboxException {
        return transaction.run();
    }

}
