/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaException;
import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * FolderNameLengthValidator class for Folder Name length validator.
 * 
 * @author mxos-dev
 * 
 */
public class FolderNameLengthValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 9072697073301436669L;
    private static Logger logger = Logger
            .getLogger(FolderNameLengthValidator.class);
    public static final String PROPERTY = "folderNameLength";
    private int folderNameLength;

    /**
     * default constructor.
     * 
     * @param maxLengthNode maxLengthNode
     */
    public FolderNameLengthValidator(JsonNode maxLengthNode) {
        if ((maxLengthNode != null) && (maxLengthNode.isIntegralNumber())) {
            this.folderNameLength = maxLengthNode.getIntValue();
        }
    }

    /**
     * validate method.
     * 
     * @param node node
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        logger.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     * 
     * @param node node
     * @param parent parent
     * @param at at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        logger.debug("validate( " + node + ", " + parent + ", " + at + ")");
        TYPE nodeType = TYPEFactory.getNodeType(node);
        if (nodeType != TYPE.STRING) {
            throw new JSONSchemaException(
                    "folderNameLength validation can only be executed on string!");
        }
        List<String> errors = new ArrayList<String>();
        String[] strArray = node.getTextValue().split("/");
        int subFolderValueLength = 0;
        for (String subFolderValue : strArray) {
            subFolderValueLength = subFolderValueLength
                    + subFolderValue.length();
            if (subFolderValueLength > this.folderNameLength) {
                errors.add(at + ": may only be " + this.folderNameLength
                        + " characters long");
            }
        }
        return errors;
    }
}
