/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.config.MxOSConfig;

import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * PatternValidator class for pattern validator.
 *
 * @author mxos-dev
 * 
 */
public class PatternValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 4460940200749213244L;
    private static Logger LOG = Logger.getLogger(PatternValidator.class);
    public static final String PROPERTY = "pattern";
    protected String pattern;

    /**
     * default constructor.
     *
     * @param patternNode
     *            patternNode
     */
    public PatternValidator(JsonNode patternNode) {
        this.pattern = "";
        if ((patternNode != null) && (patternNode.isTextual())) {
            this.pattern = patternNode.getTextValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");

        List<String> errors = new ArrayList<String>();

        TYPE nodeType = TYPEFactory.getNodeType(node);
        if (nodeType != TYPE.STRING) {
            errors.add(at + ": cannot match a " + nodeType
                    + " against a regex pattern (" + this.pattern + ")");
            return errors;
        }
        try {
            Pattern p = Pattern.compile(this.pattern);
            int regexTimeOut = MxOSConfig.getRegexValidationTimeout();
            Matcher m = createMatcherWithTimeout(node.getTextValue(), p, regexTimeOut);
            if (!m.matches()) {
                errors.add(at + ": does not match the regex pattern "
                        + this.pattern);
            }
        } catch (PatternSyntaxException pse) {
            LOG.error("Failed to apply pattern on " + at
                    + ": Invalid RE syntax [" + this.pattern + "]", pse);
        } catch (RuntimeException re) {
            errors.add("Failed to apply pattern on " + at
                    + ": Invalid RE syntax [" + this.pattern + "]");
            LOG.error("Failed to apply pattern on " + at
                    + ": Invalid RE syntax [" + this.pattern + "]", re);
        } catch (Exception e) {
            errors.add("Failed to apply pattern on " + at
                    + ": Invalid RE syntax [" + this.pattern + "]");
            LOG.error("Failed to apply pattern on " + at
                    + ": Invalid RE syntax [" + this.pattern + "]", e);
        }

        return errors;
    }
    /**
     * Method to create matcher with timeout.
     *
     * @param stringToMatch
     *            stringToMatch
     * @param regularExpressionPattern
     *            regularExpressionPattern
     * @param timeoutMillis
     *            timeoutMillis
     * @return matcher
     */
    public static Matcher createMatcherWithTimeout(String stringToMatch,
            Pattern regularExpressionPattern, int timeoutMillis) {
        CharSequence charSequence = new TimeoutRegexCharSequence(stringToMatch,
                timeoutMillis, stringToMatch,
                regularExpressionPattern.pattern());
        return regularExpressionPattern.matcher(charSequence);
    }

    private static class TimeoutRegexCharSequence implements CharSequence {

        private final CharSequence inner;

        private final int timeoutMillis;

        private final long timeoutTime;

        private final String stringToMatch;

        private final String regularExpression;

        public TimeoutRegexCharSequence(CharSequence inner, int timeoutMillis,
                String stringToMatch, String regularExpression) {
            super();
            this.inner = inner;
            this.timeoutMillis = timeoutMillis;
            this.stringToMatch = stringToMatch;
            this.regularExpression = regularExpression;
            timeoutTime = System.currentTimeMillis() + timeoutMillis;
        }

        @Override
        public char charAt(int index) {
            if (System.currentTimeMillis() > timeoutTime) {
                throw new RuntimeException("Timeout occurred after "
                        + timeoutMillis
                        + "ms while processing regular expression '"
                        + regularExpression + "' on input '" + stringToMatch
                        + "'!");
            }
            return inner.charAt(index);
        }

        @Override
        public int length() {
            return inner.length();
        }

        @Override
        public CharSequence subSequence(int start, int end) {
            return new TimeoutRegexCharSequence(inner.subSequence(start, end),
                    timeoutMillis, stringToMatch, regularExpression);
        }

        @Override
        public String toString() {
            return inner.toString();
        }
    }
}
