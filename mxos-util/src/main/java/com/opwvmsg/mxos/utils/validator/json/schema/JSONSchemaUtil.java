/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchemaProvider;
/**
 * Util to get JSON Schema object.
 * @author mxos-dev
 *
 */
public final class JSONSchemaUtil {
    /**
     * Default private constructor.
     */
    private JSONSchemaUtil() {
    }
    /**
     * Method to get JSONSchema object for given param.
     *
     * @param fileName fileName
     * @return JSONSchema JSONSchema
     * @throws MxOSException if given schema not found
     */
    public static JSONSchema getJSONSchema(String fileName)
        throws MxOSException {
        return getJSONSchema(MxOSConstants.GENERIC_APP_NAME, fileName);
    }
    /**
     * Method to get JSONSchema object for given param.
     *
     * @param schemaURL schemaURL
     * @return JSONSchema JSONSchema
     * @throws MxOSException if given schema not found
     */
    public static JSONSchema getJSONSchema(URL schemaURL) throws MxOSException {
        if (schemaURL == null || schemaURL.equals("")) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Requested param " + schemaURL + " schema not found");
        }
        InputStream schemaIS = null;
        try {
            schemaIS = new FileInputStream(schemaURL.getFile());
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        return getJSONSchema(schemaIS);
    }
    /**
     * Method to get JSONSchema object for given param.
     *
     * @param schemaIS schema inputStream
     * @return JSONSchema JSONSchema
     * @throws MxOSException if given schema not found
     */
    public static JSONSchema getJSONSchema(InputStream schemaIS)
        throws MxOSException {
        if (schemaIS == null) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Requested param " + schemaIS + " schema not found");
        }
        // and configured differently depending on the application
        ObjectMapper mapper = new ObjectMapper();
        // Allows to retrieve a JSONSchema object on various sources
        // supported by the ObjectMapper provided
        JSONSchemaProvider schemaProvider = new JacksonSchemaProvider(mapper);
        // Retrieves a JSON Schema object based on a file
        JSONSchema schema = schemaProvider.getSchema(schemaIS);
        return schema;
    }
    /**
     * Method to get JSONSchema object for given param.
     *
     * @param app app
     * @param fileName fileName
     * @return JSONSchema JSONSchema
     * @throws MxOSException if given schema not found
     */
    public static JSONSchema getJSONSchema(String app, String fileName)
        throws MxOSException {
        if (fileName == null || fileName.equals("")) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Requested param " + fileName + " schema not found");
        }
        String home = System.getProperty(MxOSConstants.MXOS_HOME);
        // Get json schema path from System properties.
        String jsonSchemaPath = System.getProperty("jsonschema.path");
        if (jsonSchemaPath == null || jsonSchemaPath.equals("")) {
            jsonSchemaPath = home + "/config/" + app + "/schema/attributes/";
        }
        InputStream is = null;
        try {
            is = new FileInputStream(jsonSchemaPath + fileName);
        } catch (IOException e) {
            // Check for the json attribut schema file in Generic MxOS.
            if (!app.equalsIgnoreCase(MxOSConstants.GENERIC_APP_NAME)) {
                return getJSONSchema(MxOSConstants.GENERIC_APP_NAME, fileName);
            }
        }
        return JSONSchemaUtil.getJSONSchema(is);
    }
    
    /**
     * Method to get JsonNode object for given param.
     * @param fileName
     * @return
     * @throws MxOSException
     * @throws Exception
     */
    public static JsonNode getJSONNode(String fileName) throws MxOSException,
            Exception {

        if (fileName == null || fileName.equals("")) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Requested param " + fileName + " schema not found");
        }

        final String app = MxOSConstants.GENERIC_APP_NAME;
        final String home = System.getProperty(MxOSConstants.MXOS_HOME);

        // Get json schema path from System properties.
        String jsonSchemaPath = System.getProperty("jsonschema.path");

        if (jsonSchemaPath == null || jsonSchemaPath.equals("")) {
            jsonSchemaPath = home + "/config/" + app + "/schema/attributes/";
        }
        InputStream is = new FileInputStream(jsonSchemaPath + fileName);

        ObjectMapper mapper = new ObjectMapper();
        JSONSchemaProvider schemaProvider = new JacksonSchemaProvider(mapper);
        JsonNode node = schemaProvider.getNode(is);
        return node;
    }
}
