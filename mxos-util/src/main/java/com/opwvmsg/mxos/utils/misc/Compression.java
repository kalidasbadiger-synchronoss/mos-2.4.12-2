/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.misc;

/**
 * This class is responsible for compression/decompression based on the type of
 * algorithm provided.
 *
 * @author mxos-dev
 */
public class Compression {

    /**
     * Compress data.
     *
     * @param compressionType
     *            Compression algorithm type.
     * @param nonCompressedData
     *            Data to compress.
     * @return returns compressed data in byte array format.
     */
    public static byte[] compress(CompressionType compressionType,
            String nonCompressedData) {
        if (compressionType == CompressionType.quicklz) {
            return QuickLZ.compress(nonCompressedData.getBytes(), 1);
        }
        return nonCompressedData.getBytes();
    }

    /**
     * De-compress data.
     *
     * @param compressionType
     *            Compression algorithm type.
     * @param compressedData
     *            Data to de-compress.
     * @return returns de-compressed data in byte array format.
     */
    public static byte[] decompress(CompressionType compressionType,
            String compressedData) {
        if (compressionType == CompressionType.quicklz) {
            return QuickLZ.decompress(compressedData.getBytes());
        }
        return compressedData.getBytes();
    }

    /**
     * Utility class, private constructor.
     */
    private Compression() {

    }
}
