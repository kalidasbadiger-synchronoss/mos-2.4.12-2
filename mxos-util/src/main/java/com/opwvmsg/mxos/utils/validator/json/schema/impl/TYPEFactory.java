/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema.impl;

import com.opwvmsg.mxos.utils.validator.json.schema.JSONSchemaException;
import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;

import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.JsonNode;

/**
 * TYPEFactory is factory class for json type.
 *
 * @author mxos-dev
 *
 */
public final class TYPEFactory {
    /**
     * Default private constructor.
     */
    private TYPEFactory() { }
    /**
     * Method to get type.
     * @param node node
     * @return TYPE TYPE
     */
    public static TYPE getType(JsonNode node) {
        JsonNode typeNode = node.get("type");
        if (node.isTextual()) {
            typeNode = node;
        }
        if (typeNode == null) {
            throw new JSONSchemaException(
                    "Invalid schema provided: property type is not defined!");
        }

        if (typeNode.isTextual()) {
            String type = typeNode.getTextValue();
            if ("object".equals(type)) {
                return TYPE.OBJECT;
            }
            if ("array".equals(type)) {
                return TYPE.ARRAY;
            }
            if ("string".equals(type)) {
                return TYPE.STRING;
            }
            if ("double".equals(type)) {
                return TYPE.DOUBLE;
            }
            if ("integer".equals(type)) {
                return TYPE.INTEGER;
            }
            if ("long".equals(type)) {
                return TYPE.LONG;
            }
            if ("float".equals(type)) {
                return TYPE.FLOAT;
            }
            if ("boolean".equals(type)) {
                return TYPE.BOOLEAN;
            }
            if ("any".equals(type)) {
                return TYPE.ANY;
            }
            if ("null".equals(type)) {
                return TYPE.NULL;
            }
            if ("date".equals(type)) {
                return TYPE.DATE;
            }
        }

        if (typeNode.isArray()) {
            return TYPE.UNION;
        }

        return TYPE.UNKNOWN;
    }
    /**
     * method to get union type.
     * @param node node
     * @return type array
     */
    public static TYPE[] getUnionType(JsonNode node) {
        JsonNode typeNode = node.get("type");
        if (typeNode == null) {
            return new TYPE[] {TYPE.UNKNOWN};
        }
        if (typeNode.isTextual()) {
            return new TYPE[] {getType(node)};
        }
        if (typeNode.isArray()) {
            List<TYPE> unionTypes = new ArrayList<TYPE>();
            for (JsonNode n : typeNode) {
                TYPE t = getType(n);
                if (t == TYPE.UNION) {
                    throw new RuntimeException(
                            "Recursive union types are not"
                            + " allowed in JSON Schema.");
                }
                unionTypes.add(t);
            }
            return (TYPE[]) unionTypes.toArray(new TYPE[0]);
        }
        return new TYPE[] {TYPE.UNKNOWN};
    }
    /**
     * method to get node type.
     * @param node node
     * @return type type
     */
    public static TYPE getNodeType(JsonNode node) {
        if (node.isContainerNode()) {
            if (node.isObject()) {
                return TYPE.OBJECT;
            }
            if (node.isArray()) {
                return TYPE.ARRAY;
            }
            return TYPE.UNKNOWN;
        }

        if (node.isValueNode()) {
            if (node.isInt()) {
                return TYPE.INTEGER;
            }
            if (node.isLong()) {
                return TYPE.LONG;
            }
            if (node.isDouble()) {
                return TYPE.DOUBLE;
            }
            if (node.isFloatingPointNumber()) {
                return TYPE.FLOAT;
            }
            if (node.isBoolean()) {
                return TYPE.BOOLEAN;
            }
            if (node.isTextual()) {
                return TYPE.STRING;
            }
            if (node.isNull()) {
                return TYPE.NULL;
            }
            return TYPE.UNKNOWN;
        }

        return TYPE.UNKNOWN;
    }
}
