/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/config/ConfigService.java#1 $
 */
package com.opwvmsg.mxos.utils.config;

/**
 * Interface for ConfigService.
 *
 * @author Aricent (partially used from RM)
 *
 */
public interface ConfigService {
    /**
     * Method to get Config instance.
     */
    void getConfig();

}
