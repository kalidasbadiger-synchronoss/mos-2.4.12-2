/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.TYPE;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.TYPEFactory;

/**
 * TypeValidator class for type validator.
 *
 * @author mxos-dev
 *
 */
public class TypeValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = -637068450453946655L;
    private static Logger LOG = Logger.getLogger(TypeValidator.class);
    public static final String PROPERTY = "type";
    private final TYPE schemaType;
    private UnionTypeValidator unionTypeValidator;

    /**
     * default constructor.
     *
     * @param schemaNode
     *            schemaNode
     */
    public TypeValidator(JsonNode schemaNode) {
        this.schemaType = TYPEFactory.getType(schemaNode);

        if (this.schemaType == TYPE.UNION) {
            this.unionTypeValidator = new UnionTypeValidator(
                    schemaNode.get("type"));
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (this.schemaType == TYPE.UNION) {
            errors.addAll(this.unionTypeValidator.validate(node, parent, at));
            return errors;
        }

        TYPE nodeType = TYPEFactory.getNodeType(node);
        if (nodeType != this.schemaType) {
            if (this.schemaType == TYPE.ANY) {
                return errors;
            }
            if (this.schemaType == TYPE.LONG) {
                if (null != node && null != node.getTextValue()
                        && node.getTextValue().length() > 0) {
                    try {
                        Long.parseLong(node.getTextValue());
                    } catch (Exception e) {
                        errors.add(at + ": " + node.getTextValue()
                                + " not a valid " + this.schemaType);
                    }
                }
                return errors;
            }
            if (this.schemaType == TYPE.DATE) {
                return errors;
            }
            if (this.schemaType == TYPE.INTEGER) {
                if (null != node && null != node.getTextValue()
                        && node.getTextValue().length() > 0) {
                    try {
                        Integer.parseInt(node.getTextValue());
                    } catch (Exception e) {
                        errors.add(at + ": " + node.getTextValue()
                                + " not a valid " + this.schemaType);
                    }
                }
                return errors;
            }
            if (this.schemaType == TYPE.DOUBLE) {
                try {
                    Double.parseDouble(node.getTextValue());
                } catch (Exception e) {
                    errors.add(at + ": " + node.getTextValue()
                            + " not a valid " + this.schemaType);
                }
                return errors;
            }
            if (this.schemaType == TYPE.FLOAT) {
                try {
                    Float.parseFloat(node.getTextValue());
                } catch (Exception e) {
                    errors.add(at + ": " + node.getTextValue()
                            + " not a valid " + this.schemaType);
                }
                return errors;
            }
            errors.add(at + ": " + nodeType + " found, " + this.schemaType
                    + " expected");
        }

        return errors;
    }
}
