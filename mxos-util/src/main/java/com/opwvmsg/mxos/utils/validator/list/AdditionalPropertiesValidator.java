/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;
import com.opwvmsg.mxos.utils.validator.json.schema.impl.JacksonSchema;

/**
 * AdditionalPropertiesValidator class for json validator.
 *
 * @author mxos-dev
 * 
 */
public class AdditionalPropertiesValidator implements JSONValidator,
        Serializable {
    private static final long serialVersionUID = 7868457793256399879L;
    public static final String PROPERTY = "additionalProperties";
    private static Logger LOG = Logger
            .getLogger(AdditionalPropertiesValidator.class);
    private boolean allowAdditionalProperties;
    private JacksonSchema additionalPropertiesSchema;
    private final List<String> allowedProperties;

    /**
     * default constructor.
     *
     * @param propertiesNode
     *            propertiesNode
     * @param additionalPropertiesNode
     *            additionalPropertiesNode
     */
    public AdditionalPropertiesValidator(JsonNode propertiesNode,
            JsonNode additionalPropertiesNode) {
        this.allowAdditionalProperties = false;
        if (additionalPropertiesNode.isBoolean()) {
            this.allowAdditionalProperties = additionalPropertiesNode
                    .getBooleanValue();
        }
        if (additionalPropertiesNode.isObject()) {
            this.allowAdditionalProperties = true;
            this.additionalPropertiesSchema = new JacksonSchema(
                    additionalPropertiesNode);
        }

        this.allowedProperties = new ArrayList<String>();
        for (Iterator<String> it = propertiesNode.getFieldNames(); it.hasNext();)
        {
            this.allowedProperties.add(it.next());
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();
        for (Iterator<String> it = node.getFieldNames(); it.hasNext();) {
            String pname = it.next();
            if (!this.allowedProperties.contains(pname)) {
                if (!this.allowAdditionalProperties) {
                    errors.add(at + "." + pname
                            + ": is not defined in the schema and the"
                            + " schema does not allow additional properties");
                } else if (this.additionalPropertiesSchema != null) {
                    errors.addAll(this.additionalPropertiesSchema.validate(
                            node.get(pname), parent, at + "." + pname));
                }
            }
        }
        return errors;
    }
}
