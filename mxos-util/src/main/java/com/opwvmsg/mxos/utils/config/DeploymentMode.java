package com.opwvmsg.mxos.utils.config;

public enum DeploymentMode {
    generic, bgc;

    public static DeploymentMode getMode(String s) {
        DeploymentMode value = null;
        try {
            if (null == s) {
                throw new IllegalArgumentException();
            }
            value = DeploymentMode.valueOf(s);
        } catch (IllegalArgumentException e) {
            value = DeploymentMode.generic;
        }
        return value;
    }
}
