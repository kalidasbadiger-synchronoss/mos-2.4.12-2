/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.json.schema;

import java.io.InputStream;
import java.io.Reader;
import java.net.URL;
import java.util.List;

/**
 * JSONSchema class for Json Schema validation.
 *
 * @author mxos-dev
 *
 */
public interface JSONSchema {
    /**
     * Validate with String param.
     * @param paramString paramString
     * @return list list
     */
    List<String> validate(String paramString);

    /**
     * Validate with InputStream param.
     * @param paramInputStream paramInputStream
     * @return list list
     */
    List<String> validate(InputStream paramInputStream);

    /**
     * Validate with Reader param.
     * @param paramReader paramReader
     * @return list list
     */
    List<String> validate(Reader paramReader);

    /**
     * Validate with URL param.
     * @param paramURL paramURL
     * @return list list
     */
    List<String> validate(URL paramURL);
}
