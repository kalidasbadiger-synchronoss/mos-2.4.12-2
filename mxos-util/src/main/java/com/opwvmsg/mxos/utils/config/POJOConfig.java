/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/config/POJOConfig.java#1 $
 */

package com.opwvmsg.mxos.utils.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import com.opwvmsg.utils.paf.config.Config;
import com.opwvmsg.utils.paf.config.ConfigException;

/**
 * POJO based config implementation.
 *
 * The setup parameter is expected to be a map of string key-value pairs in
 * config.db. Multi-value entries are represented using newline character as
 * a delimiter.
 *
 * The lookup order is as follows:
 * <ol>
 *  <li>/host/app/key
 *  <li>/*\/app/key
 *  <li>/host/common/key
 *  <li>/*\/common/key
 * </ol>
 *
 * @see POJOConfigProvider
 * @author Aricent (partially used from RM)
 */
public class POJOConfig extends Config {

    private static final int CACHE_SIZE = 1024;
    private Cache cache = new Cache(CACHE_SIZE);
    private Map<String, String> map = new HashMap<String, String>();

    /**
     * Constructs a new POJOConfig.
     *
     * @param setup key-value pairs in config.db
     */
    POJOConfig(Map<String, String> setup) {
        for (Map.Entry<String, String> entry : setup.entrySet()) {
            map.put(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Returns true if the specified key is resolvable, otherwise false.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return true if the specified key is resolvable, otherwise false
     */
    @Override
    public boolean contains(String host, String app, String key) {
        try {
            return lookup(host, app, key) != null;
        } catch (ConfigException e) {
            return false;
        }
    }

    /**
     * Returns string value for the specified key.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return string representation of the value
     * @throws ConfigException the key is not resolvable
     */
    @Override
    public String get(String host, String app, String key)
        throws ConfigException {

        return lookup(host, app, key);
    }

    /**
     * Returns string array for the specified key.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return string array representation of the value
     * @throws ConfigException the key is not resolvable
     */
    @Override
    public String[] getArray(String host, String app, String key)
        throws ConfigException {
        return StringUtils.split(lookup(host, app, key), '\n');
    }

    /**
     * Returns boolean value for the specified key.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return boolean representation of the value
     * @throws ConfigException the key is not resolvable
     */
    @Override
    public boolean getBoolean(String host, String app, String key)
        throws ConfigException {

        String value = lookup(host, app, key);
        return "true".equalsIgnoreCase(value) || "1".equals(value);
    }

    /**
     * Returns float value for the specified key.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return float representation of the value
     * @throws ConfigException the key is not resolvable
     */
    @Override
    public float getFloat(String host, String app, String key)
        throws ConfigException {

        try {
            return Float.parseFloat(lookup(host, app, key));
        } catch (NumberFormatException e) {
            throw new ConfigException("float conversion error", e);
        }
    }

    /**
     * Returns integer value for the specified key.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     * @return integer representation of the value
     * @throws ConfigException the key is not resolvable
     */
    @Override
    public int getInt(String host, String app, String key)
        throws ConfigException {

        try {
            return Integer.parseInt(lookup(host, app, key));
        } catch (NumberFormatException e) {
            throw new ConfigException("int conversion error", e);
        }
    }

    /**
     * Removes a cached value from the configuration cache.
     *
     * @param host host name in the config key
     * @param app app name in the config key
     * @param key key name in the config key
     */
    @Override
    public void removeCachedValue(String host, String app, String key) {
        cache.remove(("/" + host + "/" + app + "/" + key));
    }

    private String lookup(String host, String app, String key)
        throws ConfigException {

        host = host != null ? host.toLowerCase() : getDefaultHost();
        app = app != null ? app.toLowerCase() : getDefaultApp();
        //key = key.toLowerCase();

        String lookupKey = "/" + host + "/" + app + "/" + key;
        // Lookup from the cache first.
        String value = cache.get(lookupKey);
        if (value != null) {
            return value;
        }

        // Lookup from the map, then cache it if found.
        value = map.get(lookupKey);
        if (value != null) {
            cache.put(lookupKey, value);
            return value;
        }

        value = map.get("/*/" + app + "/" + key);
        if (value != null) {
            cache.put(lookupKey, value);
            return value;
        }

        value = map.get("/" + host + "/common/" + key);
        if (value != null) {
            cache.put(lookupKey, value);
            return value;
        }

        value = map.get("/*/common/" + key);
        if (value != null) {
            cache.put(lookupKey, value);
            return value;
        }

        throw new ConfigException("'" + key + "' not found");
    }

    /**
     * LRU Cache.
     */
    private static class Cache extends LinkedHashMap<String, String> {
        private static final long serialVersionUID = 6022815236466648042L;

        private int max;

        Cache(int max) {
            super(max, 1.0F, true);
            this.max = max;
        }

        @Override
        protected boolean removeEldestEntry(Map.Entry<String, String> eldest) {
            return size() > max;
        }
    }

}
