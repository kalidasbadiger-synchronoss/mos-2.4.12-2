/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.utils.misc;

import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.Operation;

/**
 * Class containing stat event details like statName, time taken by the
 * application for executing the operation described by the stat, stat status
 * (e.g. pass, fail or timeout) and stat validity such as valid or invalid.
 *
 * @author mxos-dev
 **/
public class StatEvent {
    private ServiceEnum statName;
    private long time;
    private StatStatus statStatus;
    private StatValidity statValidity;
    private Operation operationName;

    /**
     * Constructor.
     *
     * @param statName StatName for this statEvent.
     * @param time time taken by the application for executing the operation
     *            described by the stat.
     * @param statStatus stat status (e.g. pass, fail or timeout).
     * @param statValidity such as valid or invalid.
     */

    public StatEvent(ServiceEnum statName, Operation operationName, long time,
            StatStatus statStatus, StatValidity statValidity) {
        this.statName = statName;
        this.operationName = operationName;
        this.time = time;
        this.statStatus = statStatus;
        this.statValidity = statValidity;
    }

    /**
     * Method for updating the existing statEvent with new details.
     *
     * @param statName StatName for this statEvent.
     * @param time time taken by the application for executing the operation
     *            described by the stat.
     * @param statStatus stat status (e.g. pass, fail or timeout).
     * @param statValidity such as valid or invalid.
     */
    public void updateEvent(ServiceEnum statName, Operation operationName,
            long time, StatStatus statStatus, StatValidity statValidity) {
        this.statName = statName;
        this.operationName = operationName;
        this.time = time;
        this.statStatus = statStatus;
        this.statValidity = statValidity;
    }

    /**
     * Method for getting stats validity such as it is a valid or invalid stat.
     *
     * @return true if the stat is valid else false.
     */
    public boolean isValidEvent() {

        return this.statValidity == StatValidity.valid;
    }

    /**
     * Method for getting stats name for the statEvent.
     *
     * @return statName stats name for the statEvent.
     */
    public ServiceEnum getName() {
        return this.statName;
    }

    public Operation getOperation() {
        return this.operationName;
    }

    /**
     * Method for getting response time for the statEvent.
     *
     * @return time response time for the statEvent.
     */
    public long getTime() {
        return this.time;
    }

    /**
     * Method for getting status for the statEvent.
     *
     * @return statStatus stat status (e.g. pass, fail and timeout).
     */
    public StatStatus getStatus() {
        return this.statStatus;
    }

    /**
     * Method for invalidating the stat event.
     */
    public void invalidate() {
        this.statValidity = StatValidity.invalid;
    }
}
