/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.utils.validator.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.utils.validator.json.schema.impl.JSONValidator;

/**
 * DivisibleByValidator class for Divisible By validator.
 *
 * @author mxos-dev
 * 
 */
public class DivisibleByValidator implements JSONValidator, Serializable {
    private static final long serialVersionUID = 8255703049783662141L;
    private static Logger LOG = Logger.getLogger(DivisibleByValidator.class);
    public static final String PROPERTY = "divisibleBy";
    private long divisor = 0L;

    /**
     * default constructor.
     *
     * @param divisibleByNode
     *            divisibleByNode
     */
    public DivisibleByValidator(JsonNode divisibleByNode) {
        if (divisibleByNode.isIntegralNumber()) {
            this.divisor = divisibleByNode.getLongValue();
        }
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, String at) {
        LOG.debug("validate( " + node + ", " + at + ")");
        return validate(node, null, at);
    }

    /**
     * validate method.
     *
     * @param node
     *            node
     * @param parent
     *            parent
     * @param at
     *            at
     * @return list list
     */
    @Override
    public List<String> validate(JsonNode node, JsonNode parent, String at) {
        LOG.debug("validate( " + node + ", " + parent + ", " + at + ")");
        List<String> errors = new ArrayList<String>();

        if (node.isIntegralNumber()) {
            long nodeValue = node.getLongValue();
            if ((this.divisor != 0L) && (nodeValue % this.divisor != 0L)) {
                errors.add(at + ": must be divisible by " + this.divisor);
            }
        }
        return errors;
    }
}
