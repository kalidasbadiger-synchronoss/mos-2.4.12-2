/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.utils.datastore.hazelcast;

import com.opwvmsg.mxos.utils.datastore.IClusterIdGenerator;

import static com.opwvmsg.mxos.utils.datastore.hazelcast.HazelcastProvider.getIdGenerator;

/**
 * Hazelcast IdGenerator creates cluster-wide unique IDs. Generated IDs are long
 * type primitive values between 0 and Long.MAX_VALUE. Generated IDs are unique
 * during the life cycle of the cluster. If the entire cluster is restarted, IDs
 * start from 0 again.
 * 
 * @author
 */
class ClusterIdGenerator implements IClusterIdGenerator {

    @Override
    public long newId() {
        return getIdGenerator().newId();
    }
}
