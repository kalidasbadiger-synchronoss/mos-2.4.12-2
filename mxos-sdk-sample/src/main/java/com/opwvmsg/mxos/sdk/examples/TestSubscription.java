package com.opwvmsg.mxos.sdk.examples;

import com.opwvmsg.mxos.data.enums.NotificationProperty;
import com.opwvmsg.mxos.interfaces.service.*;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;

import java.util.*;

public class TestSubscription {

    private static String topic = "1234 56?";
    private static String callbackURL = "http://192.168.0.104:6701/7001/33852bcf-2987-3644-898a-c9a45510dc96/37477412-7c63-11e2-a46b-9c0aaaaed21a";
    //private static String callbackURL = "http://192.168.0.104:6701/%25";
    
    public static void main(String[] args) throws Exception {
        Properties p = new Properties();
        p.setProperty(ContextProperty.MXOS_CONTEXT_TYPE, ContextEnum.REST.name());
        p.setProperty(ContextProperty.MXOS_BASE_URL, "http://10.13.16.82:8081/mxos");
        p.setProperty(ContextProperty.MXOS_MAX_CONNECTIONS, "50");
        IMxOSContext context = MxOSContextFactory.getInstance().getContext("MXOS-SDK-2.1.51", p);
        ISubscriptionsService subscriptionsService = (ISubscriptionsService) context
                .getService(ServiceEnum.SubscriptionsService.name());
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(NotificationProperty.topic.name(), new ArrayList<String>());
        requestMap.get(NotificationProperty.topic.name()).add(topic);
        requestMap.put(NotificationProperty.subscription.name(), new ArrayList<String>());
        requestMap.get(NotificationProperty.subscription.name()).add(callbackURL);
        subscriptionsService.create(requestMap);
        System.out.println("Subscription Created...\n");
        requestMap.clear();
        requestMap.put(NotificationProperty.topic.name(), new ArrayList<String>());
        requestMap.get(NotificationProperty.topic.name()).add(topic);
        requestMap.put(NotificationProperty.subscription.name(), new ArrayList<String>());
        requestMap.get(NotificationProperty.subscription.name()).add(callbackURL);
        subscriptionsService.delete(requestMap);
        System.out.println("Subscription Deleted...\n");
    }
}