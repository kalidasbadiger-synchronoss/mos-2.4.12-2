/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.sdk.examples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;

/**
 * Utility class for Folder APIs.
 * 
 * @author mxos-dev
 */
public class FolderAPI {

    /**
     * Create Folder.
     * 
     * @param context mxosContext
     * @param email email
     * @param folderName folderName
     * @throws MxOSException on any error
     */
    public static void create(final IMxOSContext context, final String email,
            final String folderName) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);
        requestMap.put(FolderProperty.folderName.name(),
                new ArrayList<String>());
        requestMap.get(FolderProperty.folderName.name()).add(folderName);

        IFolderService folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        folderService.create(requestMap);
        System.out.println("Folder created...\n");
    }

    /**
     * Read Folder.
     * 
     * @param context mxosContext
     * @param email email
     * @param folderName folderName
     * @return Folder object
     * @throws MxOSException on any error
     */
    public static Folder read(final IMxOSContext context, final String email,
            final String folderName) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);
        requestMap.put(FolderProperty.folderName.name(),
                new ArrayList<String>());
        requestMap.get(FolderProperty.folderName.name()).add(folderName);

        IFolderService folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        Folder obj = folderService.read(requestMap);
        System.out.println("Folder Read...\n");
        return obj;
    }

    /**
     * Read All Folders.
     * 
     * @param context mxosContext
     * @param email email
     * @return List of Folder objects
     * @throws MxOSException on any error
     */
    public static List<Folder> readAll(final IMxOSContext context,
            final String email) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);

        IFolderService folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        List<Folder> obj = folderService.list(requestMap);
        System.out.println("Folder List...\n");
        return obj;
    }

    /**
     * Delete Folder.
     * 
     * @param context mxosContext
     * @param email email
     * @param folderName folderName
     * @throws MxOSException on any error
     */
    public static void delete(final IMxOSContext context, final String email,
            final String folderName) throws MxOSException {
        Map<String, List<String>> requestMap = new HashMap<String, List<String>>();
        requestMap.put(MailboxProperty.email.name(), new ArrayList<String>());
        requestMap.get(MailboxProperty.email.name()).add(email);
        requestMap.put(FolderProperty.folderName.name(),
                new ArrayList<String>());
        requestMap.get(FolderProperty.folderName.name()).add(folderName);

        IFolderService folderService = (IFolderService) context
                .getService(ServiceEnum.FolderService.name());
        folderService.delete(requestMap);
        System.out.println("Folder Deleted...\n");
    }

}
