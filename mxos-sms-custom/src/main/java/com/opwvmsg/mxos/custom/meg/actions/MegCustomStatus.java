/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.custom.meg.actions;

/**
 * Class for AccountStatus.
 * 
 * @author mxos-dev
 */
public enum MegCustomStatus {
    notMigrated(2), migrated(2), inMigration(0);

    private int prevIndex = 0;

    /**
     * The param prevIndex to make the update atomic.
     * For Example, we have notMigrated(2), this means we will update the
     * status to notMigrated only if the previous status is inMigration i.e. 2.
     * 
     * @param prevIndex
     */
    private MegCustomStatus(int prevIndex) {
        this.prevIndex = prevIndex;
    }

    public int previousStatus() {
        return this.prevIndex;
    }
}
