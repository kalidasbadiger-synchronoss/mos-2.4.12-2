/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaFactory.java#1 $
 */

package com.opwvmsg.mxos.custom.meg.crud;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.log4j.Logger;

/**
 * MySQL connection pool factory to access MEG data. This is required by Apache
 * object pool.
 * 
 * @author mxos-dev
 */
class MySQLMegFactory extends BasePoolableObjectFactory<MySQLMegStatusCRUD> {
    protected static Logger logger = Logger.getLogger(MySQLMegFactory.class);

    /**
     * Constructor
     */
    public MySQLMegFactory() {
    }

    @Override
    public MySQLMegStatusCRUD makeObject() throws Exception {
        return new MySQLMegStatusCRUD();
    }

    @Override
    public void destroyObject(MySQLMegStatusCRUD metaMySQLCRUD)
            throws Exception {
        metaMySQLCRUD.close();
    }

    @Override
    public boolean validateObject(final MySQLMegStatusCRUD metaMySQLCRUD) {
        return (metaMySQLCRUD.getActiveHostIndex() == MySQLConnectionManager
                .getInstance().getActiveHostIndex());
    }
}
