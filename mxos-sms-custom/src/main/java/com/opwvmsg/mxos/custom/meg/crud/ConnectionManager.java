/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.custom.meg.crud;

import java.util.List;

/**
 * Abstract ConnectionManager class that manages the retry mechanism and
 * fallback to the next host in the list
 * 
 * @author mxos-dev
 */
public abstract class ConnectionManager<T> {

    /**
     * Holds the connection Object and host index with which the connection has
     * been establised.
     * 
     * @author mxos-dev
     */
    public class ConnectionAndHost<T> {
        private T connection; // Connection Object
        private int connIndex; // Index of the host with which the connection
                               // has been established.

        /**
         * Constructor
         */
        public ConnectionAndHost(final T connection, final int connIndex) {
            this.connection = connection;
            this.connIndex = connIndex;
        }

        /**
         * @return the connection
         */
        public Object getConnection() {
            return connection;
        }

        /**
         * @return the connIndex
         */
        public int getConnIndex() {
            return connIndex;
        }

    }

    /**
     * Class that holds the host and port.
     * 
     * @author mxos-dev
     */
    public class HostPort {
        private String host;
        private String port;

        /**
         * Constructor
         * 
         * @param host
         * @param port
         */
        public HostPort(final String host, final String port) {
            this.host = host;
            this.port = port;
        }

        /**
         * @return the host
         */
        public String getHost() {
            return host;
        }

        /**
         * @return the port
         */
        public String getPort() {
            return port;
        }

    }

    // Threshold value for retry before switching to the next host in the list.
    protected int threshold;

    // List of hosts,ports that are configured.
    protected List<HostPort> hostPortList;

    // Index of the present active host
    protected int activeHostIndex = 0;

    // Failure counter. After this counter reaches threshold, switches to the
    // next host in the list
    private int failureCounter = 0;

    /**
     * Creates a new connection based with the activeHost. If failed to make a
     * connection with the activehost, will fallback to the next host in the
     * list.
     * 
     * @return ConnectionAndHost
     */
    protected ConnectionAndHost<T> getConnection() {
        final T connection = makeConnection();

        if (connection == null) {
            // If failed to make a connection with the active host, updates the
            // failure counter. If the failure counter reaches the threshold,
            // switches to the next host in the list
            failureCounter++;
            if (failureCounter >= threshold) {
                failureCounter = 0;
                activeHostIndex = (activeHostIndex + 1) % (hostPortList.size());
            }
            return null;
        } else {
            failureCounter = 0;
        }
        return new ConnectionAndHost<T>(connection, activeHostIndex);
    }

    /**
     * @return the activeHostIndex
     */
    public int getActiveHostIndex() {
        return activeHostIndex;
    }

    /**
     * Abstract class to creates the connection with the active Host.
     * 
     * @return Connection
     */
    public abstract T makeConnection();
}
