/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */

package com.opwvmsg.mxos.custom.meg.crud;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.custom.meg.actions.ICustomStatusCRUD;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * This class provides connection pool for MySQL.
 * 
 * @author mxos-dev
 */
public class MegConnectionPool implements ICRUDPool<ICustomStatusCRUD> {
    private static Logger logger = Logger.getLogger(MegConnectionPool.class);

    private static class MegConnectionPoolHolder {
        public static MegConnectionPool instance = new MegConnectionPool();
    }

    private static GenericObjectPool<MySQLMegStatusCRUD> objPool = null;

    /**
     * Method to get Instance of MySQLMegConnectionPool object.
     * 
     * @return MySQLMegConnectionPool object
     * @throws Exception Exception
     */
    public static MegConnectionPool getInstance() {
        return MegConnectionPoolHolder.instance;
    }

    /**
     * Constructor.
     * 
     * @param mysqlPropertyFile Object pool parameters property file.
     * @throws FileNotFoundException FileNotFoundException.
     * @throws IOException IOException.
     */
    private MegConnectionPool() {
        int maxConnections = Integer.parseInt(System
                .getProperty(SystemProperty.megMaxConnections.name()));
        objPool = new GenericObjectPool<MySQLMegStatusCRUD>(
                new MySQLMegFactory(), maxConnections);
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
        if (logger.isDebugEnabled()) {
            logger.debug("MegConnectionPool created.");
        }
        initializeJMXStats(maxConnections);
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    @Override
    public ICustomStatusCRUD borrowObject() throws MxOSException {
        ICustomStatusCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("# Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     * @throws Exception on any error
     */
    @Override
    public void returnObject(final ICustomStatusCRUD megCRUD)
            throws MxOSException {
        try {
            objPool.returnObject((MySQLMegStatusCRUD) megCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("# Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.MEG.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_MEG.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_MEG.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        if (null != objPool) {
            objPool.clear();
            logger.info("# Clearing the pools..");
        }
    }
}
