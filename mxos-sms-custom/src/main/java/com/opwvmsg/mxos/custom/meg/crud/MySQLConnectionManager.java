/*
 * Copyright (c) 2013 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in
 * accordance with the terms and conditions stipulated in the
 * agreement/contract under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.custom.meg.crud;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import java.sql.DriverManager;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException;

/**
 * MYSQL Connection Manager Implementation class
 * 
 * @author mxos-dev
 */
public final class MySQLConnectionManager extends ConnectionManager<Connection> {

    private static Logger logger = Logger
            .getLogger(MySQLConnectionManager.class);

    private String userName; // MySql Username
    private String password; // MySql Password
    private String mysqlDBName; // MySql DB Name

    /**
     * Constructor
     */
    private MySQLConnectionManager() {
        String mysqlHostPort = System
                .getProperty(MySQLConstants.MYSQL_HOST_AND_PORT);
        threshold = Integer.parseInt(System
                .getProperty(MySQLConstants.MYSQL_PERHOST_FAILURE_ATTEMPTS));
        password = System.getProperty(MySQLConstants.PASSWORD);
        userName = System.getProperty(MySQLConstants.USER);
        mysqlDBName = System.getProperty(MySQLConstants.MYSQL_DB_NAME);

        hostPortList = new ArrayList<HostPort>();
        StringTokenizer st = new StringTokenizer(mysqlHostPort, ";");
        while (st.hasMoreElements()) {
            String object = (String) st.nextElement();
            String[] hostPort = object.split(":");
            hostPortList.add(new HostPort(hostPort[0], hostPort[1]));
        }
        activeHostIndex = 0;
    }

    private static class MySQLConnectionPoolHolder {
        public static MySQLConnectionManager instance = new MySQLConnectionManager();
    }

    /**
     * Method to get Instance of MySQLMegConnectionPool object.
     * 
     * @return MySQLMegConnectionPool object
     */
    public static MySQLConnectionManager getInstance() {
        return MySQLConnectionPoolHolder.instance;
    }

    @Override
    public Connection makeConnection() {
        Connection connection;
        try {
            Class.forName("com.mysql.jdbc.Driver");
                
            String connectionURL = "jdbc:mysql://"
                    + hostPortList.get(activeHostIndex).getHost() + ":"
                    + hostPortList.get(activeHostIndex).getPort() + "/"
                    + mysqlDBName;
            
            // connect to the database using replication driver
            connection = DriverManager.getConnection(connectionURL, userName,
                    password);
            if (connection != null) {
                connection.setAutoCommit(false);
                connection
                        .setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
                if (logger.isDebugEnabled()) {
                    logger.debug("MySQLMegStatusCRUD object created.");
                }
            } else {
                throw new Exception("Error while connecting MEG");
            }
        } catch (final CommunicationsException e) {
            logger.error("CommunicationsException while connecting the MEG", e);
            return null;
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error(
                    "MySQLNonTransientConnectionException while connecting the MEG",
                    e);
            return null;
        } catch (final Exception e) {
            logger.error("Exception while connecting MEG", e);
            return null;
        }
        return connection;
    }

}
