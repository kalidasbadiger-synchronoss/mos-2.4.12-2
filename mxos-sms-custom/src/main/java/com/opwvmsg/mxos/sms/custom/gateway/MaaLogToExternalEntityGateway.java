/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.gateway;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.sms.custom.config.LogToExternalEntityConfigBean;
import com.opwvmsg.mxos.sms.custom.response.MaaResponseBean;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Logging Gateway implementation that sends the event to the External MAA.
 * Interface.
 * 
 * @author mxos-dev
 */
public class MaaLogToExternalEntityGateway implements
        ILogToExternalEntityGateway {

    private static Logger logger = Logger
            .getLogger(MaaLogToExternalEntityGateway.class);
    private LogToExternalEntityConfigBean configBean;

    /**
     * Constructor
     * 
     * @param configBean LogToExternalEntityConfigBean
     */
    public MaaLogToExternalEntityGateway(
            final LogToExternalEntityConfigBean configBean) {
        this.configBean = configBean;
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "MaaLogToExternalEntityGateway process start."));
        }

        ICRUDPool<HttpClient> pool = null;
        HttpClient client = null;
        final HttpPost httpPost = CustomActionUtils.createMaaHttpPost(
                configBean, inputParams);

        StringBuffer maaResponse = new StringBuffer();
        try {
            pool = MxOSApp.getInstance().getMAAConnectionPool();
            if (pool == null) {
                throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                        "MAA is currently disabled.");
            }
            client = pool.borrowObject();
            if (logger.isDebugEnabled()) {
                logger.debug("URI to be called :  " + httpPost.getURI());
            }
            client.getParams().setIntParameter(
                    CoreConnectionPNames.CONNECTION_TIMEOUT,
                    configBean.getTimeout());
            client.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT,
                    configBean.getTimeout());
            HttpResponse response1 = client.execute(httpPost);
            int returnCode = response1.getStatusLine().getStatusCode();
            if (logger.isDebugEnabled()) {
                logger.debug("return Code: " + returnCode);
            }

            BufferedReader bufferRead = null;
            if (returnCode == HttpStatus.SC_OK) {
                bufferRead = new BufferedReader(new InputStreamReader(response1
                        .getEntity().getContent()));
                String xmlLine = null;
                while (((xmlLine = bufferRead.readLine()) != null)) {
                    maaResponse.append(xmlLine);
                }
                if (logger.isInfoEnabled()) {
                    logger.info("MAA Response = " + maaResponse);
                }
            }
        } catch (MxOSException e) {
            logger.error(e);
            ConnectionErrorStats.MAA.increment();
            throw e;
        } catch (IOException e) {
            logger.error(e);
            ConnectionErrorStats.MAA.increment();
            throw new ApplicationException(
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(),
                    "IO Exception while sending the message");
        } finally {
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
            if (pool != null) {
                try {
                    pool.returnObject(client);
                } catch (MxOSException e) {
                    logger.warn("Error in finally clause while returing "
                            + "Provisioning CRUD pool.", e);
                }
            }
        }

        final MaaResponseBean responseBean = CustomActionUtils
                .parseMaaResponse(maaResponse);

        if (logger.isInfoEnabled()) {
            logger.info("Response Bean received from MAA:"
                    + responseBean.toString());
        }

        if (!responseBean.getCode().equalsIgnoreCase(
                SmsConstants.MAA_RESPONSE_SUCCESS_CODE)) {
            throw new SmsGatewayException(
                    CustomError.LOG_TO_EXTERNAL_ENTITY_ERROR.name(),
                    responseBean.getCode() + ":" + responseBean.getMessage());
        } else {
            logger.info("Log Event sent successfully to MAA Interface ");
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "MaaLogToExternalEntityGateway process end."));
        }

    }
}
