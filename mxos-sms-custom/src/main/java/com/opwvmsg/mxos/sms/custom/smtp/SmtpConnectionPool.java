/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.smtp;

import java.util.Properties;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * This class provides connection pool for SMTP Server.
 * 
 * @author mxos-dev
 */
public class SmtpConnectionPool {

    private static Logger logger = Logger.getLogger(SmtpConnectionPool.class);
    private GenericObjectPool<SmtpConnection> objPool;
    private Properties properties;

    /**
     * Constructor.
     * 
     * @param host String
     * @param port String
     * @param maxConnections int
     */
    public SmtpConnectionPool(final String host, final String port,
            final int maxConnections, 
            Properties properties) {
        this.properties = properties;
        try {
            objPool = new GenericObjectPool<SmtpConnection>(new SmtpFactory(
                    host, port, properties), maxConnections);
            objPool.setMaxIdle(-1);
            objPool.setTestOnBorrow(true);
        } catch (Exception e) {
            logger.error(e);
        }
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    public SmtpConnection borrowObject() throws Exception {
        try {
            SmtpConnection obj = objPool.borrowObject();
            return obj;
        } catch (Exception e) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    /**
     * Method to return the connection back.
     * 
     * @param smtpSend SmtpConnection
     * @throws Exception
     */
    public void returnObject(SmtpConnection smtpSend) throws Exception {
        try {
            if (properties != null
                    && Boolean.valueOf(properties
                            .getProperty(SmsConstants.MAIL_SMTP_AUTH))) {
                smtpSend.close();
                objPool.returnObject((SmtpConnection) smtpSend);
                objPool.invalidateObject(smtpSend);
            } else {
                objPool.returnObject((SmtpConnection) smtpSend);
            }
        } catch (Exception e) {
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }
}
