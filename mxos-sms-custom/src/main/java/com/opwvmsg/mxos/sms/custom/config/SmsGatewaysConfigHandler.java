/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;
import org.apache.commons.io.IOUtils;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * The SmsGatewaysConfigHandler class is used to load smsGateways.
 * configurations configured.
 * 
 * @author Aricent
 */
public final class SmsGatewaysConfigHandler {
    private static Logger logger = Logger
            .getLogger(SmsGatewaysConfigHandler.class);

    /**
     * Default Constructor.
     * 
     * @param app name of the application
     */
    public SmsGatewaysConfigHandler(String app) {
    }

    /**
     * Method to load the SMS Gateway Config XML file.
     * 
     * @param app String
     * @param smsGatewayConf Map<String, Map<String, Object>>
     * @throws MxOSException
     */
    public static void loadSmsGatewayConfigFromXML(final String fileName,
            Map<String, Map<String, Object>> smsGatewayConf)
            throws MxOSException {

        final String home = System.getProperty(SmsConstants.MXOS_HOME);

        InputStream is = null;

        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        final String configFile = home + "/config/" + fileName;

        logger.debug("gateway-config.xml File being loaded: " + configFile);

        try {
            final File file = new File(configFile);
            is = new FileInputStream(file.getCanonicalPath());
            final boolean ok = SmsGatewaysConfigParsar
                    .parse(is, smsGatewayConf);
            logger.info("loadSmsGatewayConfigFromXML"
                    + "(gateway-config.xml) ret : " + ok);
        } catch (Exception e) {
            logger.error("Error while loading gateway-config.xml", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Error while loading gateway-config.xml.");

        } finally {
            IOUtils.closeQuietly(is);
        }
    }

}
