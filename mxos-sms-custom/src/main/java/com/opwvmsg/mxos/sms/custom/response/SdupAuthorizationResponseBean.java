/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.response;

/**
 * SDUPAuthorization Response Bean Class.
 * 
 * @author mxos-dev
 */
public class SdupAuthorizationResponseBean {

    private int statusCode;
    private String statusMessage;
    private String status;
    private String serviceProvider;
    private String serviceProviderType;
    private String SMSMoBarring;

    /**
     * Constructor.
     */
    public SdupAuthorizationResponseBean() {
        super();
        this.statusCode = 0;
        this.statusMessage = null;
        this.status = null;
        this.serviceProvider = null;
        this.serviceProviderType = null;
        this.SMSMoBarring = null;
    }

    /**
     * @return the statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode the statusCode to set
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * @param statusMessage the statusMessage to set
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the serviceProvider
     */
    public String getServiceProvider() {
        return serviceProvider;
    }

    /**
     * @param serviceProvider the serviceProvider to set
     */
    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    /**
     * @return the serviceProviderType
     */
    public String getServiceProviderType() {
        return serviceProviderType;
    }

    /**
     * @param serviceProviderType the serviceProviderType to set
     */
    public void setServiceProviderType(String serviceProviderType) {
        this.serviceProviderType = serviceProviderType;
    }

    /**
     * @return the sMSMoBarring
     */
    public String getSMSMoBarring() {
        return SMSMoBarring;
    }

    /**
     * @param sMSMoBarring the sMSMoBarring to set
     */
    public void setSMSMoBarring(String sMSMoBarring) {
        SMSMoBarring = sMSMoBarring;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SdupAuthorizationResponseBean [statusCode=" + statusCode
                + ", statusMessage=" + statusMessage + ", status=" + status
                + ", serviceProvider=" + serviceProvider
                + ", serviceProviderType=" + serviceProviderType
                + ", SMSMoBarring=" + SMSMoBarring + "]";
    }

}
