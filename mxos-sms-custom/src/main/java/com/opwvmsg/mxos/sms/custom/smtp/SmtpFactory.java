/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.smtp;

import java.util.Properties;

import org.apache.commons.pool.BasePoolableObjectFactory;

import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * SMTP connection pool factory to talk over SMTP.
 * 
 * @author mxos-dev
 */
class SmtpFactory extends BasePoolableObjectFactory<SmtpConnection> {
    private final String host;
    private final String port;
    private Properties properties;
    
    public SmtpFactory(final String host, final String port,Properties properties) {
        this.host = host;
        this.port = port;
        this.properties = properties;
    }

    @Override
    public SmtpConnection makeObject() throws Exception {
        return new SmtpConnection(host, port, properties);
    }

    @Override
    public void destroyObject(SmtpConnection smtpConn) throws Exception {
        smtpConn.close();
    }

    @Override
    public boolean validateObject(final SmtpConnection smtpConn) {
        return smtpConn.isConnected();
    }
}
