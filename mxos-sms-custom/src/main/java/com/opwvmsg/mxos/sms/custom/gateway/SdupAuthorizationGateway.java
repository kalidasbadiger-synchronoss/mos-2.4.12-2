/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.gateway;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import be.belgacom.mobile.sdup.service.reader.xsd.SdupReaderServiceResponse;

import com.opwvmsg.mxos.data.enums.SmsProperty;
import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.SmsGatewayException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.sms.custom.config.AuthorizationConfigBean;
import com.opwvmsg.mxos.sms.custom.response.SdupAuthorizationResponseBean;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Authorize MSISDN implementation class that authorizes the MSISDN using SDUP.
 * External Interface.
 * 
 * @author mxos-dev
 */
public class SdupAuthorizationGateway implements IAuthorizationGateway {
    private static Logger logger = Logger
            .getLogger(SdupAuthorizationGateway.class);
    private AuthorizationConfigBean configBean;

    /**
     * Constructor with parameter configBean.
     * 
     * @param configBean AuthorizationConfigBean
     */
    public SdupAuthorizationGateway(final AuthorizationConfigBean configBean) {
        this.configBean = configBean;
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {

        final String msisdn = inputParams.get(
                SmsProperty.authorizeMsisdn.name()).get(0);

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SdupAuthorizationGateway process start."));
            logger.debug("configBean : " + configBean.toString());
        }

        final SdupReaderServiceResponse response = CustomActionUtils
                .sendSDUPRequest(configBean, msisdn);

        logger.info("Successfully sent the request to SDUP Server for MSISDN : "
                + msisdn);

        if (response == null) {
            ConnectionErrorStats.SDUP.increment();
            throw new SmsGatewayException(
                    CustomError.MSISDN_AUTHORIZE_PERMANENT_ERROR.name(),
                    "Null response received from SDUP Server");
        }

        final SdupAuthorizationResponseBean responseBean = CustomActionUtils
                .parseSDUPResponse(response);

        logger.info("Response from SDUP server for MSISDN : "
                + inputParams.get(SmsProperty.authorizeMsisdn.name()) + " : "
                + responseBean.toString());

        if (responseBean.getStatusCode() != SmsConstants.SDUP_RESPONSE_SUCCESS_CODE) {
            if (CustomActionUtils.isErrorCodeInList(
                    configBean.getTempErrorList(),
                    String.valueOf(responseBean.getStatusCode()))) {
                logger.error("SDUP Temporary Error received for MSISDN : "
                        + msisdn + " . Status Code: "
                        + responseBean.getStatusCode());
                throw new SmsGatewayException(
                        CustomError.MSISDN_AUTHORIZE_TEMPORARY_ERROR.name(),
                        responseBean.getStatusCode() + ":"
                                + responseBean.getStatusMessage());
            } else {
                logger.error("SDUP Permanent Error received for MSISDN : "
                        + msisdn + " . Status Code: "
                        + responseBean.getStatusCode());
                ConnectionErrorStats.SDUP.increment();
                throw new SmsGatewayException(
                        CustomError.MSISDN_AUTHORIZE_PERMANENT_ERROR.name(),
                        responseBean.getStatusCode() + ":"
                                + responseBean.getStatusMessage());
            }
        }

        if (CustomActionUtils.isValidMsisdn(configBean, responseBean) == false) {
            logger.error("SDUP Authorization failed for msisdn : " + msisdn);

            throw new ApplicationException(
                    CustomError.MSISDN_AUTHORIZE_PERMANENT_ERROR.name(),
                    "SDUP Authorization Failed");
        }

        logger.info("Authorization passed for msisdn : " + msisdn);

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SdupAuthorizationGateway process end."));
        }
    }
}
