/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.action.process;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.MAAUtils;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MessageLocale;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MsisdnMessageId;

/**
 * Action class for SwapMSISDN.
 * 
 * @author mxos-dev
 */
public class SwapMSISDN implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SwapMSISDN.class);
    private Map<String, List<String>> params = 
        new HashMap<String, List<String>>();

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug("SwapMSISDN action start.");
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String currentMSISDN = requestState.getInputParams()
                    .get(MailboxProperty.currentMSISDN.name()).get(0);
            final String newMSISDN = requestState.getInputParams()
                    .get(MailboxProperty.newMSISDN.name()).get(0);

            String filter = MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MSISDN
                    .replace("{" + MailboxProperty.msisdn.name() + "}",
                            currentMSISDN);
            requestState.getInputParams().put(MailboxProperty.query.name(),
                    new ArrayList<String>());
            requestState.getInputParams().get(MailboxProperty.query.name())
                    .add(filter);
            final List<Base> bases = mailboxCRUD.searchMailbox(
                    requestState, 1, maxTimeOut);

            if (null == bases || bases.isEmpty()) {
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_SEARCH.name());
            }
            Base base = bases.get(0);
            final String locale = mailboxCRUD.readGeneralPreferences(
                    base.getEmail(), requestState).getLocale();
            MessageLocale msgLocale = MessageLocale.getByValue(locale);
            String mailSubject = CustomActionUtils
                    .getMsisdnMessageSubject(msgLocale);
            requestState.getInputParams().put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            requestState.getInputParams().get(MailboxProperty.email.name())
                    .add(base.getEmail());
            if (currentMSISDN.equals(base.getMsisdn())) {
                // For Proximus Account
                // Change the msisdn to newMSISDN
                params.clear();
                IMailboxBaseService baseService = (IMailboxBaseService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.MailboxBaseService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.msisdn.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.msisdn.name()).add(newMSISDN);
                baseService.update(params);
                final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                        requestState, mailboxCRUD);
                String mailContent = CustomActionUtils.getMsisdnMessageContent(
                        MsisdnMessageId.MBX_MSG_PROXIMUS_SWAP_MSISDN,
                        msgLocale, currentMSISDN, newMSISDN);
                CustomActionUtils.depositMail(requestState, base.getEmail(),
                        mailSubject, mailContent, mssInfo
                                .getMessageStoreHosts().get(0), mssInfo
                                .getMailboxId(), msgLocale.name());
                MAAUtils.logToMailAPIAdapter("msisdnSwapFlow", base.getEmail(),
                        "smsServicesMsisdn swapped", "200");
            }
            final Credentials cred = mailboxCRUD.readCredentials(base
                    .getEmail(), requestState);
            if (currentMSISDN.equals(cred.getPasswordRecoveryMsisdn())) {
                // For Password recovery
                PasswordRecoveryPreference prp = cred
                        .getPasswordRecoveryPreference();

                // Change the msisdn to newMSISDN
                params.clear();
                ICredentialService credService = (ICredentialService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.CredentialService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.passwordRecoveryMsisdn.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.passwordRecoveryMsisdn.name()).add(
                        newMSISDN);
                credService.update(params);

                if (null != prp
                        && (PasswordRecoveryPreference.ALL.equals(prp) 
                                || PasswordRecoveryPreference.SMS
                                .equals(prp))) {
                    final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                            requestState, mailboxCRUD);
                    String mailContent = CustomActionUtils
                            .getMsisdnMessageContent(
                                    MsisdnMessageId
                                    .MBX_MSG_PWD_RECOVERY_SWAP_MSISDN,
                                    msgLocale, currentMSISDN, newMSISDN);
                    CustomActionUtils.depositMail(requestState,
                            base.getEmail(), mailSubject, mailContent, mssInfo
                                    .getMessageStoreHosts().get(0), mssInfo
                                    .getMailboxId(), msgLocale.name());
                }
                MAAUtils.logToMailAPIAdapter("msisdnSwapFlow", base.getEmail(),
                        "passwordRecoveryMsisdn swapped", "200");
            }
            final SmsServices sms = mailboxCRUD.readSmsServices(base
                    .getEmail(), requestState);
            if (currentMSISDN.equals(sms.getSmsServicesMsisdn())) {
                // For SMS features
                // Change the msisdn to newMSISDN
                params.clear();
                ISmsServicesService smsService = (ISmsServicesService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.SmsServicesService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.smsServicesMsisdn.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.smsServicesMsisdn.name()).add(
                        newMSISDN);
                smsService.update(params);

                if (BooleanType.YES.equals(sms.getSmsOnline()
                        .getSmsOnlineEnabled())
                        || BooleanType.YES.equals(sms.getSmsNotifications()
                                .getSmsAdvancedNotificationsEnabled())
                        || BooleanType.YES.equals(sms.getSmsNotifications()
                                .getSmsBasicNotificationsEnabled())) {
                    final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                            requestState, mailboxCRUD);
                    String mailContent = CustomActionUtils
                            .getMsisdnMessageContent(
                                    MsisdnMessageId
                                    .MBX_MSG_SMS_FEATURE_SWAP_MSISDN,
                                    msgLocale, currentMSISDN, newMSISDN);
                    CustomActionUtils.depositMail(requestState,
                            base.getEmail(), mailSubject, mailContent, mssInfo
                                    .getMessageStoreHosts().get(0), mssInfo
                                    .getMailboxId(), msgLocale.name());
                }
                MAAUtils.logToMailAPIAdapter("msisdnSwapFlow", base.getEmail(),
                        "smsServicesMsisdn swapped", "200");
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while swapping msisdn.", e);
            throw new InvalidRequestException(ErrorCode
                    .GEN_BAD_REQUEST.name());
        } finally {
            requestState.getInputParams().remove(MailboxProperty.query.name());
            requestState.getInputParams().remove(MailboxProperty.email.name());
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SwapMSISDN action end.");
        }
    }

}
