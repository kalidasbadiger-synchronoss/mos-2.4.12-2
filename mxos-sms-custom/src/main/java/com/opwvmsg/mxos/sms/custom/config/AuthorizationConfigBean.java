/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.config;

import java.util.Arrays;

/**
 * Config Bean class with the properties for MSISDN Authorization.
 * 
 * @author mxos-dev
 */
public class AuthorizationConfigBean {

    private String provider;
    private String type;
    private String endPointUrl;
    private String clientApp;
    private boolean dogetUp;
    private String serviceProvider;
    private String serviceProviderType;
    private String status;
    private String smsMoBarring;
    private String tempErrorList;
    private String nameSpaceUri;
    private String serviceName;

    private int[] ccOffset;
    private int[] ndcOffset;
    private int[] hlrOffset;
    private int[] regionOffset;

    /**
     * Default Constructor.
     */
    public AuthorizationConfigBean() {
        ccOffset = new int[2];
        ndcOffset = new int[2];
        hlrOffset = new int[2];
        regionOffset = new int[2];
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the endPointUrl
     */
    public String getEndPointUrl() {
        return endPointUrl;
    }

    /**
     * @param endPointUrl the endPointUrl to set
     */
    public void setEndPointUrl(String endPointUrl) {
        this.endPointUrl = endPointUrl;
    }

    /**
     * @return the clientApp
     */
    public String getClientApp() {
        return clientApp;
    }

    /**
     * @param clientApp the clientApp to set
     */
    public void setClientApp(String clientApp) {
        this.clientApp = clientApp;
    }

    /**
     * @return the dogetUp
     */
    public boolean getDogetUp() {
        return dogetUp;
    }

    /**
     * @param dogetUp the dogetUp to set
     */
    public void setDogetUp(boolean dogetUp) {
        this.dogetUp = dogetUp;
    }

    /**
     * @return the serviceProvider
     */
    public String getServiceProvider() {
        return serviceProvider;
    }

    /**
     * @param serviceProvider the serviceProvider to set
     */
    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    /**
     * @return the serviceProviderType
     */
    public String getServiceProviderType() {
        return serviceProviderType;
    }

    /**
     * @param serviceProviderType the serviceProviderType to set
     */
    public void setServiceProviderType(String serviceProviderType) {
        this.serviceProviderType = serviceProviderType;
    }

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the smsMoBarring
     */
    public String getSmsMoBarring() {
        return smsMoBarring;
    }

    /**
     * @param smsMoBarring the smsMoBarring to set
     */
    public void setSmsMoBarring(String smsMoBarring) {
        this.smsMoBarring = smsMoBarring;
    }

    /**
     * @return the tempErrorList
     */
    public String getTempErrorList() {
        return tempErrorList;
    }

    /**
     * @param tempErrorList the tempErrorList to set
     */
    public void setTempErrorList(String tempErrorList) {
        this.tempErrorList = tempErrorList;
    }

    /**
     * @return the ccOffset
     */
    public int[] getCcOffset() {
        return ccOffset;
    }

    /**
     * @param offset the ccOffset to set
     */
    public void setCcOffset(String offset) {
        String[] splitCc = offset.split(",");
        this.ccOffset[0] = Integer.parseInt(splitCc[0]);
        this.ccOffset[1] = Integer.parseInt(splitCc[1]);
    }

    /**
     * @return the ndcOffset
     */
    public int[] getNdcOffset() {
        return ndcOffset;
    }

    /**
     * @param offset the ndcOffset to set
     */
    public void setNdcOffset(String offset) {
        String[] splitNdc = offset.split(",");
        this.ndcOffset[0] = Integer.parseInt(splitNdc[0]);
        this.ndcOffset[1] = Integer.parseInt(splitNdc[1]);
    }

    /**
     * @return the hlrOffset
     */
    public int[] getHlrOffset() {
        return hlrOffset;
    }

    /**
     * @param offset the hlrOffset to set
     */
    public void setHlrOffset(String offset) {
        String[] splitHlr = offset.split(",");
        this.hlrOffset[0] = Integer.parseInt(splitHlr[0]);
        this.hlrOffset[1] = Integer.parseInt(splitHlr[1]);
    }

    /**
     * @return the regionOffset
     */
    public int[] getRegionOffset() {
        return regionOffset;
    }

    /**
     * @param offset the regionOffset to set
     */
    public void setRegionOffset(String offset) {

        String[] splitRegion = offset.split(",");
        this.regionOffset[0] = Integer.parseInt(splitRegion[0]);
        this.regionOffset[1] = Integer.parseInt(splitRegion[1]);
    }

    /**
     * @return the nameSpaceUri
     */
    public String getNameSpaceUri() {
        return nameSpaceUri;
    }

    /**
     * @param nameSpaceUri the nameSpaceUri to set
     */
    public void setNameSpaceUri(String nameSpaceUri) {
        this.nameSpaceUri = nameSpaceUri;
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @param serviceName the serviceName to set
     */
    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("AuthorizationConfigBean [provider=").append(provider)
                .append(", type=").append(type).append(", endPointUrl=")
                .append(endPointUrl).append(", clientApp=").append(clientApp)
                .append(", dogetUp=").append(dogetUp)
                .append(", serviceProvider=").append(serviceProvider)
                .append(", serviceProviderType=").append(serviceProviderType)
                .append(", status=").append(status).append(", smsMoBarring=")
                .append(smsMoBarring).append(", tempErrorList=")
                .append(tempErrorList).append(", nameSpaceUri=")
                .append(nameSpaceUri).append(", serviceName=")
                .append(serviceName).append(", ccOffset=")
                .append(Arrays.toString(ccOffset)).append(", ndcOffset=")
                .append(Arrays.toString(ndcOffset)).append(", hlrOffset=")
                .append(Arrays.toString(hlrOffset)).append(", regionOffset=")
                .append(Arrays.toString(regionOffset)).append("]");
        return builder.toString();
    }

}
