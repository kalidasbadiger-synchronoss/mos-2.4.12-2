/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.smtp;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * SMTP Connection factory. Maintains a map of SMTP Connection Pools.
 * 
 * @author mxos-dev
 */
public class SmtpConnectionPoolFactory {

    // Map of SMTP Connection Pools
    private Map<String, SmtpConnectionPool> smtpPools = null;

    public static SmtpConnectionPoolFactory instance = null;

    /**
     * Method to create a singleton instance.
     * 
     * @return SmtpConnectionPoolFactory
     * @throws MxOSException
     */
    public static SmtpConnectionPoolFactory getInstance() 
    throws MxOSException {

        if (instance == null) {
            instance = new SmtpConnectionPoolFactory();
        }

        return instance;
    }

    /**
     * Constructor.
     */
    private SmtpConnectionPoolFactory() {
        smtpPools = new LinkedHashMap<String, SmtpConnectionPool>();
    }

    /**
     * Method that returns the appropriate connection pool based on the.
     * poolType.
     * 
     * @param poolType String
     * @param host String
     * @param port String
     * @param maxConn int
     * @return SmtpConnectionPool
     * @throws MxOSException
     */
    public SmtpConnectionPool getSMTPPool(final String poolType,
            final String host, final String port, int maxConn, Properties properties) {

        if (smtpPools != null) {
            if (smtpPools.get(poolType) == null) {
                // If the pool is not created, create it and return to the
                // caller.
                SmtpConnectionPool connPool = new SmtpConnectionPool(host,
                        port, maxConn, properties);
                smtpPools.put(poolType, connPool);
                if (SmsConstants.MMG_SMTP_POOL.equals(poolType)) {
                    ConnectionStats.MMG.setCount(maxConn);
                } else if (SmsConstants.MTA_SMTP_POOL.equals(poolType)) {
                    ConnectionStats.EXTERNAL_MTA.setCount(maxConn);
                }
            }
            return smtpPools.get(poolType);
        } else {
            return null;
        }
    }
}
