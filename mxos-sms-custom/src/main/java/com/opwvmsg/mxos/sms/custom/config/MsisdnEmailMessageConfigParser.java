/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.sms.custom.config;

import static com.opwvmsg.mxos.sms.custom.utils.SmsConstants.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.Location;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.gateway.MsisdnEmailMessageFactory.MsisdnMessageKey;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * The MsisdnEmailMessageConfigParser class used to load email messages for.
 * msisdn flow from configuration File.
 * 
 * @author mxos-dev
 */
public class MsisdnEmailMessageConfigParser extends DefaultHandler {

    private static Logger logger = Logger
            .getLogger(MsisdnEmailMessageConfigParser.class);

    /**
     * Method to load msisdn message config XML file.
     * 
     * @param fileName String
     * @param messageMap Map<String, String>
     * @throws MxOSException
     */
    public static void loadMessagesFromXML(final String fileName,
            Map<String, String> messageMap) throws MxOSException {
        final String home = System.getProperty(SmsConstants.MXOS_HOME);
        Reader reader = null;
        if (home == null) {
            throw new RuntimeException("${MXOS_HOME} must be set");
        }
        final String configFile = home + "/config/" + fileName;

        logger.debug(fileName + " file being loaded: " + configFile);

        try {
            final File file = new File(configFile);
            reader = new FileReader(file.getCanonicalPath());
            parse(reader, messageMap);
            logger.info(fileName + " parsed successfully.");
        } catch (Exception e) {
            logger.error("Error while loading " + fileName, e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Error while loading " + fileName);

        } finally {
            IOUtils.closeQuietly(reader);
        }
    }

    /**
     * Parse XML by using local file.
     * 
     * @param fileName String
     * @param messageMap Map<String, String>
     * @return boolean
     * @throws MxOSException
     */
    public static boolean parse(final String fileName,
            Map<String, String> messageMap) throws MxOSException {
        final XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(fileName, new FileInputStream(
                    fileName));
        } catch (FileNotFoundException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse " + MSISDN_EMAIL_MESSAGE_FILENAME
                            + e.getMessage());
        } catch (XMLStreamException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse " + MSISDN_EMAIL_MESSAGE_FILENAME
                            + e.getMessage());
        }
        return parse(xmlr, messageMap);
    }

    /**
     * Parse XML by using Reader.
     * 
     * @param reader Reader
     * @param messageMap Map<String, String>
     * @return boolean
     * @throws MxOSException
     */
    public static boolean parse(Reader reader, Map<String, String> messageMap)
            throws MxOSException {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(reader);
        } catch (XMLStreamException e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Failed to parse " + MSISDN_EMAIL_MESSAGE_FILENAME
                            + e.getMessage());
        }
        return parse(xmlr, messageMap);
    }

    /**
     * Parse the XML reader and create the Config Bean Objects.
     * 
     * @param xmlr XMLStreamReader
     * @param messageMap Map<String, String>
     * @return boolean
     * @throws MxOSException
     */
    private static boolean parse(XMLStreamReader xmlr,
            Map<String, String> messageMap) throws MxOSException {

        boolean ret = false;
        int eventType = 0;
        LOOP: try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                case XMLEvent.START_ELEMENT:
                    String startElName = xmlr.getLocalName();
                    if (MSISDN_MESSAGE_XML_TAG_MESSAGE.equals(startElName)) {
                        String messageId = xmlr.getAttributeValue(null,
                                MSISDN_MESSAGE_XML_TAG_MESSAGE_ID);
                        String language = xmlr.getAttributeValue(null,
                                MSISDN_MESSAGE_XML_TAG_MESSAGE_LANG);
                        String content = xmlr.getElementText();
                        messageMap.put(
                                new MsisdnMessageKey(messageId, language)
                                        .toString(), content);
                    }
                    break;
                case XMLEvent.END_DOCUMENT:
                    break LOOP;
                default:
                    break;
                }
                ret = true;
            }
        } catch (Exception e) {
            ret = false;
            Location loc = xmlr.getLocation();
            logger.error("Error in parsing " + MSISDN_EMAIL_MESSAGE_FILENAME
                    + getLocInfo(loc), e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        return ret;
    }

    /**
     * Return XML location info.
     * 
     * @param loc Location
     * @return String
     */
    private static String getLocInfo(Location loc) {
        return loc.getSystemId() + ", line:" + loc.getLineNumber() + ", col:"
                + loc.getColumnNumber() + ", off:" + loc.getCharacterOffset();
    }
}
