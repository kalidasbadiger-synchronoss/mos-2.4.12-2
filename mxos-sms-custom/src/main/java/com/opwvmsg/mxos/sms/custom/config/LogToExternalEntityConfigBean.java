/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.config;

/**
 * Config Bean class with the properties for Log to External Entity Service.
 * 
 * @author mxos-dev
 */
public class LogToExternalEntityConfigBean {

    private String type;
    private String provider;
    private String url;
    private String appLogin;
    private String appPassword;
    private int timeout;

    /**
     * Default Constructor.
     */
    public LogToExternalEntityConfigBean() {
        type = null;
        provider = null;
        url = null;
        appLogin = null;
        appPassword = null;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the provider
     */
    public String getProvider() {
        return provider;
    }

    /**
     * @param provider the provider to set
     */
    public void setProvider(String provider) {
        this.provider = provider;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the appLogin
     */
    public String getAppLogin() {
        return appLogin;
    }

    /**
     * @param appLogin the appLogin to set
     */
    public void setAppLogin(String appLogin) {
        this.appLogin = appLogin;
    }

    /**
     * @return the appPassword
     */
    public String getAppPassword() {
        return appPassword;
    }

    /**
     * @param appPassword the appPassword to set
     */
    public void setAppPassword(String appPassword) {
        this.appPassword = appPassword;
    }

    /**
     * @return the timeout
     */
    public int getTimeout() {
        return timeout;
    }

    /**
     * @param timeout the timeout to set
     */
    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("LogToExternalEntityConfigBean [type=").append(type)
                .append(", provider=").append(provider).append(", url=")
                .append(url).append(", appLogin=").append(appLogin)
                .append(", appPassword=").append(appPassword)
                .append(", timeout=").append(timeout).append("]");
        return builder.toString();
    }

}
