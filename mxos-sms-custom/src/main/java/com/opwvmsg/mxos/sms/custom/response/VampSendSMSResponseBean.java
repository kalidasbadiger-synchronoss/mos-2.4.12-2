/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.sms.custom.response;

/**
 * VAMP SendSMS Response Bean Class.
 * 
 * @author mxos-dev
 */
public class VampSendSMSResponseBean {

    private String code;
    private String message;

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Get the error info.
     * 
     * @return the Error Info
     */
    public String getErrorInfo() {
        return code + ":" + message;
    }

    /**
     * Method to return the bean data as string.
     * 
     * @return String bean data.
     */
    public String toString() {
        return "LoggingResponseBean : returnCode=" + code + ", message="
                + message;
    }

}
