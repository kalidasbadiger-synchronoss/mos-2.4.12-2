/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.gateway;

import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.config.MsisdnEmailMessageConfigParser;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;

/**
 * Factory class to get Context instance containing Msisdn Email Messages.
 *
 * @author mxos-dev
 */
public class MsisdnEmailMessageFactory {

    private Map<String, String> messageMap = null;
    public static MsisdnEmailMessageFactory instance = null;

    private static Logger logger = Logger
            .getLogger(MsisdnEmailMessageFactory.class);
    /**
     * 
     * @return MsisdnEmailMessageFactory
     * @throws MxOSException
     */
    public static MsisdnEmailMessageFactory getInstance() 
    throws MxOSException {
        if (instance == null) {
            instance = new MsisdnEmailMessageFactory();
        }
        return instance;
    }
    /**
     * Default Constructor.
     * 
     * @throws MxOSException
     */
    private MsisdnEmailMessageFactory() throws MxOSException {
        messageMap = new LinkedHashMap<String, String>();
        String fileName = SmsConstants.MSISDN_EMAIL_MESSAGE_FILENAME;
        MsisdnEmailMessageConfigParser.loadMessagesFromXML(fileName,
                messageMap);
        logger.info("Parsed " + fileName + " successfully");
    }

    /**
     * Method to get email message based on the messageId and language.
     *
     * @param messageId String
     * @param language String
     * @return message
     * @throws MxOSException
     */
    public String getMessage(final String messageId, final String language) {
        String message = null;
        if (messageMap != null) {
            message = messageMap.get(new MsisdnMessageKey(messageId,
                    language).toString());
        }
        return message;
    }

    /**
     * 
     * MsisdnMessageKey class.
     *
     */
    public final static class MsisdnMessageKey {
        private String messageId;
        private String language;
        /**
         * 
         * @param messageId String
         * @param language String
         */
        public MsisdnMessageKey(String messageId, String language) {
            this.messageId = messageId;
            this.language = language;
        }

        @Override
        public String toString() {
            return messageId + ":" + language;
        }
    }
}
