/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.sms.custom.action.process;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.MxosEnums.MsisdnStatus;
import com.opwvmsg.mxos.data.enums.MxosEnums.PasswordRecoveryPreference;
import com.opwvmsg.mxos.data.enums.MxosEnums.SmsServicesMsisdnStatusType;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailboxBaseService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsNotificationsService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsOnlineService;
import com.opwvmsg.mxos.interfaces.service.mailbox.ISmsServicesService;
import com.opwvmsg.mxos.sms.custom.utils.CustomActionUtils;
import com.opwvmsg.mxos.sms.custom.utils.MAAUtils;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MessageLocale;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants.MsisdnMessageId;

/**
 * Action class for ReactivateMSISDN.
 *
 * @author mxos-dev
 */
public class ReactivateMSISDN implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ReactivateMSISDN.class);
    private Map<String, List<String>> params =
        new HashMap<String, List<String>>();

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug("ReactivateMSISDN action start.");
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        String currentLDAPDate = new SimpleDateFormat(
                System.getProperty(SystemProperty.userDateFormat.name()))
                .format(new Date(System.currentTimeMillis()));
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String msisdn = requestState.getInputParams()
                    .get(MailboxProperty.msisdn.name()).get(0);
            String filter = MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MSISDN
                    .replace("{" + MailboxProperty.msisdn.name() 
                            + "}", msisdn);
            requestState.getInputParams().put(MailboxProperty.query.name(),
                    new ArrayList<String>());
            requestState.getInputParams().get(MailboxProperty.query.name())
                    .add(filter);
            final List<Base> bases = mailboxCRUD.searchMailbox(
                    requestState, 1, maxTimeOut);

            if (null == bases || bases.isEmpty()) {
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_SEARCH.name());
            }
            Base base = bases.get(0);
            final String locale = mailboxCRUD.readGeneralPreferences(
                    base.getEmail(), requestState).getLocale();
            MessageLocale msgLocale = MessageLocale.getByValue(locale);
            String mailSubject = CustomActionUtils
                    .getMsisdnMessageSubject(msgLocale);
            requestState.getInputParams().put(MailboxProperty.email.name(),
                    new ArrayList<String>());
            requestState.getInputParams().get(MailboxProperty.email.name())
                    .add(base.getEmail());
            if (msisdn.equals(base.getMsisdn())) {
                // For Proximus Account
                if (MxosEnums.Status.CLOSING.equals(base.getStatus())) {
                    requestState.getInputParams().put(MailboxProperty.status.name(),
                            new ArrayList<String>());
                    requestState.getInputParams().get(MailboxProperty.status.name())
                            .add(MxosEnums.Status.OPEN.name());
                    MAAUtils.changeMailboxStatusViaMAA(requestState.getInputParams());
                }
                // Update msisdnStatus and lastMsisdnStatusChangeDate
                params.clear();
                IMailboxBaseService baseService = (IMailboxBaseService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.MailboxBaseService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.msisdnStatus.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.msisdnStatus.name()).add(
                        MsisdnStatus.ACTIVATED.name());
                params.put(MailboxProperty.lastMsisdnStatusChangeDate.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.lastMsisdnStatusChangeDate.name())
                        .add(currentLDAPDate);
                baseService.update(params);
                final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                        requestState, mailboxCRUD);
                String mailContent = CustomActionUtils.getMsisdnMessageContent(
                        MsisdnMessageId.MBX_MSG_PROXIMUS_REACTIVATE_MSISDN,
                        msgLocale);
                CustomActionUtils.depositMail(requestState, base.getEmail(),
                        mailSubject, mailContent, mssInfo
                                .getMessageStoreHosts().get(0), mssInfo
                                .getMailboxId(), msgLocale.name());
                MAAUtils.logToMailAPIAdapter("msisdnReactivationFlow",
                        base.getEmail(), "msisdn reactivated", "200");
            }
            final Credentials cred = mailboxCRUD.readCredentials(base
                    .getEmail(), requestState);
            if (msisdn.equals(cred.getPasswordRecoveryMsisdn())) {
                // For Password recovery
                PasswordRecoveryPreference prpForUpdate = null;
                String pwdRecoveryEmail = cred.getPasswordRecoveryEmail();
                PasswordRecoveryPreference prp = cred
                        .getPasswordRecoveryPreference();
                if (null == prp) {
                    prp = PasswordRecoveryPreference.NONE;
                }
                switch (prp) {
                case EMAIL:
                    if (null != pwdRecoveryEmail
                            && 0 != pwdRecoveryEmail.length()) {
                        prpForUpdate = PasswordRecoveryPreference.ALL;
                        break;
                    }
                case NONE:
                    prpForUpdate = PasswordRecoveryPreference.SMS;
                    break;
                default:
                    break;
                }
                params.clear();
                if (null != prpForUpdate) {
                    // update prpForUpdate value to ldap.
                    params.put(
                            MailboxProperty.passwordRecoveryPreference.name(),
                            new ArrayList<String>());
                    params.get(
                            MailboxProperty.passwordRecoveryPreference.name())
                            .add(prpForUpdate.name());
                }
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.passwordRecoveryMsisdnStatus.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.passwordRecoveryMsisdnStatus.name())
                        .add(MsisdnStatus.ACTIVATED.name());
                params.put(
                        MailboxProperty.
                        lastPasswordRecoveryMsisdnStatusChangeDate
                                .name(), new ArrayList<String>());
                params.get(
                        MailboxProperty.
                        lastPasswordRecoveryMsisdnStatusChangeDate.name())
                        .add(currentLDAPDate);
                ICredentialService credService = (ICredentialService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.CredentialService.name());
                credService.update(params);
                final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                        requestState, mailboxCRUD);
                String mailContent = CustomActionUtils.getMsisdnMessageContent(
                        MsisdnMessageId.MBX_MSG_PWD_RECOVERY_REACTIVATE_MSISDN,
                        msgLocale);
                CustomActionUtils.depositMail(requestState, base.getEmail(),
                        mailSubject, mailContent, mssInfo
                                .getMessageStoreHosts().get(0), mssInfo
                                .getMailboxId(), msgLocale.name());
                MAAUtils.logToMailAPIAdapter("msisdnReactivationFlow",
                        base.getEmail(), "passwordRecoveryMsisdn reactivated",
                        "200");
            }
            final SmsServices sms = mailboxCRUD.readSmsServices(base
                    .getEmail(), requestState);
            if (msisdn.equals(sms.getSmsServicesMsisdn())) {
                // For SMS features
                ISmsOnlineService smsOnline = (ISmsOnlineService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.SmsOnlineService.name());
                params.clear();
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.smsOnlineEnabled.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.smsOnlineEnabled.name()).add(
                        MxosEnums.BooleanType.YES.name());
                smsOnline.update(params);

                // Disable smsBasicNotificationsEnabled and
                // smsAdvancedNotificationsEnabled
                params.clear();
                ISmsNotificationsService smsNotif =
                    (ISmsNotificationsService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum
                                .SmsNotificationsService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.smsBasicNotificationsEnabled.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.smsBasicNotificationsEnabled.name())
                        .add(MxosEnums.BooleanType.YES.name());
                params.put(
                        MailboxProperty.smsAdvancedNotificationsEnabled.name(),
                        new ArrayList<String>());
                params.get(
                        MailboxProperty.smsAdvancedNotificationsEnabled.name())
                        .add(MxosEnums.BooleanType.YES.name());
                smsNotif.update(params);

                // Update smsServicesMsisdnStatus and
                // lastSmsServicesMsisdnStatusChangeDate
                params.clear();
                ISmsServicesService smsService = (ISmsServicesService) MxOSApp
                        .getInstance().getContext()
                        .getService(ServiceEnum.SmsServicesService.name());
                params.put(MailboxProperty.email.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.email.name()).add(base.getEmail());
                params.put(MailboxProperty.smsServicesMsisdnStatus.name(),
                        new ArrayList<String>());
                params.get(MailboxProperty.smsServicesMsisdnStatus.name()).add(
                        SmsServicesMsisdnStatusType.ACTIVATED.name());
                params.put(
                        MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                                .name(), new ArrayList<String>());
                params.get(
                        MailboxProperty.lastSmsServicesMsisdnStatusChangeDate
                                .name()).add(currentLDAPDate);
                smsService.update(params);
                final MssLinkInfo mssInfo = ActionUtils.readMSSLinkInfo(
                        requestState, mailboxCRUD);
                String mailContent = CustomActionUtils.getMsisdnMessageContent(
                        MsisdnMessageId.MBX_MSG_SMS_FEATURE_REACTIVATE_MSISDN,
                        msgLocale);
                CustomActionUtils.depositMail(requestState, base.getEmail(),
                        mailSubject, mailContent, mssInfo
                                .getMessageStoreHosts().get(0), mssInfo
                                .getMailboxId(), msgLocale.name());
                MAAUtils.logToMailAPIAdapter("msisdnReactivationFlow",
                        base.getEmail(), "smsServicesMsisdn reactivated", "200");
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while reactivating msisdn.", e);
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name());
        } finally {
            requestState.getInputParams().remove(MailboxProperty.query.name());
            requestState.getInputParams().remove(MailboxProperty.email.name());
            requestState.getInputParams().remove(MailboxProperty.status.name());
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("ReactivateMSISDN action end.");
        }
    }

}
