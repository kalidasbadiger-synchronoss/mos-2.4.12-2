/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.sms.custom.smtp;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.opwvmsg.mxos.error.CustomError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.sms.custom.utils.SmsConstants;
import org.apache.log4j.Logger;

/**
 * SMTP Connection class. Contains the transport and Session Objects of the.
 * Connection.
 * 
 * @author mxos-dev
 */
public final class SmtpConnection {

    private static Logger logger = Logger.getLogger(SmtpConnection.class);
    private Transport transport;
    private Session session;
    private String host;
    private String port;
    private Properties properties;

    /**
     * Constructor.
     * 
     * @param host String
     * @param port String
     * @throws MessagingException
     */
    public SmtpConnection(String host, String port, Properties properties) throws MessagingException {
        this.host = host;
        this.port = port;
        this.properties = properties;
        this.transport = null;

    }

    /**
     * Connect to SMTP server without authentication.
     *
     * @throws MessagingException
     */
    public void connect() throws MessagingException {
        properties.put(SmsConstants.MAIL_SMTP_HOST, host);
        properties.put(SmsConstants.MAIL_SMTP_PORT, port);
        logger.info("SMTP connect without Authentication");
        session = Session.getInstance(properties);
        session.setDebug(Boolean.valueOf(properties
                .getProperty(SmsConstants.MAIL_SMTP_DEBUG)));
        transport = session.getTransport(SmsConstants.SMTP_TRANSPORT);
        transport.connect();
    }

    /**
     * Connect to SMTP server using credentials if authentication on server is
     * enabled.
     *
     * @param user user
     * @param password password
     * @throws MessagingException
     */
    public void connect(final String user, final String password)
            throws MessagingException {
        properties.put(SmsConstants.MAIL_SMTP_HOST, host);
        properties.put(SmsConstants.MAIL_SMTP_PORT, port);
        boolean isAuthEnabled = Boolean.parseBoolean(properties
                .getProperty(SmsConstants.MAIL_SMTP_AUTH));
        if (isAuthEnabled) {
            logger.info("SMTP connect with Authentication");
            session = Session.getInstance(properties);
            session.setDebug(Boolean.valueOf(properties
                    .getProperty(SmsConstants.MAIL_SMTP_DEBUG)));
            transport = session.getTransport(SmsConstants.SMTP_TRANSPORT);
            logger.info("user : " + user);
            transport.connect(user, password);
        } else {
            connect();
        }
    }

    public Session getSession() {
        return session;
    }

    /**
     * Close transport connection resource.
     */
    public void close() {
        // Close the transport Connection
        try {
            transport.close();
        } catch (MessagingException e) {
            logger.error("Error while closing SMTP connection : " +e);
        }
    }

    /**
     * Close transport connection resource.
     * 
     * @return boolean
     */
    public boolean isConnected() {
        if (transport != null) {
            return transport.isConnected();
        } else {
            return true;
        }
    }

    /**
     * Construct MIME message and Send using the SMTP Protocol.
     * 
     * @param fromAddress String
     * @param toAddress String
     * @param text String
     * @throws MxOSException
     * @throws AddressException
     * @throws MessagingException
     * @throws UnsupportedEncodingException
     */
    public void sendSMTPMessage(String fromAddress, String toAddress,
            String text) throws MxOSException, AddressException,
            MessagingException, UnsupportedEncodingException {
        MimeMessage message = null;
        message = new MimeMessage(session, new ByteArrayInputStream(
                text.getBytes(SmsConstants.SMTP_CONTENT_TYPE_ISO_8859)));

        if(toAddress != null) {
            message.setFrom(new InternetAddress(fromAddress));
            InternetAddress[] address = { new InternetAddress(toAddress) };
            message.setRecipients(Message.RecipientType.TO, address);
            message.saveChanges();
            transport.sendMessage(message, address);
        } else if(message.getAllRecipients() !=null) {
            transport.sendMessage(message, message.getAllRecipients());
        }else{
            throw new ApplicationException(CustomError.SEND_MAIL_INVALID_TO_ADDRESS.name());
        }
    }

}
