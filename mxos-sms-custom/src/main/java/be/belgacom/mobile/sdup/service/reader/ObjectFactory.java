/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import be.belgacom.mobile.sdup.data.xsd.MSISDN;
import be.belgacom.mobile.sdup.service.reader.xsd.SdupReaderServiceResponse;

/**
 * This object contains factory methods for each Java content 
 * interface and Java element interface generated in the be.
 * belgacom.mobile.sdup.service.reader package.
 * <p>
 * An ObjectFactory allows you to programatically construct new 
 * instances of the Java representation for XML content. The Java 
 * representation of XML content can consist of schema derived 
 * interfaces and classes representing the binding of schema 
 * type definitions, element declarations and model groups. Factory
 * methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindSubscriberByImsiResponseReturn_QNAME =
        new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "return");
    private final static QName _FindUserProfileByIDServiceId_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "serviceId");
    private final static QName _FindUserProfileByIDClientApp_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "clientApp");
    private final static QName _FindUserProfileByIDSubscriberID_QNAME = 
        new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "subscriberID");
    private final static QName _FindSubscriberByMsisdnMsisdn_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "msisdn");
    private final static QName _SdupExceptionSdupException_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "SdupException");
    private final static QName _FindSubscriberByImsiImsi_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "imsi");
    private final static QName _ExceptionException_QNAME = new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "Exception");
    private final static QName _FindSubscriberByIdSubcriberId_QNAME = 
        new QName(
            "http://reader.service.sdup.mobile.belgacom.be", "subcriberId");

    /**
     * Create a new ObjectFactory that can be used to create new instances of
     * schema derived classes for package:
     * be.belgacom.mobile.sdup.service.reader.
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FindUserProfileByID }.
     * 
     * @return FindUserProfileByID
     */
    public FindUserProfileByID createFindUserProfileByID() {
        return new FindUserProfileByID();
    }

    /**
     * Create an instance of {@link FindUserProfileByMsisdnResponse }.
     * 
     * @return FindUserProfileByMsisdnResponse
     */
    public FindUserProfileByMsisdnResponse createFindUserProfileByMsisdnResponse() {
        return new FindUserProfileByMsisdnResponse();
    }

    /**
     * Create an instance of {@link FindSubscribersByIdListResponse }.
     * 
     * @return FindSubscribersByIdListResponse
     */
    public FindSubscribersByIdListResponse createFindSubscribersByIdListResponse() {
        return new FindSubscribersByIdListResponse();
    }

    /**
     * Create an instance of {@link Exception }.
     * 
     * @return Exception
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindSubscriberByMsisdnList }.
     * 
     * @return FindSubscriberByMsisdnList
     */
    public FindSubscriberByMsisdnList createFindSubscriberByMsisdnList() {
        return new FindSubscriberByMsisdnList();
    }

    /**
     * Create an instance of {@link FindSubscribersByIdList }.
     * 
     * @return FindSubscribersByIdList
     */
    public FindSubscribersByIdList createFindSubscribersByIdList() {
        return new FindSubscribersByIdList();
    }

    /**
     * Create an instance of {@link FindSubscriberByMsisdnListResponse }.
     * 
     * @return FindSubscriberByMsisdnListResponse
     */
    public FindSubscriberByMsisdnListResponse createFindSubscriberByMsisdnListResponse() {
        return new FindSubscriberByMsisdnListResponse();
    }

    /**
     * Create an instance of {@link FindSubscriberByImsiResponse }.
     * 
     * @return FindSubscriberByImsiResponse
     */
    public FindSubscriberByImsiResponse createFindSubscriberByImsiResponse() {
        return new FindSubscriberByImsiResponse();
    }

    /**
     * Create an instance of {@link FindSubscriberByMsisdnResponse }.
     * 
     * @return FindSubscriberByMsisdnResponse
     */
    public FindSubscriberByMsisdnResponse 
    createFindSubscriberByMsisdnResponse() {
        return new FindSubscriberByMsisdnResponse();
    }

    /**
     * Create an instance of {@link FindSubscriberByMsisdn }.
     * 
     * @return FindSubscriberByMsisdn
     */
    public FindSubscriberByMsisdn createFindSubscriberByMsisdn() {
        return new FindSubscriberByMsisdn();
    }

    /**
     * Create an instance of
     * {@link be.belgacom.mobile.sdup.service.reader.SdupException }.
     * 
     * @return be.belgacom.mobile.sdup.service.reader.SdupException
     */
    public be.belgacom.mobile.sdup.service.reader.SdupException 
    createSdupException() {
        return new be.belgacom.mobile.sdup.service.reader.SdupException();
    }

    /**
     * Create an instance of {@link FindSubscriberByImsi }.
     * 
     * @return FindSubscriberByImsi
     */
    public FindSubscriberByImsi createFindSubscriberByImsi() {
        return new FindSubscriberByImsi();
    }

    /**
     * Create an instance of {@link FindUserProfileByMsisdn }.
     * 
     * @return FindUserProfileByMsisdn
     */
    public FindUserProfileByMsisdn createFindUserProfileByMsisdn() {
        return new FindUserProfileByMsisdn();
    }

    /**
     * Create an instance of {@link IsAliveResponse }.
     * 
     * @return IsAliveResponse
     */
    public IsAliveResponse createIsAliveResponse() {
        return new IsAliveResponse();
    }

    /**
     * Create an instance of {@link FindSubscriberById }.
     * 
     * @return FindSubscriberById
     */
    public FindSubscriberById createFindSubscriberById() {
        return new FindSubscriberById();
    }

    /**
     * Create an instance of {@link FindUserProfileByIDResponse }.
     * 
     * @return FindUserProfileByIDResponse
     */
    public FindUserProfileByIDResponse createFindUserProfileByIDResponse() {
        return new FindUserProfileByIDResponse();
    }

    /**
     * Create an instance of {@link FindSubscriberByIdResponse }.
     * 
     * @return FindSubscriberByIdResponse
     */
    public FindSubscriberByIdResponse createFindSubscriberByIdResponse() {
        return new FindSubscriberByIdResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be",
            name = "return", scope = FindSubscriberByImsiResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindSubscriberByImsiResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME,
                SdupReaderServiceResponse.class,
                FindSubscriberByImsiResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be",
        name = "serviceId", scope = FindUserProfileByID.class)
    public JAXBElement<String> 
    createFindUserProfileByIDServiceId(String value) {
        return new JAXBElement<String>(_FindUserProfileByIDServiceId_QNAME,
                String.class, FindUserProfileByID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindUserProfileByID.class)
    public JAXBElement<String> 
    createFindUserProfileByIDClientApp(String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindUserProfileByID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "subscriberID", scope = FindUserProfileByID.class)
    public JAXBElement<String> createFindUserProfileByIDSubscriberID(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDSubscriberID_QNAME,
                String.class, FindUserProfileByID.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindSubscriberByMsisdnResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindSubscriberByMsisdnResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME,
                SdupReaderServiceResponse.class,
                FindSubscriberByMsisdnResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link MSISDN }{@code >}.
     * 
     * @param value MSISDN
     * @return JAXBElement<MSISDN>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "msisdn", scope = FindSubscriberByMsisdn.class)
    public JAXBElement<MSISDN> 
    createFindSubscriberByMsisdnMsisdn(MSISDN value) {
        return new JAXBElement<MSISDN>(_FindSubscriberByMsisdnMsisdn_QNAME,
                MSISDN.class, FindSubscriberByMsisdn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindSubscriberByMsisdn.class)
    public JAXBElement<String> createFindSubscriberByMsisdnClientApp(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindSubscriberByMsisdn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link be.belgacom.mobile.sdup.xsd.SdupException }{@code >}.
     * 
     * @param value SdupException
     * @return JAXBElement<be.belgacom.mobile.sdup.xsd.SdupException>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "SdupException", 
        scope = be.belgacom.mobile.sdup.service.reader.SdupException.class)
    public JAXBElement<be.belgacom.mobile.sdup.xsd.SdupException> 
    createSdupExceptionSdupException(
            be.belgacom.mobile.sdup.xsd.SdupException value) {
        return new JAXBElement<be.belgacom.mobile.sdup.xsd.SdupException>(
                _SdupExceptionSdupException_QNAME,
                be.belgacom.mobile.sdup.xsd.SdupException.class,
                be.belgacom.mobile.sdup.service.reader.SdupException.class,
                value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindUserProfileByMsisdnResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindUserProfileByMsisdnResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME,
                SdupReaderServiceResponse.class,
                FindUserProfileByMsisdnResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindSubscriberByImsi.class)
    public JAXBElement<String> 
    createFindSubscriberByImsiClientApp(String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindSubscriberByImsi.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "imsi", scope = FindSubscriberByImsi.class)
    public JAXBElement<String> createFindSubscriberByImsiImsi(String value) {
        return new JAXBElement<String>(_FindSubscriberByImsiImsi_QNAME,
                String.class, FindSubscriberByImsi.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link MSISDN }{@code >}.
     * 
     * @param value MSISDN
     * @return JAXBElement<MSISDN>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "msisdn", scope = FindUserProfileByMsisdn.class)
    public JAXBElement<MSISDN> 
    createFindUserProfileByMsisdnMsisdn(MSISDN value) {
        return new JAXBElement<MSISDN>(_FindSubscriberByMsisdnMsisdn_QNAME,
                MSISDN.class, FindUserProfileByMsisdn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "serviceId", scope = FindUserProfileByMsisdn.class)
    public JAXBElement<String> createFindUserProfileByMsisdnServiceId(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDServiceId_QNAME,
                String.class, FindUserProfileByMsisdn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindUserProfileByMsisdn.class)
    public JAXBElement<String> createFindUserProfileByMsisdnClientApp(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindUserProfileByMsisdn.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindSubscribersByIdListResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindSubscribersByIdListResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME,
                SdupReaderServiceResponse.class,
                FindSubscribersByIdListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link Object }{@code >}.
     * 
     * @param value Object
     * @return JAXBElement<Object>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "Exception", scope = Exception.class)
    public JAXBElement<Object> createExceptionException(Object value) {
        return new JAXBElement<Object>(_ExceptionException_QNAME, Object.class,
                Exception.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "subcriberId", scope = FindSubscriberById.class)
    public JAXBElement<String> 
    createFindSubscriberByIdSubcriberId(String value) {
        return new JAXBElement<String>(_FindSubscriberByIdSubcriberId_QNAME,
                String.class, FindSubscriberById.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindSubscriberById.class)
    public JAXBElement<String> 
    createFindSubscriberByIdClientApp(String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindSubscriberById.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindSubscriberByMsisdnList.class)
    public JAXBElement<String> 
    createFindSubscriberByMsisdnListClientApp(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindSubscriberByMsisdnList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindUserProfileByIDResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindUserProfileByIDResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME,
                SdupReaderServiceResponse.class,
                FindUserProfileByIDResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <} {@link String }{@code >}.
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "clientApp", scope = FindSubscribersByIdList.class)
    public JAXBElement<String> createFindSubscribersByIdListClientApp(
            String value) {
        return new JAXBElement<String>(_FindUserProfileByIDClientApp_QNAME,
                String.class, FindSubscribersByIdList.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindSubscriberByMsisdnListResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindSubscriberByMsisdnListResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME, 
                SdupReaderServiceResponse.class,
                FindSubscriberByMsisdnListResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}.
     * {@link SdupReaderServiceResponse }{@code >}.
     * 
     * @param value SdupReaderServiceResponse
     * @return JAXBElement<SdupReaderServiceResponse>
     */
    @XmlElementDecl(namespace = 
        "http://reader.service.sdup.mobile.belgacom.be", 
        name = "return", scope = FindSubscriberByIdResponse.class)
    public JAXBElement<SdupReaderServiceResponse> 
    createFindSubscriberByIdResponseReturn(
            SdupReaderServiceResponse value) {
        return new JAXBElement<SdupReaderServiceResponse>(
                _FindSubscriberByImsiResponseReturn_QNAME, 
                SdupReaderServiceResponse.class,
                FindSubscriberByIdResponse.class, value);
    }

}
