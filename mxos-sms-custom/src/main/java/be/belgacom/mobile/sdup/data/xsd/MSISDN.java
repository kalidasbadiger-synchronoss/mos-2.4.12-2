/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.data.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for MSISDN complex type.
 *
 * <p>
 * The following schema fragment specifies the expected content contained 
 * within this class.
 *
 * <pre>
 * &lt;complexType name="MSISDN">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cc" 
 *         type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="hlr" 
 *         type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="ndc" 
 *         type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="region" 
 *         type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="sn" 
 *         type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MSISDN", propOrder = { "cc", "hlr", "ndc", "region", "sn" })
public class MSISDN {

    protected Integer cc;
    protected Integer hlr;
    protected Integer ndc;
    protected Integer region;
    protected Integer sn;

    /**
     * Gets the value of the cc property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getCc() {
        return cc;
    }

    /**
     * Sets the value of the cc property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setCc(Integer value) {
        this.cc = value;
    }

    /**
     * Gets the value of the hlr property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getHlr() {
        return hlr;
    }

    /**
     * Sets the value of the hlr property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setHlr(Integer value) {
        this.hlr = value;
    }

    /**
     * Gets the value of the ndc property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getNdc() {
        return ndc;
    }

    /**
     * Sets the value of the ndc property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setNdc(Integer value) {
        this.ndc = value;
    }

    /**
     * Gets the value of the region property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getRegion() {
        return region;
    }

    /**
     * Sets the value of the region property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setRegion(Integer value) {
        this.region = value;
    }

    /**
     * Gets the value of the sn property.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getSn() {
        return sn;
    }

    /**
     * Sets the value of the sn property.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setSn(Integer value) {
        this.sn = value;
    }

}
