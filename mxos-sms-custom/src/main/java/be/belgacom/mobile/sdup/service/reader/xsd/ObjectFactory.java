/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.reader.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each.
 * Java content interface and Java element interface 
 * generated in the be.belgacom.mobile.sdup.service.reader.xsd package.
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SdupReaderServiceResponseStatusMessage_QNAME =
        new QName("http://reader.service.sdup.mobile.belgacom.be/xsd", 
                "statusMessage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of.
     *  schema derived classes for package:
     *   be.belgacom.mobile.sdup.service.reader.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SdupReaderServiceResponse }.
     * 
     * @return SdupReaderServiceResponse
     */
    public SdupReaderServiceResponse createSdupReaderServiceResponse() {
        return new SdupReaderServiceResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }.
     * {@code <}{@link String }{@code >}}
     * 
     * @param value String
     * @return JAXBElement<String>
     */
    @XmlElementDecl(namespace =
        "http://reader.service.sdup.mobile.belgacom.be/xsd",
        name = "statusMessage", scope = SdupReaderServiceResponse.class)
    public JAXBElement<String> 
    createSdupReaderServiceResponseStatusMessage(String value) {
        return new JAXBElement<String>(
                _SdupReaderServiceResponseStatusMessage_QNAME,
                String.class, SdupReaderServiceResponse.class, value);
    }

}
