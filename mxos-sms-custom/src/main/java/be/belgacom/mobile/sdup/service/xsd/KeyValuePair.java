/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for KeyValuePair complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KeyValuePair">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="key" type=
 *         "{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="values" type=
 *         "{http://www.w3.org/2001/XMLSchema}string" 
 *         maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyValuePair", propOrder = { "key", "values" })
public class KeyValuePair {

    @XmlElementRef(name = "key", namespace = 
        "http://service.sdup.mobile.belgacom.be/xsd", 
        type = JAXBElement.class)
    protected JAXBElement<String> key;
    @XmlElement(nillable = true)
    protected List<String> values;

    /**
     * Gets the value of the key property.
     * 
     * @return possible object is {@link JAXBElement }{@code <}{@link String }
     *         {@code >}
     * 
     */
    public JAXBElement<String> getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value allowed object is {@link JAXBElement }
     * {@code <} {@link String } {@code >}
     * 
     */
    public void setKey(JAXBElement<String> value) {
        this.key = ((JAXBElement<String>) value);
    }

    /**
     * Gets the value of the values property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the values property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getValues().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link String }
     * 
     * @return List<String>
     * 
     */
    public List<String> getValues() {
        if (values == null) {
            values = new ArrayList<String>();
        }
        return this.values;
    }

}
