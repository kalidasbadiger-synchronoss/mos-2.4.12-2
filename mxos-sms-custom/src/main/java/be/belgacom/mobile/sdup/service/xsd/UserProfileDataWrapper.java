/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.service.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for UserProfileDataWrapper complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content 
 * contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UserProfileDataWrapper">
 *   &lt;complexContent>
 *     &lt;extension base=
 *     "{http://service.sdup.mobile.belgacom.be/xsd}DataWrapper">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UserProfileDataWrapper")
public class UserProfileDataWrapper extends DataWrapper {

}
