/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package be.belgacom.mobile.sdup.xsd;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content.
 * interface and Java element interface generated in the 
 * be.belgacom.mobile.sdup.xsd package.
 * <p>
 * An ObjectFactory allows you to programatically construct 
 * new instances of the Java representation for XML content.
 * The Java representation of XML content can consist of schema 
 * derived interfaces and classes representing the binding
 * of schema type definitions, element declarations and model groups.
 * Factory methods for each of these are provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of.
     * schema derived classes for package: be.belgacom.mobile.sdup.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SdupException }
     * 
     * @return SdupException
     */
    public SdupException createSdupException() {
        return new SdupException();
    }

}
