##
## Add Net Mail attributeTypes and objectClasses to cn=schema
## Usage:
## ldapmodify -D cn=root -w <pw> -f <ldif>
## nm - netMail
##
dn: cn=schema
changetype: modify
add: attributetypes
attributetypes: ( 1.3.6.1.4.1.2415.8.1.2
    NAME 'netMailAutoCollect'
    DESC 'List of email address to collect from an outgoing email'
    EQUALITY caseIgnoreMatch
    ORDERING caseIgnoreOrderingMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.44 SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,all,to,none' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.5
    NAME 'netMailMsgsPerPage'
    DESC 'Number of messages displayed per page'
    EQUALITY integerMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE
    X-VALUE-CONSTRAINT 'RANGE,0-2147483647' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.6
    NAME 'netMailReplyFormat'
    DESC 'Default reply separator 0-line, 1-character, 2-none'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1,2' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.11
    NAME 'netMailDefaultEditor'
    DESC 'Use plain text or HTML editor by default 0-Plain text, 1-HTML'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.12
    NAME 'netMailMsgListCols'
    DESC 'Columns displayed in the message entries list'
    EQUALITY caseExactMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.44 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.13
    NAME 'netMailAutoPopEmail'
    DESC 'External mail is retrieved at login, automatically or manually 0-manual, 1-login, 2-automatic'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1,2' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.14
    NAME 'netMailPopEmailAcctSelect'
    DESC 'If account is POPed 0-individual, 1-all'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.16
    NAME 'netMailActiveAlias'
    DESC 'Active alias'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.17
    NAME 'netMailPopAcctDetails'
    DESC 'Pop account details (SMTP address, Destination Folder, Delete Message Flag, From Name, User Name, Password, Pop Server Name, Pop Server Port, Timeout, UID List, Icon Type, Password encryption type) per account'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 MULTI-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.18
    NAME 'netMailAbEntriesPerPage'
    DESC 'Number of address book entries to display on a web page'
    EQUALITY integerMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE
    X-VALUE-CONSTRAINT 'RANGE,0-2147483647' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.21
    NAME 'netMailEnableVoice'
    DESC 'If account can view voicemail 0-no, 1-yes'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.22
    NAME 'netMailEnableFax'
    DESC 'If account can view fax 0-no, 1-yes'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.1.24
    NAME 'netMailCurrentVer'
    DESC 'Indicates WebEdge Version user data is set up for'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.44 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.1
    NAME 'netMailSendReturnReceipt'
    DESC 'Indicates return-receipt procedure'
    EQUALITY caseIgnoreMatch
    ORDERING caseIgnoreOrderingMatch
    SUBSTR caseIgnoreSubstringsMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.44 SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,always,never,ask' )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.3
    NAME 'netMobileMailAbSortBy'
    DESC 'Contact field to sort the address book entries on a WAP page'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.4
    NAME 'netMobileMailAbDisplayBy'
    DESC 'Contact field to display contacts on a WAP page'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.19
    NAME 'netMailAddressBookContactListMax'
    DESC 'Maximum number of contact lists in a users address book'
    EQUALITY integerMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27 SINGLE-VALUE
    X-VALUE-CONSTRAINT 'RANGE,0-2147483647' )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.44
    NAME 'netMailMsgViewPrivacyFilter'
    DESC 'Determines whether HTML content in recieved messages is filtered when viewed in Webedge'
    EQUALITY booleanMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.45
    NAME 'netMailRequestReturnReceipt'
    DESC 'Automatically send return receipt requests with outgoing messages? 0-no, 1-yes'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.46
    NAME 'netMailAutoSpellCheck'
    DESC 'Automatically spell check when sending messages? 0-no, 1-yes'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.47
    NAME 'netMailVariant'
    DESC 'The current user selected variant used for the WebEdge UI'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.48
    NAME 'netMailUseTrash'
    DESC 'Move to Trash when deleting messages? 0-no, 1-yes'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1' )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.49
    NAME 'netMailSignatureBeforeReply'
    DESC 'Place signature before reply? 0-after, 1-before'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1')
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.59
    NAME 'netMailMta'
    DESC 'CoS base MTA switching for multi-MTA feature'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.60
    NAME 'netMailWebLineWidth'
    EQUALITY integermatch
    ORDERING integerOrderingMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.27{38}  SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.64
    NAME 'netMailPreferredMsgView'
    DESC 'Preferred message view? 0-html, 1-plain'
    EQUALITY numericStringMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.36{1} SINGLE-VALUE
    X-VALUE-CONSTRAINT 'CHOICE,0,1')
attributetypes: ( 1.3.6.1.4.1.2415.8.30.1.65
    NAME 'netMailLocale'
    DESC 'Per User Locale data example en-US, ja-JP'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.42.1.2
    NAME 'addressBookProvider'
    DESC 'Per User Address Book Connector. Values: mss or jndi'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.8.42.1.3
    NAME 'easyLoginId'
    DESC 'Username for the easy login feature. This will usually correspond to the subscriber id. This is alphanumeric.'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15 SINGLE-VALUE)
attributetypes: ( 1.3.6.1.4.1.2415.8.42.1.4
    NAME 'easyLoginPassword'
    DESC 'Password for the easy login feature. The user will set a password of length 4. This is alphanumeric.'
    EQUALITY caseExactMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.15{4} SINGLE-VALUE)
attributetypes: ( 1.3.6.1.4.1.2415.14.41.1.1
    NAME 'netmailrmcos'
    EQUALITY caseIgnoreIA5Match
    ORDERING caseIgnoreIA5OrderingMatch
    SUBSTR caseIgnoreIA5SubstringsMatch SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 )
attributetypes: ( 1.3.6.1.4.1.2415.14.41.1.2
    NAME 'netmailrmactivesyncenabled'
    EQUALITY booleanMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.7 SINGLE-VALUE )
attributetypes: ( 1.3.6.1.4.1.2415.14.20.1.6
    NAME 'netmailwebapp'
    DESC 'Prefered webmail application. Values: webedge, richmail'
    EQUALITY caseIgnoreMatch
    SYNTAX 1.3.6.1.4.1.1466.115.121.1.26 SINGLE-VALUE )
-
add: objectclasses
objectclasses: ( 1.3.6.1.4.1.2415.8.2.1
    NAME 'netMailUser'
    SUP top AUXILIARY
    MAY ( netMailAutoCollect $ netMailMsgsPerPage $ netMailReplyFormat $ netMailDefaultEditor $ netMailMsgListCols $ netMailAutoPopEmail $ netMailPopEmailAcctSelect $ netMailActiveAlias $ netMailPopAcctDetails $ netMailAbEntriesPerPage $ netMailEnableVoice $ netMailEnableFax $ netMailCurrentVer $ netMailSendReturnReceipt $ netMobileMailAbSortBy $ netMobileMailAbDisplayBy $ netMailAddressBookContactListMax $ netMailMsgViewPrivacyFilter $ netMailRequestReturnReceipt $ netMailAutoSpellCheck $ netMailVariant $ netMailUseTrash $ netMailSignatureBeforeReply $ netMailMta $ netMailLocale $ netMailWebLineWidth $ netMailPreferredMsgView $ addressBookProvider $ easyLoginId $ easyLoginPassword $ netmailrmcos $ netmailrmactivesyncenabled  $ netmailFullFeaturesEnabled $ netmailLastFullFeaturesConnectionDate $ netmailInfoStoreAccessAllowed $ netmailInfoStoreQuotaMB $ netmailSocialNetworkIntegrationAllowed $ netmailSocialNetworkSite $ netmailSocialNetworkSiteAccessEnabled $ netMailExternalAccounts $ netmailwebapp ) )

## Add Net Mail COS attributeTypes and objectClass to cn=full,cn=admin root
dn: cn=full,cn=admin root
changetype: modify
add: objectclass
objectclass: netMailUser
-
add: netMailEnableVoice
netMailEnableVoice: 0
-
add: netMailEnableFax
netMailEnableFax: 0
-
add: netMailReplyFormat
netMailReplyFormat: 1
-
add: netMailMsgListCols
netMailMsgListCols: from,subj,size,date
-
add: netMailSendReturnReceipt
netMailSendReturnReceipt: never
-
add: adminattributeconstraint
adminattributeconstraint: <1,10,netMailSendReturnReceipt,NO_MODIFY>
-
add: adminattributeconstraint
adminattributeconstraint: <1,100,netMailSendReturnReceipt,CHOICE,always,never,ask>
-
add: netMailPreferredMsgView
netMailPreferredMsgView: 0
-
add: addressBookProvider
addressBookProvider: mss


## Add Net Mail COS attributeTypes and objectClass to cn=default,cn=admin root
dn: cn=default,cn=admin root
changetype: modify
add: objectclass
objectclass: netMailUser
-
add: netMailEnableVoice
netMailEnableVoice: 0
-
add: netMailEnableFax
netMailEnableFax: 0
-
add: netMailReplyFormat
netMailReplyFormat: 1
-
add: netMailMsgListCols
netMailMsgListCols: from,subj,size,date
-
add: netMailSendReturnReceipt
netMailSendReturnReceipt: never
-
add: adminattributeconstraint
adminattributeconstraint: <1,10,netMailSendReturnReceipt,NO_MODIFY>
-
add: adminattributeconstraint
adminattributeconstraint: <1,100,netMailSendReturnReceipt,CHOICE,always,never,ask>
-
add: netMailPreferredMsgView
netMailPreferredMsgView: 0
-
add: addressBookProvider
addressBookProvider: mss

