/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.tasks;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.stub.utils.PopulateTask;
import com.opwvmsg.mxos.task.pojos.Task;

/**
 * Tasks service exposed to client which is responsible for doing basic task
 * related activities e.g create, update, read, search and delete task via stub
 * of MxOS.
 * 
 * @author mxos-dev
 */
public class StubTasksService implements ITasksService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long userId = 2;
        if (inputParams.containsKey(TasksProperty.userId.name())) {
            userId = Long.parseLong(inputParams
                    .get(TasksProperty.userId.name()).get(0));
        }
        return userId;
    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }
    @Override
    public Task read(Map<String, List<String>> inputParams)
            throws MxOSException {

        String taskId = "12";
        if (inputParams.containsKey(TasksProperty.taskId.name())) {
            taskId = inputParams.get(TasksProperty.taskId.name()).get(0);
        }
        Task task = PopulateTask.readTask();
        task.getTaskBase().setTaskId(taskId);
        return task;
    }

    @Override
    public List<Task> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return PopulateTask.listTasks();
    }

    @Override
    public void confirm(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
