/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsBaseService;

/**
 * Groups base service exposed to client which is responsible for doing basic
 * group related activities e.g list, read, update addressbook groups via Stub
 * of MxOS.
 * 
 * @author mxos-dev
 */
public class StubGroupsBaseService implements IGroupsBaseService {

    @Override
    public List<GroupBase> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<GroupBase> list = new ArrayList<GroupBase>();

        GroupBase groupBase1 = new GroupBase();
        groupBase1.setGroupId("MxOSTeam1");
        groupBase1.setCreated("2011-06-28T17:26:54Z");
        groupBase1.setUpdated("2011-06-28T17:26:54Z");
        groupBase1.setGroupName("mxos-dev1@openwave.com");
        groupBase1.setCustomFields("List of comma separated fields");
        groupBase1.setColorLabel(1);

        GroupBase groupBase2 = new GroupBase();
        groupBase2.setGroupId("MxOSTeam2");
        groupBase2.setCreated("2011-06-29T17:26:54Z");
        groupBase2.setUpdated("2011-06-29T17:26:54Z");
        groupBase2.setGroupName("mxos-dev2@openwave.com");
        groupBase2.setCustomFields("List of comma separated fields");
        groupBase2.setColorLabel(2);

        list.add(groupBase1);
        list.add(groupBase2);

        return list;
    }

    @Override
    public GroupBase read(Map<String, List<String>> inputParams)
            throws MxOSException {

        GroupBase groupBase = new GroupBase();
        groupBase.setGroupId("MxOSTeam");
        groupBase.setCreated("2011-06-29T17:26:54Z");
        groupBase.setUpdated("2011-06-29T17:26:54Z");
        groupBase.setGroupName("mxos-dev@openwave.com");
        groupBase.setCustomFields("List of comma separated fields");
        groupBase.setColorLabel(1);

        return groupBase;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
