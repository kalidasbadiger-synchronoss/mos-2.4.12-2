/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.logging;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;

/**
 * Credentials service exposed to client which is responsible for reading 
 * crendentials, authenticate etc., via stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubLoggingCredentialsService implements ICredentialService {

    /**
     * Default Constructor.
     */
    public StubLoggingCredentialsService() {
    }

    @Override
    public Credentials read(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
    }

    @Override
    public void authenticate(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
    }

    @Override
    public Mailbox authenticateAndGetMailbox(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }
}
