/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.stub.service.cos;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.cos.ICosCredentialsService;
import com.opwvmsg.mxos.stub.utils.PopulateCos;

/**
 * Stub Service to COS Credentials Object level operations like Read and Update.
 *
 * @author mxos-dev
 */
public class StubCosCredentialsService implements ICosCredentialsService {

    @Override
    public Credentials read(final Map<String, List<String>> inputParams)
            throws MxOSException {
        String cosId = "default";
        if (inputParams.containsKey(MailboxProperty.cosId.name())) {
            cosId = inputParams.get(MailboxProperty.cosId.name()).get(0);
        }
        return PopulateCos.getCos(cosId).getCredentials();
    }

    @Override
    public void update(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

}
