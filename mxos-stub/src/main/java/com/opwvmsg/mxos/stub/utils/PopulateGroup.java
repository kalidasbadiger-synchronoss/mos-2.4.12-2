/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.opwvmsg.mxos.addressbook.pojos.Group;
import com.opwvmsg.mxos.addressbook.pojos.GroupBase;
import com.opwvmsg.mxos.addressbook.pojos.Member;

/**
 * Populate the Data.
 * 
 * @author Aricent
 * 
 */
public final class PopulateGroup {
    /**
     * populate Group data.
     * 
     * @return Group Group object
     */
    public static String create(String name) {
        return name;
    }

    /**
     * populate Group data.
     * 
     * @return Group Group object
     */
    public static Group getGroup(String contactId) {
        GroupBase c = new GroupBase();
        c.setGroupId(contactId);
        c.setCreated(String.valueOf(new Date().getTime()));
        c.setUpdated(String.valueOf(new Date().getTime()));
        c.setGroupName("foo@bar.com");
        c.setCustomFields("key1:value1|key2:value2");
        
        Group g = new Group();
        g.setMembers(getMembers());
        return g;
    }

    /**
     * Get List of Groups.
     * 
     * @return
     */
    public static List<Group> getGroups() {
        List<Group> list = new ArrayList<Group>();
        list.add(getGroup("group1"));
        list.add(getGroup("group2"));
        return list;
    }

    /**
     * Get List of Groups.
     * 
     * @return
     */
    public static List<Member> getMembers() {
        List<Member> list = new ArrayList<Member>();
        list.add(getMember("bob"));
        list.add(getMember("foo"));
        return list;
    }

    /**
     * populate Group data.
     * 
     * @return Group Group object
     */
    public static Member getMember(String contactId) {
        Member m = new Member();
        m.setMemberId(contactId);
        String url = "http://localhost:8080/mxos-stub/nab/1234/contacts/"
                + contactId;
        m.setHref(url);
        return m;
    }
}
