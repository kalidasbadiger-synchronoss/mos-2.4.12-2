/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.domain;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.domain.IDomainService;
import com.opwvmsg.mxos.stub.utils.PopulateDomain;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubDomainService implements IDomainService {

    @Override
    public void create(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public Domain read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String domain = "bar.com";
        if (inputParams.containsKey(DomainProperty.domain.name())) {
            domain = inputParams.get(DomainProperty.domain.name()).get(0);
        }
        return PopulateDomain.getDomain(domain);
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public void delete(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public List<Domain> search(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return PopulateDomain.getDomains();
    }
}
