/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.AddressBookBase;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IAddressBookBaseService;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubAddressBookBaseService implements IAddressBookBaseService {

    @Override
    public List<AddressBookBase> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<AddressBookBase> list = new ArrayList<AddressBookBase>();

        AddressBookBase addressBase1 = new AddressBookBase();

        addressBase1.setUserId("ABUserName1");
        addressBase1.setCreated("2011-06-28T17:26:54Z");
        addressBase1.setUpdated("2011-06-28T17:26:54Z");

        AddressBookBase addressBase2 = new AddressBookBase();

        addressBase2.setUserId("ABUserName2");
        addressBase2.setCreated("2011-06-29T17:26:54Z");
        addressBase2.setUpdated("2011-06-29T17:26:54Z");

        list.add(addressBase1);
        list.add(addressBase2);

        return list;
    }

    @Override
    public AddressBookBase read(Map<String, List<String>> inputParams)
            throws MxOSException {
        AddressBookBase addressBookBase = new AddressBookBase();

        addressBookBase.setUserId("ABUserName1");
        addressBookBase.setCreated("2011-06-28T17:26:54Z");
        addressBookBase.setUpdated("2011-06-28T17:26:54Z");

        return addressBookBase;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }
}
