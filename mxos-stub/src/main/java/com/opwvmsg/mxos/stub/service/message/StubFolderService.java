/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.message;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.message.IFolderService;
import com.opwvmsg.mxos.stub.utils.PopulateFolder;

/**
 * Folder service exposed to client which is responsible for doing basic Folder
 * related activities e.g create, update, read, search and delete Folder via
 * Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubFolderService implements IFolderService {

    private static String folderId = "2342342342";
    
    @Override
    public String create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return folderId;
    }

    @Override
    public Folder read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String folderName = "INBOX";
        if (inputParams.containsKey(DomainProperty.domain.name())) {
            folderName = inputParams.get(DomainProperty.domain.name()).get(0);
        }
        return PopulateFolder.getFolder("1", folderName);
    }

    @Override
    public List<Folder> list(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return PopulateFolder.getFolders();
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public void delete(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
        
    }
}
