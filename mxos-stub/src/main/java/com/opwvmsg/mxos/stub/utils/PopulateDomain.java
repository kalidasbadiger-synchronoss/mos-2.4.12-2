/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.data.pojos.Domain;

/**
 * Populate the Data.
 * 
 * @author mxos-dev
 * 
 */
public final class PopulateDomain {
    public static Domain domain;
    private static void loadDomain() {
        InputStream is = null;
        try {
            // JsonFactory jfactory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-domain.json");
            if (is != null) {
                domain = mapper.readValue(is, Domain.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }
     /**
     * populate Domains data.
     * 
     * @return List of Domain objects
     */
    public static List<Domain> getDomains() {
        List<Domain> domains = new ArrayList<Domain>();
        domains.add(getDomain("foo.com"));
        domains.add(getDomain("bar.com"));
        return domains;
    }

    /**
     * populate Domain data.
     * 
     * @return Domain Domain object
     */
    public static Domain getDomain(final String domainName) {
        loadDomain();
        domain.setDomain(domainName);
        return domain;
    }
}
