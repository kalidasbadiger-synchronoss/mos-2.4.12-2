/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;

/**
 * Groups service exposed to client which is responsible for doing basic Address
 * Book Group related activities e.g create, update, read, search and delete
 * Group via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubGroupsService implements IGroupsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long groupId = 2;
        if (inputParams.containsKey(AddressBookProperty.groupId.name())) {
            groupId = Long.parseLong(inputParams.get(
                    AddressBookProperty.groupId.name()).get(0));
        }
        return groupId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        String userId = null;
        if (inputParams.containsKey(AddressBookProperty.userId.name())) {
            userId = inputParams.get(AddressBookProperty.userId.name()).get(0);
        }
        final long id = Long.parseLong(userId);
        List<Long> groupIdList = new ArrayList<Long>() {
            {
                add(id);
            }
        };
        return groupIdList;
    }
    
    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
    }
}
