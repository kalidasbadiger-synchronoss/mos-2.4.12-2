/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.Event;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsPersonalInfoEventsService;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * Mailbox related activities e.g create, update, read, search and delete
 * Mailbox via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubContactsPersonalInfoEventsService implements
        IContactsPersonalInfoEventsService {

    @Override
    public void create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public Event read(Map<String, List<String>> inputParams)
            throws MxOSException {

        Event event = new Event();
        event.setDate("1980-01-01");
        event.setType(Event.Type.BIRTHDAY);

        return event;
    }

    @Override
    public List<Event> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<Event> events = new ArrayList<Event>();

        Event event1 = new Event();
        event1.setDate("1980-01-01");
        event1.setType(Event.Type.BIRTHDAY);

        Event event2 = new Event();
        event2.setDate("2000-01-01");
        event2.setType(Event.Type.ANNIVERSARY);

        events.add(event1);
        events.add(event2);

        return events;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        return;
    }
}
