/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.notify;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.NotificationProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.ISubscriptionsService;
import com.opwvmsg.mxos.stub.utils.PopulateNotify;

/**
 * Subscriptions Notification service exposed to client which is responsible for 
 * doing basic subscriptions related activities e.g create, read and delete 
 * subscriptions via Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubSubscriptionsService implements ISubscriptionsService {

    
    
    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void create(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public List<String> read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        String topic = "123456";
        if (inputParams.containsKey(NotificationProperty.topic.name())) {
            topic = inputParams.get(NotificationProperty.topic.name()).get(0);
        }
        return PopulateNotify.getNotify(topic).getSubscriptions();
    }

    @Override
    public void delete(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }
}
