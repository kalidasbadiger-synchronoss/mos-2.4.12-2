/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.mailbox;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.stub.utils.PopulateMailbox;

/**
 * Credential service exposed to client which is responsible for doing basic
 * Credential related activities e.g read credentials, authenticate and update
 * password via Stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubCredentialService implements ICredentialService {

    @Override
    public Credentials read(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return PopulateMailbox.getCredentials();
    }

    @Override
    public void update(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }

    @Override
    public void authenticate(
            final Map<String, List<String>> inputParams) throws MxOSException {
        return;
    }
    @Override
    public Mailbox authenticateAndGetMailbox(Map<String, List<String>> inputParams)
            throws MxOSException {

        String email = "foo@bar.com";
        if (inputParams.containsKey(MailboxProperty.email.name())) {
            email = inputParams.get(MailboxProperty.email.name()).get(0);
        }
        return PopulateMailbox.getMailbox(email);
    }
}
