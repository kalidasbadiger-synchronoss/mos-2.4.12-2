/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.interfaces.service.ServiceLoader;

/**
 * @author mxos-dev
 *
 */
public class StubUtils {
    private static Logger logger = Logger.getLogger(StubUtils.class);

    public static InputStream getRource(String fileName) {
        InputStream is = null;
        final String home = System.getProperty(MxOSConstants.MXOS_HOME);
        if (home != null && !"".equals(home)) {
            String temp = home + "/config" + fileName;
            logger.info("Going to read " + temp);
            // First read sample-object.json from ${MXOS_HOME}
            try {
                is = new FileInputStream(temp);
            } catch (Exception e) {
                logger.error(new StringBuilder("Error while reading ").append(
                        fileName).append(" from MXOS_HOME path"));
                logger.error("So, reading the same from Jar Resources");
                is = null;
            }
        }
        // if not found, then read from jar resources.
        if (is == null) {
            String temp = "/META-INF/mxos" + fileName;
            is = ServiceLoader.class.getResourceAsStream(temp);
        }
        return is;
    }
}
