/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.notify.pojos.Notify;

/**
 * Populate the Data.
 * 
 * @author mxos-dev
 * 
 */
public final class PopulateNotify {
    public static Notify notify;
    private static void loadNotify() {
        InputStream is = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-notify.json");
            if (is != null) {
                notify = mapper.readValue(is, Notify.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * populate notify data.
     * 
     * @return Notify notify object
     */
    public static Notify getNotify(String topic) {
        loadNotify();
        notify.setTopic(topic);
        return notify;
    }
    
}
