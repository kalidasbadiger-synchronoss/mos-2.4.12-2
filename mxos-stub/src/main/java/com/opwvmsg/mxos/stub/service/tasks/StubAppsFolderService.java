/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.common.IAppsFolderService;
import com.opwvmsg.mxos.task.pojos.Folder;

/**
 * Domain service exposed to client which is responsible for doing basic domain
 * related activities e.g create, update, read, search and delete domain via
 * Stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubAppsFolderService implements IAppsFolderService {

    @Override
    public List<Folder> list(Map<String, List<String>> inputParams)
            throws MxOSException {
        List<Folder> list = new ArrayList<Folder>();

        Folder contactBase1 = new Folder();
        contactBase1.setDisplayName("Bob");
        
        Folder contactBase2 = new Folder();
        contactBase2.setDisplayName("John");
        
        list.add(contactBase1);
        list.add(contactBase2);

        return list;
    }

    @Override
    public Folder read(Map<String, List<String>> inputParams)
            throws MxOSException {
        Folder contactBase = new Folder();
        contactBase.setDisplayName("Bob");
        return contactBase;
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public long create(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
