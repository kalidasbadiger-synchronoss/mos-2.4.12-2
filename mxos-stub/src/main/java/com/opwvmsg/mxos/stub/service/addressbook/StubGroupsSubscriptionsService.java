/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.addressbook.pojos.SubscriptionLink;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsSubscriptionsService;

/**
 * Groups Subscriptions service exposed to client which is responsible for doing
 * basic Group related activities e.g create, update, read, search and delete
 * Subscriptions via stub of MxOS.
 * 
 * @author mxos-dev
 */
public class StubGroupsSubscriptionsService implements
        IGroupsSubscriptionsService {

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long subscriptionId = 2;
        if (inputParams.containsKey(AddressBookProperty.subscriptionId.name())) {
            subscriptionId = Long.parseLong(inputParams.get(
                    AddressBookProperty.subscriptionId.name()).get(0));
        }
        return subscriptionId;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        return;
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        return;

    }

    @Override
    public SubscriptionLink read(Map<String, List<String>> inputParams)
            throws MxOSException {

        SubscriptionLink subscriptionLink = new SubscriptionLink();
        subscriptionLink.setGroupId("MxOSTeam");
        subscriptionLink
                .setHref("http://mxos.com:8080/addressbook/v1/{Bob}/shared/{ABUserName}/groups/{MxOSTeam}");

        return subscriptionLink;
    }

    @Override
    public List<SubscriptionLink> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        List<SubscriptionLink> subscriptionLinks = new ArrayList<SubscriptionLink>();

        SubscriptionLink subscriptionLink1 = new SubscriptionLink();
        subscriptionLink1.setGroupId("MxOSTeam1");
        subscriptionLink1
                .setHref("http://mxos.com:8080/addressbook/v1/{Bob}/shared/{ABUserName}/groups/{MxOSTeam1}");

        SubscriptionLink subscriptionLink2 = new SubscriptionLink();
        subscriptionLink2.setGroupId("MxOSTeam2");
        subscriptionLink2
                .setHref("http://mxos.com:8080/addressbook/v1/{Bob}/shared/{ABUserName}/groups/{MxOSTeam2}");

        subscriptionLinks.add(subscriptionLink1);
        subscriptionLinks.add(subscriptionLink2);

        return subscriptionLinks;
    }
}
