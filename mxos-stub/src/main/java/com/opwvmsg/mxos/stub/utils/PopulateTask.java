/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;

/**
 * Populate the Sample Task Data.
 * 
 * @author Aricent
 * 
 */
public final class PopulateTask {
    public static Task task;
    private static void loadTask() {
        InputStream is = null;
        try {
            // JsonFactory jfactory = new JsonFactory();
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-task.json");
            if (is != null) {
                task = mapper.readValue(is, Task.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * Populate Task.
     * 
     * @return Task task POJO
     */
    public static Task readTask() {
        loadTask();
        return task;
    }

    /**
     * Populate List of Task POJOs.
     * 
     * @return list of Task POJOs
     */
    public static List<Task> listTasks() {
        loadTask();
        List<Task> tasks = new ArrayList<Task>();
        tasks.add(task);
        return tasks;
    }

    /**
     * Populate List of TaskBase POJOs.
     * 
     * @return list of TaskBase POJOs
     */
    public static List<TaskBase> listTaskBase() {
        loadTask();
        List<TaskBase> tbase = new ArrayList<TaskBase>();
        tbase.add(task.getTaskBase());
        return tbase;
    }

    /**
     * Populate TaskBase.
     * 
     * @return TaskBase POJO
     */
    public static TaskBase readTaskBase() {
        loadTask();
        return task.getTaskBase();
    }

}
