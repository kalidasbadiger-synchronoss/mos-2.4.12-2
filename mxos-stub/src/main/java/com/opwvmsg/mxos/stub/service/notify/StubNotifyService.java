/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.stub.service.notify;

import static com.opwvmsg.mxos.error.NotifyError.*;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.*;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.interfaces.service.notify.INotifyService;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.stub.utils.PopulateNotify;


/**
 * Notify service exposed to client which is responsible for doing basic
 * Notification related activities e.g read and delete Notify via stub of MxOS.
 *
 * @author mxos-dev
 */
public class StubNotifyService implements INotifyService {

    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        throw new UnsupportedOperationException("PUT on Notify not allowed");
    }

    @Override
    public Notify read(Map<String, List<String>> inputParams)
            throws MxOSException {
        String aTopic = "123454";
        if (inputParams.containsKey(topic.name())) {
            aTopic = inputParams.get(topic.name()).get(0);
        }
        return PopulateNotify.getNotify(aTopic);
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (!inputParams.containsKey(topic.name())) {
            throw new ApplicationException(NTF_INVALID_TOPIC.name());
        }
    }


    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        
    }

    @Override
    public void publish(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (inputParams.get(topic.name()).get(0) == null
                || inputParams.get(topic.name()).get(0).isEmpty()) {
            throw new NotFoundException(NTF_INVALID_TOPIC.name());
        }
    }

}
