/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id: $
 */
package com.opwvmsg.mxos.stub.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * Populate the Data.
 * 
 * @author Aricent
 */
public final class PopulateMessage {

    public static Message message;

    private static void loadMessage() {
        InputStream is = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            is = StubUtils.getRource("/sample-message.json");
            if (is != null) {
                message = mapper.readValue(is, Message.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    /**
     * populate Folder data.
     * 
     * @return Folder Folder object
     */
    public static Integer create(String messageId) {
        return messageId.hashCode();
    }

    /**
     * populate Message data.
     * 
     * @return Message Message object
     */
    public static Message getMessage(String messageId) {
        loadMessage();
        return message;
    }

    /**
     * populate messages data.
     * 
     * @return List of message objects
     */
    public static List<Message> getMessages() {
        loadMessage();
        List<Message> messages = new ArrayList<Message>();
        messages.add(message);
        Message msg1 = message;
        // msg1.setMessageId("13");
        messages.add(msg1);
        return messages;
    }

    /**
     * populate messages data.
     * 
     * @return List of message objects
     */
    public static Metadata getMessageMetaData(String messageId) {
        loadMessage();
        Metadata messageMetaData = new Metadata();
        messageMetaData = message.getMetadata();
        return messageMetaData;
    }
    
    /**
     * populate messages data uid.
     * 
     * @return List of message objects
     */
    public static Metadata getMessageMetaDataUid(String messageId) {
        loadMessage();
        Metadata messageMetaData = new Metadata();
        messageMetaData.setUid(message.getMetadata().getUid());
        return messageMetaData;
    }    

    /**
     * populate messages data.
     * 
     * @return List of message objects
     */
    public static Map<String, Metadata> getAllMessageMetaData() {
        loadMessage();
        Map<String, Metadata> messageMap = new HashMap<String, Metadata>();
        messageMap.put(message.toString(), message.getMetadata());
        return messageMap;
    }
    
    /**
     * populate Message Header.
     * 
     * @return String
     */
    public static Header getMessageHeader(String messageId) {
        loadMessage();
        Header header = new Header();
        header = message.getHeader();
        return header;
    }

    /**
     * populate Message Body.
     * 
     * @return String
     */
    public static Body getMessageBody(String messageId) {
        loadMessage();
        Body body = new Body();
        body = message.getBody();
        return body;
    }
}
