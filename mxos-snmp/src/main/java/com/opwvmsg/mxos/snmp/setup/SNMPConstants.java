/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.snmp.setup;

/**
 * Interface for SNMP constants.
 * 
 * @author ajeswani
 */
public interface SNMPConstants {
    String SNMP_ADAPTER_MBEAN = "Adaptors:protocol=SNMP";
    String SNMP_TRAP_LOG = "TrapLog";
    int SNMP_TRAP_INDEX = 4;
    String NOTIFICATION_BROADCASTER_MBEAN =
        "com.opwvmsg.mxos.process:type=NotificationBroadcaster";
}
