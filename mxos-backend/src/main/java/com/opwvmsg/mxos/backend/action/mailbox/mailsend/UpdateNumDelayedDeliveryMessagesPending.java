/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update delayed delivery messages pending.
 * 
 * @author mxos-dev
 */
public class UpdateNumDelayedDeliveryMessagesPending implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateNumDelayedDeliveryMessagesPending.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateNumDelayedDeliveryMessagesPending action start."));
        }

        try {

            final String oldNumDelayedDeliveryMessagesPending = requestState
                    .getInputParams()
                    .get(MailboxProperty.oldNumDelayedDeliveryMessagesPending
                            .name()).get(0);

            final String newNumDelayedDeliveryMessagesPending = requestState
                    .getInputParams()
                    .get(MailboxProperty.newNumDelayedDeliveryMessagesPending
                            .name()).get(0);

            final String[] timeMessageID = newNumDelayedDeliveryMessagesPending
                    .split(MxOSConstants.COMMA);
            if (timeMessageID.length != 2
                    || (timeMessageID[0] == null || timeMessageID[0].equals(""))
                    || (timeMessageID[1] == null || timeMessageID[1].equals(""))) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                .name());
            }
            try {
                SimpleDateFormat sdf = new SimpleDateFormat(
                        System.getProperty(SystemProperty.userDateFormat.name()));
                sdf.setLenient(false);
                Date date = sdf.parse(timeMessageID[0]);
            } catch (ParseException e) {
                logger.error("Error while converting user date "
                        + timeMessageID[0] + " to unix epoch time.", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_NEW_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                .name());
            }
            List<String> numDelayedDeliveryMessagesPendingList = new ArrayList<String>();
            final MailSend ms = (MailSend) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);

            if (ms != null) {
                if (ms.getNumDelayedDeliveryMessagesPending() != null) {
                    numDelayedDeliveryMessagesPendingList = ms
                            .getNumDelayedDeliveryMessagesPending();
                } else {
                    numDelayedDeliveryMessagesPendingList = new ArrayList<String>();
                }
            }

            if (numDelayedDeliveryMessagesPendingList == null
                    || numDelayedDeliveryMessagesPendingList.size() == 0) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                .name());
            }

            if (numDelayedDeliveryMessagesPendingList
                    .remove(oldNumDelayedDeliveryMessagesPending)) {
                numDelayedDeliveryMessagesPendingList
                        .add(newNumDelayedDeliveryMessagesPending);

                String[] delayedDeliveryMessagesPendingArray = numDelayedDeliveryMessagesPendingList
                        .toArray(new String[numDelayedDeliveryMessagesPendingList
                                .size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.numDelayedDeliveryMessagesPending,
                                delayedDeliveryMessagesPendingArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_NUM_DELAYED_DELIVERY_MESSAGES_PENDING
                                .name());
            }

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set num delayed delivery messages pending.",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_DELAYED_DELIVERY_MESSAGES_PENDING_UNABLE_TO_POST
                            .name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateNumDelayedDeliveryMessagesPending action end."));
        }
    }
}
