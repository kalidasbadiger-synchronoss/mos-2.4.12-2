/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/BooleanEnum.java#1 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * @author mxos-dev
 * 
 */
public enum MailboxType implements DataMap.Property {
    NORMAL("0", "normal"),
    GROUPADMIN("1", "groupAdmin"),
    GROUPMAILBOX("2", "groupMailbox"),
    MAILINGLISTSUBSCRIBER("S", "mailingListSubscriber"),
    MAILINGLIST("E", "mailingList");

    private String value;
    private String type;

    private MailboxType(String value, String type) {
        this.value = value;
        this.type = type;
    }

    public static MailboxType getByOrdinal(String value) throws InvalidRequestException {
        int ordinal = -1;
        try {
            ordinal = Integer.parseInt(value);
        } catch (NumberFormatException e) {}
        if (ordinal < 0 || ordinal >= values().length) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return values()[ordinal];
    }

    public static MailboxType getByValue(String value) throws InvalidRequestException {
        MailboxType returnVal = null;
        for (MailboxType type : values()) {
            if (type.name().equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public static MailboxType getByType(String value) throws InvalidRequestException {
        MailboxType returnVal = null;
        for (MailboxType type : values()) {
            if (type.type.equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
