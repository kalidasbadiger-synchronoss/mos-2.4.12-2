/*
 /*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.action.mailbox.auth;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Action class to load Mailbox of authenticate the user.
 * 
 * @author mxos-dev
 */
public class LoadMailboxForAuthenticate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(LoadMailboxForAuthenticate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        final long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("action start.");
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            final long t1 = Stats.startTimer();
            try {
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                Stats.stopTimer(ServiceEnum.LdapSearchObjAcquireTime, Operation.GET, t1,
                        StatStatus.pass);
            } catch (final MxOSException mosEx) {
                Stats.stopTimer(ServiceEnum.LdapSearchObjAcquireTime, Operation.GET, t1,
                        StatStatus.fail);
                throw mosEx;
            }
            final Mailbox mailbox;
            if (requestState.getInputParams()
                    .containsKey(MailboxProperty.email.name())) {
                final String email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                mailbox = mailboxCRUD.readMailbox(requestState, email);
            } else if (requestState.getInputParams()
                    .containsKey(MailboxProperty.userName.name())) {
                final String userName = requestState.getInputParams()
                        .get(MailboxProperty.userName.name()).get(0);
                mailbox = mailboxCRUD.readMailbox(requestState, userName);
            } else if (requestState.getInputParams()
                    .containsKey(MailboxProperty.userId.name())) {
                final String userName = requestState.getInputParams()
                        .get(MailboxProperty.userId.name()).get(0);
                mailbox = mailboxCRUD.readMailbox(requestState, userName);
            } else {
                final String errMsg = "email or userName not found";
                logger.warn(errMsg);
                Stats.stopTimer(ServiceEnum.ActionAuthenticateReadMailboxTime, Operation.POST, t0,
                        StatStatus.fail);
                throw new AuthorizationException(
                        MailboxError.MBX_AUTHENTICATION_FAILED.name(), errMsg);
            }
            
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailbox, mailbox);
        } catch (final MxOSException e) {
            logger.warn("Mailbox not found");
            Stats.stopTimer(ServiceEnum.ActionAuthenticateReadMailboxTime, Operation.POST, t0,
                    StatStatus.fail);
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());
        } catch (final Exception e) {
            logger.error("Error while getting Mailbox", e);
            Stats.stopTimer(ServiceEnum.ActionAuthenticateReadMailboxTime, Operation.POST, t0,
                    StatStatus.fail);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    Stats.stopTimer(ServiceEnum.ActionAuthenticateReadMailboxTime, Operation.POST, t0,
                            StatStatus.fail);
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("action end.");
        }
        Stats.stopTimer(ServiceEnum.ActionAuthenticateReadMailboxTime, Operation.POST, t0,
                StatStatus.pass);
    }
}