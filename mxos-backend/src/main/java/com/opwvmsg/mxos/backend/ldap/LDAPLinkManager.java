/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class to maintain LDAP link information
 * 
 * @author mxos-dev
 * 
 */
public class LDAPLinkManager {

    private List<LDAPLink> cacheLinks = new ArrayList<LDAPLink>();
    private List<LDAPLink> masterLinks = new ArrayList<LDAPLink>();

    private Random random = new Random();

    private static LDAPLinkManager linkManager = new LDAPLinkManager();

    private LDAPLinkManager() {

    }

    public static LDAPLinkManager instance() {
        return linkManager;
    }
    
    public void addLDAPLink(boolean cacheServer, LDAPLink link) {
        if (cacheServer) {
            cacheLinks.add(link);
        } else {
            masterLinks.add(link);
        }
    }

    public int select(boolean cacheServer) {
        return cacheServer ? random.nextInt(cacheLinks.size()) : random
                .nextInt(masterLinks.size());
    }

    public LDAPLink getLDAPLink(boolean cacheServer, int index) {
        return cacheServer ? cacheLinks.get(index) : masterLinks.get(index);
    }

    public int getSize(boolean cacheServer) {
        return cacheServer ? cacheLinks.size() : masterLinks.size();
    }

}
