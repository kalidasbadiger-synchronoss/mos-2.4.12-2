package com.opwvmsg.mxos.backend.crud;

import java.util.List;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.task.pojos.Details;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;

/**
 * Address Book APIs for interfacing with Open-Xchange (OX) server.
 * 
 * @author ajeswani
 * 
 */
public interface ITasksCRUD extends ITransaction {

    /**
     * API to add or confirm a Task.
     * 
     * @param username
     * @param task
     * @param session
     * @return
     * @throws TasksException
     */
    public String confirmTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to add multiple tasks.
     * 
     * @param username
     * @param tasks
     * @param session
     * @return
     * @throws TasksException
     */
    public List<String> createMultipleTasks(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to add or update a Task.
     * 
     * @param username
     * @param task
     * @param session
     * @return
     * @throws TasksException
     */
    public String createTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to delete task.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to delete all tasks.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTasks(MxOSRequestState mxosRequestState,
            final List<String> taskIds) throws TasksException;

    /**
     * API to get entire Task object.
     * 
     * @param mxosRequestState mxosRequestState
     * @return Task POJO
     * @throws TasksException
     */
    public Task readTask(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get list of Task details.
     * 
     * @param mxosRequestState mxosRequestState
     * @return List of Task objects
     * @throws TasksException
     */
    public List<Task> readAllTasks(MxOSRequestState mxosRequestState)
            throws TasksException;
    /**
     * API to get list of TaskBase objects.
     * 
     * @param mxosRequestState mxosRequestState
     * @return List of TaskBase objects
     * @throws TasksException
     */
    public List<TaskBase> listTaskBase(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get list of Task IDs.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public List<String> readAllTaskIds(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get Task base.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public TaskBase readTasksBase(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Participant readTasksParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task Details.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Details readTasksDetails(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task recurrence.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public Recurrence readTasksRecurrence(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task base.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksBase(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task external participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksExternalParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksParticipant(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task progress.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksProgress(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task recurrence.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksRecurrence(MxOSRequestState requestState)
            throws TasksException;

    void logout(MxOSRequestState mxosRequestState) throws TasksException;

    ExternalSession login(MxOSRequestState mxosRequestState)
            throws TasksException;

    void validateUser(MxOSRequestState mxosRequestState) throws TasksException;

    /**
     * API to add a Task Folder.
     * 
     * @param username
     * @param folderName
     * @param session
     * @return folder Id
     * @throws TasksException
     */

    public String createTaskFolder(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get Task folder.
     * 
     * @param params
     * @param folderId
     * @param url
     * @return Folder pojo
     * @throws TasksException
     */

    public Folder readTasksFolder(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to delete task folder.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTaskFolder(MxOSRequestState requestState)
            throws TasksException;

    void deleteAllTaskFolder(MxOSRequestState requestState,
            final List<String> taskIds) throws TasksException;

    List<String> readAllFolderIds(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to add or create a Task participannt.
     * 
     * @param username
     * @param taskId
     * @param email
     * @param session
     * @return
     * @throws TasksException
     */
    public void createTaskParticipant(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to delete task.
     * 
     * @param username
     * @param session
     * @return
     * @throws TasksException
     */
    public void deleteTaskParticipant(MxOSRequestState mxosRequestState)
            throws TasksException;

    /**
     * API to get list of Task participant.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public List<Participant> readAllTasksParticipants(
            MxOSRequestState mxosRequestState) throws TasksException;

    /**
     * API to update Task recurrence.
     * 
     * @param params
     * @param headers
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksDetails(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to get all Task folders.
     * 
     * @param params
     * @param url
     * @return List of Folder pojo
     * @throws TasksException
     */

    public List<Folder> listTasksFolder(MxOSRequestState requestState)
            throws TasksException;

    /**
     * API to update Task folders.
     * 
     * @param params
     * @param folderId
     * @param url
     * @return
     * @throws TasksException
     */
    public void updateTasksFolder(MxOSRequestState requestState)
            throws TasksException;
}
