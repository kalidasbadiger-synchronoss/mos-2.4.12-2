/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSRollbackAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.Status;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to create mailbox in MSS
 * 
 * @author mxos-dev
 * 
 */
public class CreateMailboxInMSS extends AbstractMailbox implements
        MxOSRollbackAction {
    private static Logger logger = Logger.getLogger(CreateMailboxInMSS.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInMSS action start.");
        }

        String email;
        if (requestState.getInputParams().containsKey(MailboxProperty.email.name())) {
            email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
        } else if (requestState.getInputParams().containsKey(MailboxProperty.userName.name())) {
            email = requestState.getInputParams()
                    .get(MailboxProperty.userName.name()).get(0);
        } else {
            email = "unknown";
        }
        boolean create = true;

        String status = requestState.getDbPojoMap().getProperty(
                MxOSPOJOs.status);
        // if status is proxy,
        // check for the config param createMssMailboxOnProxyStatus
        if (status != null && status.equalsIgnoreCase(Status.PROXY.name())) {
            create = Boolean.valueOf(MxOSConfig
                    .getCreateMssMailboxOnProxyStatus());
            if (!create) {
                logger.info("Create mailbox in MSS disabled on proxy status for email: "
                        + email);
            }
        }

        // check whether skip mailbox creation in MSS
        if (requestState.getInputParams().containsKey(SKIP_MSS)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_MSS).get(0));
            if (skip) {
                create = false;
                logger.info("Skip mailbox [" + email + "] creation in MSS.");
            }
        }

        if (create) {
            // check mailbox creation precondition
            try {
                checkMailboxCreation(requestState);
            } catch (MxOSException e) {
                logger.error("Mailbox must be created in LDAP first.", e);
                throw e;
            }

            ICRUDPool<IMetaCRUD> metaCRUDPool = null;
            IMetaCRUD metaCRUD = null;
            try {
                // Create in Metadata
                requestState.getAdditionalParams().setProperty(
                        MailboxProperty.systemfolders,
                        MxOSConfig.getSystemFolders());

                // if user does not specified the messageStoreHost,
                // choose it from config.db randomly
                readMSSLinkInfo(requestState);

                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                MailboxMetaServiceHelper.create(metaCRUD, requestState);
                logger.info("Create mailbox [" + email
                        + "] in MSS successfully.");
            } catch (MxOSException e) {
                logger.error("Error while creating mailbox [" + email
                        + "] in MSS.", e);
                throw e;
            } catch (Exception e) {
                logger.error("Error while creating mailbox [" + email
                        + "] in MSS.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            } finally {
                CRUDUtils.releaseConnection(metaCRUDPool, metaCRUD);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInMSS action end.");
        }
    }

    private void readMSSLinkInfo(MxOSRequestState requestState)
            throws MxOSException {
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            ActionUtils.readMSSLinkInfo(requestState, mailboxCRUD);
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }
    }

    public void rollback(MxOSRequestState requestState) throws MxOSException {
        ActionFactory.getInstance().exec(DeleteMailboxFromMSS.class.getName(),
                requestState);
    }

}
