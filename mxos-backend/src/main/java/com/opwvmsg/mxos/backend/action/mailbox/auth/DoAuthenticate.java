/*
 * /* Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 * The copyright to the computer software herein is the property of Openwave
 * Systems Inc. The software may be used and/or copied only with the written
 * permission of Openwave Systems Inc. or in accordance with the terms and
 * conditions stipulated in the agreement/contract under which the software has
 * been supplied. $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.auth;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordType;
import com.opwvmsg.mxos.backend.crud.ldap.PasswordUtil;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;
import com.opwvmsg.utils.paf.util.password.Password;

/**
 * Action class to authenticate the user.
 * 
 * @author mxos-dev
 */
public class DoAuthenticate implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DoAuthenticate.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        final long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug("action start.");
        }
        Credentials cred = null;
        if (requestState.getDbPojoMap().contains(MxOSPOJOs.mailbox)
                && requestState.getDbPojoMap().getPropertyAsObject(
                        MxOSPOJOs.mailbox) != null) {
            cred = ((Mailbox) requestState.getDbPojoMap().getPropertyAsObject(
                    MxOSPOJOs.mailbox)).getCredentials();
        } else if (requestState.getDbPojoMap().contains(MxOSPOJOs.credentials)
                && requestState.getDbPojoMap().getPropertyAsObject(
                        MxOSPOJOs.credentials) != null) {
            cred = (Credentials) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.credentials);
        } else {
            logger.warn("Mailbox or Credentials not found");
            Stats.stopTimer(ServiceEnum.ActionAuthenticateTime, Operation.POST, t0,
                    StatStatus.fail);
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());
        }
        if (cred == null) {
            logger.warn("Null Credentials found");
            Stats.stopTimer(ServiceEnum.ActionAuthenticateTime, Operation.POST, t0,
                    StatStatus.fail);
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());
        }
        final Password passwordProvider = MxOSApp.getInstance()
                .getPasswordProvider();
        final String givenPassword = requestState.getInputParams()
                .get(MailboxProperty.password.name()).get(0);
        PasswordType pType = null;
        if (null != cred.getPasswordStoreType()) {
            pType = PasswordType.getTypeWithValue(cred.getPasswordStoreType()
                    .toString());
        }
        String dbPassword = null;
        if (null != cred.getPassword()) {
            dbPassword = cred.getPassword();
        }
        boolean isAuthorized = false;
        if (null != pType && null != dbPassword) {
            try {
                isAuthorized = PasswordUtil.isAuthorised(passwordProvider,
                        pType, dbPassword, givenPassword);
            } catch (Exception e) {
                logger.warn("Error while authentication", e);
                Stats.stopTimer(ServiceEnum.ActionAuthenticateTime, Operation.POST, t0,
                        StatStatus.fail);
                throw new AuthorizationException(
                        MailboxError.MBX_AUTHENTICATION_FAILED.name());
            }
        }
        if (!isAuthorized) {
            Stats.stopTimer(ServiceEnum.ActionAuthenticateTime, Operation.POST, t0,
                    StatStatus.fail);
            logger.warn("Authentication failed");
            throw new AuthorizationException(
                    MailboxError.MBX_AUTHENTICATION_FAILED.name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Authentication was successful.");
            logger.debug("action end.");
        }
        Stats.stopTimer(ServiceEnum.ActionAuthenticateTime, Operation.POST, t0,
                StatStatus.pass);
    }
}
