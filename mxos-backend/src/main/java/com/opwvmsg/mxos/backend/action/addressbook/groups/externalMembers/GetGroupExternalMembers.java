/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.externalMembers;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get group external members.
 * 
 * @author mxos-dev
 * 
 */
public class GetGroupExternalMembers implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(GetGroupExternalMembers.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "GetGroupExternalMembers action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();

            List<ExternalMember> memberList = addressBookCRUD
                    .readGroupsExternalMember(requestState);

            String memberName = requestState.getInputParams()
                    .get(AddressBookProperty.memberName.name()).get(0);
            boolean memberExist = false;
            for (ExternalMember memberItr : memberList) {
                if (memberItr.getMemberName().equalsIgnoreCase(memberName)) {
                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.externalMembers, memberItr);
                    memberExist = true;
                }
            }

            if (!memberExist) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }
        } catch (AddressBookException e) {
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_GROUPS_EXTERNAL_MEMBERS_UNABLE_TO_GET,
                    e);
        } catch (final Exception e) {
            logger.error("Error while get group external members.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetGroupExternalMembers action end."));
        }
    }
}
