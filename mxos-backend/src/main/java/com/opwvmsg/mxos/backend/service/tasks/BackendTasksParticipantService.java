/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.tasks;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksParticipantService;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Base service exposed to client which is responsible for doing basic
 * base related activities e.g read base, update base, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendTasksParticipantService implements ITasksParticipantService {

    private static Logger logger = Logger
    .getLogger(BackendTasksParticipantService.class);

    /**
     * Default Constructor.
     */
    public BackendTasksParticipantService() {
        logger.info("BackendTasksParticipantService Service created...");
    }


    @Override
    public void update(Map<String, List<String>> inputParams)
    throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksParticipantService,
                    Operation.POST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.TasksParticipantService,
                    Operation.POST, t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksParticipantService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void create(Map<String, List<String>> inputParams)
    throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksParticipantService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.PUT, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }

    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
    throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksParticipantService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Participant> list(Map<String, List<String>> inputParams)
    throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksParticipantService, Operation.LIST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.LIST, t0,
                    StatStatus.pass);

            List<Participant> participantList = ((List<Participant>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allTaskparticipants));
            return participantList;
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }
    }


    @Override
    public Participant read(Map<String, List<String>> inputParams)
    throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksParticipantService,
                    Operation.GET);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.GET,
                    t0, StatStatus.pass);
            return (Participant) mxosRequestState.getDbPojoMap()
            .getPropertyAsObject(MxOSPOJOs.participant);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksParticipantService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
