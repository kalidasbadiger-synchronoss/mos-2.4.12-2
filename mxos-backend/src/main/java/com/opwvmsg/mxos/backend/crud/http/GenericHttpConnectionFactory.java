/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.http;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Notify connection pool factory to create connection to IMAP servers for
 * handling publish requests received from MSS. 
 * This is required by Apache object pool.
 * 
 * @author mxos-dev
 */
class GenericHttpConnectionFactory extends BasePoolableObjectFactory<GenericHttpClient> {
    
    @Override
    public GenericHttpClient makeObject() throws Exception {
        
        final GenericHttpClient httpClient = new GenericHttpClient();
        /* set the timeouts to the client object*/
        httpClient.getParams().setIntParameter(
                CoreConnectionPNames.CONNECTION_TIMEOUT,
                MxOSConfig.getNotifyHttpConTimeout());
        httpClient.getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT,
                MxOSConfig.getNotifyHttpReadTimeout());

        return httpClient;
    }

    @Override
    public void destroyObject(final GenericHttpClient client) throws Exception {
        Logger.getLogger(GenericHttpConnectionFactory.class).info(
                "ConMan shutdown for NotifyHttpClient: " + client);
        client.getConnectionManager().shutdown();
    }
}
