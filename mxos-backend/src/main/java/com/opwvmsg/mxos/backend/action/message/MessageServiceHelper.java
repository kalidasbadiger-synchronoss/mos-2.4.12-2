/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved. The
 * copyright to the computer software herein is the property of Openwave Systems
 * Inc. The software may be used and/or copied only with the written permission
 * of Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * Message service helper APIs which has business logic and does actual
 * operation using CRUD APIs.
 * 
 * @author mxos-dev
 */
public final class MessageServiceHelper {

    /**
     * Message reference create helper which has business logic about how to
     * create message reference. This is required for dedup and orphan tracking
     * purpose because there is not transaction between metadata and blobdata.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message reference create
     *            operation. e.g. messageId, etc.
     * @throws MxOSException Exception.
     */

    public static void createRef(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        metaCRUD.createMessageRef(mxosRequestState, 0);
    }

    /**
     * Message create helper which has business logic about how to create
     * message. It uses metadata CRUD APIs to create message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobdata CRUD connection object.
     * @param mxosRequestState - parameters for message create operation. e.g.
     *            mailboxId, folderId, messageId etc.
     * @return MessageId String
     * @throws MxOSException
     */
    public static String create(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        createRef(metaCRUD, mxosRequestState);
        metaCRUD.commit();

        createHeader(metaCRUD, mxosRequestState);
        String blob = mxosRequestState.getInputParams()
                .get(MxOSPOJOs.message.name()).get(0);

        mxosRequestState.getAdditionalParams().setProperty(
                MessageProperty.arrivalTime, System.currentTimeMillis());
        mxosRequestState.getAdditionalParams().setProperty(
                MessageProperty.lastAccessedTime, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                MessageProperty.status, MxOSConstants.NOT_SOFT_DELETE);
        mxosRequestState.getAdditionalParams().setProperty(
                MessageProperty.size, (long) blob.length());
        if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
            blobCRUD.createBlob(mxosRequestState);
        }
        return metaCRUD.createMessage(mxosRequestState, true);
    }

    /*    *//**
     * Message copy helper which has business logic about how to copy
     * message. It uses metadata CRUD APIs to copy message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message copy operation. e.g.
     *            mailboxId, folderId, messageId etc.
     * @throws MxOSException Exception.
     */
    /*
     * public static long copy(final IMetaCRUD metaCRUD, final MxOSRequestState
     * mxosRequestState) throws MxOSException { copyRef(metaCRUD,
     * mxosRequestState); metaCRUD.commit();
     * mxosRequestState.getInputParams().get(MessageProperty.messageId.name())
     * .get(0); final Message srcMessage = new Message();
     * metaCRUD.readMessage(mxosRequestState, srcMessage); // Metadata
     * MessageMetaData metaData = srcMessage.getMessageSummary()
     * .getMessageMetaData();
     * mxosRequestState.getAdditionalParams().setProperty( MessageProperty.type,
     * Integer.parseInt(metaData.getType()));
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.priority, metaData.getPriority());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.deliverndr, metaData.getDeliverNDR());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.compressiontype, metaData.getCompressionType());
     * mxosRequestState.getAdditionalParams().setProperty( MessageProperty.size,
     * metaData.getSize()); mxosRequestState.getAdditionalParams().setProperty(
     * HeaderProperty.keywords, metaData.getKeywords()); MessageHeader header =
     * srcMessage.getMessageSummary() .getMessageHeader();
     * mxosRequestState.getAdditionalParams().setProperty(HeaderProperty.date,
     * header.getDate()); mxosRequestState.getAdditionalParams().setProperty(
     * HeaderProperty.receivedfrom, header.getFrom());
     * mxosRequestState.getAdditionalParams().setProperty(
     * HeaderProperty.subject, header.getSubject());
     * mxosRequestState.getAdditionalParams().setProperty(
     * HeaderProperty.headersummary, srcMessage.getHeaderSummary());
     * mxosRequestState.getAdditionalParams().setProperty(
     * HeaderProperty.sentto, header.getTo());
     * mxosRequestState.getAdditionalParams().setProperty(HeaderProperty.cc,
     * header.getCc()); createHeader(metaCRUD, mxosRequestState);
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.arrtime, System.currentTimeMillis());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.lastaccessedtime, 0L);
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.status, MxOSConstants.NOT_SOFT_DELETE);
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagRecent, true);
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagSeen, metaData.getFlags().getFlagSeen());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagAns, metaData.getFlags().getFlagAns());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagDel, metaData.getFlags().getFlagDel());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagDraft, metaData.getFlags().getFlagDraft());
     * mxosRequestState.getAdditionalParams().setProperty(
     * MessageProperty.flagFlagged, metaData.getFlags().getFlagFlagged());
     * return metaCRUD.createMessage(mxosRequestState, true); }
     */

    /**
     * Copy Message helper which has business logic about copying message. It
     * uses metadata CRUD APIs to copy message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for message copy operation. e.g.
     *            mailboxId, folderName, messageId, etc.
     * @throws MxOSException Exception.
     */
    public static void copy(final IMetaCRUD metaCRUD, final IBlobCRUD blobCRUD,
            final MxOSRequestState mxosRequestState, final String[] messageIds)
            throws MxOSException {
        metaCRUD.copyMessages(mxosRequestState, messageIds);
    }

    /**
     * Copying all messages helper which has business logic about copying all
     * messages from source folder to destination folder. It uses metadata CRUD
     * APIs to copy messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for messages copy operation. e.g.
     *            mailboxId, folderName etc.,
     * @throws MxOSException Exception.
     */
    public static void copyAll(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        metaCRUD.copyAllMessages(mxosRequestState);
    }

    /**
     * Move Message helper which has business logic about moving message. It
     * uses metadata CRUD APIs to move messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for message move operation. e.g.
     *            mailboxId, folderName, messageId, etc.
     * @throws MxOSException Exception.
     */
    public static void move(final IMetaCRUD metaCRUD, final IBlobCRUD blobCRUD,
            final MxOSRequestState mxosRequestState, final String[] messageIds)
            throws MxOSException {
        metaCRUD.moveMessages(mxosRequestState, messageIds);
    }

    /**
     * Moving all messages helper which has business logic about moving all
     * messages from source folder to destination folder. It uses metadata CRUD
     * APIs to copy messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for messages copy operation. e.g.
     *            mailboxId, folderName etc.,
     * @throws MxOSException Exception.
     */
    public static void moveAll(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        metaCRUD.moveAllMessages(mxosRequestState);
    }

    /**
     * Message create helper which has business logic about how to create header
     * summary. It uses metadata CRUD APIs to create message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header operation.
     *            e.g. mailboxId, folderId, messageId etc.
     * @throws MxOSException Exception.
     */
    public static void createHeader(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        metaCRUD.createMessageHeader(mxosRequestState);
    }

    /**
     * Message read helper which has business logic about how to read message
     * header. It uses metadata CRUD APIs to read message header.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header operation.
     *            e.g. mailboxId, folderId, messageId etc.
     * @param messages Message POJOs which will be populated only with message
     *            header.
     * @throws MxOSException Exception.
     */
    public static void readAllMessageMetadatasInFolder(
            final IMetaCRUD metaCRUD, final MxOSRequestState mxosRequestState,
            final Map<String, Metadata> messages) throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        metaCRUD.listMetadatasInFolder(mxosRequestState, folderName, messages);
    }

    /**
     * Message read helper which has business logic about how to read message
     * header. It uses metadata CRUD APIs to read message header.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header operation.
     *            e.g. mailboxId, folderId, messageId etc.
     * @param messages Message POJOs which will be populated only with message
     *            header.
     * @throws MxOSException Exception.
     */
    public static Metadata readMessageMetadata(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        final Metadata metadata = new Metadata();
        metaCRUD.readMessageMetaData(mxosRequestState, folderName, metadata);
        return metadata;
    }

    /**
     * Multiple Message read helper which has business logic about how to read
     * multiple message header. It uses metadata CRUD APIs to read message header.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header operation.
     *            e.g. mailboxId, folderId, messageId etc.
     * @param messages Message POJOs which will be populated only with message
     *            header.
     * @throws MxOSException Exception.
     */
    public static Map<String, Metadata> readMultiMessageMetadata(
            final IMetaCRUD metaCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        final Map<String, Metadata> metaMap = new LinkedHashMap<String, Metadata>();
        metaCRUD.readMultiMessageMetaData(mxosRequestState, folderName, metaMap);
        return metaMap;
    }

    /**
     * Message read helper which has business logic about how to read message
     * header. It uses metadata CRUD APIs to read message header.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header operation.
     *            e.g. mailboxId, folderId, messageId etc.
     * @param messages Message POJOs which will be populated only with message
     *            header.
     * @throws MxOSException Exception.
     */
    public static void readMessage(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState, final Message message)
            throws MxOSException {
        metaCRUD.readMessage(mxosRequestState, message);
    }

    /**
     * Message search helper which has business logic about how to list
     * messages' inside given folder. It uses metadata CRUD APIs to list
     * messages using ListIMAPUUIDs.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message searching in folder e.g.
     *            mailboxId, folderId etc.
     * @param messages List will be populated containing Message POJO for
     *            matching messages.
     * @throws MxOSException Exception.
     */
    public static void listMessageMetaDatasInFolderUsingIMAPUUIDs(
            final IMetaCRUD metaCRUD, final MxOSRequestState mxosRequestState,
            final Map<String, Metadata> metadatas) throws MxOSException {
        // TODO: In case ListIMAPUUIDs needs to be used, then expose this
        // operation at IMetaCRUD level and use the method
        // listMetadatasInFolderUsingIMAPUUIDs in MssSlMetaCRUD
        throw new MxOSException("Operation not supported");
    }

    /**
     * Message search helper which has business logic about how to search
     * messages' inside given folder. It uses metadata CRUD APIs to search
     * messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message searching in folder e.g.
     *            mailboxId, folderId etc.
     * @param messages List will be populated containing Message POJO for
     *            matching messages.
     * @throws MxOSException Exception.
     */
    public static void listMessageMetaDatasInFolder(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState,
            final Map<String, Metadata> metadatas) throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        metaCRUD.listMetadatasInFolder(mxosRequestState, folderName, metadatas);
    }

    /**
     * Message Metadata get helper which has business logic about how to list
     * all MessageUUIDs and UIDs for messages' inside given folder. It uses
     * metadata CRUD APIs to search messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message searching in folder e.g.
     *            mailboxId, folderId etc.
     * @param metadatas Map will be populated with MessageUUIDs and UIDs.
     * @throws MxOSException Exception.
     */
    public static void listMessageMetaDatasUidsInFolder(
            final IMetaCRUD metaCRUD, final MxOSRequestState mxosRequestState,
            final Map<String, Metadata> metadatas) throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        metaCRUD.listMetadatasUidsInFolder(mxosRequestState, folderName,
                metadatas);
    }

    /**
     * Message Metadata get helper which has business logic about how to list
     * all MessageUUIDs of given folder. It uses
     * metadata CRUD APIs to search messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message searching in folder e.g.
     *            mailboxId, folderId etc.
     * @param metadatas Map will be populated with MessageUUIDs and UIDs.
     * @throws MxOSException Exception.
     */
    public static void listMessageMetaDatasUUIDsInFolder(
            final IMetaCRUD metaCRUD, final MxOSRequestState mxosRequestState,
            final List<String> uuids) throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        metaCRUD.listMetadatasUUIDsInFolder(mxosRequestState, folderName,
                uuids);
    }

    /**
     * Message search helper which has business logic about how to search
     * sorted messages inside given folder. It uses metadata CRUD APIs to search
     * messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message searching in folder e.g.
     *            mailboxId, folderId etc.
     * @param messages List will be populated containing Message POJO for
     *            matching messages.
     * @throws MxOSException Exception.
     */
    public static void searchSortedMessageMetaDatasInFolder(
            final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState,
            final Map<String, Map<String, Metadata>> messagesMetaData)
            throws MxOSException {
        final String folderName = retrieveFolderName(mxosRequestState);
        metaCRUD.searchSortedMetadatasInFolder(mxosRequestState, folderName,
                messagesMetaData);
    }

    /**
     * Message update helper which has business logic about how to update
     * message. It uses metadata CRUD APIs to update message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message update operation. e.g.
     *            mailboxId, messageId, etc.
     * @throws MxOSException Exception.
     */
    public static void update(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState, final String messageId)
            throws MxOSException {
        metaCRUD.updateMessage(mxosRequestState, messageId);
    }

    /**
     * Message delete helper which has business logic about how to hard delete
     * message. It uses metadata CRUD APIs to delete message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for message hard delete operation.
     *            e.g. mailboxId, folderId, messageId, etc.
     * @throws MxOSException Exception.
     */
    public static void delete(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException {
        metaCRUD.deleteMessages(mxosRequestState, messageIds);
    }

    /**
     * Delete all messages from folder helper which has business logic about how
     * to hard delete all the message in given folder. It uses metadata CRUD
     * APIs to delete message.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for message hard delete operation.
     *            e.g. mailboxId, folderId, messageId, etc.
     * @throws MxOSException Exception.
     */
    public static void deleteAll(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        metaCRUD.deleteAllMessages(mxosRequestState);
    }

    /**
     * Populate Message Flags: helper which reads message flags. It uses
     * metadata CRUD APIs to move messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for read operation. e.g. mailboxId,
     *            folderName etc.
     * @param messageId - messageId given in user input.
     * @throws MxOSException Exception.
     */
    public static Flags populateMessageFlags(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState,
            final String messageId) throws MxOSException {
        return metaCRUD.populateMessageFlags(mxosRequestState, messageId);
    }

    /**
     * Update Message Flags: helper which updates message flags. It uses
     * metadata CRUD APIs to move messages.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for read operation. e.g. mailboxId,
     *            folderName etc.
     * @param messageId - messageId given in user input.
     * @throws MxOSException Exception.
     */
    public static void updateMessageFlags(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState,
            final String[] messageIds) throws MxOSException {
        metaCRUD.updateMessageFlags(mxosRequestState, messageIds);
    }

    /**
     * Message blob read helper which has business logic about how to read
     * message blob. It uses blobdata CRUD APIs to read message blob.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobdata CRUD connection object.
     * @param mxosRequestState - parameters for message blob retrieval. e.g.
     *            messageId, etc.
     * @param message Message POJO filled in with blob.
     * @throws Exception Exception.
     */
    public static void readBody(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState,
            final Body body) throws MxOSException {
        blobCRUD.readBlob(mxosRequestState, body);
        updateLastAccessTime(metaCRUD, mxosRequestState);
    }

    /**
     * Mailbox helper which has business logic about how to update last access
     * time of message.It uses metadata CRUD APIs to do so.
     * 
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message access time update
     *            operation. e.g. mailboxId, folderId, messageId, etc.
     * @throws Exception Exception.
     */
    public static void updateLastAccessTime(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        metaCRUD.updateMessageLastAccessTime(mxosRequestState,
                System.currentTimeMillis());
    }

    /**
     * Private constructor because utility class should not have constructor
     * exposed.
     */
    private MessageServiceHelper() {

    }
    /**
     * Message update helper which has business logic about how to update message
     * in mailbox. 
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for message create header
     *            operation. e.g. email, folderId, messageId etc.
     * @throws MxOSException Exception.
     */
    public static void updateMessageBody(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
         metaCRUD.updateMessageBody(mxosRequestState);
    }
    
    /**
     * Get folder name from input parameters map. The returned folder name has
     * format of "abc/def". Inbox is converted to upper case.
     * 
     * @param key
     *            parameter name
     * @param mxosRequestState
     *            the request state that includes input parameter
     * @return folder name. Returns null if the input parameter does not contain
     *         the value for the key
     */
    public static String retrieveFolderName(String key,
            final MxOSRequestState mxosRequestState) {
        
        final Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();
        
        if (!inputParams.containsKey(key)) {
            return null;
        }
        
        String folderName = inputParams.get(key).get(0);
        
        // remove slashes from left and right if any
        while (folderName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            folderName = folderName.substring(1);
        }
        while (folderName.endsWith(MxOSConstants.FORWARD_SLASH)) {
            folderName = folderName.substring(0, folderName.length() - 1);
        }

        // when create inbox sub folder,whatever inbox/abc or INBOX/abc,
        // they all can be created successfully and stored as unified name INBOX/abc,
        // so when read folder by folder name,if folder name contains inbox's sub folder, 
        // convert inbox to INBOX, e.g inbox/abc -> INBOX/abc
        int index = folderName.indexOf(MxOSConstants.FORWARD_SLASH);
        if (index != -1) {
            if (folderName.substring(0, index).equalsIgnoreCase(
                    MxOSConstants.INBOX)) {
                folderName = MxOSConstants.INBOX
                        + folderName.substring(index, folderName.length());
            }
        } else {
            if (folderName.equalsIgnoreCase(MxOSConstants.INBOX)) {
                folderName = MxOSConstants.INBOX;
            }
        }
        
        return folderName;
    }

    /**
     * Get folder name from input parameters map. The returned folder name has
     * format of "abc/def". Inbox is converted to upper case. Input parameter
     * name is fixed to "folderName".
     * 
     * @param key
     *            parameter name
     * @param mxosRequestState
     *            the request state that includes input parameter
     * @return folder name. Returns null if the input parameter does not contain
     *         the value for the key
     */
    public static String retrieveFolderName(final MxOSRequestState mxosRequestState) {
        return retrieveFolderName(FolderProperty.folderName.name(), mxosRequestState);
    }
}
