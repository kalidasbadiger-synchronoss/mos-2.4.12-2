/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to check signatures.
 * 
 * @author mxos-dev
 */
public class CheckValidMailSendSignature implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(CheckValidMailSendSignature.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "CheckValidMailSendSignature action start."));
        }
        try {
            List<Signature> signatureList = null;
            MailSend ms = (MailSend) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);
            signatureList = ms.getSignatures();

            String signatureId = requestState.getInputParams()
                    .get(MailboxProperty.signatureId.name()).get(0);

            if (signatureId == null)
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_SIGNATURE_ID.name());

            boolean exists = false;

            Signature newSign = new Signature();
            if (signatureList != null) {
                for (Signature sgn : signatureList) {
                    if (sgn.getSignatureId() == Integer.parseInt(signatureId)) {
                        newSign = sgn;
                        exists = true;
                    }
                }
                if (exists == false) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_MAIL_SEND_SIGNATURE_NOT_FOUND
                                    .name());
                }
            }

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.signature,
                    newSign);

        } catch (InvalidRequestException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while checking if signatureId is existing.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_MAIL_SEND_SIGNATURE.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "CheckValidMailSendSignature action end."));
        }
    }
}
