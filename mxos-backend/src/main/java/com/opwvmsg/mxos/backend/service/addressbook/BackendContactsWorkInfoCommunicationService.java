/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Communication;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsWorkInfoCommunicationService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts WorkInfo Communication service exposed to client which is
 * responsible for doing basic WorkInfoCommunication related activities e.g read
 * WorkInfoCommunication, update WorkInfoCommunication, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendContactsWorkInfoCommunicationService implements
        IContactsWorkInfoCommunicationService {

    private static Logger logger = Logger
            .getLogger(BackendContactsWorkInfoCommunicationService.class);

    /**
     * Default Constructor.
     */
    public BackendContactsWorkInfoCommunicationService() {
        logger.info("BackendContactsWorkInfoCommunication Service created...");
    }

    @Override
    public Communication read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams,
                    ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.GET, t0, StatStatus.pass);

            return ((Communication) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(
                            MxOSPOJOs.contactsWorkInfoCommunication));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.GET, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams,
                    ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.POST, t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsWorkInfoCommunicationService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }
}
