/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.DataMap;

/**
 * This class creates LDAP filter strings used by any mOS Backend methods. 
 */
public class LdapFilterFactory {
    
    // Fixed strings
    //
    private final static String replaceKeyUid = makeKey(MailboxProperty.uid);
    private final static String replaceKeyEmail = makeKey(MailboxProperty.email);
    private final static String replaceKeyUserName = makeKey(MailboxProperty.userName);
    
    // utilities
    //
    /**
     * Generate a keyword in filter format from a property enum. 
     * For example, when MailboxProperty.email (="email") is given,
     * the method returns a string "{email}".
     * 
     * @param property
     * @return keyword used for expanding LDAP filter format. 
     */
    private static String makeKey(DataMap.Property property) {
        return "{" + property.name() + "}";
    }
    
    /**
     * Create an LDAP filter from given filter format. The format represents an
     * LDAP filter with keywords placed at variables.
     * 
     * ex) When format="(mail={email})", key="{email}", and
     * value="abc@example.com", the output would be "(mail=abc@example.com)".
     * 
     * @param format Filter format.
     * @param key Keyword to be replaced.
     * @param value Value to replace.
     * @return LDAP filter string.
     */
    private static String getFormattedFilter(final String format,
            final String key, final String value) {
        return format.replace(key, LDAPUtils.escapeLdap(value));
    }

    //
    // end utilities

    //////////////////////////////////////////////////////////////////////
    // Filter generating methods follows. All methods should have package
    // scope.
    //
    
    /**
     * @param uid User ID
     * @return Filter to search for a user entry by user ID such as email, user
     *         name, or mailbox ID.
     */
    static String getMailboxFilter(final String uid) {
        String filter = System.getProperty(SystemProperty.ldapReadMailboxFilter
                .name());
        return getFormattedFilter(filter, replaceKeyUid, uid);
    }

    /**
     * @param cosId CoS name.
     * @return Filter to search for a CoS entry by a CoS ID.
     */
    static String getCosFilter(final String cosId) {
        return "(&(objectclass=adminPolicy)(cn=" +  LDAPUtils.escapeLdap(cosId) + "))";
    }
    
    /**
     * @param email Email address.
     * @return Filter to search for an user entry by an email address.  
     */
    static String getEmailFilter(final String email) {
        return getFormattedFilter(
                MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MAILORMAILALTERNATEADDRESS,
                replaceKeyEmail, email);
    }
    
    /**
     * @param userName Login name.
     * @return Filter to search for an user entry by a login name.
     */
    static String getUserNameFilter(final String userName) {
        return getFormattedFilter(
                MxOSConstants.QUERY_SEARCH_CRITERIA_USING_MAILLOGIN,
                replaceKeyUserName, userName);
    }
}
