package com.opwvmsg.mxos.backend.action.tasks.recurrence;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

public class UpdateTaskRecurrence implements MxOSBaseAction {
	private static Logger logger = Logger.getLogger(UpdateTaskRecurrence.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("action start."));
        }

        ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
        ITasksCRUD tasksCRUD = null;

        try {
            tasksCRUDPool = MxOSApp.getInstance().getTasksCRUD();
            tasksCRUD = tasksCRUDPool.borrowObject();

            tasksCRUD.updateTasksRecurrence(requestState);
            
        } catch (TasksException e) {
            logger.error("Error while update task recurrence.", e);
            ExceptionUtils.createMxOSExceptionFromTasksException(
                    TasksError.TSK_TASKRECURRENCE_UNABLE_TO_UPDATE, e);
        } catch (final Exception e) {
            logger.error("Error while get base.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (tasksCRUDPool != null && tasksCRUD != null) {
                    tasksCRUDPool.returnObject(tasksCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("action end."));
        }
		
	}
}
