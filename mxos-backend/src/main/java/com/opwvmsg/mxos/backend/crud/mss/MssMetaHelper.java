/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.mss;

import java.util.Map;

import com.opwvmsg.mxos.backend.crud.IMetaHelper;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.exception.InvalidRequestException;

public class MssMetaHelper implements IMetaHelper {

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final MailboxProperty key, final Object value)
        throws InvalidRequestException {
        getAttributes(mxosRequestState).put(
                key.name(), value.toString());
    }

    private Map<String, String> getAttributes(MxOSRequestState requestState) {
        IBackendState backendState =
                requestState.getBackendState().get(
                        MetaDBTypes.mss.name());
        if (backendState == null) {
            backendState = new MssMetaBackendState();
            requestState.getBackendState().put(MetaDBTypes.mss.name(),
                    backendState);
        }
        return ((MssMetaBackendState) backendState).attributes;
    }
}
