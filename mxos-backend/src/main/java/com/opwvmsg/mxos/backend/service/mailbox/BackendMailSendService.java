/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.IMailSendService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox mailSend operations interface which will be exposed to the client.
 * This interface is responsible for doing mailSend related operations (like
 * Create, Read, Update, Delete, etc.) directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendMailSendService implements IMailSendService {

    private static Logger logger = Logger
            .getLogger(BackendMailSendService.class);

    /**
     * Default Constructor.
     */
    public BackendMailSendService() {
        logger.info("BackendMailSendService created...");
    }

    @Override
    public MailSend read(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MailSendService, Operation.GET, t0,
                    StatStatus.pass);

            return (MailSend) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailSend);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MailSendService, Operation.POST);
            if (inputParams == null || inputParams.size() <= 1) {
                ExceptionUtils.createInvalidRequestException(mxosRequestState);
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MailSendService, Operation.POST, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MailSendService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
