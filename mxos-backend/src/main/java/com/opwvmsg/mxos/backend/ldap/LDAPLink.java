/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap;

import java.util.Hashtable;

import javax.naming.directory.DirContext;

/**
 * Class to maintain LDAP link information
 * 
 * @author mxos-dev
 * 
 */
public class LDAPLink {
    // LDAP configuration key define
    public static final String LDAP_CTX_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
    public static final String LDAP_READ_TIMEOUT = "com.sun.jndi.ldap.read.timeout";
    public static final String LDAP_CONNECT_TIMEOUT = "com.sun.jndi.ldap.connect.timeout";

    // default value
    public static final String DEFAULT_SECURITY_AUTHENTICATION = "simple";
    public static final int DEFAULT_READ_TIMEOUT = 10000;
    public static final int DEFAULT_CONNECT_TIMEOUT = 500;

    // connection parameters
    private String url;
    private String userName;
    private String password;
    private int readTimeout = DEFAULT_READ_TIMEOUT;
    private int connectTimeout = DEFAULT_CONNECT_TIMEOUT;
    private String securityAuthentication = DEFAULT_SECURITY_AUTHENTICATION;

    public LDAPLink(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    public LDAPLink setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
        return this;
    }

    public LDAPLink setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    public LDAPLink setSecurityAuthentication(String securityAuthentication) {
        this.securityAuthentication = securityAuthentication;
        return this;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public String getSecurityAuthentication() {
        return securityAuthentication;
    }

    public Hashtable<String, String> get() {
        Hashtable<String, String> env = new Hashtable<String, String>();

        // set connection parameters
        env.put(DirContext.INITIAL_CONTEXT_FACTORY, LDAP_CTX_FACTORY);
        env.put(DirContext.SECURITY_PRINCIPAL, userName);
        env.put(DirContext.SECURITY_CREDENTIALS, password);
        env.put(DirContext.PROVIDER_URL, url);
        env.put(DirContext.SECURITY_AUTHENTICATION, securityAuthentication);
        env.put(LDAP_READ_TIMEOUT, String.valueOf(readTimeout));
        env.put(LDAP_CONNECT_TIMEOUT, String.valueOf(connectTimeout));

        return env;
    }

}
