/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update allowed senders list.
 *
 * @author mxos-dev
 */
public class UpdateAllowedSendersList implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateAllowedSendersList.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateAllowedSendersList action start."));
        }
        
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
        IMailboxCRUD mailboxCRUD = mailboxCRUDPool.borrowObject();
        
        try {
            mailboxCRUD.makeModifyEntryInContext(requestState,
                    MailboxProperty.oldAllowedSender,
                    MailboxProperty.newAllowedSender,
                    MailboxProperty.allowedSendersList,
                    MailboxError.MBX_INVALID_OLD_ALLOWED_SENDER);
            if (logger.isDebugEnabled()) {
                logger.debug("Action was done successfully.");
            }            
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateAllowedSendersList action end."));
        }
    }
}
