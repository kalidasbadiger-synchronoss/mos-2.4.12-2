/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/AuthQueryEnum.java#1 $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

/**
 *
 * @author Aricent
 *
 */
public enum AuthQuery {

    SMTPQUERY('S'), POPQUERY('P'), MBOXQUERY('M'), ALIASQUERY('A');

    private int query;

    /**
     *
     * @param status
     *            status
     */
    private AuthQuery(int query) {
        this.query = query;
    }

    /**
     * Returns the corresponding {@link String}.
     *
     * @return password hash type
     */
    public int getQuery() {
        return query;
    }
}
