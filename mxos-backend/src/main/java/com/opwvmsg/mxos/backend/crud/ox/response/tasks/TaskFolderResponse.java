/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.util.List;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.sun.jersey.api.client.ClientResponse;

public class TaskFolderResponse extends Response {

    /**
     * Get a TaskFolder Response.
     * 
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public Folder getTaskFolder(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXTasksProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXTasksProperty.data.name());
        }

        Folder taskFolder = JsonToTasksMapper.mapToTaskFolder(root);

        return taskFolder;
    }

    /*
     * Get all Tasks.
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public List<String> getAllFolderIds(ClientResponse resp)
            throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<String> tasks = JsonToTasksMapper.mapToAllFolderIds(root);

        return tasks;
    }
    
    /*
     * Get all Folders.
     * @param resp resp.
     * @throws TasksException in case any error.
     */
    public List<Folder> getAllFolders(ClientResponse resp)
            throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }

        List<Folder> folders = JsonToTasksMapper.mapToAllFolders(root);

        return folders;
    }
}
