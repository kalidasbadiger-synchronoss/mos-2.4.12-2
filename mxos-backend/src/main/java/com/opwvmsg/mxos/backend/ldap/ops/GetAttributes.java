/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import com.opwvmsg.mxos.backend.ldap.LDAPMapper;

/**
 * Class to execute getAttributes operation
 * 
 * @author mxos-dev
 * 
 */
public class GetAttributes extends Operation {

    private String name;
    private String[] attrIds;

    private Attributes result;

    public GetAttributes(String name, String[] attrIds) {
        this.name = name;
        this.attrIds = attrIds;
    }

    protected void invoke() throws NamingException {
        this.result = ldapContext.getAttributes(name, attrIds);
    }

    public Attributes getResult() {
        return result;
    }

    public <T> T getResult(LDAPMapper<T> mapper) {
        return mapper.mapping(result);
    }

}
