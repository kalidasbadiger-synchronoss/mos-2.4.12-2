/*
/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.members;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set group member email.
 * 
 * @author mxos-dev
 */
public class SetGroupMemberEmail implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetGroupMemberEmail.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupMemberEmail action start."));
        }

        if (mxosRequestState.getInputParams().containsKey(
                AddressBookProperty.memberEmail.name())) {
            if (mxosRequestState.getInputParams().containsKey(
                    AddressBookProperty.memberList.name())) {
                // Set Multiple Group Member Email
                Object[] memberNames = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberEmail.name()).toArray();

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttributes(mxosRequestState,
                                AddressBookProperty.memberEmail, memberNames);
            } else {
                // Set single Group Member Email
                String memberName = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.memberEmail.name()).get(0);

                MxOSApp.getInstance()
                        .getAddressBookHelper()
                        .setAttribute(mxosRequestState,
                                AddressBookProperty.memberEmail, memberName);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetGroupMemberEmail action end."));
        }
    }
}
