/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-actions/src/main/java/com/openwave/mxos/domain/actions/DeleteDomain.java#2 $
 */

package com.opwvmsg.mxos.backend.action.domain;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums.DomainType;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update Domain object.
 * 
 * @author mxos-dev
 */
public class UpdateDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateDomain.class);

    /**
     * Action method to update Domain object.
     * 
     * @param model model instance
     * @throws Exception in case of any error
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateDomain action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            if (requestState.getInputParams().containsKey(
                    DomainProperty.type.name())) {
                final DomainType domainType = DomainType.fromValue(requestState
                        .getInputParams().get(DomainProperty.type.name())
                        .get(0));

                String newRelayHost = "";
                if (requestState.getInputParams().get(
                        DomainProperty.relayHost.name()) != null) {
                    newRelayHost = requestState.getInputParams()
                            .get(DomainProperty.relayHost.name()).get(0);
                }

                String newRewriteDomain = "";
                if (requestState.getInputParams().get(
                        DomainProperty.alternateDomain.name()) != null) {
                    newRewriteDomain = requestState.getInputParams()
                            .get(DomainProperty.alternateDomain.name()).get(0);
                }
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState, DomainProperty.type,
                                domainType);
                if (domainType == DomainType.NONAUTH) {
                    if (newRelayHost != null && newRelayHost.length() != 0) {
                        MxOSApp.getInstance()
                                .getMailboxHelper()
                                .setAttribute(requestState,
                                        DomainProperty.relayHost, newRelayHost);
                    } else {
                        throw new InvalidRequestException(
                                DomainError.DMN_MISSING_RELAYHOST.name(),
                                "RelayHost is required for nonauth domain");
                    }
                } else if (domainType == DomainType.REWRITE) {
                    if (newRewriteDomain != null
                            && !"".equals(newRewriteDomain)) {
                        MxOSApp.getInstance()
                                .getMailboxHelper()
                                .setAttribute(requestState,
                                        DomainProperty.alternateDomain,
                                        newRewriteDomain);
                    } else {
                        throw new InvalidRequestException(
                                DomainError.DMN_MISSING_ALTERNATEDOMAIN.name(),
                                "rewriteDomain is required for rewrite domain");
                    }
                }
            }

            mailboxCRUDPool = MxOSApp.getInstance()
                    .getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();

            DomainProvisionHelper.update(mailboxCRUD, requestState);
            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while update domain.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(
                    DomainError.DMN_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateDomain action end."));
        }
    }
}
