/*
 * Copyright (c) 2015 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.InitialLdapContext;
/**
 * This class behaves the same with InitialLdapContext excepts
 * it can accept "authoritative" as parameter.
 */
public class OWBaseLdapContext extends InitialLdapContext {
    
    /**
     * The ctor initializes the super class and sets the master LDAP client pool. 
     * 
     * @param environment
     * @param connCtls 
     * @param masterLdapClientPool
     * @throws NamingException
     */
    public OWBaseLdapContext(Hashtable<String, String> environment,
            Control[] connCtls)
            throws NamingException {
        
        super(environment, connCtls);
    }
    

    public NamingEnumeration<SearchResult> search(String name, String filter,
            SearchControls cons, boolean authoritative) throws NamingException {
        return super.search(name, filter, cons);
    }
    
    public Attributes getAttributes(String name, String[] attrIds, boolean authoritative)
            throws NamingException {
        return super.getAttributes(name, attrIds);
    }
}