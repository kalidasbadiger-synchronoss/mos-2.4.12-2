/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.mailbox;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendCredentialService implements ICredentialService {

    private static Logger logger = Logger
            .getLogger(BackendCredentialService.class);

    /**
     * Default Constructor.
     */
    public BackendCredentialService() {
        logger.info("BackendCredentialService created...");
    }

    @Override
    public Credentials read(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.CredentialService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.CredentialService, Operation.GET, t0,
                    StatStatus.pass);

            return (Credentials) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.credentials);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.CredentialService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.CredentialService, Operation.POST);
            if (inputParams == null || inputParams.size() <= 1) {
                ExceptionUtils.createInvalidRequestException(mxosRequestState);
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.CredentialService, Operation.POST, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.CredentialService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void authenticate(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AuthenticateService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.AuthenticateService, Operation.POST,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.AuthenticateService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Mailbox authenticateAndGetMailbox(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.AuthenticateAndGetMailboxService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.AuthenticateAndGetMailboxService,
                    Operation.POST, t0, StatStatus.pass);
            return (Mailbox) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mailbox);
        } catch (MxOSException e) {
            logger.warn("Auth failed: " + e);
            Stats.stopTimer(ServiceEnum.AuthenticateAndGetMailboxService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }
}