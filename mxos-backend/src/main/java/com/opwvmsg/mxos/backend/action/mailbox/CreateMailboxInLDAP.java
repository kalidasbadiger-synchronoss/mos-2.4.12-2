/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSRollbackAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create mailbox in LDAP.
 * 
 * @author mxos-dev
 */
public class CreateMailboxInLDAP extends AbstractMailbox implements
        MxOSRollbackAction {
    private static Logger logger = Logger.getLogger(CreateMailboxInLDAP.class);

    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInLDAP action start.");
        }

        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // check whether skip mailbox creation in LDAP
        boolean create = true;
        if (requestState.getInputParams().containsKey(SKIP_LDAP)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_LDAP).get(0));
            if (skip) {
                create = false;
                logger.info("Skip mailbox [" + email + "] creation in LDAP");
            }
        }

        if (create) {
            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
            try {
                mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                MailboxProvisionHelper.create(mailboxCRUD, requestState);
                logger.info("Create mailbox [" + email
                        + "] in LDAP successfully.");
            } catch (final MxOSException e) {
                // If mailbox already exists on LDAP and and mailboxId exists in
                // request parameter try creating it on MSS
                if (e.getCode() != null
                        && e.getCode().equals(
                                MailboxError.MBX_ALREADY_EXISTS.name())
                        && requestState.getInputParams().containsKey(
                                MailboxProperty.mailboxId.name())) {
                    logger.warn("Mailbox [" + email
                            + "] already exists in LDAP.");
                    // check if errorOnMailboxAlreadyExists exists in input
                    // raise an exception if errorOnMailboxAlreadyExists is true
                    if ((requestState.getInputParams().containsKey(
                            ERROR_ON_ALREADY_EXISTS) && Boolean
                            .parseBoolean(requestState.getInputParams()
                                    .get(ERROR_ON_ALREADY_EXISTS).get(0)))
                            || (requestState.getInputParams().containsKey(
                                    LDAP_MAILBOX_EXISTS_RETURN_ERROR) && Boolean
                                    .parseBoolean(requestState
                                            .getInputParams()
                                            .get(LDAP_MAILBOX_EXISTS_RETURN_ERROR)
                                            .get(0)))) {
                        logger.error("Error on mailbox [" + email
                                + "] already exists in LDAP.", e);
                        throw e;
                    } else {
                        List<String> list = new ArrayList<String>(
                                    Arrays.asList("true"));
                        requestState.getInputParams()
                                   .put(IGNORE_ERROR_ON_ALREADY_EXISTS, list);
                    }
                    logger.info("Try to create mailbox [" + email
                            + "] in MSS at next action.");
                } else {
                    logger.error("Error while creating mailbox [" + email
                            + "] in LDAP.", e);
                    throw e;
                }
            } catch (final Exception e) {
                logger.error("Error while creating mailbox [" + email
                        + "] in LDAP.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            } finally {
                CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInLDAP action end.");
        }
    }

    public void rollback(MxOSRequestState requestState) throws MxOSException {
        if (requestState.getInputParams().containsKey(
                IGNORE_ERROR_ON_ALREADY_EXISTS) && Boolean
                .parseBoolean(requestState.getInputParams()
                        .get(IGNORE_ERROR_ON_ALREADY_EXISTS).get(0))) {
            return;
            
        }
        ActionFactory.getInstance().exec(DeleteMailboxFromLDAP.class.getName(),
                requestState);
    }
}
