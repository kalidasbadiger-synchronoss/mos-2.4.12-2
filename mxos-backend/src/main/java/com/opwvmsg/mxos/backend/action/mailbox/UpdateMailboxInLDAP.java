/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;


/**
 * Action class to update mailbox in backend DB
 * 
 * TODO: Class/action name ...InLDAP is inappropriate in architecture
 * perspective.  Probably renaming to ...InDb or ...InBackend is better.
 * This layer is designed to be independent from backend DB type. I keep
 * it as is since changing the name here is quite impactful to entire
 * configuration, but please consider...
 *
 * @author mxos-dev
 * 
 */
public class UpdateMailboxInLDAP extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateMailboxInLDAP.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInLDAP action start.");
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
        IMailboxCRUD mailboxCRUD = mailboxCRUDPool.borrowObject();
        try {
            mailboxCRUD.updateMailbox(requestState);
            if (logger.isDebugEnabled()) {
                logger.debug("Update mailbox in LDAP successfully.");
            }
        } catch (MxOSException e) {
            logger.error("Error while updating mailbox in LDAP", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updating mailbox in LDAP", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInLDAP action end.");
        }
    }
}
