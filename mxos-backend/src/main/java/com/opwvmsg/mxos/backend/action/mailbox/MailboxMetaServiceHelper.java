/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import java.util.ArrayList;
import java.util.List;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.folder.FolderServiceHelper;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Mailbox service helper APIs which has business logic and does actual
 * operation using CRUD APIs.
 *
 * @author mxos-dev
 */
public final class MailboxMetaServiceHelper {

    /**
     * Mailbox create helper which has business logic about how to create
     * mailbox. It uses metadata CRUD APIs to create mailbox.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for mailbox create operation. e.g.
     *            mailboxId, etc.
     * @throws MxOSException Exception.
     */
    public static void create(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.lastaccessdate, System.currentTimeMillis());
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.nummsgread, 0);
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.nummsgs, 0);
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.sizemsgread, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.sizemsgs, 0L);
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.status, MxOSConstants.NOT_SOFT_DELETE);

        metaCRUD.createMailbox(mxosRequestState);
        
        if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
            final String[] systemFolders = (String[]) mxosRequestState
                    .getAdditionalParams().getPropertyAsObject(
                            MailboxProperty.systemfolders);

            /*
             * TODO: Here assumption is that user will provide first folder
             * always as INBOX, need to write more logic to re-arrange folder if
             * in-case user doesn't provide INBOX as first, for now this is good
             * enough.
             */

            mxosRequestState.getInputParams().put(
                    FolderProperty.parentFolderId.name(),
                    new ArrayList<String>());
            mxosRequestState.getInputParams()
                    .get(FolderProperty.parentFolderId.name())
                    .add("" + MxOSConstants.ROOT_FOLDER_ID);
            for (int i = MxOSConstants.INBOX_ID; i <= systemFolders.length; i++) {
                mxosRequestState.getAdditionalParams().setProperty(
                        FolderProperty.folderId, i);
                // Default root folder
                FolderServiceHelper.create(metaCRUD, mxosRequestState,
                        systemFolders[i - MxOSConstants.INBOX_ID]);
            }
        }
        // Update mss entry if autoReplyMessage or rmFilter or mtaFilter exist
        // in the request
        metaCRUD.updateMailbox(mxosRequestState);
    }

    /**
     * Mailbox read helper which has business logic about how to read mailbox.
     * It uses metadata CRUD APIs to read mailbox.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState Mandatory parameters for mailbox read operation.
     *            e.g. mailboxId, etc.
     * @param mailbox Mailbox POJO which will be populated with mailbox data.
     * @throws MxOSException Exception.
     */
    public static void read(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState, final Base base)
            throws MxOSException {
        metaCRUD.readMailbox(mxosRequestState, base);
    }

    /**
     * Mailbox update helper which has business logic about how to create
     * mailbox. It uses metadata CRUD APIs to update mailbox.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for mailbox update
     *            operation. e.g. mailboxId, etc.
     * @throws MxOSException Exception.
     */

    public static void update(IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        mxosRequestState.getAdditionalParams().setProperty(
                MailboxProperty.lastaccessdate, System.currentTimeMillis());
        metaCRUD.updateMailbox(mxosRequestState);
    }

    /**
     * Mailbox delete helper which has business logic about how to hard delete
     * mailbox. It uses metadata CRUD APIs to delete mailbox.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param blobCRUD blobCRUD CRUD connection object.
     * @param mxosRequestState - parameters for mailbox hard delete
     *            operation. e.g. mailboxId, etc.
     * @throws MxOSException Exception.
     */

    public static void delete(final IMetaCRUD metaCRUD,
            final IBlobCRUD blobCRUD, final MxOSRequestState mxosRequestState)
            throws MxOSException {
        final List<Folder> folders = new ArrayList<Folder>();
        FolderServiceHelper.list(metaCRUD, mxosRequestState, folders);

        for (final Folder f : folders) {
            mxosRequestState.getInputParams().put(
                    FolderProperty.folderId.name(),
                    new ArrayList<String>());
            mxosRequestState.getInputParams().get(
                    FolderProperty.folderId.name()).add("" + f.getFolderId());
            FolderServiceHelper.delete(metaCRUD, blobCRUD, mxosRequestState);
        }
        metaCRUD.deleteMailbox(mxosRequestState);
    }

    /**
     * Mailbox helper which has business logic about how to update last access
     * time.It uses metadata CRUD APIs to do so.
     *
     * @param metaCRUD metadata CRUD connection object.
     * @param mxosRequestState - parameters for mailbox access time
     *            update operation. e.g. mailboxId, etc.
     * @throws MxOSException Exception.
     */

    public static void updateLastAccessTime(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        metaCRUD.updateMailboxLastAccessTime(mxosRequestState,
                System.currentTimeMillis());
    }
    
    
    /**
     * Mailbox helper which has business logic about how to get last access
     * time.It uses metadata CRUD APIs to do so.
     * 
     * @param metaCRUD
     * @param mxosRequestState
     * @return lastAccessTime
     * @throws MxOSException
     */
    public static int getLastAccessTime(final IMetaCRUD metaCRUD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        return metaCRUD.getMailboxLastAccessTime(mxosRequestState);
    }

    /**
     * Private constructor because utility class should not have constructor
     * exposed.
     */
    private MailboxMetaServiceHelper() {

    }
}
