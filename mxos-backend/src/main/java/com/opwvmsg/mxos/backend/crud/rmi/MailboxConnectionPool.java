/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.rmi;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

public class MailboxConnectionPool implements ICRUDPool<IRMIMailboxCRUD> {

    private static Logger logger = Logger
            .getLogger(MailboxConnectionPool.class);

    private GenericObjectPool<AppSuiteRMIMailboxCRUD> objPool;

    public static final String RMI_REGISTRY_URL = "rmiRegistryUrl";

    private static MailboxConnectionPool instance = new MailboxConnectionPool();

    /**
     * Method to get Instance of RestConnectionPool object.
     * 
     * @return RestConnectionPool object
     * @throws Exception Exception
     */
    public static MailboxConnectionPool getInstance() {
        return instance;
    }

    /**
     * Constructor.
     * 
     * Configure mxos-host in hosts file.
     * 
     * @throws Exception Exception.
     */
    public MailboxConnectionPool() {
        createRMIObjectPool();
    }

    @Override
    public IRMIMailboxCRUD borrowObject() throws MxOSException {
        IRMIMailboxCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.warn("Error while borrowing object. " + e.getMessage());
            throw new ApplicationException(
                    ErrorCode.RMI_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.warn("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.RMI_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    private void createRMIObjectPool() {
        try {
            String rmiHost = null;
            int rmiPort = 1099;
            if (System.getProperties().containsKey(
                    SystemProperty.oxRMIHost.name())) {
                rmiHost = System.getProperty(SystemProperty.oxRMIHost.name());
            } else {
                // Default host
                logger.warn("Using default RMI host [localhost]");
                rmiHost = "localhost";
            }
            if (System.getProperties().containsKey(
                    SystemProperty.oxRMIPort.name())) {
                try {
                    rmiPort = Integer.parseInt(System
                            .getProperty(SystemProperty.oxRMIPort.name()));
                } catch (NumberFormatException e) {
                    logger.warn("Using default RMI port [1099]");
                }
            }
            int rmiMaxPoolStubs;
            if (System.getProperties().containsKey(
                    SystemProperty.rmiMaxPoolStubs.name())) {
                rmiMaxPoolStubs = Integer.parseInt(System
                        .getProperty(SystemProperty.rmiMaxPoolStubs.name()));
            } else {
                // Default max connections
                rmiMaxPoolStubs = 10;
            }
            System.out.println("RMI Max Pool Stubs = " + rmiMaxPoolStubs);
            objPool = new GenericObjectPool<AppSuiteRMIMailboxCRUD>(
                    new MailboxFactory(rmiHost, rmiPort), rmiMaxPoolStubs);
            objPool.setMaxIdle(-1);
            System.out
                    .println("RESTMxosObjectPool Created with default values...");
            initializeJMXStats(rmiMaxPoolStubs);
        } catch (Exception e) {
            System.out
                    .println("Problem occured while creating RESTMxosObjectPool with default values...");
            e.printStackTrace();
        }
    }

    @Override
    public void resetPool() throws MxOSException {
        // do nothing
    }

    @Override
    public void returnObject(IRMIMailboxCRUD rmiMetaCRUD) throws MxOSException {
        try {
            objPool.returnObject((AppSuiteRMIMailboxCRUD) rmiMetaCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.RMI_CONNECTION_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.MAILBOX.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_MAILBOX.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_MAILBOX.decrement();
    }
}
