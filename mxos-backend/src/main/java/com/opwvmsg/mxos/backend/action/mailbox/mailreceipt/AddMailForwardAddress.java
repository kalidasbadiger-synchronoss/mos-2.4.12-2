/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to add mail forwarding address.
 *
 * @author mxos-dev
 */
public class AddMailForwardAddress implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddMailForwardAddress.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Add ForwardAddress action start."));
        }
        try {
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final List<String> newForwardingAddresses = requestState.getInputParams()
                    .get(MailboxProperty.forwardingAddress.name());

            List<String> fwdAddressList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.forwardingAddresses);
            if (null == fwdAddressList) {
                fwdAddressList = new ArrayList<String>();
            }

            int maxNumForwardingAddresses = -1; // initialize to an invalid value
            List<String> maxNumForwardingAddressesList = requestState.getInputParams()
                    .get(MailboxProperty.maxNumForwardingAddresses.name());
            
            if (maxNumForwardingAddressesList != null && maxNumForwardingAddressesList.get(0) != null) {
            	try {
            		maxNumForwardingAddresses = Integer.parseInt(maxNumForwardingAddressesList.get(0));
            	} catch (NumberFormatException nfe) {
            		logger.warn("Exception occurred while parsing maxNumForwardingAddressesList=" 
            				+ maxNumForwardingAddressesList.get(0) 
            				+ ", ignoring the exception and continuing");
            	}
            } else {
                MailReceipt mailReceipt = (MailReceipt) requestState
                        .getDbPojoMap().getPropertyAsObject(
                                MxOSPOJOs.mailReceipt);
                if (mailReceipt != null && mailReceipt.getMaxNumForwardingAddresses() != null) {
                	maxNumForwardingAddresses = mailReceipt.getMaxNumForwardingAddresses();
                }
            }
            
            int numForwardingAddresses = fwdAddressList.size() + newForwardingAddresses.size();
            
            if (maxNumForwardingAddresses != -1 && 
            		numForwardingAddresses > maxNumForwardingAddresses) {
                logger.error("Number of forwarding addresses=" 
            		+ numForwardingAddresses 
            		+ " exceeds the maxNumForwardingAddresses=" + maxNumForwardingAddresses);
                throw new MxOSException(
                        MailboxError.MBX_FORWARD_ADDRESSES_REACHED_MAX_LIMIT.name());
            }

            for (String forwardingAddress : newForwardingAddresses) {
                if (email.toLowerCase().equalsIgnoreCase(
                        forwardingAddress.toLowerCase())) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_FORWARD_ADDRESSES_AND_EMAIL_SAME
                                    .name());
                }
                if (!fwdAddressList.contains(forwardingAddress.toLowerCase())) {
                    fwdAddressList.add(forwardingAddress.toLowerCase());
                }
            }
            final String[] fwdAddresses = fwdAddressList
                    .toArray(new String[fwdAddressList.size()]);

            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.forwardingAddress,
                            fwdAddresses);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add mail forward address.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_FORWARD_ADDRESS_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Add ForwardAddress action end."));
        }
    }
}
