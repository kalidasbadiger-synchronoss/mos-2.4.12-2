/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.notify;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.error.NotifyError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.notify.INotifyService;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.Stats;

import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.NotifyService;
import static com.opwvmsg.mxos.interfaces.service.Operation.GET;
import static com.opwvmsg.mxos.interfaces.service.Operation.DELETE;
import static com.opwvmsg.mxos.interfaces.service.Operation.POST;
import static com.opwvmsg.mxos.utils.misc.StatStatus.pass;
import static com.opwvmsg.mxos.utils.misc.StatStatus.fail;
import static com.opwvmsg.mxos.data.enums.MxOSPOJOs.notify;

/**
 * Implements of {@link INotifyService} and inherits
 * {@link NotifySubsBaseBackend}
 * 
 * @author
 */
public class BackendNotifyService implements INotifyService {
    
    @Override
    public Notify read(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (!MxOSConfig.isNotifyServiceEnabled()) {
            return null;
        }
        final long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, NotifyService, GET);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(NotifyService, GET, t0, pass);

            return (Notify) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(notify);
        } catch (MxOSException e) {
            Stats.stopTimer(NotifyService, GET, t0, fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (!MxOSConfig.isNotifyServiceEnabled()) {
            return;
        }
        final long t0 = Stats.startTimer();
        try {
        final MxOSRequestState mxosRequestState = new MxOSRequestState(
                inputParams, NotifyService, DELETE);
        BackendServiceUtils.validateAndExecuteService(mxosRequestState);

        Stats.stopTimer(NotifyService, DELETE, t0, pass);
        } catch (MxOSException e) {
            Stats.stopTimer(NotifyService, DELETE, t0, fail);
            throw e;
        }
    }
    
    @Override
    public void create(Map<String, List<String>> inputParams)
            throws MxOSException {
        throw new UnsupportedOperationException(
                "CREATE not supported for Notify.");        
    }
    
    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {
        throw new UnsupportedOperationException(
                "UPDATE not supported for Notify");
    }

    @Override
    public void publish(Map<String, List<String>> inputParams)
            throws MxOSException {
        if (!MxOSConfig.isNotifyServiceEnabled()) {
            return;
        }
        final long t0 = Stats.startTimer();
        try {
        final MxOSRequestState mxosRequestState = new MxOSRequestState(
                inputParams, NotifyService, POST);
        BackendServiceUtils.validateAndExecuteService(mxosRequestState);

        Stats.stopTimer(NotifyService, POST, t0, pass);
        } catch (MxOSException e) {
            if (e.getCode().equals(NotifyError.NTF_TOPIC_NOT_FOUND.name())) {
                Stats.stopTimer(NotifyService, POST, t0, pass);
            } else {
                Stats.stopTimer(NotifyService, POST, t0, fail);
            }
            throw e;
        }
    }
    
}
