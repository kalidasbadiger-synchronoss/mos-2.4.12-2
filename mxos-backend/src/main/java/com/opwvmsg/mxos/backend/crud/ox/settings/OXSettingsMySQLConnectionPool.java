/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ox.settings;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;

/**
 * This class provides connection pool for OX Settings in MySQL.
 * 
 * @author
 */
public class OXSettingsMySQLConnectionPool implements ICRUDPool<ISettingsCRUD> {
    private static class OXSettingsMySQLConnectionPoolHolder {
        public static OXSettingsMySQLConnectionPool instance = new OXSettingsMySQLConnectionPool();
    }

    private static Logger logger = Logger
            .getLogger(OXSettingsMySQLConnectionPool.class);

    public static final String OX_MYSQL_URL = "oxMySQLURL";
    public static final String OX_MYSQL_USERNAME = "oxMySQLUserName";
    public static final String OX_MYSQL_PASSWORD = "oxMySQLPassword";

    /**
     * Method to get Instance of OXSettingsMySQLConnectionPool object.
     * 
     * @return OXMySQLConnectionPool object
     * @throws Exception Exception
     */
    public static OXSettingsMySQLConnectionPool getInstance() {
        return OXSettingsMySQLConnectionPoolHolder.instance;
    }

    private String oxMySQLURL;

    private GenericObjectPool<ISettingsCRUD> objPool;

    /**
     * Constructor.
     * 
     * Configure mxos-host in hosts file.
     * 
     * @throws Exception Exception.
     */
    public OXSettingsMySQLConnectionPool() {
        createOXMySQLObjectPool();
    }

    /**
     * Method to borrow connection.
     * 
     * @return connection connection
     * @throws Exception in case no connection is available.
     */
    @Override
    public ISettingsCRUD borrowObject() throws MxOSException {
        ISettingsCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    /**
     * Create mxosObjectPool to perform CRUD operation using REST.
     * 
     * @throws Exception Exception
     */
    private void createOXMySQLObjectPool() {
        try {
            String oxMySQLURL;
            if (System.getProperties().containsKey(OX_MYSQL_URL)) {
                oxMySQLURL = System.getProperty(OX_MYSQL_URL);
            } else {
                // Default max connections
                oxMySQLURL = "openexchange";
            }
            int mxosMaxConnections;
            if (System.getProperties().containsKey(
                    SystemProperty.mxosMaxConnections.name())) {
                mxosMaxConnections = Integer.parseInt(System
                        .getProperty(SystemProperty.mxosMaxConnections.name()));
            } else {
                // Default max connections
                mxosMaxConnections = 10;
            }
            System.out.println("MxOS Max Connections = " + mxosMaxConnections);
            String userName;
            if (System.getProperties().containsKey(OX_MYSQL_USERNAME)) {
                userName = System.getProperty(OX_MYSQL_USERNAME);
            } else {
                // Default max connections
                userName = "open-exchange";
            }
            String password;
            if (System.getProperties().containsKey(OX_MYSQL_PASSWORD)) {
                password = System.getProperty(OX_MYSQL_PASSWORD);
            } else {
                // Default max connections
                password = "db_password";
            }

            objPool = new GenericObjectPool<ISettingsCRUD>(
                    new OXSettingsMySQLFactory(oxMySQLURL, userName, password),
                    mxosMaxConnections);
            objPool.setMaxIdle(-1);
            System.out
                    .println("OXSettingsObjectPool Created with default values...");
            initializeJMXStats(mxosMaxConnections);
        } catch (Exception e) {
            System.out
                    .println("Problem occured while creating OXSettingsObjectPool with default values...");
            e.printStackTrace();
        }
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }

    /**
     * Method to return the connection back.
     * 
     * @param restCRUD restCRUD
     */

    @Override
    public void returnObject(ISettingsCRUD settingsCRUD) throws MxOSException {
        try {
            objPool.returnObject(settingsCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.OXSETTINGS.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_OXSETTINGS.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_OXSETTINGS.decrement();
    }
}
