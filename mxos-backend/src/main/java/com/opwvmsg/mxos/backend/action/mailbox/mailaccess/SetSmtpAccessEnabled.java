/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailaccess;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ldap.BooleanType;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set MailAccess::SmtpAccessEnabled.
 *
 * @author mxos-dev
 */
public class SetSmtpAccessEnabled implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetSmtpAccessEnabled.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSmtpAccessEnabled action start."));
        }
        String smtpAccessEnabled = requestState.getInputParams()
                .get(MailboxProperty.smtpAccessEnabled.name()).get(0);
        if (null != smtpAccessEnabled && smtpAccessEnabled.length() > 0) {
            smtpAccessEnabled = BooleanType.getByValue(smtpAccessEnabled)
                    .ordinal() + "";
        }
        try {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.smtpAccessEnabled,
                            smtpAccessEnabled);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set smtp access enabled.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_SMTP_ACCESS_ENABLED.name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSmtpAccessEnabled action end."));
        }
    }
}
