/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class for Search Domain.
 *
 * @author mxos-dev
 */
public class SearchDomain implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SearchDomain.class);

    /**
     * Action method for search Domain.
     *
     * @param model instance of model
     * @throws Exception throws in case of any errors
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Seach Domain action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        int maxRows = MxOSApp.getInstance().getSearchMaxRows();
        if (requestState.getInputParams().containsKey(
                MailboxProperty.maxRows.name())) {
            maxRows = Integer.parseInt(requestState.getInputParams()
                    .get(MailboxProperty.maxRows.name()).get(0));
        }
        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        try {
            mailboxCRUDPool = MxOSApp.getInstance()
                    .getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();

            final List<Domain> domains = mailboxCRUD.searchDomain(
                    requestState, maxRows, maxTimeOut);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.domains, domains);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while search domain.", e);
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Search Domain action end."));
        }
    }
}