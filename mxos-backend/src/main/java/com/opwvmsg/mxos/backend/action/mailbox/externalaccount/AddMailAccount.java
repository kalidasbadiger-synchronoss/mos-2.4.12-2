/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.externalaccount;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create mailAccounts.
 * 
 * @author mxos-dev
 */
public class AddMailAccount implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddMailAccount.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddMailAccount action start."));
        }

        try {
            ExternalAccounts ea = (ExternalAccounts) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.externalAccounts);
            String accountId = requestState.getInputParams()
                    .get(MailboxProperty.accountId.name()).get(0);
            if (accountId == null)
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ACCOUNT_ID.name());

            List<MailAccount> mailAcctList = ea.getMailAccounts();
            if (mailAcctList != null) {
                for (MailAccount ma : mailAcctList) {
                    if (ma.getAccountId() == Integer.parseInt(accountId)) {
                        throw new InvalidRequestException(
                                MailboxError.MBX_MAIL_ACCOUNT_ALREADY_EXISTS
                                        .name());
                    }
                }
            } else {
                mailAcctList = new ArrayList<MailAccount>();
            }

            MailAccount newAccount = new MailAccount();
            newAccount.setAccountId(Integer.parseInt(accountId));
            mailAcctList.add(newAccount);

            ea.setMailAccounts(mailAcctList);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailAccount,
                    newAccount);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.externalAccounts,
                    ea);

        } catch (InvalidRequestException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add mail account.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_PUT_MAIL_ACCOUNT.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddMailAccount action end."));
        }
    }

}
