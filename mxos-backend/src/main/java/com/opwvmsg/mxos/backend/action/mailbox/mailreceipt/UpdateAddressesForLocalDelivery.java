/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update addresses for local delivery.
 *
 * @author mxos-dev
 */
public class UpdateAddressesForLocalDelivery implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateAddressesForLocalDelivery.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateAddressesForLocalDelivery action start."));
        }

        try {
            final String oldAddressesFLD = requestState.getInputParams()
                    .get(MailboxProperty.oldAddressForLocalDelivery.name()).get(0);
            final String newAddressesFLD = requestState.getInputParams()
                    .get(MailboxProperty.newAddressForLocalDelivery.name()).get(0);

            List<String> addressesFLDList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.addressesForLocalDelivery);

            if (addressesFLDList == null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_LOCAL_DELIVERY_ADDRESS
                                .name());
            }
            if (!addressesFLDList.contains(oldAddressesFLD.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError
                        .MBX_INVALID_OLD_LOCAL_DELIVERY_ADDRESS_NOT_EXIST
                                .name());
            }

            if (addressesFLDList.remove(oldAddressesFLD.toLowerCase())) {
                addressesFLDList.add(newAddressesFLD.toLowerCase());
                final String[] addressesFLDArray = addressesFLDList
                        .toArray(new String[addressesFLDList.size()]);

                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.addressesForLocalDelivery,
                                addressesFLDArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_LOCAL_DELIVERY_ADDRESS
                                .name());
            }

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update address.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_LOCAL_DELIVERY_ADDRESS_POST
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteSieveFiltersBlockedSenders action end."));
        }
    }
}
