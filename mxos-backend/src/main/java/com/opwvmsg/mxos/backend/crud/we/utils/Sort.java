package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.common.collect.Ordering;
import com.google.common.collect.TreeMultimap;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.we.WEAddressBookUtil;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;

public class Sort {

    enum SortOrder implements EnumConverter {
        ASC("ascending"), DESC("descending");

        private final String value;

        SortOrder(String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

    private TreeMultimap<String, Map<String, String>> map;
    private String sortKey;
    private SortOrder sortOrder;

    private static Logger logger = Logger.getLogger(Sort.class);

    public Sort(String sortKey, String sortOrder) throws AddressBookException {

        this.sortKey = sortKey;
        if (sortOrder != null) {
            this.sortOrder = new ReverseEnumMap<SortOrder>(SortOrder.class)
                    .get(sortOrder.toLowerCase());

            switch (this.sortOrder) {
            case ASC:
                map = TreeMultimap.create(Ordering.natural(),
                        Ordering.usingToString());
                break;
            case DESC:
                map = TreeMultimap.create(Ordering.natural().reverse(),
                        Ordering.usingToString());
            }
        }
    }

    public List<Map<String, String>> process(List<Map<String, String>> rowList)
            throws AddressBookException {
        if (sortKey == null || sortOrder == null) {
            // sorting not required
            return rowList;
        }
        String sortWEKey;
        try {
            sortWEKey = WEAddressBookUtil.JSONToWE_AddressBook.get(
                    AddressBookProperty.valueOf(sortKey)).name();
        } catch (IllegalArgumentException ile) {
            logger.error("Exception occurred : ", ile);
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_SORT_KEY.name(),
                    ExceptionUtils.INVALID_ATTRIBUTE_EXCEPTION_CATEGORY,
                    AddressBookError.ABS_INVALID_SORT_KEY.name(),"");
        }

        // populate the map with the correct key
        for (Map<String, String> row : rowList) {
            // get sortKey to use from WEAddressBookUtil
            map.put(row.get(sortWEKey), row);
        }
        return new ArrayList<Map<String, String>>(map.values());
    }
}
