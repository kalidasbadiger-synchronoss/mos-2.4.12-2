/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set add signature for new mails.
 *
 * @author mxos-dev
 */
public class SetAddSignatureForNewMails implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetAddSignatureForNewMails.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetAddSignatureForNewMails action start."));
        }
        String addSignatureForNewMails = requestState.getInputParams()
                .get(MailboxProperty.addSignatureForNewMails.name()).get(0);

        try {
            if (addSignatureForNewMails != null
                    && !addSignatureForNewMails.equals("")) {
                addSignatureForNewMails = Integer
                        .toString(MxosEnums.BooleanType.fromValue(
                                addSignatureForNewMails).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.addSignatureForNewMails,
                            addSignatureForNewMails);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set add signature for new mails.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_ADD_SIGNATURE_FOR_NEW_MAILS
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetAddSignatureForNewMails action end."));
        }
    }
}
