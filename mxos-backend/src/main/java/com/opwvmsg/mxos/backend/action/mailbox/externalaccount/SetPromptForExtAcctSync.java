/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.externalaccount;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
/**
 * Action class to set the promptForExternalAccountsSync attribute
 * of ExternalAccount.
 * 
 * @author mxos-dev
 */
public class SetPromptForExtAcctSync implements MxOSBaseAction{
    private static Logger logger = Logger.getLogger(SetPromptForExtAcctSync.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetPromptForExtAcctSync action start."));
        }
        
        
        if(requestState.getInputParams()
        		.get(MailboxProperty.promptForExternalAccountSync.name()) != null ){
        	final String extMailAcctAllowed = requestState.getInputParams()
        	.get(MailboxProperty.promptForExternalAccountSync.name()).get(0);

        	if (extMailAcctAllowed != null) {
        		if (extMailAcctAllowed.equalsIgnoreCase(MxosEnums.BooleanType.YES
        				.name())) {
        			MxOSApp.getInstance()
        			.getMailboxHelper()
        			.setAttribute(
        					requestState,
        					MailboxProperty.promptForExternalAccountSync,
        					Integer.toString(MxosEnums.BooleanType.YES
        							.ordinal()));
        		} else if (extMailAcctAllowed
        				.equalsIgnoreCase(MxosEnums.BooleanType.NO.name())) {
        			MxOSApp.getInstance()
        			.getMailboxHelper()
        			.setAttribute(
        					requestState,
        					MailboxProperty.promptForExternalAccountSync,
        					Integer.toString(MxosEnums.BooleanType.NO
        							.ordinal()));
        		} else if (extMailAcctAllowed.equals("")){
        			MxOSApp.getInstance()
        			.getMailboxHelper()
        			.setAttribute(
        					requestState,
        					MailboxProperty.promptForExternalAccountSync, "");
        		} else {
        			throw new InvalidRequestException(
        					MailboxError.MBX_INVALID_PROMPT_FOR_EXTERNAL_SYNC.name());
        		}
        	}
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetPromptForExtAcctSync action end."));
        }
    }
}
