/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete Cos.
 * 
 * @author Aricent
 */
public class DeleteCos implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteCos.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteCos action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {

            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String cosId = requestState.getInputParams()
                    .get(MailboxProperty.cosId.name()).get(0);
            // Get the cos before delete
            mailboxCRUD.readCosBase(cosId, requestState);
            // Delete Cos from ldap.
            mailboxCRUD.deleteCos(cosId);

            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while cos delete.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(
                    CosError.COS_UNABLE_TO_DELETE.name(), e);
        } finally {
            try {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteCos action end."));
        }
    }

}
