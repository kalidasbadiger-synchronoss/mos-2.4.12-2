/*
/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.personalinfo.events;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to delete contacts personalInfo events.
 * 
 * @author mxos-dev
 */
public class DeleteContactsPersonalInfoEvents implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteContactsPersonalInfoEvents.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteContactsPersonalInfoEvents action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();
            String date = null;
            String backend = MxOSConfig.getAddressBookBackend();

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.birthDate.name()) != null) {

                date = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.birthDate.name()).get(0);
                if (backend
                        .equalsIgnoreCase(MxOSConfig.DEFAULT_ADDRESS_BOOK_BACKEND)) {

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthDay, date);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthMonth, date);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthYear, date);

                } 
                
                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.ox.name())) {
                    // OX Mapping
                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthDate, date);
                }
            }

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.anniversaryDate.name()) != null) {

                date = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.anniversaryDate.name()).get(0);

                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.we.name())) {
                    // WE Mapping

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryDay, date);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryMonth, date);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryYear, date);

                }

                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.ox.name())) {
                    // OX Mapping
                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryDate, date);

                }

            }

            addressBookCRUD.updateContactsPersonalInfoEvent(mxosRequestState);

        } catch (AddressBookException e) {
            logger.error("Error while deleting contacts personal info events.",
                    e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_PERSONALINFO_EVENTS_UNABLE_TO_DELETE,
                    e);
        } catch (final Exception e) {
            logger.error("Error while deleting contacts personal info events.",
                    e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "DeleteContactsPersonalInfoEvents action end."));
        }
    }
}
