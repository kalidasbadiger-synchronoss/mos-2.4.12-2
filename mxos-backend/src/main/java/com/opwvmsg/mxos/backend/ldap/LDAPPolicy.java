/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

/**
 * Class to set LDAP policy
 * 
 * @author mxos-dev
 * 
 */
public class LDAPPolicy {
    // default value
    public static final int DEFAULT_MAX_RETRY_COUNT = 3;
    public static final int DEFAULT_RETRY_INTERVAL = 30 * 1000;
    public static final int DEFAULT_FAILBACK_RETRY_INTERVAL = 30 * 1000;

    // specifics if retry this operation if current operation fails
    // when retryWhenOperationFailed is true, retry mechanism is enabled
    // when the failed exception is matched in retryExceptions
    // start to retry the operation based on maxRetryCount and retryInterval
    private boolean retryWhenOperationFailed = false;
    private List<String> retryExceptions = new ArrayList<String>();
    private int maxRetryCount = DEFAULT_MAX_RETRY_COUNT;
    private int retryInterval = DEFAULT_RETRY_INTERVAL;

    // specifics if try to connection other servers when current LDAP server is
    // down
    private boolean failover = false;

    // specifics if back to original LDAP server after failover
    // if true, try back to original LDAP server per failbackRetryInterval time
    // this feature is enabled when failover is true
    private boolean failback = false;
    private int failbackRetryInterval = DEFAULT_FAILBACK_RETRY_INTERVAL;

    public boolean isRetryWhenOperationFailed() {
        return retryWhenOperationFailed;
    }

    public LDAPPolicy setRetryPolicy(List<String> retryExceptions,
            int maxRetryCount, int retryInterval) {
        this.retryWhenOperationFailed = true;
        this.retryExceptions = retryExceptions;
        this.maxRetryCount = maxRetryCount;
        this.retryInterval = retryInterval;
        return this;
    }

    public List<String> getRetryExceptions() {
        return retryExceptions;
    }

    public boolean needToRetry(NamingException e) {
        return true;
    }

    public int getMaxRetryCount() {
        return maxRetryCount;
    }

    public int getRetryInterval() {
        return retryInterval;
    }

    public boolean isFailover() {
        return failover;
    }

    public LDAPPolicy setFailover(boolean failover) {
        this.failover = failover;
        return this;
    }

    public boolean isFailback() {
        return failback;
    }

    public LDAPPolicy setFailbackPolicy(int failbackRetryInterval) {
        this.failback = true;
        this.failbackRetryInterval = failbackRetryInterval;
        return this;
    }

    public int getFailbackRetryInterval() {
        return failbackRetryInterval;
    }

}
