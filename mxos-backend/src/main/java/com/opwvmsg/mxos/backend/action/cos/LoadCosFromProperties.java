/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to load all the COS default properties from
 * default-cos.properties while creating a cos.
 *
 * @author mxos-dev
 */
public class LoadCosFromProperties implements MxOSBaseAction {
    private static final Logger logger = Logger
            .getLogger(LoadCosFromProperties.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "LoadCosFromProperties action start."));
        }
        // Load properties from ${MXOS_HOME}/config/default-cos.properties
        try {
            String home = System.getProperty(MxOSConstants.MXOS_HOME);
            Properties config = null;
            InputStream in = null;
            try {
                in = new FileInputStream(home
                        + "/config/default-cos.properties");
                config = new Properties();
                try {
                    config.load(in);
                } catch (IOException e) {
                    logger.warn("Error while load properties.");
                    throw new MxOSException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            } catch (FileNotFoundException e) {
                logger.warn("properties file not found.");
                throw new MxOSException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                IOUtils.closeQuietly(in);
            }
            // get helper
            IMailboxHelper helper = MxOSApp.getInstance()
                    .getMailboxHelper();
            Iterator<Object> keys = config.keySet().iterator();
            // for each property
            while (keys.hasNext()) {
                // set key and property value in helper
                final String key = (String) keys.next();
                helper.setAttribute(requestState, key,
                        config.getProperty(key));
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while load cos from properties.", e);
            throw new ApplicationException(
                    CosError.COS_UNABLE_TO_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "LoadCosFromProperties action end."));
        }
    }
}
