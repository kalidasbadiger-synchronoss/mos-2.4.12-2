/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:  $
 */

package com.opwvmsg.mxos.backend.crud.mss;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.message.MessageServiceHelper;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.SortOrder;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.message.pojos.Folder.FolderSubscribed;
import com.opwvmsg.mxos.message.pojos.Header;
import com.opwvmsg.mxos.message.pojos.Message;
import com.opwvmsg.mxos.message.pojos.Metadata;
import com.opwvmsg.mxos.message.search.SearchTerm;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.intermail.log.IntermailException;
import com.opwvmsg.utils.paf.intermail.mail.AccessInfo;
import com.opwvmsg.utils.paf.intermail.mail.AttributeId;
import com.opwvmsg.utils.paf.intermail.mail.FolderInfo;
import com.opwvmsg.utils.paf.intermail.mail.Mailbox;
import com.opwvmsg.utils.paf.intermail.mail.MailboxInfo;
import com.opwvmsg.utils.paf.intermail.mail.MessageMetadata;
import com.opwvmsg.utils.paf.intermail.mail.Msg;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlagChangeInfo;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlags;
import com.opwvmsg.utils.paf.intermail.mail.MsgFlags.MsgFlagIndex;
import com.opwvmsg.utils.paf.intermail.mail.PopListInfo;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;
import com.opwvmsg.utils.paf.intermail.mail.ops.CopyMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.CreateMsg;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.DeleteMailbox;
import com.opwvmsg.utils.paf.intermail.mail.ops.GetMessageAttributes;
import com.opwvmsg.utils.paf.intermail.mail.ops.GetMsgStream;
import com.opwvmsg.utils.paf.intermail.mail.ops.ImapListUids;
import com.opwvmsg.utils.paf.intermail.mail.ops.MailboxAccessInfo;
import com.opwvmsg.utils.paf.intermail.mail.ops.MoveMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.MsLock;
import com.opwvmsg.utils.paf.intermail.mail.ops.PopList;
import com.opwvmsg.utils.paf.intermail.mail.ops.ReadMailboxInfo;
import com.opwvmsg.utils.paf.intermail.mail.ops.ReadSortedMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.RemoveMultiMsgs;
import com.opwvmsg.utils.paf.intermail.mail.ops.RenameFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.StoreMsgFlags;
import com.opwvmsg.utils.paf.intermail.mail.ops.SubscribeFolder;
import com.opwvmsg.utils.paf.intermail.mail.ops.UpdateMailboxAccessInfo;
import com.opwvmsg.utils.paf.intermail.mail.ops.UpdateMsgBlobStream;
import com.opwvmsg.utils.paf.intermail.replystore.Reply;
import com.opwvmsg.utils.paf.intermail.replystore.ops.UpdateReply;

/**
 * Mss Stateless CRUD APIs to access metadata.
 * 
 * @author mxos-dev
 */
public class MssSlMetaCRUD extends MssMetaCRUD {

    private static Logger logger = Logger.getLogger(MssSlMetaCRUD.class);

    private static EnumMap<MessageProperty, Short> READSORTEDMSGS_KEY_MAP = new EnumMap<MessageProperty, Short>(
            MessageProperty.class);
    static {
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.messageId,
                ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.flagBounce,
                ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_BOUNCE);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.deliverNDR,
                ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_DELIVEREDNDR);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.isPrivate,
                ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_PRIVATE);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.hasAttachments,
                ReadSortedMsgs.SORTED_BASED_ON_ATTACHMENTS);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.richMailFlag,
                ReadSortedMsgs.SORTED_BASED_ON_RICHMAIL_FLAG);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.arrivalTime,
                ReadSortedMsgs.SORTED_BASED_ON_ARRIVAL_TIME);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.expireTime,
                ReadSortedMsgs.SORTED_BASED_ON_EXPIRATIONTIME);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.firstSeenTime,
                ReadSortedMsgs.SORTED_BASED_ON_FIRSTSEENTIME);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.firstAccessedTime,
                ReadSortedMsgs.SORTED_BASED_ON_FIRSTACCESSTIME);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.lastAccessedTime,
                ReadSortedMsgs.SORTED_BASED_ON_LASTACCESSTIME);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.size,
                ReadSortedMsgs.SORTED_BASED_ON_SIZE);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.uid,
                ReadSortedMsgs.SORTED_BASED_ON_UID);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.flags,
                ReadSortedMsgs.SORTED_BASED_ON_MSGFLAGS);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.keywords,
                ReadSortedMsgs.SORTED_BASED_ON_KEYWORDS);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.type,
                ReadSortedMsgs.SORTED_BASED_ON_TYPE);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.priority,
                ReadSortedMsgs.SORTED_BASED_ON_PRIORITY);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.subject,
                ReadSortedMsgs.SORTED_BASED_ON_SUBJECT);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.from,
                ReadSortedMsgs.SORTED_BASED_ON_FROM_ADDRESS);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.to,
                ReadSortedMsgs.SORTED_BASED_ON_TO_FIELD);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.cc,
                ReadSortedMsgs.SORTED_BASED_ON_CC_FIELD);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.sentDate,
                ReadSortedMsgs.SORTED_BASED_ON_DATE);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.threadId,
                ReadSortedMsgs.SORTED_BASED_ON_CONVERSATION_HISTORY);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.sender,
                ReadSortedMsgs.SORTED_BASED_ON_SENDER);
        READSORTEDMSGS_KEY_MAP.put(MessageProperty.bcc,
                ReadSortedMsgs.SORTED_BASED_ON_BCC);
    }

    /**
     * Constructor.
     * 
     * @param javaMailProps javaMailProps
     * @param systemFolders systemFolders
     * @throws Exception Exception.
     */
    public MssSlMetaCRUD() {
        super();
        myDataModel = RmeDataModel.Leopard;
    }

    @Override
    public void createMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {
        logger.info("Creating Mailbox in MSS");
        try {
            // check for msslinkinfo
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost;
            String mailboxId;
            String realm;
            if (info != null) {
                mssHost = getMssHost(info.getMessageStoreHosts());
                mailboxId = info.getMailboxId();
                realm = info.getRealm();
            } else {
                mssHost = mxosRequestState.getAdditionalParams().getProperty(
                        MailboxProperty.messageStoreHost.name());
                mailboxId = mxosRequestState.getAdditionalParams().getProperty(
                        MailboxProperty.mailboxId);
                realm = mxosRequestState.getAdditionalParams().getProperty(
                        MailboxProperty.realm);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            CreateMailbox cmbox = new CreateMailbox(mssHost, mailboxId, realm,
                    null, -1, 0);
            cmbox.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("mss CreateMailBox method : creation is success");
            }
        } catch (IntermailException e) {
            logger.warn("Error while Create mailbox: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString() != null
                    && e.getFormattedString().contains("MsAlreadyExists")) {
                throw new MxOSException(MailboxError.MBX_ALREADY_EXISTS.name(),
                        e);
            } else {
                throw new MxOSException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            }
        } catch (Exception e) {
            logger.error("Error while create mailbox.", e);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while create mailbox.", t);
            throw new MxOSException(MailboxError.MBX_UNABLE_TO_CREATE.name(), t);
        }
    }

    @Override
    public void updateMailbox(MxOSRequestState mxosRequestState)
            throws MxOSException {

        if (mxosRequestState != null) {
            Map<String, List<String>> inputParams = mxosRequestState
                    .getInputParams();
            // Set the auto reply if auto reply request param exists
            if (inputParams
                    .containsKey(MailboxProperty.autoReplyMessage.name())) {
                try {
                    setAutoReplyText(
                            mxosRequestState,
                            inputParams.get(
                                    MailboxProperty.autoReplyMessage.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error(
                            "Error while setting auto reply message(MSS).", e);
                }
            }
            // Set the filter for the account if filter request param exists
            if (inputParams.containsKey(MailboxProperty.mtaFilter.name())) {
                try {
                    setMTAFilter(mxosRequestState,
                            inputParams.get(MailboxProperty.mtaFilter.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting MTA filter(MSS).", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                                    .name(), e);
                }
            }
            // Set the filter for the account if filter request param exists
            if (inputParams.containsKey(MailboxProperty.rmFilter.name())) {
                try {
                    setRMFilter(mxosRequestState,
                            inputParams.get(MailboxProperty.rmFilter.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting RM filter(MSS).", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                                    .name(), e);
                }
            }
            // Set the signature for the account if signature exists
            if (inputParams.containsKey(MailboxProperty.signature.name())) {
                try {
                    setSignature(mxosRequestState,
                            inputParams.get(MailboxProperty.signature.name())
                                    .get(0));
                } catch (Exception e) {
                    logger.error("Error while setting signature.", e);
                    throw new InvalidRequestException(
                            MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
                }
            }
            
            // Find and set mailboxAccessInfo
            updateAccessInfo(mxosRequestState);

            // Change accessControlStatus if specified
            if (inputParams.containsKey(MailboxProperty.accessControlStatus.name())) {
                setAccessControlStatus(mxosRequestState);
            }
        }
    }

    /**
     * Find properties for updateMailboxAccessInfo in the request parameters and
     * execute the update operation in MSS.
     * 
     * @param mxosRequestState mOS request parameters
     * @throws MxOSException
     */
    protected void updateAccessInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        
        Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();
        
        // Find properties for updateMailboxAccessInfo and setup if any
        AccessInfo accessInfo = new AccessInfo();
        
        String value = null;
        List<String> properties = new ArrayList<String>();
        MailboxProperty property = null;
        MailboxError error = null;
        
        final String format = System.getProperty(SystemProperty.userDateFormat.name()); 

        try {
            // failedLoginAttempts
            property = MailboxProperty.failedLoginAttempts;
            error = MailboxError.MBX_UNABLE_TO_SET_FAILED_LOGIN_ATTEMPTS;
            value = CRUDUtils.getSingleParameter(inputParams, property);
            if (value != null) {
                properties.add(property.name());
                accessInfo.setNumFailedLoginAttempts(Long.valueOf(value));
            }

            // lastLoginAttemptDate
            property = MailboxProperty.lastLoginAttemptDate;
            error = MailboxError.MBX_UNABLE_TO_SET_LAST_LOGIN_ATTEMPTS_DATE;
            value = CRUDUtils.getSingleParameter(inputParams, property);
            if (value != null) {
                properties.add(property.name());
                Date date = new SimpleDateFormat(format).parse(value);
                accessInfo.setLastLoginTime(date.getTime());
            }

            // lastSuccessfulLoginDate
            property = MailboxProperty.lastSuccessfulLoginDate;
            error = MailboxError.MBX_UNABLE_TO_SET_LAST_SUCCESSFUL_LOGIN_DATE;
            value = CRUDUtils.getSingleParameter(inputParams, property);
            if (value != null) {
                properties.add(property.name());
                Date date = new SimpleDateFormat(format).parse(value);
                accessInfo.setLastSuccessfulLoginTime(date.getTime());
            }

            // lastFailedLoginDate
            property = MailboxProperty.lastFailedLoginDate;
            error = MailboxError.MBX_UNABLE_TO_SET_FAILED_LOGIN_ATTEMPTS_DATE;
            value = CRUDUtils.getSingleParameter(inputParams, property);
            if (value != null) {
                properties.add(property.name());
                Date date = new SimpleDateFormat(format).parse(value);
                accessInfo.setLastFailedLoginTime(date.getTime());
            }
        } catch (ParseException e) {
            logger.warn("Invalid date format in " + property.name() + ": "
                    + value);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(error.name(), e);
        } catch (NumberFormatException e) {
            logger.warn("Invalid numeric format in " + property.name() + ": "
                    + value);
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(error.name(), e);
        }

        // Execute the MSS call if any property is set  
        if (!properties.isEmpty()) {
            
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ").append(realm));
            }

            String protocolType = CRUDUtils.getSingleParameter(
                    inputParams, MailboxProperty.protocolType);
            // Putting a null may cause coredump in mss.
            if (protocolType == null) {
                protocolType = "";
            } else {
                protocolType = protocolType.trim();
            }
            
            try {
                UpdateMailboxAccessInfo umai = new UpdateMailboxAccessInfo(
                        mssHost, mailboxId, realm, accessInfo, protocolType);
                umai.execute(myDataModel);
            } catch (IntermailException e) {
                logger.warn("Error while updating mailbox access info ("
                        + properties + "), error = " + e.getFormattedString());
                ConnectionErrorStats.MSS.increment();
                throw new MxOSException(error.name(), e);
            }
        }
    }

    /**
     * Updates access control status.
     * 
     * @param mxosRequestState Parameters map.
     * @throws MxOSException
     */
    protected void setAccessControlStatus(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            
            Map<String, List<String>> inputParams = mxosRequestState.getInputParams();
            
            if (!inputParams.containsKey(MailboxProperty.accessControlStatus.name())) {
                throw new MxOSException(ErrorCode.GEN_BAD_REQUEST.name(),
                        "parameter "
                                + MailboxProperty.accessControlStatus.name()
                                + " is missing.");
            }
            
            final String requestState = inputParams.get(
                    MailboxProperty.accessControlStatus.name()).get(0);
            final String command = MsLock.resolveCommand(requestState);
            if (command == null) {
                List<String> values = new ArrayList<String>();
                for (MxosEnums.AccessControlStatus status : MxosEnums.AccessControlStatus
                        .values()) {
                    values.add(status.toString());
                }
                throw new MxOSException(
                        MailboxError.MBX_INVALID_ACCESS_CONTROL_STATUS.name(),
                        new StringBuilder(
                                MailboxProperty.accessControlStatus.name())
                                .append(" should be one of ")
                                .append(values.toString()).toString());
            }
            
            MsLock msLock = new MsLock(mssHost, mailboxId, realm, command,
                    inputParams);
            msLock.execute(myDataModel);
                    
        } catch (IntermailException e) {
            logger.warn("Error while updating access control status: "
                    + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            MailboxError error;
            if (e.getFormattedString()
                    .startsWith("MsMessageStoreLocked(74/87)")) {
                error = MailboxError.MBX_MESSAGE_STORE_LOCKED;
            } else {
                error = MailboxError.MBX_UNABLE_TO_UPDATE_ACCESS_CONTROL_STATUS;
            }
            throw new MxOSException(error.name(), e);
        }
    }

    /**
     * This method retrieves AccessInfo properties from MSS and put them to
     * mxosRequestState. Optionally, the method also would populate the
     * retrieved properties to a Credential object when it is given via
     * mxosRequestState. We assume that a Credentials object is always passed
     * from a previous action when the operation requires the credentials
     * information, so that we can skip unnecessary RME calls.
     * 
     * @param mxosRequestState the execution context
     * 
     * @return Mailbox AccessInfo object for mailbox id
     * 
     * @throws Exception Throws Exception.
     */
    @Override
    public void readMailBoxAccessInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            
            Credentials credentials = (Credentials) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.credentials);
            
            // call MSS_SL_MSLOCK to retrieve access control status
            Boolean isSoftDeleted = null;
            if (credentials != null) {
                MsLock msLock = new MsLock(mssHost, mailboxId, realm,
                        MsLock.COMMAND_QUERY, mxosRequestState.getInputParams());
                msLock.execute(myDataModel);
                String accessControlStatus = msLock.getLockStatus();
                if (accessControlStatus != null) {
                    MxosEnums.AccessControlStatus acs = MxosEnums.AccessControlStatus
                            .fromValue(accessControlStatus);
                    credentials.setAccessControlStatus(acs);
                }
                isSoftDeleted = msLock.getMsSoftDeleted();
                if (isSoftDeleted != null) {
                    credentials.setSoftDeleted(isSoftDeleted);
                }
            }
            
            // call MSS_SL_MAILBOXACCESSINFO to retrieve access info
            MailboxAccessInfo mailboxAccessInfo = new MailboxAccessInfo(
                    mssHost, mailboxId, realm);
            AccessInfo accessInfo = null;
            
            if ((credentials == null || credentials.getAccessControlStatus() != MxosEnums.AccessControlStatus.LOCKED)
                    && (isSoftDeleted == null || !isSoftDeleted)) {
                try {
                    mailboxAccessInfo.execute(myDataModel);
                    accessInfo = mailboxAccessInfo.getAccessInfo();
                } catch (IntermailException e) {
                    boolean ignore = false;
                    if (credentials != null
                            && e.getFormattedString().startsWith(
                                    "MsNotFound(74/8)")) {
                        // check input parameter and ignore this error if
                        // ignoreSoftDelete is set
                        Map<String, List<String>> inputParams = mxosRequestState
                                .getInputParams();
                        final String key = MailboxProperty.ignoreSoftDelete.name();
                        if (inputParams.containsKey(key)) {
                            ignore = Boolean.parseBoolean(inputParams.get(key)
                                    .get(0));
                            accessInfo = null;
                            credentials.setSoftDeleted(Boolean.TRUE);
                        }
                    }
                    if (!ignore) {
                        throw e;
                    }
                    logger.info("MS is not found but continue in respect of ignoreSoftDelete option");
                }
            }
            
            if (credentials != null && accessInfo != null) {
                credentials.setFailedLoginAttempts((int) accessInfo
                        .getNumFailedLoginAttempts());

                credentials.setLastFailedLoginDate(new SimpleDateFormat(System
                        .getProperty(SystemProperty.userDateFormat.name()))
                        .format(new Date(accessInfo.getLastFailedLoginTime())));

                credentials
                        .setLastSuccessfulLoginDate(new SimpleDateFormat(System
                                .getProperty(SystemProperty.userDateFormat
                                        .name())).format(new Date(accessInfo
                                .getLastSuccessfulLoginTime())));

                credentials.setLastLoginAttemptDate(new SimpleDateFormat(System
                        .getProperty(SystemProperty.userDateFormat.name()))
                        .format(new Date(accessInfo.getLastLoginTime())));

                if (accessInfo.getPopLastLoginTime() > 0) {
                    credentials
                            .setPopLastLoginTime(new SimpleDateFormat(System
                                    .getProperty(SystemProperty.userDateFormat
                                            .name())).format(new Date(
                                    accessInfo.getPopLastLoginTime())));
                }

                if (accessInfo.getImapLastLoginTime() > 0) {
                    credentials.setImapLastLoginTime(new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(new Date(accessInfo
                            .getImapLastLoginTime())));
                }

                if (accessInfo.getSmtpLastLoginTime() > 0) {
                    credentials.setSmtpLastLoginTime(new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(new Date(accessInfo
                            .getSmtpLastLoginTime())));
                }
                
                if (accessInfo.getWebmailLastLoginTime() > 0) {
                    credentials.setWebmailLastLoginTime(new SimpleDateFormat(
                            System.getProperty(SystemProperty.userDateFormat
                                    .name())).format(new Date(accessInfo
                            .getWebmailLastLoginTime())));
                }
                
                mxosRequestState.getDbPojoMap().setProperty(
                        MxOSPOJOs.credentials, credentials);
            }
            mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.accessInfo,
                    mailboxAccessInfo.getAccessInfo());
        } catch (IntermailException e) {
            logger.warn("Error while reading MailBox Access Info: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MailboxError.MBX_UNABLE_TO_GET_MAILBOX_ACCESS_INFO.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading MailBox Access Info", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_MAILBOX_ACCESS_INFO.name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while reading MailBox Access Info", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_GET_MAILBOX_ACCESS_INFO.name(),
                    t);
        }
    }
    

    /**
     * Returns Last Login Time.
     * 
     * @param mxosRequestState MxOSRequestState object
     * @return long(last login Time)
     * @throws MxOSException
     */
    @Override
    public long getLastLoginTime(MxOSRequestState mxosRequestState)
            throws MxOSException {
        long lastLoginTime = -1;
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }
        String ignorePopDeleted = null;
        if (mxosRequestState.getInputParams().containsKey(
                FolderProperty.ignorePopDeleted.name())) {
            ignorePopDeleted = mxosRequestState.getInputParams()
                    .get(FolderProperty.ignorePopDeleted.name()).get(0);
        }
        ReadMailboxInfo readMailboxInfo = new ReadMailboxInfo(mssHost,
                mailboxId, realm, accessId,
                Boolean.parseBoolean(ignorePopDeleted), true);

        try {
            readMailboxInfo.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while getting Mailbox: "
                    + e.getFormattedString());
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        }

        MailboxInfo[] mailboxInfo = readMailboxInfo.getMailboxInfo();
        if (mailboxInfo != null) {
            for (MailboxInfo mi : mailboxInfo) {
                if (mi.getLastAccessDate() != -1){
                    lastLoginTime = mi.getLastAccessDate();
                    break;
                }
            }
        }
        return lastLoginTime;
    }

    /**
     * Updates the mailbox AccessInfo on the MSS.
     * 
     * @return Mailbox AccessInfo object for mailbox id
     * @throws Exception Throws Exception.
     */
    @Override
    public void updateMailBoxAccessInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String mssHost = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(
                        mssHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            AccessInfo accessInfo = (AccessInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.accessInfo);
            
            String protocolType = "";
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.protocolType.name())) {
                protocolType = mxosRequestState
                        .getInputParams()
                        .get(MailboxProperty.protocolType.name())
                        .get(0).trim();
            }
            
            UpdateMailboxAccessInfo updateMailboxAccessInfo = new UpdateMailboxAccessInfo(
                    mssHost, mailboxId, realm, accessInfo, protocolType);
            updateMailboxAccessInfo.execute(myDataModel);

        } catch (IntermailException e) {
            logger.warn("Error while updating MailBox Access Info: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_UPDATE_MAILBOX_ACCESS_INFO
                            .name(),
                    e);
        } catch (Exception e) {
            logger.error("Error while updating MailBox Access Info", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_UPDATE_MAILBOX_ACCESS_INFO
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while updating MailBox Access Info", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_UPDATE_MAILBOX_ACCESS_INFO
                            .name(),
                    t);
        }
    }

    @Override
    public String createFolder(final MxOSRequestState mxosRequestState,
            String folderName) throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        // INBOX shall be created during mailbox creation in MSS and it cannot
        // be deleted. Also INBOX cannot be case sensitive. Hence it is not
        // allowed to be created.
        if ("".equals(folderName)
                || folderName.equals(MxOSConstants.FORWARD_SLASH)
                || MxOSConstants.INBOX.equalsIgnoreCase(folderName)
                || systemfolders.contains(folderName)) {
            logger.warn(folderName + " is not allowed to create");
            throw new InvalidRequestException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), folderName
                            + " is not allowed for creation");
        }
        long UIDValidity = -1;
        if (mxosRequestState.getInputParams().get(
                FolderProperty.uidValidity.name()) != null) {
            UIDValidity = Long.parseLong(mxosRequestState.getInputParams()
                    .get(FolderProperty.uidValidity.name()).get(0));
        }
        try {
            if (ActionUtils.getFolderUUID(mxosRequestState, folderName) != null) {
                logger.warn("Folder already exists");
                throw new InvalidRequestException(
                        FolderError.FLD_ALREADY_EXISTS.name());
            }
        } catch (NotFoundException nfe) {
            // Continue to create folder
        }
        UUID parentFolderUUId = ActionUtils.getParentFolderUUID(
                mxosRequestState, folderName);
        // Remove first char slash if exists
        if (folderName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            folderName = folderName.substring(1);
        }
        if ("".equals(folderName)
                || folderName.equals(MxOSConstants.FORWARD_SLASH)
                || MxOSConstants.INBOX.equalsIgnoreCase(folderName)) {
            logger.warn(folderName + " is not allowed to create");
            throw new InvalidRequestException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), folderName
                            + " is not allowed for creation");
        }
        UUID currentParentUUID = parentFolderUUId;
        UUID folderUUID = null;
        if (folderName.contains(MxOSConstants.FORWARD_SLASH)) {
            String[] folders = folderName.split(MxOSConstants.FORWARD_SLASH);
            if (folders.length == 0) {
                logger.warn(folderName + " is not allowed to create");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(),
                        folderName + " is not allowed for creation");
            }
            StringBuilder folder = new StringBuilder();
            for (int i = 0; i < folders.length; i++) {
                String element = folders[i];
                try {
                    if (i > 0) {
                        folder.append(MxOSConstants.FORWARD_SLASH);
                    }
                    if (i == 0 && MxOSConstants.INBOX.equalsIgnoreCase(element)) {
                        folder.append(MxOSConstants.INBOX);
                    }
                    else {
                        folder.append(element);
                    }
                    if ("".equals(element)) {
                        logger.warn(folderName
                                + ": empty folder name is not allowed");
                        throw new InvalidRequestException(
                                FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(),
                                folders[i] + ": empty folder name is not allowed");
                    }
                    currentParentUUID = ActionUtils.getFolderUUID(
                            mxosRequestState, folder.toString());
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer("Folder ").append(folder)
                                .append(" already exists and UUID is:")
                                .append(currentParentUUID));
                    }
                } catch (NotFoundException nfe) {
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer("Folder ").append(folder)
                                .append(" not found"));
                    }
                    folderUUID = createFolder(mssHost, mailboxId, realm,
                            currentParentUUID, element, UIDValidity);
                    currentParentUUID = folderUUID;
                }
            }
            if (null != folderUUID) {
                mxosRequestState.getDbPojoMap().setProperty(
                        FolderProperty.folderId, folderUUID.toString());
            }
        } else {
            folderUUID = createFolder(mssHost, mailboxId, realm,
                    parentFolderUUId, folderName, UIDValidity);
            if (null != folderUUID) {
                mxosRequestState.getDbPojoMap().setProperty(
                        FolderProperty.folderId, folderUUID.toString());
            }
        }
        return folderUUID.toString();
    }

    /**
     * 
     * @param mssHost mssHost
     * @param mailboxId mailboxId
     * @param parentFolderUUID parentFolderUUID
     * @param folderName folderName
     * @param uidValidity uidValidity
     * @return folderUUID folderUUID
     * @throws MxOSException
     */
    private UUID createFolder(String mssHost, String mailboxId, String realm,
            UUID parentFolderUUID, String folderName, long uidValidity)
            throws MxOSException {
        UUID folderUUID = null;
        CreateFolder createFolder = null;
        try {
            if (parentFolderUUID != null && !"".equals(folderName)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Creating folder: " + folderName);
                }
                createFolder = new CreateFolder(mssHost, mailboxId, realm,
                        parentFolderUUID, folderName, uidValidity);
            } else {
                logger.warn(folderName + " is not allowed to create");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(),
                        folderName + " is not allowed for creation");
            }
            createFolder.execute(myDataModel);
            folderUUID = createFolder.getFolderUUID();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Successfully created the folder: ").append(folderName)
                        .append(" and the UUID is: ")
                        .append(folderUUID.toString()));
            }
        } catch (IntermailException e) {
            logger.warn("Error while creating folder: " + e.getFormattedString());
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Exception e) {
            logger.error("Error while set create folder.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while set create folder.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_CREATE.name(), t);
        }
        return folderUUID;
    }

    @Override
    public void readMailboxMetaInfo(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String mssHost = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        int accessId = getAccessId(info.getIsAdmin());
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(mssHost));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
            logger.debug(new StringBuffer().append("AccessId : ").append(
                    accessId));
        }
        String ignorePopDeleted = null;
        if (mxosRequestState.getInputParams().containsKey(
                FolderProperty.ignorePopDeleted.name())) {
            ignorePopDeleted = mxosRequestState.getInputParams()
                    .get(FolderProperty.ignorePopDeleted.name()).get(0);
        }
        ReadMailboxInfo readMailboxInfo = new ReadMailboxInfo(mssHost,
                mailboxId, realm, accessId,
                Boolean.parseBoolean(ignorePopDeleted), true);
        try {
            readMailboxInfo.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while getting Mailbox: " + e.getFormattedString());
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        }
        mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.mailboxInfo,
                readMailboxInfo.getMailboxInfo());
        calculateFolderPaths(readMailboxInfo.getFolderInfo());
        mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.folderInfo,
                readMailboxInfo.getFolderInfo());
    }

    @Override
    public String createMessage(MxOSRequestState mxosRequestState,
            boolean messageRecent) throws MxOSException {

        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

        String host = null;
        if (mxosRequestState.getInputParams().containsKey(
                MailboxProperty.messageStoreHost.name())) {
            host = mxosRequestState.getInputParams()
                    .get(MailboxProperty.messageStoreHost.name()).get(0);
        } else {
            host = getMssHost(info.getMessageStoreHosts());
        }
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }

        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        
        final String text = mxosRequestState.getInputParams()
                .get(MxOSPOJOs.message.name()).get(0);
        
        final String fromAddress = mxosRequestState.getInputParams()
                .get(MxOSPOJOs.receivedFrom.name()).get(0);

        String isPrivate = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.isPrivate.name())) {
            isPrivate = mxosRequestState.getInputParams()
                    .get(MessageProperty.isPrivate.name()).get(0);
        } else {
            isPrivate = System
                    .getProperty(SystemProperty.createMessageIsPrivateMsg
                            .name());
        }
        
        int options = 0;
        if (System.getProperty(SystemProperty.createMessageSLRMEOptions
                .name()) != null) {
            options = Integer.parseInt(System
                    .getProperty(SystemProperty.createMessageSLRMEOptions
                            .name()));
        }

        Flags flags = (Flags) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.messageFlags);
        final String strFlags = getFlagsString(flags);

        String keywords = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.keywords.name())) {
            keywords = mxosRequestState.getInputParams()
                    .get(MessageProperty.keywords.name()).get(0);
        }

        String oldMsgId = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.oldMsgId.name())) {
            oldMsgId = mxosRequestState.getInputParams()
                    .get(MessageProperty.oldMsgId.name()).get(0);
        }

        Integer uid = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.uid.name())) {
            uid = Integer.parseInt(mxosRequestState.getInputParams()
                    .get(MessageProperty.uid.name()).get(0));
        }

        String ignoreQuotaFlag = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.ignoreQuotaFlag.name())) {
            ignoreQuotaFlag = mxosRequestState.getInputParams()
                    .get(MessageProperty.ignoreQuotaFlag.name()).get(0);
            if (Boolean.parseBoolean(ignoreQuotaFlag)) {
                options = options + CreateMsg.SL_CREATE_MSG_IGNORE_QUOTA;
            }
        }
        
        String popDeletedFlag = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.popDeletedFlag.name())) {
            popDeletedFlag = mxosRequestState.getInputParams()
                    .get(MessageProperty.popDeletedFlag.name()).get(0);
            if (Boolean.parseBoolean(popDeletedFlag)) {
                options = options + CreateMsg.SL_CREATE_MSG_SPECIAL_DELETE_FLAG;
            }
        }

        String disableNotificationFlag = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.disableNotificationFlag.name())) {
            disableNotificationFlag = mxosRequestState.getInputParams()
                    .get(MessageProperty.disableNotificationFlag.name()).get(0);

        } else {
            disableNotificationFlag = System.getProperty(
                    SystemProperty.disableNotificationFlag.name(), "true");
        }

        if (Boolean.parseBoolean(disableNotificationFlag)) {
            options = options + CreateMsg.SL_CREATE_MSG_DISABLE_NOTIFICATION;
        }

        String msgBase64Encoded = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.msgBase64Encoded.name())) {
            msgBase64Encoded = mxosRequestState.getInputParams()
                    .get(MessageProperty.msgBase64Encoded.name()).get(0);
        }
        byte[] messageArr = null;
        if (Boolean.parseBoolean(msgBase64Encoded)) {
            Base64 base64 = new Base64();
            messageArr = base64.decode(text.getBytes());
        } else {
            messageArr = text.getBytes();
        }
        
        Map<String, List<String>> inputParams = mxosRequestState
                .getInputParams();

        long arrivalTime = 0;
        final String arrivalName = MessageProperty.arrivalTime.name();
        if (inputParams.containsKey(arrivalName)) {
            arrivalTime = Long.parseLong(inputParams.get(arrivalName).get(0));
            logger.info(new StringBuffer("arrivalTime=").append(arrivalTime));
        }

        String expireTime = null;
        final String expireTimeName = MessageProperty.expireTime.name();
        if (inputParams.containsKey(expireTimeName)) {
            expireTime = inputParams.get(expireTimeName).get(0);
            if (expireTime.equals(InvalidExpireTime) && 
                    MxOSConfig.isCreateMessageExpireByHeader()) {
                expireTime = mmsCustomExpireTime(messageArr, arrivalTime);
            }
        }
        if (expireTime == null) {
            expireTime = System
                    .getProperty(SystemProperty.createMessageExpireTime.name());
        }
        
        String threadId = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.threadId.name())) {
            threadId = mxosRequestState.getInputParams()
                    .get(MessageProperty.threadId.name()).get(0);
            options = options + CreateMsg.SL_CREATE_MSG_CONVERSATION_ID_PRESENT;
        }
        
        UUID folderUUID = null;
        try {
            folderUUID = ActionUtils
                    .getFolderUUID(mxosRequestState, folderName);
        } catch (NotFoundException nfe) {
            logger.info(new StringBuffer("Folder ").append(folderName).append(
                    " does not exist, trying to create"));
            createFolder(mxosRequestState, folderName);
            folderUUID = UUID.fromString(mxosRequestState
                    .getDbPojoMap().getProperty(FolderProperty.folderId));
        }
        if (logger.isDebugEnabled()) {
            logger.debug("messageStoreHost : " + host + "\tmailboxId : "
                    + mailboxId + "\trealm : " + realm + "\tfolderUUID : "
                    + folderUUID + "\tfromAddress : " + fromAddress
                    + "\tflags : " + strFlags + "\tkeywords : " + keywords
                    + "\tisPrivate : " + isPrivate + "\texpireTime : "
                    + expireTime + "\tarrivalTime : " + arrivalTime + "\toptions : " + options + "\toldMsgId : "
                    + oldMsgId + "\tuid : " + uid + "\tthreadId : "
                    + threadId);
        }

        if (null == folderUUID) {
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(),
                    "Folder: " + folderName + " not found");
        }

        try {
            CreateMsg cm = null;
            if (null != uid) {
                cm = new CreateMsg(host, mailboxId, realm, folderUUID,
                        messageArr, fromAddress, strFlags, keywords,
                        Boolean.parseBoolean(isPrivate), arrivalTime,
                        Long.parseLong(expireTime), options,
                        null, oldMsgId, uid, threadId);
            } else {
                cm = new CreateMsg(host, mailboxId, realm, folderUUID,
                        messageArr, fromAddress, strFlags, keywords,
                        Boolean.parseBoolean(isPrivate), arrivalTime,
                        Long.parseLong(expireTime), options,
                        null, oldMsgId, threadId);
            }

            cm.execute(myDataModel);

            String msgUUID = cm.getMsgUUID().toString();
            String updateRecentFlagInCreate = "false";

            if (System.getProperty(SystemProperty.updateRecentFlagAfterCreate
                    .name()) != null) {
                updateRecentFlagInCreate = System
                        .getProperty(SystemProperty.updateRecentFlagAfterCreate
                                .name());
            }

            if ((Boolean.parseBoolean(updateRecentFlagInCreate) == true)
                    && (flags.getFlagRecent() == false)) {
                updateRecentFlagAfterCreate(mxosRequestState, msgUUID, info,
                        folderUUID, popDeletedFlag, disableNotificationFlag);
            }

            return cm.getMsgUUID().toString();
        } catch (IntermailException e) {
            truncateRequestMessage(mxosRequestState, text);
            logger.warn("Error while create message: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Exception e) {
            truncateRequestMessage(mxosRequestState, text);
            logger.error("Error while create message.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Throwable t) {
            truncateRequestMessage(mxosRequestState, text);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_CREATE.name(), t);
        }
        return null;
    }
    
    /**
     * This method shall update the Recent Flag after the createMessage call.
     * 
     * @param mxosRequestState
     * @param messageId
     * @param info
     * @param folderUUID
     * @param popDeletedFlag
     * @throws MxOSException
     */
    private void updateRecentFlagAfterCreate(MxOSRequestState mxosRequestState,
            String messageId, MssLinkInfo info, UUID folderUUID,
            String popDeletedFlag, String disableNotificationFlag) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("updateRecentFlagAfterCreate Started...");
        }
        try {
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            UUID msgUUID = UUID.fromString(messageId);
            try {
                MsgFlags modFlags = new MsgFlags();
                MsgFlags newFlags = new MsgFlags();
                modFlags.SetFlag(MsgFlagIndex.flagrecent, true);
                newFlags.SetFlag(MsgFlagIndex.flagrecent, false);

                MsgFlagChangeInfo[] msgChangeInfo = new MsgFlagChangeInfo[1];
                msgChangeInfo[0] = new MsgFlagChangeInfo();
                msgChangeInfo[0].setMsgUUID(msgUUID);
                msgChangeInfo[0].setModifiedFlags(modFlags.getFlagInt());
                msgChangeInfo[0].setNewFlagValues(newFlags.getFlagInt());

                int accessId = Mailbox.ACCESS_ADMIN;
                int options = 0;
                if (Boolean.parseBoolean(popDeletedFlag)){
                	options = options + StoreMsgFlags.SPECIAL_DELETED_FLAG;
                }
                if (Boolean.parseBoolean(disableNotificationFlag)){
                	options = options + StoreMsgFlags.DISABLE_NOTIFICATION;
                }
                StoreMsgFlags storedMsgFlags = new StoreMsgFlags(host,
                        mailboxId, realm, folderUUID, msgChangeInfo,
                        options, accessId);
                storedMsgFlags.execute(myDataModel);
                logger.info("Message Flag update successful during updateRecentFlagAfterCreate.");
            } catch (Exception e) {
                logger.error(
                        "Error while update the message flags during updateRecentFlagAfterCreate.",
                        e);
            }

            if (logger.isDebugEnabled()) {
                logger.debug("updateRecentFlagAfterCreate End ");
            }
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error(
                    "Error while updating message flags during updateRecentFlagAfterCreate.",
                    e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_STORE_MSG_FLAGS.name(),
                    e);
        } catch (Throwable t) {
            logger.error(
                    "Error while updating message flags during updateRecentFlagAfterCreate.",
                    t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_STORE_MSG_FLAGS.name(),
                    t);
        }
    }

    @Override
    public void readMessage(final MxOSRequestState mxosRequestState,
            Message message) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("ReadMessage: starting read message");
        }
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        final String messageId = mxosRequestState.getInputParams()
                .get(MessageProperty.messageId.name()).get(0);
        try {
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
            }
            short[] attrType = null;
            // Get Messages using Key Message Id
            String[] keys = new String[1];
            keys[0] = messageId;
            UUID msgUUID = UUID.fromString(messageId);
            // Get message header
            GetMsgStream getMsgStream = new GetMsgStream(host, mailboxId,
                    realm, folderUUID, msgUUID,
                    GetMsgStream.FETCH_MSG_HEADER_SUMMARY);
            getMsgStream.execute(myDataModel);
            // Populate header
            Header header = null;
            String blob = new String(getMsgStream.getMsgBytes());
            if (null != blob && blob.length() > 0) {
                header = new Header();
                header.setHeaderBlob(blob);
                message.setHeader(header);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("header : " + blob);
            }

            // Get message body
            getMsgStream = new GetMsgStream(host, mailboxId, realm, folderUUID,
                    msgUUID, GetMsgStream.FETCH_MSG_BODY);
            getMsgStream.execute(myDataModel);
            // Populate body
            Body body = null;
            String bodyBlob = null;
            if (MxOSConfig.isMsgBodyBlobBase64Encoded()) {
                Base64 base64 = new Base64();
                bodyBlob = new String(base64.encode(getMsgStream.getMsgBytes()));
            } else {
                bodyBlob = new String(getMsgStream.getMsgBytes());
            }
            if (logger.isDebugEnabled()) {
                logger.debug("messageBody : " + bodyBlob);
            }
            if (null != bodyBlob && bodyBlob.length() > 0) {
                body = new Body();
                body.setMessageBlob(bodyBlob);
                message.setBody(body);
            }

            // Get MessageMetaData
            GetMessageAttributes getMsgAttrs = new GetMessageAttributes(host,
                    mailboxId, realm, folderUUID, (short) 0, keys, attrType,
                    Boolean.parseBoolean(popDeletedFlag));
            getMsgAttrs.execute(myDataModel);

            // Populate metadata
            Metadata metaData = null;
            MessageMetadata[] messageMetaData = getMsgAttrs
                    .getMessageMetadata();
            if (null != messageMetaData && messageMetaData.length > 0) {
                metaData = new Metadata();
                message.setMetadata(populateMetaData(messageMetaData[0],
                        metaData, false));
                if (logger.isDebugEnabled()) {
                    logger.debug("metadata : " + metaData);
                }
            }
        } catch (IntermailException e) {
            logger.warn("Error while reading message: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(ErrorCode.MSS_CONNECTION_ERROR.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while reading message.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while reading message.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading message.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
    }

    @Override
    protected void setSignature(MxOSRequestState mxosRequestState,
            String signature) throws MxOSException {
        UpdateReply updateOpr;
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            Reply reply = new Reply();
            reply.setType(Reply.SIGNATURE);
            reply.setText(signature);
            int operation = UpdateReply.SET_ENTRY;
            if (null == signature || "".equals(signature)) {
                operation = UpdateReply.CLEAR_ENTRY;
            }
            logger.debug(new StringBuffer().append(
                    "Signature update operation : ").append(operation));
            updateOpr = new UpdateReply(autoReplyHost, mailboxId, realm,
                    operation, reply, myDataModel);
            updateOpr.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while set signature: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
        } catch (Exception e) {
            logger.error("Error while set signature.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while set signature.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIGNATURE.name(), t);
        }
    }

    @Override
    protected void setAutoReplyText(MxOSRequestState mxosRequestState,
            String autoReplyMessage) throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            Reply reply = new Reply();
            reply.setType(Reply.AUTOREPLY);
            reply.setText(autoReplyMessage);
            int operation = UpdateReply.SET_ENTRY;
            if (null == autoReplyMessage || "".equals(autoReplyMessage)) {
                operation = UpdateReply.CLEAR_ENTRY;
            }
            logger.debug(new StringBuffer().append("AR update operation : ")
                    .append(operation));
            UpdateReply updateOpr = new UpdateReply(autoReplyHost, mailboxId,
                    realm, operation, reply, myDataModel);
            updateOpr.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while set auto-reply text: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Exception e) {
            logger.error("Error while set auto-reply text.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while set auto-reply text.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_AUTO_REPLY_MESSAGE.name(), t);
        }
    }

    @Override
    protected void setMTAFilter(MxOSRequestState mxosRequestState, String filter)
            throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            Reply reply = new Reply();
            reply.setType(Reply.USERFILTER);
            reply.setText(filter);
            int operation = UpdateReply.SET_ENTRY;
            if (null == filter || "".equals(filter)) {
                operation = UpdateReply.CLEAR_ENTRY;
            }
            logger.debug(new StringBuffer().append(
                    "MTA filter update operation : ").append(operation));
            UpdateReply updateOpr = new UpdateReply(autoReplyHost, mailboxId,
                    realm, operation, reply, myDataModel);
            updateOpr.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while set MTA filter: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    e);
        } catch (Exception e) {
            logger.error("Error while set MTA filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while set MTA filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_MTA_FILTER
                            .name(),
                    t);
        }
    }

    @Override
    protected void setRMFilter(MxOSRequestState mxosRequestState, String filter)
            throws Exception {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String autoReplyHost = info.getAutoReplyHost();
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("AutoReplyHost : ")
                        .append(autoReplyHost));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            Reply reply = new Reply();
            reply.setType(Reply.WEBEDGE_META_FILTERS);
            reply.setText(filter);
            int operation = UpdateReply.SET_ENTRY;
            if (null == filter || "".equals(filter)) {
                operation = UpdateReply.CLEAR_ENTRY;
            }
            logger.debug(new StringBuffer().append(
                    "RM filter update operation : ").append(operation));
            UpdateReply updateOpr = new UpdateReply(autoReplyHost, mailboxId,
                    realm, operation, reply, myDataModel);
            updateOpr.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while set RM filter: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    e);
        } catch (Exception e) {
            logger.error("Error while set RM filter.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    e);
        } catch (Throwable t) {
            logger.error("Error while set RM filter.", t);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_SIEVE_FILTERS_RM_FILTER
                            .name(),
                    t);
        }
    }

    @Override
    public void deleteMessages(MxOSRequestState mxosRequestState,
            String[] msgUUIDs) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMessage: starting delete message");
        }
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        try {
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            UUID[] uuIds = new UUID[msgUUIDs.length];
            for (int i = 0; i < msgUUIDs.length; i++) {
                uuIds[i] = UUID.fromString(msgUUIDs[i]);
            }
            int options = 0;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                final String popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
                if (Boolean.parseBoolean(popDeletedFlag)) {
                    options = RemoveMultiMsgs.REMOVE_MULTI_MSGS_SPECIAL_DELETE_FLAG;
                }
            }
            
            String disableNotificationFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.disableNotificationFlag.name())) {
                disableNotificationFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.disableNotificationFlag.name())
                        .get(0);

            } else {
                disableNotificationFlag = System.getProperty(
                        SystemProperty.disableNotificationFlag.name(), "true");
            }

            if (Boolean.parseBoolean(disableNotificationFlag)) {
                options = options
                        + RemoveMultiMsgs.REMOVE_MULTI_MSGS_DISABLE_NOTIFICATION;
            }

            if (logger.isDebugEnabled()) {
                for (String msgUUID : msgUUIDs) {
                    logger.debug(new StringBuffer().append(
                            "Deleting messsage with msgUUID - ")
                            .append(msgUUID));
                    logger.debug(new StringBuffer("folder UUID : ")
                            .append(folderUUID));
                    logger.debug(new StringBuffer("options : ")
                    .append(options));
                }
            }
            RemoveMultiMsgs removeMultiMsgs = new RemoveMultiMsgs(host,
                    mailboxId, realm, folderUUID, uuIds,
                    options);
            removeMultiMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("DeleteMessage: Success");
            }
        } catch (IntermailException e) {
            logger.warn("Error while deleting message: "
                    + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while deleting message from folder"
                    + folderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while deleting message from folder"
                    + folderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), t);
        }
    }

    @Override
    public void deleteAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteAllMessages: starting delete all messages");
        }
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        try {
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            int options = 0;
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
                if (Boolean.parseBoolean(popDeletedFlag)) {
                    options = RemoveMultiMsgs.REMOVE_MULTI_MSGS_SPECIAL_DELETE_FLAG;
                }
            }

            String disableNotificationFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.disableNotificationFlag.name())) {
                disableNotificationFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.disableNotificationFlag.name())
                        .get(0);

            } else {
                disableNotificationFlag = System.getProperty(
                        SystemProperty.disableNotificationFlag.name(), "true");
            }

            if (Boolean.parseBoolean(disableNotificationFlag)) {
                options = options
                        + RemoveMultiMsgs.REMOVE_MULTI_MSGS_DISABLE_NOTIFICATION;
            }

            // Get all the msgUUIDs from the folder
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving msgUUIDs");
            }
            ImapListUids imapListUids = new ImapListUids(host, mailboxId,
                    realm, folderUUID, Boolean.parseBoolean(popDeletedFlag));
            imapListUids.execute(myDataModel);
            UUID[] msgUuIds = imapListUids.getMsgUUids();
            // Remove all the message from folder
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "Deleting all messsages in folder ").append(folderName));
                logger.debug(new StringBuffer("folder UUID : ")
                        .append(folderUUID));
                logger.debug(new StringBuffer("options : ")
                .append(options));
            }
            if (null != msgUuIds && msgUuIds.length > 0) {
                RemoveMultiMsgs removeMultiMsgs = new RemoveMultiMsgs(host,
                        mailboxId, realm, folderUUID, msgUuIds,
                        options);
                removeMultiMsgs.execute(myDataModel);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("DeleteAllMessages: Success");
            }
        } catch (IntermailException e) {
            logger.warn("Error while deleting all messages: "
                    + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while deleting all messages from folder "
                    + folderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while deleting all messages from folder "
                    + folderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE_ALL.name(), t);
        }
    }

    @Override
    public void copyMessages(MxOSRequestState mxosRequestState,
            String[] msgUUIDs) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("CopyMessage: starting copy message"));
        }
        
        final String srcFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.srcFolderName.name(), mxosRequestState);
        final String toFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.toFolderName.name(), mxosRequestState);
        
        try {
            final UUID srcFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, srcFolderName);
            final UUID destFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, toFolderName);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
                logger.debug(new StringBuffer("Copying messages from folder - ")
                        .append(srcFolderName).append(" , to folder - ")
                        .append(toFolderName));
                logger.debug(new StringBuffer("Source folder UUID : ")
                        .append(srcFolderUUID));
                logger.debug(new StringBuffer("Destination folder UUID : ")
                        .append(destFolderUUID));
            }

            // Increment this numAttrs as the number of metadata parameters
            // increase
            int numAttrs = 0;

            Flags flags = (Flags) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlags);
            MsgFlags msgFlags = new MsgFlags();
            msgFlags.SetFlag(MsgFlagIndex.flagrecent, flags.getFlagRecent());
            msgFlags.SetFlag(MsgFlagIndex.flagseen, flags.getFlagSeen());
            msgFlags.SetFlag(MsgFlagIndex.flagans, flags.getFlagAns());
            msgFlags.SetFlag(MsgFlagIndex.flagflagged, flags.getFlagFlagged());
            msgFlags.SetFlag(MsgFlagIndex.flagdraft, flags.getFlagDraft());
            msgFlags.SetFlag(MsgFlagIndex.flagdel, flags.getFlagDel());
            numAttrs++;

            Integer uid = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.uid.name())) {
                uid = Integer.parseInt(mxosRequestState.getInputParams()
                        .get(MessageProperty.uid.name()).get(0));
                numAttrs++;
            }

            MessageMetadata[] copyMessageMetadatas = new MessageMetadata[msgUUIDs.length];
            for (int i = 0; i < msgUUIDs.length; i++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Copying message with \tmsgUUID : "
                            + msgUUIDs[i]);
                }
                copyMessageMetadatas[i] = new MessageMetadata(numAttrs);
                copyMessageMetadatas[i].setMessageUUID(UUID
                        .fromString(msgUUIDs[i]));
                if (null != msgFlags) {
                    copyMessageMetadatas[i].setMessageFlags(msgFlags
                            .getFlagInt());
                }
                if (null != uid) {
                    copyMessageMetadatas[i].setUid(uid);
                }
            }
            
            int options = 0;
            String disableNotificationFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.disableNotificationFlag.name())) {
                disableNotificationFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.disableNotificationFlag.name())
                        .get(0);

            } else {
                disableNotificationFlag = System.getProperty(
                        SystemProperty.disableNotificationFlag.name(), "true");
            }

            if (Boolean.parseBoolean(disableNotificationFlag)) {
                options = options + CopyMsgs.COPY_MSGS_DISABLE_NOTIFICATION;
            }

            CopyMsgs copyMsgs = new CopyMsgs(host, mailboxId, realm,
                    copyMessageMetadatas, srcFolderUUID, destFolderUUID, false,
                    options);
            copyMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("CopyMessage: Success ");
            }
        } catch (IntermailException e) {
            logger.warn("Error while copying message: "
                    + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("CassdbDataInconsistency")) {
                throw new ApplicationException(
                        MessageError.MSG_NOT_FOUND.name(), e.getMessage());
            } else {
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(),
                                e);
            }
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while copying message from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), e);
        } catch (Throwable t) {
            logger.error("Error while copying message from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY.name(), t);
        }
    }

    @Override
    public void copyAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("copyAllMessage: starting copy all messages"));
        }
        
        final String srcFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.srcFolderName.name(), mxosRequestState);
        final String toFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.toFolderName.name(), mxosRequestState);

        try {
            final UUID srcFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, srcFolderName);
            final UUID destFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, toFolderName);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            // Get all the msgUUIDs from the source folder
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving msgUUIDs");
            }
            ImapListUids imapListUids = new ImapListUids(host, mailboxId,
                    realm, srcFolderUUID, false);
            imapListUids.execute(myDataModel);
            UUID[] msgUUids = imapListUids.getMsgUUids();

            // Copy all the messages from the given folder to destination
            // folder
            if (null != msgUUids && msgUUids.length > 0) {
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer(
                            "Copying all the messages from folder - ")
                            .append(srcFolderName).append(", to folder - ")
                            .append(toFolderName));
                    logger.debug(new StringBuffer("Source folder UUID : ")
                            .append(srcFolderUUID));
                    logger.debug(new StringBuffer("Destination folder UUID : ")
                            .append(destFolderUUID));
                }
                MessageMetadata[] copyMessageMetadatas = new MessageMetadata[msgUUids.length];
                for (int i = 0; i < msgUUids.length; i++) {
                    copyMessageMetadatas[i] = new MessageMetadata(1);
                    copyMessageMetadatas[i].setMessageUUID(msgUUids[i]);
                }
                
                int options = 0;
                String disableNotificationFlag = null;
                if (mxosRequestState.getInputParams().containsKey(
                        MessageProperty.disableNotificationFlag.name())) {
                    disableNotificationFlag = mxosRequestState
                            .getInputParams()
                            .get(MessageProperty.disableNotificationFlag.name())
                            .get(0);

                } else {
                    disableNotificationFlag = System.getProperty(
                            SystemProperty.disableNotificationFlag.name(),
                            "true");
                }

                if (Boolean.parseBoolean(disableNotificationFlag)) {
                    options = options + CopyMsgs.COPY_MSGS_DISABLE_NOTIFICATION;
                }

                CopyMsgs copyMsgs = new CopyMsgs(host, mailboxId, realm,
                        copyMessageMetadatas, srcFolderUUID, destFolderUUID,
                        false, options);
                copyMsgs.execute(myDataModel);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("CopyAllMessage: Success ");
            }
        } catch (IntermailException e) {
            logger.warn("Error while copying all messages from folder "
                    + srcFolderName + " to folder " + toFolderName
                    + ": " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while copying all messages from folder "
                    + srcFolderName + " to folder " + toFolderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while copying all messages from folder "
                    + srcFolderName + " to folder " + toFolderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_COPY_ALL.name(), t);
        }
    }

    @Override
    public void moveMessages(MxOSRequestState mxosRequestState,
            String[] msgUUIDs) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("MoveMessage: starting move message"));
        }
        
        final String srcFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.srcFolderName.name(), mxosRequestState);
        final String toFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.toFolderName.name(), mxosRequestState);

        try {
            final UUID srcFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, srcFolderName);
            final UUID destFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, toFolderName);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
                logger.debug(new StringBuffer("Moving messages from folder - ")
                        .append(srcFolderName).append(" , to folder - ")
                        .append(toFolderName));
                logger.debug(new StringBuffer("Source folder UUID : ")
                        .append(srcFolderUUID));
                logger.debug(new StringBuffer("Destination folder UUID : ")
                        .append(destFolderUUID));
            }
            // Increment this numAttrs as the number of metadata parameters
            // increase
            int numAttrs = 0;
            String isPrivate = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.isPrivate.name())) {
                isPrivate = mxosRequestState.getInputParams()
                        .get(MessageProperty.isPrivate.name()).get(0);
                numAttrs++;
            }

            String expireTime = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.expireTime.name())) {
                expireTime = mxosRequestState.getInputParams()
                        .get(MessageProperty.expireTime.name()).get(0);
                numAttrs++;
            }

            Flags flags = (Flags) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlags);
            MsgFlags msgFlags = new MsgFlags();
            msgFlags.SetFlag(MsgFlagIndex.flagrecent, flags.getFlagRecent());
            msgFlags.SetFlag(MsgFlagIndex.flagseen, flags.getFlagSeen());
            msgFlags.SetFlag(MsgFlagIndex.flagans, flags.getFlagAns());
            msgFlags.SetFlag(MsgFlagIndex.flagflagged, flags.getFlagFlagged());
            msgFlags.SetFlag(MsgFlagIndex.flagdraft, flags.getFlagDraft());
            msgFlags.SetFlag(MsgFlagIndex.flagdel, flags.getFlagDel());
            numAttrs++;

            String keywords = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.keywords.name())) {
                keywords = mxosRequestState.getInputParams()
                        .get(MessageProperty.keywords.name()).get(0);
                numAttrs++;
            }

            Integer uid = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.uid.name())) {
                uid = Integer.parseInt(mxosRequestState.getInputParams()
                        .get(MessageProperty.uid.name()).get(0));
                numAttrs++;
            }

            MessageMetadata[] moveMessageMetadatas = new MessageMetadata[msgUUIDs.length];
            for (int i = 0; i < msgUUIDs.length; i++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Moving message with \tmsgUUID : "
                            + msgUUIDs[i]);
                }
                moveMessageMetadatas[i] = new MessageMetadata(numAttrs);
                moveMessageMetadatas[i].setMessageUUID(UUID
                        .fromString(msgUUIDs[i]));
                if (null != isPrivate) {
                    moveMessageMetadatas[i].setPrivate(Boolean
                            .parseBoolean(isPrivate));
                }
                if (null != expireTime) {
                    moveMessageMetadatas[i].setExpirationTime(Long
                            .parseLong(expireTime));
                }
                if (null != msgFlags) {
                    moveMessageMetadatas[i].setMessageFlags(msgFlags
                            .getFlagInt());
                }
                if (null != keywords) {
                    moveMessageMetadatas[i].setKeywords(keywords.split(","));
                }
                if (null != uid) {
                    moveMessageMetadatas[i].setUid(uid);
                }
            }
            
            int options = 0;
            String disableNotificationFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.disableNotificationFlag.name())) {
                disableNotificationFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.disableNotificationFlag.name())
                        .get(0);

            } else {
                disableNotificationFlag = System.getProperty(
                        SystemProperty.disableNotificationFlag.name(), "true");
            }

            if (Boolean.parseBoolean(disableNotificationFlag)) {
                options = options + MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
            }

            MoveMsgs moveMsgs = new MoveMsgs(host, mailboxId, realm,
                    moveMessageMetadatas, srcFolderUUID, destFolderUUID,
                    options);
            moveMsgs.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("MoveMessage: Success "));
            }
        } catch (IntermailException e) {
            logger.warn("Error while moving message from folder "
                    + srcFolderName + " to folder " + toFolderName
                    + ": " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("CassdbDataInconsistency")) {
                throw new ApplicationException(
                        MessageError.MSG_NOT_FOUND.name(), e.getMessage());
            } else {
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(),
                                e);
            }
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while moving message from folder "
                    + srcFolderName + " to folder " + toFolderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while moving message from folder "
                    + srcFolderName + " to folder " + toFolderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE.name(), t);
        }
    }

    @Override
    public void moveAllMessages(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer()
                    .append("MoveAllMessage: starting move all messages"));
        }
        
        final String srcFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.srcFolderName.name(), mxosRequestState);
        final String toFolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.toFolderName.name(), mxosRequestState);
        
        try {
            final UUID srcFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, srcFolderName);
            final UUID destFolderUUID = ActionUtils.getFolderUUID(
                    mxosRequestState, toFolderName);
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);

            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            // Get all the msgUUIDs from the source folder
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving msgUUIDs");
            }
            ImapListUids imapListUids = new ImapListUids(host, mailboxId,
                    realm, srcFolderUUID, false);
            imapListUids.execute(myDataModel);
            UUID[] msgUUids = imapListUids.getMsgUUids();
            // Move all the messages from the given folder to destination
            // folder
            if (null != msgUUids && msgUUids.length > 0) {
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer(
                            "Moving all the messages from folder - ")
                            .append(srcFolderName).append(", to folder - ")
                            .append(toFolderName));
                    logger.debug(new StringBuffer("Source folder UUID : ")
                            .append(srcFolderUUID));
                    logger.debug(new StringBuffer("Destination folder UUID : ")
                            .append(destFolderUUID));
                }
                MessageMetadata[] moveMessageMetadatas = new MessageMetadata[msgUUids.length];
                for (int i = 0; i < msgUUids.length; i++) {
                    moveMessageMetadatas[i] = new MessageMetadata(1);
                    moveMessageMetadatas[i].setMessageUUID(msgUUids[i]);
                }
                int options = 0;
                String disableNotificationFlag = null;
                if (mxosRequestState.getInputParams().containsKey(
                        MessageProperty.disableNotificationFlag.name())) {
                    disableNotificationFlag = mxosRequestState
                            .getInputParams()
                            .get(MessageProperty.disableNotificationFlag.name())
                            .get(0);

                } else {
                    disableNotificationFlag = System.getProperty(
                            SystemProperty.disableNotificationFlag.name(),
                            "true");
                }

                if (Boolean.parseBoolean(disableNotificationFlag)) {
                    options = options + MoveMsgs.MOVE_MSGS_DISABLE_NOTIFICATION;
                }

                MoveMsgs moveMsgs = new MoveMsgs(host, mailboxId, realm,
                        moveMessageMetadatas, srcFolderUUID, destFolderUUID,
                        options);
                moveMsgs.execute(myDataModel);
            }
            if (logger.isDebugEnabled()) {
                logger.debug("MoveAllMessage: Success ");
            }
        } catch (IntermailException e) {
            logger.warn("Error while moving all messages from folder "
                    + srcFolderName + " to folder " + toFolderName
                    + ": " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while moving all messages from folder "
                    + srcFolderName + " to folder " + toFolderName, e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), e);
        } catch (Throwable t) {
            logger.error("Error while moving all messages from folder "
                    + srcFolderName + " to folder " + toFolderName, t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_MOVE_ALL.name(), t);
        }
    }

    private static void calculateFolderPaths(FolderInfo[] folderInfo) {
        for (int i = 0; i < folderInfo.length; i++) {
            folderInfo[i].setPathName(getPathName(
                    folderInfo[i].getParentFolderUUID(), folderInfo)
                    + folderInfo[i].getFolderName());
        }
    }

    private static String getPathName(UUID folderUUID, FolderInfo[] folderInfo) {
        if (folderUUID == null
                || folderUUID.toString().equals(MxOSConstants.ROOT_FOLDER_UUID)) {
            return MxOSConstants.FORWARD_SLASH;
        } else {
            FolderInfo folder = null;
            for (int i = 0; i < folderInfo.length; i++) {
                // find the folder parent UUID
                if (folderInfo[i].getFolderUUID().equals(folderUUID)) {
                    folder = folderInfo[i];
                    break;
                }
            }
            return (getPathName(folder.getParentFolderUUID(), folderInfo)
                    + folder.getFolderName() + MxOSConstants.FORWARD_SLASH);
        }
    }

    @Override
    public void readFolder(MxOSRequestState mxosRequestState,
            com.opwvmsg.mxos.message.pojos.Folder folder) throws MxOSException {

        String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        if (!folderName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            folderName = MxOSConstants.FORWARD_SLASH + folderName;
        }
        int cnt = 0;
        try {
            FolderInfo[] folderInfo = (FolderInfo[]) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.folderInfo);
            if (folderInfo != null) {
                for (FolderInfo f : folderInfo) {
                    if (f.getPathName().equals(folderName)) {
                        populateFolder(folder, f);
                        cnt++;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error while reading folder.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading folder.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_GET.name(), t);
        }
        if (cnt == 0) {
            logger.warn("Given folder NOT found.");
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(),
                    "Folder: " + folderName + " not found");
        }
        return;
    }

    @Override
    public void listFolders(final MxOSRequestState mxosRequestState,
            List<com.opwvmsg.mxos.message.pojos.Folder> folders)
            throws MxOSException {
        try {
            FolderInfo[] folderInfo = (FolderInfo[]) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.folderInfo);
            if (folders == null) {
                folders = new ArrayList<com.opwvmsg.mxos.message.pojos.Folder>();
            }
            if (folderInfo != null) {
                for (int i = 0; i < folderInfo.length; i++) {
                    com.opwvmsg.mxos.message.pojos.Folder beanFolder = new com.opwvmsg.mxos.message.pojos.Folder();
                    populateFolder(beanFolder, folderInfo[i]);
                    folders.add(beanFolder);
                }
            }
            return;
        } catch (Exception e) {
            logger.error("Error while reading all folders.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_LIST.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading all folders.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_LIST.name(), t);
        }
    }

    @Override
    public void deleteFolder(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        
        // check if the folder is prohibited for deletion
        String problemType = null;
        if (folderName.equals("")) {
            problemType = "empty folder name";
        } else if (folderName.equalsIgnoreCase(MxOSConstants.INBOX)) {
            problemType = "inbox";
        } else if (systemfolders.contains(folderName)) {
            problemType = "system folder";
        }
        if (problemType != null) {
            String errorMessage = new StringBuilder(folderName)
                    .append(": ").append(problemType)
                    .append(" is not allowed for deletion").toString();
            logger.warn(errorMessage);
            throw new InvalidRequestException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), errorMessage);
        }
        
        try {
            UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            UUID parentFolderUUID = ActionUtils.getParentFolderUUID(
                    mxosRequestState, folderName);
            boolean force = false;
            if (mxosRequestState.getInputParams().get(
                    FolderProperty.force.name()) != null) {
                force = getForceParam(mxosRequestState.getInputParams()
                        .get(FolderProperty.force.name()).get(0));
            }

            DeleteFolder deleteFolder = new DeleteFolder(host, mailboxId,
                    realm, parentFolderUUID, folderUUID, force);
            deleteFolder.execute(myDataModel);
            if (logger.isDebugEnabled()) {
                logger.debug("Successfully deleted the folder : " + folderName);
            }
            return;
        } catch (IntermailException e) {
            logger.warn("Error while moving message: "
                    + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while deleting folder.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while deleting folder.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_DELETE.name(), t);
        }
    }

    protected void populateFolder(com.opwvmsg.mxos.message.pojos.Folder folder,
            FolderInfo f) {
        if (null != f.getFolderUUID()) {
            folder.setFolderId(f.getFolderUUID());
            folder.setFolderName(f.getPathName());
            folder.setNextUID((long) f.getFolderNextUID());
            folder.setUidValidity((long) f.getFolderUidValidity());
            folder.setNumMessages((int) f.getFolderTotMsgs());
            folder.setNumReadMessages((int) (f.getFolderTotMsgs() - f
                    .getFolderTotMsgsUnRead()));
            folder.setNumUnreadMessages((int) f.getFolderTotMsgsUnRead());
            folder.setFolderSizeBytes(f.getFolderTotBytes());
            folder.setFolderSizeReadBytes((f.getFolderTotBytes() - f
                    .getFolderTotBytesUnRead()));
            folder.setFolderSizeUnreadBytes(f.getFolderTotBytesUnRead());
            folder.setParentFolderId(f.getParentFolderUUID());
            if (f.getFolderSubscribed() == 1)
                folder.setFolderSubscribed(FolderSubscribed.TRUE);
            else
                folder.setFolderSubscribed(FolderSubscribed.FALSE);
            folder.setKeywords(f.getFolderKeywords());
        }
    }

    /** METADATA Methods **/

    /**
     * This method is not being used, this is just a backup in case if
     * listMessageUUIDs needs to be used then this method can be reused by
     * adding a helper method in MessageServiceHelper.java and configuring the
     * actions RetrieveIMAPUUIDs.java and
     * ListMessagesMetaDataInFolderIMAPUUIDs.java in message-mxos-rules.xml
     */
    public void listMetadatasInFolderUsingIMAPUUIDs(
            MxOSRequestState mxosRequestState, final String folderName,
            Map<String, Metadata> metadatas) throws MxOSException {

        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            String[] keys = (String[]) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.imapMessageUids);
            if (null == keys || keys.length < 1) {
                logger.info("No message UUid's found, returning empty list.");
                return;
            } else {
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer().append("Message UUIDs : ")
                            .append(Arrays.toString(keys)));
                }
            }
            UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            short[] attrType = {};
            short keyType = GetMessageAttributes.KEYTYPE_MESSAGE_UUID;
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append(
                        "Reading message metadata from folder : ").append(
                        folderUUID));
            }
            GetMessageAttributes readMsgs = new GetMessageAttributes(host,
                    mailboxId, realm, folderUUID, keyType, keys, attrType,
                    Boolean.parseBoolean(popDeletedFlag));
            readMsgs.execute(myDataModel);
            MessageMetadata[] msgMetadata = readMsgs.getMessageMetadata();
            for (int i = 0; i < msgMetadata.length; i++) {
                // For each message from RME create a message POJO and update
                // the messages list
                metadatas.put(msgMetadata[i].getMessageUUID().toString(),
                        this.populateMetaData(msgMetadata[i], new Metadata(),
                                false));
            }
            logger.info("Message Metadata List successful with size: "
                    + msgMetadata.length);
            return;
        } catch (IntermailException e) {
            logger.warn("Error while reading messages: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }

    @Override
    public void listMetadatasInFolder(MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Metadata> metadatas)
            throws MxOSException {

        try {
            if (logger.isDebugEnabled()) {
                logger.debug("List Metadatas in Folder Start.");
            }
            /** getting MssInfo **/
            final MssLinkInfo info = (MssLinkInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            final Map<String, List<String>> inputParam = mxosRequestState
                    .getInputParams();
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
            }

            /** sortKey **/
            short keyType = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                final String sortKey = inputParam.get(
                        MailboxProperty.sortKey.name()).get(0);
                for (MessageProperty prop : MessageProperty.values()) {
                    if (prop.name().equals(sortKey)) {
                        if (READSORTEDMSGS_KEY_MAP.containsKey(prop)) {
                            keyType = READSORTEDMSGS_KEY_MAP.get(prop);
                        }
                    }
                }
            }
            /** sortOrder **/
            SortOrder sortOrder = SortOrder.ascending;
            String sortOrderStr = sortOrder.name();
            if (inputParam.containsKey(MailboxProperty.sortOrder.name())) {
                sortOrderStr = inputParam.get(MailboxProperty.sortOrder.name())
                        .get(0);
            } else {
                sortOrderStr = System.getProperty(
                        SystemProperty.msgMetadataSortingOrderDefault.name(),
                        "0");
            }
            if (SortOrder.ascending.name().equals(sortOrderStr)) {
                sortOrder = SortOrder.ascending;
            } else if (SortOrder.descending.name().equals(sortOrderStr)) {
                sortOrder = SortOrder.descending;
            }
            int sliceCount = Integer.MAX_VALUE;

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("ListMetadatasInFolder for host: ").append(host)
                        .append(", mailboxId: ").append(mailboxId)
                        .append(", realm: ").append(realm)
                        .append(", folder: ").append(folderUUID)
                        .append(", sortKey: ").append(keyType)
                        .append(", sortOrder: ").append(sortOrder)
                        .append(", popDeletedFlag: ").append(popDeletedFlag)
                        .append(", sliceCount: ").append(sliceCount)
                        .append(", sliceReversed: ").append(sortOrder.getOrderValue()));
            }

            MessageMetadata[] messageMetadata = null;
            if (popDeletedFlag != null && !("".equals(popDeletedFlag))) {
                PopListInfo popListInfo = getPopListInfo(host, mailboxId,
                        realm, folderUUID, Boolean.parseBoolean(popDeletedFlag));
                UUID[] msgUuIds = popListInfo.getMsgUUids();
                if (msgUuIds != null && msgUuIds.length > 0) {
                    String[] keys = new String[msgUuIds.length];
                    if (SortOrder.descending.name().equals(sortOrderStr)) {
                        int j = 0;
                        for (int i = (msgUuIds.length - 1); i >= 0; i--) {
                            keys[j] = msgUuIds[i].toString();
                            j++;
                        }
                    } else {
                        for (int i = 0; i < msgUuIds.length; i++) {
                            keys[i] = msgUuIds[i].toString();
                        }
                    }
                    if (logger.isDebugEnabled()) {
                        logger.debug(new StringBuffer().append(
                                "Reading messages metadata for messageIds : ")
                                .append(msgUuIds));
                    }
                    short[] attrType = {};
                    keyType = GetMessageAttributes.KEYTYPE_MESSAGE_UUID;
                    messageMetadata = getMessageAttributes(host, mailboxId,
                            realm, folderUUID, keyType, keys, attrType,
                            Boolean.parseBoolean(popDeletedFlag));
                    if (logger.isDebugEnabled()) {
                        logger.debug("MessageMetadata Size: "
                                + messageMetadata.length);
                    }
                    for (int i = 0; i < messageMetadata.length; i++) {
                        // For each MessageMetadata create a Metadata POJO and
                        // update the Metadata map
                        final Metadata metadata = this.populateMetaData(
                                messageMetadata[i], new Metadata(), false);
                        metadatas.put(messageMetadata[i].getMessageUUID()
                                .toString(), metadata);
                    }
                }
            } else {
                final ReadSortedMsgs readMsgs = new ReadSortedMsgs(host,
                        mailboxId, realm, folderUUID, keyType, null, null,
                        sliceCount, sortOrder.getOrderValue(), null, false, true);
                readMsgs.execute();
                final LinkedHashMap<Integer, MessageMetadata[]> sortedMessages = readMsgs
                        .getSortedMessages();
                if (logger.isDebugEnabled()) {
                    logger.debug("sortedMessages Size: "
                            + sortedMessages.size());
                }
                final Iterator<Entry<Integer, MessageMetadata[]>> itr = sortedMessages
                        .entrySet().iterator();
                while (itr.hasNext()) {
                    Entry<Integer, MessageMetadata[]> mapEntry = itr.next();
                    messageMetadata = mapEntry.getValue();
                    if (logger.isDebugEnabled()) {
                        logger.debug("MessageMetadata Size: "
                                + messageMetadata.length);
                    }
                    for (int i = 0; i < messageMetadata.length; i++) {
                        // For each MessageMetadata create a Metadata POJO and
                        // update the Metadata map
                        final Metadata metadata = this.populateMetaData(
                                messageMetadata[i], new Metadata(), false);
                        metadatas.put(messageMetadata[i].getMessageUUID()
                                .toString(), metadata);
                    }
                }
            }
        } catch (IntermailException e) {
            logger.warn("Error while reading messages: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while reading messages from folder", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }

    @Override
    public void listMetadatasUidsInFolder(MxOSRequestState mxosRequestState,
            final String folderName, Map<String, Metadata> metadatas)
            throws MxOSException {

        try {
            if (logger.isDebugEnabled()) {
                logger.debug("List Metadatas UIDs in Folder");
            }
            /** getting MssInfo **/
            final MssLinkInfo info = (MssLinkInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                            .get(MessageProperty.popDeletedFlag.name()).get(0);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("ListIMAPUIDs for host : ").append(host)
                        .append(", mailboxId: ").append(mailboxId)
                        .append(", realm: ").append(realm).append(", folder: ")
                        .append(folderUUID).append(", popDeletedFlag: ")
                        .append(popDeletedFlag));
            }
            // Get all the msgUUIDs from the folder
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving msgUUIDs");
            }
            ImapListUids imapListUids = new ImapListUids(host, mailboxId,
                    realm, folderUUID, Boolean.parseBoolean(popDeletedFlag));
            imapListUids.execute(myDataModel);
            UUID[] msgUuIds = imapListUids.getMsgUUids();
            int[] msgUids = imapListUids.getImapUids();
            if (null != msgUuIds && null != msgUids && msgUuIds.length > 0) {
                for (int i = 0; i < msgUuIds.length; i++) {
                    final Metadata metadata = new Metadata();
                    metadata.setUid(new Long(msgUids[i]));
                    metadatas.put(msgUuIds[i].toString(), metadata);
                }
            }
        } catch (IntermailException e) {
            logger.warn("Error while reading messages meta data uids list: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while reading messages meta data uids list", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages meta data uids list", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages meta data uids list", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), t);
        }
    }

    @Override
    public void listMetadatasUUIDsInFolder(MxOSRequestState mxosRequestState,
            final String folderName, List<String> uuids)
            throws MxOSException {

        try {
            if (logger.isDebugEnabled()) {
                logger.debug("List Metadatas UIDs in Folder");
            }
            /** getting MssInfo **/
            final MssLinkInfo info = (MssLinkInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            final Map<String, List<String>> inputParam = mxosRequestState.getInputParams();
            /** shortKey **/
            short keyType = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                final String sortKey = inputParam.get(
                        MailboxProperty.sortKey.name()).get(0);
                for (MessageProperty prop : MessageProperty.values()) {
                    if (prop.name().equals(sortKey)) {
                        if (READSORTEDMSGS_KEY_MAP.containsKey(prop)) {
                            keyType = READSORTEDMSGS_KEY_MAP.get(prop);
                        }
                    }
                }
            }
            
            keyType = ReadSortedMsgs.getKeyType(inputParam,keyType);

            /** getting optional parameters **/
            String sliceStart = null;
            if (inputParam.containsKey(MailboxProperty.sliceStart.name())) {
                if (inputParam.get(MailboxProperty.sliceStart.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceStart.name()).get(0)))) {
                    sliceStart = inputParam.get(
                            MailboxProperty.sliceStart.name()).get(0);
                }
            }

            String sliceEnd = null;
            if (inputParam.containsKey(MailboxProperty.sliceEnd.name())) {
                if (inputParam.get(MailboxProperty.sliceEnd.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceEnd.name()).get(0)))) {
                    sliceEnd = inputParam.get(MailboxProperty.sliceEnd.name())
                            .get(0);
                }
            }

            int sliceCount = Integer.MAX_VALUE;
            /*
             * ReadSortedMsgs is also used for both "list" and "search" APIs.
             * Only for "search" a sliceCount is needed, for "list" providing
             * entire data-list.
             */
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                String sliceCountStr;
                if (inputParam.containsKey(MailboxProperty.sliceCount.name())) {
                    sliceCountStr = inputParam.get(
                            MailboxProperty.sliceCount.name()).get(0);
                } else {
                    sliceCountStr = System.getProperty(
                            SystemProperty.sortedMsgMetadataListCountDefault
                                    .name(), "10");
                }
                sliceCount = Integer.parseInt(sliceCountStr);
                if (sliceCount < 0) {
                    sliceCount = Integer.MAX_VALUE;
                }
            }

            SortOrder sortOrder = SortOrder.ascending;
            String sortOrderStr = sortOrder.name();
            if (inputParam.containsKey(MailboxProperty.sortOrder.name())) {
                sortOrderStr = inputParam.get(MailboxProperty.sortOrder.name())
                        .get(0);
            } else {
                sortOrderStr = System.getProperty(
                        SystemProperty.msgMetadataSortingOrderDefault.name(),
                        "0");
            }
            for (SortOrder so : SortOrder.values()) {
                if (so.name().equals(sortOrderStr)) {
                    sortOrder = so;
                }
            }
            final short sliceReversed = sortOrder.getOrderValue();

            boolean popDeletedFlag = true;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = Boolean.parseBoolean(mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0));
            }
            String conversationViewReq = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.conversationViewReq.name())) {
                conversationViewReq = mxosRequestState.getInputParams()
                        .get(MessageProperty.conversationViewReq.name()).get(0);
            }

            boolean sortByUnreadFlag = inputParam
                    .containsKey(MailboxProperty.sortKey.name())
                    && inputParam.get(MailboxProperty.sortKey.name()).get(0)
                            .equals(MessageProperty.flagUnread.name());
            final short[] attrType;
            if (sortByUnreadFlag) {
                attrType = new short[2];
                attrType[0] = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
                attrType[1] = ReadSortedMsgs.ATTR_TYPE_MSG_FLAGS;
            } else {
                attrType = new short[1];
                attrType[0] = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                    .append("ReadSortedMsgs for host : ").append(host)
                    .append(", mailboxId: ").append(mailboxId)
                    .append(", realm: ").append(realm).append(", folder: ")
                    .append(folderUUID).append(", popDeletedFlag: ")
                    .append(popDeletedFlag).append(", sortKey: ")
                    .append(keyType).append(", sliceStart: ")
                    .append(sliceStart).append(", sliceEnd: ")
                    .append(sliceEnd).append(", sliceCount: ")
                    .append(sliceCount).append(", sliceReversed: ")
                    .append(sliceReversed).append(", attrType: ")
                    .append(Arrays.toString(attrType)).append(", conversationViewReq: ")
                    .append(conversationViewReq));
            }
            final ReadSortedMsgs readMsgs = new ReadSortedMsgs(host, mailboxId,
                    realm, folderUUID, keyType, sliceStart, sliceEnd,
                    sliceCount, sliceReversed, attrType,
                    Boolean.parseBoolean(conversationViewReq), popDeletedFlag);
            readMsgs.execute();
            final LinkedHashMap<Integer, MessageMetadata[]> sortedMessages = readMsgs
                    .getSortedMessages();
            if (logger.isDebugEnabled()) {
                logger.debug("sortedMessages Size: " + sortedMessages.size());
            }
            Map<String, Boolean> flagUnreadMap = new LinkedHashMap<String, Boolean>();
            MessageMetadata[] messageMetadata = null;
            String conversationIndex;
            final Iterator<Entry<Integer, MessageMetadata[]>> itr = sortedMessages
                    .entrySet().iterator();
            while (itr.hasNext()) {
                Entry<Integer, MessageMetadata[]> mapEntry = itr.next();
                conversationIndex = mapEntry.getKey().toString();
                if (logger.isDebugEnabled()) {
                    logger.debug("Conversation Index: " + conversationIndex);
                }
                messageMetadata = mapEntry.getValue();
                if (logger.isDebugEnabled()) {
                    logger.debug("MessageMetadata Size: "
                            + messageMetadata.length);
                }
                for (int i = 0; i < messageMetadata.length; i++) {
                    /**
                     * For each MessageMetadata for conversation create a
                     * metadata POJO and update the metadata map
                     **/
                    final Metadata metadata = this.populateMetaData(
                            messageMetadata[i], new Metadata(),
                            Boolean.parseBoolean(conversationViewReq));
                    if (metadata != null) {
                        String uuid = messageMetadata[i].getMessageUUID().toString();
                        if (sortByUnreadFlag) {
                            flagUnreadMap.put(uuid, metadata.getFlagUnread());
                        } else {
                            uuids.add(uuid);
                        }
                    }
                }
            }
            if (sortByUnreadFlag) {
                uuids.addAll(MssSlMetaCRUD.sortByFlagUnread(flagUnreadMap, sliceReversed != 0));
            }
        } catch (IntermailException e) {
            logger.warn("Error while reading messages meta data uids list: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while reading messages meta data uids list", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages meta data uids list", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages meta data uids list", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_GET_MESSAGE_METADATA_UID_LIST.name(), t);
        }
    }
    
    /**
     * Ad hoc solution for LEAPFROG-2768
     * Since MSS has not supported keyType=flagUnread, we have to sort the LinkedHashMap in Java
     * 
     * @param map a LinkedHashMap to be sorted by value 
     * @param reverse a boolean that indicates the sort order - 
     *                true: read mails first (flagUnread=false), false: unread mails (flagUnread=true) first
     * @return a list of UUIDs sorted by flagUnread
     */
    public static List<String> sortByFlagUnread(Map<String, Boolean> map, final boolean reverse) {
        List<String> result = new LinkedList<String>();
        List<Map.Entry<String, Boolean>> list = new LinkedList<Map.Entry<String, Boolean>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Boolean>>() {
            public int compare(Map.Entry<String, Boolean> o1, Map.Entry<String, Boolean> o2) {
                int r = o1.getValue().compareTo(o2.getValue());
                if (r != 0)
                    return reverse ? -r : r;
                else
                    return reverse ? -o1.getKey().compareTo(o2.getKey()) : o1.getKey().compareTo(o2.getKey());
            }
        });
        for (Map.Entry<String, Boolean> entry : list) {
            result.add(entry.getKey());
        }
        return result;
    }
    
    @Override
    public void readMessageMetaData(final MxOSRequestState mxosRequestState,
            final String folderName, Metadata metadata) throws MxOSException {

        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            short[] attrType = {};
            short keyType = GetMessageAttributes.KEYTYPE_MESSAGE_UUID;
            String messageId = mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0);
            String[] keys = { messageId };
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append(
                        "Reading messages from folder : ").append(folderUUID));
                logger.debug(new StringBuffer().append(
                        "Reading messages for messageId : ").append(messageId));
            }
            GetMessageAttributes readMsgs = new GetMessageAttributes(host,
                    mailboxId, realm, folderUUID, keyType, keys, attrType,
                    Boolean.parseBoolean(popDeletedFlag));
            readMsgs.execute(myDataModel);
            MessageMetadata[] msgMetadata = readMsgs.getMessageMetadata();
            if (msgMetadata.length > 0) {
                this.populateMetaData(msgMetadata[0], metadata, false);
            }
            logger.info("Message Metadata read successful.");
        } catch (IntermailException e) {
            logger.warn("Error while reading Messages From MSS: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        }
        return;
    }

    @Override
    public void updateMessageFlags(MxOSRequestState mxosRequestState,
            String[] msgUUIDs) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("updating Message Flags...");
        }
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
            UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            Flags flags = (Flags) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.messageFlags);
            MsgFlags modStatus = new MsgFlags();
            MsgFlags newFlags = new MsgFlags();
            if (null != flags.getFlagRecent()) {
                modStatus.SetFlag(MsgFlagIndex.flagrecent, true);
                newFlags.SetFlag(MsgFlagIndex.flagrecent, flags.getFlagRecent());
            }
            if (null != flags.getFlagSeen()) {
                modStatus.SetFlag(MsgFlagIndex.flagseen, true);
                newFlags.SetFlag(MsgFlagIndex.flagseen, flags.getFlagSeen());
            }
            if (null != flags.getFlagAns()) {
                modStatus.SetFlag(MsgFlagIndex.flagans, true);
                newFlags.SetFlag(MsgFlagIndex.flagans, flags.getFlagAns());
            }
            if (null != flags.getFlagFlagged()) {
                modStatus.SetFlag(MsgFlagIndex.flagflagged, true);
                newFlags.SetFlag(MsgFlagIndex.flagflagged,
                        flags.getFlagFlagged());
            }
            if (null != flags.getFlagDraft()) {
                modStatus.SetFlag(MsgFlagIndex.flagdraft, true);
                newFlags.SetFlag(MsgFlagIndex.flagdraft, flags.getFlagDraft());
            }
            if (null != flags.getFlagDel()) {
                modStatus.SetFlag(MsgFlagIndex.flagdel, true);
                newFlags.SetFlag(MsgFlagIndex.flagdel, flags.getFlagDel());
            }
            boolean setKeyword = true;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.deleteKeyword.name())) {
                setKeyword = Boolean.parseBoolean(mxosRequestState
                        .getInputParams().get(MessageProperty.keywords.name())
                        .get(0));
            }
            String keywords = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.keywords.name())) {
                modStatus.SetFlag(MsgFlagIndex.keywords, true);
                newFlags.SetFlag(MsgFlagIndex.keywords, setKeyword);
                keywords = mxosRequestState.getInputParams()
                        .get(MessageProperty.keywords.name()).get(0);
            }
            
            MsgFlagChangeInfo[] msgChangeInfo = new MsgFlagChangeInfo[msgUUIDs.length];
            
            UUID[] uuids = new UUID[msgUUIDs.length];
            for (int i = 0; i < msgUUIDs.length; i++) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Updating message metadata with \tmsgUUID : "
                            + msgUUIDs[i]);
                }
                msgChangeInfo[i] = new MsgFlagChangeInfo();
                uuids[i] = UUID.fromString(msgUUIDs[i]);
                msgChangeInfo[i].setMsgUUID(uuids[i]);
                msgChangeInfo[i].setModifiedFlags(modStatus.getFlagInt());
                msgChangeInfo[i].setNewFlagValues(newFlags.getFlagInt());
                msgChangeInfo[i].setKeywords(keywords);
            }
            
            int options = 0;
            String disableNotificationFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.disableNotificationFlag.name())) {
                disableNotificationFlag = mxosRequestState
                        .getInputParams()
                        .get(MessageProperty.disableNotificationFlag.name())
                        .get(0);

            } else {
                disableNotificationFlag = System.getProperty(
                        SystemProperty.disableNotificationFlag.name(),
                        "true");
            }

            if (Boolean.parseBoolean(disableNotificationFlag)) {
                options = options + StoreMsgFlags.DISABLE_NOTIFICATION;
            }

            int accessId = getAccessId(info.getIsAdmin());

            if ((modStatus.getFlagInt() != 0) || (options != 0)) {
                StoreMsgFlags op3 = new StoreMsgFlags(host, mailboxId, realm,
                        folderUUID, msgChangeInfo, options, accessId);
                op3.execute(myDataModel);
                if (logger.isDebugEnabled()) {
                    logger.debug("updateMessageFlags(StoreMsgFlags): Success ");
                }
            }

            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                final String popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
                if (Boolean.parseBoolean(popDeletedFlag)) {
                    RemoveMultiMsgs op3 = new RemoveMultiMsgs(host, mailboxId,
                            realm, folderUUID, uuids,
                            StoreMsgFlags.SPECIAL_DELETED_FLAG);
                    op3.execute(myDataModel);
                }
                if (logger.isDebugEnabled()) {
                    logger.debug("updateMessageFlags(RemoveMultiMsgs): Success ");
                }
            }
            
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while updating message flags.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while updating message flags.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), t);
        }
    }

    @Override
    public void searchSortedMetadatasInFolder(
            MxOSRequestState mxosRequestState, String folderName,
            Map<String, Map<String, Metadata>> searchMetadata)
            throws MxOSException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Entered.");
            }
            /** getting MssInfo **/
            final MssLinkInfo info = (MssLinkInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            final SearchTerm searchTerm = (SearchTerm) mxosRequestState
                    .getAdditionalParams().getPropertyAsObject(
                            MessageProperty.searchquery);
            final Map<String, List<String>> inputParam = mxosRequestState
                    .getInputParams();

            /** shortKey **/
            short keyType = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                final String sortKey = inputParam.get(
                        MailboxProperty.sortKey.name()).get(0);
                for (MessageProperty prop : MessageProperty.values()) {
                    if (prop.name().equals(sortKey)) {
                        if (READSORTEDMSGS_KEY_MAP.containsKey(prop)) {
                            keyType = READSORTEDMSGS_KEY_MAP.get(prop);
                        }
                    }
                }
            }
            
            keyType = ReadSortedMsgs.getKeyType(inputParam,keyType);

            /** getting optional parameters **/
            String sliceStart = null;
            if (inputParam.containsKey(MailboxProperty.sliceStart.name())) {
                if (inputParam.get(MailboxProperty.sliceStart.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceStart.name()).get(0)))) {
                    sliceStart = inputParam.get(
                            MailboxProperty.sliceStart.name()).get(0);
                }
            }

            String sliceEnd = null;
            if (inputParam.containsKey(MailboxProperty.sliceEnd.name())) {
                if (inputParam.get(MailboxProperty.sliceEnd.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceEnd.name()).get(0)))) {
                    sliceEnd = inputParam.get(MailboxProperty.sliceEnd.name())
                            .get(0);
                }
            }

            int sliceCount = Integer.MAX_VALUE;
            /*
             * ReadSortedMsgs is also used for both "list" and "search" APIs.
             * Only for "search" a sliceCount is needed, for "list" providing
             * entire data-list.
             */
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                String sliceCountStr;
                if (inputParam.containsKey(MailboxProperty.sliceCount.name())) {
                    sliceCountStr = inputParam.get(
                            MailboxProperty.sliceCount.name()).get(0);
                } else {
                    sliceCountStr = System.getProperty(
                            SystemProperty.sortedMsgMetadataListCountDefault
                                    .name(), "10");
                }
                sliceCount = Integer.parseInt(sliceCountStr);
                if (sliceCount < 0) {
                    sliceCount = Integer.MAX_VALUE;
                }
            }

            SortOrder sortOrder = SortOrder.ascending;
            String sortOrderStr = sortOrder.name();
            if (inputParam.containsKey(MailboxProperty.sortOrder.name())) {
                sortOrderStr = inputParam.get(MailboxProperty.sortOrder.name())
                        .get(0);
            } else {
                sortOrderStr = System.getProperty(
                        SystemProperty.msgMetadataSortingOrderDefault.name(),
                        "0");
            }
            for (SortOrder so : SortOrder.values()) {
                if (so.name().equals(sortOrderStr)) {
                    sortOrder = so;
                }
            }
            final short sliceReversed = sortOrder.getOrderValue();

            String conversationViewReq = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.conversationViewReq.name())) {
                conversationViewReq = mxosRequestState.getInputParams()
                        .get(MessageProperty.conversationViewReq.name()).get(0);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("ReadSortedMsgs for host : ").append(host)
                        .append(", mailboxId: ").append(mailboxId)
                        .append(", realm: ").append(realm).append(", folder: ")
                        .append(folderUUID).append(", sortKey: ")
                        .append(keyType).append(", sliceStart: ")
                        .append(sliceStart).append(", sliceEnd: ")
                        .append(sliceEnd).append(", sliceCount: ")
                        .append(sliceCount).append(", sliceReversed: ")
                        .append(sliceReversed)
                        .append(", conversationViewReq: ")
                        .append(conversationViewReq));
            }
            
            String strLimit = null;
            int limit = 0;
            if (inputParam.containsKey(MailboxProperty.sliceCount.name())) {
                strLimit = inputParam.get(MailboxProperty.sliceCount.name())
                        .get(0);
                limit = Integer.valueOf(strLimit);
            }
                        
            if (keyType == ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID
                    && limit > 1 && searchTerm != null
                    && conversationViewReq == null) {
                searchLimitSortedMetadatasInFolder(mxosRequestState,
                        folderName, searchMetadata);
                return;
            }

            final ReadSortedMsgs readMsgs = new ReadSortedMsgs(host, mailboxId,
                    realm, folderUUID, keyType, sliceStart, sliceEnd,
                    sliceCount, sliceReversed, null,
                    Boolean.parseBoolean(conversationViewReq), true);
            readMsgs.execute();
            final LinkedHashMap<Integer, MessageMetadata[]> sortedMessages = readMsgs
                    .getSortedMessages();
            if (logger.isDebugEnabled()) {
                logger.debug("sortedMessages Size: " + sortedMessages.size());
            }
            
            List<MetadataForSort> metadataList = null;

            boolean sortByUnreadFlag = inputParam
                    .containsKey(MailboxProperty.sortKey.name())
                    && inputParam.get(MailboxProperty.sortKey.name()).get(0)
                            .equals(MessageProperty.flagUnread.name())
                    && conversationViewReq == null;
            if (sortByUnreadFlag) {
                metadataList = new ArrayList<MetadataForSort>();
            }
            
            MessageMetadata[] messageMetadata = null;
            String conversationIndex;
            final Iterator<Entry<Integer, MessageMetadata[]>> itr = sortedMessages
                    .entrySet().iterator();
            String[] stopWords = MxOSConfig.getStopWords();
            boolean isSearchBody = searchTerm != null && searchTerm.isContainBodySearch(stopWords);
            while (itr.hasNext()) {
                Entry<Integer, MessageMetadata[]> mapEntry = itr.next();
                conversationIndex = mapEntry.getKey().toString();
                if (logger.isDebugEnabled()) {
                    logger.debug("Conversation Index: " + conversationIndex);
                }
                messageMetadata = mapEntry.getValue();
                if (logger.isDebugEnabled()) {
                    logger.debug("MessageMetadata Size: "
                            + messageMetadata.length);
                }
                LinkedHashMap<String, Metadata> metadataMap = new LinkedHashMap<String, Metadata>();
                Message message = null;
                for (int i = 0; i < messageMetadata.length; i++) {
                    /**
                     * For each MessageMetadata for conversation create a
                     * metadata POJO and update the metadata map
                     **/
                    message = new Message();
                    final Metadata metadata = this.populateMetaData(
                            messageMetadata[i], new Metadata(),
                            Boolean.parseBoolean(conversationViewReq));
                    message.setMetadata(metadata);
                    if (isSearchBody){
                        //get the message body
                        UUID msgUUID = messageMetadata[i].getMessageUUID();
                        GetMsgStream getMsgStream = new GetMsgStream(host, mailboxId,
                                realm, folderUUID, msgUUID, GetMsgStream.FETCH_MSG_BODY);
                        getMsgStream.execute(myDataModel);
                        String text = null;
                        text = new String(getMsgStream.getMsgBytes());
                        Body body = new Body();
                        if ((text != null) && (text.length() != 0)) {
                            body.setMessageBlob(text);
                        }
                        message.setBody(body);
                    }
                    if (searchTerm == null || searchTerm.match(message)) {
                        metadataMap.put(messageMetadata[i].getMessageUUID()
                                .toString(), metadata);
                        if(sortByUnreadFlag){
                            MetadataForSort mfs = new MetadataForSort(conversationIndex,messageMetadata[i].getMessageUUID()
                                    .toString(),metadata);
                            metadataList.add(mfs);
                        }
                    }
                    message = null;
                    // Update search metadata map
                    if (metadataMap != null && metadataMap.size() > 0 && !sortByUnreadFlag) {
                        searchMetadata.put(conversationIndex, metadataMap);
                    }
                }
            }
            if (sortByUnreadFlag) {
                Collections.sort(metadataList,
                        new Comparator<MetadataForSort>() {

                            @Override
                            public int compare(MetadataForSort o1,
                                    MetadataForSort o2) {
                                int r = o1
                                        .getMetadata()
                                        .getFlagUnread()
                                        .compareTo(
                                                o2.getMetadata()
                                                        .getFlagUnread());
                                

                                if (r != 0) {
                                    return sliceReversed == 0 ? r : -r;
                                } else {
                                    return sliceReversed == 0 ? o1.getMsguuid()
                                            .compareTo(o2.getMsguuid()) : -o1
                                            .getMsguuid().compareTo(
                                                    o2.getMsguuid());

                                }

                            }

                        });

                int messageIndex = 0;
                for (MetadataForSort mfs : metadataList) {
                    LinkedHashMap<String, Metadata> metadata = new LinkedHashMap<String, Metadata>();
                    metadata.put(mfs.getMsguuid(), mfs.getMetadata());
                    searchMetadata.put(String.valueOf(messageIndex), metadata);
                    messageIndex++;
                }
            }
        } catch (IntermailException e) {
            logger.warn("Error while searching given folder: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while searching given folder.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while searching given folder.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }
    
    
    
    private void searchLimitSortedMetadatasInFolder(
            MxOSRequestState mxosRequestState, String folderName,
            Map<String, Map<String, Metadata>> searchMetadata)
            throws MxOSException {
        try {
            if (logger.isDebugEnabled()) {
                logger.debug("Entered.");
            }
            /** getting MssInfo **/
            final MssLinkInfo info = (MssLinkInfo) mxosRequestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);

            final SearchTerm searchTerm = (SearchTerm) mxosRequestState
                    .getAdditionalParams().getPropertyAsObject(
                            MessageProperty.searchquery);
            final Map<String, List<String>> inputParam = mxosRequestState
                    .getInputParams();

            /** shortKey **/
            short keyType = ReadSortedMsgs.SORTED_BASED_ON_MESSAGE_UUID;
            if (inputParam.containsKey(MailboxProperty.sortKey.name())) {
                final String sortKey = inputParam.get(
                        MailboxProperty.sortKey.name()).get(0);
                for (MessageProperty prop : MessageProperty.values()) {
                    if (prop.name().equals(sortKey)) {
                        if (READSORTEDMSGS_KEY_MAP.containsKey(prop)) {
                            keyType = READSORTEDMSGS_KEY_MAP.get(prop);
                        }
                    }
                }
            }

            /** getting optional parameters **/
            String sliceStart = null;
            if (inputParam.containsKey(MailboxProperty.sliceStart.name())) {
                if (inputParam.get(MailboxProperty.sliceStart.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceStart.name()).get(0)))) {
                    sliceStart = inputParam.get(
                            MailboxProperty.sliceStart.name()).get(0);
                }
            }

            String sliceEnd = null;
            if (inputParam.containsKey(MailboxProperty.sliceEnd.name())) {
                if (inputParam.get(MailboxProperty.sliceEnd.name()).get(0) != null
                        && !("".equals(inputParam.get(
                                MailboxProperty.sliceEnd.name()).get(0)))) {
                    sliceEnd = inputParam.get(MailboxProperty.sliceEnd.name())
                            .get(0);
                }
            }

            int sliceCount = 0;
            String strDefaultSliceCount = System.getProperty(
                    SystemProperty.sortedMsgMetadataListCountDefault.name(),
                    "1000");

            int defaultSliceCount = Integer.parseInt(strDefaultSliceCount);

            String sliceCountStr = inputParam.get(
                    MailboxProperty.sliceCount.name()).get(0);
            sliceCount = Integer.parseInt(sliceCountStr);

            if (sliceCount > defaultSliceCount) {
                sliceCount = defaultSliceCount;
            }
            

            SortOrder sortOrder = SortOrder.ascending;
            String sortOrderStr = sortOrder.name();
            if (inputParam.containsKey(MailboxProperty.sortOrder.name())) {
                sortOrderStr = inputParam.get(MailboxProperty.sortOrder.name())
                        .get(0);
            } else {
                sortOrderStr = System.getProperty(
                        SystemProperty.msgMetadataSortingOrderDefault.name(),
                        "0");
            }
            for (SortOrder so : SortOrder.values()) {
                if (so.name().equals(sortOrderStr)) {
                    sortOrder = so;
                }
            }
            final short sliceReversed = sortOrder.getOrderValue();

            String conversationViewReq = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.conversationViewReq.name())) {
                conversationViewReq = mxosRequestState.getInputParams()
                        .get(MessageProperty.conversationViewReq.name()).get(0);
            }

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer()
                        .append("ReadSortedMsgs for host : ").append(host)
                        .append(", mailboxId: ").append(mailboxId)
                        .append(", realm: ").append(realm).append(", folder: ")
                        .append(folderUUID).append(", sortKey: ")
                        .append(keyType).append(", sliceStart: ")
                        .append(sliceStart).append(", sliceEnd: ")
                        .append(sliceEnd).append(", sliceCount: ")
                        .append(sliceCount).append(", sliceReversed: ")
                        .append(sliceReversed)
                        .append(", conversationViewReq: ")
                        .append(conversationViewReq));
            }
            
            String strLimit = null;
            int resultLimit = 0;
            if (inputParam.containsKey(MailboxProperty.sliceCount.name())) {
                strLimit = inputParam.get(MailboxProperty.sliceCount.name())
                        .get(0);
                resultLimit = Integer.valueOf(strLimit);
            }
                 
            LinkedHashMap<Integer, MessageMetadata[]> sortedMessages = null;
            int messageIndex = 0;
            List<String> msguuid = new ArrayList<String>();
            do{
                final ReadSortedMsgs readMsgs = new ReadSortedMsgs(host, mailboxId,
                        realm, folderUUID, keyType, sliceStart, sliceEnd,
                        sliceCount, sliceReversed, null,
                        Boolean.parseBoolean(conversationViewReq), true);
                readMsgs.execute();
                sortedMessages = readMsgs
                        .getSortedMessages();
               
                int sortedMsgSize = sortedMessages.size();
                if (logger.isDebugEnabled()) {
                    logger.debug("sortedMessages Size: " + sortedMessages.size());
                }
               
                MessageMetadata[] messageMetadata = null;
                String conversationIndex;
                String nextSliceStart = null;
                String[] stopWords = MxOSConfig.getStopWords();
                boolean isSearchBody = searchTerm != null && searchTerm.isContainBodySearch(stopWords);
                final Iterator<Entry<Integer, MessageMetadata[]>> itr = sortedMessages
                        .entrySet().iterator();
                while (itr.hasNext()) {
                    Entry<Integer, MessageMetadata[]> mapEntry = itr.next();
                    conversationIndex = mapEntry.getKey().toString();
                    if (logger.isDebugEnabled()) {
                        logger.debug("Conversation Index: " + conversationIndex);
                    }
                    messageMetadata = mapEntry.getValue();
                    if (logger.isDebugEnabled()) {
                        logger.debug("MessageMetadata Size: "
                                + messageMetadata.length);
                    }
                    LinkedHashMap<String, Metadata> metadataMap = new LinkedHashMap<String, Metadata>();
                    Message message = null;
                    for (int i = 0; i < messageMetadata.length; i++) {

                        /*
                         * if mOS invoke MSS twice and more, there would be a
                         * message overlap in searchMetadata so we will not put
                         * the same message in searchMetadata.
                         */
                        
                        if (msguuid.contains(messageMetadata[i]
                                .getMessageUUID().toString())) {
                            continue;
                        }

                       
                        /**
                         * For each MessageMetadata for conversation create a
                         * metadata POJO and update the metadata map
                         **/
                        final Metadata metadata = this.populateMetaData(
                                messageMetadata[i], new Metadata(),
                                Boolean.parseBoolean(conversationViewReq));
                        message = new Message();
                        message.setMetadata(metadata);
                        if (isSearchBody){
                            //get the message body
                            UUID msgUUID = messageMetadata[i].getMessageUUID();
                            GetMsgStream getMsgStream = new GetMsgStream(host, mailboxId,
                                    realm, folderUUID, msgUUID, GetMsgStream.FETCH_MSG_BODY);
                            getMsgStream.execute(myDataModel);
                            String text = null;
                            text = new String(getMsgStream.getMsgBytes());
                            Body body = new Body();
                            if ((text != null) && (text.length() != 0)) {
                                body.setMessageBlob(text);
                            }
                            message.setBody(body);
                        }
                                               
                        if (searchTerm != null && searchTerm.match(message) && resultLimit > 0) {
                            metadataMap.put(messageMetadata[i].getMessageUUID()
                                    .toString(), metadata);
                            msguuid.add(messageMetadata[i].getMessageUUID()
                                    .toString());
                            resultLimit--;
                        }
                        nextSliceStart = messageMetadata[i].getMessageUUID()
                                .toString();
                    }
                    // Update search metadata map
                    if (metadataMap != null && metadataMap.size() > 0) {
                        searchMetadata.put(String.valueOf(messageIndex), metadataMap);
                        messageIndex++;
                    }
                   
                }
               

                if (sortedMsgSize < sliceCount) {
                    break;
                }
               
                if (resultLimit == 0) {
                    break;
                }
               
                sliceStart = nextSliceStart;
               
            }while(true);
            
        } catch (IntermailException e) {
            logger.warn("Error while searching given folder: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while searching given folder.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } catch (Throwable t) {
            logger.error("Error while searching given folder.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_SEARCH.name(), t);
        }
    }


    class MetadataForSort {
        private String conversationIndex;
        private String msguuid;
        private Metadata metadata;

        public MetadataForSort(String conversationIndex, String msguuid,
                Metadata metadata) {
            this.conversationIndex = conversationIndex;
            this.msguuid = msguuid;
            this.metadata = metadata;
        }

        /**
         * @return the conversationIndex
         */
        public String getConversationIndex() {
            return conversationIndex;
        }

        /**
         * @return the msguuid
         */
        public String getMsguuid() {
            return msguuid;
        }

        /**
         * @return the metadata
         */
        public Metadata getMetadata() {
            return metadata;
        }

        @Override
        public String toString() {
            return msguuid + ":" + this.metadata.getFlagUnread();
        }
    }

    @Override
    public void listMessageUUIDs(MxOSRequestState mxosRequestState)
            throws MxOSException, IntermailException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        UUID folderUUID = null;
        folderUUID = ActionUtils.getFolderUUID(mxosRequestState, folderName);
        String popDeletedFlag = null;
        if (mxosRequestState.getInputParams().containsKey(
                MessageProperty.popDeletedFlag.name())) {
            popDeletedFlag = mxosRequestState.getInputParams()
                    .get(MessageProperty.popDeletedFlag.name()).get(0);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("folderName:" + folderName + " folder UID:"
                    + folderUUID);
        }
        ImapListUids op1 = new ImapListUids(host, mailboxId, realm, folderUUID,
                Boolean.parseBoolean(popDeletedFlag));
        op1.execute(myDataModel);
        if (null != op1.getMsgUUids()) {
            String[] keys = new String[op1.getMsgUUids().length];
            for (int i = 0; i < op1.getMsgUUids().length; i++) {
                keys[i] = op1.getMsgUUids()[i].toString();
            }
            mxosRequestState.getDbPojoMap().setProperty(
                    MxOSPOJOs.imapMessageUids, keys);
        }
    }

    /**
     * Method to populate MetaData POJO using Msg object returned from RME.
     * 
     * @param msg message from RME
     * @param metaData metaData to be populated
     * @param isFolderUUIDReq specifies whether folderUUID is required in
     *            metaData
     * @return Message POJO
     * @throws InvalidRequestException
     * @throws Exception
     */
    protected Metadata populateMetaData(MessageMetadata msg, Metadata metaData,
            boolean isFolderUUIDReq) throws InvalidRequestException {
        Flags flags = null;
        try {
            Iterator<AttributeId> keysItr = msg.getAttributeKeys().iterator();
            while (keysItr.hasNext()) {
                AttributeId attrId = keysItr.next();
                if (attrId == null) {
                    if (logger.isDebugEnabled()) {
                        logger.debug("AttributeId == null!");
                    }
                    break;
                }
                switch (attrId) {
                case attr_deliveredNDR:
                    metaData.setDeliverNDR(msg.getDeliveredNDR() == true ? 1
                            : 0);
                    break;
                case attr_hasAttachment:
                    metaData.setHasAttachments(msg.getHasAttachment());
                    break;
                case attr_arrivalTime:
                    metaData.setArrivalTime(msg.getArrivalTime());
                    break;
                case attr_expirationTime:
                    metaData.setExpireOn(msg.getExpirationTime());
                    break;
                case attr_lastAccessedTime:
                    metaData.setLastAccessedTime(msg.getLastAccessedTime());
                    break;
                case attr_size:
                    metaData.setSize(new Long(msg.getSize()));
                    break;
                case attr_uid:
                    metaData.setUid(new Long(msg.getUid()));
                    break;
                case attr_folderUUID:
                    if (isFolderUUIDReq) {
                        metaData.setFolderId(msg.getFolderUUID());
                    }
                    break;
                case attr_msgFlags:
                    flags = this.getFlagsFromMessage(msg.getMessageFlags());
                    break;
                case attr_specialDeleted:
                    metaData.setPopDeletedFlag(MxosEnums.TrueOrFalse
                            .fromValue(msg.getPopDeleted().toString()));
                    break;
                case attr_keywords:
                    metaData.setKeywords(Arrays.asList(msg.getKeywords()));
                    break;
                case attr_type:
                    metaData.setType(msg.getMsgType());
                    break;
                case attr_priority:
                    final String priority = msg.getMsgPriority();
                    if (priority != null && !priority.equals("")) {
                        metaData.setPriority(msg.getMsgPriority());
                    }
                    break;
                case attr_subject:
                    metaData.setSubject(msg.getSubject());
                    break;
                case attr_from:
                    metaData.setFrom(msg.getFrom());
                    break;
                case attr_to:
                    metaData.setTo(Arrays.asList(msg.getTo()));
                    break;
                case attr_cc:
                    metaData.setCc(Arrays.asList(msg.getCc()));
                    break;
                case attr_bcc:
                    metaData.setBcc(Arrays.asList(msg.getBcc()));
                    break;
                case attr_replyTo:
                    metaData.setReplyTo(Arrays.asList(msg.getReplyTo()));
                    break;
                case attr_inReplyTo:
                    metaData.setInReplyTo(msg.getInReplyTo());
                    break;
                case attr_blobMessageId:
                    metaData.setBlobMessageId(msg.getBlobMessageId());
                    break;
                case attr_date:
                    metaData.setSentDate(msg.getSentDate());
                    break;
                case attr_references:
                    metaData.setReferences(msg.getReferences());
                    break;
                case attr_bounce:
                    metaData.setFlagBounce(msg.getBounce());
                    break;
                case attr_private:
                    metaData.setFlagPriv(msg.getPrivate());
                    break;
                case attr_richMailFlag:
                    metaData.setFlagRichMail(msg.getRichMailFlag());
                    break;
                case attr_conversation:
                    metaData.setThreadId(msg.getConversationId());
                    break;
                case attr_oldmsgid:
                    metaData.setOldMsgId(msg.getOldMsgId());
                    break;
                default:
                    break;
                }
            }
        } catch (NumberFormatException nfe) {
            logger.warn("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_DATA.name(), nfe);
        } catch (Exception e) {
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_DATA.name(), e);
        }
        if (null != flags) {
            metaData.setFlagRecent(flags.getFlagRecent());
            metaData.setFlagAns(flags.getFlagAns());
            metaData.setFlagDel(flags.getFlagDel());
            metaData.setFlagDraft(flags.getFlagDraft());
            metaData.setFlagFlagged(flags.getFlagFlagged());
            metaData.setFlagSeen(flags.getFlagSeen());
            metaData.setFlagUnread(!flags.getFlagSeen());
        }
        return metaData;
    }

    /**
     * Retrieve the flags from the flag locations.
     * 
     * @param msgFlags
     * @return
     */
    protected Flags getFlagsFromMessage(boolean[] msgFlags) {
        Flags flags = new Flags();
        // Setting message flags
        flags.setFlagRecent(msgFlags[Msg.MssFlagOrder.flagrecent.ordinal()]);
        flags.setFlagSeen(msgFlags[Msg.MssFlagOrder.flagseen.ordinal()]);
        flags.setFlagAns(msgFlags[Msg.MssFlagOrder.flagans.ordinal()]);
        flags.setFlagFlagged(msgFlags[Msg.MssFlagOrder.flagflagged.ordinal()]);
        flags.setFlagDraft(msgFlags[Msg.MssFlagOrder.flagdraft.ordinal()]);
        flags.setFlagDel(msgFlags[Msg.MssFlagOrder.flagdel.ordinal()]);
        return flags;
    }
    
    @Override
    public void updateFolder(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        
        final String folderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.folderName.name(), mxosRequestState);

        String newfolderName = MessageServiceHelper.retrieveFolderName(
                FolderProperty.toFolderName.name(), mxosRequestState);
        
        if (logger.isDebugEnabled()) {
            logger.debug("folderName:" + folderName + " newfolderName:"
                    + newfolderName);
        }
        int uidValidity = -1;
        if (mxosRequestState.getDbPojoMap().getProperty(
                FolderProperty.uidValidity.name()) != null) {
            uidValidity = Integer.parseInt(mxosRequestState.getDbPojoMap()
                    .getProperty(FolderProperty.uidValidity.name()));
        }
        if (!folderName.equals(newfolderName)) {
            if (("".equals(folderName) && ("".equals(newfolderName)))
                    || folderName.equals(MxOSConstants.FORWARD_SLASH)
                    || newfolderName.equals(MxOSConstants.FORWARD_SLASH)
                    || MxOSConstants.INBOX.equalsIgnoreCase(folderName)
                    || systemfolders.contains(folderName)) {
                logger.warn(folderName + " is not allowed to update");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(),
                        folderName + " is not allowed for update to "
                                + newfolderName);
            }
        }
        try {
            if (ActionUtils.getFolderUUID(mxosRequestState, newfolderName) != null) {
                if ((ActionUtils.getFolderUUID(mxosRequestState, newfolderName))
                        .equals(ActionUtils.getFolderUUID(mxosRequestState,
                                folderName))
                        && (mxosRequestState.getInputParams().get(
                                FolderProperty.uidValidity.name()) != null)) {
                    logger.info("do nothing continue with updation of folder uidValidity.");
                } else {
                    logger.warn("Folder already exists");
                    throw new InvalidRequestException(
                            FolderError.FLD_ALREADY_EXISTS.name());
                }
            }
        } catch (NotFoundException nfe) {
            logger.warn("Continue to update folder");
        }
        UUID parentFolderUUID = ActionUtils.getParentFolderUUID(
                mxosRequestState, folderName);
        UUID newParentFolderUUID = ActionUtils.getParentFolderUUID(
                mxosRequestState, newfolderName);
        UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                folderName);
        boolean isMoveOperation = false;
        if (!parentFolderUUID.equals(newParentFolderUUID)) {
            isMoveOperation = true;
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("new Folder name :")
                    .append(newfolderName).append(" folderUUID :")
                    .append(folderUUID).append(" parentFolderUUID :")
                    .append(parentFolderUUID).append(" uidValidity :")
                    .append(uidValidity));
        }
        if (newParentFolderUUID != null
                && !newParentFolderUUID.toString().equals(
                        MxOSConstants.ROOT_FOLDER_UUID)) {
            newfolderName = newfolderName.substring(newfolderName
                    .lastIndexOf(MxOSConstants.FORWARD_SLASH) + 1);
        }
        if (newfolderName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            newfolderName = newfolderName.substring(newfolderName
                    .indexOf(MxOSConstants.FORWARD_SLASH) + 1);
        }
        RenameFolder renameFolder = null;
        try {
            if (newParentFolderUUID != null && !"".equals(newfolderName)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("Updating folder: " + folderName
                            + " to folder: " + newfolderName);
                }
                renameFolder = new RenameFolder(host, mailboxId, realm,
                        folderUUID, newfolderName, newParentFolderUUID,
                        isMoveOperation, uidValidity);
            } else {
                logger.warn(newfolderName + " is not allowed to update");
                throw new InvalidRequestException(
                        FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(),
                        folderName + " is not allowed for updation");
            }
            renameFolder.execute(myDataModel);
        } catch (IntermailException e) {
            logger.warn("Error while updating folder: " + e.getFormattedString());
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Exception e) {
            logger.error("Error while update folder.", e);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while update folder.", t);
            throw new MxOSException(
                    FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), t);
        }
    }

    @Override
    public void updateFolderSubscribed(MxOSRequestState mxosRequestState)
            throws MxOSException {
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        
        String folderName = MessageServiceHelper.retrieveFolderName(FolderProperty.toFolderName.name(),
                mxosRequestState);
        
        if (folderName != null) {
            readMailboxMetaInfo(mxosRequestState);
        }
        else {
            folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
        }

        String folderSubscribed = null;
        if (mxosRequestState.getInputParams().get(
                FolderProperty.folderSubscribed.name()) != null) {
            folderSubscribed = mxosRequestState.getInputParams()
                    .get(FolderProperty.folderSubscribed.name()).get(0);
        }
        SubscribeFolder subscribeFolder = null;
        UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                folderName);
        if (logger.isDebugEnabled()) {
            logger.info("value of folderName is :" + folderName
                    + " folderSubscribed: " + folderSubscribed
                    + " folderUUID: " + folderUUID);
        }
        if (folderUUID != null && folderSubscribed != null) {
            subscribeFolder = new SubscribeFolder(host, mailboxId, realm,
                    folderUUID, Boolean.valueOf(folderSubscribed));
            try {
                subscribeFolder.execute(myDataModel);
            } catch (IntermailException e) {
                logger.warn("Error while updating folder: " + e.getFormattedString());
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(),
                                e);
            } catch (Exception e) {
                logger.error("Error while update folder.", e);
                throw new MxOSException(
                        FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), e);
            } catch (Throwable t) {
                logger.error("Error while update folder.", t);
                throw new MxOSException(
                        FolderError.FLD_UNABLE_TO_PERFORM_UPDATE.name(), t);
            }
        }
    }

    @Override
    public void readMultiMessageMetaData(MxOSRequestState mxosRequestState,
            String folderName, Map<String, Metadata> metaMap)
            throws MxOSException {
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            short[] attrType = {};
            short keyType = GetMessageAttributes.KEYTYPE_MESSAGE_UUID;

            final List<String> msgIds = mxosRequestState.getInputParams().get(
                    MessageProperty.messageId.name());
            final String[] keys = msgIds.toArray(new String[msgIds.size()]);
            String popDeletedFlag = null;
            if (mxosRequestState.getInputParams().containsKey(
                    MessageProperty.popDeletedFlag.name())) {
                popDeletedFlag = mxosRequestState.getInputParams()
                        .get(MessageProperty.popDeletedFlag.name()).get(0);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append(
                        "Reading messages from folder : ").append(folderUUID));
                logger.debug(new StringBuffer().append(
                        "Reading messages for messageId : ").append(msgIds));
            }
            GetMessageAttributes readMsgs = new GetMessageAttributes(host,
                    mailboxId, realm, folderUUID, keyType, keys, attrType,
                    Boolean.parseBoolean(popDeletedFlag));
            readMsgs.execute(myDataModel);
            MessageMetadata[] msgMetadata = readMsgs.getMessageMetadata();
            for (int i = 0; i < msgMetadata.length; i++) {
                // For each message from RME create a message POJO and update
                // the messages list
                metaMap.put(msgMetadata[i].getMessageUUID().toString(),
                        this.populateMetaData(msgMetadata[i], new Metadata(),
                                false));
            }
            logger.info("Message Metadata Multi Fetch successful with size: "
                    + msgMetadata.length);
        } catch (IntermailException e) {
            logger.warn("Error while reading Messages From MSS: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            ExceptionUtils.createApplicationExceptionFromIntermailException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (Exception e) {
            logger.error("Error while reading messages from folder", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading messages from folder", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
        return;
    }

    @Override
    public void readMessageHeader(MxOSRequestState mxosRequestState,
            Header header) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Read message header method in MSS SL: starting");
        }
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            UUID msgUUID = UUID.fromString(mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0));
            GetMsgStream getMsgStream = new GetMsgStream(host, mailboxId,
                    realm, folderUUID, msgUUID,
                    GetMsgStream.FETCH_MSG_HEADER_SUMMARY);
            getMsgStream.execute(myDataModel);
            String text = new String(getMsgStream.getMsgBytes());
            if ((text != null) && (text.length() != 0)) {
                header.setHeaderBlob(text);
            }
            mxosRequestState.getDbPojoMap().setProperty(
                    MxOSPOJOs.headerSummary, header);
        } catch (IntermailException e) {
            logger.warn("Error while reading message header: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("FioReadFail")) {
                throw new ApplicationException(
                        MessageError.MSG_NOT_FOUND.name(), e.getMessage());
            } else {
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                                e);
            }
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while reading message header.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading message header.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
    }

    @Override
    public void readBlob(MxOSRequestState mxosRequestState, Body body)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Read message body method in MSS SL: starting");
        }
        try {
            MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            String host = getMssHost(info.getMessageStoreHosts());
            String mailboxId = info.getMailboxId();
            String realm = info.getRealm();
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer().append("Host : ").append(host));
                logger.debug(new StringBuffer().append("MailboxId : ").append(
                        mailboxId));
                logger.debug(new StringBuffer().append("Realm : ")
                        .append(realm));
            }
            final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);
            final UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                    folderName);
            UUID msgUUID = UUID.fromString(mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0));
            GetMsgStream getMsgStream = new GetMsgStream(host, mailboxId,
                    realm, folderUUID, msgUUID, GetMsgStream.FETCH_MSG_BODY);
            getMsgStream.execute(myDataModel);
            String text = null;
            if (MxOSConfig.isMsgBodyBlobBase64Encoded()) {
                Base64 base64 = new Base64();
                text = new String(base64.encode(getMsgStream.getMsgBytes()));
            } else {
                text = new String(getMsgStream.getMsgBytes());
            }
            if ((text != null) && (text.length() != 0)) {
                body.setMessageBlob(text);
            }
            mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.messageBody,
                    body);
        } catch (IntermailException e) {
            logger.warn("Error while reading message body: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("FioReadFail")) {
                throw new ApplicationException(
                        MessageError.MSG_NOT_FOUND.name(), e.getMessage());
            } else {
                ExceptionUtils
                        .createApplicationExceptionFromIntermailException(
                                MessageError.MSG_UNABLE_TO_PERFORM_GET.name(),
                                e);
            }
        } catch (NotFoundException e) {
            logger.error("Error while searching given folder.", e);
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name(), e);
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while reading message body.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } catch (Throwable t) {
            logger.error("Error while reading message body.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), t);
        }
    }

    public PopListInfo getPopListInfo(String host, String mailboxId,
            String realm, UUID folderUUID, boolean popDeletedFlag)
            throws IntermailException {
        try {
            PopList popList = new PopList(host, mailboxId, realm, folderUUID,
                    popDeletedFlag);
            popList.execute(myDataModel);
            PopListInfo popListInfo = popList.getPopListInfo();
            return popListInfo;
        } catch (IntermailException e) {
            logger.warn("Error while reading pop list info: " + e.getFormattedString());
            throw e;
        }
    }
    
    public MessageMetadata[] getMessageAttributes(String host,
            String mailboxId, String realm, UUID folderUUID, short keyType,
            String[] keys, short[] attrType, boolean popDeletedFlag)
            throws IntermailException {
        try {
            GetMessageAttributes readMsgs = new GetMessageAttributes(host,
                    mailboxId, realm, folderUUID, keyType, keys, attrType,
                    popDeletedFlag);
            readMsgs.execute(myDataModel);
            MessageMetadata[] messageMetadata = readMsgs.getMessageMetadata();
            return messageMetadata;
        } catch (IntermailException e) {
            logger.warn("Error while reading message attributes: " + e.getFormattedString());
            throw e;
        }
    }

    public static void createMailBox(String mssHost, String mailboxId,
            String realm, String welcomeMessage) {
        try {
            CreateMailbox createMailbox = new CreateMailbox(mssHost, mailboxId,
                    realm, welcomeMessage, 0, 0);
            createMailbox.execute(RmeDataModel.Leopard);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteMailBox(String mssHost, String mailboxId,
            String realm) {
        try {
            DeleteMailbox deleteMailbox = new DeleteMailbox(mssHost, mailboxId,
                    realm, false, false, false, RmeDataModel.Leopard);
            deleteMailbox.execute(RmeDataModel.Leopard);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static FolderInfo[] getFolderInfo(String mssHost, String mailboxId,
            String realm) {
        ReadMailboxInfo readMailboxInfo = new ReadMailboxInfo(mssHost,
                mailboxId, realm, 0, false, true);
        try {
            readMailboxInfo.execute(RmeDataModel.Leopard);
        } catch (Exception e) {
            e.printStackTrace();
        }
        calculateFolderPaths(readMailboxInfo.getFolderInfo());
        FolderInfo[] folderInfo = readMailboxInfo.getFolderInfo();
        return folderInfo;

    }

    public static UUID getFolderUUID(String mssHost, String mailboxId,
            String realm, String folderPathName) throws NotFoundException {
        ReadMailboxInfo readMailboxInfo = new ReadMailboxInfo(mssHost,
                mailboxId, realm, 0, false, true);
        try {
            readMailboxInfo.execute(RmeDataModel.Leopard);
        } catch (Exception e) {
            e.printStackTrace();
        }
        calculateFolderPaths(readMailboxInfo.getFolderInfo());
        FolderInfo[] folderInfo = readMailboxInfo.getFolderInfo();
        UUID folderUUId = null;
        if (!folderPathName.startsWith(MxOSConstants.FORWARD_SLASH)) {
            folderPathName = MxOSConstants.FORWARD_SLASH + folderPathName;
        }

        if (folderInfo != null) {
            for (FolderInfo fi : folderInfo) {
                if (fi.getPathName().equals(folderPathName)) {
                    folderUUId = fi.getFolderUUID();
                    break;
                }
            }
        }

        if (null == folderUUId) {
            throw new NotFoundException(FolderError.FLD_NOT_FOUND.name());
        }
        return folderUUId;
    }

    @Override
    public void updateMessageBody(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("mss SL updateMessage method : starting");
        }

        String text = mxosRequestState.getInputParams()
                .get(MessageProperty.message.name()).get(0);
        MssLinkInfo info = (MssLinkInfo) mxosRequestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
        String host = getMssHost(info.getMessageStoreHosts());
        String mailboxId = info.getMailboxId();
        String realm = info.getRealm();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer().append("Host : ").append(host));
            logger.debug(new StringBuffer().append("MailboxId : ").append(
                    mailboxId));
            logger.debug(new StringBuffer().append("Realm : ").append(realm));
        }
        final String folderName = MessageServiceHelper.retrieveFolderName(mxosRequestState);

        UUID msgUUID = UUID.fromString(mxosRequestState.getInputParams()
                .get(MessageProperty.messageId.name()).get(0));

        UUID folderUUID = ActionUtils.getFolderUUID(mxosRequestState,
                folderName);
        UpdateMsgBlobStream update = new UpdateMsgBlobStream(host, mailboxId,
                realm, folderUUID, msgUUID, text);

        try {
            update.execute();
            if (logger.isDebugEnabled()) {
                logger.debug("mss updateMessageBlob method : updation is success");
            }

        } catch (IntermailException e) {
            logger.warn("Error while update mailbox: " + e.getFormattedString());
            ConnectionErrorStats.MSS.increment();
            if (e.getFormattedString().contains("MsUpdateFailwithRollback")) {
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_UPDATE_ROLLBACK_SUCCESS
                                .name(),
                        e.getMessage());
            } else if (e.getFormattedString().contains(
                    "MsUpdateFailwithoutRollback")) {
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_UPDATE_ROLLBACK_FAILED
                                .name(),
                        e.getMessage());
            } else {
                throw new MxOSException(
                        MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
            }
        } catch (NumberFormatException nfe) {
            logger.error("Number format exception occured.", nfe);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), nfe);
        } catch (IllegalArgumentException ile) {
            logger.error("Illegal argument exception occured.", ile);
            throw new InvalidRequestException(
                    MessageError.MSG_INVALID_MESSAGE_ID.name(), ile);
        } catch (Exception e) {
            logger.error("Error while update mailbox.", e);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), e);
        } catch (Throwable t) {
            logger.error("Error while update mailbox.", t);
            throw new MxOSException(
                    MessageError.MSG_UNABLE_TO_PERFORM_UPDATE.name(), t);
        }
    }
 }

