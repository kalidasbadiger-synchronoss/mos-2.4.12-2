/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.MxOSPOJOs.notify;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.*;
import static com.opwvmsg.mxos.error.NotifyError.NTF_TOPIC_NOT_FOUND;
import static com.opwvmsg.mxos.interfaces.service.Operation.POSTUID;
import static com.opwvmsg.mxos.interfaces.service.ServiceEnum.NotifyService;
import static com.opwvmsg.mxos.utils.misc.StatStatus.pass;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.notify.pojos.Notify;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Action to publish notify message
 * 
 * @author mxos-dev
 */
public class PublishNotify implements MxOSBaseAction {
    
    private static Logger logger = Logger.getLogger(PublishNotify.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("PublishNotify action STARTED.");
        }
        final long t0 = Stats.startTimer();

        /* fetching topic */
        final String aTopic = requestState.getInputParams().get(topic.name())
                .get(0);
        
        /* fetching subscription list resulted from GetNotify action */
        final Notify notifyObj = (Notify) requestState.getDbPojoMap()
                .getPropertyAsObject(notify);
        if (null == notifyObj) {
            if (logger.isDebugEnabled()) {
                logger.debug("No Notify found for topic: " + aTopic);
            }
            return;
        }
        final List<String> subList = notifyObj.getSubscriptions();
        if (subList == null || subList.isEmpty()) {
            /* no subscriptions provided for given topic */
            if (logger.isDebugEnabled()) {
                logger.debug("No subscriptions found for topic: " + aTopic);
            }
            throw new NotFoundException(NTF_TOPIC_NOT_FOUND.name());
        }
        
        /* getting message for publish */
        final String msg = requestState.getInputParams()
                .get(notifyMessage.name()).get(0);
        
        final boolean notifyPublishParellelProcessingEnabled = Boolean
                .parseBoolean(System.getProperty(
                        SystemProperty.notifyPublishParellelProcessingEnabled
                                .name(), "false"));
        
        for (String sub : subList) {
            PublishNotifyHandler publishNotifyHandler = new PublishNotifyHandler(
                    aTopic, sub, msg);
            if (notifyPublishParellelProcessingEnabled) {
                /* put the task to thread pool */
                PublishNotifyHandler.publish(publishNotifyHandler);
            } else {
                publishNotifyHandler.publishURL();
            }
        }

        Stats.stopTimer(NotifyService, POSTUID, t0, pass);
        if (logger.isDebugEnabled()) {
            logger.debug("PublishNotify action ENDED.");
        }
    }
}
