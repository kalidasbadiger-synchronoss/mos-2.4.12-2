/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import java.util.List;

import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Implementation class for Mailbox Provision.
 * 
 * @author mxos-dev
 */
public class MailboxProvisionHelper {
    /**
     * Helper method to create mailbox.
     * 
     * @param mailboxProvisionCURD mailboxProvisionCURD
     * @param mailboxPrimaryData mailboxPrimaryData
     * @param optionalData optionalData
     * @throws MxOSException MxOSException
     */
    public static void create(final IMailboxCRUD mailboxProvisionCURD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        mailboxProvisionCURD.createMailbox(mxosRequestState);
    }

    /**
     * Helper method to read Mailbox.
     * 
     * @param mailboxProvisionCURD mailboxProvisionCURD
     * @param mailboxData mailboxData
     * @return Mailbox mailbox POJO
     * @throws MxOSException
     * @throws Exception Exception
     */
    public static Mailbox read(final IMailboxCRUD mailboxProvisionCURD,
            final String email) throws MxOSException {
        return mailboxProvisionCURD.readMailbox(null, email);
    }

    public static List<Base> search(
            final IMailboxCRUD mailboxProvisionCURD,
            final MxOSRequestState requestState, final int maxSearchRows,
            final int maxSearchTimeout) throws MxOSException {
        return mailboxProvisionCURD.searchMailbox(requestState, maxSearchRows,
                maxSearchTimeout);
    }

    /**
     * Helper method to update mailbox.
     * 
     * @param mailboxProvisionCURD mailboxProvisionCURD
     * @param mailboxPrimaryData mailboxPrimaryData
     * @param optionalData optionalData
     * @throws InvalidRequestException
     * @throws ApplicationException
     * @throws AuthorizationException
     * @throws NotFoundException
     * @throws Exception Exception
     */
    public static void update(final IMailboxCRUD mailboxProvisionCURD,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        mailboxProvisionCURD.updateMailbox(mxosRequestState);
    }

    /**
     * Helper method to delete mailbox.
     *
     * @param mailboxProvisionCURD
     * @param requestState
     * @throws MxOSException
     */
    public static void delete(final IMailboxCRUD mailboxProvisionCURD,
            MxOSRequestState requestState) throws MxOSException {
        mailboxProvisionCURD.deleteMailbox(requestState);
    }
}
