/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.logging;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set preference via MAA.
 *
 * @author Aricent
 *
 */

public class PrefMaaLoggingAction extends MaaLoggingAction {
    private static Logger logger = Logger
        .getLogger(PrefMaaLoggingAction.class);

    /**
     * Action to make call to MAA.
     *
     * @param model
     *            model.
     * @throws Exception
     *             exception
     */
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Start : PrefMaaLoggingAction");
        }
        String userName;
        String domain;
        try {
            String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            Base base = ActionUtils.getUserInfoFromLDAP(email);
            userName = ActionUtils.getUsernameFromEmail(base.getEmail());
            domain = ActionUtils.getDomainFromEmail(base.getEmail());
            Map<String, String> params = new HashMap<String, String>();
            params.put(ARG1, userName);
            params.put(ARG2, domain);
            if (requestState.getInputParams().get(
                    MailboxProperty.customFields.name()) != null) {
                String customFields = requestState.getInputParams()
                        .get(MailboxProperty.customFields.name()).get(0);
                Map<String, String> cfmap = ActionUtils.getMapFromString(
                        requestState.getOperationName(), customFields);
                String pref = cfmap.get("bgcOptionsPref");
                if (pref != null) {
                    params.put(ARG3, pref);
                }
            }

            LoggingResponseBean maaResponse = callMaa(
                    UPDATE_OPTIONS_PREF_LOGGING_SUB_URL, params);
            // Analyze logging response
            analyzeLoggingResponse(maaResponse);
        } catch (MxOSException me) {
            logger.error(me);
            throw me;
        } catch (Exception e) {
            logger.error(e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);

        }
        if (logger.isDebugEnabled()) {
            logger.debug("End : PrefMaaLoggingAction");
        }
        return;
    }
}
