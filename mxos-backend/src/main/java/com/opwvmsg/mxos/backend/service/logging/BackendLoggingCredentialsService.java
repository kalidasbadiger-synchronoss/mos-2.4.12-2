/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.logging;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.mailbox.ICredentialService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Credentials service exposed to client which is responsible for reading 
 * crendentials, authenticate etc., directly in the database.
 *
 * @author mxos-dev
 */
public class BackendLoggingCredentialsService implements ICredentialService {
    private static Logger logger = Logger
            .getLogger(BackendLoggingCredentialsService.class);

    /**
     * Default Constructor.
     */
    public BackendLoggingCredentialsService() {
        logger.info("BackendLoggingCredentialsService created...");
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
        throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.LoggingCredentialsService,
                    Operation.POST);

            if (inputParams == null || inputParams.size() <= 1) {
                ExceptionUtils.createInvalidRequestException(mxosRequestState);
            }
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.LoggingCredentialsService,
                    Operation.POST, t0, StatStatus.pass);
        } catch (final MxOSException e) {
            Stats.stopTimer(ServiceEnum.LoggingCredentialsService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Credentials read(Map<String, List<String>> inputParams)
        throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void authenticate(Map<String, List<String>> inputParams)
        throws MxOSException {
        // TODO Auto-generated method stub
    }
    @Override
    public Mailbox authenticateAndGetMailbox(Map<String, List<String>> inputParams)
        throws MxOSException {
        // TODO Auto-generated method stub
        return null;
    }
}
