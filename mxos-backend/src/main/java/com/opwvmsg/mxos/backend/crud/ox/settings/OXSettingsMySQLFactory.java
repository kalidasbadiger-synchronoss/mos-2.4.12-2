/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ox.settings;

import org.apache.commons.pool.BasePoolableObjectFactory;

import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;

/**
 * MySQL connection pool factory. This is required by Apache object pool.
 * 
 * @author mxos-dev
 */
class OXSettingsMySQLFactory extends BasePoolableObjectFactory<ISettingsCRUD> {
    private String oxMySQLURL;
    private String userName;
    private String password;

    /**
     * Constructor.
     * 
     * @param oxMySQLURL OX MySQL URL.
     */
    public OXSettingsMySQLFactory(String oxMySQLURL, String userName,
            String password) {
        this.oxMySQLURL = oxMySQLURL;
        this.userName = userName;
        this.password = password;
    }

    @Override
    public void destroyObject(ISettingsCRUD settingsCRUD) throws Exception {
        settingsCRUD.close();
    }

    @Override
    public ISettingsCRUD makeObject() throws Exception {
        return new OXSettingsMySQLCRUD(oxMySQLURL, userName, password);
    }
}
