/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

/**
 * Action class to set Maximum allowed number of Aliases.
 * 
 * @author mxos-dev
 */
public class SetMaxNumAliases implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMaxNumAliases.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxNumAliases action start."));
        }
        try {
            final String maxNumAliases = requestState.getInputParams()
                    .get(MailboxProperty.maxNumAliases.name()).get(0);
            if (!requestState.getOperationName().contains(
                    ServiceEnum.CosBaseService.name())
                    && (!ActionUtils.isCreateMailboxOperation(requestState
                            .getOperationName()))) {
                List<String> aliasesList = null;
                if (requestState.getDbPojoMap().contains(MxOSPOJOs.base)) {
                    final Base base = (Base) requestState.getDbPojoMap()
                        .getPropertyAsObject(MxOSPOJOs.base);
                    aliasesList = base.getEmailAliases();
                }
                try {
                    final int maxNumAliasesInt = Integer.parseInt(maxNumAliases);
                    if (aliasesList != null) {
                        if (aliasesList.size() > maxNumAliasesInt) {
                            throw new InvalidRequestException(
                                    MailboxError.MBX_INVALID_MAXNUM_OF_ALIASES
                                            .name());
                        }
                    }
                } catch (final NumberFormatException e) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_INVALID_MAXNUM_OF_ALIASES.name(),
                            e);
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.maxNumAliases,
                            maxNumAliases);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while set max num aliases.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_NUM_ALIAS.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxNumAliases action end."));
        }
    }
}
