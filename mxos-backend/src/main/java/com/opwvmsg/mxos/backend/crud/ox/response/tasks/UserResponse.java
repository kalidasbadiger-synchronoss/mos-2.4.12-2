package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.crud.ox.response.JsonToSessionMapper;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.sun.jersey.api.client.ClientResponse;

public class UserResponse extends Response {

    public String getUserId(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);

        if (root == null
                || root.path(OXTasksProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXTasksProperty.data.name());
        }

        return JsonToSessionMapper.mapToUserId(root);
    }
}
