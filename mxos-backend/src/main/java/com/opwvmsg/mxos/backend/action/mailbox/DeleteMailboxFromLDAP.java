/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete mailbox.
 * 
 * @author mxos-dev
 */
public class DeleteMailboxFromLDAP extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(DeleteMailboxFromLDAP.class);

    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromLDAP action start.");
        }
        final String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // check whether skip mailbox deletion from MSS
        boolean delete = true;
        if (requestState.getInputParams().containsKey(SKIP_LDAP)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_LDAP).get(0));
            if (skip) {
                delete = false;
                logger.info("Skip mailbox [" + email + "] deletion from LDAP.");
            }
        }

        // compatible with old parameter deleteOnlyOnMss
        if (requestState.getInputParams().containsKey(
                MailboxProperty.deleteOnlyOnMss.name())) {
            boolean deleteOnlyOnMSS = Boolean.valueOf(requestState
                    .getInputParams()
                    .get(MailboxProperty.deleteOnlyOnMss.name()).get(0));
            if (deleteOnlyOnMSS) {
                delete = false;
                logger.info("Delete mailbox [" + email
                        + "] only from MSS, so skip LDAP.");
            }
        }

        // delete mailbox from LDAP
        if (delete) {
            // in order to call three sun mailbox deletion actions separately
            // remove the action GetMailboxDn from MailboxService:DELETE in
            // rules.xml
            // So when DeleteMailboxFromLDAP action is called
            // Action GetMailboxDn will be called here first
            getMailboxDn(requestState);

            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
            try {
                mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                mailboxCRUD.deleteMailbox(requestState);
                logger.info("Delete mailbox [" + email
                        + "] from LDAP successfully.");
            } catch (final MxOSException e) {
                logger.error("Error while deleting mailbox [" + email
                        + "] from LDAP", e);
                throw e;
            } catch (final Exception e) {
                logger.error("Error while deleting mailbox [" + email
                        + "] from LDAP", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
            } finally {
                CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromLDAP action end.");
        }
    }

    public void getMailboxDn(MxOSRequestState requestState)
            throws MxOSException {
        ActionFactory.getInstance().exec(GetMailboxDn.class.getName(),
                requestState);
    }

}
