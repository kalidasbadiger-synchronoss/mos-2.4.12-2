/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.AuthenticationException;
import javax.naming.InvalidNameException;
import javax.naming.NameAlreadyBoundException;
import javax.naming.NameNotFoundException;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.OperationNotSupportedException;
import javax.naming.SizeLimitExceededException;
import javax.naming.TimeLimitExceededException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InvalidAttributeIdentifierException;
import javax.naming.directory.InvalidAttributeValueException;
import javax.naming.directory.InvalidAttributesException;
import javax.naming.directory.NoSuchAttributeException;
import javax.naming.directory.SchemaViolationException;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.AuthorizationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.jmx.counter.ConnectionErrorStats;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * LDAP Implementation class for Mailbox Account CRUD.
 * 
 * @author mxos-dev
 */
public class LdapMailboxCRUD implements IMailboxCRUD {
    private static Logger logger = Logger.getLogger(LdapMailboxCRUD.class);
    
    // Object parameters
    private OWBaseLdapContext ldapContext = null;
    private boolean status = false;
    private final Map<String, Attributes> groupMap = new LinkedHashMap<String, Attributes>();
    private String activeHostURL = null;
    private Hashtable<String, String> crudEnv;
    private final int ldapMaxRetryCount;
    private final int ldapSleepBetweenRetry;
    private final int numHosts;
    private final String crudType;
    final LdapCRUDFactory factory;

    /**
     * Default Constructor.
     * 
     * @throws Exception Exception in case of any error on connection.
     */
    public LdapMailboxCRUD(String ldapCrudType,
            LdapCRUDFactory factory) throws Exception {

        ldapMaxRetryCount = Integer.parseInt(System.getProperty(
                SystemProperty.ldapMaxRetryCount.name(), "0"));
        ldapSleepBetweenRetry = Integer.parseInt(System.getProperty(
                SystemProperty.ldapSleepBetweenRetry.name(), "1"));
        crudType = ldapCrudType;
        this.factory = factory;
        crudEnv = factory.getEnv();
        numHosts = factory.getNumHosts();
        activeHostURL = crudEnv.get(DirContext.PROVIDER_URL);
        for (int i = 0; i < numHosts && this.status == false; i++) {
            int retryCount = 0;
            do {
                try {
                    getNewContext(retryCount);
                    break;
                } catch (final Exception e) {
                    retryCount++;
                    if (retryCount <= ldapMaxRetryCount) {
                        logger.info("Error in LDAP connection, retrying... "
                                + "retryCount: " + retryCount);
                        continue;
                    } else {
                        logger.error("Error while initializing LDAP CRUD. "
                                + "Exceeded ldapMaxRetryCount for host "
                                + activeHostURL);
                        getNextAvailableHost();
                    }
                }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (this.status == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new ApplicationException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
    }

    /**
     * getNextAvailableHost based on the crudType
     */
    private void getNextAvailableHost() {
        if (logger.isDebugEnabled()) {
            logger.debug("LDAP CRUD active Host: " + activeHostURL);
        }
        
        String poolActiveURL = factory.getEnv().get(DirContext.PROVIDER_URL);
        Hashtable<String, String> newEnv = factory.getEnv();
            
        if (logger.isDebugEnabled()) {
            logger.debug("LDAP pool active Host: " + poolActiveURL);
        }
        if ((activeHostURL).equals(poolActiveURL)) {
            factory.getActiveLdapHost();
            newEnv = factory.getEnv();
        }
        crudEnv = newEnv;
        activeHostURL = crudEnv.get(DirContext.PROVIDER_URL);
        logger.info("Considering next available host: " + activeHostURL);
    }

    /**
     * getConnection - This gets new connection
     * 
     * @return boolean
     * @throws Exception
     */
    private void getNewContext(final int retryCount) throws NamingException, InterruptedException {
        try {
            if (ldapContext != null) {
                ldapContext.close();
            }
            logger.info("Trying to get new LDAP Context for host: "
                    + crudEnv.get(DirContext.PROVIDER_URL));
            // Create another initial context
            if (retryCount > 0) {
                logger.info("Thread sleep before retry ldapSleepBetweenRetry: "
                        + ldapSleepBetweenRetry);
                Thread.sleep(ldapSleepBetweenRetry);
            }
            ldapContext = factory.createLdapContext(crudEnv);
            if (ldapContext == null) {
                logger.warn("Error initializing LDAP connection while reconnecting.");
                incrementConnectionErrorStats();
                this.status = false;
                return;
            }
            logger.info("LDAP Context initialized successfully for host: "
                    + crudEnv.get(DirContext.PROVIDER_URL));
            incrementConnectionSucessStats();
            this.status = true;
            return;
        } catch (final NamingException e) {
            if (e.getRootCause() != null) {
                logger.warn("Error initializing LDAP connection while reconnecting. Reason: "
                        + e.getRootCause().getMessage());
            } else {
                logger.warn("Error initializing LDAP connection while reconnecting. Reason: "
                        + e.getMessage());
            }
            incrementConnectionErrorStats();
            this.status = false;
            throw e;
        }
    }

    /**
     * incrementConnectionErrorStats for JMX
     */
    private void incrementConnectionErrorStats() {
        if (crudType.equalsIgnoreCase(MxOSConstants.LDAPMAILBOXCRUD)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Incrementing error LDAP stats");
            }
            ConnectionErrorStats.LDAP.increment();
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Incrementing error LDAPSEARCH stats");
            }
            ConnectionErrorStats.LDAPSEARCH.increment();
        }
    }

    /**
     * incrementConnectionSucessStats for JMX
     */
    private void incrementConnectionSucessStats() {
        if (crudType.equalsIgnoreCase(MxOSConstants.LDAPMAILBOXCRUD)) {
            if (logger.isDebugEnabled()) {
                logger.debug("Incrementing success LDAP stats");
            }
            ConnectionStats.LDAP.increment();
        } else {
            if (logger.isDebugEnabled()) {
                logger.debug("Incrementing success LDAPSEARCH stats");
            }
            ConnectionStats.LDAPSEARCH.increment();
        }
    }

    /**
     * This method is used to check if the ldapContext connection is still
     * properly connected and configured.
     * 
     * @return
     * @throws MxOSException
     */
    public boolean isConnected() throws MxOSException {
        String poolActiveURL = factory.getEnv().get(DirContext.PROVIDER_URL);
        Hashtable<String, String> newEnv = factory.getEnv();
        
        if (logger.isDebugEnabled()) {
            logger.debug("LDAP CRUD active Host: " + activeHostURL
                    + " Status: " + this.status);
            logger.debug("LDAP Pool active Host: " + poolActiveURL);
        }
        if (!activeHostURL.equals(poolActiveURL)) {
            if (logger.isDebugEnabled()) {
                logger.debug("LDAP pool host and CRUD active host are different. "
                        + "Considering active host as " + poolActiveURL);
            }
            crudEnv = newEnv;
            activeHostURL = crudEnv.get(DirContext.PROVIDER_URL);
            this.status = false;
        }
        logger.info("LDAP CRUD active Host: " + activeHostURL + " Status: "
                + this.status);
        return this.status;
    }

    /**
     * Method to close the ldap connection.
     * 
     * @throws MxOSException
     */
    public void close() throws MxOSException {
        try {
            if (ldapContext != null) {
                ldapContext.close();
                this.status = false;
            }
        } catch (NamingException e) {
            this.status = false;
            logger.error("Error while closing LDAP CRUD.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
    
    /**
     * Resources valid only within the operation are released by this method.
     * This method is mainly used by the pool class.
     */
    public void releaseResources() {
        groupMap.clear();
    }

    /**
     * 
     * This method fetches the LDAP attributes for the mailbox with cos caching
     * support.
     * 
     * @param fulldc
     * @param attributeNames
     * @param isCosDataRequired
     * @param cosMap
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    private Attributes executeLDAPRetrieve(final String searchFilter,
            final String[] attributeNames,
            Map<String, List<String>> inputParams, final boolean doCosCaching,
            Map<String, Attributes> cosMap, final boolean isGroupCacheReq)
            throws MxOSException, NamingException {
        final long t0 = Stats.startTimer();
        logger.info("LDAP Read start for: " + searchFilter);
        Attributes attrs = null;
        boolean hostActive = false;
        
        // read options
        boolean isGroupAttrsReq = false;
        boolean doSearchForBlockLists = false;
        boolean isAuthoritative = false;
        if (inputParams != null) {
            isGroupAttrsReq = readInputParamBoolean(inputParams,
                    MailboxProperty.adminControl, false);
            doSearchForBlockLists = readInputParamBoolean(inputParams,
                    MailboxProperty.senderBlocking, false);
            isAuthoritative = readInputParamBoolean(inputParams,
                    MailboxProperty.authoritative, false);
        }
        
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        final int maxTimeOut = MxOSApp.getInstance()
                                .getSearchMaxTimeOut();
                        final SearchControls sc = new SearchControls(
                                SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                                attributeNames, false, false);
                        NamingEnumeration<SearchResult> results = null;
                        results = ldapContext.search("", searchFilter, sc, isAuthoritative);
                        if (results != null && results.hasMore()) {
                            SearchResult searchResult = results.next();
                            if (searchResult != null
                                    && searchResult.getAttributes() != null) {
                                attrs = searchResult.getAttributes();
                                String nameInNamespace = searchResult
                                        .getNameInNamespace();
                                if (logger.isDebugEnabled()) {
                                    logger.debug("NameInNamespace : "
                                            + nameInNamespace);
                                }
                                
                                Attribute cosIdAttr = attrs
                                        .get(LDAPMailboxProperty.adminpolicydn
                                                .name());
                                if (cosIdAttr != null
                                        && cosIdAttr.get() != null) {
                                    Attributes cosAttrs = getCosAttributes(
                                            cosIdAttr.get().toString(),
                                            attributeNames, inputParams);
                                    attrs = addAttrsToExistingAttrs(attrs,
                                            cosAttrs);
                                    cosIdAttr = null;
                                    cosAttrs = null;
                                }
                                
                                // Do extra search for senderBlocking lists
                                if (doSearchForBlockLists) {
                                    searchSendersControlLists(nameInNamespace, attrs, 
                                            makeTempState(inputParams));
                                }
                                
                                if (isGroupAttrsReq) {
                                    // Check and retrieve family mailbox attributes
                                    Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                            attrs, nameInNamespace, isGroupCacheReq, inputParams);
                                    attrs = addAttrsToExistingAttrs(attrs, groupAttrs);
                                    nameInNamespace = null;
                                    groupAttrs = null;
                                }
                                
                                Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                        Operation.GET, t0, StatStatus.pass);
                            }
                            searchResult = null;
                        }
                        results = null;
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPRetrieve.", e);
                        Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                Operation.GET, t0, StatStatus.fail);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Time out error in executeLDAPRetrieve.",
                                e);
                        Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                Operation.GET, t0, StatStatus.fail);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        logger.error(
                                "Naming error during executeLDAPRetrieve.", e);
                        Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                Operation.GET, t0, StatStatus.fail);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                Operation.GET, t0, StatStatus.fail);
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        Stats.stopTimer(ServiceEnum.LDAPGetMailboxTime,
                                Operation.GET, t0, StatStatus.fail);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return attrs;
    }

    /**
     * executeLDAPRetrieve with full DC
     * 
     * @param fulldc
     * @param attributeNames
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes executeLDAPRetrieve(final String fulldc,
            final String[] attributeNames, Map<String, List<String>> inputParams)
            throws MxOSException, NamingException {
        logger.info("LDAP Read start for: " + fulldc);
        boolean isAuthoritative = false;
        isAuthoritative = readInputParamBoolean(inputParams,
                        MailboxProperty.authoritative, false);
        Attributes result = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        result = ldapContext.getAttributes(fulldc,
                                attributeNames, isAuthoritative);
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPRetrieve.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Time out error in executeLDAPRetrieve.",
                                e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error(
                                "Naming error during executeLDAPRetrieve.", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return result;
    }
    
    /**
     * executeLDAPRetrieve with group attributes
     * 
     * @param searchFilter
     * @return
     * @throws MxOSException
     * @throws NamingException
     * @throws InterruptedException
     */
    private Attributes executeLDAPRetrieve(String searchFilter,
            MxOSRequestState mxosRequestState) throws MxOSException, NamingException {
        logger.info("LDAP Read start for: " + searchFilter);
        
        // read options
        boolean isGroupAttrsReq = false;
        boolean doSearchForBlockLists = false;
        boolean isAuthoritative = false;
        Map<String, List<String>> inputParams = null;
        if (mxosRequestState != null) {
            inputParams = mxosRequestState.getInputParams();
            isGroupAttrsReq = readInputParamBoolean(inputParams,
                    MailboxProperty.adminControl, false);
            doSearchForBlockLists = readInputParamBoolean(inputParams,
                    MailboxProperty.senderBlocking, false);
            isAuthoritative = readInputParamBoolean(inputParams,
                    MailboxProperty.authoritative, false);
        }
        
        Attributes attrs = null;
        final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                try {
                    if (this.status == false) {
                        getNewContext(retryCount);
                    }
                    final SearchControls sc = new SearchControls(
                            SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                            LDAPUtils.mailboxAttributes, false, false);
                    NamingEnumeration<SearchResult> results = null;
                    results = ldapContext.search("", searchFilter, sc, isAuthoritative);
                    if (results != null && results.hasMore()) {
                        SearchResult searchResult = results.next();
                        if (searchResult != null
                                && searchResult.getAttributes() != null) {
                            attrs = searchResult.getAttributes();
                            String targetDn = searchResult.getNameInNamespace();
                            
                            // Retrieve COS attributes
                            Attribute cosIdAttr = attrs
                                    .get(LDAPMailboxProperty.adminpolicydn
                                            .name());
                            if (cosIdAttr != null
                                    && cosIdAttr.get() != null) {
                                Attributes cosAttrs = getCosAttributes(
                                        cosIdAttr.get().toString(),
                                        LDAPUtils.mailboxAttributes, inputParams);
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosAttrs);
                                cosIdAttr = null;
                                cosAttrs = null;
                            }
                            
                            // Do extra search for senderBlocking lists
                            if (doSearchForBlockLists) {
                                searchSendersControlLists(targetDn, attrs, mxosRequestState);
                            }
                            
                            // Read adminControl info
                            if (isGroupAttrsReq) {
                                String groupName = targetDn;
                                if (logger.isDebugEnabled()) {
                                    logger.debug("GroupName : " + groupName);
                                }
                                // Check and retrieve family mailbox attributes
                                Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                        attrs, groupName, false, inputParams);
                                attrs = addAttrsToExistingAttrs(attrs,
                                        groupAttrs);
                                groupName = null;
                                groupAttrs = null;

                                // Check and retrieve derived group admin
                                // attributes
                                if (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name()) != null) {
                                    Attribute attr = attrs
                                            .get(LDAPMailboxProperty.mailfamilymailbox
                                                    .name());
                                    String mailFamilyMailbox = (String) attr
                                            .get();
                                    if (mailFamilyMailbox
                                            .equals(MailboxType.GROUPADMIN
                                                    .getValue())) {
                                        String dn = LDAPUtils
                                                .getGroupDnFromGroupName(groupName);
                                        if (dn != null) {
                                            Attributes derivedAttrs = readAdminAllocationsAttributes(
                                                    dn, inputParams);
                                            attrs = addAttrsToExistingAttrs(
                                                    attrs, derivedAttrs);
                                            derivedAttrs = null;
                                        }
                                    }
                                }
                            }
                        }
                        searchResult = null;
                    }
                    results = null;
                    hostActive = true;
                    break;
                } catch (final AuthenticationException e) {
                    logger.error("Error in executeLDAPRetrieve.", e);
                    throw new AuthorizationException(
                            ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                } catch (final TimeLimitExceededException e) {
                    logger.error("Timeout error in executeLDAPRetrieve.", e);
                    throw e;
                } catch (final OperationNotSupportedException e) {
                    this.status = false;
                    logger.error(
                            "Naming error during executeLDAPRetrieve.", e);
                    throw new ApplicationException(
                            ErrorCode.LDP_NAMING_ERROR.name(), e);
                } catch (final NamingException e) {
                    logger.warn("Error while LDAP Read: " + e.getMessage());
                    if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                        if (++retryCount <= ldapMaxRetryCount) {
                            logger.info("Error in LDAP connection, retrying count: "
                                    + retryCount);
                            incrementConnectionErrorStats();
                            this.status = false;
                            continue;
                        } else {
                            logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                    + activeHostURL);
                            getNextAvailableHost();
                        }
                    } else {
                        throw e;
                    }
                } catch (final Exception e) {
                    logger.error("Error while LDAP Read: ", e);
                    throw new ApplicationException(
                            ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return attrs;
    }

    private Attributes executeLDAPRetrieveMSSLinkAttributes(
            String searchFilter, Map<String, List<String>> inputParams)
            throws MxOSException, NamingException {
        logger.info("LDAP Read start for: " + searchFilter);
        boolean isAuthoritative = readInputParamBoolean(inputParams,
                MailboxProperty.authoritative, false);
        Attributes attrs = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        final int maxTimeOut = MxOSApp.getInstance()
                                .getSearchMaxTimeOut();
                        final SearchControls sc = new SearchControls(
                                SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                                LDAPUtils.mailboxMSSLinkAttributes, false, false);
                        NamingEnumeration<SearchResult> results = null;
                        results = ldapContext.search("", searchFilter, sc, isAuthoritative);
                        if (results != null && results.hasMore()) {
                            SearchResult searchResult = results.next();
                            if (searchResult != null
                                    && searchResult.getAttributes() != null) {
                                attrs = searchResult.getAttributes();
                            }
                            // Retrieve COS attributes
                            // currently we need only Realm from COS data
                            Attribute cosIdAttr = attrs
                                    .get(LDAPMailboxProperty.adminpolicydn
                                            .name());
                            if (cosIdAttr != null && cosIdAttr.get() != null) {
                                Attributes cosAttrs = getCosAttributes(
                                        cosIdAttr.get().toString(),
                                        LDAPUtils.mailboxAttributes, inputParams);
                                attrs = addAttrsToExistingAttrs(attrs, cosAttrs);
                                cosIdAttr = null;
                                cosAttrs = null;
                            }
                            searchResult = null;
                        }
                        results = null;
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in LDAP read.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Error in LDAP read.", e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error("Error in LDAP read.", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return attrs;
    }

    /**
     * executeLDAPSearch for generic formattedLdapFilter
     * 
     * @param ldapBaseDn
     * @param formattedLdapFilter
     * @param sc
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    private NamingEnumeration<SearchResult> executeLDAPSearch(
            final String ldapBaseDn, final String formattedLdapFilter,
            final SearchControls sc, Map<String, List<String>> inputParams)
            throws MxOSException, NamingException {
        logger.info("LDAP Search start for: " + formattedLdapFilter);
        boolean isAuthoritative = readInputParamBoolean(inputParams,
                MailboxProperty.authoritative, false);
        NamingEnumeration<SearchResult> results = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        results = ldapContext.search(ldapBaseDn,
                                formattedLdapFilter, sc, isAuthoritative);
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPSearch.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Time out error in executeLDAPSearch.", e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error("Naming error during executeLDAPSearch.",
                                e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return results;
    }

    private NamingEnumeration<SearchResult> executeLDAPGroupSearch(
            String adminRealmDn, final String groupAdminLdapFilter,
            final SearchControls groupAdminSc) throws MxOSException,
            NamingException {
        logger.info("LDAP Search start for: " + groupAdminLdapFilter);
        NamingEnumeration<SearchResult> groupAdminSearchResult = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        groupAdminSearchResult = ldapContext.search(
                                adminRealmDn, groupAdminLdapFilter,
                                groupAdminSc);
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPGroupSearch.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error(
                                "Time out error in executeLDAPGroupSearch.", e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error(
                                "Naming error during executeLDAPGroupSearch.",
                                e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return groupAdminSearchResult;
    }

    private void executeLDAPCreate(final String name, final Object obj,
            final Attributes attributes) throws MxOSException, NamingException {
        logger.info("LDAP Create start for: " + name);
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        ldapContext.bind(name, obj, attributes);
                        hostActive = true;
                        break;
                    } catch (NameAlreadyBoundException e) {
                        logger.warn("Error in executeLDAPCreate. Entry already exists");
                        throw e;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPCreate.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Time out error in executeLDAPCreate.", e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error("Naming error during executeLDAPCreate.",
                                e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final SchemaViolationException e) {
                        logger.warn("Error in executeLDAPCreate.", e);
                        throw new InvalidRequestException(
                                ErrorCode.LDP_SCHEMA_VIOLATION.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
    }

    private void executeLDAPUpdate(final String mailboxDn,
            final Attributes attributes) throws MxOSException, NamingException {
        logger.info("LDAP Update start for: " + mailboxDn);
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        ldapContext.modifyAttributes(mailboxDn,
                                DirContext.REPLACE_ATTRIBUTE, attributes);
                        hostActive = true;
                        break;
                    } catch (final AuthenticationException e) {
                        logger.error("Error in executeLDAPUpdate.", e);
                        throw new AuthorizationException(
                                ErrorCode.LDP_AUTHENTICATION_ERROR.name(), e);
                    } catch (final TimeLimitExceededException e) {
                        logger.error("Time out error in executeLDAPUpdate.", e);
                        throw e;
                    } catch (final OperationNotSupportedException e) {
                        this.status = false;
                        logger.error("Naming error during executeLDAPUpdate.",
                                e);
                        throw new ApplicationException(
                                ErrorCode.LDP_NAMING_ERROR.name(), e);
                    } catch (final SchemaViolationException e) {
                        logger.warn("Error in executeLDAPUpdate.", e);
                        throw new InvalidRequestException(
                                ErrorCode.LDP_SCHEMA_VIOLATION.name(), e);
                    } catch (final NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
    }

    /**
     * executeLDAPDelete for a given dn
     * 
     * @param mailboxDn
     * @throws MxOSException
     * @throws NamingException
     */
    private void executeLDAPDelete(final String mailboxDn)
            throws MxOSException, NamingException {
        if (logger.isDebugEnabled()) {
            logger.debug("Executing executeLDAPDelete with dn - " + mailboxDn);
        }
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        ldapContext.unbind(mailboxDn);
                        hostActive = true;
                        break;
                    } catch (NameNotFoundException eNotFound) {
                        logger.warn("Exception while executeLDAPDelete");
                        throw eNotFound;
                    } catch (NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw e;
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
    }

    @Override
    public String readMailboxDn(final MxOSRequestState requestState)
            throws MxOSException {
        String[] attributes = new String[] { LDAPMailboxProperty.mailfamilymailbox
                .name() };
        final String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        logger.info("LDAP Read start for: " + email);
        boolean isAuthoritative = readInputParamBoolean(requestState.getInputParams(),
                MailboxProperty.authoritative, false);
        String nameInNamespace = null;
        Attributes attrs = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        final int maxTimeOut = MxOSApp.getInstance()
                                .getSearchMaxTimeOut();
                        final SearchControls sc = new SearchControls(
                                SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                                attributes, false, false);
                        NamingEnumeration<SearchResult> results = ldapContext.search("",
                                getMailboxFilter(email), sc, isAuthoritative);
                        if (results != null && results.hasMore()) {
                            SearchResult searchResult = results.next();
                            if (searchResult != null
                                    && searchResult.getAttributes() != null) {
                                attrs = searchResult.getAttributes();
                                String mailFamilyMailbox = null;
                                if (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name()) != null) {
                                    mailFamilyMailbox = attrs
                                            .get(LDAPMailboxProperty.mailfamilymailbox
                                                    .name()).get().toString();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("mailFamilyMailbox : "
                                                + mailFamilyMailbox);
                                    }
                                    if (mailFamilyMailbox
                                            .equals(MailboxType.GROUPMAILBOX
                                                    .getValue())
                                            || mailFamilyMailbox
                                                    .equals(MailboxType.NORMAL
                                                            .getValue())
                                            || mailFamilyMailbox
                                                    .equals(MailboxType.GROUPADMIN
                                                            .getValue())) {
                                        nameInNamespace = searchResult
                                                .getNameInNamespace();
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("NameInNamespace : "
                                                    + nameInNamespace);
                                        }
                                        requestState
                                                .getDbPojoMap()
                                                .setProperty(
                                                        MxOSPOJOs.familyMailboxDn,
                                                        nameInNamespace);
                                        requestState.getDbPojoMap()
                                                .setProperty(MxOSPOJOs.type,
                                                        mailFamilyMailbox);
                                    }
                                }
                            }
                            searchResult = null;
                        }
                        results = null;
                        hostActive = true;
                        break;
                    } catch (NameNotFoundException e) {
                        logger.warn("Exception while readMailboxDn", e);
                        throw new NotFoundException(
                                MailboxError.MBX_NOT_FOUND.name(), e);
                    } catch (NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                        }
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return nameInNamespace;
    }

    @Override
    public String readAdminRealmDn(String email, Map<String, List<String>> inputParams)
            throws MxOSException {
        logger.info("LDAP Read start for: " + email);
        String[] attributes = new String[] { LDAPMailboxProperty.mailfamilymailbox
                .name() };
        boolean isAuthoritative = readInputParamBoolean(inputParams,
                MailboxProperty.authoritative, false);
        NamingEnumeration<SearchResult> results = null;
        String adminRealmdn = null;
        Attributes attrs = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        final int maxTimeOut = MxOSApp.getInstance()
                                .getSearchMaxTimeOut();
                        final SearchControls sc = new SearchControls(
                                SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                                attributes, false, false);

                        results = ldapContext.search("",
                                getMailboxFilter(email), sc, isAuthoritative);
                        if (results != null && results.hasMore()) {
                            SearchResult searchResult = results.next();
                            if (searchResult != null
                                    && searchResult.getAttributes() != null) {
                                attrs = searchResult.getAttributes();
                                String mailFamilyMailbox = null;
                                if (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name()) != null) {
                                    mailFamilyMailbox = attrs
                                            .get(LDAPMailboxProperty.mailfamilymailbox
                                                    .name()).get().toString();
                                    if (logger.isDebugEnabled()) {
                                        logger.debug("mailFamilyMailbox : "
                                                + mailFamilyMailbox);
                                    }
                                    if (mailFamilyMailbox
                                            .equals(MailboxType.GROUPADMIN
                                                    .getValue())) {
                                        String nameInNamespace = searchResult
                                                .getNameInNamespace();
                                        if (logger.isDebugEnabled()) {
                                            logger.debug("NameInNamespace : "
                                                    + nameInNamespace);
                                        }
                                        Attributes groupAttrs = getGroupMailboxAttributes(LDAPUtils
                                                .getGroupDnFromGroupName(nameInNamespace),inputParams);
                                        if (groupAttrs != null
                                                && groupAttrs
                                                        .get(LDAPGroupMailboxProperty.adminRealmdn
                                                                .name()) != null) {
                                            adminRealmdn = groupAttrs
                                                    .get(LDAPGroupMailboxProperty.adminRealmdn
                                                            .name()).get()
                                                    .toString();
                                            groupAttrs = null;
                                        }
                                    } else {
                                        throw new InvalidRequestException(
                                                MailboxError.MBX_NOT_GROUP_ADMIN_ACCOUNT
                                                        .name());
                                    }
                                } else {
                                    throw new InvalidRequestException(
                                            MailboxError.MBX_NOT_GROUP_ACCOUNT
                                                    .name());
                                }
                            }
                            searchResult = null;
                        }
                        results = null;
                        hostActive = true;
                        break;
                    } catch (NameNotFoundException e) {
                        logger.warn("Exception while readAdminRealmDn", e);
                        throw new NotFoundException(
                                MailboxError.MBX_NOT_FOUND.name(), e);
                    } catch (NamingException e) {
                        logger.warn("Error while LDAP Read: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP Read. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                        }
                    } catch (MxOSException e) {
                        logger.warn("Exception while readAdminRealmDn", e);
                        throw e;
                    } catch (final Exception e) {
                        logger.error("Error while LDAP Read: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return adminRealmdn;
    }

    @Override
    public void deleteDomain(final String domainName) throws MxOSException {
        String fulldc = LDAPUtils.getDomainSearchQuery(domainName);
        logger.info("LDAP Delete domain start for: " + fulldc);
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        ldapContext.unbind(fulldc);
                        hostActive = true;
                        break;
                    } catch (NameNotFoundException e) {
                        logger.warn("Exception while deleteDomain", e);
                        throw new InvalidRequestException(
                                DomainError.DMN_NOT_FOUND.name(), e);
                    } catch (NamingException e) {
                        logger.warn(
                                "Exception while deleteDomain. Reason: ", e);
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while LDAP delete. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                        }
                    } catch (Exception e) {
                        logger.error("Error while deleteDomain", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
    }

    /**
     * retrieveFamilyMailbox attributes
     * 
     * @param attrs
     * @param nameSpace
     * @param isGroupCachingReq
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes retrieveFamilyMailboxAttrs(Attributes attrs,
            String nameSpace, boolean isGroupCachingReq,
            Map<String, List<String>> inputParams) throws MxOSException,
            NamingException {
        String dn = LDAPUtils.getGroupDnFromGroupName(nameSpace);
        if (logger.isDebugEnabled()) {
            logger.debug("dn : " + dn);
        }
        
        Attributes familyAttrs = new BasicAttributes();
        String mailFamilyMailbox = null;
        if (attrs.get(LDAPMailboxProperty.mailfamilymailbox.name()) != null) {
            mailFamilyMailbox = attrs
                    .get(LDAPMailboxProperty.mailfamilymailbox.name()).get()
                    .toString();
            if (logger.isDebugEnabled()) {
                logger.debug("mailFamilyMailbox : " + mailFamilyMailbox);
            }
            if (mailFamilyMailbox.equals(MailboxType.GROUPMAILBOX.getValue())
                    || mailFamilyMailbox.equals(MailboxType.GROUPADMIN
                            .getValue())
                    || mailFamilyMailbox.equals(MailboxType.NORMAL
                            .getValue())) {
                if (isGroupCachingReq && groupMap.containsKey(nameSpace)) {
                    familyAttrs = groupMap.get(dn);
                } else {
                    // Get group attributes to get adminRealmDn
                    Attributes groupAttrs = getGroupMailboxAttributes(dn, inputParams);
                    if (logger.isDebugEnabled()) {
                        logger.debug("groupAttrs : " + groupAttrs);
                    }
                    familyAttrs = addAttrsToExistingAttrs(familyAttrs,
                            groupAttrs);

                    // Get group admin attributes if adminRealmDn exists
                    if (groupAttrs.get(LDAPGroupMailboxProperty.adminRealmdn
                            .name()) != null) {
                        String adminRealmDn = groupAttrs
                                .get(LDAPGroupMailboxProperty.adminRealmdn
                                        .name()).get().toString();
                        groupAttrs = null;
                        if (logger.isDebugEnabled()) {
                            logger.debug("adminRealmDn : " + adminRealmDn);
                        }
                        Attributes adminAttrs = getGroupAdminAttributes(adminRealmDn);
                        if (logger.isDebugEnabled()) {
                            logger.debug("adminAttributes : " + adminAttrs);
                        }
                        familyAttrs = addAttrsToExistingAttrs(familyAttrs,
                                adminAttrs);
                        adminAttrs = null;
                    }
                    if (isGroupCachingReq) {
                        groupMap.put(dn, familyAttrs);
                    }
                }

            }
        }
        return familyAttrs;
    }

    @Override
    public void createDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        final String domain = mxosRequestState.getInputParams()
                .get(DomainProperty.domain.name()).get(0);
        final String[] dc = domain.split("\\.");
        try {
            String fulldc = "";
            for (int i = dc.length - 1; i >= 0; i--) {
                if (fulldc.length() == 0) {
                    fulldc = "dc=" + dc[i];
                } else {
                    fulldc = "dc=" + dc[i] + "," + fulldc;
                }
                Attributes attr = null;
                if (i != 0) {
                    attr = LDAPUtils.buildDomainParentAttributes(dc[i]);
                    try {
                        executeLDAPCreate(fulldc.toString(), null, attr);
                    } catch (final NameAlreadyBoundException e) {
                        logger.warn(e);
                    } catch (final InvalidAttributeIdentifierException e) {
                        logger.warn(e);
                    }
                } else {
                    final IBackendState backendState = mxosRequestState
                            .getBackendState().get(MailboxDBTypes.ldap.name());
                    LDAPUtils
                            .buildDomainAttributes(
                                    ((LDAPBackendState) backendState).attributes,
                                    dc[i]);
                    executeLDAPCreate(fulldc.toString(), null,
                            ((LDAPBackendState) backendState).attributes);
                }
            }
        } catch (NameAlreadyBoundException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_ALREADY_EXISTS.name(), e);
        } catch (InvalidAttributeIdentifierException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (InvalidAttributeValueException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (InvalidNameException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_UNABLE_TO_PERFORM_CREATE.name(), e);
        } catch (Exception e) {
            logger.error("LDAP error while create domain.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Get all cos attributes using adminPolicyDn.
     * 
     * @param adminPolicyDn
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getCosAttributes(String adminPolicyDn,
            String[] attributeNames, Map<String, List<String>> inputParams)
            throws MxOSException, NamingException {

        Attributes cosAttrs = null;
        boolean getAllCosAttributes = false;

        if (null != attributeNames
                && attributeNames.equals(LDAPUtils.mailboxAttributes)) {
            getAllCosAttributes = true;
        }

        boolean cosCachingEnabled = java.lang.Boolean.parseBoolean(System
                .getProperty(SystemProperty.ldapCosCachingEnabled.name()));

        if (cosCachingEnabled) {
            Attributes attributes = MxOSApp
                    .getInstance()
                    .getLdapCosCache()
                    .getCosAttributes(adminPolicyDn, getAllCosAttributes, this,
                            attributeNames, inputParams);
            return attributes;
        } else {
            cosAttrs = executeLDAPRetrieve(adminPolicyDn, attributeNames, inputParams);
        }
        return cosAttrs;
    }

    /**
     * 
     * @param groupName
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getGroupMailboxAttributes(String groupDn,
            Map<String, List<String>> inputParams)
            throws MxOSException, NamingException {
        Attributes groupAttrs = executeLDAPRetrieve(groupDn,
                LDAPUtils.groupMailboxAttributes, inputParams);
        return groupAttrs;
    }

    /**
     * 
     * @param adminRealmDn
     * @return
     * @throws MxOSException
     * @throws NamingException
     */
    public Attributes getGroupAdminAttributes(String adminRealmDn)
            throws MxOSException, NamingException {
        Attributes adminAttrs = new BasicAttributes();
        String groupAdminLdapFilter = "objectclass=*";
        final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        SearchControls groupAdminSc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, 0, maxTimeOut,
                LDAPUtils.groupAdminMailboxAttributes, false, false);
        NamingEnumeration<SearchResult> groupAdminSearchResult = null;
        groupAdminSearchResult = executeLDAPGroupSearch(adminRealmDn,
                groupAdminLdapFilter, groupAdminSc);
        while (groupAdminSearchResult != null
                && groupAdminSearchResult.hasMore()) {
            SearchResult searchResults = groupAdminSearchResult.next();
            if (searchResults != null && searchResults.getAttributes() != null) {
                adminAttrs = addAttrsToExistingAttrs(adminAttrs,
                        searchResults.getAttributes());
            }
        }
        return adminAttrs;
    }

    /**
     * Utility method to add new attributes to the given attributes and return.
     * 
     * @param attrs
     * @param newAttrs
     * @return
     * @throws NamingException
     */
    public Attributes addAttrsToExistingAttrs(Attributes attrs,
            Attributes newAttrs) throws NamingException {
        if (attrs != null && newAttrs != null) {
            final NamingEnumeration<? extends Attribute> newAttrsEnum = newAttrs
                    .getAll();
            while (newAttrsEnum.hasMore()) {
                final Attribute attr = newAttrsEnum.next();
                if (attr != null && attrs.get(attr.getID()) == null) {
                    attrs.put(attr);
                }
            }
        }
        return attrs;
    }
    
    /**
     * Utility method to merge new attributes to the given attributes and return.
     * 
     * @param attrs
     * @param newAttrs
     * @return
     * @throws NamingException
     */
    public Attributes mergeAttrsToExistingAttrs(Attributes attrs,
            Attributes newAttrs, List<String> idListFilter) throws NamingException {
        if (attrs != null && newAttrs != null) {
            final NamingEnumeration<? extends Attribute> newAttrsEnum = newAttrs
                    .getAll();
            while (newAttrsEnum.hasMore()) {
                final Attribute attr = newAttrsEnum.next();
                if (attr == null || 
                        idListFilter != null && !idListFilter.contains(attr.getID()))
                    continue;
                if (attrs.get(attr.getID()) == null)
                    attrs.put(attr);
                else
                    attrs.get(attr.getID()).add(attr.get());
            }
        }
        return attrs;
    }

    @Override
    public Domain readDomain(final String domainName, 
            Map<String, List<String>> inputParams) throws MxOSException {
        String fulldc = LDAPUtils.getDomainSearchQuery(domainName);
        Attributes domainResult = null;
        String currentAttribute = null;
        NamingEnumeration<? extends Attribute> namingEnum = null;
        try {
            domainResult = executeLDAPRetrieve(fulldc,
                    LDAPUtils.domainAttributes, inputParams);
            if (domainResult != null) {
                final Domain domain = new Domain();

                namingEnum = domainResult.getAll();
                while (namingEnum.hasMore()) {
                    final Attribute attr = namingEnum.next();
                    currentAttribute = attr.getID();
                    final LDAPDomainProperty ldapDomainProperty = LDAPDomainProperty
                            .valueOf(attr.getID().toLowerCase());
                    if (ldapDomainProperty == LDAPDomainProperty.domainname) {
                        domain.setDomain(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailrewritedomain) {
                        domain.setAlternateDomain(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailwildcardaccount) {
                        domain.setDefaultMailbox(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.mailrelayhost) {
                        domain.setRelayHost(attr.get().toString());
                    } else if (ldapDomainProperty == LDAPDomainProperty.domaintype) {
                        domain.setType(LDAPDomainType.fromLdap(attr.get()
                                .toString()));
                    } else {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Invalid domain attribute name retrieved from "
                                        + "LDAP:" + currentAttribute
                                        + ", input parameters:" + domainName);
                    }
                }
                return domain;
            } else {
                // We assume domainResult being null means domain is not
                // present.
                throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                        "Domain not found." + domainName);
            }
        } catch (final IllegalArgumentException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Invalid domain attribute name retrieved from LDAP:"
                            + currentAttribute + domainName);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } catch (final NameNotFoundException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } catch (final NamingException e) {
            logger.warn("Exception while readDomain", e);
            throw new NotFoundException(DomainError.DMN_NOT_FOUND.name(),
                    "Domain not found " + domainName);
        } finally {
            domainResult = null;
            if (namingEnum != null) {
                try {
                    namingEnum.close();
                } catch (NamingException e) {
                    throw new ApplicationException(
                            ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                }
            }
        }
    }

    @Override
    public List<Domain> searchDomain(final MxOSRequestState mxosRequestState,
            final int searchMaxRows, final int searchMaxTimeout)
            throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, searchMaxRows, searchMaxTimeout,
                LDAPUtils.domainAttributes, false, false);

        // TODO: If email is provided by user then directly do get query instead
        // of search.
        NamingEnumeration<SearchResult> results = null;
        try {
            results = executeLDAPSearch("", formattedLdapFilter, sc, mxosRequestState.getInputParams());
            NamingEnumeration<? extends Attribute> namingEnum = null;
            final List<Domain> domains = new ArrayList<Domain>();
            try {
                while (results != null && results.hasMore()) {
                    SearchResult searchResult = results.next();
                    final Domain domain = new Domain();
                    Attributes attributes = searchResult.getAttributes();
                    namingEnum = attributes.getAll();
                    attributes = null;
                    while (namingEnum.hasMore()) {
                        final Attribute attr = namingEnum.next();
                        final LDAPDomainProperty ldapDomainProperty = LDAPDomainProperty
                                .valueOf(attr.getID().toLowerCase());
                        if (ldapDomainProperty == LDAPDomainProperty.domainname) {
                            domain.setDomain(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailrewritedomain) {
                            domain.setAlternateDomain(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailwildcardaccount) {
                            domain.setDefaultMailbox(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.mailrelayhost) {
                            domain.setRelayHost(attr.get().toString());
                        } else if (ldapDomainProperty == LDAPDomainProperty.domaintype) {
                            domain.setType(LDAPDomainType.fromLdap(attr.get()
                                    .toString()));
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.GEN_INTERNAL_ERROR.name(),
                                    "Invalid domain attribute name retrieved from"
                                            + "LDAP:" + attr.getID()
                                            + ", input parameters:");
                        }
                    }
                    domains.add(domain);
                    searchResult = null;
                }
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            } finally {
                if (namingEnum != null) {
                    namingEnum.close();
                }
                namingEnum = null;
            }
            return domains;
        } catch (Exception e) {
            logger.error("Error while search domain", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    @Override
    public void updateDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String fulldc = LDAPUtils
                    .getDomainSearchQuery(mxosRequestState.getInputParams()
                            .get(DomainProperty.domain.name()).get(0));
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            final Attributes attrs = ((LDAPBackendState) backendState).attributes;
            executeLDAPUpdate(fulldc, attrs);
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateDomain", e);
            throw new InvalidRequestException(DomainError.DMN_NOT_FOUND.name(),
                    e);
        } catch (NamingException e) {
            logger.warn("Exception while updateDomain", e);
            throw new ApplicationException(
                    DomainError.DMN_UNABLE_TO_PERFORM_UPDATE.name(), "Failed to update the LDAP." + e.getMessage());
        }
    }

    /**
     * buildAttributes method creates Attributes object with given attributes.
     * 
     * @return BasicAttribute Attributes
     */
    public static void getDefaultCosAttributes(Attributes attr) {
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        String[] objectclasses = MxOSConfig.getCosObjectClasses();
        for (String oc : objectclasses) {
            ocattr.add(oc);
        }
        attr.put(ocattr);
    }

    /**
     * buildAttributes method creates Attributes object with given attributes.
     * 
     * @return BasicAttribute Attributes
     */
    public static void getDefaultMailboxAttributes(Attributes attr) {
        BasicAttribute ocattr = new BasicAttribute("objectclass");
        String[] objectclasses = MxOSConfig.getMailboxObjectClasses();
        for (String oc : objectclasses) {
            ocattr.add(oc);
        }
        attr.put(ocattr);
    }

    @Override
    public void createMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            // If groupName and type exists in the request parameters create
            // group HOH
            // and if only groupName exists then create child(sub) mailbox in
            // the given group
            // else create a normal mailbox
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.groupName.name())
                    && mxosRequestState.getInputParams().containsKey(
                            MailboxProperty.type.name())) {
                String type = mxosRequestState.getInputParams()
                        .get(MailboxProperty.type.name()).get(0);
                if (type.equals(MailboxType.GROUPADMIN.getType())) {
                    logger.info("Creating groupAdmin mailbox");
                    createFamilyMailboxHOH(mxosRequestState);
                    logger.info("Creating groupAdmin mailbox success");
                } else if (type.equals(MailboxType.GROUPMAILBOX.getType())) {
                    logger.info("Creating groupMailbox(sub mailbox) mailbox");
                    createChildMailbox(mxosRequestState);
                    logger.info("Creating groupMailbox(sub mailbox) mailbox success");
                }
            } else {
                logger.info("Creating mailbox");
                createNormalMailbox(mxosRequestState);
                logger.info("Creating mailbox success");
            }
        } catch (NameAlreadyBoundException e) {
            logger.warn("Mailbox already exists in LDAP and skipping LDAP mailbox creation");
            throw new InvalidRequestException(
                    MailboxError.MBX_ALREADY_EXISTS.name(), e);
        } catch (InvalidAttributeValueException e) {
            logger.warn("Failed due to invalid attribute value", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Invalid Data provided", e);
            if (mxosRequestState.getDbPojoMap()
                    .getProperty(MxOSPOJOs.groupName) != null) {
                throw new InvalidRequestException(
                        MailboxError.MBX_GROUP_NOT_EXISTS.name(), e);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
            }
        } catch (InvalidAttributesException e) {
            logger.warn("Invalid Data provided", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while createMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while createMailbox", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while createMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * 
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createNormalMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException, NamingException {
        String mailboxDn = LDAPUtils.getMailboxDn(mxosRequestState.getInputParams());
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        getDefaultMailboxAttributes(((LDAPBackendState) backendState).attributes);
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        try {
            final String mailboxId = (String) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            attrs.put(LDAPMailboxProperty.mailboxid.name(),
                    mailboxId);
        } catch (NamingException e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }
        executeLDAPCreate(mailboxDn, null,
                ((LDAPBackendState) backendState).attributes);
    }

    /**
     * 
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createChildMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException, NamingException {
        String mailboxDn = LDAPUtils.getDnForGroupMailbox(mxosRequestState
                .getInputParams(),
                mxosRequestState.getDbPojoMap()
                        .getProperty(MxOSPOJOs.groupName));
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        getDefaultMailboxAttributes(((LDAPBackendState) backendState).attributes);
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        try {
            final String mailboxId = (String) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            attrs.put(LDAPMailboxProperty.mailboxid.name(),
                    mailboxId);
        } catch (NamingException e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }
        executeLDAPCreate(mailboxDn, null,
                ((LDAPBackendState) backendState).attributes);
    }

    /**
     * Create
     * 
     * @param mxosRequestState
     * @throws MxOSException
     * @throws NamingException
     */
    private void createFamilyMailboxHOH(final MxOSRequestState mxosRequestState)
            throws MxOSException, NamingException {
        try {
            // Create place holders map
            Map<String, String> replacePlaceholdersMap = createPlaceHoldersMap(mxosRequestState);
            // Get the defaultHOHMap by replacing the place holders from
            // default-group-HOH.properties configuration file
            Map<String, Map<String, List<String>>> defaultHOHMap = LDAPUtils
                    .getDefaultHOHMap(replacePlaceholdersMap);

            if (logger.isDebugEnabled()) {
                logger.debug("Default HOH Map : " + defaultHOHMap);
            }

            /* CREATE adminRealmDn */
            try {
                logger.info("Creating adminRealmDn");
                Attributes createAdminRealmDnAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ADMINREALMDN.name()));
                String adminRealmDn = createAdminRealmDnAttrs.get("dn").get()
                        .toString();
                createAdminRealmDnAttrs.remove("dn");
                executeLDAPCreate(adminRealmDn, null, createAdminRealmDnAttrs);
                logger.info("Create adminRealmDn done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE adminAllocations */
            try {
                logger.info("Creating admin allocations");
                Attributes createAdminAllocsAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ADMINALLOCS.name()));
                String adminAllocsDn = createAdminAllocsAttrs.get("dn").get()
                        .toString();
                createAdminAllocsAttrs.remove("dn");
                executeLDAPCreate(adminAllocsDn, null, createAdminAllocsAttrs);
                logger.info("Create adminAllocations done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE roleHOH */
            try {
                logger.info("Creating roleHOH");
                Attributes createRoleHOHAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ROLEHOH.name()));
                String roleHOHDn = createRoleHOHAttrs.get("dn").get()
                        .toString();
                createRoleHOHAttrs.remove("dn");
                executeLDAPCreate(roleHOHDn, null, createRoleHOHAttrs);
                logger.info("Create roleHOH done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* CREATE organizationalUnit */
            logger.info("Creating organizationalUnit");
            try {
                Attributes organizationalUnitAttrs = getDefaultHOHAtts(defaultHOHMap
                        .get(DefaultHOHPropertyKey.ORGANIZATIONALUNIT.name()));
                String organizationalUnitDn = organizationalUnitAttrs.get("dn")
                        .get().toString();
                organizationalUnitAttrs.remove("dn");
                executeLDAPCreate(organizationalUnitDn, null,
                        organizationalUnitAttrs);
                logger.info("Create organizationalUnit done..!");
            } catch (final NameAlreadyBoundException e) {
                logger.warn(e);
            }

            /* UPDATE organizationUnit */
            logger.info("Updating organizationUnit with self-read ACI");
            Attributes selfReadACIAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.SELFREADACL.name()));
            String selfReadACIDn = selfReadACIAttrs.get("dn").get().toString();
            selfReadACIAttrs.remove("dn");
            executeLDAPUpdate(selfReadACIDn, selfReadACIAttrs);
            logger.info("Updating organizationUnit with self-read ACI done..!");

            /* UPDATE relateAdminRealm */
            logger.info("Updating relateAdminRealm");
            Attributes relateAdminRealmAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.RELATEADMINREALM.name()));
            String relateAdminRealmDn = relateAdminRealmAttrs.get("dn").get()
                    .toString();
            relateAdminRealmAttrs.remove("dn");
            executeLDAPUpdate(relateAdminRealmDn, relateAdminRealmAttrs);
            logger.info("Updating relateAdminRealm done..!");

            /* UPDATE memberAccessACI */
            logger.info("Updating memberAccessACI");
            Attributes memberAccessACIAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.MEMBERACCESSACI.name()));
            String memberAccessACIDn = memberAccessACIAttrs.get("dn").get()
                    .toString();
            memberAccessACIAttrs.remove("dn");
            executeLDAPUpdate(memberAccessACIDn, memberAccessACIAttrs);
            logger.info("Updating memberAccessACI done..!");

            /* UPDATE adminConstraints */
            logger.info("Updating adminConstraints");
            Attributes adminConstraintsAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.ADMINCONSTRAINTS.name()));
            String adminConstraintsDn = adminConstraintsAttrs.get("dn").get()
                    .toString();
            adminConstraintsAttrs.remove("dn");
            executeLDAPUpdate(adminConstraintsDn, adminConstraintsAttrs);
            logger.info("Updating adminConstraints done..!");

            /* CREATE Group Admin */
            logger.info("Creating groupHOH");
            Attributes groupHOHAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.GROUPHOH.name()));
            String groupHOHDn = groupHOHAttrs.get("dn").get().toString();
            groupHOHAttrs.remove("dn");
            executeLDAPCreate(groupHOHDn, null, groupHOHAttrs);
            logger.info("Create groupHOH done...!");

            /* UPDATE adminRealmDn */
            logger.info("Updating memberGroupHOH");
            Attributes groupHOHMemberAttrs = getDefaultHOHAtts(defaultHOHMap
                    .get(DefaultHOHPropertyKey.MEMBERGROUPHOH.name()));
            String groupHOHMemberDn = groupHOHMemberAttrs.get("dn").get()
                    .toString();
            groupHOHMemberAttrs.remove("dn");
            executeLDAPUpdate(groupHOHMemberDn, groupHOHMemberAttrs);
            logger.info("Updating memberGroupHOH done..!");

        } catch (NamingException e) {
            throw e;
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        }
    }

    /**
     * 
     * @param defaultHOHMap
     * @return
     */
    private Attributes getDefaultHOHAtts(Map<String, List<String>> defaultHOHMap) {
        if (defaultHOHMap == null) {
            return null;
        }
        Attributes attrs = new BasicAttributes();
        Iterator<String> keysItr = defaultHOHMap.keySet().iterator();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            List<String> values = defaultHOHMap.get(key);
            if (values.size() > 1) {
                Attribute attr = new BasicAttribute(key);
                Iterator<String> valuesItr = values.iterator();
                while (valuesItr.hasNext()) {
                    attr.add(valuesItr.next());
                }
                attrs.put(attr);
            } else {
                Attribute attr = new BasicAttribute(key);
                attr.add(values.get(0));
                attrs.put(attr);
            }
        }
        return attrs;
    }

    /**
     * createPlaceHoldersMap
     * 
     * @param mxosRequestState
     * @return
     * @throws MxOSException
     */
    private Map<String, String> createPlaceHoldersMap(
            final MxOSRequestState mxosRequestState) throws MxOSException {

        Map<String, String> replacePlaceholdersMap = new HashMap<String, String>();

        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        final Attributes attrs = ((LDAPBackendState) backendState).attributes;
        final String mailboxId;
        final String status;
        final String password;
        final String passwordStoreType;
        final String adminpolicydn;
        try {
            mailboxId = (String) attrs.get(
                    LDAPMailboxProperty.mailboxid.name()).get();
            status = (String) attrs.get(
                    LDAPMailboxProperty.mailboxstatus.name()).get();
            password = (String) attrs.get(
                    LDAPMailboxProperty.mailpassword.name()).get();
            passwordStoreType = (String) attrs.get(
                    LDAPMailboxProperty.mailpasswordtype.name()).get();
            adminpolicydn = (String) attrs.get(
                    LDAPMailboxProperty.adminpolicydn.name()).get();

        } catch (Exception e) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name(),
                    e);
        }

        final String group = mxosRequestState.getInputParams()
                .get(MailboxProperty.groupName.name()).get(0);
        final String origanizationalUnit = LDAPUtils
                .getOrganizationalUnitForGroup(group);
        final String groupName = group.split("\\.")[0];
        final String email = mxosRequestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // Optional params
        final String userNameAsEmail = System
                .getProperty(SystemProperty.storeUserNameAsEmail.name());
        final String[] token = email.split(MxOSConstants.AT_THE_RATE);
        final String userName = java.lang.Boolean.valueOf(userNameAsEmail) ? email
                : token[0];
        final String domain = token[1];

        final String messageStoreHost = mxosRequestState.getAdditionalParams()
                .getProperty(MailboxProperty.messageStoreHost.name());
        final String autoReplyHost = mxosRequestState.getAdditionalParams()
                .getProperty(MailboxProperty.autoReplyHost.name());

        replacePlaceholdersMap.put("{groupName}", groupName);
        replacePlaceholdersMap.put("{mailboxId}", mailboxId);
        replacePlaceholdersMap.put("{mailboxStatus}", status);
        replacePlaceholdersMap.put("{password}", password);
        replacePlaceholdersMap.put("{passwordStoreType}", passwordStoreType);
        replacePlaceholdersMap.put("{cosId}", adminpolicydn);
        replacePlaceholdersMap.put("{email}", email);
        replacePlaceholdersMap.put("{domain}", domain);
        replacePlaceholdersMap.put("{userName}", userName);
        replacePlaceholdersMap.put("{organizationalUnit}", origanizationalUnit);
        replacePlaceholdersMap.put("{messageStoreHost}", messageStoreHost);
        replacePlaceholdersMap.put("{autoReplyHost}", autoReplyHost);
        return replacePlaceholdersMap;
    }

    @Override
    public Mailbox readMailbox(MxOSRequestState requestState, final String uid) throws MxOSException {
        try {
            Map<String, List<String>> inputParams = requestState.getInputParams();
            if (!inputParams.containsKey(MailboxProperty.adminControl.name())) {
                addParam(inputParams, MailboxProperty.adminControl,
                        MxOSConfig.isGroupMailboxEnabled() ? "true" : "false");
            }
            final Map<String, Attributes> cosMap = new LinkedHashMap<String, Attributes>();
            final Attributes mailboxResult = executeLDAPRetrieve(
                    getMailboxFilter(uid),
                    LDAPUtils.mailboxConfiguredAttributes, inputParams, true,
                    cosMap, true);
            if (mailboxResult != null) {
                final Mailbox mailbox = new Mailbox();
                mailbox.setBase(new Base());
                mailbox.setWebMailFeatures(new WebMailFeatures());
                mailbox.setCredentials(new Credentials());
                mailbox.setGeneralPreferences(new GeneralPreferences());
                mailbox.setMailSend(new MailSend());
                mailbox.setMailReceipt(new MailReceipt());
                mailbox.setMailAccess(new MailAccess());
                mailbox.setMailStore(new MailStore());
                mailbox.setInternalInfo(new InternalInfo());
                mailbox.setSmsServices(new SmsServices());
                mailbox.setSocialNetworks(new SocialNetworks());
                mailbox.setExternalAccounts(new ExternalAccounts());
                LDAPUtils.populateMailbox(mailbox, mailboxResult);
                return mailbox;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailbox", e);
            throw e;
        } catch (NamingException e) {
            logger.warn("Exception while readMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while readMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<Mailbox> readMultipleMailboxes(MxOSRequestState requestState,
            final List<String> list) throws MxOSException {
        List<Mailbox> mailboxes = new ArrayList<Mailbox>();
        try {
            Map<String, List<String>> inputParams = requestState.getInputParams();
            if (!inputParams.containsKey(MailboxProperty.adminControl.name())) {
                addParam(inputParams, MailboxProperty.adminControl,
                        MxOSConfig.isGroupMailboxEnabled() ? "true" : "false");
            }
            final Map<String, Attributes> cosMap = new LinkedHashMap<String, Attributes>();
            for (String email : list) {
                try {
                    final Attributes mailboxResult = executeLDAPRetrieve(
                            getMailboxFilter(email),
                            LDAPUtils.mailboxConfiguredAttributes, inputParams, true,
                            cosMap, true);
                    if (mailboxResult != null) {
                        final Mailbox mailbox = new Mailbox();
                        mailbox.setBase(new Base());
                        mailbox.setWebMailFeatures(new WebMailFeatures());
                        mailbox.setCredentials(new Credentials());
                        mailbox.setGeneralPreferences(new GeneralPreferences());
                        mailbox.setMailSend(new MailSend());
                        mailbox.setMailReceipt(new MailReceipt());
                        mailbox.setMailAccess(new MailAccess());
                        mailbox.setMailStore(new MailStore());
                        mailbox.setInternalInfo(new InternalInfo());
                        mailbox.setSmsServices(new SmsServices());
                        mailbox.setSocialNetworks(new SocialNetworks());
                        mailbox.setExternalAccounts(new ExternalAccounts());
                        LDAPUtils.populateMailbox(mailbox, mailboxResult);
                        mailboxes.add(mailbox);
                    } else {
                        logger.warn(new StringBuffer().append("Mailbox ")
                                .append(email).append(" not found"));
                    }
                } catch (MxOSException e) {
                    // Skip and Do next if mailbox is not available or not
                    // found.
                    logger.error("Error while getting Mailbox for email, "
                            + email + " while multiple GET for mailbox", e);
                }
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (Exception e) {
            logger.error("Error while readMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
        return mailboxes;
    }

    @Override
    public List<Base> searchMailbox(final MxOSRequestState mxosRequestState,
            final int maxSearchRows, final int maxSearchTimeout)
            throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final String ldapBaseDn;
        if (mxosRequestState.getInputParams().get(MailboxProperty.scope.name()) != null) {
            ldapBaseDn = LDAPUtils.getScopeSearchQuery(mxosRequestState
                    .getInputParams().get(MailboxProperty.scope.name()).get(0));
        } else
            ldapBaseDn = "";
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAttributes, false, false);

        NamingEnumeration<SearchResult> results = null;
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        final long t0 = Stats.startTimer();
        try {
            results = executeLDAPSearch(ldapBaseDn, formattedLdapFilter, sc,
                    mxosRequestState.getInputParams());
            final List<Base> bases = new ArrayList<Base>();
            try {
                while (results != null && results.hasMore()) {
                    SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        Attributes attrs = searchResult.getAttributes();

                        String groupName = searchResult.getNameInNamespace();
                        if (logger.isDebugEnabled()) {
                            logger.debug("GroupName : " + groupName);
                        }
                        if (MxOSConfig.isGroupMailboxEnabled()) {
                            // Check and retrieve family mailbox attributes
                            Attributes groupAttrs = retrieveFamilyMailboxAttrs(
                                    attrs, groupName, true, mxosRequestState.getInputParams());
                            attrs = addAttrsToExistingAttrs(attrs, groupAttrs);
                            groupAttrs = null;
                        }
                        Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime,
                                Operation.SEARCH, t0, StatStatus.pass);
                        Attribute cosIdAttr = attrs
                                .get(LDAPMailboxProperty.adminpolicydn.name());
                        if (cosIdAttr != null && cosIdAttr.get() != null) {
                            Attributes cosResult = null;
                            // check the cos is already exists in cosCache
                            // read otherwise.
                            if (cosCache
                                    .containsKey(cosIdAttr.get().toString())) {
                                cosResult = cosCache.get(cosIdAttr.get()
                                        .toString());
                            } else {
                                cosResult = getCosAttributes(cosIdAttr.get()
                                        .toString(),
                                        LDAPUtils.mailboxAttributes,
                                        mxosRequestState.getInputParams());
                                cosCache.put(cosIdAttr.get().toString(),
                                        cosResult);
                                cosIdAttr = null;
                            }
                            if (cosResult != null) {
                                // add cos data into mailbox data
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosResult);
                                cosResult = null;
                            }
                        }
                        final Base base = new Base();
                        try {
                            LDAPUtils.populateBase(base, attrs);
                            bases.add(base);
                        } catch (Throwable e) {
                            logger.error("Problem while populating Mailbox : "
                                    + base);
                        }
                    }
                    searchResult = null;
                } // End of while
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            }
            return bases;
        } catch (NamingException e) {
            logger.warn("Exception while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime,
                    Operation.SEARCH, t0, StatStatus.fail);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SEARCH.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime,
                    Operation.SEARCH, t0, StatStatus.fail);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searchMailbox", e);
            Stats.stopTimer(ServiceEnum.LDAPSearchMailboxTime,
                    Operation.SEARCH, t0, StatStatus.fail);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                    results = null;
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    /**
     * Retrieve all attributes in the given ldapBaseDn.
     * 
     * @param ldapBaseDn.
     * @return attributes
     * @throws MxOSException.
     */

    public Attributes readAdminAllocationsAttributes(final String ldapBaseDn,
            Map<String, List<String>> inputParams)
            throws MxOSException {
        NamingEnumeration<SearchResult> results = null;
        Attributes attrs = null;
        // COS Caching
        final Map<String, Attributes> cosCache = new HashMap<String, Attributes>();
        try {
            final int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
            final SearchControls sc = new SearchControls(
                    SearchControls.SUBTREE_SCOPE, 0, maxTimeOut,
                    LDAPUtils.mailboxAttributes, false, false);
            if (ldapBaseDn != null) {
                results = executeLDAPSearch(ldapBaseDn, "objectclass=*", sc, inputParams);
            }
            int adminNumUsers = 0;
            List<String> subMailboxArray = new ArrayList<String>();
            long adminUsedStorageSizeKB = 0;
            try {
                while (results != null && results.hasMore()) {
                    SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        attrs = searchResult.getAttributes();
                        Attribute cosIdAttr = attrs
                                .get(LDAPMailboxProperty.adminpolicydn.name());
                        if (cosIdAttr != null && cosIdAttr.get() != null) {
                            Attributes cosResult = null;
                            // check the cos is already exists in cosCache
                            // read otherwise.
                            if (cosCache
                                    .containsKey(cosIdAttr.get().toString())) {
                                cosResult = cosCache.get(cosIdAttr.get()
                                        .toString());
                            } else {
                                cosResult = getCosAttributes(cosIdAttr.get().toString(),
                                        LDAPUtils.mailboxAttributes, inputParams);
                                cosCache.put(cosIdAttr.get().toString(),
                                        cosResult);
                            }
                            if (cosResult != null) {
                                // add cos data into mailbox data
                                attrs = addAttrsToExistingAttrs(attrs,
                                        cosResult);
                                cosIdAttr = null;
                                cosResult = null;
                            }
                        }
                        if (MxOSConfig.isGroupAdminIncluded()
                                || (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name()) != null && (attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                                .name())
                                        .get()
                                        .toString()
                                        .equalsIgnoreCase(
                                                MailboxType.GROUPMAILBOX
                                                        .getValue())
                                    || attrs
                                        .get(LDAPMailboxProperty.mailfamilymailbox
                                            .name())
                                        .get()
                                        .toString()
                                        .equalsIgnoreCase(
                                                MailboxType.NORMAL
                                                    .getValue())))) {
                            if (attrs.get(LDAPMailboxProperty.mail.name()) != null) {
                                adminNumUsers++;
                                subMailboxArray.add(attrs
                                        .get(LDAPMailboxProperty.mail.name())
                                        .get().toString());
                            }
                            if (attrs.get(LDAPMailboxProperty.mailquotatotkb
                                    .name()) != null) {
                                adminUsedStorageSizeKB = adminUsedStorageSizeKB
                                        + Long.parseLong(attrs
                                                .get(LDAPMailboxProperty.mailquotatotkb
                                                        .name()).get()
                                                .toString());
                            }
                        }
                    }
                    searchResult = null;
                } // End of while
                if (logger.isDebugEnabled()) {
                    logger.debug("value of adminNumUsers is :" + adminNumUsers);
                    logger.debug("value of derived mailwquota is :"
                            + adminUsedStorageSizeKB);
                }
                attrs.put(LDAPGroupAdminMailboxProperty.adminnumusers.name(),
                        adminNumUsers);
                attrs.put(
                        LDAPGroupAdminMailboxProperty.adminusedstoragekb.name(),
                        adminUsedStorageSizeKB);
            } catch (SizeLimitExceededException e) {
                logger.warn("Exception get groupAdminAllocation attributes", e);
                throw e;
            } catch (TimeLimitExceededException e) {
                logger.warn("Exception get groupAdminAllocation attributes", e);
                throw e;
            }
            return attrs;
        } catch (NamingException e) {
            logger.warn("Exception while get groupAdminAllocation attributes",
                    e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while get groupAdminAllocation attributes",
                    e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while get groupAdminAllocation attributes", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                    results = null;
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    /**
     * Search Credentials objects for mail/maillogin/mailalternateaddress.
     * 
     * @param mxosRequestState - input request state.
     * @return Map<String, Credentials>.
     * @throws MxOSException on Credentials(s) not found or any other error.
     */
    @Override
    public Credentials searchCredentials(
            final MxOSRequestState requestState) throws MxOSException {

        String filter;
        
        // Make the query filter. Key is either an email address or a user name. 
        Map<String, List<String>> inputParams = requestState.getInputParams();
        final String email = CRUDUtils.getSingleParameter(inputParams, MailboxProperty.email); 
        if (email != null) {
            filter = LdapFilterFactory.getEmailFilter(email);
        } else {
            final String userName = CRUDUtils.getSingleParameter(inputParams,
                    MailboxProperty.userName);
            filter = LdapFilterFactory.getUserNameFilter(userName);
        }
        
        // conduct the search
        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        Credentials credentials = searchCredentialsInternal(filter, requestState,
                MxOSConstants.MAX_SEARCH_ROWS, maxTimeOut);
        
        if (credentials == null) {
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name());
        }
        
        return credentials;
    }

    /**
     * Called by searchCredentials and conducts LDAP search to query for
     * credentials of a subscriber. Search key is either email address or login
     * name. Apart from returning Credentials object, this method puts a
     * MssLinkInfo into the given MxOSRequestState.
     * 
     * @param queryFilter LDAP filter to be used for search.
     * @param mxosRequestState Operation context.
     * @param maxSearchRows Maximum search rows.
     * @param maxSearchTimeout Search timeout value.
     * @return The credentials of the specified subscriber. The method returns
     *         null if the subscriber was not found.
     * @throws MxOSException
     */
    private Credentials searchCredentialsInternal(
            final String queryFilter,
            final MxOSRequestState mxosRequestState, final int maxSearchRows,
            final int maxSearchTimeout) throws MxOSException {
        
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAuthenticateAttributes, false, false);

        NamingEnumeration<SearchResult> results = null;
        Credentials credentials = null;
        try {
            results = executeLDAPSearch("", queryFilter, sc,
                    mxosRequestState.getInputParams());

            if (results != null && results.hasMore()) {
                SearchResult searchResult = results.next();
                if (searchResult != null
                        && searchResult.getAttributes() != null) {
                    final Attributes attrs = searchResult.getAttributes();
                    
                    // Make output Credentials
                    credentials = new Credentials();
                    LDAPUtils.populateCredentials(credentials, attrs);
                    
                    // Make output MssLinkInfo
                    final MssLinkInfo info = new MssLinkInfo();
                    LDAPUtils.populateMSSLinkInfo(info, attrs);
                    mxosRequestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.mssLinkInfo, info);
                }
                
                searchResult = null;
            }

        } catch (NamingException e) {
            logger.warn("Exception while searching Credentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SEARCH_CREDENTIALS.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searching Credentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searching Credentials", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                    results = null;
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
        
        return credentials;
    }

    @Override
    public Credentials readRawCredentials(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            final Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateRawCredentials(credentials, attrs);
                return credentials;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading raw Credentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void updateMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            String mailboxDn = null;
            if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.groupAdminAllocCountsDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.groupAdminAllocCountsDn);
            } else if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.adminRealmDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.adminRealmDn);
            } else if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.familyMailboxDn) != null) {
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.familyMailboxDn);
            } else {
                mailboxDn = resolveDN(mxosRequestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0), mxosRequestState.getInputParams());
            }
            
            LdapEntryBackendState leState = LdapEntryBackendState.getState(mxosRequestState);
            for (LdapEntryBackendState.Entry entry : leState.getEntries()) {
                if (entry.tags.contains(LdapEntryBackendState.Tag.OP_MODIFY_ATTRIBUTES)){
                    logger.debug("modifying entry " + entry);
                    executeLDAPUpdate(entry.dn, entry.attributes);
                } else  if (entry.tags.contains(LdapEntryBackendState.Tag.OP_ADD_ENTRY)) {
                    // request may contain adding a related LDAP entry.
                    logger.debug("adding entry " + entry);
                    executeLDAPCreate(entry.dn, null, entry.attributes);
                }
                else {
                    logger.debug("ignoring entry " + entry);
                }
            }
            
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            if (backendState.getSize() > 0) {
                final Attributes attrs = ((LDAPBackendState) backendState).attributes;
                executeLDAPUpdate(mailboxDn, attrs);
            }
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while updateMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_UPDATE.name(),
                    "Failed to update the LDAP." + e.getMessage());
        } catch (MxOSException e) {
            logger.warn("Exception while updateMailbox", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updateMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void deleteMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            String mailboxDn = null;
            if (mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.familyMailboxDn) != null) {
                if (mxosRequestState.getDbPojoMap().getProperty(MxOSPOJOs.type) != null) {
                    if (mxosRequestState
                            .getDbPojoMap()
                            .getProperty(MxOSPOJOs.type)
                            .equalsIgnoreCase(MailboxType.GROUPADMIN.getValue())) {
                        logger.error("Delete not allowed for group admin account.");
                        throw new InvalidRequestException(
                                MailboxError.MBX_UNABLE_TO_DELETE_GROUP_ADMIN
                                        .name(),
                                "Mailbox delete not allowed for group admin(HOH) account.");
                    }
                }
                mailboxDn = mxosRequestState.getDbPojoMap().getProperty(
                        MxOSPOJOs.familyMailboxDn);
            } else {
                mailboxDn = resolveDN(mxosRequestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0), mxosRequestState.getInputParams());
            }
            
            // LEAPFROG-3145 delete any child nodes whose cn=senders
            String sendersDN = null;
            String ldapFilter = "objectclass=sendersControl";
            SearchControls sc = new SearchControls(SearchControls.ONELEVEL_SCOPE, 0, 0, null, false, false);
            NamingEnumeration<SearchResult> results = executeLDAPSearch(
                    mailboxDn, ldapFilter, sc, mxosRequestState.getInputParams());
            while (results != null && results.hasMore()) {
                SearchResult searchResult = results.next();
                if (searchResult != null && searchResult.getAttributes() != null) {
                    sendersDN = searchResult.getNameInNamespace();
                    if (sendersDN != null) {
                        logger.debug("Deleting the node whose dn=" + sendersDN);
                        executeLDAPDelete(sendersDN);
                    }
                }
            }
            results = null;

            executeLDAPDelete(mailboxDn);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new NotFoundException(ErrorCode.LDP_NO_SUCH_ATTRIBUTE.name(),
                    e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.warn("Exception while deleteMailbox", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP delete.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final NamingException e) {
            logger.warn("Exception while deleteMailbox", e);
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final MxOSException e) {
            logger.error("Error while deleteMailbox", e);
            throw e;
        } catch (final Exception e) {
            logger.error("Error while deleteMailbox", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void commit() throws ApplicationException {
        // Not required for LDAP

    }

    @Override
    public void rollback() throws ApplicationException {
        // Not required for LDAP
    }

    @Override
    public boolean authenticate(String email, String password)
            throws MxOSException {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public Base readMailboxBase(MxOSRequestState mxosRequestState, String email)
            throws MxOSException {
        try {
            Map<String, List<String>> inputParams = null;
            if(mxosRequestState != null){
                inputParams = mxosRequestState.getInputParams();
                if (!inputParams.containsKey(MailboxProperty.adminControl.name())) {
                    addParam(inputParams, MailboxProperty.adminControl,
                            MxOSConfig.isGroupMailboxEnabled() ? "true" : "false");
                }
            }
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final Base base = new Base();
                base.setAllowedDomains(new ArrayList<String>());
                base.setEmailAliases(new ArrayList<String>());
                LDAPUtils.populateBase(base, attrs);
                attrs = null;
                return base;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailboxBase", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailboxBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public WebMailFeatures readWebMailFeatures(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final WebMailFeatures wmf = new WebMailFeatures();
                LDAPUtils.populateWebMailFeatures(wmf, attrs);
                attrs = null;
                return wmf;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readWebMailFeatures", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readWebMailFeatures", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public WebMailFeatures readCosWebMailFeatures(String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            final WebMailFeatures wmf = new WebMailFeatures();
            Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId), mxosRequestState);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateWebMailFeatures(wmf, attrs);
            attrs = null;
            return wmf;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosWebMailFeatures", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading WebMailFeatures for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public Credentials readCredentials(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateCredentials(credentials, attrs);
                attrs = null;
                return credentials;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCredentials", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCredentials", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCredentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCredentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GeneralPreferences readGeneralPreferences(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final GeneralPreferences gp = new GeneralPreferences();
                LDAPUtils.populateGeneralPreferences(gp, attrs);
                attrs = null;
                return gp;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readGeneralPreferences", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readGeneralPreferences", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailSend readMailSend(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final MailSend ms = new MailSend();
                LDAPUtils.populateMailSend(ms, attrs);
                attrs = null;
                return ms;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailSend", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailSend", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailSend", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailSend", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailReceipt readMailReceipt(final MxOSRequestState mxosRequestState,
            String email) throws MxOSException {
        try {
            if (!mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.adminControl.name())) {
                addParam(mxosRequestState.getInputParams(),
                        MailboxProperty.adminControl,
                        MxOSConfig.isGroupMailboxEnabled() ? "true" : "false");
            }
            Attributes attrs = executeLDAPRetrieve(getMailboxFilter(email),
                    mxosRequestState);
            if (attrs != null) {
                final MailReceipt mr = new MailReceipt();
                LDAPUtils.populateMailReceipt(mr, attrs);
                attrs = null;
                return mr;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailReceipt", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailReceipt", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    public void searchSendersControlLists(String userDn, Attributes parentAttrs,
            MxOSRequestState mxosRequestState) throws NamingException, MxOSException {
        NamingEnumeration<SearchResult> results = null;
        String ldapFilter = "objectclass=sendersControl";
        String requestAttrs[] = {
                LDAPMailboxProperty.mailapprovedsenderslist.name(),
                LDAPMailboxProperty.mailblockedsenderslist.name()
        };
        
        if (mxosRequestState != null) {
            // Put the senders control list items in the parent entry
            // into the context.
            // NOTE: We clone the attributes here since the original attrs
            //       may change later.
            
            Attributes savingAttrs = new BasicAttributes();
            for (String attributeType : requestAttrs) {
                Attribute attr = parentAttrs.get(attributeType);
                if (attr != null) {
                    Attribute savingAttr = new BasicAttribute(attributeType);
                    NamingEnumeration<? extends Object> valueEnum = attr.getAll();
                    while (valueEnum.hasMore()) {
                        savingAttr.add(valueEnum.next());
                    }
                    savingAttrs.put(savingAttr);
                }
            }
            LdapEntryBackendState.addEntry(mxosRequestState, userDn, savingAttrs,
                    LdapEntryBackendState.Tag.ENTRY_USER);
        }
        
        SearchControls sc = new SearchControls(
                SearchControls.OBJECT_SCOPE, 0, 0, requestAttrs, false, false);
        try {
            String entryDn = "cn=senders," + userDn;
            results = executeLDAPSearch(entryDn, ldapFilter, sc, mxosRequestState.getInputParams());
        } catch (NamingException ex){
            logger.debug("No SearchControl objects exist", ex);
        }
        while (results != null && results.hasMore()) {
            SearchResult result = results.next();
            Attributes controlLists = result.getAttributes();
            if (mxosRequestState != null) {
                LdapEntryBackendState.addEntry(mxosRequestState,
                        result.getNameInNamespace(), controlLists,
                        LdapEntryBackendState.Tag.ENTRY_SENDERS_CONTROL);
            }
            // put clones of found attributes into parent
            NamingEnumeration<? extends Attribute> attrEnum = controlLists.getAll();
            while (attrEnum.hasMore()) {
                Attribute controlList = attrEnum.next();
                Attribute parentAttr = parentAttrs.get(controlList.getID());
                if (parentAttr == null) {
                    parentAttr = new BasicAttribute(controlList.getID());
                    parentAttrs.put(parentAttr);
                }
                NamingEnumeration<? extends Object> valueEnum = controlList.getAll();
                while (valueEnum.hasMore()) {
                    parentAttr.add(valueEnum.next());
                }
            }
        }
    }

    @Override
    public MailAccess readMailAccess(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final MailAccess ma = new MailAccess();
                ma.setAllowedIPs(new ArrayList<String>());
                LDAPUtils.populateMailAccess(ma, attrs);
                attrs = null;
                return ma;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailAccess", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailAccess", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailAccess", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailAccess", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
    
    @Override
    public MailStore readMailStore(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            addParam(mxosRequestState.getInputParams(), MailboxProperty.adminControl,
                    MxOSConfig.isGroupMailboxEnabled() ? "true" : "false");
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final MailStore ms = new MailStore();
                LDAPUtils.populateMailStore(ms, attrs);
                attrs = null;
                return ms;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailStore", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailStore", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMailStore", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailStore", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public SmsServices readSmsServices(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final SmsServices smsServices = new SmsServices();
                LDAPUtils.populateSmsServices(smsServices, attrs);
                attrs = null;
                return smsServices;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSmsServices", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSmsServices", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSmsServices", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSmsServices", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public InternalInfo readInternalInfo(String email,
            MxOSRequestState requestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), requestState);
            if (attrs != null) {
                final InternalInfo info = new InternalInfo();
                info.setMessageEventRecords(new MessageEventRecords());
                LDAPUtils.populateInternalInfo(info, attrs);
                final MssLinkInfo mssLinkInfo = new MssLinkInfo();
                LDAPUtils.populateMSSLinkInfo(mssLinkInfo, attrs);
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.mssLinkInfo,
                        mssLinkInfo);
                attrs = null;
                return info;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readInternalInfo", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readInternalInfo", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MessageEventRecords readMessageEventRecords(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final MessageEventRecords msgEventRecords = new MessageEventRecords();
                LDAPUtils.populateMessageEventRecords(msgEventRecords, attrs);
                attrs = null;
                return msgEventRecords;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readMessageEventRecords", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMessageEventRecords", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public InternalInfo readCosInternalInfo(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final InternalInfo info = new InternalInfo();
            info.setMessageEventRecords(new MessageEventRecords());
            Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId), mxosRequestState);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateInternalInfo(info, attrs);
            info.setVersion(null);
            attrs = null;
            return info;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosInternalInfo", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading InternalInfo for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MessageEventRecords readCosMessageEventRecords(String cosId,
            MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final MessageEventRecords mers = new MessageEventRecords();
            Attributes attrs = executeLDAPRetrieve(getCosFilter(cosId),
                    mxosRequestState);
            if (null == attrs) {
                throw new NameNotFoundException();
            }
            LDAPUtils.populateMessageEventRecords(mers, attrs);
            attrs = null;
            return mers;
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMessageEventRecords", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while reading MessageEventRecords for Cos.", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MssLinkInfo readMSSLinkInfo(String email, Map<String, List<String>> inputParams)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieveMSSLinkAttributes(
                    getMailboxFilter(email), inputParams);
            if (attrs != null) {
                final MssLinkInfo info = new MssLinkInfo();
                LDAPUtils.populateMSSLinkInfo(info, attrs);
                attrs = null;
                return info;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMSSLinkInfo", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMSSLinkInfo", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMSSLinkInfo", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public String readMailboxRealm(String email, Map<String, List<String>> inputParams)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieveMSSLinkAttributes(
                    getMailboxFilter(email), inputParams);
            if (attrs != null) {
                final MssLinkInfo info = new MssLinkInfo();
                LDAPUtils.populateMSSLinkInfo(info, attrs);
                attrs = null;
                return info.getRealm();
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readMailboxRealm", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readMailboxRealm", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailboxRealm", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
    
    @Override
    public SocialNetworks readSocialNetworks(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final SocialNetworks sn = new SocialNetworks();
                sn.setSocialNetworkSites(new ArrayList<SocialNetworkSite>());
                LDAPUtils.populateSocialNetworks(sn, attrs);
                attrs = null;
                return sn;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSocialNetworks", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSocialNetworks", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSocialNetworks: " + e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSocialNetworks", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Method to dump attributes.
     * 
     * @param attrs attrs
     */
    public void dumpAttributes(Attributes attrs) {
        try {
            final NamingEnumeration<? extends Attribute> namingEnum = attrs
                    .getAll();
            while (namingEnum != null && namingEnum.hasMore()) {
                final Attribute attr = namingEnum.next();
                logger.debug("Key = " + attr.getID() + ", Value = "
                        + attr.get().toString());
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void createCos(MxOSRequestState requestState) throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(requestState
                    .getInputParams());
            IBackendState backendState = requestState.getBackendState().get(
                    MailboxDBTypes.ldap.name());
            getDefaultCosAttributes(((LDAPBackendState) backendState).attributes);
            // To dump the attributes.
            // dumpAttributes(((LDAPBackendState) backendState).attributes);
            executeLDAPCreate(cosDn, null,
                    ((LDAPBackendState) backendState).attributes);
        } catch (NameAlreadyBoundException e) {
            logger.warn("Exception while createCos", e);
            throw new InvalidRequestException(
                    CosError.COS_ALREADY_EXISTS.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while createCos", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_CREATE.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while createCos: " + e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while createCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }

    }

    @Override
    public Base readCosBase(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final Base base = new Base();
                LDAPUtils.populateBase(base, attrs);
                // type should not passed in cos base api.
                base.setType(null);
                attrs = null;
                return base;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosBase", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosBase", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosBase", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public SocialNetworks readCosSocialNetworks(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final SocialNetworks sn = new SocialNetworks();
                LDAPUtils.populateSocialNetworks(sn, attrs);
                attrs = null;
                return sn;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosSocialNetworks", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void updateCos(MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(mxosRequestState
                    .getInputParams());
            final IBackendState backendState = mxosRequestState
                    .getBackendState().get(MailboxDBTypes.ldap.name());
            Attributes attrs = ((LDAPBackendState) backendState).attributes;
            if (attrs == null || attrs.size() == 0) {
                throw new InvalidRequestException(
                        ErrorCode.GEN_BAD_REQUEST.name());
            }
            executeLDAPUpdate(cosDn, attrs);
            attrs = null;
            // cosMap.put(cosDn, attrs);
        } catch (InvalidNameException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while updateCos", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_UPDATE.name(), "Failed to update the LDAP." + e.getMessage());
        } catch (MxOSException e) {
            logger.warn("Exception while updateCos", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updateCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void deleteCos(String cosId) throws MxOSException {
        try {
            final String cosDn = LDAPUtils.getCosDn(cosId);
            executeLDAPDelete(cosDn);
            // cosMap.remove(cosDn);
        } catch (final NoSuchAttributeException e) {
            logger.warn("Exception while deleteCos", e);
            throw new NotFoundException(ErrorCode.LDP_NO_SUCH_ATTRIBUTE.name(),
                    e);
        } catch (NameNotFoundException e) {
            logger.warn("Exception while deleteCos", e);
            throw new InvalidRequestException(CosError.COS_NOT_FOUND.name(), e);
        } catch (final TimeLimitExceededException e) {
            logger.warn("Exception while deleteCos", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        } catch (final OperationNotSupportedException e) {
            this.status = false;
            logger.error("Naming error during LDAP deleteCOS.", e);
            ConnectionErrorStats.LDAP.increment();
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final InvalidAttributeValueException e) {
            logger.warn("Exception while deleteCos", e);
            throw new InvalidRequestException(CosError.COS_COSID_IN_USE.name(),
                    e);
        } catch (final NamingException e) {
            logger.warn("Exception while deleteCos", e);
            throw new ApplicationException(ErrorCode.LDP_NAMING_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Error while deleteCos", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<Base> searchCos(MxOSRequestState mxosRequestState,
            int maxSearchRows, int maxSearchTimeout) throws MxOSException {
        final String formattedLdapFilter = mxosRequestState.getInputParams()
                .get(MailboxProperty.query.name()).get(0);
        final SearchControls sc = new SearchControls(
                SearchControls.SUBTREE_SCOPE, maxSearchRows, maxSearchTimeout,
                LDAPUtils.mailboxAttributes, false, false);

        NamingEnumeration<SearchResult> results = null;
        try {
            if (MxOSConfig.getLdapCosBaseDn().contains(MxOSConstants.COMMA))
                results = executeLDAPSearch(
                        MxOSConfig.getLdapCosBaseDn().substring(
                                MxOSConfig.getLdapCosBaseDn().indexOf(
                                        MxOSConstants.COMMA) + 1),
                        formattedLdapFilter, sc, mxosRequestState.getInputParams());
            else
                results = executeLDAPSearch(MxOSConfig.getLdapCosBaseDn(),
                        formattedLdapFilter, sc, mxosRequestState.getInputParams());
            final List<Base> bases = new ArrayList<Base>();
            try {
                while (results != null && results.hasMore()) {
                    SearchResult searchResult = results.next();
                    if (searchResult != null
                            && searchResult.getAttributes() != null) {
                        final Base base = new Base();
                        Attribute cosId = searchResult.getAttributes().get(
                                MailboxProperty.Cn.name());
                        if (cosId != null) {
                            base.setCosId(cosId.get().toString());
                            cosId = null;
                        }
                        try {
                            LDAPUtils.populateBase(base,
                                    searchResult.getAttributes());
                            bases.add(base);
                        } catch (Throwable e) {
                            logger.error("Problem while populating Cos : "
                                    + base);
                        }
                    }
                    searchResult = null;
                }// End of while
            } catch (SizeLimitExceededException e) {
                // Nothing to do.
            } catch (TimeLimitExceededException e) {
                // Nothing to do.
            }
            return bases;
        } catch (NamingException e) {
            logger.warn("Exception while searchCos", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_PERFROM_SEARCH.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while searchCos", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while search cos", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (results != null) {
                    results.close();
                    results = null;
                }
            } catch (NamingException e) {
                throw new ApplicationException(
                        ErrorCode.LDP_CONNECTION_ERROR.name(), e);
            }
        }
    }

    @Override
    public ExternalAccounts readExternalAccounts(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final ExternalAccounts ea = new ExternalAccounts();
                LDAPUtils.populateExternalAccounts(ea, attrs);
                attrs = null;
                return ea;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readExternalAccounts", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readExternalAccounts", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public ExternalAccounts readCosExternalAccounts(String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final ExternalAccounts extAcc = new ExternalAccounts();
                LDAPUtils.populateExternalAccounts(extAcc, attrs);
                attrs = null;
                return extAcc;
            } else {
                throw new NotFoundException(
                        CosError.COS_EXTERNAL_ACCOUNT_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw new NotFoundException(
                    CosError.COS_EXTERNAL_ACCOUNT_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosExternalAccounts", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosExternalAccounts", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }

    }

    @Override
    public SmsServices readCosSmsServices(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {

        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);

            if (attrs != null) {
                final SmsServices smsServices = new SmsServices();
                LDAPUtils.populateSmsServices(smsServices, attrs);
                attrs = null;
                return smsServices;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }

        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosSmsServices", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosSmsServices", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailAccess readCosMailAccess(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);

            if (attrs != null) {
                final MailAccess ma = new MailAccess();
                LDAPUtils.populateMailAccess(ma, attrs);
                attrs = null;
                return ma;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailAccess", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readMailAccess COS", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailStore readCosMailStore(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final MailStore ms = new MailStore();
                LDAPUtils.populateMailStore(ms, attrs);
                attrs = null;
                return ms;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailStore", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosMailStore", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailReceipt readCosMailReceipt(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final MailReceipt mr = new MailReceipt();
                LDAPUtils.populateMailReceipt(mr, attrs);
                attrs = null;
                return mr;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailReceipt", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GeneralPreferences readCosGeneralPreferences(String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final GeneralPreferences generalPreferences = new GeneralPreferences();
                LDAPUtils.populateGeneralPreferences(generalPreferences, attrs);
                attrs = null;
                return generalPreferences;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosGeneralPreferences", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosPreferences", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public Credentials readCosCredentials(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final Credentials credentials = new Credentials();
                LDAPUtils.populateCredentials(credentials, attrs);
                attrs = null;
                return credentials;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosCredentials", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosCredentials", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public MailSend readCosMailSend(String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final String cosFilter = getCosFilter(cosId);
            Attributes attrs = executeLDAPRetrieve(cosFilter, mxosRequestState);
            if (attrs != null) {
                final MailSend ms = new MailSend();
                LDAPUtils.populateMailSend(ms, attrs);
                attrs = null;
                return ms;
            } else {
                throw new NotFoundException(CosError.COS_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw new NotFoundException(CosError.COS_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw new InvalidRequestException(
                    CosError.COS_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readCosMailSend", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readCosBase", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readForwardingAddresses(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateForwardingAddresses(list, attrs);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readForwardingAddresses", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readForwardingAddresses", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAddressesForLocalDelivery(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAddressesForLocalDelivery(list, attrs);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAddressesForLocalDelivery", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readAddressesForLocalDelivery", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAllowedSendersList(MxOSRequestState mxosRequestState, String email)
            throws MxOSException {
        return readSenderBlockingList(mxosRequestState, email,
                LDAPMailboxProperty.mailapprovedsenderslist);
    }

    @Override
    public List<String> readBlockedSendersList(MxOSRequestState mxosRequestState, String email)
            throws MxOSException {
        return readSenderBlockingList(mxosRequestState, email,
                LDAPMailboxProperty.mailblockedsenderslist);
    }
        
    private List<String> readSenderBlockingList(MxOSRequestState mxosRequestState,
            String email, LDAPMailboxProperty ldapProperty)
            throws MxOSException {
        
        try {
            addParam(mxosRequestState.getInputParams(),
                    MailboxProperty.senderBlocking, "true");
            Attributes attrs = executeLDAPRetrieve(getMailboxFilter(email),
                    mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateListAttribute(list, attrs, ldapProperty);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception in readBlockedSendersList " + ldapProperty.name(), e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception in readBlockedSendersList " + ldapProperty.name(), e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception in readBlockedSendersList " + ldapProperty.name(), e);
            throw e;
        } catch (Exception e) {
            logger.warn("Exception in readBlockedSendersList " + ldapProperty.name(), e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readSieveFiltersBlockedSendersList(String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateSieveFiltersBlockedSendersList(list, attrs);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readSieveFiltersBlockedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readSieveFiltersBlockedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public AdminControl readAdminControl(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            final AdminControl adminControl = new AdminControl();
            
            addParam(mxosRequestState.getInputParams(), MailboxProperty.adminControl, "true");
            
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                LDAPUtils.populateAdminControl(adminControl, attrs);
                attrs = null;
                return adminControl;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readAdminControl", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readAdminControl", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readAdminControl", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readAdminControl", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAdminBlockedSendersList(String email, MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            addParam(mxosRequestState.getInputParams(), MailboxProperty.adminControl, "true");
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAdminBlockedSendersList(list, attrs);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readBlockedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readBlockedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public List<String> readAdminApprovedSendersList(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            addParam(mxosRequestState.getInputParams(), MailboxProperty.adminControl, "true");
            Attributes attrs = executeLDAPRetrieve(
                    getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final List<String> list = new ArrayList<String>();
                LDAPUtils.populateAdminApprovedSendersList(list, attrs);
                attrs = null;
                return list;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readApprovedSendersList", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readApprovedSendersList", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public GroupAdminAllocations readGroupAdminAllocations(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            addParam(mxosRequestState.getInputParams(), MailboxProperty.adminControl, "true");
            Attributes attrs = executeLDAPRetrieve(getMailboxFilter(email), mxosRequestState);
            if (attrs != null) {
                final GroupAdminAllocations groupAdminAllocations = new GroupAdminAllocations();
                LDAPUtils.populateGroupAdminAllocations(groupAdminAllocations,
                        attrs);
                attrs = null;
                return groupAdminAllocations;
            } else {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
        } catch (NameNotFoundException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
        } catch (NamingException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_GET.name(), e);
        } catch (MxOSException e) {
            logger.warn("Exception while readGroupAdminAllocations", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while readGroupAdminAllocations", e);
            throw new ApplicationException(
                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
        }
    }
    
    /**
     * get the mailbox dn from LDAP by email or email aliases
     * 
     * @param email email or email aliases
     * @return mailbox dn
     * @throws MxOSException
     */
    public String resolveDN(String email, Map<String, List<String>> inputParams) throws MxOSException {
        logger.info("LDAP Read DN start for: " + email);
        boolean isAuthoritative = readInputParamBoolean(inputParams,
                MailboxProperty.authoritative, false);
        String mailboxDN = null;
        boolean hostActive = false;
        for (int i = 0; i < numHosts && hostActive == false; i++) {
            int retryCount = 0;
            do {
                    try {
                        if (this.status == false) {
                            getNewContext(retryCount);
                        }
                        final int maxTimeOut = MxOSApp.getInstance()
                                .getSearchMaxTimeOut();
                        final SearchControls sc = new SearchControls(
                                SearchControls.SUBTREE_SCOPE, 1, maxTimeOut,
                                LDAPUtils.mailboxAttributes, false, false);
                        NamingEnumeration<SearchResult> results = ldapContext.search("",
                                getMailboxFilter(email), sc, isAuthoritative);
                        if (results != null && results.hasMore()) {
                            SearchResult searchResult = results.next();
                            if (searchResult != null
                                    && searchResult.getAttributes() != null) {
                                mailboxDN = searchResult.getNameInNamespace();
                                results = null;
                            }
                        }
                        hostActive = true;
                        break;
                    } catch (NameNotFoundException e) {
                        logger.warn("Exception while resolveDN", e);
                        throw new NotFoundException(
                                MailboxError.MBX_NOT_FOUND.name(), e);
                    } catch (NamingException e) {
                        logger.warn("Error while resolveDN: " + e.getMessage());
                        if (this.status == false || LDAPRetryUtility.isRetryRequired(e)) {
                            if (++retryCount <= ldapMaxRetryCount) {
                                logger.info("Error in LDAP connection, retrying count: "
                                        + retryCount);
                                incrementConnectionErrorStats();
                                this.status = false;
                                continue;
                            } else {
                                logger.error("Error while resolveDN. Exceeded ldapMaxRetryCount for host "
                                        + activeHostURL);
                                getNextAvailableHost();
                            }
                        } else {
                            throw new ApplicationException(
                                    ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                        }
                    } catch (final Exception e) {
                        logger.error("Error while resolveDN: ", e);
                        throw new ApplicationException(
                                ErrorCode.LDP_CONNECTION_ERROR.name(), e);
                    }
            } while (retryCount <= ldapMaxRetryCount);
        }
        if (hostActive == false) {
            logger.error("# All directory hosts are down, no further retries..");
            throw new MxOSException(ErrorCode.LDP_CONNECTION_ERROR.name(),
                    "All directory hosts are down.");
        }
        return mailboxDN;
    }
    
    private static String getMailboxFilter(final String uid) {
        return LdapFilterFactory.getMailboxFilter(uid);
    }

    private static String getCosFilter(final String cosId) {
        return LdapFilterFactory.getCosFilter(cosId);
    }
    
    // TODO: move this to utils
    public static boolean readInputParamBoolean(
            Map<String, List<String>> inputParams, DataMap.Property property,
            boolean defaultVal) {
        if(inputParams == null)
            return defaultVal;
        List<String> values = inputParams.get(property.name());
        if (values == null) {
            return defaultVal;
        }
        return java.lang.Boolean.parseBoolean(values.get(0)); 
    }
    
    // TODO: move this to utils
    void addParam(Map<String, List<String>> inputParams, DataMap.Property property, String value) {
        List<String> values = new ArrayList<String>();
        values.add(value);
        inputParams.put(property.name(), values);
    }
    
    private MxOSRequestState makeTempState(Map<String, List<String>> inputParams) {
        MxOSRequestState state = new MxOSRequestState(inputParams,
                ServiceEnum.MailboxService, Operation.GET); // last two are dummy
        ActionUtils.setBackendState(state);
        return state;
    }

    @Override
    public void makeModifyEntryInContext(MxOSRequestState requestState,
            MailboxProperty propertyForRemovingValue,
            MailboxProperty propertyForAddingValue,
            MailboxProperty propertyToModify,
            MailboxError notFoundError) throws MxOSException {
        
        LDAPMailboxProperty attributeType =
                LDAPUtils.JSONToLDAP_Mailbox.get(propertyToModify);
        
        // get input parameters
        String valueToRemove = requestState.getInputParams()
                .get(propertyForRemovingValue.name()).get(0).toLowerCase();
        
        String valueToAdd = null;
        if (propertyForAddingValue != null) {
            valueToAdd = requestState.getInputParams()
                .get(propertyForAddingValue.name()).get(0).toLowerCase();
        }

        // Get source entries from the operation context
        LdapEntryBackendState state = LdapEntryBackendState.getState(requestState);

        // Find an LDAP entry among the source entries that includes the
        // specified value. We make another entry out of the found
        // entry to specify ldapmodify that will be run in the next action.
        //
        boolean foundEntry = false;
        for (LdapEntryBackendState.Entry entry : state.getEntries()) {
            Attributes attrs = entry.attributes;
            List<String> values = new ArrayList<String>();
            
            try {
                LDAPUtils.populateListAttribute(values, attrs, attributeType);
            } catch (Exception e) {
                // Shouldn't be seen in production. There's some bug if we hit this.
                logger.error("Error in converting attribute values to a list of string.", e);
                throw new ApplicationException(MailboxError.MBX_INTERNAL_ERROR.name());
            }
            
            if (values.remove(valueToRemove)) {
                foundEntry = true;
                if (valueToAdd != null) {
                    values.add(valueToAdd);
                }
                
                // Clear current context
                state.clearEntries();
                
                // make a new LDAP entry and add to context
                Attribute newAttr = new BasicAttribute(attributeType.name());
                for (String value : values) {
                    newAttr.add(value);
                }
                Attributes newAttrs = new BasicAttributes();
                newAttrs.put(newAttr);
                
                state.addEntry(entry.dn, newAttrs,
                        LdapEntryBackendState.Tag.OP_MODIFY_ATTRIBUTES);

                // we're done
                break;
            }
        }

        if (!foundEntry) {
            throw new InvalidRequestException(
                    notFoundError.name(),
                    "Target item " + valueToRemove + " does not exist.");
        }
    }

    @Override
    public void addSendersControlListItem(MxOSRequestState requestState,
            MailboxProperty itemProperty, MailboxProperty listProperty, int maxItems,
            MailboxError alreadyExistsError, MailboxError sizeExceedsError)
                    throws MxOSException {
        
        // make input items  
        
        Set<String> itemsToAdd = null;
        List<String> params =
                requestState.getInputParams().get(itemProperty.name());
        
        if (params == null) {
            logger.debug("Property " + itemProperty + " to add is not specified.");
            return;
        } else {
            // make all lower case
            itemsToAdd = new HashSet<String>();
            for (String item : params) {
                itemsToAdd.add(item.toLowerCase());
            }
        }

        // initialization
        LDAPMailboxProperty attributeType =
                LDAPUtils.JSONToLDAP_Mailbox.get(listProperty);

        // Check and update the backend state
        LdapEntryBackendState state = LdapEntryBackendState.getState(requestState);
        String userDn = null;
        LdapEntryBackendState.Entry entryToModify = null;
        int numItems = 0;
        for (LdapEntryBackendState.Entry entry : state.getEntries()) {
            Attributes attrs = entry.attributes;
            Attribute targetAttribute = attrs.get(attributeType.name());
            
            if (targetAttribute != null) {
                // check name conflict 
                try {
                    NamingEnumeration<? extends Object> values = targetAttribute.getAll();
                    while (values.hasMore()) {
                        String value = values.next().toString().toLowerCase();
                        if (itemsToAdd.contains(value)) {
                            throw new InvalidRequestException(alreadyExistsError.name(),
                                   listProperty + " item " + value + " already exists.");
                        }
                    }
                } catch (NamingException ex) {
                    logger.error("unexpected error in getting attribute values", ex);
                    throw new ApplicationException(MailboxError.MBX_INTERNAL_ERROR.name());
                }
                
                // accumulate number of existing items
                numItems += targetAttribute.size();
            }
            
            // entry type specific handling 
            if (entry.tags.contains(LdapEntryBackendState.Tag.ENTRY_SENDERS_CONTROL)) {
                if (targetAttribute == null) {
                    targetAttribute = new BasicAttribute(attributeType.name());
                    attrs.put(targetAttribute);
                }
                for (String item : itemsToAdd) {
                    targetAttribute.add(item);
                }
                entryToModify = entry;
            } else if (entry.tags.contains(LdapEntryBackendState.Tag.ENTRY_USER)) {
                userDn = entry.dn;
            }
        }

        if (maxItems > 0) {
            numItems += itemsToAdd.size();
            if (logger.isDebugEnabled()) {
                logger.debug("number of " + attributeType.name() + " items=" + numItems);
            }
            if (numItems > maxItems) {
                throw new InvalidRequestException(sizeExceedsError.name());
            }
        }

        if (entryToModify != null) {
            // Clear current context to avoid later action would pick up unnecessary ones
            state.clearEntries();
            entryToModify.tags.add(LdapEntryBackendState.Tag.OP_MODIFY_ATTRIBUTES);
            state.addEntry(entryToModify);
        } else {
            logger.debug("making a new sub-entry for senders control");
            // We didnt find existing senders control entry. So we make one.
            if (userDn == null) {
                // it's a bug if we hit this
                logger.error("User entry is missing in backend state");
                throw new ApplicationException(MailboxError.MBX_INTERNAL_ERROR.name());
            }
            state.clearEntries();
            String dn = "cn=senders," + userDn;
            Attributes attrs = new BasicAttributes();
            attrs.put("cn", "senders");
            attrs.put("objectclass", "top");
            attrs.put("objectclass", "senderscontrol");
            for (String item : itemsToAdd) {
                attrs.put(attributeType.name(), item);
            }
            logger.debug("adding entry, dn: " + dn);
            logger.debug("adding entry, attributes: " + attrs);
            LdapEntryBackendState.Entry entry = new LdapEntryBackendState.Entry(dn, attrs);
            entry.tags.add(LdapEntryBackendState.Tag.OP_ADD_ENTRY);
            entry.tags.add(LdapEntryBackendState.Tag.ENTRY_SENDERS_CONTROL);
            state.addEntry(entry);
        }
    }
}
