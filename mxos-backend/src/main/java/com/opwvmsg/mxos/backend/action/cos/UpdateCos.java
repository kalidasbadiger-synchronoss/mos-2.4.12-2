/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update Cos.
 *
 * @author mxos-devs
 */
public class UpdateCos implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateCos.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateCos action start."));
        }        
        if (requestState == null || requestState.getBackendState() == null ||
                requestState.getBackendState().size() == 0) {
            throw new InvalidRequestException(ErrorCode.GEN_BAD_REQUEST.name());
        }
        ICRUDPool<IMailboxCRUD> provCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            provCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = provCRUDPool.borrowObject();

            mailboxCRUD.updateCos(requestState);

            mailboxCRUD.commit();
        } catch (final MxOSException e) {
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while cos update.", e);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        } catch (final Throwable t) {
            logger.error("Error while cos update.", t);
            if (mailboxCRUD != null) {
                mailboxCRUD.rollback();
            }
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
        } finally {
            if (provCRUDPool != null && mailboxCRUD != null) {
                try {
                    provCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateCos action end."));
        }
    }
}
