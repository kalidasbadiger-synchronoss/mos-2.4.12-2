/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.external.ldap;
import java.util.Hashtable;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;

/**
 * LDAP connection pool factory to access LDAP Data from external ldap server.
 * This is required by Apache object pool. *
 * @author mxos-dev
 */
class ExternalLdapObjectFactory extends BasePoolableObjectFactory<ExternalLdapObject> {
    private final Hashtable<String, String> env;
    protected static Logger logger = Logger.getLogger(ExternalLdapObjectFactory.class);

    /**
     * Constructor.
     *
     * @param env Hashtable contains list of all the configurations.
     */
    public ExternalLdapObjectFactory(Hashtable<String, String> env) {
        this.env = env;
    }

    @Override
    public ExternalLdapObject makeObject() throws Exception {
        return new ExternalLdapObject(this.env);
    }

    @Override
    public void destroyObject(final ExternalLdapObject ldapCURObject)
            throws Exception {
        ldapCURObject.close();
    }

    @Override
    public boolean validateObject(final ExternalLdapObject ldapCRUD) {
        final boolean status = ldapCRUD.isConnected();
        // Reset the entire pool if one connection failed
        // This impact the performance in re-establishing the connection
        // for all the objects in the pool.
        if (!status) {
            try {
                ICRUDPool<ExternalLdapObject> provCRUDPool = MxOSApp.getInstance()
                        .getCURLdapPool();
                if (provCRUDPool != null) {
                    provCRUDPool.resetPool();
                }
            } catch (Exception e) {
                logger.error("Error while resetting External LDAP pools.", e);
            }
        }
        return status;
    }
}
