/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.mailbox.info.GetMessageStoreLinkInfo;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete mailbox from MSS
 * 
 * @author mxos-dev
 * 
 */
public class DeleteMailboxFromMSS extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteMailboxFromMSS.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromMSS action start.");
        }

        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // check whether skip mailbox deletion from MSS
        boolean delete = true;
        if (requestState.getInputParams().containsKey(SKIP_MSS)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_MSS).get(0));
            if (skip) {
                delete = false;
                logger.info("Skip mailbox [" + email + "] deletion from MSS.");
            }
        }

        if (delete) {
            // in order to call three sun mailbox deletion actions separately
            // remove the action GetMailboxDn from MailboxService:DELETE in
            // rules.xml
            // So when DeleteMailboxFromMSS action is called
            // Action GetMessageStoreLinkInfo will be called here first
            getMessageStoreLinkInfo(requestState);

            ICRUDPool<IMetaCRUD> metaCRUDPool = null;
            IMetaCRUD metaCRUD = null;
            try {
                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                metaCRUD.deleteMailbox(requestState);
                logger.info("Delete mailbox [" + email
                        + "] from MSS successfully.");

                // Check whether recreate mailbox on MSS
                boolean recreateOnMss = false;
                if (requestState.getInputParams().containsKey(
                        MailboxProperty.recreateOnMss.name())) {
                    recreateOnMss = Boolean.valueOf(requestState
                            .getInputParams()
                            .get(MailboxProperty.recreateOnMss.name()).get(0));
                }
                if (recreateOnMss) {
                    // call CreateMailboxInMSS action to recreate
                    try {
                        logger.info("Start to recreate mailbox [" + email
                                + "] in MSS.");
                        ActionFactory.getInstance().exec(
                                CreateMailboxInMSS.class.getName(),
                                requestState);
                        logger.info("Recreate mailbox [" + email
                                + "] in MSS successfully.");
                    } catch (MxOSException e) {
                        logger.error("Recreate mailbox [" + email
                                + "] in MSS failed.", e);
                        throw e;
                    }
                }
            } catch (MxOSException e) {
                logger.error("Error while deleting mailbox [" + email
                        + "] from MSS.", e);
                throw e;
            } catch (Exception e) {
                logger.error("Error while deleting mailbox [" + email
                        + "] from MSS.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
            } finally {
                CRUDUtils.releaseConnection(metaCRUDPool, metaCRUD);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromMSS action end.");
        }
    }

    private void getMessageStoreLinkInfo(MxOSRequestState requestState)
            throws MxOSException {
        ActionFactory.getInstance().exec(
                GetMessageStoreLinkInfo.class.getName(), requestState);
    }

}
