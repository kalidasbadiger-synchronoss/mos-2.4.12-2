/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.common;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.common.IExternalLoginService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * AddressBook login service exposed to client which is responsible for doing basic
 * addressBook related activities (more specific for OX) e.g login and logout
 * 
 * @author mxos-dev
 */
public class BackendExternalLoginService implements IExternalLoginService {

    private static Logger logger = Logger
            .getLogger(BackendExternalLoginService.class);

    /**
     * Default Constructor.
     */
    public BackendExternalLoginService() {
        logger.info("BackendOXLoginService Service created...");
    }

    @Override
    public ExternalSession login(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ExternalLoginService, Operation.POST);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.POST, t0,
                    StatStatus.pass);
            return (ExternalSession) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.session);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void logout(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ExternalLoginService, Operation.GET);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.GET, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalLoginService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
