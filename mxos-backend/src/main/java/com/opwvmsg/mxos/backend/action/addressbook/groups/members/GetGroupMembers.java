/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.members;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Member;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get group members.
 * 
 * @author mxos-dev
 * 
 */
public class GetGroupMembers implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetGroupMembers.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetGroupMembers action start."));
        }

        try {

            String name = requestState.getInputParams()
                    .get(AddressBookProperty.groupId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(requestState, AddressBookProperty.groupId,
                            name);

            List<Member> memberList = (List<Member>) requestState
                    .getDbPojoMap().getPropertyAsObject(MxOSPOJOs.allMembers);
            String memberId = requestState.getInputParams()
                    .get(AddressBookProperty.memberId.name()).get(0);

            boolean memberExist = false;
            for (Member memberItr : memberList) {
                if (memberItr.getMemberId().equalsIgnoreCase(memberId)) {
                    requestState.getDbPojoMap().setProperty(MxOSPOJOs.members,
                            memberItr);
                    memberExist = true;
                }
            }
            if (!memberExist) {
                throw new AddressBookException(
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(),
                        ExceptionUtils.NOT_FOUND_EXCEPTION_CATEGORY,
                        AddressBookError.ABS_GROUPS_MEMBER_NOT_FOUND.name(), "");
            }
        } catch (AddressBookException e) {
            logger.error("Error while creating group members.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_GROUPS_MEMBERS_UNABLE_TO_GET, e);
        } catch (final Exception e) {
            logger.error("Error while get group members.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetGroupMembers action end."));
        }
    }
}
