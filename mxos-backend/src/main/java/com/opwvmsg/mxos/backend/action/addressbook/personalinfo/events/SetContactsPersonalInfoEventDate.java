/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.addressbook.personalinfo.events;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set date attribute of Contact PersonalInfo Event object.
 * 
 * @author mxos-dev
 * 
 */

public class SetContactsPersonalInfoEventDate implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetContactsPersonalInfoEventDate.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetContactsPersonalInfoEventDate action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();
            String date = null;
            String backend = MxOSConfig.getAddressBookBackend();

            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
            simpleDateFormat.applyLocalizedPattern(System
                    .getProperty(SystemProperty.userDateFormat.name()));
            simpleDateFormat.setTimeZone(TimeZone
                    .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));
            simpleDateFormat.setLenient(false);

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.birthDate.name()) != null) {

                date = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.birthDate.name()).get(0);

                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.we.name())) {
                    // WE Mapping

                    Date birthDate = simpleDateFormat.parse(date);
                    GregorianCalendar birthDayCal = new GregorianCalendar();
                    birthDayCal.setTimeZone(TimeZone
                            .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));
                    birthDayCal.setTime(birthDate);
                    birthDayCal.setLenient(false);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthDay,
                                    birthDayCal.get(Calendar.DAY_OF_MONTH));

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthMonth,
                                    birthDayCal.get(Calendar.MONTH) + 1);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthYear,
                                    birthDayCal.get(Calendar.YEAR));

                }
                if (MxOSConfig.getAddressBookBackend().equals(
                        AddressBookDBTypes.ox.name())) {
                    // OX Mapping
                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.birthDate, date);
                }
            } else if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.anniversaryDate.name()) != null) {

                date = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.anniversaryDate.name()).get(0);

                if (backend
                        .equalsIgnoreCase(MxOSConfig.DEFAULT_ADDRESS_BOOK_BACKEND)) {
                    // WE Mapping

                    Date anniversaryDate = simpleDateFormat.parse(date);
                    GregorianCalendar anniversaryCal = new GregorianCalendar();
                    anniversaryCal.setTimeZone(TimeZone
                            .getTimeZone(MxOSConstants.WE_DEFAULT_TIMEZONE));
                    anniversaryCal.setTime(anniversaryDate);
                    anniversaryCal.setLenient(false);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryDay,
                                    anniversaryCal.get(Calendar.DAY_OF_MONTH));

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryMonth,
                                    anniversaryCal.get(Calendar.MONTH) + 1);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryYear,
                                    anniversaryCal.get(Calendar.YEAR));

                } else {
                    // OX Mapping
                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.anniversaryDate, date);

                }

            }

            addressBookCRUD.updateContactsPersonalInfoEvent(mxosRequestState);
        } catch (AddressBookException e) {
            logger.error("Error while updating personalInfo event.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_PERSONALINFO_EVENTS_UNABLE_TO_UPDATE,
                    e);
        } catch (ParseException pe) {
            logger.error("Wrong date format given.", pe);
            throw new ApplicationException(
                    AddressBookError.ABS_PERSONALINFO_INVALID_EVENTS_DATE
                            .name(),
                    pe);

        } catch (final Exception e) {
            logger.error("Error while updating personalInfo event.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetContactsPersonalInfoEventDate action end."));
        }
    }
}
