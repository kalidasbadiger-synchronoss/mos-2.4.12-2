/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.InitialLdapContext;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * LDAP connection pool factory to access LDAP Data such as Mailbox Profile,
 * Domain and COS. This is required by Apache object pool.
 * 
 * @author mxos-dev
 */
class LdapCRUDFactory extends LdapClientFactory<LdapMailboxCRUD> {
    
    LdapCRUDFactory() throws MxOSException {
        super();
    }

    private static Logger logger = Logger.getLogger(LdapCRUDFactory.class);

    @Override
    public LdapMailboxCRUD makeObject() throws Exception {
        
        logger.info("Connecting to host: "
                + getEnv().get(DirContext.PROVIDER_URL));
        return new LdapMailboxCRUD(MxOSConstants.LDAPMAILBOXCRUD, this);
    }

    @Override
    public void destroyObject(final LdapMailboxCRUD ldapCRUD) throws Exception {
        ldapCRUD.close();
    }

    @Override
    public boolean validateObject(final LdapMailboxCRUD ldapCRUD) {
        boolean status;
        try {
            status = ldapCRUD.isConnected();
            if (logger.isDebugEnabled()) {
                logger.info("LDAP CRUD status: " + status);
            }
        } catch (MxOSException e1) {
            return false;
        }
        return status;
    }

    @Override
    String[] getLdapServers() throws MxOSException {
        return MxOSConfig.getLdapServers();
    }
    
    /**
     * This method is called back from LdapMailboxCRUD. LdapMailboxCRUD is not
     * actually an LDAP client object, but rather a driver to execute
     * MailboxCRUD business logic using LDAP. A MailboxCRUD object owns and
     * updates a LdapContext object whenever necessary, so utilizes this method
     * to obtain a new LdapContext object.
     * 
     * @param localEnv
     * @return 
     * @throws NamingException
     */
    OWBaseLdapContext createLdapContext(Hashtable<String, String> localEnv)
            throws NamingException {
        return new OWBaseLdapContext(localEnv, null);
    }
    
    /**
     * This method is called whenever pool.borrowObject() is executed in order
     * to update the factory state.
     * 
     * @throws MxOSException
     */
    void notifyBorrow() throws MxOSException {
        if (!MxOSConfig.isLdapServerAMaster(env.get(MxOSConfig.PROVIDER_HOST))) {
            loadLdapHosts();
            getActiveLdapHost();
        }
    }
}
