/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Abstract mailbox for CRUD
 * 
 * @author mxos-dev
 * 
 */
public abstract class AbstractMailbox {
    // user can specified these parameters to skip action
    // and these parameters will not documented
    protected static final String SKIP_LDAP = "skipLDAP";
    protected static final String SKIP_MSS = "skipMSS";
    protected static final String SKIP_OX = "skipOX";

    // user can specified these parameter to raise an exception when mailbox
    // already exists in LDAP
    protected static final String ERROR_ON_ALREADY_EXISTS = "errorOnMailboxAlreadyExists";
    protected static final String LDAP_MAILBOX_EXISTS_RETURN_ERROR = "LdapMailboxExistsReturnError";
    // virtual parameter to indicate the case that mailbox already exist and ignore thus error when create it in LDAP
    protected static final String IGNORE_ERROR_ON_ALREADY_EXISTS = "ignoreErrorOnMailboxAlreadyExists";

    /**
     * check mailbox creation precondition
     * 
     * @param requestState
     * @throws MxOSException
     */
    protected void checkMailboxCreation(MxOSRequestState requestState)
            throws MxOSException {
        if (requestState.getInputParams().containsKey(SKIP_LDAP)) {
            boolean skipLDAP = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_LDAP).get(0));
            if (skipLDAP) {
                // the mailbox must be created in LDAP first for mailbox
                // creation in MSS or OX
                // if user specified that skip mailbox creation in LDAP
                // we need to check whether mailbox exists in LDAP
                if (!checkMailboxExistInLDAP(requestState)) {
                    throw new MxOSException(
                            MailboxError.MBX_UNABLE_TO_GET.name(),
                            "Mailbox must be created in LDAP first.");
                }
            }
        }

    }

    /**
     * Check whether mailbox exists in LDAP by email
     * 
     * @param requestState
     * @return true or false
     */
    protected boolean checkMailboxExistInLDAP(MxOSRequestState requestState)
            throws MxOSException {
        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            Mailbox mailbox = mailboxCRUD.readMailbox(requestState, email);
            if (mailbox != null) {
                IMailboxHelper helper = MxOSApp.getInstance()
                        .getMailboxHelper();
                helper.setAttribute(requestState, MailboxProperty.mailboxId,
                        mailbox.getBase().getMailboxId());
                requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailboxId,
                        mailbox.getBase().getMailboxId());
                return true;
            }
            return false;
        } catch (MxOSException e) {
            throw e;
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }
    }

    /**
     * check whether delete only on MSS
     * 
     * @param requestState
     * @return
     */
    protected boolean deleteOnlyOnMSS(MxOSRequestState requestState) {
        boolean deleteOnlyOnMss = false;
        if (requestState.getInputParams().containsKey(
                MailboxProperty.deleteOnlyOnMss.name())) {
            deleteOnlyOnMss = Boolean.getBoolean(requestState.getInputParams()
                    .get(MailboxProperty.deleteOnlyOnMss.name()).get(0));
        }
        return deleteOnlyOnMss;
    }

}
