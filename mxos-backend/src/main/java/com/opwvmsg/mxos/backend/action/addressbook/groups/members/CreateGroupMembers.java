/*
/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook.groups.members;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create group members.
 * 
 * @author mxos-dev
 */
public class CreateGroupMembers implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(CreateGroupMembers.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateGroupMembers action start."));
        }

        ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
        IAddressBookCRUD addressBookCRUD = null;

        try {
            addressBookCRUDPool = MxOSApp.getInstance().getAddressBookCRUD();
            addressBookCRUD = addressBookCRUDPool.borrowObject();
            String name = mxosRequestState.getInputParams()
                    .get(AddressBookProperty.groupId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.groupId, name);

            if (mxosRequestState.getInputParams().containsKey(
                    AddressBookProperty.memberList.name())) {
                // Create Multiple Group Members
                List<String> members = addressBookCRUD
                        .createMultipleGroupMembers(mxosRequestState);
                mxosRequestState.getDbPojoMap().setProperty(
                        MxOSPOJOs.memberIdList, members);
            } else {
                // Create Single Group Member
                String member = addressBookCRUD
                        .createGroupMembers(mxosRequestState);
                mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.memberId,
                        Long.parseLong(member));
            } 
        } catch (AddressBookException e) {
            logger.error("Error while creating group members.", e);
            ExceptionUtils.createMxOSExceptionFromAddressBookException(
                    AddressBookError.ABS_GROUPS_MEMBERS_UNABLE_TO_PUT, e);
        } catch (final Exception e) {
            logger.error("Error while creating group members.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (addressBookCRUDPool != null && addressBookCRUD != null) {
                    addressBookCRUDPool.returnObject(addressBookCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("CreateGroupMembers action end."));
        }
    }
}
