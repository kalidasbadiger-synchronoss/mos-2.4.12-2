package com.opwvmsg.mxos.backend.action.message;

import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.message.pojos.Metadata;

/**
 * Action class to get sorted list of message meta data in a folder.
 * 
 * @author mxos-dev
 */
public class SearchSortedMessagesMetaData implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SearchSortedMessagesMetaData.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Action Started.");
        }

        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {
            Map<String, Map<String, Metadata>> messagesMetaData
                = new LinkedHashMap<String, Map<String, Metadata>>();

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();

            MessageServiceHelper.searchSortedMessageMetaDatasInFolder(metaCRUD,
                    requestState, messagesMetaData);

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageMetaData,
                    messagesMetaData);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while search messages meta data.", e);
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_SEARCH_MESSAGE_META_DATA.name(),
                    e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Action Ended.");
        }
    }

}
