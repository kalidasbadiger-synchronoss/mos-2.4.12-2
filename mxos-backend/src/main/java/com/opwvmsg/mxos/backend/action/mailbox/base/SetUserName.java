/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set userName.
 *
 * @author mxos-dev
 */
public class SetUserName implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetUserName.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetUserName action start.");
        }
        final String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        String userName = requestState.getInputParams()
				.get(MailboxProperty.userName.name()).get(0);
        try {
			String storeUserNameAsEmail = System
					.getProperty(SystemProperty.storeUserNameAsEmail.name());
			// if storeUserNameAsEmail is true, userName must have domain.
			if (Boolean.valueOf(storeUserNameAsEmail)) {
				userName += (MxOSConstants.AT_THE_RATE + ActionUtils
						.getArrayFromEmail(email)[1]);
			}
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.userName, userName);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set user name.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_USERNAME.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetUserName action end.");
        }
    }
}
