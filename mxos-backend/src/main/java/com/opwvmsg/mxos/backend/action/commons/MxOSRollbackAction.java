/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.commons;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * If the action supports rollback,it should implement this interface
 * 
 * @author mxos-dev
 * 
 */
public interface MxOSRollbackAction extends MxOSBaseAction {

    /**
     * Method to process the rollback when fail
     * 
     * @param requestState
     * @throws MxOSException
     */
    void rollback(MxOSRequestState requestState) throws MxOSException;

}
