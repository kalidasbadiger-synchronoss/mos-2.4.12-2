package com.opwvmsg.mxos.backend.action.addressbook.base;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

public class SearchContactBase implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(SearchContactBase.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SearchContactsBase action start."));
        }
        ICRUDPool<ISettingsCRUD> oxMySQLSettingsCRUDPool = null;
        ISettingsCRUD oxMySQLSettingsCRUD = null;
        List<ContactBase> contactBases = null;
        try {
            oxMySQLSettingsCRUDPool = MxOSApp.getInstance()
                    .getOXSettingsMySQLCRUD();
            oxMySQLSettingsCRUD = oxMySQLSettingsCRUDPool.borrowObject();
            contactBases = oxMySQLSettingsCRUD.searchContactBase(requestState);
            if (logger.isDebugEnabled()) {
                logger.debug("Search ContactBase for OX MySQL successfully.");
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.allContactBase,
                    contactBases);
        } catch (final MxOSException e) {
            logger.error("Error while searching ContactBase for OX MySQL.", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while searching ContactBase for OX MySQL.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            CRUDUtils.releaseConnection(oxMySQLSettingsCRUDPool,
                    oxMySQLSettingsCRUD);
        }
    }

}
