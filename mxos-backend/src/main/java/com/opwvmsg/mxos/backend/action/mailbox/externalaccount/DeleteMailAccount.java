/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.mailbox.externalaccount;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to delete the mailAccounts.
 * 
 * @author mxos-dev
 */
public class DeleteMailAccount implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteMailAccount.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMailAccount action start."));
        }
        try {
            String accountId = requestState.getInputParams()
                    .get(MailboxProperty.accountId.name()).get(0);

            if (accountId == null)
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ACCOUNT_ID.name());

            ExternalAccounts ea = (ExternalAccounts) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.externalAccounts);
            List<MailAccount> mailAccountList = ea.getMailAccounts();
            List<MailAccount> newMailAccountList = new ArrayList<MailAccount>();

            boolean isAccountPresent = false;
            if (mailAccountList != null) {
                for (MailAccount ma : mailAccountList) {
                    if (ma.getAccountId() == Integer.parseInt(accountId)) {
                        isAccountPresent = true;
                    } else
                        newMailAccountList.add(ma);
                }
            }

            if (isAccountPresent == false) {
                throw new InvalidRequestException(
                        MailboxError.MBX_MAIL_ACCOUNT_NOT_FOUND.name());
            }

            ea.setMailAccounts(newMailAccountList);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.externalAccounts,
                    ea);

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete mail account.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_DELETE_MAIL_ACCOUNT.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMailAccount action end."));
        }
    }
}
