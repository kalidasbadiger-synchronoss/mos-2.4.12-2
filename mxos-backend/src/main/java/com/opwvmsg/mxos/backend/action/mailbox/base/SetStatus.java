/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.Arrays;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.commons.NotifyUtils;
import com.opwvmsg.mxos.backend.action.commons.NotifyUtils.NotifyMsgType;
import com.opwvmsg.mxos.backend.action.notify.GetNotify;
import com.opwvmsg.mxos.backend.action.notify.PublishNotify;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.NotificationProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.DeploymentMode;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set status.
 * 
 * @author mxos-dev
 */
public class SetStatus implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetStatus.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetStatus action start.");
        }
        final String status = requestState.getInputParams()
                .get(MailboxProperty.status.name()).get(0);
        DeploymentMode mode = MxOSConfig.getDeploymentMode();
        logger.info("Deployment mode is set to: " + mode);

        // Update Status is not supported for BGC. Use logging API instead.
        if (DeploymentMode.bgc.equals(mode)) {
            throw new ApplicationException(ErrorCode.GEN_INVALID_DATA.name());
        }
        try {
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.status, status);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set status.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_ACCOUNT_STATUS.name(), e);
        }

        // if status is changed, need to notify IMAP server
        if (!ActionUtils.isCreateMailboxOperation(requestState
                .getOperationName())) {
            notify(requestState);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("SetStatus action end.");
        }
    }

    private void notify(MxOSRequestState requestState) {
        Base base = (Base) requestState.getDbPojoMap().getPropertyAsObject(
                MxOSPOJOs.base);
        if (base != null) {
            String status = requestState.getInputParams()
                    .get(MailboxProperty.status.name()).get(0);
            String mailboxId = base.getMailboxId();
            String topic = NotifyUtils.getMailboxStatusNotifyTopic(mailboxId);
            String notifyMessage = NotifyUtils.getMailboxStatusNotifyMessage(
                    NotifyMsgType.StatusChanged, mailboxId, status);

            requestState.getInputParams().put(
                    NotificationProperty.topic.name(), Arrays.asList(topic));
            requestState.getInputParams().put(
                    NotificationProperty.notifyMessage.name(),
                    Arrays.asList(notifyMessage));

            try {
                ActionFactory.getInstance().exec(GetNotify.class.getName(),
                        requestState);
                ActionFactory.getInstance().exec(PublishNotify.class.getName(),
                        requestState);
            } catch (Exception e) {
                // we need to ignore notify exception for normal operation
                logger.error(
                        "Failed to notify IMAP server when status is changed to "
                                + status, e);
            }
        }
    }
}
