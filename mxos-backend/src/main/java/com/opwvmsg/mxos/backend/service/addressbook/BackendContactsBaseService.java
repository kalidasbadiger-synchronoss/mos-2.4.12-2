/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsBaseService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Base service exposed to client which is responsible for doing basic
 * base related activities e.g read base, update base, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendContactsBaseService implements IContactsBaseService {

    private static Logger logger = Logger
            .getLogger(BackendContactsBaseService.class);

    /**
     * Default Constructor.
     */
    public BackendContactsBaseService() {
        logger.info("BackendContactsBaseService Service created...");
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ContactBase> list(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsBaseService, Operation.GETALL);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.GETALL, t0,
                    StatStatus.pass);

            return ((List<ContactBase>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allContactBase));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.GETALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public ContactBase read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsBaseService, Operation.GET);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.GET, t0,
                    StatStatus.pass);

            return ((ContactBase) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.contactBase));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsBaseService,
                    Operation.POST);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.POST,
                    t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<ContactBase> search(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsBaseService,
                    Operation.SEARCH);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.SEARCH,
                    t0, StatStatus.pass);
            return ((List<ContactBase>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allContactBase));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsBaseService, Operation.SEARCH,
                    t0, StatStatus.fail);
            throw e;
        }
    }
}
