/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set cloudmark filters spam policy.
 *
 * @author mxos-dev
 */
public class SetCloudmarkFiltersSpamPolicy implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetCloudmarkFiltersSpamPolicy.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersSpamPolicy action start."));
        }
        String spamPolicy = requestState.getInputParams()
                .get(MailboxProperty.spamPolicy.name()).get(0);
        try {
            if (spamPolicy != null && !spamPolicy.equals("")) {
                spamPolicy = MxosEnums.SpamPolicy.fromValue(spamPolicy)
                        .toString();
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.spamPolicy,
                            spamPolicy);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set cloudmark filters spam policy.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SPAM_POLICY
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersSpamPolicy action end."));
        }
    }
}
