/*
 * Copyright (c) 2015 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.Control;
import javax.naming.ldap.LdapContext;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;
/**
 * This class behaves the same with InitialLdapContext except search method. The
 * search method is overridden for read through feature.
 * 
 * The search method first runs normal search using the super class. If that
 * search returns any result, the method is done there. If no result is
 * returned, the method tries the same search against master. In order to do it,
 * this context has LDAP client pool for master. The owner of the pool is
 * LdapSearchCRUDFactory and instances of this class shares the pool.
 */
public class SearchLdapContext extends OWBaseLdapContext {
    
    private static Logger logger = Logger.getLogger(SearchLdapContext.class);
    
    final GenericObjectPool<LdapContext> masterLdapClientPool;
    /**
     * The ctor initializes the super class and sets the master LDAP client pool. 
     * 
     * @param environment
     * @param connCtls 
     * @param masterLdapClientPool
     * @throws NamingException
     */
    public SearchLdapContext(Hashtable<String, String> environment,
            Control[] connCtls,
            GenericObjectPool<LdapContext> masterLdapClientPool)
            throws NamingException {
        
        super(environment, connCtls);
        
        this.masterLdapClientPool = masterLdapClientPool;
        if (this.masterLdapClientPool == null) {
            // should never happen
            throw new NamingException(
                    "SearchLdapContext requires master LDAP client pool");
        }
    }
    
    @Override
    public NamingEnumeration<SearchResult> search(String name, String filter,
            SearchControls cons, boolean authoritative) throws NamingException {
        return authoritative ? searchMasterLdap(name, filter, cons) 
                : search(name, filter, cons);
    }
    
    public NamingEnumeration<SearchResult> searchMasterLdap(String name, String filter,
            SearchControls cons) throws NamingException {
        
        logger.info("Try the LDAP master.");
        NamingEnumeration<SearchResult> result = null;
        LdapContext master;
        try {
            master = masterLdapClientPool.borrowObject();
        } catch (Exception ex) {
            throw new NamingException(
                    "Unable to acquire LDAP master client object from the pool"
                            + ex);
        }
        
        try {
            result = master.search(name, filter, cons);
            try {
                masterLdapClientPool.returnObject(master);
            } catch (Exception ex) {
                // Log and continue.
                logger.error(
                        "Error in returning LDAP master client object to the pool.",
                        ex);
            }
            master = null;
        } finally {
            try {
                if (master != null) {
                    masterLdapClientPool.invalidateObject(master);
                }
            } catch (Exception ex) {
                // Log and continue.
                logger.error(
                        "Error in invalidating LDAP master client object to the pool.",
                        ex);
            }
        }
        return result;
    }
    
    @Override
    public Attributes getAttributes(String name, String[] attrIds, boolean authoritative)
            throws NamingException {
        return authoritative ? getAttributesFromMaster(name, attrIds)
                : getAttributes(name, attrIds);
    }
    
    public Attributes getAttributesFromMaster(String name, String[] attrIds)
            throws NamingException {
        logger.info("Try the LDAP master.");
        Attributes result = null;
        LdapContext master;
        try {
            master = masterLdapClientPool.borrowObject();
        } catch (Exception ex) {
            throw new NamingException(
                    "Unable to acquire LDAP master client object from the pool"
                            + ex);
        }
        
        try {
            result = master.getAttributes(name, attrIds);
            try {
                masterLdapClientPool.returnObject(master);
            } catch (Exception ex) {
                // Log and continue.
                logger.error(
                        "Error in returning LDAP master client object to the pool.",
                        ex);
            }
            master = null;
        } finally {
            try {
                if (master != null) {
                    masterLdapClientPool.invalidateObject(master);
                }
            } catch (Exception ex) {
                // Log and continue.
                logger.error(
                        "Error in invalidating LDAP master client object to the pool.",
                        ex);
            }
        }
        return result;
    }
}