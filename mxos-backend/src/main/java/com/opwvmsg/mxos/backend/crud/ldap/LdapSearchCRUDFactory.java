/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;
import java.util.concurrent.atomic.AtomicInteger;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.ldap.LdapContext;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * LDAP Search connection pool factory to access LDAP Data such as Mailbox
 * Profile, Domain and COS. This is required by Apache object pool.
 *
 * @author mxos-dev
 */
class LdapSearchCRUDFactory extends LdapCRUDFactory  {
    protected static Logger logger =
        Logger.getLogger(LdapSearchCRUDFactory.class);
    
    protected GenericObjectPool<LdapContext> masterLdapClientPool;
    protected final AtomicInteger counter = new AtomicInteger(0);
    protected final int ldapMinOperations;
    protected final boolean switchLdapFeature;
    protected final AtomicInteger failbackCounter = new AtomicInteger(0);
    protected final int failbackMinOperations;
    protected final boolean failback;
    private Object lock = new Object();
    private boolean lazyInitialized = false;
    
    public LdapSearchCRUDFactory() throws MxOSException {
        super();
        
        ldapMinOperations = Integer.parseInt(System.getProperty(
                SystemProperty.switchLdapHostAfterMinOperations.name(), "0"));
        switchLdapFeature = (ldapMinOperations > 0);
        failbackMinOperations = Integer.parseInt(System.getProperty(
                SystemProperty.ldapFailbackMinOperations.name(), "0"));
        failback = (failbackMinOperations > 0);
    }

    @Override
    public LdapMailboxCRUD makeObject() throws Exception {
        logger.info("Connecting to host: "
                + getEnv().get(DirContext.PROVIDER_URL));
        return new LdapMailboxCRUD(MxOSConstants.LDAPSEARCHMAILBOXCRUD, this);
    }

    @Override
    public void destroyObject(LdapMailboxCRUD mailboxCRUD)
            throws Exception {
        mailboxCRUD.close();
    }

    @Override
    public boolean validateObject(final LdapMailboxCRUD ldapCRUD) {
        boolean status;
        try {
            status = ldapCRUD.isConnected();
            if (logger.isDebugEnabled()) {
                logger.info("LDAP CRUD status: " + status);
            }
        } catch (MxOSException e1) {
            return false;
        }
        return status;
    }
    
    @Override
    void notifyBorrow() {
        if (switchLdapFeature && counter.incrementAndGet() >=
                (ldapMinOperations + RandomUtils.nextInt(ldapMinOperations))) {
            logger.info("LDAP pool Switching to next available LDAP host");
            getActiveLdapHost();
            counter.set(0);
        }
        if (!switchLdapFeature && failback && !env.get(DirContext.PROVIDER_URL).equals(urls[0]) &&
                failbackCounter.incrementAndGet() >= failbackMinOperations) {
            logger.info("Failback to original LDAP host");
            if (urls.length > 0) env.put(DirContext.PROVIDER_URL, urls[0]);
            if (hosts.length > 0) env.put(MxOSConfig.PROVIDER_HOST,
                    hosts[0].split(MxOSConstants.COLON)[0]);
            activeHostIndex = 1;
            failbackCounter.set(0);
        }
    }
    
    @Override
    String[] getLdapServers() {
        String[] hosts = null;
        hosts = MxOSConfig.getLdapServers();
        if (hosts == null || hosts.length == 0) {
            // TODO: I doubt if this is a good idea. Shouldn't we throw an exception?
            logger.warn("Default LDAP Server hosts not found, loading Master"
                    + " LDAP for SearchConnectionPool...");
            hosts = MxOSConfig.getLdapMasterServers();
        }
        return hosts;
    }
    
    /**
     * Returns ReadThroughLdapContext if read through is enabled.
     * InitialLdapContext otherwise. 
     * @throws NamingException 
     */
    @Override
    OWBaseLdapContext createLdapContext(Hashtable<String, String> localEnv)
            throws NamingException {
        try {
            lazyInitializationMasterLdapClientPool();
        } catch (MxOSException ex) {
            // ignore exception here if masterLdapClientPool initialization fails;
            // the exception will throw out in the readThroughldapcontext class if
            // the masterLdapServer not set;
        }
        return new SearchLdapContext(localEnv, null, masterLdapClientPool);
    }
    
    /**
     * lazy initialization of the masterLdapClientPool
     * @throws MxOSException 
     */
    private void lazyInitializationMasterLdapClientPool() throws MxOSException {
        if(!lazyInitialized) {
           synchronized(lock){
               try {
                   if(!lazyInitialized){
                       MasterLdapClientFactory factory = new MasterLdapClientFactory();
                       int maxSize = Integer.parseInt(System.getProperty(
                               SystemProperty.ldapPoolMaxSize.name(), "0"));
                       masterLdapClientPool = new GenericObjectPool<LdapContext>(factory, maxSize);
                       masterLdapClientPool.setMaxIdle(-1);
                       lazyInitialized = true;
                   }
               } catch (MxOSException ex) {
                   if (ex.getCode().equals(ErrorCode.LDAP_INVALID_CONFIGURATION.name()))
                       lazyInitialized = true;
                   throw ex;
               }
           }
        }
    }
}
