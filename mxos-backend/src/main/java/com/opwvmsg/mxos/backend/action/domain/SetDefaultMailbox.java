/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.domain;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.DomainProperty;
import com.opwvmsg.mxos.error.DomainError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set default mailbox.
 * 
 * @author mxos-dev
 * 
 */
public class SetDefaultMailbox implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetDefaultMailbox.class);

    /**
     * Action method to set Domain type.
     *
     * @param model model instance
     * @throws Exception in case of any error
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetDefaultMailbox action start."));
        }
        try {
            final String dm = requestState.getInputParams()
                    .get(DomainProperty.defaultMailbox.name()).get(0);
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, DomainProperty.defaultMailbox,
                            dm);
        } catch (MxOSException e) {
            throw new InvalidRequestException(
                    DomainError.DMN_INVALID_DEFAULT_MAILBOX.name(), e);
        } catch (final Exception e) {
            logger.error("Error while set default mailbox.", e);
            throw new ApplicationException(
                    DomainError.DMN_INVALID_DEFAULT_MAILBOX.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetDefaultMailbox action end."));
        }
    }
}
