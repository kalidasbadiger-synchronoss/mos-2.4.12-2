/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update mail forwarding address.
 *
 * @author mxos-dev
 */
public class UpdateMailForwardAddress implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateMailForwardAddress.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateMailbox action start."));
        }
        try {
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final String oldFwdAddress = requestState.getInputParams()
                    .get(MailboxProperty.oldForwardingAddress.name()).get(0);
            final String newFwdAddress = requestState.getInputParams()
                    .get(MailboxProperty.newForwardingAddress.name()).get(0);

            if (email.toLowerCase().equalsIgnoreCase(
                    newFwdAddress.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_FORWARD_ADDRESSES_AND_EMAIL_SAME.name());
            }

            final List<String> fwdAddressList = (List<String>) requestState
                    .getDbPojoMap().getPropertyAsObject(
                            MxOSPOJOs.forwardingAddresses);

            if (fwdAddressList == null ||
                    !fwdAddressList.contains(oldFwdAddress.toLowerCase())) {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_FORWARDING_ADDRESS_NOT_EXIST.name());
            }
            if (fwdAddressList.remove(oldFwdAddress.toLowerCase())) {
                fwdAddressList.add(newFwdAddress.toLowerCase());
                final String[] mailFwdAddressesArray = fwdAddressList
                        .toArray(new String[fwdAddressList.size()]);
    
                MxOSApp.getInstance().getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.forwardingAddress,
                                mailFwdAddressesArray);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_OLD_FORWARDING_ADDRESS.name());
            }

        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while update mail forward address.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_FORWARD_ADDRESS_POST.name(), e);
        }
    }
}
