/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.IBackendState;

public class WEAddressBookBackendState implements IBackendState {
    public Map<String, String> attributes;
    public List<Map<String, String>> attributesList;

    public WEAddressBookBackendState() {
        attributes = new HashMap<String, String>();
        attributesList = new ArrayList<Map<String, String>>();
    }
    
    public synchronized void initialize(int size) {
        for (int i=0; i<size; i++) {
            attributesList.add(new HashMap<String, String>());
        }
    }

    @Override
    public int getSize() {
        if (attributes != null) {
            return attributes.size();
        } else {
            return 0;
        }
    }
}
