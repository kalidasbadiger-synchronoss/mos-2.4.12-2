/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.smsservices;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set sms advanced notifications enabled.
 *
 * @author mxos-dev
 */
public class SetSmsAdvancedNotificationsEnabled implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetSmsAdvancedNotificationsEnabled.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetSmsAdvancedNotificationsEnabled action start."));
        }
        String smsAdvancedNotificationsEnabled = requestState
                .getInputParams()
                .get(MailboxProperty.smsAdvancedNotificationsEnabled.name())
                .get(0);

        try {
            if (smsAdvancedNotificationsEnabled != null
                    && !smsAdvancedNotificationsEnabled.equals("")) {
                smsAdvancedNotificationsEnabled = Integer
                        .toString(MxosEnums.BooleanType.fromValue(
                                smsAdvancedNotificationsEnabled).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.smsAdvancedNotificationsEnabled,
                            smsAdvancedNotificationsEnabled);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set SMS advanced notifications enabled.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_SMS_ADVANCED_NOTIFICATIONS_ENABLED
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetSmsAdvancedNotificationsEnabled action end."));
        }
    }
}
