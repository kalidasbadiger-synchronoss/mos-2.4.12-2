/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.webmailfeatures;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ldap.BooleanType;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set WebMailFeatures::BusinessFeaturesEnabled.
 *
 * @author mxos-dev
 */
public class SetBusinessFeaturesEnabled implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetBusinessFeaturesEnabled.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetBusinessFeaturesEnabled action start."));
        }
        String buzFeaturesEnabled = requestState.getInputParams()
                .get(MailboxProperty.businessFeaturesEnabled.name()).get(0);
        if (null != buzFeaturesEnabled && buzFeaturesEnabled.length() > 0) {
            buzFeaturesEnabled = BooleanType.getByValue(buzFeaturesEnabled)
                    .getBusinessFeature();
        }
        try {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.businessFeaturesEnabled,
                            buzFeaturesEnabled);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set business features enabled.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetBusinessFeaturesEnabled action end."));
        }
    }
}
