/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.FolderProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * Action class to UpdateFolderSubscribed.
 *
 * @author mxos-dev
 */
public class UpdateFolderUidValidity implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(UpdateFolderUidValidity.class);

    /**
     * Action method to UpdateFolderSubscribed.
     * 
     * @param model instance of model
     * @throws Exception throws in case of any errors
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "UpdateFolderUidValidity action started."));
        }
        CRUDProtocol protocol = CRUDUtils.getCRUDProtocol(requestState);
        RmeDataModel rmeDataModel = (protocol == CRUDProtocol.httprme) ? RmeDataModel.Leopard : RmeDataModel.CL;        
        if (rmeDataModel == RmeDataModel.Leopard) {
            final String uidValidity = requestState.getInputParams()
                    .get(FolderProperty.uidValidity.name()).get(0);
            if (uidValidity != null) {
                requestState.getDbPojoMap().setProperty(
                        FolderProperty.uidValidity, uidValidity);

            } else {
                throw new InvalidRequestException(
                        FolderError.FLD_INVALID_UID_VALIDITY.name());
            }
        } else {
            // uidValidity is NOT required for stateful RME
            logger.info("Invalid MSS Rme Version found, considering "
                    + "as Stateless MSS.");
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("UpdateFolderUidValidity action end."));
        }
    }
}