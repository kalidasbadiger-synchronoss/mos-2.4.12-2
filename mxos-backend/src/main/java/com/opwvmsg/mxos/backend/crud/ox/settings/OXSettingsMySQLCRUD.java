/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.settings;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.common.base.Strings;
import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;
import com.mysql.jdbc.exceptions.jdbc4.MySQLNonTransientConnectionException;
import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.crud.ox.addressbook.OXAddressBookUtil;
import com.opwvmsg.mxos.backend.crud.we.utils.Query;
import com.opwvmsg.mxos.backend.crud.we.utils.Query.Condition;
import com.opwvmsg.mxos.backend.crud.we.utils.Term;
import com.opwvmsg.mxos.backend.crud.we.utils.Visitor;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.OXSettingsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Class to implement methods OX specific methods for communicating and parsing
 * the response
 * 
 * @author mxos-dev
 * 
 */
public class OXSettingsMySQLCRUD implements ISettingsCRUD {
    private static final String CONFIG_DB_STRING = "configdb";
    private final static String INSERT_ALIAS_SQL;
    private final static String DELETE_ALL_ALIAS_SQL;
    private final static String UPDATE_EXTERNAL_ACCOUNT_ID_SQL;
    private final static String INSERT_EXTERNAL_INCOMING_ACCOUNT_SQL;
    private final static String INSERT_EXTERNAL_OUTGOING_ACCOUNT_SQL;
    private final static String DELETE_EXTERNAL_INCOMING_ACCOUNTS_SQL;
    private final static String DELETE_EXTERNAL_OUTGOING_ACCOUNTS_SQL;
    private final static String DELETE_EXTERNAL_ACCOUNTS_PROPERTIES_SQL;
    private final static String LINE_WRAP_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL;
    private final static String AUTO_COLLECT_CONTACTS_WHILE_READING_SQL;
    private final static String REPLY_TO_SQL;
    private final static String FROM_ADDRESS_SQL;
    private final static String EXCLUDE_ORIGINAL_FROM_REPLY_SQL;
    private final static String INCLUDE_ORIGINAL_IN_REPLY_SQL;
    private final static String DISABLE_IMAGE_IN_HTML_SQL;
    private final static String ENABLE_IMAGE_IN_HTML_SQL;
    private final static String DISABLE_DELETE_MAIL_PERMANENT_SQL;
    private final static String ENABLE_DELETE_MAIL_PERMANENT_SQL;
    private final static String TIMEZONE_SQL;
    private final static String LANGUAGE_SQL;
    private final static String DB_WRITE_POOL_SQL;
    
    private final static String SELECT_LANGUAGE_SQL;
    private final static String SELECT_SEND_ADDRESS_SQL;
    private final static String SELECT_USER_CONTACT_BASE_SQL;
    
    static {

        INSERT_ALIAS_SQL = new StringBuilder()
                .append("INSERT INTO user_attribute ")
                .append("(cid, id, name, value,uuid) ")
                .append("SELECT cid, id, 'alias', ?, ? ").append("FROM user ")
                .append("WHERE imapLogin=?").toString();
        DELETE_ALL_ALIAS_SQL = new StringBuilder().append("DELETE  a.* ")
                .append("FROM user_attribute as a ")
                .append("LEFT JOIN user as b ")
                .append("ON a.cid = b.cid and a.id = b.id and a.name='alias' ")
                .append("WHERE (b.imapLogin=?)").toString();
        UPDATE_EXTERNAL_ACCOUNT_ID_SQL = new StringBuilder()
                .append("UPDATE sequence_mail_service AS a ")
                .append("INNER JOIN user AS b ").append("ON a.cid = b.cid ")
                .append("SET a.id = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        INSERT_EXTERNAL_INCOMING_ACCOUNT_SQL = new StringBuilder()
                .append("INSERT INTO user_mail_account ")
                .append("(cid, id, user, name, url, login, password, primary_addr, default_flag, trash, sent, drafts, spam, confirmed_spam, confirmed_ham, spam_handler, unified_inbox, trash_fullname, sent_fullname, drafts_fullname, spam_fullname, confirmed_spam_fullname, confirmed_ham_fullname, personal, replyTo) ")
                .append("SELECT cid, ?, id, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'confirmed-spam', 'confirmed-ham', 'NoSpamHandler', 0, '', '', '', '', '', '', ?, null ")
                .append("FROM user ").append("WHERE imapLogin=?").toString();
        INSERT_EXTERNAL_OUTGOING_ACCOUNT_SQL = new StringBuilder()
                .append("INSERT INTO user_transport_account ")
                .append("(cid, id, user, name, url, login, password, send_addr, personal, replyTo, default_flag, unified_inbox) ")
                .append("SELECT cid, ?, id, ?, ?, ?, ?, ?, null, null, 0, 0 ")
                .append("FROM user ").append("WHERE imapLogin=?").toString();
        DELETE_EXTERNAL_INCOMING_ACCOUNTS_SQL = new StringBuilder()
                .append("DELETE  a.* ").append("FROM user_mail_account as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        DELETE_EXTERNAL_OUTGOING_ACCOUNTS_SQL = new StringBuilder()
                .append("DELETE  a.* ")
                .append("FROM user_transport_account as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        DELETE_EXTERNAL_ACCOUNTS_PROPERTIES_SQL = new StringBuilder()
                .append("DELETE  a.* ")
                .append("FROM user_mail_account_properties as a, ")
                .append("user as b ")
                .append("WHERE a.cid = b.cid and a.user = b.id ")
                .append("AND b.imapLogin=?").toString();
        LINE_WRAP_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET auto_linebreak = ? ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 0, ")
                .append("contactCollectOnMailAccess = 1 ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 1, ")
                .append("contactCollectOnMailAccess = 0 ")
                .append("WHERE (b.imapLogin=?)").toString();
        AUTO_COLLECT_CONTACTS_WHILE_READING_SQL = new StringBuilder()
                .append("UPDATE user_setting_server AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET contactCollectOnMailTransport = 0, ")
                .append("contactCollectOnMailAccess = 0 ")
                .append("WHERE (b.imapLogin=?)").toString();
        REPLY_TO_SQL = new StringBuilder()
                .append("UPDATE user_mail_account AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET replyTo = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        FROM_ADDRESS_SQL = new StringBuilder()
                .append("UPDATE prg_contacts AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.fid = 6 ")
                .append("SET field01 = ? ").append("WHERE (b.imapLogin=?)")
                .toString();
        EXCLUDE_ORIGINAL_FROM_REPLY_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 10)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        INCLUDE_ORIGINAL_IN_REPLY_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 10)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        DISABLE_IMAGE_IN_HTML_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 14)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        ENABLE_IMAGE_IN_HTML_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 14)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        DISABLE_DELETE_MAIL_PERMANENT_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits | (1 << 3)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        ENABLE_DELETE_MAIL_PERMANENT_SQL = new StringBuilder()
                .append("UPDATE user_setting_mail AS a ")
                .append("INNER JOIN user AS b ")
                .append("ON a.cid = b.cid and a.user = b.id ")
                .append("SET bits = (bits & ~(1 << 3)) ")
                .append("WHERE (b.imapLogin=?)").toString();
        TIMEZONE_SQL = new StringBuilder().append("UPDATE user ")
                .append("SET timeZone = ? ").append("WHERE imapLogin=?")
                .toString();
        LANGUAGE_SQL = new StringBuilder().append("UPDATE user ")
                .append("SET preferredLanguage = ? ")
                .append("WHERE imapLogin=?").toString();
        DB_WRITE_POOL_SQL = new StringBuilder()
                .append("SELECT url, db_schema, login, password ")
                .append("FROM configdb.login2context AS a, ")
                .append("configdb.context_server2db_pool AS b, ")
                .append("configdb.db_pool AS c ")
                .append("WHERE a.login_info=? ").append("and b.cid=a.cid ")
                .append("and c.db_pool_id=b.write_db_pool_id").toString();
        
        SELECT_LANGUAGE_SQL = new StringBuilder()
                .append("SELECT preferredLanguage ")
                .append("FROM user ")
                .append("WHERE mail=? AND imapLogin=?").toString();
        
        SELECT_SEND_ADDRESS_SQL = new StringBuilder()
                .append("SELECT usm.send_addr ")
                .append("FROM user_setting_mail usm LEFT JOIN user u ")
                .append("ON u.cid = usm.cid ")
                .append("WHERE u.mail=? AND u.imapLogin=?").toString();
        
        SELECT_USER_CONTACT_BASE_SQL = new StringBuilder()
                .append("SELECT prg.intfield01,prg.fid ")
                .append("FROM prg_contacts prg, user usr ")
                .append("WHERE prg.cid = usr.cid ")
                .append("AND lower(usr.mail) = lower(?) ").append("AND")
                .toString();
    }
    private static final String NONE_STRING = "none";
    private static final String DEFAULT_IMAP_URL = "imap://localhost:25";
    private static final String MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";

    private static Logger logger = Logger.getLogger(OXSettingsMySQLCRUD.class);

    // MySQL URL for the server connection
    private String mysqlURL;
    private String userName;
    private String password;

    // Server connection used for accessing configdb database
    private Connection connection;

    // Sharded mysql db connection
    private Connection shardConnection;
    private final int oxSettingsMaxRetryCount;
    private final int oxSettingsSleepBetweenRetry;

    /**
     * Constructor
     * 
     * @throws Exception
     */
    public OXSettingsMySQLCRUD(String oxMySQLURL, String userName,
            String password) throws Exception {
        try {
            this.mysqlURL = oxMySQLURL;
            this.userName = userName;
            this.password = password;
            oxSettingsMaxRetryCount = Integer.parseInt(System.getProperty(
                    SystemProperty.oxMySQLRetryCount.name(), "2"));
            oxSettingsSleepBetweenRetry = Integer.parseInt(System.getProperty(
                    SystemProperty.oxMySQLRetryInterval.name(), "5"));
            
            Class.forName(MYSQL_JDBC_DRIVER);

            // connect to the database using replication driver
            connection = DriverManager.getConnection(oxMySQLURL, userName,
                    password);
            if (connection != null) {
                if (logger.isDebugEnabled()) {
                    logger.debug("OXSettingsCRUD connection object created.");
                }
            } else {
                throw new Exception(
                        "Error while connecting to OX MySQL database");
            }
        } catch (final CommunicationsException e) {
            logger.error(
                    "CommunicationsException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error(
                    "MySQLNonTransientConnectionException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Exception while connecting to OX MySQL database", e);
            throw new MxOSException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void close() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

    @Override
    public void commit() throws ApplicationException {
        if (shardConnection == null) {
            // the transaction is already finalized
            return;
        }
        
        try {
            shardConnection.commit();
            if (shardConnection != connection) {
                logger.debug("closing shard connection");
                shardConnection.close();
            }
            shardConnection = null;
            
        } catch (final Exception e) {
            logger.error("Error while commit to OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }
    }

    @Override
    public void createSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    @Override
    public void deleteSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        // TODO Auto-generated method stub

    }

    private java.sql.ResultSet executeQuery(List<String> params, String sql,PreparedStatement st)
            throws SQLException, ApplicationException {
        ResultSet result = null;
        int retryCount = 0;
        if (logger.isDebugEnabled()) {
            logger.debug("sql Query mysqlURL: " + mysqlURL);
            logger.debug("sql Query userName: " + userName);
            logger.debug("sql Query password: " + password);
            logger.debug("sql Query: " + sql);
            for (String param: params) {
                logger.debug("sql Query param: " + param);
            }
        }
        do {
            try {
                if (connection == null) {
                    Thread.sleep(oxSettingsSleepBetweenRetry);
                    connection = DriverManager.getConnection(mysqlURL,
                            userName, password);
                    if (connection != null) {
                        if (logger.isDebugEnabled()) {
                            logger.debug("OXSettingsMySQL connection created.");
                        }
                    } else {
                        retryCount++;
                        continue;
                    }
                }

                st = connection.prepareStatement(sql);
                int paramIndex = 1;
                for (String param : params) {
                    st.setString(paramIndex++, param);
                }
                result = st.executeQuery();
                retryCount = oxSettingsMaxRetryCount + 1;

            } catch (final CommunicationsException e) {
                retryCount++;
                connection = null;
                logger.info("SQL CommunicationsException while OXSettingsMySQL connection creation to MYSQL. Reconnect Count: "
                        + retryCount);
            } catch (InterruptedException e) {
                logger.error("Error while retrying for the MYSQL Connection");
                throw new ApplicationException(
                        ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(),
                        "Error while retrying for the MYSQL Connection");
            }
        } while (retryCount <= oxSettingsMaxRetryCount);

        if (result == null) {
            logger.error("Failed to connect to OXSettingsMySQL. Retried for "
                    + retryCount + ".No further retries..");
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name());
        }
        return result;
    }

    private void executeUpdate(List<String> params,
            String sql) throws SQLException, ApplicationException {
        java.sql.PreparedStatement st;
        int result;

        if (logger.isDebugEnabled()) {
            logger.debug("sql Update mysqlURL: " + mysqlURL);
            logger.debug("sql Update userName: " + userName);
            logger.debug("sql Update password: " + password);
            logger.debug("sql Update: " + sql);
            for (String param: params) {
                logger.debug("sql Update param: " + param);
            }
        }
        st = shardConnection.prepareStatement(sql);

        int paramIndex = 1;
        for (String param : params) {
            st.setString(paramIndex++, param);
        }
        try {
            result = st.executeUpdate();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (SQLException ex) {
                logger.error("Error while calling update on OX MySQL database");
            }
        }

        if (result == -1) {
            logger.error("Error while calling update on OX MySQL database");
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name());
        }
    }
    
    private void executeInsertAlias(List<String> params,
            String sql) throws SQLException, ApplicationException {
        java.sql.PreparedStatement st;
        int result;

        if (logger.isDebugEnabled()) {
            logger.debug("sql Update mysqlURL: " + mysqlURL);
            logger.debug("sql Update userName: " + userName);
            logger.debug("sql Update password: " + password);
            logger.debug("sql Update: " + sql);
            for (String param: params) {
                logger.debug("sql Update param: " + param);
            }
        }
        st = shardConnection.prepareStatement(sql);
        
        st.setString(1, params.get(0));
        st.setBytes(2, UUIDs.toByteArray(UUID.randomUUID()));
        st.setString(3, params.get(1));

        try {
            result = st.executeUpdate();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (SQLException ex) {
                logger.error("Error while calling update on OX MySQL database");
            }
        }

        if (result == -1) {
            logger.error("Error while calling update on OX MySQL database");
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name());
        }
    }

    /**
     * Get connection to shard database.
     * 
     * @param mxosRequestState
     * @return
     * @throws MxOSException
     */
    private Connection getShardConnection(MxOSRequestState mxosRequestState)
            throws MxOSException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String oxMySQLURL = null;
        String oxMySQLUser = null;
        String oxMySQLPassword = null;
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsCRUD getUserDatabaseConnection called.");
        }
        try {
            List<String> params = new ArrayList<String>();
            String email = mxosRequestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            Base base = ActionUtils.getUserInfoFromLDAP(email);
            // the email may be aliases or the part before @ is not same to userName
            final String userName = base.getUserName();
            mxosRequestState.getAdditionalParams().setProperty(MailboxProperty.userName, userName);
            mxosRequestState.getAdditionalParams().setProperty(MailboxProperty.email, base.getEmail());
            params.add(userName);
            rs = executeQuery(params, DB_WRITE_POOL_SQL,st);

            while (rs.next()) {
                oxMySQLURL = rs.getString(1);
                if (oxMySQLURL == null) {
                    // the user is not provisioned in OX, throw exception
                    logger.error("Error while retrieving jdbcUrl from OX MySQL database, "
                            + "pls check if user is provisioned in AppSuite.");
                    throw new ApplicationException(
                            ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name());
                }
                if (oxMySQLURL.contains("localhost")) {
                    if (mysqlURL.contains(rs.getString(2))) {
                        // reuse existing connection
                        return connection;
                    } else {
                        // connect to actual user database on same localhost
                        oxMySQLURL = mysqlURL.replace(CONFIG_DB_STRING, rs.getString(2));
                    }
                } else {
                    // none-localhost case
                    // mysql://masterdb/?useUnicode=true&characterEncoding=UTF-8&autoReconnect=true
                    // Need to place the db_schema name before the ? mark. 
                    // connect to actual user database on same localhost
                    oxMySQLURL = oxMySQLURL.replace("?", rs.getString(2) + "?");
                }

                oxMySQLUser = rs.getString(3);
                oxMySQLPassword = rs.getString(4);
            }
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error("SQLError while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationsException e) {
            logger.info(
                    "SQL CommunicationsException while update to OX MySQL database",
                    e);
        } catch (final Exception e) {
            logger.error("Error while updating the OX MySQL database", e);
            if (e instanceof MxOSException) {
                if (((MxOSException) e).getCode().equals(MailboxError.MBX_NOT_FOUND.name())) {
                    throw new MxOSException(
                            ErrorCode.OXS_USER_ID_NOT_FOUND.name(),
                            "Invalid Username.");
                } else {
                    throw new ApplicationException(
                            ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
                }
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
            }
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
            } catch (final Exception e) {
                logger.error("Error while closing rs, and st", e);
                throw new ApplicationException(
                        ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
            }
        }
        
        Connection localConnection = null;

        try {
            Class.forName(MYSQL_JDBC_DRIVER);
            if (logger.isDebugEnabled()) {
                logger.debug("oxMySQLURL: " + oxMySQLURL);
                logger.debug("oxMySQLUser: " + oxMySQLUser);
                logger.debug("oxMySQLPassword: " + oxMySQLPassword);
            }
            // connect to the database using replication driver
            // TODO - use connection pool based on keyed object pool
            localConnection = DriverManager.getConnection(oxMySQLURL, oxMySQLUser,
                    oxMySQLPassword);
            if (localConnection != null) {
                localConnection.setAutoCommit(false);
                if (logger.isDebugEnabled()) {
                    logger.debug("connection created.");
                }
            } else {
                throw new Exception(
                        "Error while connecting to OX MySQL database");
            }
        } catch (final CommunicationsException e) {
            logger.error(
                    "CommunicationsException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final MySQLNonTransientConnectionException e) {
            logger.error(
                    "MySQLNonTransientConnectionException while connecting to OX MySQL database",
                    e);
            throw new MxOSException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            logger.error("Exception while connecting to OX MySQL database : " +  e.getMessage());
        }
        return localConnection;
    }

    @Override
    public String readSetting(MxOSRequestState mxosRequestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsMySQLCRUD readSetting is called...");
        }
        if (mxosRequestState.getAdditionalParams().contains(OXSettingsProperty.ox_setting)) {
            String field = mxosRequestState.getAdditionalParams().getProperty(OXSettingsProperty.ox_setting);
            if (!Strings.isNullOrEmpty(field)) {
                OXSettingsProperty p = OXSettingsProperty.valueOf(field);
                try {
                    shardConnection = getShardConnection(mxosRequestState);
                    if (shardConnection != null) {
                        switch (p) {
                        case preferredLanguage:
                            return selectPreferredLanguage(mxosRequestState);
                        case send_addr:
                            return selectSendAddress(mxosRequestState);
                        default:
                            break;
                        }
                    }
                } finally {
                    if (shardConnection != null && shardConnection != connection) {
                        try {
                            shardConnection.close();
                        } catch (SQLException e) {
                            logger.error("Error while closing connection for shard DB", e);
                            throw new ApplicationException(ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
                        } finally {
                            shardConnection = null;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    private String selectPreferredLanguage(MxOSRequestState requestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Select preferredLanguage from AppSuite MySQL DB...");
        }

        String email = requestState.getAdditionalParams().getProperty(
                MailboxProperty.email);
        String userName = requestState.getAdditionalParams().getProperty(
                MailboxProperty.userName);
        if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(userName)) {
            return null;
        } else {
            return selectFieldFromShardDB(Arrays.asList(email, userName),
                    SELECT_LANGUAGE_SQL);
        }
    }
    
    private String selectSendAddress(MxOSRequestState requestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("Select primary_addr from AppSuite MySQL DB...");
        }

        String email = requestState.getAdditionalParams().getProperty(
                MailboxProperty.email);
        String userName = requestState.getAdditionalParams().getProperty(
                MailboxProperty.userName);
        if (Strings.isNullOrEmpty(email) || Strings.isNullOrEmpty(userName)) {
            return null;
        } else {
            return selectFieldFromShardDB(Arrays.asList(email, userName),
                    SELECT_SEND_ADDRESS_SQL);
        }
    }
    
    private String selectFieldFromShardDB(List<String> params, String sql)
            throws ApplicationException {
        if (logger.isDebugEnabled()) {
            logger.debug("sql Select mysqlURL: " + mysqlURL);
            logger.debug("sql Select userName: " + userName);
            logger.debug("sql Select: " + sql);
            for (String param : params) {
                logger.debug("sql Select param: " + param);
            }
        }
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            st = shardConnection
                    .prepareStatement(sql);
            int paramIndex = 1;
            for (String param : params) {
                st.setString(paramIndex++, param);
            }
            rs = st.executeQuery();
            String field = null;
            if (rs.next()) {
                field = rs.getString(1);
            }
            return field;
        } catch (SQLException e) {
            logger.error("Error while querying from ShardDB", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }finally{
            try{
                if(rs != null){
                    rs.close();
                }
                if(st != null){
                    st.close();
                }
            }catch(SQLException ex){
                logger.error("Error while querying from ShardDB", ex);
            }
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        if (shardConnection == null) {
            // transaction is already finalized
            return;
        }
        try {
            shardConnection.rollback();
            if (shardConnection != connection) {
                logger.debug("closing shard connection");
                shardConnection.close();
            }
            shardConnection = null;
        } catch (final Exception e) {
            logger.error("Error while rollback to OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     *  This API checks if there is anything to update to OX via MySQL
     *  
     *  @param mxosRequestState mxosRequestState
     *  @return true if something to update in OX otherwise false
     */
    public static boolean needUpdate(final MxOSRequestState mxosRequestState) {
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        Attributes attrs = ((LDAPBackendState) backendState).attributes;
        if (attrs != null && attrs.size() > 0) {
            Attribute attr = attrs
                    .get(LDAPMailboxProperty.netmaillocale.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.msgtimezone.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.netmailusetrash.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.netmailmsgviewprivacyfilter
                    .name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.mailincludeoriginal.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.mailfrom.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.netMailAutoCollect.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.mailreplyto.name());
            if (attr != null) {
                return true;
            }
            attr = attrs.get(LDAPMailboxProperty.netmailweblinewidth.name());
            if (attr != null) {
                return true;
            }

            attr = attrs.get(LDAPMailboxProperty.mailalternateaddress.name());
            if (attr != null) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void updateSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsCRUD updateSetting called.");
        }
        shardConnection = getShardConnection(mxosRequestState);
        if (shardConnection == null) {
            return;
        }
        try {
            IBackendState backendState = mxosRequestState.getBackendState()
                    .get(MailboxDBTypes.ldap.name());
            Attributes attrs = ((LDAPBackendState) backendState).attributes;
            List<String> params = new ArrayList<String>();
            // the two values are set in getShardConnection method
            String email = mxosRequestState.getAdditionalParams().getProperty(
                    MailboxProperty.email);
            String userName = mxosRequestState.getAdditionalParams()
                    .getProperty(MailboxProperty.userName);

            if (attrs != null && attrs.size() > 0) {
                Attribute attr = attrs.get(LDAPMailboxProperty.netmaillocale
                        .name());
                if (attr != null) {
                    String locale = attr.get().toString();
                    params.clear();
                    params.add(locale);
                    params.add(userName);
                    executeUpdate(params, LANGUAGE_SQL);
                }
                attr = attrs.get(LDAPMailboxProperty.msgtimezone.name());
                if (attr != null) {
                    String locale = attr.get().toString();
                    params.clear();
                    params.add(locale);
                    params.add(userName);
                    executeUpdate(params, TIMEZONE_SQL);
                }
                attr = attrs.get(LDAPMailboxProperty.netmailusetrash.name());
                if (attr != null) {
                    String sql;
                    String netMailUseTrash = attr.get().toString();
                    if (netMailUseTrash.equals("1")) {
                        sql = ENABLE_DELETE_MAIL_PERMANENT_SQL;
                    } else {
                        sql = DISABLE_DELETE_MAIL_PERMANENT_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    executeUpdate(params, sql);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.netmailmsgviewprivacyfilter
                                .name());
                if (attr != null) {
                    String sql;
                    String netmailmsgviewprivacyfilter = attr.get().toString();
                    if (netmailmsgviewprivacyfilter.equals("TRUE")) {
                        sql = DISABLE_IMAGE_IN_HTML_SQL;
                    } else {
                        sql = ENABLE_IMAGE_IN_HTML_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    executeUpdate(params, sql);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.mailincludeoriginal.name());
                if (attr != null) {
                    String sql;
                    String mailIncludeOriginal = attr.get().toString();
                    if (mailIncludeOriginal.equals("1")) {
                        sql = INCLUDE_ORIGINAL_IN_REPLY_SQL;
                    } else {
                        sql = EXCLUDE_ORIGINAL_FROM_REPLY_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.mailfrom.name());
                if (attr != null) {
                    String sql = FROM_ADDRESS_SQL;
                    String mailFrom = attr.get().toString();
                    params.clear();
                    params.add(mailFrom);
                    params.add(userName);
                    executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.netMailAutoCollect.name());
                if (attr != null) {
                    String sql = "";
                    String netMailAutoCollect = attr.get().toString();
                    if (netMailAutoCollect.isEmpty()
                            || netMailAutoCollect.equals(NONE_STRING)) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_READING_SQL;
                    } else if (netMailAutoCollect.equals("all")) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_ALL_MAIL_SQL;
                    } else if (netMailAutoCollect.equals("to")) {
                        sql = AUTO_COLLECT_CONTACTS_WHILE_SENDING_SQL;
                    }
                    params.clear();
                    params.add(userName);
                    executeUpdate(params, sql);
                }
                attr = attrs.get(LDAPMailboxProperty.mailreplyto.name());
                if (attr != null) {
                    String mailreplyto = attr.get().toString();
                    params.clear();
                    params.add(mailreplyto);
                    params.add(userName);
                    executeUpdate(params, REPLY_TO_SQL);
                }
                attr = attrs
                        .get(LDAPMailboxProperty.netmailweblinewidth.name());
                if (attr != null) {
                    String netmailweblinewidth = attr.get().toString();
                    if (StringUtils.isNumeric(netmailweblinewidth)) {
                        params.clear();
                        params.add(netmailweblinewidth);
                        params.add(userName);
                        executeUpdate(params, LINE_WRAP_SQL);
                    }
                }

                attr = attrs.get(LDAPMailboxProperty.mailalternateaddress
                        .name());
                if (attr != null) {
                    params.clear();
                    params.add(userName);
                    executeUpdate(params, DELETE_ALL_ALIAS_SQL);
                    for (int i = 0; i < attr.size(); i++) {
                        String alias = attr.get(i).toString();
                        params.clear();
                        params.add(alias);
                        params.add(userName);
                        executeInsertAlias(params, INSERT_ALIAS_SQL);
                    }
                    params.clear();
                    params.add(email);
                    params.add(userName);
                    executeInsertAlias(params, INSERT_ALIAS_SQL);
                }
                commit();
            }
        } catch (final MySQLNonTransientConnectionException e) {
            rollback();
            logger.error("SQLError while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final CommunicationsException e) {
            rollback();
            logger.info(
                    "SQL CommunicationsException while update to OX MySQL database",
                    e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
        } finally {
            if (shardConnection != null && shardConnection != connection) {
		try {
		    shardConnection.close();
		} catch (SQLException e) {
		    logger.error("Error while closing connection for shard DB",
			    e);
		    throw new ApplicationException(
			    ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
		} finally {
		    shardConnection = null;
		}
            }
        }
    }

    /*
     * ----------- Searches contacts directly in OX DB and gets the contacts
     * based on query parameters for a particular user. ----------
     */
    @Override
    public List<ContactBase> searchContactBase(MxOSRequestState requestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("OXSettingsCRUD readContactBase called.");
        }
        ResultSet rs = null;
        PreparedStatement st = null;
        try {
            shardConnection = getShardConnection(requestState);
        } catch (MxOSException e) {
            throw e;
        }

        List<ContactBase> contactBases = null;
        try {
            // request parameter 'query' is used to create the where clause for
            // SQL query.
            String query = requestState.getInputParams()
                    .get(AddressBookProperty.query.name()).get(0);
            StringBuilder whereClause = new StringBuilder();

            if(query.endsWith("=")){
                throw new AddressBookException(AddressBookError.ABS_INVALID_SEARCH_QUERY.name());
            }
            // parsing the query to get the attributes and conditions for SQL
            // query.
            Query parsedQuery = new Query();
            parsedQuery.parse(query);
            List<Visitor> visitorList = parsedQuery.getVisitors();
            List<Condition> conditionList = parsedQuery.getConditions();
            Iterator<Visitor> vIter = visitorList.iterator();
            Iterator<Condition> conIter = conditionList.iterator();
            
            // creating the where clause for the SQL query.
            whereClause.append(" ( ");
            while (vIter.hasNext()) {
                Term term = (Term) vIter.next();
                if (term.getOperand().equals(
                        OXAddressBookUtil.JSONToOX_AddressBook.get(
                                AddressBookProperty.valueOf("workInfoEmail"))
                                .toString())) {
                    whereClause.append(" LOWER("
                            + OXAddressBookUtil.JSONToOX_AddressBookMYSQL.get(
                                    AddressBookProperty
                                            .valueOf("workInfoEmail"))
                                    .toString() + ") = LOWER('"
                            + term.getValue() + "')");
                }
                if (term.getOperand().equals(
                        OXAddressBookUtil.JSONToOX_AddressBook.get(
                                AddressBookProperty
                                        .valueOf("personalInfoEmail"))
                                .toString())) {
                    whereClause.append(" LOWER("
                            + OXAddressBookUtil.JSONToOX_AddressBookMYSQL.get(
                                    AddressBookProperty
                                            .valueOf("personalInfoEmail"))
                                    .toString() + ") = LOWER('"
                            + term.getValue() + "')");
                }
                if (term.getOperand().equals(
                        OXAddressBookUtil.JSONToOX_AddressBook.get(
                                AddressBookProperty.valueOf("email3"))
                                .toString())) {
                    whereClause.append(" LOWER("
                            + OXAddressBookUtil.JSONToOX_AddressBookMYSQL.get(
                                    AddressBookProperty.valueOf("email3"))
                                    .toString() + ") = LOWER('"
                            + term.getValue() + "')");
                }
                if (conIter.hasNext()) {
                    whereClause.append(" " + conIter.next());
                }
            }
            whereClause.append(" )");
            // preparing the actual SQL query.
            String sql = new StringBuilder()
                    .append(SELECT_USER_CONTACT_BASE_SQL).append(whereClause)
                    .toString();
            
            // preparing JDBC prepareStatement which will be executed.
            st = shardConnection.prepareStatement(sql);
            st.setString(
                    1,
                    requestState.getAdditionalParams().getProperty(
                            MailboxProperty.email));
            
            // executing the prepareStatement and fetches the data from OX DB.
            rs = st.executeQuery();
            contactBases = new ArrayList<ContactBase>();
            
            // setting the ContactBase objects based on the data that were fetched from OX DB.
            while (rs.next()) {
                ContactBase contactBase = new ContactBase();
                String contactID = "" + rs.getInt("intfield01");
                String folderID = "" + rs.getInt("fid");
                logger.info("Contact ID: " + contactID);
                contactBase.setContactId(contactID);
                contactBase.setCategories("");
                contactBase.setColorLabel(null);
                contactBase.setCreated("");
                contactBase.setCustomFields("");
                contactBase.setFolderId(folderID);
                contactBase.setIsPrivate(null);
                contactBase.setNotes("");
                contactBase.setUpdated("");
                contactBases.add(contactBase);
            }
        } catch (AddressBookException e) {
            logger.error("Error while parsing the query: ", e);
            throw new MxOSException(e.getCode(), e);
        } catch (SQLException se) {
            se.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (shardConnection != null) {
                    shardConnection.close();
                }

            } catch (final Exception e) {
                logger.error("Error while closing rs, st, and shardConnection",
                        e);
                throw new ApplicationException(
                        ErrorCode.OXS_MYSQL_CONNECTION_ERROR.name(), e);
            }

        }
        return contactBases;
    }
}
