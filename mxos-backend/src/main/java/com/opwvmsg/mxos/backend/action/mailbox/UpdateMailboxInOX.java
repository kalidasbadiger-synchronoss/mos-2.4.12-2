/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.ox.settings.OXSettingsHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.settings.OXSettingsMySQLCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to update mailbox in OX
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMailboxInOX extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateMailboxInOX.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInOX action start.");
        }

        // Update in OX if backend is set to OX
        boolean appSuiteIntegrated = Boolean.valueOf(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        boolean oxBackend = AddressBookDBTypes.ox.name().equals(
                MxOSConfig.getAddressBookBackend());
        if (appSuiteIntegrated && oxBackend) {
            updateOXMySQL(requestState);
            updateOXHTTP(requestState);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInOX action end.");
        }
    }

    private void updateOXMySQL(final MxOSRequestState requestState)
            throws MxOSException {
        if (!OXSettingsMySQLCRUD.needUpdate(requestState)) {
            logger.debug("Nothing to update for OX MySQL");
            return;
        }

        // call mysql update methods
        ICRUDPool<ISettingsCRUD> oxMySQLSettingsCRUDPool = null;
        ISettingsCRUD oxMySQLSettingsCRUD = null;
        try {
            oxMySQLSettingsCRUDPool = MxOSApp.getInstance()
                    .getOXSettingsMySQLCRUD();
            oxMySQLSettingsCRUD = oxMySQLSettingsCRUDPool.borrowObject();
            oxMySQLSettingsCRUD.updateSetting(requestState);
            oxMySQLSettingsCRUD.commit();
            if (logger.isDebugEnabled()) {
                logger.debug("Update mailbox for OX MySQL successfully.");
            }
        } catch (final MxOSException e) {
            logger.error("Error while updating mailbox for OX MySQL.", e);
            if (oxMySQLSettingsCRUD != null) {
                oxMySQLSettingsCRUD.rollback();
            }
            throw e;
        } catch (Exception e) {
            logger.error("Error while updating mailbox for OX MySQL.", e);
            if (oxMySQLSettingsCRUD != null) {
                oxMySQLSettingsCRUD.rollback();
            }
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            CRUDUtils.releaseConnection(oxMySQLSettingsCRUDPool,
                    oxMySQLSettingsCRUD);
        }
    }

    private void updateOXHTTP(final MxOSRequestState requestState)
            throws MxOSException {
        if (!OXSettingsHttpCRUD.needUpdate(requestState)) {
            logger.debug("Nothing to update for OX HTTP");
            return;
        }
        // Call HTTP Updates
        ICRUDPool<ISettingsCRUD> oxHTTPSettingsCRUDPool = null;
        ISettingsCRUD oxHTTPSettingsCRUD = null;
        try {
            // call http update methods
            oxHTTPSettingsCRUDPool = MxOSApp.getInstance()
                    .getOXSettingsHttpCRUD();
            oxHTTPSettingsCRUD = oxHTTPSettingsCRUDPool.borrowObject();
            oxHTTPSettingsCRUD.updateSetting(requestState);
            if (logger.isDebugEnabled()) {
                logger.debug("Update mailbox for OX HTTP successfully.");
            }
        } catch (final MxOSException e) {
            logger.error("Error while updating mailbox for OX HTTP.", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updating mailbox for OX HTTP.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            CRUDUtils.releaseConnection(oxHTTPSettingsCRUDPool,
                    oxHTTPSettingsCRUD);
        }
    }
}
