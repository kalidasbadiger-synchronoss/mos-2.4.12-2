/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to create Message.
 * 
 * @author mxos-dev
 */
public class GetMessageBody implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMessageBody.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMessageBody action start."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;
        Body body = null;

        try {                        
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            if (logger.isDebugEnabled()) {
                logger.debug("Retrieving Internal Info for Email :" + email);
            }
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            blobCRUD = CRUDUtils.getBlobCRUD(requestState);
            body = new Body();
            /*
             * TODO: For now this API fills in only message body in Message
             * POJO. Other fields will remain blank on message POJO. In future
             * we might want to send it as separate POJO.
             */
            MessageServiceHelper.readBody(metaCRUD, blobCRUD, requestState,
                    body);
            metaCRUD.commit();
            blobCRUD.commit();
        } catch (final MxOSException e) {
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get message body.", e);
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMessageBody action end."));
        }
    }
}
