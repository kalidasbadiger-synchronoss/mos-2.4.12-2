package com.opwvmsg.mxos.backend.crud.exception;

public class ComponentException extends Exception {

    private static final long serialVersionUID = 8634793376715923619L;

    protected String category;
    protected String code;
    protected String errorId;

    public ComponentException(Exception e) {
        super(e);
    }

    public ComponentException(String message) {
        super(message);
    }

    public ComponentException(String message, Exception e) {
        super(message, e);
    }

    public ComponentException(String message, String category, String code,
            String errorId) {
        super(message);
        this.category = category;
        this.code = code;
        this.errorId = errorId;
    }

    public ComponentException(String message, String category, String code,
            String errorId, Exception e) {
        super(message, e);
        this.category = category;
        this.code = code;
        this.errorId = errorId;
    }

    public String getCategory() {
        return category;
    }

    public String getCode() {
        return code;
    }

    public String getErrorId() {
        return errorId;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }
}
