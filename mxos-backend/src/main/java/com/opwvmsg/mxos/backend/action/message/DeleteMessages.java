/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to Delete Message.
 * 
 * @author mxos-dev
 */
public class DeleteMessages implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteMessages.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMessage action start."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        IBlobCRUD blobCRUD = null;
        try {
            List<String> messageIdsList = requestState.getInputParams()
            .get(MessageProperty.messageId.name());
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD = CRUDUtils.getBlobCRUD(requestState);
            }

            String[] messageIds = messageIdsList
                    .toArray(new String[messageIdsList.size()]);
            MessageServiceHelper.delete(metaCRUD, blobCRUD, requestState,
                    messageIds);

            if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                blobCRUD.commit();
            }
            metaCRUD.commit();
        } catch (final MxOSException e) {
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while delete message.", e);
            if (blobCRUD != null) {
                blobCRUD.rollback();
            }
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    MessageError.MSG_UNABLE_TO_PERFORM_DELETE.name(), e);
        } finally {
            try {
                if (metaCRUDPool != null && metaCRUD != null) {
                    metaCRUDPool.returnObject(metaCRUD);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteMessage action end."));
        }
    }
}
