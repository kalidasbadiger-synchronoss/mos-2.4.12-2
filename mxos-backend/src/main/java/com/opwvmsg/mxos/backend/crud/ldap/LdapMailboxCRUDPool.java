/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Implementation of LDAP CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class LdapMailboxCRUDPool implements ICRUDPool<IMailboxCRUD> {
    protected static Logger logger = Logger.getLogger(LdapMailboxCRUDPool.class);

    protected GenericObjectPool<LdapMailboxCRUD> objPool;

    protected int maxSize = 10;
    
    protected ConnectionStats poolStatsActiveClients;
    
    final LdapCRUDFactory factory; 
    
    public enum Type {
        REGULAR,
        SEARCH,
    }
    
    final protected Type type;
    
    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public LdapMailboxCRUDPool(Type type) throws MxOSException {
        
        // Configure the pool
        this.type = type;
        ConnectionStats poolStatsMax;
        if (type == Type.REGULAR) {
            factory = new LdapCRUDFactory();
            poolStatsMax = ConnectionStats.LDAP;
            poolStatsActiveClients = ConnectionStats.ACTIVE_LDAP;
        } else {
            factory = new LdapSearchCRUDFactory();
            poolStatsMax = ConnectionStats.LDAPSEARCH;
            poolStatsActiveClients = ConnectionStats.ACTIVE_LDAPSEARCH;
        }
        
        // initialize        
        this.maxSize = MxOSConfig.getLdapPoolMaxSize();
        loadPool(maxSize);
        initializeJMXStats(poolStatsMax, maxSize);
        logger.info("Loaded LdapConnectionPool...");
    }

    /**
     * Method to load Pool.
     */
    protected void loadPool(int maxSize) {
        objPool = new GenericObjectPool<LdapMailboxCRUD>(factory, maxSize);
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
    }

    @Override
    public void resetPool() throws MxOSException {
        if (null != objPool) {
            objPool.clear();
            logger.info("# Clearing the pools..");
        }
    }


    @Override
    public IMailboxCRUD borrowObject() throws MxOSException {
        IMailboxCRUD obj = null;
        logger.info(new StringBuilder("(type=").append(type)
                .append(") Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        if (objPool.getNumActive() >= maxSize - 2) {
            logger.warn("LDAP (" + type.name()
                    + ") active connections reaching max...:"            
                    + objPool.getNumActive());
        }
        
        try {
            factory.notifyBorrow();
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("type=" + type + ", # Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("type=" + type + ", Borrowed object is null.");
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    @Override
    public void returnObject(IMailboxCRUD provisionCRUD) throws MxOSException {
        try {
            LdapMailboxCRUD obj = (LdapMailboxCRUD) provisionCRUD;
            obj.releaseResources();
            objPool.returnObject(obj);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("type=" + type + ", Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        logger.info(new StringBuilder("(type=").append(type)
                .append(") Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(ConnectionStats poolStatsMax, long count) {
        poolStatsMax.setCount(count);
        if (logger.isDebugEnabled()) {
            logger.debug("type=" + type + ", JMX stats initialized. Type: " + type.name()
                    + ", Count: " + count);
        }
    }

    protected void incrementJMXStats() {
        if (logger.isDebugEnabled()) {
            logger.debug("type=" + type + ", Incrementing " + poolStatsActiveClients.name() + " stats");
        }
        poolStatsActiveClients.increment();
    }

    protected void decrementJMXStats() {
        if (logger.isDebugEnabled()) {
            logger.debug("type=" + type + ", Decrementing " + poolStatsActiveClients.name() + " stats");
        }
        poolStatsActiveClients.decrement();
    }

}