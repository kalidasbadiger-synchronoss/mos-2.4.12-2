/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap;

import java.util.List;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.ldap.ops.Bind;
import com.opwvmsg.mxos.backend.ldap.ops.Construct;
import com.opwvmsg.mxos.backend.ldap.ops.GetAttributes;
import com.opwvmsg.mxos.backend.ldap.ops.ModifyAttributes;
import com.opwvmsg.mxos.backend.ldap.ops.Operation;
import com.opwvmsg.mxos.backend.ldap.ops.Search;
import com.opwvmsg.mxos.backend.ldap.ops.Unbind;

/**
 * Class to implement LDAP operation
 * 
 * @author mxos-dev
 * 
 */
public class LDAPTemplate {
    private static Logger logger = Logger.getLogger(LDAPTemplate.class);

    private LDAPPolicy policy = null;

    private DirContext ldapContext = null;
    private boolean cacheServer;

    // used for failback plocy
    private int initialAssignedServerIndex = 0;
    private int currrentServerIndex = 0;
    private long lastFailBackTime = 0;

    private boolean connected = false;

    public LDAPTemplate(LDAPPolicy policy, boolean cacheServer)
            throws NamingException {
        this.policy = policy;
        this.cacheServer = cacheServer;
        this.initialAssignedServerIndex = LDAPLinkManager.instance().select(
                cacheServer);
        this.currrentServerIndex = this.initialAssignedServerIndex;
        createNewLDAPContext(LDAPLinkManager.instance().getLDAPLink(
                cacheServer, initialAssignedServerIndex));
    }

    public void bind(String name, Object obj, Attributes attrs)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Bind bind = new Bind(name, obj, attrs);
        bind.setLdapContext(ldapContext);
        bind.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(bind);
    }

    public void unbind(String name) throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Unbind unbind = new Unbind(name);
        unbind.setLdapContext(ldapContext);
        unbind.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(unbind);
    }

    public void modifyAttributes(String name, int mod_op, Attributes attrs)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        ModifyAttributes modifyAttributes = new ModifyAttributes(name, mod_op,
                attrs);
        modifyAttributes.setLdapContext(ldapContext);
        modifyAttributes.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(modifyAttributes);
    }

    public void modifyAttributes(String name, ModificationItem[] mods)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        ModifyAttributes modifyAttributes = new ModifyAttributes(name, mods);
        modifyAttributes.setLdapContext(ldapContext);
        modifyAttributes.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(modifyAttributes);
    }

    public NamingEnumeration<SearchResult> search(String name,
            Attributes matchingAttributes, String[] attributesToReturn)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, matchingAttributes, attributesToReturn);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults();
    }

    public <T> List<T> search(String name, Attributes matchingAttributes,
            String[] attributesToReturn, LDAPMapper<T> mapper)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, matchingAttributes, attributesToReturn);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults(mapper);
    }

    public NamingEnumeration<SearchResult> search(String name, String filter,
            SearchControls cons) throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, filter, cons);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults();
    }

    public <T> List<T> search(String name, String filter, SearchControls cons,
            LDAPMapper<T> mapper) throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, filter, cons);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults(mapper);
    }

    public NamingEnumeration<SearchResult> search(String name,
            String filterExpr, Object[] filterArgs, SearchControls cons)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, filterExpr, filterArgs, cons);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults();
    }

    public <T> List<T> search(String name, String filterExpr,
            Object[] filterArgs, SearchControls cons, LDAPMapper<T> mapper)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        Search search = new Search(name, filterExpr, filterArgs, cons);
        search.setLdapContext(ldapContext);
        search.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(search);
        return search.getResults(mapper);
    }

    public Attributes getAttributes(String name, String[] attrIds)
            throws NamingException {
        backToInitialLDAPServerIfPossiable();
        GetAttributes getAttributes = new GetAttributes(name, attrIds);
        getAttributes.setLdapContext(ldapContext);
        getAttributes.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(getAttributes);
        return getAttributes.getResult();
    }

    public <T> T getAttributes(String name, String[] attrIds,
            LDAPMapper<T> mapper) throws NamingException {
        backToInitialLDAPServerIfPossiable();
        GetAttributes getAttributes = new GetAttributes(name, attrIds);
        getAttributes.setLdapContext(ldapContext);
        getAttributes.retryWhenOperationFailed(policy.getRetryExceptions(),
                policy.getMaxRetryCount(), policy.getRetryInterval());
        executeWithfailover(getAttributes);
        return getAttributes.getResult(mapper);
    }

    public boolean isConnected() {
        return connected;
    }

    private void createNewLDAPContext(LDAPLink link) throws NamingException {
        logger.info("Init LDAP context: LDAPLinkInfo=" + link);

        boolean success = true;
        int serverCount = 0;
        // create a new LDAP context by given link
        do {
            try {
                if (!success) {
                    calculateNextServer();
                    link = LDAPLinkManager.instance().getLDAPLink(cacheServer,
                            currrentServerIndex);
                    serverCount++;
                    logger.info("Trying next LDAP server: " + link);
                }
                createLDAPContext(link, true);
                success = true;
                this.connected = true;
            } catch (NamingException e) {
                success = false;
                this.connected = false;
                logger.error(
                        "Failed to create LDAP context for host : "
                                + link.getUrl(), e);
                if (policy.isFailover()) {
                    if (serverCount < LDAPLinkManager.instance().getSize(
                            cacheServer)) {
                        continue;
                    } else {
                        throw e;
                    }
                } else {
                    throw e;
                }
            }
        } while (!success);
    }

    private void createLDAPContext(LDAPLink link, boolean setRetry)
            throws NamingException {
        Construct context = new Construct(link);
        if (setRetry) {
            context.retryWhenOperationFailed(policy.getRetryExceptions(),
                    policy.getMaxRetryCount(), policy.getRetryInterval());
        }
        context.execute();
        this.ldapContext = context.getLdapContext();
    }

    private void executeWithfailover(Operation operation)
            throws NamingException {
        boolean success = true;
        int serverCount = 0;
        do {
            try {
                if (!success) {
                    // close failed LDAP context first
                    if (ldapContext != null) {
                        logger.info("LDAP context has existed, close it first.");
                        try {
                            ldapContext.close();
                        } catch (NamingException e) {
                            logger.error(
                                    "Close LDAP context failed, ignore it, continue to create a new LDAP context",
                                    e);
                        }
                    }

                    // try other LDAP server
                    calculateNextServer();
                    LDAPLink link = LDAPLinkManager.instance().getLDAPLink(
                            cacheServer, currrentServerIndex);
                    createLDAPContext(link, true);
                    operation.setLdapContext(ldapContext);

                    serverCount++;
                    logger.info("Trying next LDAP server: "
                            + getCurrentLDAPURL());
                }
                operation.execute();
            } catch (NamingException e) {
                success = false;
                logger.error("Operation [" + operation.getOperationName()
                        + "] failed on the LDAP server : "
                        + getCurrentLDAPURL(), e);
                if (policy.isFailover()) {
                    if (serverCount < LDAPLinkManager.instance().getSize(
                            cacheServer)) {
                        continue;
                    } else {
                        throw e;
                    }
                } else {
                    throw e;
                }
            }
        } while (!success);
    }

    private String getCurrentLDAPURL() throws NamingException {
        if (ldapContext != null) {
            Object url = ldapContext.getEnvironment().get(
                    DirContext.PROVIDER_URL);
            return url != null ? url.toString() : null;
        } else {
            return null;
        }
    }

    private void calculateNextServer() {
        currrentServerIndex = currrentServerIndex + 1;
        if (currrentServerIndex >= LDAPLinkManager.instance().getSize(
                cacheServer)) {
            currrentServerIndex = 0;
        }
    }

    private void backToInitialLDAPServerIfPossiable() {
        if (policy.isFailback()) {
            if (initialAssignedServerIndex != currrentServerIndex) {
                try {
                    // this is the start time that back to initial LDAP server
                    if (lastFailBackTime == 0) {
                        this.lastFailBackTime = System.currentTimeMillis();
                    } else {
                        // only exceed the interval time, mOS try to connect
                        // initial LDAP server
                        if (System.currentTimeMillis() - lastFailBackTime > policy
                                .getFailbackRetryInterval()) {
                            LDAPLink link = LDAPLinkManager.instance()
                                    .getLDAPLink(cacheServer,
                                            initialAssignedServerIndex);
                            createLDAPContext(link, false);

                            // if success to back to initial LDAP server
                            this.lastFailBackTime = 0;
                        }
                    }
                } catch (NamingException e) {
                    this.lastFailBackTime = System.currentTimeMillis();
                    logger.warn("Can not be back to initial LDAP server.");
                }
            }
        }
    }

}
