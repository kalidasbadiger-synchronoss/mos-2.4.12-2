/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.socialnetworks;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetSNIntegrationAllowed implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetSNIntegrationAllowed.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSNIntegrationAllowed action start."));
        }
        final String sNIntegrationAllowed = requestState.getInputParams()
                .get(MailboxProperty.socialNetworkIntegrationAllowed.name()).get(0);

        if (sNIntegrationAllowed != null) {
            if (sNIntegrationAllowed.equalsIgnoreCase(MxosEnums.BooleanType.YES
                    .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.socialNetworkIntegrationAllowed,
                                Integer.toString(MxosEnums.BooleanType.YES
                                        .ordinal()));
            } else if (sNIntegrationAllowed
                    .equalsIgnoreCase(MxosEnums.BooleanType.NO.name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.socialNetworkIntegrationAllowed,
                                Integer.toString(MxosEnums.BooleanType.NO
                                        .ordinal()));
            } else if (sNIntegrationAllowed.equals("")){
                MxOSApp.getInstance()
                .getMailboxHelper()
                .setAttribute(
                        requestState,
                        MailboxProperty.socialNetworkIntegrationAllowed, "");
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_SNINTEGRATIONALLOWED.name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetSNIntegrationAllowed action end."));
        }
    }
}
