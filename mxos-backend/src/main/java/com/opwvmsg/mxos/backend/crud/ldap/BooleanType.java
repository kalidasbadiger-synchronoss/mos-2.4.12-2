/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/data/BooleanEnum.java#1 $
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
 * @author mxos-dev
 * 
 */
public enum BooleanType implements DataMap.Property {
    NO("FALSE", "Regular"),
    YES("TRUE", "Business");

    private String boolValue;
    private String businessFeature;

    private BooleanType(String boolValue, String businessFeature) {
        this.boolValue = boolValue;
        this.businessFeature = businessFeature;
    }

    public static BooleanType getByOrdinal(String value) throws InvalidRequestException {
        int ordinal = -1;
        try {
            ordinal = Integer.parseInt(value);
        } catch (NumberFormatException e) {}
        if (ordinal < 0 || ordinal >= values().length) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return values()[ordinal];
    }

    public static BooleanType getByValue(String value) throws InvalidRequestException {
        BooleanType returnVal = null;
        for (BooleanType type : values()) {
            if (type.name().equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public static BooleanType getByBoolValue(String value) throws InvalidRequestException {
        BooleanType returnVal = null;
        for (BooleanType type : values()) {
            if (type.boolValue.equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public static BooleanType getByBusinessFeature(String value) throws InvalidRequestException {
        BooleanType returnVal = null;
        for (BooleanType type : values()) {
            if (type.businessFeature.equalsIgnoreCase(value)) {
                returnVal = type;
                break;
            }
        }
        if (null == returnVal) {
            throw new InvalidRequestException(
                    ErrorCode.GEN_INVALID_DATA.name(), "Invalid data supplied: " + value);
        }
        return returnVal;
    }

    public String getBoolValue() {
        return boolValue;
    }

    public String getBusinessFeature() {
        return businessFeature;
    }

    public String getValue() {
        return name().toLowerCase();
    }

}
