/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.Name;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IContactsNameService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Contacts Name service exposed to client which is responsible for doing basic
 * name related activities e.g read name, update name, etc. directly in the
 * database.
 * 
 * @author mxos-dev
 */
public class BackendContactsNameService implements IContactsNameService {

    private static Logger logger = Logger
            .getLogger(BackendContactsNameService.class);

    /**
     * Default Constructor.
     */
    public BackendContactsNameService() {
        logger.info("BackendContactsNameService Service created...");
    }

    /**
     * Read
     */
    @Override
    public Name read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsNameService, Operation.GET);
            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsNameService, Operation.GET, t0,
                    StatStatus.pass);

            return ((Name) mxosRequestState.getDbPojoMap().getPropertyAsObject(
                    MxOSPOJOs.contactName));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsNameService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    /**
     * Update
     */
    @Override
    public void update(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ContactsNameService,
                    Operation.POST);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ContactsNameService, Operation.POST,
                    t0, StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ContactsNameService, Operation.POST,
                    t0, StatStatus.fail);
            throw e;
        }
    }
}
