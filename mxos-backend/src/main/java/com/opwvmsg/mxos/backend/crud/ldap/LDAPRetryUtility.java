/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * LDAP Retry Utility class.
 * 
 * @author mxos-dev
 */
public class LDAPRetryUtility {
    private static Logger logger = Logger.getLogger(LDAPRetryUtility.class);

    public static final String JAVAX_NAMING_PKG = "javax.naming.";
    public static List<Class> expsClasses;

    static {
        final String[] exps = MxOSConfig.getLdapRetryOnExceptions();
        if (exps != null && exps.length > 0) {
            expsClasses = new ArrayList<Class>();
            for (final String exp : exps) {
                if (exp != null && !"".equals(exp.trim())) {
                    try {
                        expsClasses.add(Class.forName(JAVAX_NAMING_PKG + exp.trim()));
                    } catch (final ClassNotFoundException e) {
                        logger.warn(exp
                                + " is not a valid LDAP Excpetion, skipping it.");
                    }
                }
            }
        }
    }

    /**
     * The method to find whether retry logic should applicable or not.
     * 
     * @param ne NamingException
     * @return boolean true- retry required, false - not required
     */
    public static boolean isRetryRequired(final Object ne) {
        if (ne != null) {
            for (final Class expClass : expsClasses) {
                if (ne.getClass() == expClass) {
                    logger.info("LDAP Retry is required: " + ne.toString());
                    return true;
                }
            }
            
            // This section is used to handle retry on ldap timeout errors
            // Connection timedout
            // Read timedout
            if (ne instanceof NamingException) {
                NamingException e = (NamingException) ne;
                final String errMsg;
                if (e.getRootCause() != null) {
                    errMsg = e.getRootCause().getMessage();
                } else {
                    errMsg = e.getMessage();
                }
                if (errMsg != null && errMsg.contains("timed out")) {
                    logger.info("LDAP Retry is required: " + ne.toString());
                    return true;
                }
            }
        }
        logger.info("LDAP Retry not required: " + ne.toString());
        return false;
    }
}
