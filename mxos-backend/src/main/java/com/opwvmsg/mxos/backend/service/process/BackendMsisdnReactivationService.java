/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.process;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnReactivationService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Fiona Reactivation service exposed to client which is responsible for
 * reactivating the MSISDN via BACKEND API of MxOS.
 * 
 * @author mxos-dev
 */
public class BackendMsisdnReactivationService implements
        IMsisdnReactivationService {
    private static Logger logger = Logger
            .getLogger(BackendMsisdnReactivationService.class);

    /**
     * Default Constructor.
     * 
     * @throws Exception on any error.
     */
    public BackendMsisdnReactivationService() throws MxOSException {
        logger.info("BackendMsisdnReactivationService is created");
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "BackendMsisdnReactivationService process start"));
        }

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MsisdnReactivationService,
                    Operation.POST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MsisdnReactivationService,
                    Operation.POST, t0, StatStatus.pass);

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "BackendMsisdnReactivationService process end"));
            }
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MsisdnReactivationService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }
}
