/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave/mxos/service/MySQLMetaConnectionPool.java#2 $
 */

package com.opwvmsg.mxos.backend.external.ldap;

import java.util.Hashtable;
import java.util.Map.Entry;

import javax.naming.directory.DirContext;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.utils.paf.config.Config;

/**
 * Implementation of External LDAP connection pool.
 * 
 * @author mxos-dev
 */
public class ExternalLdapConnectionPool implements ICRUDPool<ExternalLdapObject> {

    private final String EXTERNAL_LDAP_HOST="externalLdapHost";
    private final String EXTERNAL_LDAP_PORT="externalLdapPort";
    private final String BIND_DN="externalLdapBindDn";
    private final String BIND_PASSWORD="externalLdapBindPassword";
    private final String BASE_DN="externalLdapBaseDn";
    private final String SEARCH_FILTER="externalLdapSearchFilter";

    private final String EXTERNAL_LDAP_BIND_TIMEOUT_IN_MS="extLdapBindTimeoutMS";
    private final String EXTERNAL_LDAP_CONNET_TIMEOUT_IN_MS="extLdapConnectTimeoutMS";
    private final String EXTERNAL_LDAP_SEARCH_TIMEOUT_IN_MS="extLdapSearchTimeoutMS";
    private final String EXTERNAL_LDAP_POOL_TIMEOUT_IN_MS="extLdapPoolTimeoutMS";

    private final String LDAP_POOL_INIT_SIZE="extLdapPoolInitSize";
    private final String LDAP_POOL_PREF_SIZE="extLdapPoolPrefSize";
    private final String LDAP_POOL_MAX_SIZE="extLdapPoolMaxSize";

    private final String ATTRIBUTE_LIST="externalLdapAttributesList";


    protected static Logger logger = Logger.getLogger(ExternalLdapConnectionPool.class);

    protected GenericObjectPool<ExternalLdapObject> objPool;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public ExternalLdapConnectionPool() throws MxOSException {
    }
    /**
     * Method to load Pool.
     */
    public void loadPool() {
        // TODO - Read all the LDAP info from config.db
        final Hashtable<String, String> env = getConfigParams();
        for (Entry<String, String> entry : env.entrySet()) {
            logger.debug("Key :" + entry.getKey() + ", Value:"
                    + entry.getValue());
        }
        String maxSize = env.get("com.sun.jndi.ldap.connect.pool.maxsize");
        if (maxSize == null || maxSize.equals("")) {
            maxSize = "10"; // Default pool size
        }
        objPool = new GenericObjectPool<ExternalLdapObject>(new ExternalLdapObjectFactory(
                env), Integer.parseInt(maxSize));
        initializeJMXStats(Long.parseLong(maxSize));
        objPool.setMaxIdle(-1);
        objPool.setTestOnBorrow(true);
    }

    @Override
    public void resetPool() throws MxOSException {
        if (null != objPool) {
            objPool.clear();
            logger.info("# Clearing the pools..");
        }
    }

    @Override
    public ExternalLdapObject borrowObject() throws MxOSException {
        try {
            final Hashtable<String, String> env = getConfigParams();
            for (Entry<String, String> entry : env.entrySet()) {
                logger.debug("Key :" + entry.getKey() + ", Value:"
                        + entry.getValue());
            }
            ExternalLdapObject obj = new ExternalLdapObject(env);

            incrementJMXStats();
            if (logger.isDebugEnabled()) {
                logger.debug("# One more object borrowed..");
            }
            return obj;
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("# Error while borrowing object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    @Override
    public void returnObject(ExternalLdapObject object)
            throws MxOSException {
        try {
        	object.close();
        	decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while return object object.", e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.LDAP.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_LDAP.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_LDAP.decrement();
    }

    protected Hashtable<String, String> getConfigParams() {
        Hashtable<String, String> env = new Hashtable<String, String>();

        String configHost = MxOSConfig.getConfigHost();
        String defaultApp = MxOSConfig.getDefaultApp();
        String url = getLdapUrl();
        String bindDn = MxOSConfig.getString(configHost, defaultApp,BIND_DN,MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_DN));

        String password = MxOSConfig.getString(configHost, defaultApp,BIND_PASSWORD,MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_PASSWORD));

        String searchFilter = MxOSConfig.getString(configHost, defaultApp,SEARCH_FILTER);
        String baseDN = MxOSConfig.getString(configHost, defaultApp,BASE_DN);
        String attributeList=MxOSConfig.getString(configHost, defaultApp,ATTRIBUTE_LIST);

        String initSize = MxOSConfig.getString(configHost, defaultApp,LDAP_POOL_INIT_SIZE,"1");
        String prefSize = MxOSConfig.getString(configHost, defaultApp,LDAP_POOL_PREF_SIZE,"5");        
        String maxSize = MxOSConfig.getString(configHost, defaultApp,LDAP_POOL_MAX_SIZE,"10");

        String maxSearchTimeOut=MxOSConfig.getString(configHost, defaultApp,EXTERNAL_LDAP_SEARCH_TIMEOUT_IN_MS,"10001");
        String bindTimeOut=MxOSConfig.getString(configHost, defaultApp,EXTERNAL_LDAP_BIND_TIMEOUT_IN_MS,"10001");
        String connectTimeout=MxOSConfig.getString(configHost, defaultApp,EXTERNAL_LDAP_CONNET_TIMEOUT_IN_MS,"10001");
        String poolTimeOut = MxOSConfig.getString(configHost, defaultApp,EXTERNAL_LDAP_POOL_TIMEOUT_IN_MS,"30001");

        ExternalLDAPUtils externalLdapUtils=ExternalLDAPUtils.getInstance();
        externalLdapUtils.setBaseDn(baseDN);
        externalLdapUtils.setBindDn(bindDn);
        externalLdapUtils.setSearchFilter(searchFilter);
        externalLdapUtils.setCURPassword(password);
        externalLdapUtils.setAttributList(attributeList);
        externalLdapUtils.setMaxSearchTimeout(maxSearchTimeOut);
        if (logger.isDebugEnabled()) {
            logger.debug("[url=" +url + ", extLdapPoolTimeoutMS=" + poolTimeOut
                    + "ms , extLdapSearchTimeoutMS=" + maxSearchTimeOut
                    + "ms , extLdapConnectTimeoutMS=" + connectTimeout + "ms , extLdapBindTimeoutMS=" + bindTimeOut
                    +"ms , LDAP_POOL_INIT_SIZE=" + initSize+" , LDAP_PREF_SIZE=" + prefSize
                    +" , LDAP_POOL_MAX_SIZE=" + maxSize+"]");
            logger.debug("ExternalLDAPUtils :"+externalLdapUtils.toString());
        }
        env.put(DirContext.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");
        env.put(DirContext.PROVIDER_URL, url);
        env.put(DirContext.SECURITY_AUTHENTICATION, "simple");
        env.put(DirContext.SECURITY_PRINCIPAL, bindDn);
        env.put(DirContext.SECURITY_CREDENTIALS, password);
        System.setProperty("com.sun.jndi.ldap.connect.pool", "false");
        env.put("com.sun.jndi.ldap.connect.pool.initsize", initSize);
        env.put("com.sun.jndi.ldap.connect.pool.prefsize", prefSize);
        env.put("com.sun.jndi.ldap.connect.pool.maxsize", maxSize);
        env.put("com.sun.jndi.ldap.connect.pool.timeout", poolTimeOut);
        env.put("com.sun.jndi.ldap.connect.timeout",connectTimeout);
        env.put("com.sun.jndi.ldap.read.timeout",bindTimeOut);
        return env;
    }

    protected String getLdapUrl() {
        if (logger.isDebugEnabled()) {
            logger.debug("getLdapUrl...");
        }
        String defaultApp = MxOSConfig.getDefaultApp();
        String ldapUrl = "ldap://";
        String host = Config.getInstance().get(MxOSConfig.getConfigHost(),defaultApp,EXTERNAL_LDAP_HOST ,MxOSConfig.getConfigHost());
        String ldapPort=Config.getInstance().get(MxOSConfig.getConfigHost(),defaultApp,EXTERNAL_LDAP_PORT ,"389");
        ldapUrl=ldapUrl+host+":"+ldapPort;
        if (logger.isDebugEnabled()) {
            logger.debug("Ldap URL for External Ldap Server: "+ldapUrl);
        }
        return ldapUrl;
    }

}
