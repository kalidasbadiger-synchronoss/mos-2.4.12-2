/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionFactory;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSRollbackAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to create mailbox in OX
 * 
 * @author mxos-dev
 * 
 */
public class CreateMailboxInOX extends AbstractMailbox implements
        MxOSRollbackAction {
    private static Logger logger = Logger.getLogger(CreateMailboxInOX.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInOX action start.");
        }
        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // check whether skip mailbox creation in OX
        boolean create = true;
        if (requestState.getInputParams().containsKey(SKIP_OX)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_OX).get(0));
            if (skip) {
                create = false;
                logger.info("Skip mailbox [" + email + "] creation in OX");
            }
        }

        // Create in OX if backend is set to OX
        boolean appSuiteIntegrated = Boolean.parseBoolean(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        boolean oxCreateContextEnabled = Boolean.parseBoolean(System
                .getProperty(SystemProperty.oxCreateContextEnabled.name()));
        if (appSuiteIntegrated && oxCreateContextEnabled && create) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                // check mailbox creation precondition
                try {
                    checkMailboxCreation(requestState);
                } catch (MxOSException e) {
                    logger.error("Mailbox must be created in LDAP first.", e);
                    throw e;
                }

                ICRUDPool<IRMIMailboxCRUD> rmiMailboxCRUDPool = null;
                IRMIMailboxCRUD rmiMailboxCRUD = null;
                try {
                    rmiMailboxCRUDPool = MxOSApp.getInstance()
                            .getRMIMailboxCRUD();
                    rmiMailboxCRUD = rmiMailboxCRUDPool.borrowObject();
                    rmiMailboxCRUD.createMailbox(requestState);
                    logger.info("Create mailbox [" + email
                            + "] in OX successfully.");
                } catch (final MxOSException e) {
                    logger.error("Error while creating mailbox [" + email
                            + "] in OX.", e);
                    throw e;
                } catch (Exception e) {
                    logger.error("Error while creating mailbox [" + email
                            + "] in OX.", e);
                    throw new ApplicationException(
                            MailboxError.MBX_UNABLE_TO_CREATE.name(), e);
                } finally {
                    CRUDUtils.releaseConnection(rmiMailboxCRUDPool,
                            rmiMailboxCRUD);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("CreateMailboxInOX action end.");
        }
    }

    public void rollback(MxOSRequestState requestState) throws MxOSException {
        ActionFactory.getInstance().exec(DeleteMailboxFromOX.class.getName(),
                requestState);
    }

}
