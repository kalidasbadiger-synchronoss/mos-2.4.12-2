/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.addressbook.pojos.ExternalMember;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsExternalMembersService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Groups External Members service exposed to client which is responsible 
 * for doing basic activities e.g create, read, update, delete
 * 
 * @author mxos-dev
 */
public class BackendGroupsExternalMembersService implements
 IGroupsExternalMembersService {

    private static Logger logger = Logger
            .getLogger(BackendGroupsExternalMembersService.class);

    /**
     * Default Constructor.
     */
    public BackendGroupsExternalMembersService() {
        logger.info("BackendGroupsExternalMembersService Service created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsExternalMemberService, Operation.PUT);

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.PUT, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
        return 0;
    }

    @Override
    public void delete(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsExternalMemberService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsExternalMemberService,
                    Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService,
                    Operation.DELETEALL, t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService,
                    Operation.DELETEALL, t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public ExternalMember read(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsExternalMemberService, Operation.GET);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.GET, t0,
                    StatStatus.pass);

            return ((ExternalMember) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.externalMembers));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public List<ExternalMember> readAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsExternalMemberService,
                    Operation.GETALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.GETALL,
                    t0, StatStatus.pass);

            return ((List<ExternalMember>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allExternalMembers));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsExternalMemberService, Operation.GETALL,
                    t0, StatStatus.fail);
            throw e;
        }
    }

}
