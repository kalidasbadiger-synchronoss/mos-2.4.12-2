/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.addressbook.IGroupsService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Groups service exposed to client which is responsible for doing basic name
 * related activities e.g read name, update name, etc. directly in the database.
 * 
 * @author mxos-dev
 */
public class BackendGroupsService implements IGroupsService {

    private static Logger logger = Logger.getLogger(BackendGroupsService.class);

    /**
     * Default Constructor.
     */
    public BackendGroupsService() {
        logger.info("BackendGroupsService Service created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsService, Operation.PUT);

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.PUT, t0,
                    StatStatus.pass);
            return Long.parseLong(mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.groupId));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsService, Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.DELETE, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.DELETE, t0,
                    StatStatus.fail);
            throw e;
        }

    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsService, Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.DELETEALL, t0,
                    StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.DELETEALL, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsService, Operation.POST);
            String input = inputParams.get(
                    AddressBookProperty.groupsList.name()).get(0);

            // convert input param to multiple requests
            String[] requests = input.split(MxOSConstants.LINE_REGEXP);

            // check if number of requests is not 0
            if (requests != null && requests.length == 0) {
                // throw exception
                throw new MxOSException(
                        AddressBookError.ABS_INVALID_MULTIPLE_GROUPS.name());
            }
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.FIELD_REGEXP);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUAL_REGEXP);
                    if (!inputParams.containsKey(paramKV[0])) {
                        inputParams.put(paramKV[0], new ArrayList<String>(
                                requests.length));
                    }
                }
            }

            // parse each line and extract the input params per request
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.FIELD_REGEXP);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUAL_REGEXP);
                    for (String inputParam : inputParams.keySet()) {
                        if (inputParam.equals(paramKV[0]) && paramKV.length > 1) {
                            // add new value at current index
                            inputParams.get(inputParam).add(paramKV[1]);
                        } else {
                            if (inputParams.get(inputParam).size() < i
                                    && !inputParam
                                            .equals(AddressBookProperty.userId
                                                    .name())
                                    && !inputParams.get(inputParam).isEmpty()) {
                                inputParams.get(inputParam).add(null);
                            }
                        }
                    }
                }
            }

            ActionUtils.setAddressBookBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.POST, t0,
                    StatStatus.pass);
            return (List<Long>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.groupIdList);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void moveMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.GroupsService, Operation.MOVE);
            String input = inputParams.get(
                    AddressBookProperty.groupsList.name()).get(0);

            // convert input param to multiple requests
            String[] requests = input.split(MxOSConstants.LINE_REGEXP);

            // check if number of requests is not 0
            if (requests != null && requests.length == 0) {
                // throw exception
                throw new MxOSException(
                        AddressBookError.ABS_INVALID_MULTIPLE_MOVE_GROUPS
                                .name());
            }
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.FIELD_REGEXP);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUAL_REGEXP);
                    if (inputParams.get(paramKV[0]) == null) {
                        inputParams.put(paramKV[0], new ArrayList<String>(
                                requests.length));
                    }
                    inputParams.get(paramKV[0]).add(paramKV[1]);
                }
            }

            ActionUtils.setAddressBookBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.GroupsService, Operation.MOVE, t0,
                    StatStatus.pass);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.GroupsService, Operation.MOVE, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
