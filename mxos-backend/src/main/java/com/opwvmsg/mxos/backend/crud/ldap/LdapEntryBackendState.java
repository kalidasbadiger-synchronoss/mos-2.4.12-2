package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.directory.Attributes;

import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;

/**
 * This is a collection of tuples of LDAP (dn, attributes).
 * The class is useful when you pass LDAP entries among actions. 
 */
public class LdapEntryBackendState implements IBackendState {
    
    // used for directing operation in later action
    public enum Tag {
        // about suggesting operation
        OP_MODIFY_ATTRIBUTES,
        OP_ADD_ENTRY,
        // about type of the entry
        ENTRY_USER,
        ENTRY_SENDERS_CONTROL
    }
    
    public static class Entry {
        public final String dn;
        public final Attributes attributes;
        public final Set<Tag> tags; // for any additional information
        
        public Entry(String dn, Attributes attributes) {
            this.dn = dn;
            this.attributes = attributes;
            tags = new HashSet<Tag>();
        }
        
        public Entry(String dn, Attributes attributes, Tag option) {
            this.dn = dn;
            this.attributes = attributes;
            tags = new HashSet<Tag>(1);
            tags.add(option);
        }
        
        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder("dn=").append(dn)
                    .append(", attributes=").append(attributes)
                    .append(", tags=").append(tags);
            return sb.toString();
        }
    }
    
    public static String getType() {
        return "ldapentries";
    }
    
    private final List<Entry> entries;
    
    public LdapEntryBackendState() {
        entries = new ArrayList<Entry>();
    }
    
    public Entry getEntry(int index) {
        return entries.get(index);
    }
    
    public List<Entry> getEntries() {
        return entries;
    }

    public void addEntry(Entry e) {
        entries.add(e);
    }
    
    public void addEntry(String dn, Attributes attrs) {
        entries.add(new Entry(dn, attrs));
    }
    
    public void addEntry(String dn, Attributes attrs, Tag tag) {
        entries.add(new Entry(dn, attrs, tag));
    }

    // static versions of addEntry's 
    public static void addEntry(MxOSRequestState mxosRequestState, Entry e) {
        LdapEntryBackendState state = getState(mxosRequestState);
        state.addEntry(e);
    }
    
    public static void addEntry(MxOSRequestState mxosRequestState,
            String dn, Attributes attrs) {
        LdapEntryBackendState state = getState(mxosRequestState);
        state.addEntry(dn, attrs);
    }
    
    public static void addEntry(MxOSRequestState mxosRequestState,
            String dn, Attributes attrs, Tag tag) {
        LdapEntryBackendState state = getState(mxosRequestState);
        state.addEntry(dn, attrs, tag);
    }
    
    public void clearEntries() {
        entries.clear();
    }
    
    public static LdapEntryBackendState getState(MxOSRequestState mxosRequestState) {
        LdapEntryBackendState state =
                (LdapEntryBackendState) mxosRequestState.getBackendState().get(getType());
        if (state == null) {
            state = new LdapEntryBackendState();
            mxosRequestState.getBackendState().put(getType(), state);
        }
        return state;
    }
    
    @Override
    public int getSize() {
        return entries.size();
    }    
}
