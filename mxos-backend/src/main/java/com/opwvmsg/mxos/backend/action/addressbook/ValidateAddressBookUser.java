/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to validate userId.
 * 
 * @author mxos-dev
 * 
 */
public class ValidateAddressBookUser implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ValidateAddressBookUser.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (!MxOSApp.getInstance().enforceValidation()) {
            logger.info("Use Validation is skipped, as trustedClient is true");
            return;
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Validate user action start."));
        }
        // folderId only applies for OX backend
        if (MxOSConfig.getAddressBookBackend().equals(
                AddressBookDBTypes.ox.name())) {

            ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
            IAddressBookCRUD addressBookCRUD = null;

            try {
                addressBookCRUDPool = MxOSApp.getInstance()
                        .getAddressBookCRUD();
                addressBookCRUD = addressBookCRUDPool.borrowObject();

                if (mxosRequestState.getInputParams().get(
                        AddressBookProperty.userId.name()) != null) {

                    String name = mxosRequestState.getInputParams()
                            .get(AddressBookProperty.userId.name()).get(0);

                    MxOSApp.getInstance()
                            .getAddressBookHelper()
                            .setAttribute(mxosRequestState,
                                    AddressBookProperty.userId, name);
                }

                addressBookCRUD.validateUser(mxosRequestState);
            } catch (AddressBookException e) {
                ExceptionUtils.createMxOSExceptionFromAddressBookException(
                        AddressBookError.ABS_INVALID_SESSION, e);
            } catch (final Exception e) {
                logger.error("Error while validate user.", e);
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                try {
                    if (addressBookCRUDPool != null && addressBookCRUD != null) {
                        addressBookCRUDPool.returnObject(addressBookCRUD);
                    }
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        // if backend is WebEdge, get mailboxId
        if (MxOSConfig.getAddressBookBackend().equals(
                AddressBookDBTypes.we.name())) {
            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;

            try {
                mailboxCRUDPool = MxOSApp.getInstance()
                        .getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();

                // this requires MailboxProperty.email, which is same as
                // AddressBookProperty.userId, so we give the MSSLinkInfo method
                // userId and then swap email back to original value.
                String userId = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.userId.name()).get(0);

                String email = null;
                if (mxosRequestState.getInputParams().get(
                        AddressBookProperty.email.name()) != null) {
                    email = mxosRequestState.getInputParams()
                            .get(AddressBookProperty.email.name()).get(0);
                }

                mxosRequestState.getInputParams().put(
                        MailboxProperty.email.name(), new ArrayList<String>());
                mxosRequestState.getInputParams()
                        .get(MailboxProperty.email.name()).add(userId);
                ActionUtils.readMSSLinkInfo(mxosRequestState, mailboxCRUD);
                if (email != null) {
                    mxosRequestState.getInputParams()
                            .get(MailboxProperty.email.name()).clear();
                    mxosRequestState.getInputParams()
                            .get(MailboxProperty.email.name()).add(email);
                } else {
                    mxosRequestState.getInputParams().remove(
                            MailboxProperty.email.name());
                }

            } catch (final MxOSException e) {
                logger.error("Error while get mss link info.", e);
                throw e;
            } catch (final Exception e) {
                logger.error("Error while get mss link info.", e);
                throw new ApplicationException(
                        MailboxError.MBX_UNABLE_TO_MSSLINKINFO_GET.name(), e);
            } finally {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    try {
                        mailboxCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(),
                                "Error in finally clause while returing "
                                        + "Provisioning CRUD pool:"
                                        + e.getMessage());
                    }
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Validate user action end."));
        }
    }
}
