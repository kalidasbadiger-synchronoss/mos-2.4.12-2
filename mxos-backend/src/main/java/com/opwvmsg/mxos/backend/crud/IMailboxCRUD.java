/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.data.pojos.Domain;
import com.opwvmsg.mxos.data.pojos.AdminControl;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.data.pojos.GroupAdminAllocations;
import com.opwvmsg.mxos.data.pojos.InternalInfo;
import com.opwvmsg.mxos.data.pojos.MailAccess;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.data.pojos.MailSend;
import com.opwvmsg.mxos.data.pojos.MailStore;
import com.opwvmsg.mxos.data.pojos.Mailbox;
import com.opwvmsg.mxos.data.pojos.MessageEventRecords;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.data.pojos.SmsServices;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.data.pojos.WebMailFeatures;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to Domain and Mailbox Provisioning CRUD.
 * 
 * @author mxos-dev
 */
public interface IMailboxCRUD extends ITransaction {
    /**
     * Create Domain.
     *
     * @param mxosRequestState - domainData to be created.
     * @throws MxOSException - on domain not created or any other error.
     */
    void createDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Read Domain.
     *
     * @param domainName - name of the domain to be read.
     * @return Domain - on successful read.
     * @throws MxOSException - on domain not found or any other error.
     */
    Domain readDomain(final String domainName,
            Map<String, List<String>> inputParams) throws MxOSException;

    /**
     * Update Domain.
     *
     * @param mxosRequestState - domain data to be updated.
     * @throws MxOSException - on domain not found or any other error.
     */
    void updateDomain(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Delete Domain.
     *
     * @param domainName - name of the domain to be deleted.
     * @throws MxOSException - on domain not found or any other error.
     */
    void deleteDomain(final String domainName) throws MxOSException;

    /**
     * Search Domains.
     *
     * @param mxosRequestState - search query.
     * @param searchMaxRows - max rows to be returned.
     * @param searchMaxTimeout - max time to wait.
     * @return List - of Domain objects.
     * @throws MxOSException on domain not found or any other error.
     */
    List<Domain> searchDomain(final MxOSRequestState mxosRequestState,
            final int searchMaxRows, final int searchMaxTimeout)
            throws MxOSException;

    /**
     * Create Cos.
     *
     * @param requestState - Cos data to be created.
     * @throws MxOSException - on mailbox not created or any other error.
     */
    void createCos(final MxOSRequestState requestState)
            throws MxOSException;

    /**
     * Read Cos Base.
     *
     * @param cosId cosId.
     * @return Base - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    Base readCosBase(final String cosId, MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Update Cos.
     *
     * @param mailboxPrimaryData - Cos data to be updated.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    void updateCos(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Delete Cos.
     *
     * @param cosId cosId.
     * @throws MxOSException on mailbox not found or any other error.
     */
    void deleteCos(final String cosId) throws MxOSException;

    /**
     * Search Cos objects.
     *
     * @param mxosRequestState - input request state.
     * @param maxSearchRows - max rows to be returned.
     * @param maxSearchTimeout - max time to wait.
     * @return List of Cos Objects.
     * @throws MxOSException on mailbox(s) not found or any other error.
     */
    List<Base> searchCos(final MxOSRequestState mxosRequestState,
            final int maxSearchRows, final int maxSearchTimeout)
            throws MxOSException;

    /**
     * Read Credentials for a COS.
     *
     * @param cosId - COS ID.
     * @return Credentials - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    Credentials readCosCredentials(String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException;
    
    /**
     * Read Cos General Preferences.
     * 
     * @param cosId - COS ID.
     * @return GeneralPreferences - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    GeneralPreferences readCosGeneralPreferences(String cosId, MxOSRequestState mxosRequestState)
               throws MxOSException;
  
    /**
     * Create Mailbox.
     *
     * @param requestState - Mailbox data to be created.
     * @throws MxOSException - on mailbox not created or any other error.
     */
    void createMailbox(final MxOSRequestState requestState)
            throws MxOSException;

    /**
     * Read Family Mailbox Dn.
     *
     * @param requestState mxos request state.
     * @return String - if group mailbox returns dn else return null.
     * @throws MxOSException - Any error.
     */
    String readMailboxDn(final MxOSRequestState requestState) throws MxOSException;

    /**
     * Read Family Mailbox Dn.
     *
     * @param email - email address.
     * @return String - if family mailbox returns admin realm dn else return null.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    String readAdminRealmDn(final String email,
            Map<String, List<String>> inputParams) throws MxOSException;

    /**
     * Read Mailbox.
     *
     * @param mxosRequestState - request state used for passing options
     * @param uid - can be email or mailboxId or other uid for mailbox.
     * @return Mailbox - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    Mailbox readMailbox(MxOSRequestState mxosRequestState, final String uid) throws MxOSException;

    /**
     * Read Multiple Mailboxes.
     *
     * @param mxosRequestState - request state used for passing options
     * @param email - list of email addresses.
     * @return List<Mailbox> - on successful get.
     * @throws MxOSException - on any error.
     */
    List<Mailbox> readMultipleMailboxes(MxOSRequestState requestState, final List<String> list)
            throws MxOSException;

    /**
     * Read Mailbox Base.
     *
     * @param mxosRequestState - request state used for passing options
     * @param email - email address.
     * @return Base - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    Base readMailboxBase(MxOSRequestState mxosRequestState, final String email)
            throws MxOSException;

    /**
     * Update Mailbox.
     *
     * @param mailboxPrimaryData - Mailbox data to be updated.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    void updateMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Delete Mailbox.
     *
     * @param email email address.
     * @throws MxOSException on mailbox not found or any other error.
     */
    void deleteMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Search Mailbox objects.
     *
     * @param mxosRequestState - input request state.
     * @param maxSearchRows - max rows to be returned.
     * @param maxSearchTimeout - max time to wait.
     * @return List of Mailbox Objects.
     * @throws MxOSException on mailbox(s) not found or any other error.
     */
    List<Base> searchMailbox(final MxOSRequestState mxosRequestState,
            final int maxSearchRows, final int maxSearchTimeout)
            throws MxOSException;

    /**
     * Read WebMailFeatures.
     *
     * @param email - email address.
     * @return WebMailFeatures - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    WebMailFeatures readWebMailFeatures(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read WebMailFeatures for a COS.
     *
     * @param cosId - COS ID.
     * @return WebMailFeatures - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    WebMailFeatures readCosWebMailFeatures(final String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Authenticate Mailbox.
     *
     * @param email - email address of mailbox.
     * @param password - input password of mailbox.
     * @return true - if given mailbox is authenticated.
     * @throws MxOSException - on mailbox not found or any other error.
     */
     boolean authenticate(final String email, final String password) throws MxOSException;

     /**
      * Read Credentials.
      *
      * @param email - email address.
      * @return Credentials - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    Credentials readCredentials(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;
    
     /**
      * Read GeneralPreferences.
      *
      * @param email - email address.
      * @return GeneralPreferences - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    GeneralPreferences readGeneralPreferences(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read MailSend.
      *
      * @param email - email address.
      * @return MailSend - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    MailSend readMailSend(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read MailReceipt.
      *
      * @param mxosRequestState - request state used for passing options
      * @param email - email address.
      * @param includeAdminControl - request includes a property requests=adminControl
      * @param includeSenderBlocking - request includes a property requests=SenderBlocking
      * @return MailReceipt - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     MailReceipt readMailReceipt(final MxOSRequestState mxosRequestState, final String email) throws MxOSException;

     /**
      * Read ForwardingAddresses.
      *
      * @param email - email address.
      * @return List - on successful mailbox forwardingAddresses get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    List<String> readForwardingAddresses(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read AddressesForLocalDelivery.
      *
      * @param email - email address.
      * @return List - on successful mailbox AddressesForLocalDelivery get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    List<String> readAddressesForLocalDelivery(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read AllowedSendersList.
      *
      * @param email - email address.
      * @return List - on successful mailbox AllowedSendersList get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     List<String> readAllowedSendersList(MxOSRequestState requestState, final String email) throws MxOSException;

     /**
      * Read BlockedSendersList.
      *
      * @param mxosRequestState - request state used for passing options
      * @param email - email address.
      * @return List - on successful mailbox BlockedSendersList get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     List<String> readBlockedSendersList(final MxOSRequestState mxosRequestState, final String email) throws MxOSException;

     /**
      * Read Admin blocked senders list.
      * 
      * @param email - email for which details have to be read
      * @return List<String> - list of admin blocked addresses.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    List<String> readAdminBlockedSendersList(final String email,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read Admin approved senders list.
      * 
      * @param email - email for which details have to be read
      * @return List<String> - list of admin approved addresses.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     List<String> readAdminApprovedSendersList(final String email,
             MxOSRequestState mxosRequestState) throws MxOSException;
     /**
      * Read Admin blocked senders list.
      * 
      * @param dn - distinguished name for which admin blocked senders details
      *            have to be fetched.
      * @return List<String> - list of admin blocked addresses.
      * @throws MxOSException - on mailbox not found or any other error.
      */

     /**
      * Read SieveFiltersBlockedSendersList.
      *
      * @param email - email address.
      * @return List - on successful mailbox SieveFiltersBlockedSendersList get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     List<String> readSieveFiltersBlockedSendersList(final String email,
             MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read MailAccess.
      *
      * @param email - email address.
      * @return MailAccess - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     MailAccess readMailAccess(final String email, MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read MailStore.
      *
      * @param email - email address.
      * @return MailStore - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     MailStore readMailStore(final String email, MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read InternalInfo.
      *
      * @param email - email address.
      * @param mxosRequestState - input request state.
      * @return InternalInfo - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     InternalInfo readInternalInfo(final String email, MxOSRequestState requestState) throws MxOSException;

     /**
      * Read MessageEventRecords.
      *
      * @param email - email address.
      * @return MessageEventRecords - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    MessageEventRecords readMessageEventRecords(final String email,
            MxOSRequestState requestState) throws MxOSException;

     /**
      * Read InternalInfo for a COS.
      *
      * @param cosId - COS ID.
      * @return InternalInfo - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    InternalInfo readCosInternalInfo(final String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException;

     /**
      * Read MessageEventRecords for a COS.
      *
      * @param cosId - COS ID.
      * @return MessageEventRecords - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
    MessageEventRecords readCosMessageEventRecords(final String cosId,
            MxOSRequestState mxosRequestState) throws MxOSException;


     /**
      * Read MSSLinkInfo.
      *
      * @param email - email address.
      * @return MSSLinkInfo - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     MssLinkInfo readMSSLinkInfo(final String email, Map<String, List<String>> inputParams) throws MxOSException;

     /**
      * Read MailboxRealm.
      *
      * @param email - email address.
      * @return String - on successful return realm.
      * @throws MxOSException - on mailbox not found or any other error.
      */
     String readMailboxRealm(final String email, Map<String, List<String>> inputParams) throws MxOSException;
     
    /**
     * Read SmsServices.
     *
     * @param email - email address.
     * @return SmsServices - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
     SmsServices readSmsServices(final String email, MxOSRequestState mxosRequestState) throws MxOSException;
     
     /**
      * Read SocialNetworks.
      *
      * @param email - email address.
      * @return SocialNetwork - on successful mailbox Profile get.
      * @throws MxOSException - on mailbox not found or any other error.
      */
      SocialNetworks readSocialNetworks(final String email, MxOSRequestState mxosRequestState) throws MxOSException;
      
      /**
       * Read ExternalAccounts.
       *
       * @param email - email address.
       * @return ExternalAccounts - on successful mailbox Profile get.
       * @throws MxOSException - on mailbox not found or any other error.
       */
      ExternalAccounts readExternalAccounts(final String email, MxOSRequestState mxosRequestState) throws MxOSException;
      
      /**
       * Read Cos ExternalAccounts.
       *
       * @param email - email address.
       * @return ExternalAccounts - on successful mailbox Profile get.
       * @throws MxOSException - on mailbox not found or any other error.
       */
      ExternalAccounts readCosExternalAccounts(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read MailAccess from COS.
     * 
     * @param cosId - cosId.
     * @return MailAccess - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    MailAccess readCosMailAccess(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read SmsServices Object from COS.
     * 
     * @param cosId - COS Id.
     * @return SmsServices - on successful COS get.
     * @throws MxOSException - on COS not found or any other error.
     */
    SmsServices readCosSmsServices(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read Cos SocialNetworks.
     *
     * @param cosId - CosId.
     * @return SocialNetwork - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    SocialNetworks readCosSocialNetworks(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read Cos MailStore.
     * 
     * @param cosId - cosId.
     * @return MailStore- on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    MailStore readCosMailStore(String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read CosMailReceipt.
     * 
     * @param cosId - cosId
     * @return MailReceipt - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    MailReceipt readCosMailReceipt(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Read CosMailSend.
     * 
     * @param cosId - cosId
     * @return MailSend - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    MailSend readCosMailSend(final String cosId, MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Search for a subscriber's Credentials by an email address or a user name.
     * 
     * @param mxosRequestState - input request state.
     * @param maxSearchRows - max rows to be returned.
     * @param maxSearchTimeout - max time to wait.
     * @return List of Credentials Objects.
     * @throws MxOSException on Credentials' not found or any other error.
     */
    Credentials searchCredentials(final MxOSRequestState mxosRequestState)
            throws MxOSException;
    
    /**
     * Read raw Credentials for Authentication purposes.
     *
     * @param email - email address.
     * @return Credentials - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    Credentials readRawCredentials(final String email, MxOSRequestState mxosRequestState) throws MxOSException;
    
    /**
     * Read AdminControl for family mailbox.
     *
     * @param email - email address.
     * @return AdminControl - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    AdminControl readAdminControl(final String email, MxOSRequestState mxosRequestState) throws MxOSException;
    
    /**
     * Read GroupAdminAllocations for family mailbox.
     * 
     * @param email - email address.
     * @return GroupAdminAllocations - on successful mailbox Profile get.
     * @throws MxOSException - on mailbox not found or any other error.
     */
    GroupAdminAllocations readGroupAdminAllocations(final String email, MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Make a modify entry into the operation context.
     * 
     * @param requestState The operation context. Data is available at
     *            requestState.backendState
     * @param propertyForRemovingValue Property name in requestState.inputParams
     *            that specifies removing value.
     * @param propertyForAddingValue Property name in requestState.inputParams
     *            that specifies adding value. Specify null if there is no value
     *            to add.
     * @param propertyToModify Attribute type to modify.
     * @param notFoundError Error used when the modify target was not round.
     * 
     * @throw MxOSException If the context does not have the value to remove/replace.
     */
    void makeModifyEntryInContext(MxOSRequestState requestState,
            MailboxProperty propertyForRemovingValue,
            MailboxProperty propertyForAddingValue,
            MailboxProperty propertyToModify,
            MailboxError notFoundError) throws MxOSException;

    /**
     * Add a senders control list item.
     * 
     * @param requestState The operation context. Data is available at
     *            requestState.backendState.
     * @param itemProperty Specifies property for adding item.
     * @param listProperty Specifies senders control list.
     * @param maxItems Number of maximum entries
     * @param alreadyExistsError Error used when the specified item already exists
     * @param sizeExceedsError Error used when number of items would exceed maximum
     * 
     * @throws MxOSException When specified item(s) already exists,
     *             when number of items reaches maximum.
     */
    void addSendersControlListItem(MxOSRequestState requestState,
            MailboxProperty itemProperty,
            MailboxProperty listProperty,
            int maxItems,
            MailboxError alreadyExistsError,
            MailboxError sizeExceedsError) throws MxOSException;
}
