/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved. The copyright
 * to the computer software herein is the property of Openwave Systems Inc. The
 * software may be used and/or copied only with the written permission of
 * Openwave Systems Inc. or in accordance with the terms and conditions
 * stipulated in the agreement/contract under which the software has been
 * supplied. $Id:
 * //mxos/Mxos_2_Mainline/mxos/mxos-dao/src/main/java/com/openwave
 * /mxos/service/IBlobCRUD.java#1 $
 */

package com.opwvmsg.mxos.backend.crud;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to access blobdata.
 * 
 * @author Jaydeep
 */
public interface IBlobCRUD extends ITransaction {

    /**
     * Method to create blob.
     * 
     * @param messagePrimaryData Mandatory parameters for message blob create
     *            operation. e.g. messageId etc.
     * @param messageOptionalData Optional parameters for message blob create
     *            operation.
     */
    void createBlob(final MxOSRequestState mxosRequestState) throws MxOSException;

    /**
     * Method to read blob.
     * 
     * @param messagePrimaryData Mandatory parameters for message blob read
     *            operation. e.g. messageId etc.
     * @param compressionType Compression algorithm type, if not compressed then
     *            pass "NotApplicable".
     * @param message Message POJO will be filled in with blob.
     */
    void readBlob(final MxOSRequestState mxosRequestState, final Body body)
            throws MxOSException;

    /**
     * Method to delete blob.
     * 
     * @param messagePrimaryData Mandatory parameters for message blob delete
     *            operation. e.g. messageId etc.
     */
    void deleteBlob(final MxOSRequestState mxosRequestState, final String messageId)
            throws MxOSException;
}
