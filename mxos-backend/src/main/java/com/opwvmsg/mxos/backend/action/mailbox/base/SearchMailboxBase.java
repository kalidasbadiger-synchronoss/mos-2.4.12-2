/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Action class for Search Mailbox with wildcard characters.
 *
 * @author mxos-dev
 */
public class SearchMailboxBase implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SearchMailboxBase.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        final long t0 = Stats.startTimer();
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SearchMailboxBase action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        int maxRows = MxOSApp.getInstance().getSearchMaxRows();
        if (requestState.getInputParams().containsKey(
                MailboxProperty.maxRows.name())) {
            maxRows = Integer.parseInt(requestState.getInputParams()
                    .get(MailboxProperty.maxRows.name()).get(0));
        }
        int maxTimeOut = MxOSApp.getInstance().getSearchMaxTimeOut();
        try {
            mailboxCRUDPool = MxOSApp.getInstance()
                    .getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final List<Base> bases = mailboxCRUD.searchMailbox(
                    requestState, maxRows, maxTimeOut);

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.bases, bases);

        } catch (final MxOSException e) {
            Stats.stopTimer(ServiceEnum.ActionSearchMailboxBaseTime, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        } catch (final Exception e) {
            logger.error("Error while search mailbox base.", e);
            Stats.stopTimer(ServiceEnum.ActionSearchMailboxBaseTime, Operation.GET, t0,
                    StatStatus.fail);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SEARCH.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SearchMailboxBase action end."));
        }
        Stats.stopTimer(ServiceEnum.ActionSearchMailboxBaseTime, Operation.GET, t0,
                StatStatus.pass);
    }
}