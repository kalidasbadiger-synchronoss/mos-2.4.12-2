/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service.tasks;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.tasks.ITasksService;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Tasks service exposed to client which is responsible for doing basic
 * task related activities e.g create task, delete task, etc.
 * 
 * @author mxos-dev
 */
public class BackendTasksService implements ITasksService {

    private static Logger logger = Logger.getLogger(BackendTasksService.class);

    private static final String LINE_REGEXP = "\\r?\\n";

    /**
     * Default Constructor.
     */
    public BackendTasksService() {
        logger.info("BackendTasksService created...");
    }

    @Override
    public long create(final Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService, Operation.PUT);

            ActionUtils.setBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksService, Operation.PUT, t0,
                    StatStatus.pass);
            return Long.parseLong(mxosRequestState.getDbPojoMap().getProperty(
                    MxOSPOJOs.taskId));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.PUT, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Long> createMultiple(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService, Operation.POST);

            String input = inputParams.get(TasksProperty.tasksList.name()).get(
                    0);

            // convert input param to multiple requests
            String[] requests = input.split(LINE_REGEXP);

            // check if number of requests is not 0
            if (requests != null && requests.length == 0) {
                // throw exception
                throw new MxOSException(
                        TasksError.TSK_INVALID_MULTIPLE_TASKS.name());
            }
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.SEPARATOR);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUALS);
                    if (!inputParams.containsKey(paramKV[0])) {
                        inputParams.put(paramKV[0], new ArrayList<String>(
                                requests.length));
                    }
                }
            }

            // parse each line and extract the input params per request
            for (int i = 0; i < requests.length; i++) {
                String[] params = requests[i].split(MxOSConstants.SEPARATOR);

                for (String param : params) {
                    String[] paramKV = param.split(MxOSConstants.EQUALS);
                    for (String inputParam : inputParams.keySet()) {
                        if (inputParam.equals(paramKV[0]) && paramKV.length > 1) {
                            if (!inputParam.equals(TasksProperty.userId.name())) {
                                // pad with nulls
                                for (int j = inputParams.get(inputParam).size(); j < i; j++) {
                                    inputParams.get(inputParam).add(null);
                                }
                                // add new value at current index
                                inputParams.get(inputParam).add(paramKV[1]);
                            }
                        }
                    }
                }
            }

            for (String inputParam : inputParams.keySet()) {
                if (!inputParam.equals(TasksProperty.userId.name())) {
                    if (inputParams.get(inputParam).size() < requests.length) {
                        for (int i = inputParams.get(inputParam).size(); i < requests.length; i++) {
                            inputParams.get(inputParam).add(null);
                        }
                    }
                }
            }

            ActionUtils.setTasksBackendState(mxosRequestState);

            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksService, Operation.POST, t0,
                    StatStatus.pass);
            return (List<Long>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.taskIdList);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void delete(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService,
                    Operation.DELETE);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksService, Operation.DELETE,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.DELETE,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void deleteAll(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();
        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService,
                    Operation.DELETEALL);
            ActionUtils.setBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);
            Stats.stopTimer(ServiceEnum.TasksService, Operation.DELETEALL,
                    t0, StatStatus.pass);
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.DELETEALL,
                    t0, StatStatus.fail);
            throw e;
        }
    }

    @Override
    public Task read(Map<String, List<String>> inputParams)
            throws MxOSException {
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService, Operation.GET);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.TasksService, Operation.GET, t0,
                    StatStatus.pass);

            return ((Task) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.task));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.GET, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Task> list(Map<String, List<String>> inputParams)
            throws MxOSException {

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.TasksService, Operation.LIST);
            ActionUtils.setTasksBackendState(mxosRequestState);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.TasksService, Operation.LIST, t0,
                    StatStatus.pass);

            return ((List<Task>) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.allTasks));
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.TasksService, Operation.LIST, t0,
                    StatStatus.fail);
            throw e;
        }
    }

    @Override
    public void confirm(Map<String, List<String>> inputParams)
            throws MxOSException {
        // TODO Auto-generated method stub
        
    }
}
