/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.folder;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.mailbox.MailboxMetaServiceHelper;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Folder;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.FolderError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to ListFolders.
 * 
 * @author mxos-dev
 */
public class ListFolders implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ListFolders.class);

    /**
     * Action method to ListFolders.
     * 
     * @param model instance of model
     * @throws Exception throws in case of any errors
     */
    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ListFolders action started."));
        }
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;

        try {
            List<Folder> folders = new ArrayList<Folder>();

            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            FolderServiceHelper.list(metaCRUD, requestState, folders);

            MailboxMetaServiceHelper.updateLastAccessTime(metaCRUD,
                    requestState);
            metaCRUD.commit();

            requestState.getDbPojoMap().setProperty(MxOSPOJOs.folders, folders);

        } catch (final MxOSException e) {
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw e;
        } catch (final Exception e) {
            logger.error("Error while list folders.", e);
            if (metaCRUD != null) {
                metaCRUD.rollback();
            }
            throw new ApplicationException(
                    FolderError.FLD_UNABLE_TO_PERFORM_SEARCH.name(), e);
        } finally {
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("ListFolders action end."));
        }
    }

    /*
     * TODO: This code prepares folder name with full path. Need to revisit this
     * code.
     */
    public static String getFolderPathName(Folder folder, List<Folder> folders) {
        String folderName = folder.getFolderName();
        /*
         * TODO: Need to revisit this code to generate folder path. Integer
         * parentFolderId = folder.getParentFolderId(); if (parentFolderId !=
         * null && parentFolderId > 0) { String parentFolderName =
         * getFolderPathName(parentFolderId, foldersData); if (parentFolderName
         * != null) { return parentFolderName + "/" + folderName; } }
         */
        return folderName;
    }

}
