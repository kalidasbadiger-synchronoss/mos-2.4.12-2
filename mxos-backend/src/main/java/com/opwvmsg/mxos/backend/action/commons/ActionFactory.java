/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.commons;

import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.validator.OperationRule;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action registry which holds all the actions defined for MxOS. It is
 * responsible for calling action one after another for given operation.
 *
 * @author mxos-dev
 *
 */
public class ActionFactory {
    private static Logger logger = Logger.getLogger(ActionFactory.class);

    private static class ActionFactoryHolder {
        public static ActionFactory instance = new ActionFactory();
    }
    
    protected static final HashMap<String, MxOSBaseAction> actionRegistory =
            new HashMap<String, MxOSBaseAction>();

    /**
     * Default constructor.
     */
    private ActionFactory() {
    }

    public static ActionFactory getInstance() {
        return ActionFactoryHolder.instance;
    }

    /**
     * Method to register each action into ActionRegistry.
     * 
     * @param fqClassName name of the action class
     * @throws Exception Exception
     */
    public void register(final String fqClassName) throws Exception {
        try {
            logger.debug("Registering : " + fqClassName);
            final Class<?> kls = Class.forName(fqClassName);
            final MxOSBaseAction action = (MxOSBaseAction) kls.newInstance();
            actionRegistory.put(fqClassName, action);
        } catch (final ClassNotFoundException e) {
            logger.error("Can't create class " + fqClassName, e);
            throw e;
        } catch (final Exception e) {
            logger.error("Can't instantiate class " + fqClassName, e);
            throw e;
        }
    }

    /**
     * Method to execute all the actions.
     * 
     * @param actionIds - queue of actions
     * @param model - instance of model
     * @throws MxOSException MxOSException
     */
    public void exec(final List<String> actionIds,
            final MxOSRequestState mxosRequestState) throws MxOSException {
        logger.debug("Total actions - " + actionIds);
        for (int i = 0; i < actionIds.size(); i++) {
            logger.debug("action - " + actionIds.get(i));
            final MxOSBaseAction actionObject = actionRegistory.get(actionIds.get(i));
            try {
                actionObject.run(mxosRequestState);
            } catch (final MxOSException e) {
                OperationRule operationRule = MxOSApp.getInstance().getRules()
                        .getOperations().get(mxosRequestState.getOperationName());
                if (operationRule.rollbackWhenFail()) {
                    logger.info("Rollback for operation : " + mxosRequestState.getOperationName());
                    rollback(actionIds, i, mxosRequestState);
                }
                // throw original exception
                throw e;
            }
        }
    }
    
    /**
     * Method to execute single action
     * 
     * @param actionId action id
     * @param requestState request context
     * @throws MxOSException MxOSException
     */
    public void exec(String actionId, MxOSRequestState requestState) throws MxOSException {
        MxOSBaseAction actionObject = actionRegistory.get(actionId);
        if(actionObject == null) {
            logger.warn("Action " + actionId + " does not exist in registory.");
        } else {
            actionObject.run(requestState);
        }
    }
    
    /**
     * rollback for this current operation
     * 
     * @param actionIds the action queue list
     * @param index current action index in the action queue
     * @param requestState request context
     */
    private void rollback(List<String> actionIds, int index, MxOSRequestState requestState) {
        // call before actions rollback method
        for (int i = index - 1; i >= 0; i--) {
            MxOSBaseAction action = actionRegistory.get(actionIds.get(i));
            if(action instanceof MxOSRollbackAction) {
                try {
                    ((MxOSRollbackAction) action).rollback(requestState);
                } catch (MxOSException e) {
                    logger.error("Action " + actionIds.get(i) + " rollback for operation " 
                            + requestState.getOperationName() + " failed.", e);
                }
            }
        }
    }
}
