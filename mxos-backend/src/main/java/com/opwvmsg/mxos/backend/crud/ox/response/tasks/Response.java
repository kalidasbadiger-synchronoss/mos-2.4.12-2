/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.io.IOException;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;

/**
 * Abstract class to handle exceptions and converting json returned by OX into a
 * parseable tree
 * 
 * @author mxos-dev
 * 
 */
public abstract class Response {

    public static int getMultipleErrorCount(JsonNode jsonTree) {
        int count = 0;
        if (jsonTree.isArray()) {
            Iterator<JsonNode> nodes = jsonTree.getElements();
            while (nodes.hasNext()) {
                if (!nodes.next().path(OXContactsProperty.error.name())
                        .isMissingNode()) {
                    count++;
                }
            }
        }
        return count;
    }

    public static JsonNode getMultipleJsonTree(ClientResponse resp)
            throws ClientHandlerException, UniformInterfaceException,
            TasksException {
        String str = resp.getEntity(String.class);
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(JsonParser.Feature.INTERN_FIELD_NAMES, false);

        JsonNode jsonTree = null;
        try {
            jsonTree = mapper.readTree(str);
        } catch (JsonProcessingException e) {
            throw new TasksException(e);
        } catch (IOException e) {
            throw new TasksException(e);
        }
        return jsonTree;
    }

    public static void validateResponse(ClientResponse resp)
            throws TasksException {

        JsonNode jsonTree = getMultipleJsonTree(resp);

        validateResponse(jsonTree);
    }

    private static void validateResponse(JsonNode jsonTree)
            throws TasksException {
        if (!jsonTree.path(OXContactsProperty.error.name()).isMissingNode()) {

            int i = 0;
            JsonNode errorParams = jsonTree
                    .path(OXContactsProperty.error_params.name());
            Object[] parameters = new Object[errorParams.size()];

            for (JsonNode node : errorParams) {
                parameters[i++] = node.asText();
            }

            String message = jsonTree.path(OXContactsProperty.error.name())
                    .asText();

            message = String.format(message.replaceAll("\\$d", "\\$s"),
                    parameters);

            String category = jsonTree.path(OXContactsProperty.category.name())
                    .asText();
            String code = jsonTree.path(OXContactsProperty.code.name())
                    .asText();
            String errorId = jsonTree.path(OXContactsProperty.error_id.name())
                    .asText();

            throw new TasksException(message, category, code, errorId);
        }
    }

    protected JsonNode getTree(ClientResponse resp) throws TasksException {
        JsonNode jsonTree = getMultipleJsonTree(resp);

        validateResponse(jsonTree);

        return jsonTree;
    }

}
