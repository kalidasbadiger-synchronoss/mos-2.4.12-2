/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.message;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MetaDBTypes;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.message.pojos.Flags;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MessageError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.utils.paf.intermail.mail.RmeDataModel;

/**
 * Action class to get Message Flags.
 * 
 * @author mxos-dev
 */
public class GetMessageFlags implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMessageFlags.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetMessageFlags action start.");
        }
        CRUDProtocol protocol = CRUDUtils.getCRUDProtocol(requestState);
        RmeDataModel rmeDataModel = (protocol == CRUDProtocol.httprme) ? RmeDataModel.Leopard : RmeDataModel.CL;
        if (rmeDataModel == RmeDataModel.Leopard) {
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlags,
                    new Flags());
            // We are reading "MxOSPOJOs.messageFlagsMap" property in all
            // SetFlagXXX actions.
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlagsMap,
                    new HashMap<String, Boolean>());
            logger.info("No Get is required in Stateless MSS.");
        } else {
            // is required only for statefull RME
            ICRUDPool<IMetaCRUD> metaCRUDPool = null;
            IMetaCRUD metaCRUD = null;
            IBlobCRUD blobCRUD = null;

            try {
                final String email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                if (logger.isDebugEnabled()) {
                    logger.debug("Retrieving message flags for Email: " + email);
                }
                final String messageId = requestState.getInputParams()
                        .get(MessageProperty.messageId.name()).get(0);

                metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
                metaCRUD = metaCRUDPool.borrowObject();
                if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                    blobCRUD = CRUDUtils.getBlobCRUD(requestState);
                }

                Flags flags = MessageServiceHelper.populateMessageFlags(
                        metaCRUD, blobCRUD, requestState, messageId);

                // Commit/rollback is required for last access time.
                metaCRUD.commit();
                if (MxOSApp.getInstance().getMetaDBType() != MetaDBTypes.mss) {
                    blobCRUD.commit();
                }

                requestState.getDbPojoMap().setProperty(MxOSPOJOs.messageFlags,
                        flags);
                // We are reading "MxOSPOJOs.messageFlagsMap" property in all
                // SetFlagXXX actions.
                requestState.getDbPojoMap().setProperty(
                        MxOSPOJOs.messageFlagsMap,
                        new HashMap<String, Boolean>());
            } catch (final MxOSException e) {
                if (blobCRUD != null) {
                    blobCRUD.rollback();
                }
                if (metaCRUD != null) {
                    metaCRUD.rollback();
                }
                throw e;
            } catch (final Exception e) {
                logger.error("Error while get message flags.", e);
                if (blobCRUD != null) {
                    blobCRUD.rollback();
                }
                if (metaCRUD != null) {
                    metaCRUD.rollback();
                }
                throw new ApplicationException(
                        MessageError.MSG_UNABLE_TO_PERFORM_GET.name(), e);
            } finally {
                if (metaCRUDPool != null && metaCRUD != null) {
                    try {
                        metaCRUDPool.returnObject(metaCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetMessageFlags action end.");
        }
    }
}
