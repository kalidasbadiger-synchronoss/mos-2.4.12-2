/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import com.opwvmsg.mxos.backend.requeststate.IBackendState;

public class LDAPBackendState implements IBackendState {
    public Attributes attributes;

    public LDAPBackendState() {
        attributes = new BasicAttributes();
    }

    @Override
    public int getSize() {
        if (attributes != null) {
            return attributes.size();
        } else {
            return 0;
        }
    }
}
