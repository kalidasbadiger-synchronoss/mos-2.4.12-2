package com.opwvmsg.mxos.backend.action.tasks.participant;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

public class SetTaskParticipantName implements MxOSBaseAction{
	private static Logger logger = Logger.getLogger(SetTaskParticipantName.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}
		
		int participantNameCount = requestState.getInputParams()
		.get(TasksProperty.participantName.name()).size();

		StringBuilder participantNames = new StringBuilder();
		for(int i=0;i<participantNameCount; i++){
			participantNames.append(requestState.getInputParams()
					.get(TasksProperty.participantName.name()).get(i));
			participantNames.append(",");
		}
		
		MxOSApp.getInstance()
		.getTasksHelper()
		.setAttribute(requestState, TasksProperty.participantName,
				participantNames.toString());
		
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
