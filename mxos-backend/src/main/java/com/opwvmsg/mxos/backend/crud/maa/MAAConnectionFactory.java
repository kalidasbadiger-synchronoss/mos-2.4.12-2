/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.maa;

import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;

import com.opwvmsg.mxos.data.enums.SystemProperty;

/**
 * MAA connection pool factory to create connection to MAA.
 * This is required by Apache object pool.
 *
 * @author mxos-dev
 */
class MAAConnectionFactory extends BasePoolableObjectFactory<HttpClient> {

    @Override
    public HttpClient makeObject() throws Exception {
        String maaTimeoutMS = System.getProperty(SystemProperty.maaTimeoutMS
                .name());
        if (maaTimeoutMS == null || maaTimeoutMS.equals("")) {
            maaTimeoutMS = "10000"; // Default timeout period
        }
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.getParams().setIntParameter(
                CoreConnectionPNames.CONNECTION_TIMEOUT,
                Integer.parseInt(maaTimeoutMS));
        return httpclient;
    }

    @Override
    public void destroyObject(final HttpClient client)
            throws Exception {
        client.getConnectionManager().shutdown();
    }
}
