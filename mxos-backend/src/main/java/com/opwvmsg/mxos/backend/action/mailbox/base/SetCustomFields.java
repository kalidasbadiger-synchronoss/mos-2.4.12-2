/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxCustomProperty;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;

/**
 * Action class to set cosId.
 * 
 * @author mxos-dev
 */
public class SetCustomFields implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetCustomFields.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetCustomFields action start."));
        }
        try {
            String customFields = requestState.getInputParams()
                    .get(MailboxProperty.customFields.name()).get(0);
            Map<String, String> customFieldMap = getMapFromString(
                    requestState.getOperationName(), customFields);
            if (logger.isDebugEnabled()) {
                logger.debug("customFieldMap: " + customFieldMap);
            }
            if (customFieldMap != null) {
                if (customFieldMap.size() > 0) {
                    Set<String> keySet = customFieldMap.keySet();
                    Iterator<String> it = keySet.iterator();
                    while (it.hasNext()) {
                        String key = it.next();
                        LDAPMailboxCustomProperty lmcp = LDAPMailboxCustomProperty
                                .valueOf(key);
                        MxOSApp.getInstance()
                                .getMailboxHelper()
                                .setAttribute(requestState, lmcp,
                                        customFieldMap.get(key));
                    }
                }
            }
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while set custom fields.", e);
            throw new MxOSException(
                    MailboxError.MBX_UNABLE_TO_SET_CUSTOMFIELDS.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetCustomFields action end."));
        }
    }

    /**
     * Static utility to convert customFields blob string to Map.
     * 
     * @param operationName API
     * @param input customFields
     * @return Map of strings
     * @throws MxOSException exception if any parsing error.
     */
    public static Map<String, String> getMapFromString(
            final String operationName, final String input)
            throws MxOSException {
        // TODO: Move this into utility
        Map<String, String> output = new HashMap<String, String>();
        if (input != null) {
            String[] entries = input.split("\\|");
            for (String entry : entries) {
                String[] values = entry.split("\\:");
                if (values.length > 1) {
                    output.put(values[0], values[1]);
                } else {
                    // null and empty are NOT allowed for COS APIs.
                    if (operationName.contains(ServiceEnum.CosBaseService
                            .name())) {
                        // throw an error
                        throw new InvalidRequestException(
                                MailboxError.MBX_INVALID_CUSTOMFIELD.name());
                    } else {
                        // Null and empty are allowed in mailbox APIs.
                        output.put(values[0], "");
                    }
                }
            }
        }
        return output;
    }
}
