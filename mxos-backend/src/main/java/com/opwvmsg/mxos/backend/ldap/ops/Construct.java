/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import javax.naming.NamingException;
import javax.naming.directory.InitialDirContext;

import com.opwvmsg.mxos.backend.ldap.LDAPLink;

/**
 * Class to construct a LDAP context
 * 
 * @author mxos-dev
 * 
 */
public class Construct extends Operation {

    private LDAPLink link;

    public Construct(LDAPLink link) {
        this.link = link;
    }

    protected void invoke() throws NamingException {
        this.ldapContext = new InitialDirContext(link.get());
    }

}
