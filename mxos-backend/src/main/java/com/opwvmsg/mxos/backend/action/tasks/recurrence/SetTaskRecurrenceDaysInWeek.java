package com.opwvmsg.mxos.backend.action.tasks.recurrence;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

public class SetTaskRecurrenceDaysInWeek implements MxOSBaseAction{
	private static Logger logger = Logger.getLogger(SetTaskRecurrenceDaysInWeek.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		// TODO Auto-generated method stub
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}

		int daysInWeekNum = requestState.getInputParams()
		.get(TasksProperty.daysInWeek.name()).size();

		StringBuilder daysInWeek = new StringBuilder();
		for(int i=0;i<daysInWeekNum; i++){
			daysInWeek.append(requestState.getInputParams()
					.get(TasksProperty.daysInWeek.name()).get(i));
			daysInWeek.append(",");
		}

		MxOSApp.getInstance()
		.getTasksHelper()
		.setAttribute(requestState, TasksProperty.daysInWeek,
				daysInWeek.toString());

		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
