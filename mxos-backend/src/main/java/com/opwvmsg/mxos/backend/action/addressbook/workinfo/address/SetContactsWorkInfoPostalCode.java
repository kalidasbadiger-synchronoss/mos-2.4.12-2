/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.addressbook.workinfo.address;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set postalCode attribute of Contact WorkInfo object.
 * 
 * @author mxos-dev
 * 
 */

public class SetContactsWorkInfoPostalCode implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetContactsWorkInfoPostalCode.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetContactsWorkInfoPostalCode action start."));
        }

        if (mxosRequestState.getInputParams().containsKey(
                AddressBookProperty.contactsList.name())) {
            Object[] name = null;

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.postalCode.name()) != null) {

                name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.postalCode.name()).toArray();
            } else if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.workInfoPostalCode.name()) != null) {

                name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.workInfoPostalCode.name())
                        .toArray();
            }

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttributes(mxosRequestState,
                            AddressBookProperty.workInfoPostalCode, name);
        } else {
            String name = null;

            if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.postalCode.name()) != null) {

                name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.postalCode.name()).get(0);
            } else if (mxosRequestState.getInputParams().get(
                    AddressBookProperty.workInfoPostalCode.name()) != null) {

                name = mxosRequestState.getInputParams()
                        .get(AddressBookProperty.workInfoPostalCode.name())
                        .get(0);
            }

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(mxosRequestState,
                            AddressBookProperty.workInfoPostalCode, name);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetContactsWorkInfoPostalCode action end."));
        }
    }
}
