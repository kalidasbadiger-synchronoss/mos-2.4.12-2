/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Belgacom-mxos/bgc-mxos-lib/src/main/java/com/openwave/mxos/data/PrivPrefEnum.java#2 $
 */
package com.opwvmsg.mxos.backend.action.logging.validate;

import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.exception.InvalidRequestException;

/**
*
* @author Aricent
*
*/
public enum PrivPrefEnum {
    NEGZERO("-0"), NEGONE("-1"), NEGTWO("-2"), NEGTHREE("-3"), NEGFOUR("-4"),
    NEGFIVE("-5"), NEGSIX("-6"), POSZERO("0"), POSONE("1"), POSTWO("2"),
    POSTHREE("3"), POSFOUR("4"), POSFIVE("5"), POSSIX("6");

    private static final Map<String, PrivPrefEnum>
    BGC_PRIV_PREF_MAP = new HashMap<String, PrivPrefEnum>();

    private String privpref;

    private PrivPrefEnum(String privpref) {
        this.privpref = privpref;
    }

    static {
        BGC_PRIV_PREF_MAP.put(NEGZERO.privpref, NEGZERO);
        BGC_PRIV_PREF_MAP.put(NEGONE.privpref, NEGONE);
        BGC_PRIV_PREF_MAP.put(NEGTWO.privpref, NEGTWO);
        BGC_PRIV_PREF_MAP.put(NEGTHREE.privpref, NEGTHREE);
        BGC_PRIV_PREF_MAP.put(NEGFOUR.privpref, NEGFOUR);
        BGC_PRIV_PREF_MAP.put(NEGFIVE.privpref, NEGFIVE);
        BGC_PRIV_PREF_MAP.put(NEGSIX.privpref, NEGSIX);
        BGC_PRIV_PREF_MAP.put(POSZERO.privpref, POSZERO);
        BGC_PRIV_PREF_MAP.put(POSONE.privpref, POSONE);
        BGC_PRIV_PREF_MAP.put(POSTWO.privpref, POSTWO);
        BGC_PRIV_PREF_MAP.put(POSTHREE.privpref, POSTHREE);
        BGC_PRIV_PREF_MAP.put(POSFOUR.privpref, POSFOUR);
        BGC_PRIV_PREF_MAP.put(POSFIVE.privpref, POSFIVE);
        BGC_PRIV_PREF_MAP.put(POSSIX.privpref, POSSIX);
    }

    /**
     * Method to get Status enum from key.
     *
     * @param privPref privPref
     * @return PrivPrefEnum PrivPrefEnum
     * @throws InvalidDataException if enum not found for given data
     */
    public static PrivPrefEnum getPrivPrefEnum(String privPref)
        throws InvalidRequestException {
        if (privPref == null) {
            throw new InvalidRequestException("Invalid data supplied");
        } else {
            if (BGC_PRIV_PREF_MAP.get(privPref) != null) {
                return BGC_PRIV_PREF_MAP.get(privPref);
            } else {
                throw new InvalidRequestException("Invalid data supplied");
            }
        }
    }
}
