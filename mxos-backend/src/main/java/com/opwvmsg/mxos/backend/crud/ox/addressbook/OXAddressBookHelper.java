package com.opwvmsg.mxos.backend.crud.ox.addressbook;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.crud.IAddressBookHelper;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.exception.InvalidRequestException;

public class OXAddressBookHelper implements IAddressBookHelper {

    private Map<String, String> getAttributes(MxOSRequestState requestState) {
        IBackendState backendState = requestState.getBackendState().get(
                AddressBookDBTypes.ox.name());
        if (backendState == null) {
            backendState = new OXAddressBookBackendState();
            requestState.getBackendState().put(AddressBookDBTypes.ox.name(),
                    backendState);
        }
        return ((OXAddressBookBackendState) backendState).attributes;
    }

    @Override
    public void setAttribute(final MxOSRequestState mxosRequestState,
            final AddressBookProperty key, final Object value)
            throws InvalidRequestException {
        if (OXAddressBookUtil.JSONToOX_AddressBook.get(key) != null) {
            getAttributes(mxosRequestState).put(
                    OXAddressBookUtil.JSONToOX_AddressBook.get(key).name(),
                    value.toString());
        }
    }

    @Override
    public void setAttributes(MxOSRequestState mxosRequestState,
            AddressBookProperty key, Object[] values)
            throws InvalidRequestException {
        for (int i = 0; i < values.length; i++) {
            if (OXAddressBookUtil.JSONToOX_AddressBook.get(key) != null) {
                Map<String, String> map = getAttributesList(mxosRequestState,
                        values.length).get(i);
                map.put(OXAddressBookUtil.JSONToOX_AddressBook.get(key).name(),
                        (String) values[i]);
            }
        }
    }

    private List<Map<String, String>> getAttributesList(
            MxOSRequestState requestState, int size) {
        IBackendState backendState = requestState.getBackendState().get(
                AddressBookDBTypes.ox.name());
        if (backendState == null) {
            backendState = new OXAddressBookBackendState();
            requestState.getBackendState().put(AddressBookDBTypes.ox.name(),
                    backendState);
        }
        OXAddressBookBackendState state = ((OXAddressBookBackendState) backendState);
        if (state.attributesList.size() == 0) {
            state.initialize(size);
        }
        return state.attributesList;
    }
}
