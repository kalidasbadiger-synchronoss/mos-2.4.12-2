/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.posix;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.crud.IBlobCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.message.pojos.Body;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;
import com.opwvmsg.mxos.utils.misc.Compression;
import com.opwvmsg.mxos.utils.misc.CompressionType;

/**
 * POSIX CRUD APIs to access blob data.
 *
 * @author mxos-dev
 */
public class PosixBlobCRUD implements IBlobCRUD {
    private static Logger logger = Logger.getLogger(PosixBlobCRUD.class);
    private int maxBuckets;
    private String blobRootFolder;

    /**
     * Constructor: Builds up the buckets.
     *
     * @throws MxOSException MxOSException
     */
    public PosixBlobCRUD() throws MxOSException {
        try {
            maxBuckets = MxOSConfig.getPosixmaxBuckets();
            blobRootFolder = MxOSConfig.getPosixDbRootFolder();

            if ((blobRootFolder == null) || (blobRootFolder.isEmpty())) {
                // System.out.println("No Root Folder configured.");
                throw new InvalidRequestException("No Root Folder configured.");
            }

            if (maxBuckets <= 0) {
                // System.out.println("No Root Folder configured.");
                throw new InvalidRequestException(
                        "Max Buckets needs to be some positive value.");
            }

            final File rootFile = new File(blobRootFolder);

            if (!rootFile.exists()) {
                // Create the directory
                if (!rootFile.mkdirs())
                    throw new ApplicationException(
                            "Failed to create the folder: " + blobRootFolder);
            }

            if (rootFile.list().length == 0) {
                int childIter = 0;

                while (childIter < maxBuckets) {
                    // Create Subdirectories
                    createDir(blobRootFolder + File.separator
                            + String.valueOf(childIter));
                    childIter++;
                }
                // System.out.println("Successfully created buckets");
            } else {
                // The number of buckets shall be fixed once the buckets are
                // created.
                maxBuckets = rootFile.list().length;
                // System.out.println("maxFolders" + maxFolders);
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while posix blob crud creation.", e);
            throw new ApplicationException(e.getCause() != null ? e.getCause()
                    .getMessage() : e.getMessage());
        }
        logger.info("Posix Blob CRUD loaded...");
    }

    /**
     * Create File.
     *
     * @param mxosRequestState mxosRequestState
     * @throws MxOSException MxOSException
     */
    public void createBlob(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        try {
            // Get the message id.
            final String messageId = mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0);

            if ((messageId == null) || (messageId.isEmpty())) {
                // System.out.println("Invalid message id received");
                throw new InvalidRequestException("No message id received");
            }

            // Identify the directory where to store the message
            final String msgFile = getMsgFilePath(messageId);

            // Get the blob.
            final String messageBlob = mxosRequestState.getInputParams()
                    .get(MessageProperty.messagebody.name()).get(0);

            CompressionType compressionType;
            if (mxosRequestState.getInputParams().get(
                    MessageProperty.compressiontype.name()) != null) {
                compressionType = CompressionType.valueOf(mxosRequestState
                        .getInputParams()
                        .get(MessageProperty.compressiontype.name()).get(0));
            } else {
                compressionType = MxOSApp.getInstance().getCompressionType();
            }

            // Create the file
            createFile(msgFile);

            // System.out.println("Successfully created file: " + msgFile);

            // Do compression.
            final byte[] compressedData = Compression.compress(compressionType,
                    messageBlob);

            // Write data to File.
            writeDataToFile(msgFile, compressedData);

        } catch (final Exception e) {
            logger.error("Error while create file.", e);
            throw new ApplicationException(
                    ErrorCode.PSX_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Get the content of the file with the provided message id.
     *
     * @param mxosRequestState mxosRequestState
     * @param compressionType compressionType
     * @param message message
     * @throws MxOSException MxOSException
     */
    public void readBlob(final MxOSRequestState mxosRequestState,
            Body body)
            throws MxOSException {
        try {

            final String messageId = mxosRequestState.getInputParams()
                    .get(MessageProperty.messageId.name()).get(0);

            if ((messageId == null) || (messageId.isEmpty())) {
                // System.out.println("No message id received");
                throw new InvalidRequestException("No message id received");
            }

            // Identify the directory where to store the message
            final String msgFile = getMsgFilePath(messageId);

            // System.out.println("Reading the contents of the File: " +
            // msgFile);
            final StringBuffer contents = readDataFromFile(msgFile);
            final CompressionType compressionType = MxOSApp.getInstance()
                    .getCompressionType();
            // Do de-compression.
            final byte[] deCompressedData = Compression.decompress(
                    compressionType, contents.toString());
            if (body == null) {
                body = new Body();
            }
            body.setMessageBlob(new String(deCompressedData));
        } catch (final Exception e) {
            logger.error("Error while read file.", e);
            throw new ApplicationException(
                    ErrorCode.PSX_CONNECTION_ERROR.name(), e);
        }
    }

    /**
     * Delete the file.
     * 
     * @param mxosRequestState mxosRequestState
     * @param messageId messageId
     * @throws MxOSException MxOSException
     */
    public void deleteBlob(final MxOSRequestState mxosRequestState,
            final String messageId) throws MxOSException {
        if ((messageId == null) || (messageId.isEmpty())) {
            // System.out.println("No message id received");
            throw new InvalidRequestException("No message id received");
        }

        // Identify the directory where to store the message
        final String msgFile = getMsgFilePath(messageId);

        final File f1 = new File(msgFile);
        if (f1.delete()) {
            // System.out.println("Successfully deleted the file");
        } else {
            throw new ApplicationException(
                    ErrorCode.PSX_CONNECTION_ERROR.name(),
                    "Failed to delete the file(or File does not exist) for msgId: "
                            + messageId);
        }
    }

    @Override
    public void commit() throws ApplicationException {
        // Not required for Posix Blob CRUD

    }

    @Override
    public void rollback() throws ApplicationException {
        // Not required for Posix Blob CRUD
    }

    /**
     * Read the file.
     *
     * @param fileName: Absolute path of the file.
     * @return StringBuilder StringBuilder
     * @throws IOException IOException
     */
    private StringBuffer readDataFromFile(final String fileName)
            throws IOException {
        BufferedInputStream bufferedInput = null;
        byte[] buffer = new byte[8192];
        StringBuffer contents = new StringBuffer();

        try {
            bufferedInput = new BufferedInputStream(new FileInputStream(
                    fileName));
            while (bufferedInput.read(buffer) != -1) {
                contents.append(new String(buffer));
            }
        } finally {
            if (bufferedInput != null)
                bufferedInput.close();
        }
        return contents;
    }

    /**
     * Write data to File.
     *
     * @param fileName - fileName
     * @param data - data
     * @throws IOException Exception
     */
    private void writeDataToFile(final String fileName, final byte[] data)
            throws IOException {
        BufferedOutputStream bufferedOutput = null;
        try {
            bufferedOutput = new BufferedOutputStream(new FileOutputStream(
                    fileName));
            bufferedOutput.write(data);
        } finally {
            // Close the BufferedOutputStream
            if (bufferedOutput != null) {
                bufferedOutput.flush();
                bufferedOutput.close();
            }
        }
    }

    /**
     * Identifies the directory where we the file can be stored or picked up
     * based on the message id. Example 1: msgId: 20120112091206.AAE13649
     * maxBuckets: 10 Directory Number: 1669(Hash Code of 49) % 10(maxBuckets) =
     * 10 Example 2: msgId: 4 maxBuckets: 10 Directory Number: 52(Hash Code of
     * 4) % 10(maxBuckets) = 2.
     *
     * @param msgId - Message id of the message.
     * @return Directory Number(Not the absolute path)
     */
    private int getMsgDir(final String msgId) {
        if (msgId.length() > 5) {
            final String subStr = msgId.substring(0, 5);
            return subStr.hashCode() % maxBuckets;
        } else {
            return msgId.hashCode() % maxBuckets;
        }
    }

    /**
     * Identifies the absolute path of the file for the message id.
     *
     * @param msgId - Message id of the message.
     * @return: Absolute path.
     */
    private String getMsgFilePath(final String msgId) {
        // Identify the directory where to store the message
        final int dirNum = getMsgDir(msgId);

        StringBuffer dirPath = new StringBuffer();
        dirPath.append(blobRootFolder);
        dirPath.append(File.separator);
        dirPath.append(dirNum);
        dirPath.append(File.separator);
        dirPath.append(msgId);

        return dirPath.toString();
    }

    /**
     * Create File.
     *
     * @param fileName - Absolute path of the file.
     * @throws IOException, MxOSException
     */
    private void createFile(final String fileName) throws IOException,
            MxOSException {
        // Create a File.
        final File f = new File(fileName);
        if (!f.createNewFile())
            throw new MxOSException("Failed to create the file: " + fileName);
    }

    /**
     * Create Directory.
     *
     * @param dir Absolute path of the Dir.
     * @throws MxOSException MxOSException
     */
    private void createDir(final String dir) throws MxOSException {
        // Create a directory.
        final File f = new File(dir);
        if (!f.mkdirs())
            throw new MxOSException("Failed to create the folder: " + dir);
    }
}