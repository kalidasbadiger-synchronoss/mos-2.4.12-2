/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.captcha;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Credentials;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to validate captcha after LDAP update is done
 * 
 * @author mxos-dev
 * 
 */
public class PostValidateCaptcha implements MxOSBaseAction {
    private Logger logger = Logger.getLogger(PostValidateCaptcha.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("PostValidateCaptcha action start.");
        }

        int failedCaptchaAttempts = 0;
        int maxFailedCaptchaAttempts = -1;
        // mandatory parameter
        boolean captchaStatus = Boolean.valueOf(requestState.getInputParams()
                .get(MailboxProperty.isValid.name()).get(0));

        // if CAPTCHA validity is false/true, then get the details of
        // failure cases and the count of it.
        Credentials credentials = (Credentials) requestState.getDbPojoMap()
                .getPropertyAsObject(MxOSPOJOs.credentials);
        try {
            if (credentials.getFailedCaptchaLoginAttempts() != null) {
                failedCaptchaAttempts = credentials
                        .getFailedCaptchaLoginAttempts();
            }
            if (credentials.getMaxFailedCaptchaLoginAttempts() != null) {
                maxFailedCaptchaAttempts = credentials
                        .getMaxFailedCaptchaLoginAttempts();
            }
        } catch (NullPointerException npEx) {
            // it doesn't happen in theory
            logger.error("mandatory value in credentials are null", npEx);
            // assuming the failedCaptchaAttempts value as 0.
            // assuming the maxFailedCaptchaAttempts value as -1.
        } finally {
            if (!captchaStatus) {
                // Incrementing the attempt count, as the same is
                // incremented along with the mailbox update. the
                // increment is required here because the initial
                // credentials fetched have the previous value and the
                // incremented value is required for logic calculation.
                ++failedCaptchaAttempts;
            }
        }

        if (!captchaStatus) {
            // Throws the error in case of CAPTCHA is not valid.
            if (failedCaptchaAttempts < maxFailedCaptchaAttempts) {
                logger.error("CAPTCHA is not valid - Not exceeded the attempts");
                throw new MxOSException(
                        MailboxError.CPT_ATTEMPTS_ALLOWED.name(),
                        "CAPTCHA is not valid - Not exceeded the attempts");
            } else {
                logger.error("CAPTCHA is not valid - Exceeded the attempts");
                throw new MxOSException(
                        MailboxError.CPT_ATTEMPTS_EXCEEDED.name(),
                        "CAPTCHA is not valid - Exceeded the attempts");
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("PostValidateCaptcha action end.");
        }
    }
}
