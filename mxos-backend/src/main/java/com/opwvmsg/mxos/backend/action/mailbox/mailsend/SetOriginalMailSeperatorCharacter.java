/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailsend;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set .
 *
 * @author mxos-dev
 */
public class SetOriginalMailSeperatorCharacter implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetOriginalMailSeperatorCharacter.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetOriginalMailSeperatorCharacter action start."));
        }
        String originalMailSeperatorCharacter = requestState
                .getInputParams()
                .get(MailboxProperty.originalMailSeperatorCharacter.name())
                .get(0);

        try {
            // The below code is just a hack, need to handle this at frame work level
            if (originalMailSeperatorCharacter.equals("=")) {
                originalMailSeperatorCharacter = "0";
            } else if (originalMailSeperatorCharacter.equals(">")) {
                originalMailSeperatorCharacter = "1";
            } else if (originalMailSeperatorCharacter.equalsIgnoreCase("none")) {
                originalMailSeperatorCharacter = "2";
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_ORIGINAL_MAIL_SEPERATOR_CHARACTER
                                .name());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.originalMailSeperatorCharacter,
                            originalMailSeperatorCharacter);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set original mail separator character.", e);
            throw new InvalidRequestException(
                    MailboxError
                    .MBX_UNABLE_TO_SET_ORIGINAL_MAIL_SEPERATOR_CHARACTER
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetOriginalMailSeperatorCharacter action end."));
        }
    }
}
