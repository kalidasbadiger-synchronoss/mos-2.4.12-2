/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.crud;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Interface to create/read/update/delete mailbox.
 * 
 * @author mxos-dev
 */
public interface IRMIMailboxCRUD extends ITransaction {

    /**
     * Method to create mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox create operation using RMI api.
     * @throws MxOSException if any error.
     */
    void createMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to read mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox read operation.
     * @param base Mailbox Base POJO which will be populated with mailbox data using RMI api.
     * @throws MxOSException if any error.
     */
    void readMailbox(final MxOSRequestState mxOSRequestStatus, final Base base)
            throws MxOSException;

    /**
     * Method to update mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox update operation using RMI api.
     * @throws MxOSException if any error.
     */
    void updateMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

    /**
     * Method to hard delete mailbox metadata.
     * 
     * @param mxosRequestState parameters for mailbox hard delete operation using RMI api.
     * @throws MxOSException if any error.
     */
    void deleteMailbox(final MxOSRequestState mxosRequestState)
            throws MxOSException;

}
