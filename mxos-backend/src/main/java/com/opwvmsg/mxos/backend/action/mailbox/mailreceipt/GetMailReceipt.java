/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.DataMap;
import com.opwvmsg.mxos.data.pojos.MailReceipt;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get MailReceipt object.
 *
 * @author mxos-dev
 *
 */
public class GetMailReceipt implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailReceipt.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "GetMailReceipt action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;
        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;

        try {
            mailboxCRUDPool =
                    MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String email = requestState.getInputParams().get(
                    MailboxProperty.email.name()).get(0);
            final MailReceipt mr = mailboxCRUD.readMailReceipt(requestState, email);
            
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            metaCRUD.readMailbox(requestState, null);
            
            DataMap dataMap = requestState.getAdditionalParams();

            if (dataMap != null) {
                String autoReplyMessage = dataMap
                        .getProperty(MailboxProperty.autoReplyMessage);
                String mtaFilter = dataMap
                        .getProperty(MailboxProperty.mtaFilter);
                String rmFilter = dataMap.getProperty(MailboxProperty.rmFilter);
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuffer(" AutoReplyText : ")
                            .append(autoReplyMessage));
                    logger.debug(new StringBuffer(" MTA Filter : ")
                            .append(mtaFilter));
                    logger.debug(new StringBuffer(" RM Filter : ")
                            .append(rmFilter));
                }
                if (autoReplyMessage != null && !autoReplyMessage.equals("")) {
                    mr.setAutoReplyMessage(autoReplyMessage);
                }
                if (mtaFilter != null && !mtaFilter.equals("")) {
                    mr.getFilters().getSieveFilters().setMtaFilter(mtaFilter);
                }
                if (rmFilter != null && !rmFilter.equals("")) {
                    mr.getFilters().getSieveFilters().setRmFilter(rmFilter);
                }
            }
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.mailReceipt, mr);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get mail receipt.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_MAILRECEIPT_GET.name(), e);
        } catch (final Throwable t) {
            logger.error("Error while get mail receipt.", t);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), t);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
            if (metaCRUDPool != null && metaCRUD != null) {
                try {
                    metaCRUDPool.returnObject(metaCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Meta CRUD pool:" + e.getMessage());
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetMailReceipt action end."));
        }
    }
}
