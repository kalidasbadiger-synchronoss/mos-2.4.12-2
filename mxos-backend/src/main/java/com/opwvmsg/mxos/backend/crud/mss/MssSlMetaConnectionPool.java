/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.mss;

import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.ConnectionStats;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Implementation of Stateless MSS Meta CRUD connection pool.
 * 
 * @author mxos-dev
 */
public class MssSlMetaConnectionPool implements ICRUDPool<IMetaCRUD> {
    private static Logger logger = Logger
            .getLogger(MssSlMetaConnectionPool.class);

    private static GenericObjectPool<MssSlMetaCRUD> objPool;
    protected final int maxSize;

    /**
     * Constructor.
     * 
     * @throws Exception Exception.
     */
    public MssSlMetaConnectionPool() {
        maxSize = MxOSConfig.getMetaMaxConnections();
        objPool = new GenericObjectPool<MssSlMetaCRUD>(new MssSlMetaFactory(),
                maxSize);
        initializeJMXStats(maxSize);
    }

    @Override
    public IMetaCRUD borrowObject() throws MxOSException {
        IMetaCRUD obj = null;
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
        if (objPool.getNumActive() >= maxSize - 2) {
            logger.warn("MSS active connections reaching max...:"
                    + objPool.getNumActive());
        }
        try {
            obj = objPool.borrowObject();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while borrowing object.", e);
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name(), e);
        }

        if (obj == null) {
            logger.error("Borrowed object is null.");
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name());
        }
        incrementJMXStats();
        return obj;
    }

    @Override
    public void returnObject(IMetaCRUD metaCRUD) throws MxOSException {
        try {
            objPool.returnObject((MssSlMetaCRUD) metaCRUD);
            decrementJMXStats();
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while returning object.", e);
            throw new ApplicationException(
                    ErrorCode.MSS_CONNECTION_ERROR.name(), e);
        }
        logger.debug(new StringBuilder("Connections in Pool - Active: ")
                .append(objPool.getNumActive()).append(", Idle: ")
                .append(objPool.getNumIdle()).toString());
    }

    protected void initializeJMXStats(long count) {
        ConnectionStats.MSSSLMETA.setCount(count);
    }

    protected void incrementJMXStats() {
        ConnectionStats.ACTIVE_MSSSLMETA.increment();
    }

    protected void decrementJMXStats() {
        ConnectionStats.ACTIVE_MSSSLMETA.decrement();
    }

    @Override
    public void resetPool() throws MxOSException {
        // TODO Auto-generated method stub

    }
}
