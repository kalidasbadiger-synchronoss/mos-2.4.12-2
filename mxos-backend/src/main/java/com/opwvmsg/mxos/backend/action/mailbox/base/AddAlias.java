/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to add email alias.
 *
 * @author mxos-dev
 */
public class AddAlias implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(AddAlias.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddAlias action started."));
        }
        try {
            final List<String> newEmailAliases = requestState.getInputParams()
                    .get(MailboxProperty.emailAlias.name());
            final List<String> allowedDomains;
            final List<String> emailAliasesList;
            final Base base = (Base) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.base);
            // Create or Update Mailbox Request
            if (ActionUtils.isCreateMailboxOperation(requestState
                    .getOperationName())) {
                if (null == base || null == base.getMaxNumAliases()) {
                    logger.warn("MaxNumAliases is missing");
                    throw new ApplicationException(
                            MailboxError.MBX_ALIAS_UNABLE_TO_CREATE.name());
                }
                allowedDomains = new ArrayList<String>();
                emailAliasesList = new ArrayList<String>();
                // default domain in allowedDomains list
                final String email = requestState.getInputParams()
                        .get(MailboxProperty.email.name()).get(0);
                allowedDomains.add(ActionUtils.getDomainFromEmail(email));
            } else {
                // Add alias for existing mailbox
                 if (base != null) {
                    if (null == base.getMaxNumAliases()) {
                        logger.warn("MaxNumAliases does not exist for account or in COS.");
                        throw new ApplicationException(
                                MailboxError.MBX_ALIAS_UNABLE_TO_CREATE.name());
                    }
                    allowedDomains = base.getAllowedDomains();
                    if (null != base.getEmailAliases() ) {
                        emailAliasesList = base.getEmailAliases();
                    } else {
                        emailAliasesList = new ArrayList<String>();
                    }
                } else {
                    throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
                }                
            }
            if ((emailAliasesList.size() + newEmailAliases.size()) > base
                    .getMaxNumAliases()) {
                throw new InvalidRequestException(
                        MailboxError.MBX_ALIAS_REACHED_MAX_LIMIT.name());
            }
            for (String alias : newEmailAliases) {
                String aliasDomainName = ActionUtils
                        .getDomainFromEmail(alias);
                // Check for allowed domain
                if (!allowedDomains.contains(aliasDomainName.toLowerCase())) {
                    throw new InvalidRequestException(
                            MailboxError.MBX_ALIAS_NOT_EXIST_IN_ALLOWED_DOMAINS
                                    .name());
                }
                // add alias if already not exists
                if (!emailAliasesList.contains(alias.toLowerCase())) {
                    emailAliasesList.add(alias.toLowerCase());
                }
            }            
            // convert list to array
            final String[] emailAliasesArray = emailAliasesList
                    .toArray(new String[emailAliasesList.size()]);

            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.emailAlias,
                            emailAliasesArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add alias.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALIAS_UNABLE_TO_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddAlias action end."));
        }
    }
}
