/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.we.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.AddressBookError;

public class Query implements Visitor {

    public enum Condition implements EnumConverter {
        AND("&&"), OR("||");

        private final String value;

        Condition(String value) {
            this.value = value;
        }

        @Override
        public String convert() {
            return value;
        }
    }

    private List<Visitor> visitors;
    private List<Condition> conditions;

    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    private static Logger logger = Logger.getLogger(Query.class);

    public Query() {
        // WE only supports 4 terms
        visitors = new ArrayList<Visitor>();
        conditions = new ArrayList<Condition>();
    }

    public void addCondition(String condition) throws AddressBookException {
        if (condition != null) {
            if (condition.equals(Condition.AND.convert())) {
                conditions.add(Condition.AND);
            } else if (condition.equals(Condition.OR.convert())) {
                conditions.add(Condition.OR);
            }
        }
    }

    private void addVisitor(Visitor visitor) throws AddressBookException {
        if (visitor != null) {
            visitors.add(visitor);
        }
    }

    @Override
    public void parse(String input) throws AddressBookException {
        String[] visitors = input
                .split(MxOSConstants.QUERY_SEARCH_CRITERIA_FOR_CONTACTS);
        if (visitors == null) {
            logger.error("Invalid input provided " + input);
            throw new AddressBookException(
                    AddressBookError.ABS_INVALID_SEARCH_QUERY.name());
        } else {
            Visitor visitor = new Term();
            visitor.parse(visitors[0]);
            addVisitor(visitor);
        }
        for (int i = 1; i < visitors.length;) {
            addCondition(visitors[i++]);
            Visitor visitor = new Term();
            visitor.parse(visitors[i++]);
            addVisitor(visitor);
        }
    }

    @Override
    public boolean visit(Map<String, String> valueMap) {
        if (visitors == null || visitors.size() == 0)
            return true;

        boolean result = visitors.get(0).visit(valueMap);
        for (int i = 0; i < conditions.size();) {
            if (!conditions.isEmpty()) {
                // we dont support short-circuiting
                if (conditions.get(i) == Condition.AND) {
                    result = result & visitors.get(++i).visit(valueMap);
                } else if (conditions.get(i) == Condition.OR) {
                    result = result | visitors.get(++i).visit(valueMap);
                }
            }
        }
        return result;
    }

    public List<Visitor> getVisitors() {
        return visitors;
    }

    public void setVisitors(List<Visitor> visitors) {
        this.visitors = visitors;
    }

    public List<Condition> getConditions() {
        return conditions;
    }
}
