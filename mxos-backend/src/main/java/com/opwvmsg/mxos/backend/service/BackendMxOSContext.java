/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service;

import java.util.Properties;

import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.ContextEnum;
import com.opwvmsg.mxos.interfaces.service.IMxOSContext;
import com.opwvmsg.mxos.interfaces.service.ServiceLoader;

/**
 * Mailbox service exposed to client which is responsible for doing basic
 * mailbox related activities e.g create mailbox, delete mailbox, etc. directly
 * in the database.
 * 
 * @author mxos-dev
 */
public class BackendMxOSContext extends IMxOSContext {
    /**
     * Default constructor.
     * 
     * @throws MxOSException MxOSException
     * @throws Exception Exception
     */
    public BackendMxOSContext() throws MxOSException {
        serviceMap = ServiceLoader.loadServices(ContextEnum.BACKEND);
        System.out.println("BackendMxOSContext created...");
    }

    @Override
    public void init(final String contextId, final Properties p)
            throws MxOSException {
        this.contextId = contextId;
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }
}
