package com.opwvmsg.mxos.backend.service.process;

import java.util.List;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IExternalLdapAttributesService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

import org.apache.log4j.Logger;

public class BackendExternalLdapAttributesService implements IExternalLdapAttributesService {

    private static Logger logger = Logger.getLogger(BackendExternalLdapAttributesService.class);

    @SuppressWarnings("unchecked")
    @Override
    public Map<String, List<String>> readAttributes(
            Map<String, List<String>> inputParams) throws MxOSException {

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("BackendExternalLdapAttributesRetrieveService readAttributes() start"));
        }
        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.ExternalLdapAttributesService, Operation.GET);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.ExternalLdapAttributesService, Operation.GET, t0,
                    StatStatus.pass);

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("BackendExternalLdapAttributesRetrieveService readAttributes() end"));
            }

            return  (Map<String, List<String>>) mxosRequestState.getDbPojoMap().getPropertyAsObject(MxOSPOJOs.externalLdapAttributeMap);

        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.ExternalLdapAttributesService, Operation.POST, t0,
                    StatStatus.fail);
            throw e;
        }
    }
}
