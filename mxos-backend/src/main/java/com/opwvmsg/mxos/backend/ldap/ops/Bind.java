/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.ldap.ops;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;

/**
 * Class to execute LDAP bind operation
 *
 * @author mxos-dev
 *
 */
public class Bind extends Operation {
    
    private String name;
    private Object obj;
    private Attributes attrs;
    
    public Bind(String name, Object obj, Attributes attrs) {
        this.name = name;
        this.obj = obj;
        this.attrs = attrs;
    }

    protected void invoke() throws NamingException {
        ldapContext.bind(name, obj, attrs);
    }

}
