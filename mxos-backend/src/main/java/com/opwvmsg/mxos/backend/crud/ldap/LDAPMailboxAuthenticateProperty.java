/**
 * 
 */
package com.opwvmsg.mxos.backend.crud.ldap;

/**
 *  Property class for Mailbox Authenticate.
 *
 * @author mxos-dev
 *
 */
public enum LDAPMailboxAuthenticateProperty {
	mailboxid,mailmessagestore,mailautoreplyhost,mailRealm,
	mail, mailpasswordtype, 
	mailLastLoginAttemptDate, mailFailedLoginAttempts, mailMaxFailedLoginAttempts, mailFailedCaptchaLoginAttempts,
	mailMaxFailedCaptchaLoginAttempts, mailpassword
}
