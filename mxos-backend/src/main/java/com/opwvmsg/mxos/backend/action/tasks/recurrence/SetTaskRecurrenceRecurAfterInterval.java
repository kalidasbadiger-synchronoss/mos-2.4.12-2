package com.opwvmsg.mxos.backend.action.tasks.recurrence;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.action.tasks.base.SetTaskOwner;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

public class SetTaskRecurrenceRecurAfterInterval implements MxOSBaseAction{
	 private static Logger logger = Logger.getLogger(SetTaskRecurrenceRecurAfterInterval.class);
	@Override
	public void run(MxOSRequestState requestState) throws MxOSException {
		// TODO Auto-generated method stub
		 if (logger.isDebugEnabled()) {
	            logger.debug(new StringBuffer("action start."));
	        }

	        final String name = requestState.getInputParams()
	                .get(TasksProperty.recurAfterInterval.name()).get(0);
	        
	        MxOSApp.getInstance()
	                .getTasksHelper()
	                .setAttribute(requestState, TasksProperty.recurAfterInterval,
	                        name);

	        if (logger.isDebugEnabled()) {
	            logger.debug(new StringBuffer("action end."));
	        }
	}

}
