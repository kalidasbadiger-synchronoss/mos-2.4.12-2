/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set extensionAttributes .
 * 
 * @author mxos-dev
 */
public class SetExtensionAttributes implements MxOSBaseAction {
	private static Logger logger = Logger
			.getLogger(SetExtensionAttributes.class);

	@Override
	public void run(final MxOSRequestState requestState) throws MxOSException {
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer(
					"SetExtensionAttributes action start."));
		}
		try {
			String extensionAttributes = requestState.getInputParams()
					.get(MailboxProperty.extensionAttributes.name()).get(0);
			if (logger.isDebugEnabled()) {
				logger.debug("extensionAttributes: " + extensionAttributes);
			}
			Map<String, String> extensionAttributeMap = getMapFromString(
					requestState.getOperationName(), extensionAttributes);
			if (null != extensionAttributeMap
					&& extensionAttributeMap.size() > 0) {
				Set<String> keySet = extensionAttributeMap.keySet();
				Iterator<String> it = keySet.iterator();
				while (it.hasNext()) {
					String key = it.next();
					MxOSApp.getInstance()
							.getMailboxHelper()
							.setAttribute(requestState, key,
									extensionAttributeMap.get(key));
				}
			} else {
				logger.error("Invalid Extension Attribute.");
				throw new MxOSException(
						MailboxError.MBX_INVALID_EXTENSION_ATTRIBUTES.name());
			}
		} catch (MxOSException e) {
			throw e;
		} catch (Exception e) {
			logger.error("Error while set extension atributes.", e);
			throw new MxOSException(
					MailboxError.MBX_UNABLE_TO_SET_EXTENSION_ATTRIBUTES.name(),
					e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("SetExtensionAttributes action end."));
		}
	}

	/**
	 * Static utility to convert extensionAttributes blob string to Map.
	 * 
	 * @param operationName
	 * @param input
	 * @return output
	 * @throws MxOSException
	 */
	public Map<String, String> getMapFromString(final String operationName,
			final String input) throws MxOSException {
		Map<String, String> output = null;
		if (input.indexOf(MxOSConstants.COLON) != -1) {
			output = new HashMap<String, String>();
			if (input.indexOf("|") != -1) {
				String[] entries = input.split(MxOSConstants.PIPE);
				for (String entry : entries) {
					String[] values = entry.split("(?<!\\\\)"
							+ Pattern.quote(":"));
					if (values.length > 0) {
						String value = (values.length > 1) ? values[1] : "";
						if (checkPresence(values[0])) {
							output.put(values[0], value.replaceAll("\\\\", ""));
						} else {
							output = null;
							break;
						}
					}
				}
			} else {
				String[] values = input.split("(?<!\\\\)" + Pattern.quote(":"));
				if (values.length > 0) {
					String value = (values.length > 1) ? values[1] : "";
					if (checkPresence(values[0])) {
						output.put(values[0], value.replaceAll("\\\\", ""));
					}
				}
			}
		}
		return output;
	}

	/**
	 * 
	 * Check whether the user sent key is part of the extension attributes
	 * configured in the Config DB.
	 * 
	 * @return
	 */
	public boolean checkPresence(String key) {
		final String[] ldapExtAttributes = MxOSConfig
				.getExtensionsOptionalCosAttributes();
		for (String ldapExtAttribute : ldapExtAttributes) {
			if (ldapExtAttribute.toLowerCase().equals(key.toLowerCase())) {
				if (logger.isDebugEnabled()) {
					logger.debug("Extensions Attribute key " + key
							+ " found in Config DB.");
				}
				return true;
			}
		}
		return false;
	}
}
