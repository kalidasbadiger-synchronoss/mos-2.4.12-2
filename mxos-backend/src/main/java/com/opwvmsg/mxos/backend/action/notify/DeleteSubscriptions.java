/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.NotificationProperty.subscription;
import static com.opwvmsg.mxos.data.enums.NotificationProperty.topic;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;
import static com.opwvmsg.mxos.error.NotifyError.NTF_SUBSCRIPTION_NOT_FOUND;
import static com.opwvmsg.mxos.error.NotifyError.NTF_INVALID_SUBSCRIPTION;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

/**
 * Action to delete notify subscriptions
 * 
 * @author
 */
public class DeleteSubscriptions implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(DeleteSubscriptions.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteSubscriptions action start.");
        }

        final String aTopic = requestState.getInputParams().get(topic.name())
                .get(0);
        final List<String> subList = requestState.getInputParams().get(
                subscription.name());
        
        if (subList == null || subList.isEmpty()) {
            /* no subscriptions provided for given topic */
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder("No subscriptions provided for ")
                        .append(aTopic));
            }
            throw new InvalidRequestException(NTF_INVALID_SUBSCRIPTION.name());
        }
        
        final IDataStoreMultiMap<String, String> mMap = MxOSApp.getInstance()
                .getMultiMapDataStore();
        
        for (String sub : subList) {
            if (mMap.containsEntry(NOTIFY_MULTIMAP_ID, aTopic, sub)) {
                mMap.removeEntry(NOTIFY_MULTIMAP_ID, aTopic, sub);
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuilder(
                            "DeleteSubscriptions success. Topic ").append(aTopic)
                            .append(" and Subscription ").append(sub)
                            .append(" is deleted."));
                }
            } else {
                /* given topic not found in DataStore */
                if (logger.isDebugEnabled()) {
                    logger.debug(new StringBuilder(
                            "DeleteSubscriptions failed. Topic ").append(aTopic)
                            .append(" and Subscription ").append(sub)
                            .append(" not found."));
                }
                throw new NotFoundException(
                        NTF_SUBSCRIPTION_NOT_FOUND.name());
            }
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("Remaining Subscriptions size : "
                    + mMap.size(NOTIFY_MULTIMAP_ID));
            logger.debug("DeleteSubscriptions action end.");
        }
    }

}
