/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.external.ldap;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

/**
 * This is a utility class for external ldap related properties.
 * 
 * @author mx-os dev
 */
public class ExternalLDAPUtils {

    //Constant for input parameters
    private static final String PARAM_FILTER_VALUE="filterValue";

    protected static Logger logger = Logger.getLogger(ExternalLDAPUtils.class);
    private static ExternalLDAPUtils m_ldapUtils;
    private  String bindDn=null;
    private  String password=null;
    private  String baseDn=null;
    private  String searchFilter=null;
    private  String[] attributeList;
    private int maxSearchTimeout;


    /**
     * Default private constructor.
     */
    private ExternalLDAPUtils () {

    }

    public static ExternalLDAPUtils  getInstance(){
        if(m_ldapUtils==null)
        {
            synchronized (ExternalLDAPUtils.class) {
                if(m_ldapUtils==null)
                    m_ldapUtils=new ExternalLDAPUtils();
            }
        }
        return m_ldapUtils;
    }

    public String getBindDn() {
        return bindDn;
    }

    public void setBindDn(String curBindDn) {
        this.bindDn = curBindDn;
    }

    public String getCURPassword() {
        return password;
    }

    public void setCURPassword(String password) {
        this.password = password;
    }

    public String getBaseDn() {
        return baseDn;
    }

    public void setBaseDn(String baseDn) {
        this.baseDn = baseDn;
    }

    public String getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(String searchFilter) {
        this.searchFilter = searchFilter;
    }



    public String[] getAttributeList() {
        return attributeList;
    }

    public void setAttributList(String attributsString) {
        if(attributsString!=null){
            this.attributeList=attributsString.split(",");
        }
    }

    @Override
    public String toString() {
        return "LDAPUtils [bindDn=" + bindDn + ", password=" + password
                + ", baseDn=" + baseDn + ", searchFilter=" + searchFilter
                +", attributeList=" + Arrays.toString(attributeList)
                + "]";
    }

    public int getMaxSearchTimeout() {
        return maxSearchTimeout;
    }

    public void setMaxSearchTimeout(String  maxSearchTimeoutStr) {
        try{
            this.maxSearchTimeout = Integer.valueOf(maxSearchTimeoutStr);
        }catch(NumberFormatException e){
            logger.error("NumberFormatException in setMaxSearchTimeout() Using default",e);
            this.maxSearchTimeout=10;
        }
    }

    /**
     * This method will perform search in external ldap using input parameters.
     * @param mxosRequestState
     * @param externalLdapObject
     * @throws MxOSException
     */
    @SuppressWarnings("rawtypes")
    public static void retireveExternalLdapAttributes(MxOSRequestState mxosRequestState,
            ExternalLdapObject externalLdapObject)throws MxOSException {
        if(logger.isDebugEnabled())
            logger.debug("Inside retireveExternalLdapAttributes()....");
        String filterValue = mxosRequestState.getInputParams().get(PARAM_FILTER_VALUE).get(0);

        Attributes attributes = externalLdapObject.retrieveLdapAttributes(filterValue);
        HashMap<String, List<String>> attributeMap=new HashMap<String, List<String>>();
        if(attributes!=null){
            if(logger.isDebugEnabled())
                logger.debug("Attributes found for filterValue "+filterValue);
            try{
                for (Enumeration attributeEnumeration = attributes.getAll(); attributeEnumeration.hasMoreElements();) {
                    Attribute attribute=(Attribute)attributeEnumeration.nextElement();    	  
                    String attributeId=attribute.getID();
                    List <String> values=new LinkedList<String>();
                    for (Enumeration attributeValues = attribute.getAll(); attributeValues.hasMoreElements();) {
                        values.add(attributeValues.nextElement().toString());
                    }
                    attributeMap.put(attributeId, values);
                }
            }catch (final NamingException e) {
                logger.warn("NamingException while LDAP retireveExternalLdapAttributes().", e);
                throw new ApplicationException(
                        ErrorCode.LDP_NAMING_ERROR.name(), e);
            } 
        }else{
            if(logger.isDebugEnabled())
                logger.debug("No attribute found for filterValue"+filterValue);
        }
        mxosRequestState.getDbPojoMap().setProperty(MxOSPOJOs.externalLdapAttributeMap, attributeMap);
    }
}
