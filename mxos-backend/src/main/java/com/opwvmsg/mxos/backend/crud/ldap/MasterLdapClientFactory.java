/*
 * Copyright (c) 2015 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */

package com.opwvmsg.mxos.backend.crud.ldap;

import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * This is a simple LdapClientFactory subclass to make InitialLdapContext
 * objects for master directory.
 */
public class MasterLdapClientFactory extends LdapClientFactory<LdapContext> {
    
    protected static Logger logger = Logger.getLogger(MasterLdapClientFactory.class);

    public MasterLdapClientFactory() throws MxOSException {
        super();
    }

    @Override
    public LdapContext makeObject() throws Exception {
        return new InitialLdapContext(env, null);
    }
    
    @Override
    public void destroyObject(final LdapContext ldapContext) throws Exception {
        ldapContext.close();
    }

    /**
     * Strategy: Read master servers from configuration. Pick up only primary
     * master and return.
     * 
     * The method throws an exception when no master server is available.
     */
    @Override
    String[] getLdapServers() throws MxOSException {
        String[] masters = MxOSConfig.getLdapMasterServers();
        if (masters == null || masters.length < 1) {
            throw new MxOSException(ErrorCode.LDAP_INVALID_CONFIGURATION.name(),
                    "master directory server is not found in the configuration.");
        }
        logger.debug("master servers in config are " + masters[0]);
        // pick up only primary master.
        String[] servers = new String[1];
        servers[0] = masters[0];
        return servers;
    }
}

