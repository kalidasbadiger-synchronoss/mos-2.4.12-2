/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud;

import com.opwvmsg.mxos.exception.ApplicationException;

/**
 * Interface for Application level transaction management in MxOS.
 * 
 * @see <i>mxos Reference Guide</i>
 * 
 * @author mxos-dev
 */
public interface ITransaction {
    /**
     * This is useful to achieve transaction across services. E.g. system folder
     * creation for new mailbox request has to happen in a transaction.
     * 
     * @throws Exception
     *             Exception.
     */
    void commit() throws ApplicationException;

    /**
     * This is useful to rollback transaction across services.
     * 
     * @throws Exception
     *             Exception.
     */
    void rollback() throws ApplicationException;
}
