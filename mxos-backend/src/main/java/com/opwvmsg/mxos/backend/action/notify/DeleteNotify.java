/*
 * Copyright (c) 2013 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.notify;

import static com.opwvmsg.mxos.data.enums.NotificationProperty.topic;
import static com.opwvmsg.mxos.error.NotifyError.NTF_TOPIC_NOT_FOUND;
import static com.opwvmsg.mxos.data.enums.MxOSConstants.NOTIFY_MULTIMAP_ID;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;
import com.opwvmsg.mxos.utils.datastore.IDataStoreMultiMap;

/**
 * Action to delete notify/topic
 * 
 * @author
 * 
 */
public class DeleteNotify implements MxOSBaseAction {

    private static Logger logger = Logger.getLogger(DeleteNotify.class);

    @Override
    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteNotify action start.");
        }

        final String aTopic = requestState.getInputParams().get(topic.name())
                .get(0);

        final IDataStoreMultiMap<String, String> mMap = MxOSApp.getInstance()
                .getMultiMapDataStore();
        if (logger.isDebugEnabled()) {
            logger.debug("Total Subscriptions size : "
                    + mMap.size(NOTIFY_MULTIMAP_ID));
        }
        
        if (mMap.containsKey(NOTIFY_MULTIMAP_ID, aTopic)) {
            mMap.remove(NOTIFY_MULTIMAP_ID, aTopic);
        } else {
            /* given topic not found in DataStore */
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder("DeleteNotify failed. Topic ")
                        .append(aTopic).append(" not found."));
            }
            throw new NotFoundException(NTF_TOPIC_NOT_FOUND.name());
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuilder("DeleteNotify action end. Topic: ")
            .append(aTopic).append(" deleted."));
        }
    }

}
