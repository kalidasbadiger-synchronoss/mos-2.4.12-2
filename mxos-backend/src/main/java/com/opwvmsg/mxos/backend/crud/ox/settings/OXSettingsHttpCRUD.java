package com.opwvmsg.mxos.backend.crud.ox.settings;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.opwvmsg.mxos.addressbook.pojos.ContactBase;
import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ComponentException;
import com.opwvmsg.mxos.backend.crud.exception.LoginUserException;
import com.opwvmsg.mxos.backend.crud.exception.SettingsException;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPBackendState;
import com.opwvmsg.mxos.backend.crud.ldap.LDAPMailboxProperty;
import com.opwvmsg.mxos.backend.crud.ox.OXAbstractHttpCRUD;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.LoginResponse;
import com.opwvmsg.mxos.backend.crud.ox.response.addressbook.UserResponse;
import com.opwvmsg.mxos.backend.requeststate.IBackendState;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.common.ExternalSession;
import com.opwvmsg.mxos.data.enums.MailboxDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.SignatureInReplyType;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.pojos.ExternalAccounts;
import com.opwvmsg.mxos.data.pojos.MailAccount;
import com.opwvmsg.mxos.data.pojos.Signature;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.sun.jersey.api.client.ClientResponse;

public class OXSettingsHttpCRUD extends OXAbstractHttpCRUD implements
        ISettingsCRUD {

    private static Logger logger = Logger.getLogger(OXSettingsHttpCRUD.class);
    private static final String DATA_STRING = "data";
    private static final String NULL_STRING = "null";
    private static final String MISC_STRING = "misc";
    private static final String ABOVE_STRING = "above";
    private static final String BELOW_STRING = "below";
    private static final String INSERTION_STRING = "insertion";
    private static final String MAIL_MODULE = "io.ox/mail";
    private static final String SIGNATURE_STRING = "signature";
    private static final String CONTENT_STRING = "content";
    private static final String DISPLAYNAME_STRING = "displayname";
    private static final String MODULE_STRING = "module";
    private static final String TYPE_STRING = "type";
    private static final String SNIPPET_STRING = "snippet";
    private static final String JSLOB_STRING = "jslob";
    private static final String ACTION_STRING = "action";
    private static final String NEW_STRING = "new";
    private static final String LIST_STRING = "list";
    private static final String UPDATE_STRING = "update";
    private static final String DELETE_STRING = "delete";
    private static final String ALL_STRING = "all";
    private static final String OX_HTTP_URL_STRING = "oxHttpURL";
    private static final String PASSWORD_STRING = "test";
    private static final String ID = "id";
    private static final String Mail_ID = "io.ox/mail";
    private static final String DEF_SIG = "defaultSignature";
    private static final String REM_DEL_PERM = "removeDeletedPermanently";
    private static final String ALLOW_HTML_MSGS = "allowHtmlMessages";
    private static final String MSG_FORMAT = "messageFormat";
    private static final String ALLOW_HTML_IMAGES = "allowHtmlImages";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String DISPLAY_NAME = "display_name";
    private static final String GET = "get";
    private static final String FIRST_LAST_NAME_FOLDER = "6";
    private static final String USER_STRING = "user";
    private static final String SESSION = "session";
    private static final String COOKIE_STR = "cookieString";
    private static final String FOLDER = "folder";
    private static final String TIME_STAMP = "timestamp";
    private static final String ACC_STRING = "account";
    private static final String NEWC_STRING = "new";

    /**
     * Constructor.
     * 
     * @param baseURL - base url of mxos
     */
    public OXSettingsHttpCRUD(String baseURL) {
        webResource = getWebResource(baseURL);
    }

    @Override
    public void commit() throws ApplicationException {
        // do nothing
    }

    @Override
    public void createSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        ExternalSession session = null;
        Map<String, List<String>> params = mxosRequestState.getInputParams();
        String email = params.get(MailboxProperty.email.name()).get(0);
        try {
            final String userName = ActionUtils.getUserName(email);
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());

                session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, add the signature
                    if (signature != null) {
                        String signatureId = createSignature(mxosRequestState,
                                signature, session);
                        if (signature.getIsDefault() == BooleanType.YES) {
                            final List<String> keys = new ArrayList<String>();
                            final List<String> values = new ArrayList<String>();
                            keys.add(DEF_SIG);
                            values.add(signatureId);
                            updateOXSetting(mxosRequestState, session, keys,
                                    values);
                        }
                        mxosRequestState.getDbPojoMap().setProperty(
                                MxOSPOJOs.signatureId, signatureId);
                    }
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
        } finally {
            if (session != null) {
                try {
                    logoutOX(email, session);
                } catch (ComponentException e) {
                    logger.warn("Error while logout from OX: ", e);
                }
            }
        }
    }

    private String createSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(NEW_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));

        JSONObject json = new JSONObject();

        try {
            json.put(TYPE_STRING, SIGNATURE_STRING);
            json.put(MODULE_STRING, MAIL_MODULE);
            json.put(DISPLAYNAME_STRING, signature.getSignatureName());
            json.put(CONTENT_STRING, signature.getSignature());
            if (signature.getAddSignatureInReplyType() != null) {
                JSONObject jsonChild = new JSONObject();
                switch (signature.getAddSignatureInReplyType()) {
                case BEFORE_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, ABOVE_STRING);
                    break;
                case AFTER_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, BELOW_STRING);
                    break;
                default:
                }
                json.put(MISC_STRING, jsonChild);
            } else {
                // Default signature position
                JSONObject jsonChild = new JSONObject();
                jsonChild.put(INSERTION_STRING, BELOW_STRING);
                json.put(MISC_STRING, jsonChild);
            }
            
            addHeaderUserId(mxosRequestState, paramsNew);

            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    json.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private int readDefaultSignature(MxOSRequestState mxosRequestState,
            ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();
        paramsNew.put(ACTION_STRING, Arrays.asList(LIST_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        StringBuilder json = new StringBuilder();
        try {
            json.append("[ \"").append(Mail_ID).append("\" ]");
            if (logger.isDebugEnabled()) {
                logger.debug("Json Request: " + json.toString());
                logger.debug("paramsNew Request: " + paramsNew.toString());
            }
            final ClientResponse response = put(JSLOB_STRING, paramsNew,
                    json.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                if (logger.isDebugEnabled()) {
                    logger.debug("OX jslob data : " + result);
                }
                ObjectMapper mapper = new ObjectMapper();
                JsonNode rootNode = mapper.readTree(result);
                if (rootNode != null) {
                    JsonNode idNode = rootNode.findPath(DEF_SIG);
                    if (idNode != null) {
                        logger.info("idNode data : " + idNode.toString());
                        return idNode.asInt();
                    }
                }
                return -1;
            } else {
                return -1;
            }
        } catch (final Exception e) {
            logger.warn(ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private void updateOXSetting(MxOSRequestState mxosRequestState,
            ExternalSession session, final List<String> keys,
            final List<String> values) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(UPDATE_STRING));
        paramsNew.put(SESSION, Arrays.asList(session.getSessionId()));
        paramsNew.put(COOKIE_STR, Arrays.asList(session.getCookieString()));
        paramsNew.put(ID, Arrays.asList(Mail_ID));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        JSONObject json = new JSONObject();
        try {
            for (int i = 0; i < keys.size(); i++) {
                json.put(keys.get(i), values.get(i));
                if (logger.isDebugEnabled()) {
                    logger.debug("Update key:" + keys.get(i) + ", value:"
                            + values.get(i));
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Json Request: " + json.toString());
                logger.debug("paramsNew Request: " + paramsNew.toString());
            }
            put(JSLOB_STRING, paramsNew, json.toString());
        } catch (final Exception e) {
            logger.warn(ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private void updateUserName(MxOSRequestState mxosRequestState,
            ExternalSession session, final List<String> keys,
            final List<String> values) throws MxOSException {

        final int userId = getUserId(mxosRequestState, session);
        final Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(UPDATE_STRING));
        paramsNew.put(SESSION, Arrays.asList(session.getSessionId()));
        paramsNew.put(COOKIE_STR, Arrays.asList(session.getCookieString()));
        paramsNew.put(ID, Arrays.asList(Integer.toString(userId)));
        paramsNew.put(FOLDER, Arrays.asList(FIRST_LAST_NAME_FOLDER));
        paramsNew.put(TIME_STAMP,
                Arrays.asList(Long.toString(cal.getTimeInMillis())));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        JSONObject json = new JSONObject();
        try {
            for (int i = 0; i < keys.size(); i++) {
                json.put(keys.get(i), values.get(i));
                if (logger.isDebugEnabled()) {
                    logger.debug("Update key:" + keys.get(i) + ", value:"
                            + values.get(i));
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Json Request: " + json.toString());
                logger.debug("paramsNew Request: " + paramsNew.toString());
            }
            put(USER_STRING, paramsNew, json.toString());
        } catch (final Exception e) {
            logger.warn(ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private void updateExternalAcc(final MxOSRequestState mxosRequestState,
            final ExternalSession session, final MailAccount ma)
            throws MxOSException {

        final List<String> keys = new ArrayList<String>();
        final List<String> values = new ArrayList<String>();

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(NEW_STRING));
        paramsNew.put(SESSION, Arrays.asList(session.getSessionId()));
        paramsNew.put(COOKIE_STR, Arrays.asList(session.getCookieString()));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        if (ma.getAccountUserName() != null) {
            keys.add("login");
            values.add(ma.getAccountUserName());
        } else {
            throw new InvalidRequestException("accountUserName is missing");
        }

        keys.add("personal");
        values.add(ma.getAccountUserName());

        if (ma.getAccountPassword() != null) {
            keys.add("password");
            values.add(ma.getAccountPassword());
        } else {
            throw new InvalidRequestException("accountPassword is missing");
        }

        // This is mandatory parameter from OX otherwise PUT request fails
        keys.add("spam_handler");
        values.add("NoSpamHandler");

        if (ma.getServerAddress() != null) {
            keys.add("mail_server");
            values.add(ma.getServerAddress());
        } else {
            throw new InvalidRequestException("serverAddress is missing");
        }

        if (ma.getServerPort() != null) {
            keys.add("mail_port");
            values.add(ma.getServerPort().toString());
        } else {
            throw new InvalidRequestException("serverPort is missing");
        }

        if (ma.getMailSendSMTPAddress() != null) {
            keys.add("transport_server");
            values.add(ma.getMailSendSMTPAddress());
        } else {
            throw new InvalidRequestException("mailSendSMTPAddress is missing");
        }

        // This is mandatory parameter from OX
        keys.add("transport_protocol");
        values.add("smtp");

        if (ma.getMailSendSMTPPort() != null) {
            keys.add("transport_port");
            values.add(ma.getMailSendSMTPPort().toString());
        } else {
            throw new InvalidRequestException("mailSendSMTPPort is missing");
        }

        if (ma.getSmtpLogin() != null) {
            keys.add("transport_login");
            values.add(ma.getSmtpLogin());
        } else {
            throw new InvalidRequestException("useSmtpLogin is missing");
        }

        if (ma.getSmtpPassword() != null) {
            keys.add("transport_password");
            values.add(ma.getSmtpPassword());

            keys.add("transport_credentials");
            values.add("true");
        } else {
            keys.add("transport_credentials");
            values.add("false");

        }

        if (ma.getAccountName() != null) {
            keys.add("name");
            values.add(ma.getAccountName());
        } else {
            throw new InvalidRequestException("accountName is missing");
        }

        if (ma.getEmailAddress() != null) {
            keys.add("primary_address");
            values.add(ma.getEmailAddress());
        } else {
            throw new InvalidRequestException("emailAddress is missing");
        }

        if (ma.getTrashFolderName() != null) {
            final String[] trashDelim = ma.getTrashFolderName().split("[\\\\]");
            if (trashDelim.length > 0) {
                keys.add("trash");
                values.add(trashDelim[trashDelim.length - 1]);
            }
        }

        if (ma.getSendFolderName() != null) {
            final String[] sentDelim = ma.getSendFolderName().split("[\\\\]");
            if (sentDelim.length > 0) {
                keys.add("sent");
                values.add(sentDelim[sentDelim.length - 1]);
            }
        }

        if (ma.getDraftsFolderName() != null) {
            final String[] draftDelim = ma.getDraftsFolderName()
                    .split("[\\\\]");
            if (draftDelim.length > 0) {
                keys.add("drafts");
                values.add(draftDelim[draftDelim.length - 1]);
            }
        }

        if (ma.getSpamFolderName() != null) {
            final String[] spamDelim = ma.getSpamFolderName().split("[\\\\]");
            if (spamDelim.length > 0) {
                keys.add("spam");
                values.add(spamDelim[spamDelim.length - 1]);
            }
        }

        if (ma.getTrashFolderName() != null) {
            keys.add("trash_fullname");
            values.add(ma.getTrashFolderName());
        }

        if (ma.getSendFolderName() != null) {
            keys.add("sent_fullname");
            values.add(ma.getSendFolderName());
        }

        if (ma.getDraftsFolderName() != null) {
            keys.add("drafts_fullname");
            values.add(ma.getDraftsFolderName());
        }

        if (ma.getSpamFolderName() != null) {
            keys.add("spam_fullname");
            values.add(ma.getSpamFolderName());
        }

        if (ma.getAccountType() != null) {
            keys.add("mail_protocol");
            if (ma.getAccountType().equalsIgnoreCase("pop3ssl") || ma.getAccountType().equalsIgnoreCase("pop3s")) {
                values.add("pop3");
            } else if (ma.getAccountType().equalsIgnoreCase("imapssl") || ma.getAccountType().equalsIgnoreCase("imaps")) {
                values.add("imap");
            } else {
                values.add(ma.getAccountType().toLowerCase());
            }
        } else {
            throw new InvalidRequestException("accountType is missing");
        }

        keys.add("mail_secure");
        if ((ma.getAccountType().equalsIgnoreCase("pop3ssl") || ma
                .getAccountType().equalsIgnoreCase("imapssl") || ma
                .getAccountType().equalsIgnoreCase("pop3s") || ma
                .getAccountType().equalsIgnoreCase("imaps"))) {
            values.add("true");
        } else {
            values.add("false");
        }

        if (ma.getSmtpSSLEnabled() != null) {
            keys.add("transport_secure");
            if (ma.getSmtpSSLEnabled() == com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType.YES) {
                values.add("true");
            } else {
                values.add("false");
            }
        } else {
            throw new InvalidRequestException("smtpSSLEnabled is missing");
        }

        JSONObject json = new JSONObject();
        try {
            for (int i = 0; i < keys.size(); i++) {
                json.put(keys.get(i), values.get(i));
                if (logger.isDebugEnabled()) {
                    logger.debug("Update key:" + keys.get(i) + ", value:"
                            + values.get(i));
                }
            }

            if (logger.isDebugEnabled()) {
                logger.debug("Json Request: " + json.toString());
                logger.debug("paramsNew Request: " + paramsNew.toString());
            }
            put(ACC_STRING, paramsNew, json.toString());
        } catch (final Exception e) {
            logger.warn(ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private String updateSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(UPDATE_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));
        final List<String> paramList3 = new ArrayList<String>();
        paramList3.add(signature.getSignatureId().toString());
        paramsNew.put(OXContactsProperty.id.name(), paramList3);
        
        addHeaderUserId(mxosRequestState, paramsNew);

        JSONObject json = new JSONObject();
        try {

            json.put(TYPE_STRING, SIGNATURE_STRING);
            json.put(MODULE_STRING, MAIL_MODULE);
            json.put(DISPLAYNAME_STRING, signature.getSignatureName());
            json.put(CONTENT_STRING, signature.getSignature());

            if (signature.getAddSignatureInReplyType() != null) {
                JSONObject jsonChild = new JSONObject();
                switch (signature.getAddSignatureInReplyType()) {
                case BEFORE_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, ABOVE_STRING);
                    break;
                case AFTER_ORIGINAL_MESSAGE:
                    jsonChild.put(INSERTION_STRING, BELOW_STRING);
                    break;
                default:
                }
                json.put(MISC_STRING, jsonChild);
            }
        
            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    json.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private String deleteSignature(MxOSRequestState mxosRequestState,
            Signature signature, ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(DELETE_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        JSONObject json = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        try {
            json.put(OXContactsProperty.id.name(),
                    jsonArray.put(signature.getSignatureId()));

            final ClientResponse response = put(SNIPPET_STRING, paramsNew,
                    jsonArray.toString());
            String result = response.getEntity(String.class);
            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                return object.getString(DATA_STRING);
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private List<Signature> listSignature(MxOSRequestState mxosRequestState,
            ExternalSession session) throws MxOSException {

        List<Signature> signatures = new ArrayList<Signature>();
        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(ALL_STRING));
        paramsNew.put("session", Arrays.asList(session.getSessionId()));
        paramsNew.put("cookieString", Arrays.asList(session.getCookieString()));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse response = get(SNIPPET_STRING, paramsNew);
            String result = response.getEntity(String.class);

            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                JSONArray objArray = (JSONArray) object.get(DATA_STRING);
                for (int i = 0; i < objArray.length(); i++) {
                    Signature signature = new Signature();
                    object = objArray.getJSONObject(i);
                    if (!object.isNull("id")) {
                        if (!object.getString("id").equals(NULL_STRING)) {
                            signature.setSignatureId(object.getInt("id"));
                        }
                    }
                    if (!object.isNull("displayname")) {
                        signature.setSignatureName(object
                                .getString("displayname"));
                    }
                    if (!object.isNull("content")) {
                        signature.setSignature(object.getString("content"));
                    }
                    if (!object.isNull(MISC_STRING)) {
                        JSONObject miscObj = (JSONObject) object.get("misc");
                        if (!miscObj.isNull(INSERTION_STRING)) {
                            if (miscObj.getString(INSERTION_STRING)
                                    .equalsIgnoreCase(BELOW_STRING)) {
                                signature
                                        .setAddSignatureInReplyType(SignatureInReplyType.AFTER_ORIGINAL_MESSAGE);
                            } else {
                                signature
                                        .setAddSignatureInReplyType(SignatureInReplyType.BEFORE_ORIGINAL_MESSAGE);
                            }
                        }
                    }
                    signatures.add(signature);
                }
                return signatures;
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    private Integer getUserId(MxOSRequestState mxosRequestState,
            ExternalSession session) throws MxOSException {

        Map<String, List<String>> paramsNew = new HashMap<String, List<String>>();

        paramsNew.put(ACTION_STRING, Arrays.asList(GET));
        paramsNew.put(SESSION, Arrays.asList(session.getSessionId()));
        paramsNew.put(COOKIE_STR, Arrays.asList(session.getCookieString()));
        
        addHeaderUserId(mxosRequestState, paramsNew);

        try {
            final ClientResponse response = get(USER_STRING, paramsNew);
            String result = response.getEntity(String.class);

            if (result.contains(DATA_STRING)) {
                JSONObject object = new JSONObject(result);
                JSONObject dataObj = (JSONObject) object.get(DATA_STRING);

                if (!dataObj.isNull(ID)) {
                    if (!dataObj.getString(ID).equals(NULL_STRING)) {
                        return dataObj.getInt(ID);
                    }
                }
                throw new ApplicationException(
                        ErrorCode.OXS_USER_ID_NOT_FOUND.name());
            } else {
                throw new ApplicationException(
                        ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
            }
        } catch (final Exception e) {
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
        }
    }

    @Override
    public void deleteSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        ExternalSession session = null;
        Map<String, List<String>> params = mxosRequestState.getInputParams();
        String email = params.get(MailboxProperty.email.name()).get(0);
        try {
            final String userName = ActionUtils.getUserName(email);
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());
                session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, list the signatures
                    if (signature != null) {
                        deleteSignature(mxosRequestState, signature, session);
                    }
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
        } finally {
            if (session != null) {
                try {
                    logoutOX(email, session);
                } catch (ComponentException e) {
                    logger.warn("Error while logout from OX: ", e);
                }
            }
        }
    }

    @Override
    public void rollback() throws ApplicationException {
        // do nothing
    }

    /**
     *  This API checks if there is anything to update to OX via HTTP
     *  
     *  @param mxosRequestState mxosRequestState
     *  @return true if something to update in OX otherwise false
     */
    public static boolean needUpdate(MxOSRequestState mxosRequestState) {
        if (mxosRequestState.getInputParams().get(
                MailboxProperty.signatureId.name()) != null) {
            return true;
        }
        IBackendState backendState = mxosRequestState.getBackendState().get(
                MailboxDBTypes.ldap.name());
        Attributes attrs = ((LDAPBackendState) backendState).attributes;

        if (attrs != null && attrs.size() > 0) {
            Attribute attrUseTrash = attrs
                    .get(LDAPMailboxProperty.netmailusetrash.name());
            if (attrUseTrash != null) {
                return true;
            }

            Attribute attrUseRichTextEditor = attrs
                    .get(LDAPMailboxProperty.netmaildefaulteditor.name());
            if (attrUseRichTextEditor != null) {
                return true;
            }

            Attribute attrDisplayHeader = attrs
                    .get(LDAPMailboxProperty.netmailpreferredmsgview.name());
            if (attrDisplayHeader != null) {
                return true;
            }

            Attribute attrFilterHTMLContent = attrs
                    .get(LDAPMailboxProperty.netmailmsgviewprivacyfilter.name());
            if (attrFilterHTMLContent != null) {
                return true;
            }

            Attribute attrCn = attrs.get(LDAPMailboxProperty.cn.name());

            if (attrCn != null) {
                return true;
            }

            Attribute attrSn = attrs.get(LDAPMailboxProperty.sn.name());
            if (attrSn != null) {
                return true;
            }

            // Update External Account
            Attribute attrExternalAcc = attrs
                    .get(LDAPMailboxProperty.netmailpopacctdetails.name());
            if (attrExternalAcc != null) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public void updateSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        ExternalSession session = null;
        Map<String, List<String>> params = mxosRequestState.getInputParams();
        String email = params.get(MailboxProperty.email.name()).get(0);
        try {
            final String userName = ActionUtils.getUserName(email);
            Signature signature = (Signature) mxosRequestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.signature);
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuilder(" Signature : ")
                        .append(signature));
            }
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());

                session = loginOX(mxosRequestState);

                if (session != null) {
                    IBackendState backendState = mxosRequestState
                            .getBackendState().get(MailboxDBTypes.ldap.name());
                    Attributes attrs = ((LDAPBackendState) backendState).attributes;

                    final List<String> keys = new ArrayList<String>();
                    final List<String> values = new ArrayList<String>();

                    if (mxosRequestState.getInputParams().get(
                            MailboxProperty.signatureId.name()) != null) {
                        // Next, add the signature
                        if (signature != null) {
                            updateSignature(mxosRequestState, signature,
                                    session);
                            if (signature.getIsDefault() == BooleanType.YES) {
                                keys.add(DEF_SIG);
                                values.add(String.valueOf(signature
                                        .getSignatureId()));
                            } else if (signature.getIsDefault() == BooleanType.NO) {
                                keys.add(DEF_SIG);
                                values.add("");
                            }
                        }
                    }

                    if (attrs != null && attrs.size() > 0) {
                        Attribute attrUseTrash = attrs
                                .get(LDAPMailboxProperty.netmailusetrash.name());
                        if (attrUseTrash != null) {
                            keys.add(REM_DEL_PERM);
                            final int enumOrdinal = Integer
                                    .parseInt(attrUseTrash.get().toString());
                            values.add(CRUDUtils.booleanString(enumOrdinal));
                        }

                        Attribute attrUseRichTextEditor = attrs
                                .get(LDAPMailboxProperty.netmaildefaulteditor
                                        .name());
                        if (attrUseRichTextEditor != null) {
                            keys.add(MSG_FORMAT);
                            values.add(mxosRequestState
                                    .getInputParams()
                                    .get(MailboxProperty.useRichTextEditor
                                            .name()).get(0));

                        }

                        Attribute attrDisplayHeader = attrs
                                .get(LDAPMailboxProperty.netmailpreferredmsgview
                                        .name());
                        if (attrDisplayHeader != null) {
                            final int enumOrdinal = Integer
                                    .parseInt(attrDisplayHeader.get()
                                            .toString());
                            if (enumOrdinal == MxosEnums.DisplayHeadersType.HTML
                                    .ordinal()) {
                                keys.add(ALLOW_HTML_MSGS);
                                values.add("true");
                            } else if (enumOrdinal == MxosEnums.DisplayHeadersType.TEXT
                                    .ordinal()) {
                                keys.add(ALLOW_HTML_MSGS);
                                values.add("false");
                            }
                        }

                        Attribute attrFilterHTMLContent = attrs
                                .get(LDAPMailboxProperty.netmailmsgviewprivacyfilter
                                        .name());
                        if (attrFilterHTMLContent != null) {
                            keys.add(ALLOW_HTML_IMAGES);
                            final String filterHTMLContentValue = attrFilterHTMLContent
                                    .get().toString();

                            values.add(filterHTMLContentValue.toLowerCase());
                        }

                        boolean updateUser = false;
                        Attribute attrCn = attrs.get(LDAPMailboxProperty.cn
                                .name());
                        final List<String> keysUser = new ArrayList<String>();
                        final List<String> valuesUser = new ArrayList<String>();

                        if (attrCn != null) {
                            updateUser = true;
                            keysUser.add(FIRST_NAME);
                            valuesUser.add(attrCn.get().toString());
                        }

                        Attribute attrSn = attrs.get(LDAPMailboxProperty.sn
                                .name());
                        if (attrSn != null) {
                            updateUser = true;
                            keysUser.add(LAST_NAME);
                            valuesUser.add(attrSn.get().toString());
                        }

                        if (attrCn != null && attrSn != null) {
                            updateUser = true;
                            keysUser.add(DISPLAY_NAME);
                            valuesUser.add(attrSn.get().toString() + ", "
                                    + attrCn.get().toString());
                        }

                        if (updateUser) {
                            updateUserName(mxosRequestState, session, keysUser,
                                    valuesUser);
                        }

                        // Update External Account
                        Attribute attrExternalAcc = attrs
                                .get(LDAPMailboxProperty.netmailpopacctdetails
                                        .name());
                        if (attrExternalAcc != null) {
                            final String accountId = mxosRequestState
                                    .getInputParams()
                                    .get(MailboxProperty.accountId.name())
                                    .get(0);

                            final ExternalAccounts ea = (ExternalAccounts) mxosRequestState
                                    .getDbPojoMap().getPropertyAsObject(
                                            MxOSPOJOs.externalAccounts);
                            List<MailAccount> mailAccountList = ea
                                    .getMailAccounts();

                            if (mailAccountList != null) {
                                for (MailAccount ma : mailAccountList) {
                                    if (ma.getAccountId() == Integer
                                            .parseInt(accountId)) {
                                        updateExternalAcc(mxosRequestState,
                                                session, ma);
                                        break;
                                    }
                                }
                            }

                        }

                    }
                    // Finally update OX settings
                    if (keys.size() > 0 && values.size() > 0) {
                        updateOXSetting(mxosRequestState, session, keys, values);
                    }

                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
                }
            }
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
        } finally {
            if (session != null) {
                try {
                    logoutOX(email, session);
                } catch (ComponentException e) {
                    logger.warn("Error while logout from OX: ", e);
                }
            }
        }
    }

    @Override
    public String readSetting(MxOSRequestState mxosRequestState)
            throws MxOSException {
        ExternalSession session = null;
        Map<String, List<String>> params = mxosRequestState.getInputParams();
        String email = params.get(MailboxProperty.email.name()).get(0);
        try {
            final String userName = ActionUtils.getUserName(email);
            if (System.getProperties().containsKey(OX_HTTP_URL_STRING)) {

                // Get the session using auth bypass mechanism
                params.put("userId", new ArrayList<String>());
                params.get("userId").add(userName);

                // This is done as later we sometimes need to clear the password
                // which needs modifiable list
                params.put("password", new ArrayList<String>());
                params.get("password").add(PASSWORD_STRING);

                params.put("origin_ip", new ArrayList<String>());
                params.get("origin_ip").add(getHostIpAddress());
                session = loginOX(mxosRequestState);

                if (session != null) {
                    // Next, list the signatures
                    List<Signature> signatures = listSignature(
                            mxosRequestState, session);
                    if (signatures != null && signatures.size() > 0) {
                        // Read Default Signature
                        int defSignId = readDefaultSignature(mxosRequestState,
                                session);
                        if (defSignId > 0) {
                            for (Signature sign : signatures) {
                                if (sign.getSignatureId() == defSignId) {
                                    sign.setIsDefault(BooleanType.YES);
                                    break;
                                }
                            }
                        }
                    }
                    mxosRequestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.allSignatures, signatures);
                } else {
                    logger.error("Login to OX failed for user " + email);
                    throw new ApplicationException(
                            ErrorCode.OXS_HTTP_CONNECTION_ERROR.name());
                }
            }
            return null;
        } catch (final Exception e) {
            rollback();
            logger.error("Error while updating the OX MySQL database", e);
            throw new ApplicationException(
                    ErrorCode.OXS_HTTP_CONNECTION_ERROR.name(), e);
        } finally {
            if (session != null) {
                try {
                    logoutOX(email, session);
                } catch (ComponentException e) {
                    logger.warn("Error while logout from OX: ", e);
                }
            }
        }
    }

    private ClientResponse get(final String subURL,
            final Map<String, List<String>> params) throws MxOSException,
            SettingsException {
        try {
            return getOX(subURL, params);
        } catch (ComponentException e) {
            throw new SettingsException(e);
        }
    }

    private ClientResponse put(final String subURL,
            final Map<String, List<String>> params, final String jsonString)
            throws MxOSException, SettingsException {
        try {
            return putOX(subURL, params, jsonString);
        } catch (ComponentException e) {
            throw new SettingsException(e);
        }
    }

    @Override
    protected ExternalSession getSessionFromResponse(final ClientResponse r)
            throws LoginUserException {
        try {
            return new LoginResponse().getSession(r);
        } catch (ComponentException e) {
            throw new LoginUserException(e);
        }
    }

    @Override
    protected String getUserIdFromResponse(ClientResponse resp)
            throws LoginUserException {
        try {
            return new UserResponse().getUserId(resp);
        } catch (ComponentException e) {
            throw new LoginUserException(e);
        }
    }

	@Override
	public List<ContactBase> searchContactBase(MxOSRequestState requestState)
			throws MxOSException {
		// TODO Auto-generated method stub
	        throw new MxOSException(ErrorCode.GEN_REQUEST_FORBIDDEN.name(),"This Method is not supported.");
	}
}
