/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.credentials;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update password.
 * 
 * @author mxos-dev
 */
public class SetPasswordRecoveryPreference implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetPasswordRecoveryPreference.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPasswordRecoveryPreference action start."));
        }
        String passRecPref = requestState.getInputParams()
                .get(MailboxProperty.passwordRecoveryPreference.name()).get(0);
        // 0-none, 1-email, 2-SMS, 3-All

        if (passRecPref != null) {
            if (passRecPref
                    .equalsIgnoreCase(MxosEnums.PasswordRecoveryPreference.NONE
                            .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryPreference,
                                Integer.toString(MxosEnums.PasswordRecoveryPreference.NONE
                                        .ordinal()));
            } else if (passRecPref
                    .equalsIgnoreCase(MxosEnums.PasswordRecoveryPreference.EMAIL
                            .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryPreference,
                                Integer.toString(MxosEnums.PasswordRecoveryPreference.EMAIL
                                        .ordinal()));
            } else if (passRecPref
                    .equalsIgnoreCase(MxosEnums.PasswordRecoveryPreference.SMS
                            .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryPreference,
                                Integer.toString(MxosEnums.PasswordRecoveryPreference.SMS
                                        .ordinal()));
            } else if (passRecPref
                    .equalsIgnoreCase(MxosEnums.PasswordRecoveryPreference.ALL
                            .name())) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(
                                requestState,
                                MailboxProperty.passwordRecoveryPreference,
                                Integer.toString(MxosEnums.PasswordRecoveryPreference.ALL
                                        .ordinal()));
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_PASSWORD_RECOVERY_PREFERENCE
                                .name());
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetPasswordRecoveryPreference action end."));
        }
    }
}
