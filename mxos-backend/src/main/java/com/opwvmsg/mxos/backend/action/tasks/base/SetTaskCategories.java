/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.tasks.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set categories attribute of Task Base object.
 * 
 * @author mxos-dev
 * 
 */

public class SetTaskCategories implements MxOSBaseAction {
	private static Logger logger = Logger.getLogger(SetTaskCategories.class);

	@Override
	public void run(final MxOSRequestState mxosRequestState)
			throws MxOSException {
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}
		if (mxosRequestState.getInputParams().containsKey(
				TasksProperty.tasksList.name())) {
			Object[] categories = mxosRequestState.getInputParams()
					.get(TasksProperty.categories.name()).toArray();

			MxOSApp.getInstance()
					.getTasksHelper()
					.setAttributes(mxosRequestState, TasksProperty.categories,
							categories);
		} else {
			final String categories = mxosRequestState.getInputParams()
					.get(TasksProperty.categories.name()).get(0);

			MxOSApp.getInstance()
					.getTasksHelper()
					.setAttribute(mxosRequestState, TasksProperty.categories,
							categories);
		}
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
