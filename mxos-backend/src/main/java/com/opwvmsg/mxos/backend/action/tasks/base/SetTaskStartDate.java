/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.tasks.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set startDate attribute of Task Base object.
 * 
 * @author mxos-dev
 * 
 */

public class SetTaskStartDate implements MxOSBaseAction {
	private static Logger logger = Logger.getLogger(SetTaskStartDate.class);

	@Override
	public void run(final MxOSRequestState mxosRequestState)
			throws MxOSException {
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}
		if (mxosRequestState.getInputParams().containsKey(
				TasksProperty.tasksList.name())) {
			Object[] startDate = mxosRequestState.getInputParams()
					.get(TasksProperty.startDate.name()).toArray();

			MxOSApp.getInstance()
					.getTasksHelper()
					.setAttributes(mxosRequestState, TasksProperty.startDate,
							startDate);
		} else {
			final String startDate = mxosRequestState.getInputParams()
					.get(TasksProperty.startDate.name()).get(0);

			MxOSApp.getInstance()
					.getTasksHelper()
					.setAttribute(mxosRequestState, TasksProperty.startDate,
							startDate);
		}
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
