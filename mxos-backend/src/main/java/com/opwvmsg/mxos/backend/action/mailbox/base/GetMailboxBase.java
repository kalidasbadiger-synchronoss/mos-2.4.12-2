/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.OXSettingsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get Mailbox object.
 * 
 * @author mxos-dev
 * 
 */
public class GetMailboxBase implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMailboxBase.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetMailboxBase action start.");
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String email = requestState.getInputParams()
                    .get(MailboxProperty.email.name()).get(0);
            final Base base = mailboxCRUD.readMailboxBase(requestState, email);
            setDeafultSendAddress(requestState, base);
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.base, base);
        } catch (final MxOSException e) {
            throw e;
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetMailboxBase action end.");
        }
    }
    
    private void setDeafultSendAddress(MxOSRequestState requestState,
            Base base) throws MxOSException {
        // Canada Bell wants to get AppSuite default sender address
        // by mOS API, the value is selected from aliases
        // so this value is put into Base POJO
        boolean appSuiteIntegrated = Boolean.valueOf(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        boolean oxBackend = AddressBookDBTypes.ox.name().equals(
                MxOSConfig.getAddressBookBackend());
        if (appSuiteIntegrated && oxBackend) {
            ICRUDPool<ISettingsCRUD> settingsCRUDPool = null;
            ISettingsCRUD settingsCRUD = null;

            try {
                requestState.getAdditionalParams().setProperty(
                        OXSettingsProperty.ox_setting,
                        OXSettingsProperty.send_addr.name());
                settingsCRUDPool = MxOSApp.getInstance()
                        .getOXSettingsMySQLCRUD();
                settingsCRUD = settingsCRUDPool.borrowObject();
                String defaultSendAddress = settingsCRUD
                        .readSetting(requestState);
                base.setDefaultSendAddress(defaultSendAddress);
            } catch (MxOSException e) {
                throw e;
            } catch (Exception e) {
                logger.error("Error while setting defaultSendAddress", e);
            } finally {
                CRUDUtils.releaseConnection(settingsCRUDPool, settingsCRUD);
            }
        }
    }
}
