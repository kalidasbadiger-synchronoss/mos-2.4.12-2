/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.crud.ISettingsCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.OXSettingsProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.GeneralPreferences;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get GeneralPreferences object.
 *
 * @author mxos-dev
 *
 */
public class GetGeneralPreferences implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetGeneralPreferences.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetGeneralPreferences action start.");
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool =
                    MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            final String email = requestState.getInputParams().get(
                    MailboxProperty.email.name()).get(0); 
            final GeneralPreferences gp = mailboxCRUD.readGeneralPreferences(
                    email, requestState);
            setLocale(requestState, gp);
            
            requestState.getDbPojoMap().setProperty(
                    MxOSPOJOs.generalPreferences, gp);
        } catch (MxOSException e) {
            throw e;
        } finally {
            CRUDUtils.releaseConnection(mailboxCRUDPool, mailboxCRUD);
        }
        
        if (logger.isDebugEnabled()) {
            logger.debug("GetGeneralPreferences action end.");
        }
    }
    
    private void setLocale(MxOSRequestState requestState, GeneralPreferences gp)
            throws MxOSException {
        // when call mOS GeneralPerfeences to update locale
        // if AppSuite is integrated, mOS also update the AppSuite locale
        // but AppSuite does not use mOS API to update its locale
        // so the two sides locale are not consistency
        // Canada Bell wants to use mOS API to update and get AppSuite locale
        // so we need to get AppSuite locale if this API is called
        boolean appSuiteIntegrated = Boolean.valueOf(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        boolean oxBackend = AddressBookDBTypes.ox.name().equals(
                MxOSConfig.getAddressBookBackend());
        if (appSuiteIntegrated && oxBackend) {
            ICRUDPool<ISettingsCRUD> settingsCRUDPool = null;
            ISettingsCRUD settingsCRUD = null;

            try {
                requestState.getAdditionalParams().setProperty(
                        OXSettingsProperty.ox_setting,
                        OXSettingsProperty.preferredLanguage.name());

                settingsCRUDPool = MxOSApp.getInstance()
                        .getOXSettingsMySQLCRUD();
                settingsCRUD = settingsCRUDPool.borrowObject();
                String locale = settingsCRUD.readSetting(requestState);
                // use AppSuite locale to override LDAP value
                if (locale != null) {
                    gp.setLocale(locale);
                }
            } catch (MxOSException e) {
                throw e;
            } finally {
                CRUDUtils.releaseConnection(settingsCRUDPool, settingsCRUD);
            }
        }
    }
}
