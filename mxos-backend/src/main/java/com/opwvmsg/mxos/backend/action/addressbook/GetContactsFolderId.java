/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.addressbook;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IAddressBookCRUD;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.exception.AddressBookException;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.AddressBookProperty;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to get Contact Folder Id.
 * 
 * @author mxos-dev
 * 
 */
public class GetContactsFolderId implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetContactsFolderId.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetContactsFolderId action start."));
        }

        if (requestState.getInputParams().get(
                AddressBookProperty.contactId.name()) != null) {
            String name = requestState.getInputParams()
                    .get(AddressBookProperty.contactId.name()).get(0);

            MxOSApp.getInstance()
                    .getAddressBookHelper()
                    .setAttribute(requestState, AddressBookProperty.contactId,
                            name);
        }

        if (requestState.getInputParams().containsKey(
                AddressBookProperty.folderId.name())
                && requestState.getInputParams()
                        .get(AddressBookProperty.folderId.name()).get(0) != null) {
            logger.info("Read default folderId skipped, as folderId provided");
            return;
        }
        // folderId only applies for OX backend
        if (MxOSConfig.getAddressBookBackend().equals(
                AddressBookDBTypes.ox.name())) {

            ICRUDPool<IAddressBookCRUD> addressBookCRUDPool = null;
            IAddressBookCRUD addressBookCRUD = null;

            Map<String, List<String>> inputParams = requestState
                    .getInputParams();

            try {
                addressBookCRUDPool = MxOSApp.getInstance()
                        .getAddressBookCRUD();
                addressBookCRUD = addressBookCRUDPool.borrowObject();
                Integer folderId = addressBookCRUD
                        .readContactFolderId(requestState);
                List<String> folderIdList = new ArrayList<String>();
                folderIdList.add(String.valueOf(folderId));
                inputParams.put(AddressBookProperty.folderId.name(), folderIdList);
            } catch (AddressBookException e) {
                logger.error("Error while get folder.", e);
                ExceptionUtils.createMxOSExceptionFromAddressBookException(
                        AddressBookError.ABS_CONTACTS_FOLDER_UNABLE_TO_GET, e);
            } catch (final Exception e) {
                logger.error("Error while get folder.", e);
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                try {
                    if (addressBookCRUDPool != null && addressBookCRUD != null) {
                        addressBookCRUDPool.returnObject(addressBookCRUD);
                    }
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetContactsFolderId action end."));
        }
    }
}
