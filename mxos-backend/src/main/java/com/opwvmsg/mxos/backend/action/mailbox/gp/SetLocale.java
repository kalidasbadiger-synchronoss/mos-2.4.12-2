/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.gp;

import java.util.Locale;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class SetLocale implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetLocale.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetLocale action start."));
        }
        try {
            final String newLocale = requestState.getInputParams()
                    .get(MailboxProperty.locale.name()).get(0);
            final String newLocaleStr = newLocale.replace('-', '_');

            if (isValidLocale(newLocaleStr)) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState, MailboxProperty.locale,
                                newLocaleStr);
            } else {
                throw new InvalidRequestException(
                        MailboxError.MBX_INVALID_LOCALE.name());
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set locale.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_LOCALE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetLocale action end."));
        }
    }

    /**
     * To validate the given locale.
     * 
     * @param value locale
     * @return returns true if valid
     */
    private static boolean isValidLocale(final String value) {
        final Locale[] locales = Locale.getAvailableLocales();

        for (final Locale l : locales) {
            if (value.equals(l.toString())) {
                return true;
            }
        }
        return false;
    }
}
