/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.info;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set MessageEventRecords::maxFilesSizeKB.
 *
 * @author mxos-dev
 */
public class SetMaxFilesSizeKB implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMaxFilesSizeKB.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetMaxFilesSizeKB action start.");
        }
        final String maxFilesSizeKB = requestState.getInputParams()
                .get(MailboxProperty.maxFilesSizeKB.name()).get(0);
        try {
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.maxFilesSizeKB,
                            maxFilesSizeKB);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set max file size.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_FILES_SIZE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("SetMaxFilesSizeKB action end.");
        }
    }
}
