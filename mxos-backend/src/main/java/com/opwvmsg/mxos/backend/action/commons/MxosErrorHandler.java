/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Mxos_2_Mainline/mxos/mxos-core/src/main/java/com/openwave/mxos/exception/MxosErrorHandler.java#1 $
 */
package com.opwvmsg.mxos.backend.action.commons;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.Location;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * The MxosErrorHandler class is used to load the MxOS error mappings from
 * configuration.
 *
 * @author Aricent
 */
public class MxosErrorHandler {

    private static Logger logger = Logger.getLogger(MxosErrorHandler.class);

    private static final String ERROR = "error";
    private static final String CUSTOMCODE = "customCode";
    private static final String CODE = "code";
    private static final String MESSAGE = "message";

    /**
     * Default constructor.
     */
    protected MxosErrorHandler() {
    }

    /**
     * Error map to hold response error bean.
     */
    protected static final HashMap<String, ResponseErrorBean> ERROR_MAP =
        new HashMap<String, ResponseErrorBean>();

    /**
     * Parse XML by using local file.
     *
     * @param fileName Local File name
     * @param errorMap Map containing Operation rules
     * @return boolean
     * @throws MxOSException if any
     */
    public static boolean parse(String fileName,
            Map<String, ResponseErrorBean> errorMap) throws MxOSException {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(fileName,
                    new FileInputStream(fileName));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return parse(xmlr, errorMap);
    }

    /**
     * Parse XML by using InputStream.
     *
     * @param is InputStream
     * @param errorMap Map containing Operation rules
     * @return boolean parsing success or not
     * @throws MxOSException if any
     */
    public static boolean parse(InputStream is,
            Map<String, ResponseErrorBean> errorMap) throws MxOSException {
        XMLInputFactory xmlif = XMLInputFactory.newInstance();
        XMLStreamReader xmlr = null;
        try {
            xmlr = xmlif.createXMLStreamReader(is);
        } catch (XMLStreamException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return parse(xmlr, errorMap);
    }

    /**
     * Parse the XML reader.
     *
     * @param xmlr XMLReader
     * @param errorMap errorMap map
     * @throws MxOSException in case of any error while parsing rules
     */
    private static boolean parse(XMLStreamReader xmlr,
            Map<String, ResponseErrorBean> errorMap) throws MxOSException {
        boolean ret = true;
        int eventType = 0;

    LOOP:
        try {
            while (xmlr.hasNext()) {
                eventType = xmlr.next();
                switch (eventType) {
                    case XMLEvent.END_DOCUMENT:
                        break LOOP;
                    case XMLEvent.START_ELEMENT:
                        String name = xmlr.getLocalName();
                        if (ERROR.equals(name)) {
                            String customErrorCode =
                                xmlr.getAttributeValue(null, CUSTOMCODE);
                            String mxosErrorCode =
                                xmlr.getAttributeValue(null, CODE);
                            String errorMessage =
                                xmlr.getAttributeValue(null, MESSAGE);

                            ResponseErrorBean errorBean =
                                new ResponseErrorBean();
                            errorBean.setCustomErrorCode(customErrorCode);
                            errorBean.setMxosErrorCode(mxosErrorCode);
                            errorBean.setShortMessage(errorMessage);

                            errorMap.put(mxosErrorCode, errorBean);
                        }
                        break;
                    default:
                        break;
                }
            }
        } catch (Exception e) {
            ret = false;
            Location loc = xmlr.getLocation();
            logger.error("Error in parsing Error mapping XML:"
                    + getLocInfo(loc), e);
            throw new MxOSException(ErrorCode.GEN_INTERNAL_ERROR.name(), e);
        }
        return ret;
    }

    /**
     * Return XML location info.
     *
     * @return String - location info
     */
    private static String getLocInfo(Location loc) {
        return loc.getSystemId() + ", line:" + loc.getLineNumber() + ", col:"
                + loc.getColumnNumber() + ", off:" + loc.getCharacterOffset();
    }
}
