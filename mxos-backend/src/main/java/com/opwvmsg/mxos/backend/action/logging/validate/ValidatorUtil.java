/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: //mxos/Belgacom-mxos/bgc-mxos-lib/src/main/java/com/openwave/mxos/util/ValidatorUtil.java#4 $
 */
package com.opwvmsg.mxos.backend.action.logging.validate;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.opwvmsg.mxos.utils.config.MxOSConfig;


/**
 * Validator Util class of mxos.
 *
 * @author Aricent
 *
 */
public final class ValidatorUtil {

    /**
     * Private Constructor.
     */
    private ValidatorUtil() {

    }

    /**
     * Method to validate optionspriv request parameter.
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateOptionsPriv(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[0-9]{1,3}$", value);
        }
        /*boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 999) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;*/
    }

    /**
     * Method to validate optionspref request parameter.
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateOptionsPref(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[0-9]{1,2}$", value);
        }
        /*boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 99) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;*/
    }

    /**
     * Method to validate fkey request parameter. Varchar of 32 characters, no
     * validation on it, not unique. If empty or more than 32 characters, error
     * 42
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateFKey(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else if (value.contains("*") || value.contains("%")) {
            return false;
        } else {
            return checkMatch("^.{1,32}$", value);
        }
        /*boolean isValid = true;
        if (value.length() == 0 || value.length() > 32) {
            isValid = false;
        }
        return isValid;*/
    }

    /**
     * Method to validate psource request parameter. Numeric value between 0 and
     * 127. Validation only if numeric. 41 error code if not numeric
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validatePSource(String value) {
        if (value == null || value.length() == 0) {
            return false;
        }
        boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 127) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;
    }
    /**
     * Method to validate newsPref request parameter.
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateNewsPref(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[0-9]{1,2}$", value);
        }
        /*boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 99) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;*/
    }
    /**
     * Method to validate expireDaysPriv request parameter.
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateExpireDaysPriv(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[0-9]{1,4}$", value);
        }
        /*boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 9999) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;*/
    }
    /**
     * Method to validate expireDaysPref request parameter.
     *
     * @param value value
     * @return isValid isValid
     */
    public static boolean validateExpireDaysPref(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[0-9]{1,4}$", value);
        }
        /*boolean isValid = true;
        try {
            int i = Integer.parseInt(value);
            if (i < 0 || i > 9999) {
                isValid = false;
            }
        } catch (Exception e) {
            isValid = false;
        }
        return isValid;*/
    }

    /**
     * Method to validate relayHost request parameter.
     *
     * @param value
     *            value
     * @return boolean boolean
     */
    public static boolean validateNextHop(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^[a-z0-9\\.\\-]{4,254}$", value);
        }
    }
    /**
     * Method to validate bgcDomainfkey request parameter.
     *
     * @param value
     *            value
     * @return boolean boolean
     */
    public static boolean validateDomainFKey(String value) {
        if (value == null || value.length() == 0) {
            return false;
        } else {
            return checkMatch("^\\d{1,}$", value);
        }
    }

    /**
     * Method to check the given domainName is eixsts or not in primary domains
     * list.
     *
     * @param domainName
     *            domainName
     * @return true - if exists, false - if not exists.
     */
    public static boolean isExistsInPrimaryDomainsList(String domainName) {
        if (domainName == null || domainName.equals("")) {
            return false;
        }
        String[] pds = MxOSConfig.getBgcPrimaryDomains();
        if (pds == null || pds.length == 0) {
            return false;
        }
        for (String pd : pds) {
            if (pd != null && pd.equalsIgnoreCase(domainName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to check pattern with the given regular expression.
     *
     * @param pattern pattern
     * @param str str
     * @return true - if matches, false - if not matches.
     */
    public static boolean checkMatch(String pattern, String str) {
        if (pattern != null && str != null) {
            Pattern p = Pattern.compile(pattern);
            Matcher m = p.matcher(str);
            return m.matches();
        } else {
            return false;
        }
    }
}
