/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMetaCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to update mailbox in MSS
 * 
 * @author mxos-dev
 * 
 */
public class UpdateMailboxInMSS extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger.getLogger(UpdateMailboxInMSS.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInMSS action start.");
        }

        ICRUDPool<IMetaCRUD> metaCRUDPool = null;
        IMetaCRUD metaCRUD = null;
        try {
            metaCRUDPool = CRUDUtils.getMetaCRUDPool(requestState);
            metaCRUD = metaCRUDPool.borrowObject();
            metaCRUD.updateMailbox(requestState);
            if (logger.isDebugEnabled()) {
                logger.debug("Update mailbox in MSS successfully.");
            }
        } catch (MxOSException e) {
            logger.error("Error while updating mailbox in MSS", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while updating mailbox in MSS", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            CRUDUtils.releaseConnection(metaCRUDPool, metaCRUD);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("UpdateMailboxInMSS action end.");
        }
    }
}
