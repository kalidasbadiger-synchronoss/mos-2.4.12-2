/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.tasks.details;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.task.pojos.Details;

/**
 * Action class to Get Tasks Details object.
 * 
 * @author mxos-dev
 * 
 */
public class GetTaskDetails implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetTaskDetails.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetTaskDetails action start."));
        }

        ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
        ITasksCRUD taskDetails = null;

        try {
            tasksCRUDPool = MxOSApp.getInstance().getTasksCRUD();
            taskDetails = tasksCRUDPool.borrowObject();
            Details details = taskDetails
            .readTasksDetails(requestState);
            logger.info("Details Response : "+details.toString());
            requestState.getDbPojoMap().setProperty(MxOSPOJOs.details,
            		details);
            
        } catch (TasksException e) {
            logger.error("Error while get details.", e);
            ExceptionUtils.createMxOSExceptionFromTasksException(
                    TasksError.TSK_UNABLE_TO_GET_TASK_DETAILS, e);
        } catch (final Exception e) {
            logger.error("Error while get participant.", e);
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    e);
        } finally {
            try {
                if (tasksCRUDPool != null && taskDetails != null) {
                    tasksCRUDPool.returnObject(taskDetails);
                }
            } catch (final MxOSException e) {
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("GetTaskDetails action end."));
        }
    }
}
