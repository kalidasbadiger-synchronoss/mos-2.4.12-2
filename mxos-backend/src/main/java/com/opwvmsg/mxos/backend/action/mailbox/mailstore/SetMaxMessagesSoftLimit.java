/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.action.mailbox.mailstore;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set mailsoftquotamaxmsgs.
 * 
 * @author mxos-dev
 */
public class SetMaxMessagesSoftLimit implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetMaxMessagesSoftLimit.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxMessagesSoftLimit action start."));
        }
        try {
            final String maxMessagesSoftLimit = requestState.getInputParams()
                    .get(MailboxProperty.maxMessagesSoftLimit.name()).get(0);

            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.maxMessagesSoftLimit,
                            maxMessagesSoftLimit);
        } catch (final NumberFormatException e) {
            logger.warn("Number Format error while maxMessagesSoftLimit.");
            throw new InvalidRequestException(
                    MailboxError.MBX_INVALID_MAX_MESSAGES_SOFT_LIMIT.name(), e);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set maxMessagesSoftLimit.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_MAX_MESSAGES_SOFT_LIMIT.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("SetMaxMessagesSoftLimit action end."));
        }
    }
}
