/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */
package com.opwvmsg.mxos.backend.action.tasks.details;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set progress attribute of Task Base object.
 * 
 * @author mxos-dev
 * 
 */

public class SetTaskDetailsTargetCost implements MxOSBaseAction {
	private static Logger logger = Logger.getLogger(SetTaskDetailsTargetCost.class);

	@Override
	public void run(final MxOSRequestState mxosRequestState)
			throws MxOSException {
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action start."));
		}
		final String targetCost = mxosRequestState.getInputParams()
		.get(TasksProperty.targetCost.name()).get(0);

		MxOSApp.getInstance()
		.getTasksHelper()
		.setAttribute(mxosRequestState, TasksProperty.targetCost,
				targetCost);
		if (logger.isDebugEnabled()) {
			logger.debug(new StringBuffer("action end."));
		}
	}
}
