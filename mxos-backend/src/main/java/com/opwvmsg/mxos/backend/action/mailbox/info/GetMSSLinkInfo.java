/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.info;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.CRUDProtocol;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MessageProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse;
import com.opwvmsg.mxos.data.pojos.MssLinkInfo;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to get MSSLinkInfo object.
 * 
 * @author mxos-dev
 * 
 */
public class GetMSSLinkInfo implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(GetMSSLinkInfo.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("GetMSSLinkInfo action start.");
        }
        // If we get the messageStoreHost and mailboxId as Input Parameters,
        // avoid the LDAP GET call
        try {
            if ((requestState.getInputParams()
                    .containsKey(MailboxProperty.mailboxId.name()))
                    && (requestState.getInputParams()
                            .containsKey(MailboxProperty.messageStoreHost
                                    .name()))) {
                MssLinkInfo info = new MssLinkInfo();
                info.setMailboxId(requestState.getInputParams()
                        .get(MailboxProperty.mailboxId.name()).get(0));
                info.setMessageStoreHosts(requestState.getInputParams().get(
                        MailboxProperty.messageStoreHost.name()));

                if (requestState.getInputParams().get(
                        MailboxProperty.realm.name()) != null) {
                    String realm = requestState.getInputParams()
                            .get(MailboxProperty.realm.name()).get(0);
                    realm = realm.replaceAll("/", "");
                    info.setRealm(realm);
                }
                if (requestState.getInputParams().get(
                        MessageProperty.isAdmin.name()) != null) {
                    final String isAdmin = requestState.getInputParams()
                            .get(MessageProperty.isAdmin.name()).get(0);
                    info.setIsAdmin(TrueOrFalse.fromValue(isAdmin));
                } else {
                    info.setIsAdmin(TrueOrFalse.FALSE);
                }
                if ((CRUDUtils.getCRUDProtocol(info) == CRUDProtocol.httprme)
                        && (info.getRealm() != null)) {
                    requestState.getDbPojoMap().setProperty(
                            MxOSPOJOs.mssLinkInfo, info);
                    CRUDUtils.resolveBackendProtocol(info);
                    return;
                }
            }
        } catch (final MxOSException e) {
            throw e;
        } catch (Exception e) {
            throw new ApplicationException(ErrorCode.GEN_INTERNAL_ERROR.name(),
                    "Error while creating MSS Link Info Object from Input Parameter "
                            + e.getMessage());
        }

        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD mailboxCRUD = null;

        try {
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxSearchCRUD();
            mailboxCRUD = mailboxCRUDPool.borrowObject();
            ActionUtils.readMSSLinkInfo(requestState, mailboxCRUD);
            MssLinkInfo info = (MssLinkInfo) requestState.getDbPojoMap()
                    .getPropertyAsObject(MxOSPOJOs.mssLinkInfo);
            if (requestState.getInputParams().containsKey(
                    MailboxProperty.messageStoreHost.name())) {
                info.setMessageStoreHosts(requestState.getInputParams().get(
                        MailboxProperty.messageStoreHost.name()));
            }
        } catch (final MxOSException e) {
            e.setRequestParams(requestState.getInputParams().toString());
            e.setOperationType(requestState.getOperationName());
            logger.warn("Error while reading messageStore Info: "
                    + e.toString());
            throw e;
        } catch (final Exception e) {
            logger.error("Error while get mss link info.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_MSSLINKINFO_GET.name(), e);
        } finally {
            if (mailboxCRUDPool != null && mailboxCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(mailboxCRUD);
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(),
                            "Error in finally clause while returing "
                                    + "Provisioning CRUD pool:"
                                    + e.getMessage());
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("GetMSSLinkInfo action end.");
        }
    }
}
