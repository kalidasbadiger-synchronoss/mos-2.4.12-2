/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.commons;

import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.opwvmsg.mxos.data.enums.MailboxProperty;

/**
 * Util of notification class
 * 
 * @author mxos-dev
 * 
 */
public class NotifyUtils {

    public static final String MAILBOX_STATUS_NOTIFY_TOPIC = "mailboxId.status.mailbox";

    public static final String NOTIFY_MSG_ATTR_TYPE = "Type";
    public static final String NOTIFY_MSG_ATTR_MAILBOX_ID = "MailboxId";
    public static final String NOTIFY_MSG_ATTR_NEW_VALUE = "NewValue";
    
    public static String getMailboxStatusNotifyTopic(String mailboxId) {
        return MAILBOX_STATUS_NOTIFY_TOPIC.replace(
                MailboxProperty.mailboxId.name(), mailboxId);
    }

    public static String getMailboxStatusNotifyMessage(NotifyMsgType msgType,
            String mailboxId, String newValue) {
        ObjectNode node = new ObjectMapper().createObjectNode();
        node.put(NOTIFY_MSG_ATTR_TYPE, msgType.name());
        node.put(NOTIFY_MSG_ATTR_MAILBOX_ID, mailboxId);
        if (!StringUtils.isEmpty(newValue)) {
            node.put(NOTIFY_MSG_ATTR_NEW_VALUE, newValue);
        }
        return node.toString();
    }

    public static enum NotifyMsgType {
        // for mailbox status
        StatusChanged, PasswordChanged;
    }
}
