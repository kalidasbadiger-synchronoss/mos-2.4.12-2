/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.CRUDUtils;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IRMIMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.AddressBookDBTypes;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to delete mailbox from OX
 * 
 * @author mxos-dev
 * 
 */
public class DeleteMailboxFromOX extends AbstractMailbox implements
        MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteMailboxFromOX.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromOX action start.");
        }

        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);

        // check whether skip mailbox deletion from OX
        boolean delete = true;
        if (requestState.getInputParams().containsKey(SKIP_OX)) {
            boolean skip = Boolean.valueOf(requestState.getInputParams()
                    .get(SKIP_OX).get(0));
            if (skip) {
                delete = false;
                logger.info("Skip mailbox [" + email + "] deletion from OX.");
            }
        }

        // compatible with old parameter deleteOnlyOnMss
        if (requestState.getInputParams().containsKey(
                MailboxProperty.deleteOnlyOnMss.name())) {
            boolean deleteOnlyOnMSS = Boolean.valueOf(requestState
                    .getInputParams()
                    .get(MailboxProperty.deleteOnlyOnMss.name()).get(0));
            if (deleteOnlyOnMSS) {
                delete = false;
                logger.info("Delete mailbox [" + email
                        + "] only from MSS, so skip OX.");
            }
        }

        Boolean appSuiteIntegrated = Boolean.parseBoolean(System
                .getProperty(SystemProperty.appSuiteIntegrated.name()));
        if (appSuiteIntegrated && delete) {
            if (MxOSConfig.getAddressBookBackend().equals(
                    AddressBookDBTypes.ox.name())) {
                ICRUDPool<IRMIMailboxCRUD> rmiMailboxCRUDPool = null;
                IRMIMailboxCRUD rmiMailboxCRUD = null;
                try {
                    rmiMailboxCRUDPool = MxOSApp.getInstance()
                            .getRMIMailboxCRUD();
                    rmiMailboxCRUD = rmiMailboxCRUDPool.borrowObject();
                    rmiMailboxCRUD.deleteMailbox(requestState);
                    logger.info("Delete mailbox [" + email
                            + "] from OX successfully.");
                } catch (MxOSException e) {
                    logger.error("Error while deleting mailbox [" + email
                            + "] from OX", e);
                    throw e;
                } catch (Exception e) {
                    logger.error("Error while deleting mailbox [" + email
                            + "] from OX", e);
                    throw new ApplicationException(
                            MailboxError.MBX_UNABLE_TO_DELETE.name(), e);
                } finally {
                    CRUDUtils.releaseConnection(rmiMailboxCRUDPool,
                            rmiMailboxCRUD);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug("DeleteMailboxFromOX action end.");
        }
    }

}
