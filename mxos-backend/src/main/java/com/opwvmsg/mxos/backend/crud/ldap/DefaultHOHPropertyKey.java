package com.opwvmsg.mxos.backend.crud.ldap;

public enum DefaultHOHPropertyKey {

    ADMINREALMDN("adminRealm"), ADMINALLOCS("adminAllocs"), ROLEHOH("roleHOH"), ORGANIZATIONALUNIT(
            "organizationalUnit"), SELFREADACL("selfReadACI"), RELATEADMINREALM(
            "relateAdminRealm"), MEMBERACCESSACI("memberAccessACI"), ADMINCONSTRAINTS(
            "adminConstraints"), GROUPHOH("groupHOH"), MEMBERGROUPHOH(
            "memberGroupHOH");

    private String key;

    private String value;

    /**
     * 
     * @param type type
     * @param typeText typeText
     */
    private DefaultHOHPropertyKey(final String type, final String value) {
        this.key = type;
        this.value = value;
    }

    /**
     * 
     * @param typeText typeText
     */
    private DefaultHOHPropertyKey(final String value) {
        this.value = value;
    }

    /**
     * Returns the corresponding key.
     * 
     * @return key
     */
    public final String getKey() {
        return key;
    }

    /**
     * Returns value.
     * 
     * @return String text
     */
    public final String getValue() {
        return value;
    }
}
