/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.cos;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.IMailboxHelper;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.CosError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set all the required params used in create Cos.
 *
 * @author mxos-dev
 */
public class SetCreateCosParams implements MxOSBaseAction {
    private static final Logger logger = Logger
            .getLogger(SetCreateCosParams.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCreateCosParams action start."));
        }
        // Mandatory params
        final String cosId =
            requestState.getInputParams()
                        .get(MailboxProperty.cosId.name()).get(0);

        try {
            IMailboxHelper helper = MxOSApp.getInstance()
                    .getMailboxHelper();
            helper.setAttribute(requestState, MailboxProperty.Cn.name(),
                    cosId.toLowerCase());
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while create cos params.", e);
            throw new ApplicationException(
                    CosError.COS_UNABLE_TO_CREATE.name(), e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCreateCosParams action end."));
        }
    }
}
