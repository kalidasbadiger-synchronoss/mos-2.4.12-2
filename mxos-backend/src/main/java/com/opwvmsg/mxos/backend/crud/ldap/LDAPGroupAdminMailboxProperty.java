/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.crud.ldap;

/**
 * Property class for Group Admin Mailbox.
 *
 * @author mxos-dev
 *
 */
public enum LDAPGroupAdminMailboxProperty {
    
    billingid,
    adminapprovedsenderslist,
    adminblockedsenderslist,
    adminparentaldefaultdisposition,
    adminparentalrejectaction,
    adminparentalrejectinfo,

    adminmaxdomains,
    adminmaximap,
    adminmaximapssl,
    adminmaxintermanager,
    adminmaxmaildomains,
    adminmaxmailinglists,
    adminmaxmobilemail,
    adminmaxorgs,
    adminmaxpop,
    adminmaxpopssl,
    adminmaxrealms,
    adminmaxselfcare,
    adminmaxsmtp,
    adminmaxsmtpssl,
    adminmaxstoragekb,
    adminmaxusers,
    adminmaxwebmail,
    adminmaxwebmailssl,
    adminminallowedaccesslevel,
    adminnumdomains,
    adminnumimap,
    adminnumimapssl,
    adminnumintermanager,
    adminnummaildomains,
    adminnummailinglists,
    adminnummobilemail,
    adminnumorgs,
    adminnumorgunits,
    adminnumpop,
    adminnumpopssl,
    adminnumrealms,
    adminnumselfcare,
    adminnumsmtp,
    adminnumsmtpssl,
    adminnumusers,
    adminnumwebmail,
    adminusedstoragekb,
    adminnumwebmailssl,
    adminparentadminpolicy,
    admintargetdn,
    adminparentdomains,
    adminalloweddomains,
    member
}
