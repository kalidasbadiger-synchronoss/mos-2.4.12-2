/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.pojos.Base;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.exception.NotFoundException;

/**
 * Action class to delete email alias.
 *
 * @author mxos-dev
 */
public class DeleteAlias implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(DeleteAlias.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteAlias action start."));
        }
        try {
            final String removeEmailAlias =
                    requestState.getInputParams()
                    .get(MailboxProperty.emailAlias.name()).get(0);
            final List<String> emailAliasesList;
    
            ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
            IMailboxCRUD mailboxCRUD = null;
    
            final Base base;
            try {
                mailboxCRUDPool =
                        MxOSApp.getInstance().getMailboxCRUD();
                mailboxCRUD = mailboxCRUDPool.borrowObject();
                base = ActionUtils.readMailboxProvisioning(requestState,
                        mailboxCRUD);
            } catch (final Exception e) {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name(), e);
            } finally {
                if (mailboxCRUDPool != null && mailboxCRUD != null) {
                    try {
                        mailboxCRUDPool.returnObject(mailboxCRUD);
                    } catch (final MxOSException e) {
                        throw new ApplicationException(
                                ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                    }
                }
            }
    
            if (base == null) {
                throw new NotFoundException(MailboxError.MBX_NOT_FOUND.name());
            }
    
            if (base.getEmailAliases() != null) {
                emailAliasesList = base.getEmailAliases();
            } else {
                emailAliasesList = new ArrayList<String>();
            }
            if (!emailAliasesList.remove(removeEmailAlias)) {
                throw new NotFoundException(
                        MailboxError.MBX_INVALID_ALIAS.name());
            }
            final String[] emailAliasesArray =
                    emailAliasesList.toArray(new String[emailAliasesList.size()]);
            MxOSApp.getInstance().getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.emailAlias,
                            emailAliasesArray);
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while delete alias.", e);
            throw new ApplicationException(
                    MailboxError.MBX_ALIAS_UNABLE_TO_DELETE.name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("DeleteAlias action end."));
        }
    }
}
