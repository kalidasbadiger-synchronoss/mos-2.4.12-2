/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ExceptionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.validator.InputValidator;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.error.AddressBookError;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.jmx.counter.HandlerStats;

/**
 * Util class of BackendServices.
 * 
 * @author mxos-dev
 */
public final class BackendServiceUtils {
    private static Logger logger = Logger.getLogger(BackendServiceUtils.class);

    /**
     * Default private constructor.
     */
    private BackendServiceUtils() {

    }

    /**
     * Method to validate and execute the given operations.
     * 
     * @param mxosRequestState - request params
     * @param operationName - Name of the operation to be performed
     * @throws MxOSException - on any exception
     */
    public static void validateAndExecuteService(
            final MxOSRequestState mxosRequestState) throws MxOSException {
        try {
            HandlerStats.ACTIVE_REQUESTS.increment();
            try {
                InputValidator.validateInputParams(
                        mxosRequestState.getInputParams(),
                        mxosRequestState.getOperationName());
            } catch (MxOSException e) {
                logger.warn("Validation Failed: " + e.getCode());
                throw e;
            }
            final List<String> actionsQueue = InputValidator.populateActions(
                    mxosRequestState.getInputParams(),
                    mxosRequestState.getOperationName());
            MxOSApp.getInstance().getActionFactory()
                    .exec(actionsQueue, mxosRequestState);
            logger.info(mxosRequestState.getOperationName()
                    + " API call success");
        } catch (final MxOSException e) {
            if (e.getCode() == null || e.getCode().equals("")) {
                e.setCode(ErrorCode.GEN_INTERNAL_ERROR.name());
            }
            // password hide implementation
            if (mxosRequestState.getInputParams().containsKey(
                    MailboxProperty.password.name())) {
                List<String> passwordList = mxosRequestState.getInputParams()
                        .get(MailboxProperty.password.name());
                passwordList.clear();
                passwordList.add("XXXXX");
            }
            e.setRequestParams(mxosRequestState.getInputParams().toString());
            e.setOperationType(mxosRequestState.getOperationName());
            ExceptionUtils.prepareMxOSErrorObject(e);
            logger.error(new StringBuilder("MxOSException is received: ")
                    .append(e));
            logger.info(mxosRequestState.getOperationName()
                    + " API call failed");
            throw e;
        } catch (final Exception e) {
            logger.error("Error while validation.", e);
            ApplicationException e1 = new ApplicationException(
                    ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            ExceptionUtils.prepareMxOSErrorObject(e1);
            logger.info(mxosRequestState.getOperationName()
                    + " API call failed");
            throw e1;
        } finally {
            HandlerStats.ACTIVE_REQUESTS.decrement();
        }
    }

    public static void fillupMultipleRequests(
            final Map<String, List<String>> inputParams1, final String listKey)
            throws MxOSException {

        String input = inputParams1.get(listKey).get(0);
        // convert input param to multiple requests
        String[] requests = input.split(MxOSConstants.LINE_REGEXP);

        // check if number of requests is not 0
        if (requests != null && requests.length == 0) {
            // throw exception
            throw new MxOSException(
                    AddressBookError.ABS_INVALID_MULTIPLE_CONTACTS.name());
        }
        Map<String, List<String>> multiParams = new HashMap<String, List<String>>();
        for (int i = 0; i < requests.length; i++) {
            String[] params = requests[i].split(MxOSConstants.FIELD_REGEXP);

            for (String param : params) {
                String[] paramKV = param.split(MxOSConstants.EQUALS);
                if (!multiParams.containsKey(paramKV[0])) {
                    multiParams.put(paramKV[0], new ArrayList<String>(
                            requests.length));
                }
            }
        }

        // parse each line and extract the input params per request
        for (int i = 0; i < requests.length; i++) {
            String[] params = requests[i].split(MxOSConstants.FIELD_REGEXP);

            for (String param : params) {
                String[] paramKV = param.split(MxOSConstants.EQUALS);
                for (String inputParam : multiParams.keySet()) {
                    if (inputParam.equals(paramKV[0]) && paramKV.length > 1) {
                        // pad with nulls
                        for (int j = multiParams.get(inputParam).size(); j < i; j++) {
                            multiParams.get(inputParam).add(null);
                        }
                        // add new value at current index
                        multiParams.get(inputParam).add(
                                paramKV[1].replace("\\&", "&").replace("\\&",
                                        "&"));
                    }
                }
            }
        }

        for (String inputParam : multiParams.keySet()) {
            if (multiParams.get(inputParam).size() < requests.length) {
                for (int i = multiParams.get(inputParam).size(); i < requests.length; i++) {
                    multiParams.get(inputParam).add(null);
                }
            }
        }
        inputParams1.putAll(multiParams);
    }
}
