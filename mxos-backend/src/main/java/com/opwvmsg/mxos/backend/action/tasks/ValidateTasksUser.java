/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.tasks;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.ITasksCRUD;
import com.opwvmsg.mxos.backend.crud.exception.ExceptionUtils;
import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.TasksDBTypes;
import com.opwvmsg.mxos.data.enums.TasksProperty;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.TasksError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to validate userId.
 * 
 * @author mxos-dev
 * 
 */
public class ValidateTasksUser implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(ValidateTasksUser.class);

    @Override
    public void run(final MxOSRequestState mxosRequestState)
            throws MxOSException {
        if (!MxOSApp.getInstance().enforceValidation()) {
            logger.info("Use Validation is skipped, as trustedClient is true");
            return;
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Validate user action start."));
        }

        // folderId only applies for OX backend
        if (MxOSConfig.getTasksBackend().equals(TasksDBTypes.ox.name())) {

            ICRUDPool<ITasksCRUD> tasksCRUDPool = null;
            ITasksCRUD tasksCRUD = null;

            try {
                tasksCRUDPool = MxOSApp.getInstance().getTasksCRUD();
                tasksCRUD = tasksCRUDPool.borrowObject();

                if (mxosRequestState.getInputParams().get(
                        TasksProperty.userId.name()) != null) {

                    String name = mxosRequestState.getInputParams()
                            .get(TasksProperty.userId.name()).get(0);

                    MxOSApp.getInstance()
                            .getTasksHelper()
                            .setAttribute(mxosRequestState,
                                    TasksProperty.userId, name);
                }

                tasksCRUD.validateUser(mxosRequestState);
            } catch (TasksException e) {
                ExceptionUtils.createMxOSExceptionFromTasksException(
                        TasksError.TSK_INVALID_SESSION, e);
            } catch (final Exception e) {
                logger.error("Error while validate user.", e);
                throw new ApplicationException(
                        ErrorCode.GEN_INTERNAL_ERROR.name(), e);
            } finally {
                try {
                    if (tasksCRUDPool != null && tasksCRUD != null) {
                        tasksCRUDPool.returnObject(tasksCRUD);
                    }
                } catch (final MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("Validate user action end."));
        }
    }
}
