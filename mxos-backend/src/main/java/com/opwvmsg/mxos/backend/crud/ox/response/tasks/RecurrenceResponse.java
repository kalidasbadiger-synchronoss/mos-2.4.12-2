package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.sun.jersey.api.client.ClientResponse;

public class RecurrenceResponse extends Response {

    public Recurrence getRecurrence(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        Logger logger = Logger.getLogger(RecurrenceResponse.class);
        logger.info("Recurrence JsonNode : "+root.toString());
        Recurrence recurrence = JsonToTasksMapper.mapToTaskRecurrence(root);
        return recurrence;
    }
}
