/*
 /*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSPOJOs;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set groupName to the db pojo of requestState.
 *
 * @author mxos-dev
 */
public class SetGroupName implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetGroupName.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SetGroupName action start."));
            }
            requestState.getDbPojoMap().setProperty(
                    MxOSPOJOs.groupName,
                    requestState.getInputParams()
                            .get(MailboxProperty.groupName.name()).get(0));
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SetGroupName action end."));
            }
        } else {
            if (ActionUtils.isCreateMailboxOperation(requestState
                    .getOperationName())) {
                throw new MxOSException(
                        MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
            }
        }
    }
}
