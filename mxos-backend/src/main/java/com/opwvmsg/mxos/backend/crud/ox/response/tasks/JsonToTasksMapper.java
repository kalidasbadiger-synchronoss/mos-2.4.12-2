/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums.BooleanType;
import com.opwvmsg.mxos.data.enums.MxosEnums.TrueOrFalse;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.task.pojos.DaysInWeek;
import com.opwvmsg.mxos.task.pojos.Details;
import com.opwvmsg.mxos.task.pojos.Flags;
import com.opwvmsg.mxos.task.pojos.Folder;
import com.opwvmsg.mxos.task.pojos.Folder.Type;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.opwvmsg.mxos.task.pojos.Participant.ParticipantType;
import com.opwvmsg.mxos.task.pojos.Permissions;
import com.opwvmsg.mxos.task.pojos.Recurrence;
import com.opwvmsg.mxos.task.pojos.Task;
import com.opwvmsg.mxos.task.pojos.TaskBase;
import com.opwvmsg.mxos.task.pojos.TaskBase.Priority;
import com.opwvmsg.mxos.task.pojos.TaskBase.Status;

public class JsonToTasksMapper {

    public static final String UTC_TZ = "UTC";
    private final static String ACTION_STRING = "action";
    private final static String NEW_STRING = "new";
    private final static String MODULE_STRING = "module";
    private final static String TASKS_STRING = "tasks";
    private final static String DATA_STRING = "data";
    private final static String DELETE_STRING = "delete";

    public static String mapFromTaskBase(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksBaseFields(params, map);
        mapFromTasksRecurrenceFields(params, map);

        if (folder != null) {
            map.put(OXTasksProperty.folder_id.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    public static String mapFromTaskFolder(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksFolderFields(params, map);

        //map.put(MODULE_STRING, params.get(TasksProperty.module.name()).get(0));
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    /**
     * Map from Task Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static void mapFromMultipleTask(Map<String, String> params,
            String folder, ArrayNode arrNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(ACTION_STRING, NEW_STRING);
        objNode.put(MODULE_STRING, TASKS_STRING);

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksBaseFields(params, map);

        map.put(OXTasksProperty.folder_id.name(), folder);

        objNode.put(DATA_STRING, objMapper.convertValue(map, JsonNode.class));
        arrNode.add(objNode);

    }

    /**
     * Map from Contact Base to String.
     * 
     * @param base resp.
     * @throws JsonGenerationException.
     * @throws JsonMappingException.
     * @throws IOException.
     * @throws ParseException.
     */
    public static void mapFromMultipleTask(String contactId, String folder,
            ArrayNode arrNode, ObjectMapper objMapper)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {
        ObjectNode objNode = objMapper.createObjectNode();

        objNode.put(ACTION_STRING, DELETE_STRING);
        objNode.put(MODULE_STRING, TASKS_STRING);

        // Add timestamp
        Long updateTime = System.nanoTime();
        objNode.put(OXTasksProperty.timestamp.name(), updateTime.toString());

        Map<String, String> map = new HashMap<String, String>();
        map.put(OXTasksProperty.folder.name(), folder);
        map.put(OXTasksProperty.id.name(), contactId);

        objNode.put(DATA_STRING, objMapper.convertValue(map, JsonNode.class));
        arrNode.add(objNode);

    }

    private static void mapFromTasksBaseFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));

        if (attributes.get(OXTasksProperty.id.name()) != null) {
            map.put(OXTasksProperty.id.name(),
                    attributes.get(OXTasksProperty.id.name()));
        }
        if (attributes.get(OXTasksProperty.title.name()) != null) {
            map.put(OXTasksProperty.title.name(),
                    attributes.get(OXTasksProperty.title.name()));
        }
        if (attributes.get(OXTasksProperty.organizer.name()) != null) {
            map.put(OXTasksProperty.organizer.name(),
                    attributes.get(OXTasksProperty.organizer.name()));
        }
        if (attributes.get(OXTasksProperty.priority.name()) != null) {
            Priority priority = Priority.fromValue(attributes
                    .get(OXTasksProperty.priority.name()));
            map.put(OXTasksProperty.priority.name(),
                    String.valueOf(priority.ordinal() + 1));
        }
        if (attributes.get(OXTasksProperty.status.name()) != null) {
            Status status = Status.fromValue(attributes
                    .get(OXTasksProperty.status.name()));
            map.put(OXTasksProperty.status.name(),
                    String.valueOf(status.ordinal() + 1));
        }
        if (attributes.get(OXTasksProperty.private_flag.name()) != null) {
            Boolean privateFlag = false;
            if (attributes.get(OXTasksProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.YES.name())) {
                privateFlag = true;
            }
            if (attributes.get(OXTasksProperty.private_flag.name())
                    .equalsIgnoreCase(BooleanType.NO.name())) {
                privateFlag = false;
            }
            map.put(OXTasksProperty.private_flag.name(), privateFlag.toString());
        }
        if (attributes.get(OXTasksProperty.color_label.name()) != null) {
            map.put(OXTasksProperty.color_label.name(),
                    attributes.get(OXTasksProperty.color_label.name()));
        }
        if (attributes.get(OXTasksProperty.categories.name()) != null) {
            map.put(OXTasksProperty.categories.name(),
                    attributes.get(OXTasksProperty.categories.name()));
        }
        if (attributes.get(OXTasksProperty.note.name()) != null) {
            map.put(OXTasksProperty.note.name(),
                    attributes.get(OXTasksProperty.note.name()));
        }
        if (attributes.get(OXTasksProperty.start_date.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.start_date.name()));
            map.put(OXTasksProperty.start_date.name(),
                    Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.end_date.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.end_date.name()));
            map.put(OXTasksProperty.end_date.name(),
                    Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.alarm.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.alarm.name()));
            map.put(OXTasksProperty.alarm.name(), Long.toString(date.getTime()));
        }
        if (attributes.get(OXTasksProperty.percent_completed.name()) != null) {
            map.put(OXTasksProperty.percent_completed.name(),
                    attributes.get(OXTasksProperty.percent_completed.name()));
        }
    }
    
    public static String mapFromTaskRecurrence(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksRecurrenceFields(params, map);

        if (folder != null) {
            map.put(OXTasksProperty.folder_id.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    private static void mapFromTasksRecurrenceFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));

        if (attributes.get(OXTasksProperty.recurrence_type.name()) != null) {
            String recuType=attributes.get(OXTasksProperty.recurrence_type.name());
            Recurrence.Type recurType = Recurrence.Type.fromValue(recuType);
            map.put(OXTasksProperty.recurrence_type.name(),Integer.toString(recurType.ordinal()));
        }

        if (attributes.get(OXTasksProperty.days.name()) != null) {
            final String days = attributes.get(OXTasksProperty.days.name());
            final String[] daysArray=days.split(",");
            int daysInWeek=0;
            for(final String day:daysArray){
                daysInWeek+= (int)Math.pow(2, DaysInWeek.fromValue(day).ordinal());
            }
            map.put(OXTasksProperty.days.name(),Integer.toString(daysInWeek));
        }

        if (attributes.get(OXTasksProperty.day_in_month.name()) != null) {
            map.put(OXTasksProperty.day_in_month.name(),attributes.get(OXTasksProperty.day_in_month.name()));
        }

        if (attributes.get(OXTasksProperty.month.name()) != null) {
            map.put(OXTasksProperty.month.name(),attributes.get(OXTasksProperty.month.name()));
        }

        if (attributes.get(OXTasksProperty.interval.name()) != null) {
            map.put(OXTasksProperty.interval.name(),attributes.get(OXTasksProperty.interval.name()));
        }

        if (attributes.get(OXTasksProperty.until.name()) != null) {
            final Date date = simpleDateFormat.parse(attributes
                    .get(OXTasksProperty.until.name()));
            map.put(OXTasksProperty.until.name(),
                    Long.toString(date.getTime()));
        }

        if (attributes.get(OXTasksProperty.notification.name()) != null) {
            map.put(OXTasksProperty.notification.name(),attributes.get(OXTasksProperty.notification.name()));
        }

        if (attributes.get(OXTasksProperty.occurrences.name()) != null) {
            map.put(OXTasksProperty.occurrences.name(),attributes.get(OXTasksProperty.occurrences.name()));
        }
    }
    
    public static String mapFromTaskDetails(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, String> map = new HashMap<String, String>();
        mapFromTasksDetailsFields(params, map);

        if (folder != null) {
            map.put(OXTasksProperty.folder_id.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }

    private static void mapFromTasksDetailsFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        if (attributes.get(OXTasksProperty.actual_costs.name()) != null) {
            String actual_costs=attributes.get(OXTasksProperty.actual_costs.name());
            map.put(OXTasksProperty.actual_costs.name(),actual_costs);
        }

        if (attributes.get(OXTasksProperty.actual_duration.name()) != null) {
            map.put(OXTasksProperty.actual_duration.name(),attributes.get(OXTasksProperty.actual_duration.name()));
        }

        if (attributes.get(OXTasksProperty.billing_information.name()) != null) {
            map.put(OXTasksProperty.billing_information.name(),attributes.get(OXTasksProperty.billing_information.name()));
        }

        if (attributes.get(OXTasksProperty.target_costs.name()) != null) {
            map.put(OXTasksProperty.target_costs.name(),attributes.get(OXTasksProperty.target_costs.name()));
        }

        if (attributes.get(OXTasksProperty.target_duration.name()) != null) {
            map.put(OXTasksProperty.target_duration.name(),attributes.get(OXTasksProperty.target_duration.name()));
        }

        if (attributes.get(OXTasksProperty.currency.name()) != null) {
            map.put(OXTasksProperty.currency.name(),attributes.get(OXTasksProperty.currency.name()));
        }
        if (attributes.get(OXTasksProperty.trip_meter.name()) != null) {
            map.put(OXTasksProperty.trip_meter.name(),attributes.get(OXTasksProperty.trip_meter.name()));
        }
        if (attributes.get(OXTasksProperty.companies.name()) != null) {
            map.put(OXTasksProperty.companies.name(),attributes.get(OXTasksProperty.companies.name()));
        }
    }
    
    public static String mapFromTaskParticipant(Map<String, String> params,
            String folder) throws JsonGenerationException,
            JsonMappingException, IOException, ParseException {

        Map<String, Object> map = new HashMap<String, Object>();
        mapFromTasksParticipantFields(params, map);

        if (folder != null) {
            map.put(OXTasksProperty.folder_id.name(), folder);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(map);
    }


    private static void mapFromTasksParticipantFields(Map<String, String> attributes,
            Map<String, Object> taskBaseMap) throws ParseException {

        List<Map> list = new ArrayList<Map>();

        String[] typeArray = null;
        String[] nameArray = null;
        String[] emailArray = null;

        if (attributes.get(OXTasksProperty.type.name()) != null) {
            final String type = attributes.get(OXTasksProperty.type.name());
            typeArray=type.split(",");
        }

        if (attributes.get(OXTasksProperty.display_name.name()) != null) {
            final String name = attributes.get(OXTasksProperty.type.name());
            nameArray=name.split(",");
        }

        if (attributes.get(OXTasksProperty.mail.name()) != null) {
            final String email = attributes.get(OXTasksProperty.mail.name());
            emailArray=email.split(",");
        }

        if(null != emailArray){
            for(int i=0; i<emailArray.length; i++){
                Map<String, String> map = new HashMap<String, String>();
                map.put(OXTasksProperty.mail.name(),emailArray[i]);
                if(null != typeArray && i < typeArray.length){
                    map.put(OXTasksProperty.type.name(),typeArray[i]);
                }
                if(null != nameArray && i < nameArray.length){
                    map.put(OXTasksProperty.name.name(),nameArray[i]);
                }
                list.add(map);
            }
        }

        taskBaseMap.put("participants",list);
    }
    
    private static void mapFromTasksFolderFields(Map<String, String> attributes,
            Map<String, String> map) throws ParseException {

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));

        if (attributes.get(OXTasksProperty.id.name()) != null) {
            map.put(OXTasksProperty.id.name(),
                    attributes.get(OXTasksProperty.id.name()));
        }
        if (attributes.get(OXTasksProperty.folder_id.name()) != null) {
            map.put(OXTasksProperty.folder_id.name(),
                    attributes.get(OXTasksProperty.folder_id.name()));
        }
        if (attributes.get(OXTasksProperty.title.name()) != null) {
            map.put(OXTasksProperty.title.name(),
                    attributes.get(OXTasksProperty.title.name()));
        }
        if (attributes.get(OXTasksProperty.module.name()) != null) {
            if (attributes.get(OXTasksProperty.module.name()).equalsIgnoreCase(
                    "task")) {
                map.put(OXTasksProperty.module.name(), "tasks");
            }
            if (attributes.get(OXTasksProperty.module.name()).equalsIgnoreCase(
                    "addressBook")) {
                map.put(OXTasksProperty.module.name(), "contacts");
            }
        }
        if (attributes.get(OXTasksProperty.type.name()) != null) {
            map.put(OXTasksProperty.type.name(),
                    attributes.get(OXTasksProperty.type.name()));
        }
        if (attributes.get(OXTasksProperty.subFolders.name()) != null) {
            Boolean haveSubFolders = false;
            if (attributes.get(OXTasksProperty.subFolders.name())
                    .equalsIgnoreCase(BooleanType.YES.name())) {
                haveSubFolders = true;
            }
            if (attributes.get(OXTasksProperty.subFolders.name())
                    .equalsIgnoreCase(BooleanType.NO.name())) {
                haveSubFolders = false;
            }
            map.put(OXTasksProperty.subFolders.name(), haveSubFolders.toString());
        }
        if (attributes.get(OXTasksProperty.own_rights.name()) != null) {
            map.put(OXTasksProperty.own_rights.name(),
                    attributes.get(OXTasksProperty.own_rights.name()));
        }
        if (attributes.get(OXTasksProperty.summary.name()) != null) {
            map.put(OXTasksProperty.summary.name(),
                    attributes.get(OXTasksProperty.summary.name()));
        }
        if (attributes.get(OXTasksProperty.standard_folder.name()) != null) {
            Boolean standardFolders = false;
            if (attributes.get(OXTasksProperty.standard_folder.name())
                    .equalsIgnoreCase(BooleanType.YES.name())) {
                standardFolders = true;
            }
            if (attributes.get(OXTasksProperty.standard_folder.name())
                    .equalsIgnoreCase(BooleanType.NO.name())) {
                standardFolders = false;
            }
            map.put(OXTasksProperty.standard_folder.name(), standardFolders.toString());
        }
        if (attributes.get(OXTasksProperty.total.name()) != null) {
            map.put(OXTasksProperty.total.name(),
                    attributes.get(OXTasksProperty.total.name()));
        }
        if (attributes.get(OXTasksProperty.newObjects.name()) != null) {
            map.put(OXTasksProperty.newObjects.name(),
                    attributes.get(OXTasksProperty.newObjects.name()));
        }
        if (attributes.get(OXTasksProperty.unread.name()) != null) {
            map.put(OXTasksProperty.unread.name(),
                    attributes.get(OXTasksProperty.unread.name()));
        }
        if (attributes.get(OXTasksProperty.deleted.name()) != null) {
            map.put(OXTasksProperty.deleted.name(),
                    attributes.get(OXTasksProperty.deleted.name()));
        }
        if (attributes.get(OXTasksProperty.capabilities.name()) != null) {
            map.put(OXTasksProperty.capabilities.name(),
                    attributes.get(OXTasksProperty.capabilities.name()));
        }
        if (attributes.get(OXTasksProperty.standard_folder_type.name()) != null) {
            map.put(OXTasksProperty.standard_folder_type.name(),
                    attributes.get(OXTasksProperty.standard_folder_type.name()));
        }
        if (attributes.get(OXTasksProperty.supported_capabilities.name()) != null) {
            map.put(OXTasksProperty.supported_capabilities.name(),
                    attributes.get(OXTasksProperty.supported_capabilities.name()));
        }
        
    }
    
    public static Folder mapToTaskFolder(JsonNode root) {
        Folder folder = new Folder();
        mapToTaskFolderFields(root, folder);
        return folder;
    }

    private static void mapToTaskFolderFields(JsonNode root, Folder folder) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        Permissions permissions = new Permissions();
        Flags flags = new Flags();
        if (root.path(OXTasksProperty.id.name()).isTextual()) {
            Integer id = root.path(OXTasksProperty.id.name()).asInt();
            folder.setFolderId(String.valueOf(id));
        } else {
            folder.setFolderId(root.asText());
        }
        if (root.path(OXTasksProperty.title.name()).isTextual()) {
            folder.setDisplayName(root.path(OXTasksProperty.title.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.module.name()).isTextual()) {
            folder.setModule(root.path(OXTasksProperty.module.name()).asText());
        }
        if (root.path(OXTasksProperty.type.name()).isInt()) {
            int type = root.path(OXTasksProperty.type.name()).asInt();
            if (type == 1) {
                folder.setType(Type.PRIVATE);
            } else if (type == 2) {
                folder.setType(Type.PUBLIC);
            } else if (type == 3) {
                folder.setType(Type.SHARED);
            } else if (type == 5) {
                folder.setType(Type.SYSTEM);
            }

        }
        if (root.path(OXTasksProperty.subFolders.name()).isBoolean()) {
            if (root.path(OXTasksProperty.subFolders.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                folder.setHaveSubFolders(BooleanType.YES);
            } else if (root.path(OXTasksProperty.subFolders.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.FALSE)) {
                folder.setHaveSubFolders(BooleanType.NO);
            }
        }
        if (root.path(OXTasksProperty.own_rights.name()).isInt()) {
            folder.setOwnRights(root.path(OXTasksProperty.own_rights.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.permissions.name()).isArray()) {
            Iterator<JsonNode> tempNodeIter = root.path(
                    OXTasksProperty.permissions.name()).getElements();

            Iterator<JsonNode> arryNode = tempNodeIter.next().getElements();
            JsonNode entity = arryNode.next();
            if (entity.isInt()) {
                permissions.setEntity(entity.asInt());
            }
            JsonNode bits = arryNode.next();
            if (bits.isInt()) {
                permissions.setBits(bits.asInt());
            }
            JsonNode group = arryNode.next();
            if (group.isBoolean()) {
                if (group.asText().equalsIgnoreCase(MxOSConstants.TRUE)) {
                    permissions.setGroup(BooleanType.YES);
                } else if (group.asText().equalsIgnoreCase(MxOSConstants.FALSE)) {
                    permissions.setGroup(BooleanType.NO);
                }
            }

            folder.setPermissions(permissions);
        }
        if (root.path(OXTasksProperty.summary.name()).isTextual()) {
            folder.setDescription(root.path(OXTasksProperty.summary.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.standard_folder.name()).isBoolean()) {
            if (root.path(OXTasksProperty.standard_folder.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                folder.setIsDefault(BooleanType.YES);
            } else if (root.path(OXTasksProperty.standard_folder.name())
                    .asText().equalsIgnoreCase(MxOSConstants.FALSE)) {
                folder.setIsDefault(BooleanType.NO);
            }
        }
        if (root.path(OXTasksProperty.total.name()).isInt()) {
            folder.setNumTotalObjects(Integer.parseInt(root.path(
                    OXTasksProperty.total.name()).asText()));
        }
        if (root.path(OXTasksProperty.newObjects.name()).isInt()) {
            folder.setNumNewObjects(Integer.parseInt(root.path(
                    OXTasksProperty.newObjects.name()).asText()));
        }
        if (root.path(OXTasksProperty.unread.name()).isInt()) {
            folder.setNumUnreadObjects(Integer.parseInt(root.path(
                    OXTasksProperty.unread.name()).asText()));
        }
        if (root.path(OXTasksProperty.deleted.name()).isInt()) {
            folder.setNumDeletedObjects(Integer.parseInt(root.path(
                    OXTasksProperty.deleted.name()).asText()));
        }
        if (root.path(OXTasksProperty.capabilities.name()).isTextual()) {
            folder.setCapabilities(root.path(
                    OXTasksProperty.capabilities.name()).asText());
        }
        if (root.path(OXTasksProperty.standard_folder_type.name()).isTextual()) {
            folder.setStandardFolderType(root.path(
                    OXTasksProperty.standard_folder_type.name()).asText());
        }
        if (root.path(OXTasksProperty.supported_capabilities.name()).isArray()) {
            Iterator<JsonNode> tempNodeIter = root.path(
                    OXTasksProperty.supported_capabilities.name())
                    .getElements();
         
            List<String> listOfSupportedCapabilities = new ArrayList<String>();
            while (tempNodeIter.hasNext()) {
                JsonNode supportedCapabilites = tempNodeIter.next();
                listOfSupportedCapabilities.add(supportedCapabilites.asText());
            }
            folder.setSupportedCapabilities(listOfSupportedCapabilities);
        }
        if (root.path(OXTasksProperty.subscribed.name()).isBoolean()) {
            if (root.path(OXTasksProperty.subscribed.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                flags.setHidenFlag(BooleanType.YES);
            } else if (root.path(OXTasksProperty.subscribed.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.FALSE)) {
                flags.setHidenFlag(BooleanType.NO);
            }
        }
        if (root.path(OXTasksProperty.subscr_subflds.name()).isBoolean()) {
            if (root.path(OXTasksProperty.subscr_subflds.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                flags.setHidenSubFoldersFlag(BooleanType.YES);
            } else if (root.path(OXTasksProperty.subscr_subflds.name())
                    .asText().equalsIgnoreCase(MxOSConstants.FALSE)) {
                flags.setHidenSubFoldersFlag(BooleanType.NO);
            }
        }
        if (root.path(OXTasksProperty.publicationFlag.name()).isBoolean()) {
            if (root.path(OXTasksProperty.publicationFlag.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                flags.setPublicationFlag(BooleanType.YES);
            } else if (root.path(OXTasksProperty.publicationFlag.name())
                    .asText().equalsIgnoreCase(MxOSConstants.FALSE)) {
                flags.setPublicationFlag(BooleanType.NO);
            }
        }
        if (root.path(OXTasksProperty.subscriptionFlag.name()).isBoolean()) {
            if (root.path(OXTasksProperty.subscriptionFlag.name()).asText()
                    .equalsIgnoreCase(MxOSConstants.TRUE)) {
                flags.setSubscriptionFlag(BooleanType.YES);
            } else if (root.path(OXTasksProperty.subscriptionFlag.name())
                    .asText().equalsIgnoreCase(MxOSConstants.FALSE)) {
                flags.setSubscriptionFlag(BooleanType.NO);
            }
        }
        if (flags.getHidenFlag() != null
                || flags.getHidenSubFoldersFlag() != null
                || flags.getPublicationFlag() != null
                || flags.getSubscriptionFlag() != null) {
            folder.setFlags(flags);
        }
    }

    public static String mapFromFolderIds(List<String> folderIds)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {
        ArrayList<String> task = new ArrayList<String>();
        for (String folderId : folderIds) {
            task.add(folderId);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(task);
    }
    public static TaskBase mapToTaskBase(JsonNode root) {
        TaskBase taskBase = new TaskBase();
        mapToTaskBaseFields(root, taskBase);
        return taskBase;
    }

    private static void mapToTaskBaseFields(JsonNode root, TaskBase taskBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        if (root.path(OXTasksProperty.id.name()).isInt()) {
            Integer id = root.path(OXTasksProperty.id.name()).asInt();
            taskBase.setTaskId(String.valueOf(id));
        }
        if (root.path(OXTasksProperty.folder_id.name()).isInt()) {
            taskBase.setFolderId(root.path(OXTasksProperty.folder_id.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.title.name()).isTextual()) {
            taskBase.setName(root.path(OXTasksProperty.title.name()).asText());
        }
        if (root.path(OXTasksProperty.priority.name()).isInt()) {
            taskBase.setPriority(Priority.fromOrdinal(root.path(
                    OXTasksProperty.priority.name()).asInt() - 1));
        }
        if (root.path(OXTasksProperty.status.name()).isInt()) {
            taskBase.setStatus(Status.fromOrdinal(root.path(
                    OXTasksProperty.status.name()).asInt() - 1));
        }
        if (root.path(OXTasksProperty.private_flag.name()).isBoolean()) {
            Boolean isPrivate = root.path(OXTasksProperty.private_flag.name())
                    .asBoolean();
            taskBase.setIsPrivate(isPrivate ? BooleanType.YES : BooleanType.NO);
        }
        if (root.path(OXTasksProperty.color_label.name()).isInt()) {
            taskBase.setColorLabel(root
                    .path(OXTasksProperty.color_label.name()).asInt());
        }
        if (root.path(OXTasksProperty.categories.name()).isTextual()) {
            taskBase.setCategories(root.path(OXTasksProperty.categories.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.note.name()).isTextual()) {
            taskBase.setNotes(root.path(OXTasksProperty.note.name()).asText());
        }
        if (root.path(OXTasksProperty.creation_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.creation_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCreatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.last_modified.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.last_modified.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setUpdatedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.start_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.start_date.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setStartDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.end_date.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.end_date.name()).asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setDueDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.date_completed.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.date_completed.name())
                    .asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setCompletedDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.alarm.name()).isLong()) {
            Long dateLong = root.path(OXTasksProperty.alarm.name()).asLong();
            Date dateObj = new Date(dateLong);
            taskBase.setReminderDate(simpleDateFormat.format(dateObj));
        }
        if (root.path(OXTasksProperty.percent_completed.name()).isInt()) {
            taskBase.setProgress(root.path(
                    OXTasksProperty.percent_completed.name()).asInt());
        }
    }

    private static void columnsToTaskBaseFields(JsonNode root, TaskBase taskBase) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        Iterator<JsonNode> tempNodeIter = root.getElements();
        JsonNode node = tempNodeIter.next();
        if (node.isInt()) {
            // Set TaskId
            taskBase.setTaskId(String.valueOf(node.asInt()));
        }
        node = tempNodeIter.next();
        if (node.isInt()) {
            // Set folderId
            taskBase.setFolderId(String.valueOf(node.asInt()));
        }
        node = tempNodeIter.next();
        if (node.isTextual()) {
            // Set Name
            taskBase.setName(String.valueOf(node.asText()));
        }
        node = tempNodeIter.next();
        if (node.isInt()) {
            // Set Priority
            taskBase.setPriority(Priority.fromOrdinal(node.asInt() - 1));
        }
        /*
         * if (root.path(OXTasksProperty.status.name()).isInt()) {
         * taskBase.setStatus(Status.fromOrdinal(root.path(
         * OXTasksProperty.status.name()).asInt() - 1)); } if
         * (root.path(OXTasksProperty.private_flag.name()).isBoolean()) {
         * Boolean isPrivate = root.path(OXTasksProperty.private_flag.name())
         * .asBoolean(); taskBase.setIsPrivate(isPrivate ? BooleanType.YES :
         * BooleanType.NO); } if
         * (root.path(OXTasksProperty.color_label.name()).isInt()) {
         * taskBase.setColorLabel(root
         * .path(OXTasksProperty.color_label.name()).asInt()); } if
         * (root.path(OXTasksProperty.categories.name()).isTextual()) {
         * taskBase.setCategories(root.path(OXTasksProperty.categories.name())
         * .asText()); } if (root.path(OXTasksProperty.note.name()).isTextual())
         * { taskBase.setNotes(root.path(OXTasksProperty.note.name()).asText());
         * } if (root.path(OXTasksProperty.creation_date.name()).isLong()) {
         * Long dateLong = root.path(OXTasksProperty.creation_date.name())
         * .asLong(); Date dateObj = new Date(dateLong);
         * taskBase.setCreatedDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.last_modified.name()).isLong()) { Long
         * dateLong = root.path(OXTasksProperty.last_modified.name()) .asLong();
         * Date dateObj = new Date(dateLong);
         * taskBase.setUpdatedDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.start_date.name()).isLong()) { Long
         * dateLong = root.path(OXTasksProperty.start_date.name()) .asLong();
         * Date dateObj = new Date(dateLong);
         * taskBase.setStartDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.end_date.name()).isLong()) { Long dateLong
         * = root.path(OXTasksProperty.end_date.name()) .asLong(); Date dateObj
         * = new Date(dateLong);
         * taskBase.setDueDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.date_completed.name()).isLong()) { Long
         * dateLong = root.path(OXTasksProperty.date_completed.name())
         * .asLong(); Date dateObj = new Date(dateLong);
         * taskBase.setCompletedDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.alarm.name()).isLong()) { Long dateLong =
         * root.path(OXTasksProperty.alarm.name()) .asLong(); Date dateObj = new
         * Date(dateLong);
         * taskBase.setReminderDate(simpleDateFormat.format(dateObj)); } if
         * (root.path(OXTasksProperty.percent_completed.name()).isInt()) {
         * taskBase.setProgress(root.path(
         * OXTasksProperty.percent_completed.name()).asInt()); }
         */
    }

    public static List<String> mapToAllTaskIds(JsonNode root)
            throws TasksException {
        List<String> tasksList = new ArrayList<String>();

        Iterator<JsonNode> arrayIter = root.getElements();
        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();
            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();

            JsonNode isTaskNode = tempNodeIter.next();
            if (isTaskNode.isInt()) {
                // Set TaskId
                tasksList.add(String.valueOf(isTaskNode.asInt()));
            }
        }
        return tasksList;
    }

    public static List<String> mapToAllFolderIds(JsonNode root)
            throws TasksException {
        List<String> foldersList = new ArrayList<String>();

        Iterator<JsonNode> arrayIter = root.getElements();
        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();
            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();
            while (tempNodeIter.hasNext()) {
                JsonNode tempArrayElements = tempNodeIter.next();
                Iterator<JsonNode> tempNodeIters = tempArrayElements
                        .getElements();
                JsonNode isTaskFolderNode = tempNodeIters.next();
                JsonNode isDefault = tempNodeIters.next();
                Boolean isDefaultFolder = false;
                if (isDefault.isBoolean()) {
                    isDefaultFolder = isDefault.asBoolean();
                }
                JsonNode folderTypeNode = tempNodeIters.next();
                int folderType = 0;
                if (folderTypeNode.isInt()) {
                    folderType = folderTypeNode.asInt();
                }
                if (isTaskFolderNode.isTextual()
                        && (!isDefaultFolder && folderType != 5)) {
                    // Set folderId
                    foldersList.add(String.valueOf(isTaskFolderNode.asInt()));
                }
            }
        }
        return foldersList;
    }
    
    public static void mapToMultipleTasks(JsonNode root, List<String> taskIdList)
            throws TasksException {

        Iterator<JsonNode> arrayIter = root.getElements();
        String message = null;
        String category = null;
        String code = null;
        String errorId = null;
        boolean exceptionFound = false;
        int errorIndex = 0;
        int noOfErrors = 0;

        // index of the element
        int index = 0;

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();

            if (!tempArrayElement.path(OXTasksProperty.data.name())
                    .isMissingNode()) {
                JsonNode temp = tempArrayElement.path(OXTasksProperty.data
                        .name());

                if (temp.path(OXTasksProperty.id.name()).isInt()) {
                    Integer id = temp.path(OXTasksProperty.id.name()).asInt();
                    taskIdList.add(String.valueOf(id));
                }
            } else if (!tempArrayElement.path(OXTasksProperty.error.name())
                    .isMissingNode()) {

                int i = 0;
                JsonNode errorParams = tempArrayElement
                        .path(OXTasksProperty.error_params.name());
                Object[] parameters = new Object[errorParams.size()];

                for (JsonNode node : errorParams) {
                    parameters[i++] = node.asText();
                }

                if (tempArrayElement.path(OXTasksProperty.error.name())
                        .isTextual()) {
                    message = tempArrayElement.path(
                            OXTasksProperty.error.name()).asText();

                    message = String.format(message.replaceAll("\\$d", "\\$s"),
                            parameters);
                }

                if (tempArrayElement.path(OXTasksProperty.category.name())
                        .isTextual()) {
                    category = tempArrayElement.path(
                            OXTasksProperty.category.name()).asText();
                }
                if (tempArrayElement.path(OXTasksProperty.code.name())
                        .isTextual()) {
                    code = tempArrayElement.path(OXTasksProperty.code.name())
                            .asText();
                }
                if (tempArrayElement.path(OXTasksProperty.error_id.name())
                        .isTextual()) {
                    errorId = tempArrayElement.path(
                            OXTasksProperty.error_id.name()).asText();
                }
                exceptionFound = true;
                errorIndex = index;
                noOfErrors++;
            }

            index++;
        }

        if (exceptionFound) {
            throw new TasksException("Creation of row : " + errorIndex
                    + " failed with " + message, category, code, errorId);
        }

    }

    public static String mapFromTaskIds(List<String> taskIds, String folder)
            throws JsonGenerationException, JsonMappingException, IOException,
            ParseException {

        List<Map<String, String>> tasksList = new ArrayList<Map<String, String>>();
        Map<String, String> task;
        for (String taskId : taskIds) {
            task = new HashMap<String, String>();
            task.put(OXTasksProperty.folder.name(), folder);
            task.put(OXTasksProperty.id.name(), taskId);
            tasksList.add(task);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tasksList);
    }



    public static String mapFromTaskParticipantIds(List<String> taskParticipantIds, String folder)
    throws JsonGenerationException, JsonMappingException, IOException,
    ParseException {

        List<Map<String, String>> tasksList = new ArrayList<Map<String, String>>();
        Map<String, String> task;
        for (String taskId : taskParticipantIds) {
            task = new HashMap<String, String>();
            task.put(OXTasksProperty.folder.name(), folder);
            task.put(OXTasksProperty.id.name(), taskId);
            tasksList.add(task);
        }

        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(tasksList);
    }

    public static List<Task> mapToAllTasks(JsonNode root) throws TasksException {

        List<Task> tasksList = new ArrayList<Task>();
        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            Task task = new Task();
            TaskBase base = new TaskBase();
            task.setTaskBase(base);

            columnsToTaskBaseFields(arrayIter.next(), base);

            tasksList.add(task);
        }
        return tasksList;
    }

    public static List<TaskBase> mapToListTaskBase(JsonNode root) throws TasksException {

        List<TaskBase> taskBaseList = new ArrayList<TaskBase>();
        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            TaskBase base = new TaskBase();
            columnsToTaskBaseFields(arrayIter.next(), base);
            taskBaseList.add(base);
        }
        return taskBaseList;
    }

    public static Recurrence mapToTaskRecurrence(JsonNode root) {
        Recurrence recurrence = new Recurrence();
        mapToTaskRecurrenceFields(root, recurrence);
        return recurrence;
    }

    private static void mapToTaskRecurrenceFields(JsonNode root,
            Recurrence recurrence) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));

        if (root.path(OXTasksProperty.recurrence_type.name()).isInt()) {
            recurrence.setType(Recurrence.Type.fromOrdinal(root.path(
                    OXTasksProperty.recurrence_type.name()).asInt()));
        }

        if (root.path(OXTasksProperty.days.name()).isInt()) {
            String days = root.path(OXTasksProperty.days.name()).asText();

            int b[] = new int[10];
            int a = Integer.parseInt(days);

            int i = 0;
            while (a != 0) {
                b[i] = a & 1;
                a = a >> 1;
                i++;
            }

            List<DaysInWeek> daysIneekList = new ArrayList<DaysInWeek>();
            for (int j = i; j > 0; j--) {
                if (b[j - 1] == 1) {
                    DaysInWeek tmp = DaysInWeek.fromOrdinal(j - 1);
                    daysIneekList.add(tmp);

                }
            }
            recurrence.setDaysInWeek(daysIneekList);
        }

        if (root.path(OXTasksProperty.day_in_month.name()).isInt()) {
            recurrence.setDaysInMonth(root.path(
                    OXTasksProperty.day_in_month.name()).asInt());
        }

        if (root.path(OXTasksProperty.month.name()).isInt()) {
            recurrence.setMonthInYear(root.path(OXTasksProperty.month.name())
                    .asInt());
        }
        if (root.path(OXTasksProperty.interval.name()).isInt()) {
            recurrence.setRecurAfterInterval((root
                    .path(OXTasksProperty.interval.name()).asInt()));
        }
        if (root.path(OXTasksProperty.until.name()).isTextual()) {
            recurrence.setEndDate(root.path(OXTasksProperty.until.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.notification.name()).isTextual()) {
            String notificationValue=root.path(OXTasksProperty.notification.name()).asText();
            recurrence.setNotification(TrueOrFalse.valueOf(notificationValue));
        }
        if (root.path(OXTasksProperty.occurrences.name()).isInt()) {
            recurrence.setOccurrences(root.path(
                    OXTasksProperty.occurrences.name()).asInt());
        }
    }

    public static Participant mapToTaskParticipant(JsonNode root) {
        Participant participant = new Participant();
        mapToTaskParticipantFields(root, participant);
        return participant;
    }

    private static void mapToTaskParticipantFields(JsonNode root, Participant participant) {
        if (root.path(OXTasksProperty.userId.name()).isInt()) {
            participant.setParticipantId(String.valueOf(root.path(OXTasksProperty.userId.name()).asInt()));
        }

        if (root.path(OXTasksProperty.type.name()).isInt()) {
            Integer id = root.path(OXTasksProperty.type.name()).asInt();
            Logger logger = Logger.getLogger(JsonToTasksMapper.class);
            logger.info("Participant id : "+id);

            ParticipantType participantType=ParticipantType.fromOrdinal(id-1);
            logger.info("Participant participantType : "+participantType);
            participant.setParticipantType(participantType);
        }

        if (root.path(OXTasksProperty.display_name.name()).isTextual()) {
            participant.setParticipantName(root.path(OXTasksProperty.display_name.name())
                    .asText());
        }
        if (root.path(OXTasksProperty.mail.name()).isTextual()) {
            participant.setParticipantEmail(root.path(OXTasksProperty.mail.name()).asText());
        }
        if (root.path(OXTasksProperty.confirmation.name()).isTextual()) {
            participant.setParticipantMessage((root.path(
                    OXTasksProperty.confirmation.name()).asText()));
        }

        if (root.path(OXTasksProperty.confirm_message.name()).isTextual()) {
            participant.setParticipantMessage((root.path(
                    OXTasksProperty.confirm_message.name()).asText()));
        }
    }
    
    public static Details mapToTaskDetails(JsonNode root) {
        Details taskDetails = new Details();
        mapToTaskDetailsFields(root, taskDetails);
        return taskDetails;
    }

    private static void mapToTaskDetailsFields(JsonNode root, Details taskDetails) {
         
        if (root.path(OXTasksProperty.actual_costs.name()).isInt()) {
            taskDetails.setActualCost(String.valueOf(root.path(OXTasksProperty.actual_costs.name()).asInt()));
        }
        
        if (root.path(OXTasksProperty.actual_duration.name()).isTextual()) {
            taskDetails.setActualWork(root.path(OXTasksProperty.actual_duration.name())
                    .asText());
        }
        
        if (root.path(OXTasksProperty.billing_information.name()).isTextual()) {
            taskDetails.setBilling(root.path(OXTasksProperty.billing_information.name()).asText());
        }
        
        if (root.path(OXTasksProperty.target_costs.name()).isInt()) {
            taskDetails.setTargetCost(String.valueOf(root.path(OXTasksProperty.target_costs.name()).asInt()));
        }
        
        if (root.path(OXTasksProperty.target_duration.name()).isTextual()) {
            taskDetails.setTargetWork(root.path(OXTasksProperty.target_duration.name()).asText());
        }
        
        if (root.path(OXTasksProperty.currency.name()).isTextual()) {
            taskDetails.setCurrency(root.path(OXTasksProperty.currency.name()).asText());
        }
        
        if (root.path(OXTasksProperty.trip_meter.name()).isTextual()) {
            taskDetails.setMileage(root
                    .path(OXTasksProperty.trip_meter.name()).asText());
        }
        
        if (root.path(OXTasksProperty.companies.name()).isTextual()) {
            taskDetails.setCompanies(root
                    .path(OXTasksProperty.companies.name()).asText());
        }
    }
    
    public static List<Folder> mapToAllFolders(JsonNode root)
            throws TasksException {

        List<Folder> foldersList = new ArrayList<Folder>();
        Iterator<JsonNode> arrayIter = root.getElements();

        while (arrayIter.hasNext()) {
            JsonNode tempArrayElement = arrayIter.next();
            Iterator<JsonNode> tempNodeIter = tempArrayElement.getElements();
            while (tempNodeIter.hasNext()) {
                Folder folder = new Folder();

                columnsToFolderFields(tempNodeIter.next(), folder);

                foldersList.add(folder);
            }
        }
        return foldersList;
    }
    
    private static void columnsToFolderFields(JsonNode root, Folder folder) {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
        simpleDateFormat.applyLocalizedPattern(System
                .getProperty(SystemProperty.userDateFormat.name()));
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(UTC_TZ));
        Iterator<JsonNode> tempNodeIter = root.getElements();
        JsonNode node = tempNodeIter.next();
        if (node.isTextual()) {
            // Set folderId
            folder.setFolderId(node.asText());
        }
        node = tempNodeIter.next();
        if (node.isBoolean()) {
            // Set is default folder
            if (node.asBoolean()) {
                folder.setIsDefault(BooleanType.YES);
            } else {
                folder.setIsDefault(BooleanType.NO);
            }
        }
        node = tempNodeIter.next();
        if (node.isInt()) {
            // Set type
            if (node.asInt() == 1) {
                folder.setType(Type.PRIVATE);
            } else if (node.asInt() == 2) {
                folder.setType(Type.PUBLIC);
            } else if (node.asInt() == 3) {
                folder.setType(Type.SHARED);
            } else if (node.asInt() == 5) {
                folder.setType(Type.SYSTEM);
            }
        }
        node = tempNodeIter.next();
        if (node.isTextual()) {
            // Set Name
            folder.setTitle(node.asText());
        }
        node = tempNodeIter.next();
        if (node.isTextual()) {
            // Set module
            folder.setModule(node.asText());
        }
        
    }
}
