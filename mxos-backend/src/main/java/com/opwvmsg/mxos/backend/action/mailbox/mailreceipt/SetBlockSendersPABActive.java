/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set block senders pab active.
 *
 * @author mxos-dev
 */
public class SetBlockSendersPABActive implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetBlockSendersPABActive.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetBlockSendersPABActive action start."));
        }
        String blockSendersPABActive = requestState.getInputParams()
                .get(MailboxProperty.blockSendersPABActive.name()).get(0);

        try {
            if (blockSendersPABActive != null
                    && !blockSendersPABActive.equals("")) {
                blockSendersPABActive = Integer.toString(MxosEnums.BooleanType
                        .fromValue(blockSendersPABActive).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.blockSendersPABActive,
                            blockSendersPABActive);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set block senders PAB active.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_BLOCK_SENDERS_PAB_ACTIVE
                            .name(),
                    e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetBlockSendersPABActive action end."));
        }
    }
}
