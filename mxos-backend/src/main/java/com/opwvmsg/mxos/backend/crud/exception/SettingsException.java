package com.opwvmsg.mxos.backend.crud.exception;

public class SettingsException extends ComponentException {

    private static final long serialVersionUID = 8634793376715923619L;

    public SettingsException(Exception e) {
        super(e);
    }

    public SettingsException(String message) {
        super(message);
    }

    public SettingsException(String message, Exception e) {
        super(message, e);
    }

    public SettingsException(String message, String category, String code,
            String errorId) {
        super(message);
    }

    public SettingsException(String message, String category, String code,
            String errorId, Exception e) {
        super(message, e);
    }
}
