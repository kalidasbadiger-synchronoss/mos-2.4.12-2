/*
 * Copyright (c) 2015 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Messaging Inc. The software may be used and/or copied only
 * with the written permission of Openwave Messaging Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.crud.ldap;

import java.util.Hashtable;

import javax.naming.directory.DirContext;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.log4j.Logger;

import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * This is a base class of any LDAP client object pool factory used in mOS. The
 * constructor reads mOS configuration and builds environment that is used to
 * create initial DirContext.
 * 
 * @param <GenericLdapClientClass> The class the factory uses for creating pool
 *            objects. The class can be anything. However, it should be
 *            basically meant for LDAP access, such as LdapMailboxCRUD,
 *            InitialLdapContext, etc.
 */
public abstract class LdapClientFactory<GenericLdapClientClass> extends
        BasePoolableObjectFactory<GenericLdapClientClass> {
    
    protected static Logger logger =
            Logger.getLogger(LdapClientFactory.class);
    
    protected Hashtable<String, String> env;
    protected int activeHostIndex = 0;
    protected int numHosts = 0;
    protected String[] hosts;
    protected String[] urls;

    @Override
    public abstract GenericLdapClientClass makeObject() throws Exception;
    
    /**
     * Make LDAP server list by factory's own strategy.
     * 
     * @return LDAP server list.  The format of each entry should be <host_name>[:<port>]
     * @throws MxOSException 
     */
    abstract String[] getLdapServers() throws MxOSException;
    
    LdapClientFactory() throws MxOSException {
        env = getConfigParams();
    }
    
    /**
     * Reads the Configuration Parameters and store in the env.
     * 
     * @return
     * @throws MxOSException 
     */
    protected Hashtable<String, String> getConfigParams() throws MxOSException {

        env = new Hashtable<String, String>();
        
        String defaultHost = MxOSConfig.getDefaultHost();
        String defaultDirServer = MxOSConfig.getDefaultDirectoryServer();

        String userDn = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_DN,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_DN));

        String password = MxOSConfig.getString(defaultHost, defaultDirServer,
                MxOSConfig.Keys.DEFAULT_BIND_PASSWORD,
                MxOSConfig.getDefault(MxOSConfig.Keys.DEFAULT_BIND_PASSWORD));

        loadLdapHosts();
        getActiveLdapHost();

        env.put(DirContext.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(DirContext.SECURITY_AUTHENTICATION, "simple");
        env.put(DirContext.SECURITY_PRINCIPAL, userDn);
        env.put(DirContext.SECURITY_CREDENTIALS, password);
        System.setProperty("com.sun.jndi.ldap.connect.pool", "false");
        if (logger.isDebugEnabled()) {
            logger.debug("ldap Connect timeout: " + MxOSConfig.getLdapConnectTimeout());
        }
        env.put("com.sun.jndi.ldap.read.timeout", String.valueOf(MxOSConfig.getLdapReadTimeout()));
        env.put("com.sun.jndi.ldap.connect.timeout", String.valueOf(MxOSConfig.getLdapConnectTimeout()));
        
        return env;
    }

    /**
     * Reads the LDAP Hosts information from Configuration
     * @throws MxOSException 
     */
    protected void loadLdapHosts() throws MxOSException {
        String ldap = "ldap://";
        
        hosts = getLdapServers();
        
        numHosts = hosts.length;
        
        urls = new String[hosts.length];
        for (int i = 0; i < hosts.length; i++) {
            if (!hosts[i].startsWith(ldap)) {
                urls[i] = ldap + hosts[i];
            }
        }
    }

    /**
     * Gets the LDAP URL of the active Host
     */
    protected void getActiveLdapHost() {
        if(logger.isDebugEnabled()) {
            logger.debug("LdapConnectionPool:getLdapUrl");
            logger.debug("activeHostIndex value is : " + activeHostIndex);
            logger.debug("Ldap url is : " + urls[activeHostIndex]);
        }
        env.put(DirContext.PROVIDER_URL, urls[activeHostIndex]);
        env.put(MxOSConfig.PROVIDER_HOST,
                hosts[activeHostIndex].split(MxOSConstants.COLON)[0]);
        activeHostIndex++;
        if (activeHostIndex >= hosts.length) {
            activeHostIndex = 0 ;
        }       
    }

    /**
     * @return the numHosts
     */
    public int getNumHosts() {
        return numHosts;
    }

    /**
     * @return the env
     * 
     *         TODO: Exposing env to class users is not a good idea as tracking
     *         env changes would be difficult if you do (and actually we do in
     *         LdapMailboxCRUD). Also, class users may break the env that could
     *         prevent created LDAP clients from being operational. Any methods
     *         that changes env should be moved in this class or subclass.
     *         Furthermore, we may want to avoid changing env during operation.
     *         We lose track of number of current connections for each LDAP
     *         server in servers list since env is changing.
     */
    public Hashtable<String, String> getEnv() {
        return env;
    }

}
