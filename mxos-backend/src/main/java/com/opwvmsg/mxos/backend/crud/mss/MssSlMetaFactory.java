/*
 * Copyright (c) 2011 Openwave Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */

package com.opwvmsg.mxos.backend.crud.mss;

import org.apache.commons.pool.BasePoolableObjectFactory;

/**
 * Stateless Mss connection pool factory to access metadata.
 * This is required by Apache object pool.
 *
 * @author mxos-dev
 */
class MssSlMetaFactory extends BasePoolableObjectFactory<MssSlMetaCRUD> {

    /**
     * Constructor.
     */
    public MssSlMetaFactory() {
    }

    @Override
    public MssSlMetaCRUD makeObject() throws Exception {
        return new MssSlMetaCRUD();
    }

    @Override
    public void destroyObject(MssSlMetaCRUD mssSlMetaCRUD) throws Exception {
        mssSlMetaCRUD.close();
    }
}
