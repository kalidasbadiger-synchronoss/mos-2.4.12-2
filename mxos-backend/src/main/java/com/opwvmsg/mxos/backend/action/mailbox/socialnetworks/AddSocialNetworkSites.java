/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.socialnetworks;

import java.util.List;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.ActionUtils;
import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.crud.ICRUDPool;
import com.opwvmsg.mxos.backend.crud.IMailboxCRUD;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.SystemProperty;
import com.opwvmsg.mxos.data.pojos.SocialNetworkSite;
import com.opwvmsg.mxos.data.pojos.SocialNetworks;
import com.opwvmsg.mxos.error.ErrorCode;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set locale.
 * 
 * @author mxos-dev
 */
public class AddSocialNetworkSites implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(AddSocialNetworkSites.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddSocialNetworkSites action start."));
        }
        ICRUDPool<IMailboxCRUD> mailboxCRUDPool = null;
        IMailboxCRUD provCRUD = null;
        String email = requestState.getInputParams()
                .get(MailboxProperty.email.name()).get(0);
        String socialNetworkSite = requestState.getInputParams()
                .get(MailboxProperty.socialNetworkSite.name()).get(0);
        List<String> socialNetworkSiteAccessEnabledList = requestState
                .getInputParams().get(
                        MailboxProperty.socialNetworkSiteAccessEnabled.name());
        String socialNetworkSiteAccessEnabled = System
                .getProperty(SystemProperty.defaultSocialNetworkSiteAccessEnabled
                        .name());
        List<SocialNetworkSite> socialNetworkSiteList = null;
        if (socialNetworkSiteAccessEnabledList != null
                && !socialNetworkSiteAccessEnabledList.get(0).equals("")) {
            socialNetworkSiteAccessEnabled = socialNetworkSiteAccessEnabledList
                    .get(0);
        }
        if (socialNetworkSiteAccessEnabled == null) {
            throw new ApplicationException(
                    MailboxError.MBX_SOCIALNETWORK_SITE_DEFAULT_VALUE_MISSING
                            .name());
        }
        if (logger.isDebugEnabled()) {
            logger.debug("socialNetworkSite -->" + socialNetworkSite);
            logger.debug("socialNetworksiteAccessEnabled -->"
                    + socialNetworkSiteAccessEnabled);
        }
        try {
            if (socialNetworkSite == null || socialNetworkSite.equals("")) {
                throw new MxOSException(
                        MailboxError.MBX_INVALID_SOCIALNETWORKSITE.name());
            }
            mailboxCRUDPool = MxOSApp.getInstance().getMailboxCRUD();
            provCRUD = mailboxCRUDPool.borrowObject();
            SocialNetworks sn = provCRUD.readSocialNetworks(email, requestState);
            socialNetworkSiteList = sn.getSocialNetworkSites();
            SocialNetworkSite[] objSNSArray = new SocialNetworkSite[1];

            int index = 0;
            if (socialNetworkSiteList != null) {
                objSNSArray = new SocialNetworkSite[socialNetworkSiteList
                        .size() + 1];
                for (SocialNetworkSite sns : socialNetworkSiteList) {
                    if (!sns.getSocialNetworkSite().equalsIgnoreCase(
                            socialNetworkSite)) {
                        objSNSArray[index] = sns;
                        index++;
                    }
                }
            }
            SocialNetworkSite newSNSObject = new SocialNetworkSite();
            newSNSObject.setSocialNetworkSite(socialNetworkSite.toLowerCase());
            newSNSObject
                    .setSocialNetworkSiteAccessEnabled(socialNetworkSiteAccessEnabled.toLowerCase());
            objSNSArray[index] = newSNSObject;

            String[] strSNSArray = ActionUtils
                    .socialNetArrayToStringArray(objSNSArray);

            if (strSNSArray != null) {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.socialNetworkSite, strSNSArray);
            }
        } catch (MxOSException e) {
            throw e;
        } catch (Exception e) {
            logger.error("Error while add social network sites.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_PUT_SOCIALNETWORKSSITE.name(), e);
        } finally {
            if (mailboxCRUDPool != null && provCRUD != null) {
                try {
                    mailboxCRUDPool.returnObject(provCRUD);
                } catch (MxOSException e) {
                    throw new ApplicationException(
                            ErrorCode.GEN_INTERNAL_ERROR.name(), e);
                }
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer("AddSocialNetworkSites action end."));
        }
    }

}
