/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.utils.config.MxOSConfig;

/**
 * Action class to set admin reject info.
 * 
 * @author mxos-dev
 */
public class SetAdminRejectInfo implements MxOSBaseAction {
    private static Logger logger = Logger.getLogger(SetAdminRejectInfo.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (MxOSConfig.isGroupMailboxEnabled()) {
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "SetAdminRejectInfo action start."));
            }
            final String adminRejectInfo = requestState.getInputParams()
                    .get(MailboxProperty.adminRejectInfo.name()).get(0);
            try {
                MxOSApp.getInstance()
                        .getMailboxHelper()
                        .setAttribute(requestState,
                                MailboxProperty.adminRejectInfo,
                                adminRejectInfo);
            } catch (final MxOSException e) {
                throw e;
            } catch (final Exception e) {
                logger.error("Error while set admin reject info action.", e);
                throw new InvalidRequestException(
                        MailboxError.MBX_UNABLE_TO_SET_ADMIN_REJECT_INFO.name(),
                        e);
            }
            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer("SetAdminRejectInfo action end."));
            }
        } else {
            logger.error("Error while set admin reject action, group mailbox feature is disabled.");
            throw new InvalidRequestException(
                    MailboxError.GROUP_MAILBOX_FEATURE_IS_DISABLED.name());
        }
    }
}