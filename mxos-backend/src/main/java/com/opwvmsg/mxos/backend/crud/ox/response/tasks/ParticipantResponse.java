package com.opwvmsg.mxos.backend.crud.ox.response.tasks;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;

import com.opwvmsg.mxos.backend.crud.exception.TasksException;
import com.opwvmsg.mxos.data.enums.OXContactsProperty;
import com.opwvmsg.mxos.data.enums.OXTasksProperty;
import com.opwvmsg.mxos.task.pojos.Participant;
import com.sun.jersey.api.client.ClientResponse;

public class ParticipantResponse extends Response {

    public Participant getParticipant(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;	
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        
        root=root.path("participants");
		root = root.get(0);
		Logger logger = Logger.getLogger(ParticipantResponse.class);
		logger.info("Participant JsonNode : "+root.toString());
		
        Participant participant = JsonToTasksMapper.mapToTaskParticipant(root);
        return participant;
    }
    
    public Participant getParticipantId(ClientResponse resp) throws TasksException {
        JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;	
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        
        
        Logger logger = Logger.getLogger(ParticipantResponse.class);
        logger.info("Participant JsonNode : "+root.toString());
        
        Participant participant = new Participant();
        if (root.path(OXTasksProperty.id.name()).isInt()) {
			participant.setParticipantId(String.valueOf(root.path(OXTasksProperty.id.name()).asInt()));
		}
        
        return participant;
    }

	public List<Participant> getAllParticipants(ClientResponse resp) throws TasksException {
		List<Participant> participantList = new ArrayList<Participant>();
		Logger logger = Logger.getLogger(ParticipantResponse.class);

		JsonNode root = getTree(resp);
        if (root == null
                || root.path(OXContactsProperty.data.name()).isMissingNode()) {
            return null;	
        } else {
            root = root.path(OXContactsProperty.data.name());
        }
        logger.info("List of Participants JsonNode : "+root.toString());
        root=root.path("participants");
        
        
        int participantsSize=root.size();
        logger.info("Participant JsonNode size : "+participantsSize);
        
        for(int i=0;i<participantsSize;i++){
        	JsonNode participantNode = root.get(i);
    		logger.info("Participant JsonNode : "+root.toString());
    		Participant participant = JsonToTasksMapper.mapToTaskParticipant(participantNode);
    		participantList.add(participant);
        }
		
        return participantList;
	}
}
