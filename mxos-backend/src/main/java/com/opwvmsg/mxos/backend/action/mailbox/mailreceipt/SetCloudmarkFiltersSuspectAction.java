/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.mailreceipt;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxOSConstants;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set cloudmark filters suspect action.
 *
 * @author mxos-dev
 */
public class SetCloudmarkFiltersSuspectAction implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetCloudmarkFiltersSuspectAction.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersSuspectAction action start."));
        }
        String suspectAction = requestState.getInputParams()
                .get(MailboxProperty.suspectAction.name()).get(0);
        try {
            if (suspectAction != null && !suspectAction.equals("")) {
                suspectAction = MxosEnums.CloudmarkActionType.fromValue(
                        suspectAction).toString();
                if (suspectAction.equalsIgnoreCase(MxOSConstants.QUARANTINE)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.folderName.name())) {
                        String folderName = requestState.getInputParams()
                                .get(MailboxProperty.folderName.name()).get(0);
                        suspectAction += MxOSConstants.SPACE + folderName;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.FOLDER_NAME_REQUIRED.name());
                    }
                } else if (suspectAction.equalsIgnoreCase(MxOSConstants.TAG)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.tagText.name())) {
                        String tagText = requestState.getInputParams()
                                .get(MailboxProperty.tagText.name()).get(0);
                        suspectAction += MxOSConstants.SPACE + tagText;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.TAG_TEXT_REQUIRED.name());
                    }
                } else if (suspectAction.equalsIgnoreCase(MxOSConstants.HEADER)) {
                    if (null != requestState.getInputParams().get(
                            MailboxProperty.headerText.name())) {
                        String headerText = requestState.getInputParams()
                                .get(MailboxProperty.headerText.name()).get(0);
                        suspectAction += MxOSConstants.SPACE + headerText;
                    } else {
                        logger.error("Error while set cloudmark filters spam action.");
                        throw new InvalidRequestException(
                                MailboxError.HEADER_TEXT_REQUIRED.name());
                    }
                }
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.suspectAction,
                            suspectAction);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set cloudmark filters suspect action.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_CLOUDMARK_FILTERS_SUSPECT_ACTION
                            .name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetCloudmarkFiltersSuspectAction action end."));
        }
    }
}
