/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.crud.mss;

import java.util.HashMap;
import java.util.Map;

import com.opwvmsg.mxos.backend.requeststate.IBackendState;
/**
 * Backend State class for MSS.
 *
 * @author mxos-dev
 */
public class MssMetaBackendState implements IBackendState {
    public Map<String, String> attributes;
    /**
     * Default constructor.
     */
    public MssMetaBackendState() {
        attributes = new HashMap<String, String>();
    }

    @Override
    public int getSize() {
        if(attributes != null) {
            return attributes.size();
        } else {
            return 0;
        }
    }
}
