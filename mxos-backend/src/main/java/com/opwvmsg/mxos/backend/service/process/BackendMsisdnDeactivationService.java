/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.backend.service.process;

import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.backend.service.BackendServiceUtils;
import com.opwvmsg.mxos.exception.MxOSException;
import com.opwvmsg.mxos.interfaces.service.Operation;
import com.opwvmsg.mxos.interfaces.service.ServiceEnum;
import com.opwvmsg.mxos.interfaces.service.process.IMsisdnDeactivationService;
import com.opwvmsg.mxos.utils.misc.StatStatus;
import com.opwvmsg.mxos.utils.misc.Stats;

/**
 * Fiona Deactivation service exposed to client which is responsible for
 * deactivating the MSISDN via BACKEND API of MxOS.
 * 
 * @author mxos-dev
 */
public class BackendMsisdnDeactivationService implements
        IMsisdnDeactivationService {
    private static Logger logger = Logger
            .getLogger(BackendMsisdnDeactivationService.class);

    /**
     * Default Constructor.
     * 
     * @throws Exception on any error.
     */
    public BackendMsisdnDeactivationService() throws MxOSException {
        logger.info("BackendMsisdnDeactivationService is created");
    }

    @Override
    public void process(final Map<String, List<String>> inputParams)
            throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "BackendMsisdnDeactivationService process start"));
        }

        long t0 = Stats.startTimer();

        try {
            final MxOSRequestState mxosRequestState = new MxOSRequestState(
                    inputParams, ServiceEnum.MsisdnDeactivationService,
                    Operation.POST);
            BackendServiceUtils.validateAndExecuteService(mxosRequestState);

            Stats.stopTimer(ServiceEnum.MsisdnDeactivationService,
                    Operation.POST, t0, StatStatus.pass);

            if (logger.isDebugEnabled()) {
                logger.debug(new StringBuffer(
                        "BackendMsisdnDeactivationService process end"));
            }
        } catch (MxOSException e) {
            Stats.stopTimer(ServiceEnum.MsisdnDeactivationService,
                    Operation.POST, t0, StatStatus.fail);
            throw e;
        }
    }
}
