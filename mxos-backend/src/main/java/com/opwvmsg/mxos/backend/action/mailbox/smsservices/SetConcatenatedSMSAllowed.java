/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.backend.action.mailbox.smsservices;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.data.enums.MxosEnums;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.InvalidRequestException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set sms services concatenated SMS allowed.
 *
 * @author mxos-dev
 */
public class SetConcatenatedSMSAllowed implements MxOSBaseAction {
    private static Logger logger = Logger
            .getLogger(SetConcatenatedSMSAllowed.class);

    @Override
    public void run(final MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetConcatenatedSMSAllowed action start."));
        }
        String concatenatedSMSAllowed = requestState.getInputParams()
                .get(MailboxProperty.concatenatedSMSAllowed.name()).get(0);

        try {
            if (concatenatedSMSAllowed != null
                    && !concatenatedSMSAllowed.equals("")) {
                concatenatedSMSAllowed = Integer.toString(MxosEnums.BooleanType
                        .fromValue(concatenatedSMSAllowed).ordinal());
            }
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState,
                            MailboxProperty.concatenatedSMSAllowed,
                            concatenatedSMSAllowed);

        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set concatenated SMS allowed.", e);
            throw new InvalidRequestException(
                    MailboxError.MBX_UNABLE_TO_SET_CONCATENATED_SMS_ALLOWED
                            .name(),
                    e);
        }
        if (logger.isDebugEnabled()) {
            logger.debug(new StringBuffer(
                    "SetConcatenatedSMSAllowed action end."));
        }
    }
}
