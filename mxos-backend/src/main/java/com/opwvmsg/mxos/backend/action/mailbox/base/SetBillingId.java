/*
 * Copyright (c) 2014 Openwave Messaging Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 */
package com.opwvmsg.mxos.backend.action.mailbox.base;

import org.apache.log4j.Logger;

import com.opwvmsg.mxos.backend.action.commons.MxOSApp;
import com.opwvmsg.mxos.backend.action.commons.MxOSBaseAction;
import com.opwvmsg.mxos.backend.requeststate.MxOSRequestState;
import com.opwvmsg.mxos.data.enums.MailboxProperty;
import com.opwvmsg.mxos.error.MailboxError;
import com.opwvmsg.mxos.exception.ApplicationException;
import com.opwvmsg.mxos.exception.MxOSException;

/**
 * Action class to set billingId
 * 
 * @author mxos-dev
 * 
 */
public class SetBillingId implements MxOSBaseAction {
    private static final Logger logger = Logger.getLogger(SetBillingId.class);

    public void run(MxOSRequestState requestState) throws MxOSException {
        if (logger.isDebugEnabled()) {
            logger.debug("SetBillingId action start.");
        }

        try {
            String billingId = requestState.getInputParams()
                    .get(MailboxProperty.billingId.name()).get(0);
            MxOSApp.getInstance()
                    .getMailboxHelper()
                    .setAttribute(requestState, MailboxProperty.billingId,
                            billingId);
        } catch (final MxOSException e) {
            throw e;
        } catch (final Exception e) {
            logger.error("Error while set billingId.", e);
            throw new ApplicationException(
                    MailboxError.MBX_UNABLE_TO_SET_BILLINGID.name(), e);
        }

        if (logger.isDebugEnabled()) {
            logger.debug("SetBillingId action end.");
        }
    }

}
