 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Task Recurrence - Read API
 
 To Read a Task recurrence object of a Task

* API Description

 Read Task Recurrence API will return entire task recurrence data

** Invoking using SDK - Returns void

 *  void ITasksRecurrenceService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/tasks/\{taskId\}/recurrence

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>

 * taskId=<ID of the Task>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** REST URL Response

 * Success Response - HTTP 200 with Task Base Object as body

+--
{		
	type: "Daily",	
	daysInWeek: "[Monday,Wednesday,Friday]",	
	recurAfterInterval: "2",	
	endDate: "2011-07-29T17:26:54Z",	
	notification: "Report release progress status"	
	occurrences: "24"	
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_INVALID_SESSION" message="Invalid session." />
    <error code="TSK_INVALID_COOKIE" message="Invalid cookie." />
    <error code="TSK_INVALID_TASKID" message="Invalid Task Id." />
    <error code="TSK_TASKBASE_UNABLE_TO_GET" message="Invalid Task Id." />  
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 