 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Task Base - Update API
 
 To Update a Task Base object of a Task

* API Description

 Update Task Base API will update entire task base data

** Invoking using SDK - Returns void

 *  void ITasksBaseService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/tasks/\{taskId\}/base

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>

 * taskId=<ID of the Task>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** Optional Parameters

 * folderId=<Open Xchange folder where the task should be created, default is private folder (22)> 
 
 * name=<Name for a task>
 
 * owner=<Owner of the task Eg. test1@openwave.com>
 
 * priority=<Priority for a task, Allowed Values : "Low", "High", "Medium">
 
 * status=<Status of a task, Allowed Values: "Not started", "In progress", "Done", "Waiting", "Deferred">
 
 * isPrivate=<Allowed Values - "yes", no">
 
 * colorLabel=<Any integer value which corresponds to color number>
 
 * categories=<String containing comma separated categories. Order is preserved.>
 
 * notes=<Notes for the task as text value>
 
 * startDate=<Start date for a task, Allowed value - String>
 
 * dueDate=<Due date for a task, Allowed value - String>
 
 * reminderDate=<Reminder ,Allowed value - String>
 
 * progress=<Progress, Allowed value - integer> 
 
** REST URL Response

 * Success Response - HTTP 200 with Task Base Object as body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_INVALID_SESSION" message="Invalid session." />
    <error code="TSK_INVALID_COOKIE" message="Invalid cookie." />
    <error code="TSK_INVALID_TASKID" message="Invalid Task Id." />
    <error code="TSK_INVALID_NAME" message="Invalid task name." />
    <error code="TSK_INVALID_OWNER" message="Invalid task owner." />
    <error code="TSK_INVALID_PRIORITY" message="Invalid task priority." />
    <error code="TSK_INVALID_STATUS" message="Invalid task status" />
    <error code="TSK_INVALID_PRIVATE_FLAG" message="Invalid task private flag(isPrivate)." />
    <error code="TSK_INVALID_COLOR_LABEL" message="Invalid task color label." />
    <error code="TSK_INVALID_CATEGORIES" message="Invalid task categories." />
    <error code="TSK_INVALID_NOTES" message="Invalid task notes." />
    <error code="TSK_INVALID_UPDATED_DATE" message="Invalid task updated date." />
    <error code="TSK_INVALID_START_DATE" message="Invalid task start date." />
    <error code="TSK_INVALID_DUE_DATE" message="Invalid task due date." />
    <error code="TSK_INVALID_REMINDER_DATE" message="Invalid task reminder date." />
    <error code="TSK_INVALID_PROGRESS" message="Invalid task progress." />
    <error code="TSK_TASKBASE_UNABLE_TO_UPDATE" message="Unable to update a task." />  
    <error code="TSK_TASKBASE_UNABLE_TO_GET" message="Invalid Task Id." /> 
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 