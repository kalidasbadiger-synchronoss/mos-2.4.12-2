 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Task Detail - Read API
 
 To Read a Task Detail object of a Task

* API Description

 Read Task Detail API will return entire task Detail data

** Invoking using SDK - Returns Details object

 *  Details ITasksDetailsService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/task/v2/\{userId\}/tasks/\{taskId\}/details

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>

 * taskId=<ID of the Task>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** REST URL Response

 * Success Response - HTTP 200 with Task DetailS Object as body

+--
{		
	actualCost: "",	
	actualWork: "",	
	billing: "",	
	targetCost: "xyz dollars",	
	targetWork: "2 week Sprint",	
	currency: "Dollars",	
	mileage: "0",	
	companies: "Example.com",	
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="TSK_INVALID_USERNAME" message="Invalid username." />
    <error code="TSK_INVALID_SESSION" message="Invalid session." />
    <error code="TSK_INVALID_COOKIE" message="Invalid cookie." />
    <error code="TSK_INVALID_TASKID" message="Invalid Task Id." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 