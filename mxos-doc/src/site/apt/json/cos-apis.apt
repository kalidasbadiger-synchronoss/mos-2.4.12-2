 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------
 
COS APIs
 
* COS Macro APIs
 
 * {{{./cos/cos-create.html}ICosService.create(�)}}
 
    This operation is responsible for creating COS.
     
 * {{{./cos/cos-delete.html}ICosService.delete(�)}}
 
    This operation is responsible for deleting COS.
 
* COS Micro APIs
 
** COS Base APIs
 
  Provides COS Base Object level operations like Read, Update and List.
 
 * {{{./cos/base/base-read.html}ICosBaseService.read(�)}}
 
    This operation is responsible for reading COS.
    
 * {{{./cos/base/base-update.html}ICosBaseService.update(�)}}
  
    This operation is responsible for updating COS.
    
 * {{{./cos/base/base-search.html}ICosBaseService.search(�)}} 
 
    This operation is responsible for searching COS.
  
** COS WebMailFeatures APIs
  
  Interface to COS for WebMailFeatures Object level operations like Read, Update.
  
 * {{{./cos/webMailFeatures/webMailFeatures-read.html}ICosWebMailFeaturesService.read(�)}}
 
    This operation is responsible for reading WebMailFeatures for a COS.
 
 * {{{./cos/webMailFeatures/webMailFeatures-update.html}ICosWebMailFeaturesService.update(�)}} 

    This operation is responsible for updating WebMailFeatures attributes for a COS.

** COS WebMailFeatures/addressBookFeature APIs
  
  Interface to COS for WebMailFeaturesAddressBook Object level operations like Read, Update.
  
 * {{{./cos/webMailFeatures/addressBook/addressBookFeatures-read.html}ICosWebMailFeaturesAddressBookService.read(�)}}
 
    This operation is responsible for reading WebMailFeatures for a COS.
 
 * {{{./cos/webMailFeatures/addressBook/addressBookFeatures-update.html}ICosWebMailFeaturesAddressBookService.update(�)}} 

    This operation is responsible for updating WebMailFeatures attributes for a COS.

** COS Credentials APIs
 
  Interface to COS Credentials Object level operations like Read and Update.
 
 * {{{./cos/credentials/credentials-read.html}ICosCredentialsService.read(�)}}
 
    This operation is responsible for reading COS credentials. 
 
 * {{{./cos/credentials/credentials-update.html}ICosCredentialsService.update(�)}}
 
    This operation is responsible for updating COS credentials.
 
** COS GeneralPreferences APIs
 
  Interface to COS GeneralPreferences Object level operations like Read and Update. 
 
 * {{{./cos/generalPreferences/generalPreferences-read.html}ICosGeneralPreferencesService.read(�)}}
 
    This operation is responsible for reading COS GeneralPreferences. 
 
 * {{{./cos/generalPreferences/generalPreferences-update.html}ICosGeneralPreferencesService.update(�)}}
 
    This operation is responsible for updating COS GeneralPreferences. 
 
** COS MailSend APIs
 
  Interface to COS MailSend Object level operations like Read and Update.
 
 * {{{./cos/mailSend/cos-mailSend-read.html}ICosMailSendService.read(�)}}
 
    This operation is responsible for reading COS MailSend.
 
 * {{{./cos/mailSend/cos-mailSend-update.html}ICosMailSendService.update(�)}}
 
    This operation is responsible for updating COS MailSend.
 
** COS MailReceipt APIs

  Interface to COS for MailReceipt Object level operations like Read, Update.
 
 * {{{./cos/mailReceipt/mailReceipt-read.html}IMailReceiptService.read(�)}}

    This operation is responsible for reading MailReceipt for a COS.
 
 * {{{./cos/mailReceipt/mailReceipt-update.html}IMailReceiptService.update(�)}} 

    This operation is responsible for updating MailReceipt attributes for a COS.

** COS mailReceipt/senderBlocking APIs
 
 * {{{./cos/mailReceipt/senderBlocking/senderBlocking-update.html}ISenderBlockingService.update(�)}}
  
    This operation is responsible for updating SenderBlocking attributes for a COS.
 
** COS mailReceipt/filters/sieveFilters APIs

  Interface to COS for MailReceiptSieveFilters Object level operations like Update.

 * {{{./cos/mailReceipt/filters/sieveFilters/sieveFilters-update.html}IMailReceiptSieveFiltersService.update(�)}}
 
    This operation is responsible for updating MailReceiptSieveFilters attributes for a COS.
  
** COS mailReceipt/filters/cloudMarkFilters APIs

  Interface to COS for MailReceiptCloudmarkFilters Object level operations like Read, Update.
 
 * {{{./cos/mailReceipt/filters/cloudmarkFilters/cloudmarkFilters-read.html}IMailReceiptCloudmarkFiltersService.read(�)}}

    This operation is responsible for reading MailReceipt for a COS.
 
 * {{{./cos/mailReceipt/filters/cloudmarkFilters/cloudmarkFilters-update.html}IMailReceiptCloudmarkFiltersService.update(�)}} 

    This operation is responsible for updating MailReceipt attributes for a COS.    
 
** COS MailAccess APIs

  Interface to COS for MailAccess Object level operations like Read, Update.

 * {{{./cos/mailAccess/cosMailAccess-read.html}ICosMailAccessService.read(�)}}

    This operation is responsible for reading MailAccess for a COS.

 * {{{./cos/mailAccess/cosMailAccess-update.html}ICosMailAccessService.update(�)}} 

    This operation is responsible for updating MailAccess attributes for a COS.

** COS MailStore APIs
 
  Mail store operations interface which will be exposed to the client. This
  interface is responsible for doing mail store related operations (like Read,
  Update, etc.).
 
 * {{{./cos/mailStore/cos-mailStore-read.html}ICosMailStoreService.read(�)}}
 
    This operation is responsible for reading mail store COS attributes. It uses one or more
    actions to do this activity.
 
 * {{{./cos/mailStore/cos-mailStore-update.html}ICosMailStoreService.update(�)}}
 
    This operation is responsible for updating mail store cos attributes. It uses one or
    more actions to do this activity. 
 
** COS ExternalStore APIs
 
  External store operations interface which will be exposed to the client. This
  interface is responsible for doing mail store related operations (like Read,
  Update, etc.).
 
 * {{{./cos/mailStore/externalStore/cos-externalStore-read.html}ICosExternalStoreService.read(�)}}
 
    This operation is responsible for reading mail store COS attributes. It uses one or more
    actions to do this activity.
 
 * {{{./cos/mailStore/externalStore/cos-externalStore-update.html}ICosExternalStoreService.update(�)}}
 
    This operation is responsible for updating mail store cos attributes. It uses one or
    more actions to do this activity. 
 
** COS socialNetworks APIs

  Interface to COS for SocialNetwork Object level operations like Read, Update.

 * {{{./cos/socialNetworks/socialNetworks-read.html}ICosSocialNetworkService.read(�)}}

    This operation is responsible for reading SocialNetwork for a COS.

 * {{{./cos/socialNetworks/socialNetworks-update.html}ICosSocialNetworkService.update(�)}}
 
    This operation is responsible for updating SocialNetwork attributes for a COS.
 
** COS externalAccounts APIs

  Interface to COS for ExternalAccounts Object level operations like Read, Update.

 * {{{./cos/externalAccounts/externalAccount-read.html}ICosExternalAccountsService.read(�)}}

    This operation is responsible for reading ExternalAccounts for a COS.

 * {{{./cos/externalAccounts/externalAccount-update.html}ICosExternalAccountsService.update(�)}} 
 
    This operation is responsible for updating ExternalAccounts attributes for a COS.
 
** COS smsServices APIs

  Interface to COS for SmsServices Object level operations like Read, Update.

 * {{{./cos/smsServices/cosSmsServices-read.html}ICosSmsServicesService.read(�)}}
 
    This operation is responsible for reading SmsServices for a COS.

 * {{{./cos/smsServices/cosSmsServices-update.html}ICosSmsServicesService.update(�)}}

    This operation is responsible for updating SmsServices attributes for a COS.

** COS smsServices/smsOnline APIs

  Interface to COS for SmsOnline Object level operations like Read, Update.

 * {{{./cos/smsServices/smsOnline/cosSmsOnline-read.html}ICosSmsOnlineService.read(�)}}

    This operation is responsible for reading SmsOnline for a COS.

 * {{{./cos/smsServices/smsOnline/cosSmsOnline-update.html}ICosSmsOnlineService.update(�)}}
 
    This operation is responsible for updating SmsOnline attributes for a COS. 

** COS internalinfo APIs

  Interface to COS for InternalInfo Object level operations like Read, Update.

 * {{{./cos/internalInfo/internalInfo-read.html}ICosInternalInfoService.read(�)}}

    This operation is responsible for reading InternalInfo for a COS.

 * {{{./cos/internalInfo/internalInfo-update.html}ICosInternalInfoService.update(�)}}
 
    This operation is responsible for updating InternalInfo attributes for a COS.
 
** COS internalInfo/messageEventRecords APIs

  Interface to COS for MessageEventRecords Object level operations like Read, Update.

 * {{{./cos/internalInfo/messageEventRecords/messageEventRecords-read.html}ICosMessageEventRecordsService.read(�)}}

    This operation is responsible for reading MessageEventRecords for a COS.

 * {{{./cos/internalInfo/messageEventRecords/messageEventRecords-update.html}ICosMessageEventRecordsService.update(�)}}

    This operation is responsible for updating MessageEventRecords attributes for a COS.
