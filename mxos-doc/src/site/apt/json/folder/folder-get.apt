 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

getFolder API
 
 Get folder details

* API Description

** Invoking using SDK

 *  Folder read(final Map<String, List<String>> inputParams) throws MxOSException;

** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}

** GET Request Mandatory HTTP Query Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Name of the folder to be retrieved, folder name is case sensitive; maximum allowed folder name length and folder depth are 255 characters and 30 respectively>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false and this attribute is only allowed for stateful>
 
 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false and this attribute is only allowed for stateful>

 * ignorePopDeleted=<Ignore pop deleted, allowed values are true/false - Only for RME version 175 and higher>
       false - Gets all messages including popDeleted Messages
       true  - Do not get messages marked as popDeleted

** REST URL Response
 
 * Success Response - HTTP 200 with the following JSON response in the body for stateful

+--
{
    "folderName" : "INBOX",
    "nextUID" : 10,
    "nextUIDValidity" : 1234,
    "numMessages" : 100,
    "numReadMessages" : 100,
    "numUnreadMessages" : 0,
    "folderSizeBytes" : 2000,
    "folderSizeReadBytes" : 2000,
    "folderSizeUnreadBytes" : 0,
}
+--

 * Success Response - HTTP 200 with the following JSON response in the body for stateless
 
+--
 {
	folderId: "8264f690-6adc-336d-b0e4-0e98ef5a262e"
	folderName: "/SentMail"
	nextUID: 1000
	uidValidity: 2343240
	numMessages: 10
	numReadMessages: 10
	numUnreadMessages: 0
	folderSizeBytes: 410755
	folderSizeReadBytes: 410755
	folderSizeUnreadBytes: 0
	parentFolderId: "00000000-0000-1000-0000-000000000000"
	folderSubscribed: "true"
	keywords: "keywords"
}
+--
 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--

 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if unable to get folder
    <error code="FLD_UNABLE_TO_PERFORM_GET" message="Unable to perform Get Folder operation." />
 * Error code, if unable to find the given folder
     <error code="FLD_NOT_FOUND" message="Given folder NOT found." />  
 * Error code, if isAdmin parameter is Invalid.   
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
 * Error code, if  supressMers parameter is Invalid.   
    <error code="FLD_INVALID_SUPRESS_MERS" message="Invalid supressMers value." />       
+--

*** Common errors 

    {{{../errors/common-errors.html} Common Error Details}}