 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

updateFolder API

 Update folder details

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException;

** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}

** Mandatory Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Name of the new folder to be updated, folder name is case sensitive; maximum allowed folder name length and folder depth are 255 characters and 30 respectively>
 
 * toFolderName=<New folder name, new folder name is case sensitive>

** Optional Parameters

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false and this attribute is only allowed for stateful>
 
 * optionSupressMers=<Specifies to suppress MERS events for this operation, allowed values are true/false and this attribute is only allowed for stateful>
 
 * uidValidity=<uidValidity of a folder and this attribute is only allowed for stateless>
 
 * folderSubscribed=<folderSubscribed indicates if the folder is subscribed, allowed values are true/false and this attribute is only allowed for stateless>

** REST URL Response

 * Success Response - HTTP 200 without body
 
 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body
 
 Note: In case of stateless folderUUID(folderId) of the folder to be renamed will get updated.
  
** Error Codes

*** API specific errors

+--

 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if folderName is Invalid.   
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
 * Error code, if tofolderName is Invalid.   
    <error code="FLD_INVALID_TO_FOLDERNAME" message="Invalid toFolderName value." />
 * Error code, if isAdmin parameter is Invalid.   
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
 * Error code, if  supressMers parameter is Invalid.   
    <error code="FLD_INVALID_SUPRESS_MERS" message="Invalid supressMers value." />   
 * Error code, if unable to create folder   
    <error code="FLD_UNABLE_TO_PERFORM_UPDATE" message="Unable to perform Update Folder operation." />
 * Error code, if unable to find the given folder
     <error code="FLD_NOT_FOUND" message="Given folder NOT found." />
 * Error code, if folder already exists.   
    <error code="FLD_ALREADY_EXISTS" message="Given folder already exists." />
 * Error code, if uidValidity is Invalid
    <error code="FLD_INVALID_UID_VALIDITY" message="Invalid uidValidity value." /> 
 * Error code, if folderSubscribed is Invalid
    <error code="FLD_INVALID_FOLDER_SUBSCRIBED" message="Invalid folderSubscribed value." />            
+--

*** Common errors 

    {{{../errors/common-errors.html} Common Error Details}}
