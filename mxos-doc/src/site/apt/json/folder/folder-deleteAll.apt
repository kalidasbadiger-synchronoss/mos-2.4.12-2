 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

deleteAllFolder API

 Deletes all the folders

* API Description

** Invoking using SDK

 *  void deleteAll(final Map<String, List<String>> inputParams) throws MxOSException;

** Invoking using REST URL

 * URL - DELETE http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders

** Mandatory Parameters

 * email=<Subscriber's email id>

** REST URL Response

 * Success Response - HTTP 200 without body
 
 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--

 * Error code, if email is bad formatted or Invalid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." /> 
 * Error code, if any required parameter is missing.
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters." />
 * Error code, if unable to delete mailbox in mss   
    <error code="MBX_UNABLE_TO_DELETE" message="Unable to perform Mailbox Delete operation." />
 * Error code, if unable to create mailbox in mss  
    <error code="MBX_UNABLE_TO_CREATE" message="Unable to perform Mailbox Create operation." />
+--

*** Common related errors

    {{{../errors/common-errors.html} Common Error Details}}
