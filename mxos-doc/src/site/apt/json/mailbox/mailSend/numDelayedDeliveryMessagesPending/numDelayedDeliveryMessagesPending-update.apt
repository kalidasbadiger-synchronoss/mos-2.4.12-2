 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

numDelayedDeliveryMessagesPending - update API
 
 To Update numDelayedDeliveryMessagesPending attributes value

* API Description

** Invoking using SDK

 *  void INumDelayedDeliveryMessagesPendingService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailSend/numDelayedDeliveryMessagesPending/\{oldNumDelayedDeliveryMessagesPending\}/\{newNumDelayedDeliveryMessagesPending\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
 * oldNumDelayedDeliveryMessagesPending=<Old numDelayedDeliveryMessagesPending value>
 
 * newNumDelayedDeliveryMessagesPending=<New numDelayedDeliveryMessagesPending value>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code, if oldNumDelayedDeliveryMessagesPending is invalid
    <error code="MBX_INVALID_OLD_NUM_DELAYED_DELIVERY_MESSAGES_PENDING" message="Invalid old delayed delivery messages pending." />
 * Error code, if newNumDelayedDeliveryMessagesPending is invalid
    <error code="MBX_INVALID_NEW_NUM_DELAYED_DELIVERY_MESSAGES_PENDING" message="Invalid new delayed delivery messages pending." />
 * Error code, if email is invalid or not RFC compliant.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
   
+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}

 