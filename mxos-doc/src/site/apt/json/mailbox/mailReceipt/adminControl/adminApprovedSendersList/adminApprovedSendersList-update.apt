 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Admin control adminApprovedSendersList - update API
 
 To update  admin control adminApprovedSendersList attributes

* API Description

** Invoking using SDK

 *  void update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST https://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailReceipt/adminControl/adminApprovedSendersList/\{oldAdminApprovedSender\}/\{newAdminApprovedSender\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * oldAdminApprovedSender=<Old admin approved sender address to be replaced>

 * newAdminApprovedSender=<New admin approved sender address>

 * Example: http://localhost:8080/mxos/mailbox/v2/test@openwave.com/mailReceipt/adminControl/adminApprovedSendersList/oldadminapprovedsender.com/newadminapprovedsender.com

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    <error code="MBX_NOT_GROUP_ACCOUNT" message=
    		"Mailbox is not a group account." />
    <error code="MBX_NOT_GROUP_ADMIN_ACCOUNT" message=
    		"Given email not a valid family administrator"/>
    <error code="MBX_INVALID_ADMIN_OLD_APPROVED_SENDER" message=
    		"Invalid admin old approved sender provided"/>
    <error code="MBX_ADMIN_APPROVED_SENDER_NOT_EXIST" message=
    		"Admin old approved sender does not exist"/>
    <error code="MBX_INVALID_ADMIN_NEW_APPROVED_SENDER" message=
    		"Invalid admin new approved sender provided"/>
    <error code="MBX_UNABLE_TO_ADMIN_APPROVED_SENDERS_POST" message=
    		"Unable to update admin old approved sender list with new approved senders list"/>
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 