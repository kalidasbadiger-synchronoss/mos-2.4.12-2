 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailAccess AllowedIPs - delete API
 
 To delete an existing allowed IP

* API Description

** Invoking using SDK

 * void IMailAccessAllowedIpService.delete(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - DELETE http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailAccess/allowedIPs/\{allowedIP\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * allowedIP=<Allowed IP in IPv4 or IPv6 format>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_DELETE" message="Unable to perform AllowedIPs DELETE operation." />
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    // allowedIP format is not valid.
    <error code="MBX_INVALID_MAILACCESS_ALLOWED_IP" message="Invalid allowedIP." />
    // allowedIP format is not valid.
    <error code="MBX_MAILACCESS_ALLOWED_IP_NOT_EXIST" message="Allowed IP does not exist." />
+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}
 