 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailAccess AllowedIPs - create API
 
 To create an allowed IP

* API Description

** Invoking using SDK

 * void IMailAccessAllowedIpService.create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * For Single allowedIP in input
  URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailAccess/allowedIPs/\{allowedIP\}

 * For Multiple allowedIPs in input
  URL - PUT http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailAccess/allowedIPs

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

 * allowedIP=<Allowed IP in IPv4 or IPv6 format>
   Example: allowedIP=10.x.x.1&allowedIP=10.x.x.2&allowedIP=10.x.x.3

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_MAILACCESS_ALLOWED_IPS_UNABLE_TO_CREATE" message="Unable to perform AllowedIPs Create operation." />
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    // allowedIP format is not valid.
    <error code="MBX_INVALID_MAILACCESS_ALLOWED_IP" message="Invalid allowedIP." />
    <error code="MBX_MAILACCESS_ALLOWED_IPS_REACHED_MAX_LIMIT" message="Allowed IPs reached the maximum limit." />
+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}
