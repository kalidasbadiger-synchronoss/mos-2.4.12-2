 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MailAccess - read API
 
 To read mail access POJO

* API Description

** Invoking using SDK - Returns MailAccess POJO

 * MailAccess IMailAccessService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns MailAccess JSON

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/mailAccess

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
    "allowedIPs": [
        "10.203.201.123",
        "fd00:1:11:5a:221:5aff:fea7:65e0"
    ],
    "popAccessType": "none",
    "popSSLAccessType": "all",
    "imapAccessType": "none",
    "imapSSLAccessType": "all",
    "smtpAccessEnabled": "yes",
    "smtpSSLAccessEnabled": "yes",
    "smtpAuthenticationEnabled": "yes",
    "webmailAccessType": "all",
    "webmailSSLAccessType": "all",
    "mobileMailAccessType": "all"
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // This error usually comes server is not able to get the MailAccess object.
    <error code="MBX_UNABLE_TO_MAILACCESS_GET" message="Unable to perform MailAccess GET operation." />
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
