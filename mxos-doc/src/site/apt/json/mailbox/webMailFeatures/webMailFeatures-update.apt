 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

WebMailFeatures - update API
 
 To Update WebMailFeatures attributes

* API Description

** Invoking using SDK

 *  void IWebMailFeaturesService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/webMailFeatures

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
** Optional Parameters (Any one is required)

 * businessFeaturesEnabled=<Authorization for business users or regular users. Allowed values: No|Yes>

 * fullFeaturesEnabled=<This states if full feature is enabled or not. Allowed values: No|Yes>

 * lastFullFeaturesConnectionDate=<Date when the Full feature was accessed. Allowed values: Date in yyyy-MM-dd'T'HH:mm:ss'Z' format>
 
 * allowPasswordChange=<This attribute specifies whether subscribers have permission to change their WebEdge account password.>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // Email format is not valid.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    // Email does not exist.
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    <error code="MBX_UNABLE_TO_SET_BUSINESS_FEATURES_ENABLED" message="Unable to update businessFeaturesEnabled." />
    <error code="MBX_UNABLE_TO_SET_FULL_FEATURES_ENABLED" message="Unable to update fullFeaturesEnabled." />
    <error code="MBX_UNABLE_TO_SET_LAST_FULL_FEATURES_CONN_DATE" message="Unable to update lastFullFeaturesConnectionDate." />
    <error code="MBX_INVALID_LAST_FULL_FEATURES_CONN_DATE" message="Invalid lastFullFeaturesConnectionDate." />
    <error code="MBX_UNABLE_TO_SET_ALLOW_PASSWORD_CHANGE" message="Unable to update allowPasswordChange" />
    <error code="MBX_INVALID_ALLOW_PASSWORD_CHANGE" message="Invalid value for allowPasswordChange" />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
