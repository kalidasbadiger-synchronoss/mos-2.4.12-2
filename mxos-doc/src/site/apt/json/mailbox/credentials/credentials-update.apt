 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox Credentials - update API
 
 To Update Mailbox Credentials attributes

* API Description

** Invoking using SDK

 *  void ICredentialService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/credentials

** Mandatory Parameters

 * email=<Subscriber's email address>
 
** Optional Parameters (Any one is required)

  * passwordStoreType= <Update of only passwordStoreType is not allowed; it can be updated only along with password;
                        subscriber's password Type allowed values - clear, unix, sha1, ssha1, sha256, ssha256, md5-po, sha1, custom1, custom2, custom3, custom4, custom5, bcrypt>
                        Note - Currently mOS does not support custom2, custom3, custom4 and custom5 types. 
  
  * preEncryptedPasswordAllowed= <PreEncryptedPasswordAllowed, allowed value -true, false.
                                 If it is true  mOS to check for optional attribute passwordStoreType,
                                 If passwordStoreType is not passed in the request, mOS to use passwordStoreType as SHA1,
                                 If passwordStoreType is passed in the request and is not SHA1, mOS to send back the error,
                                 If the preEnryptedPasswordAllowed is not passed in the request or its value is false, it will follow existing behaviour. 
                                 If passwordStoreType is 'bcrypt' and its value is true, it will follow existing behaviour.
                                 If passwordStoreType is 'bcrypt' and its value is false, an exception will be thrown.>  
  
  * password= <Subscriber's password, By default, the allowed values are strings between 1 and 60 characters, 
               containing letters, numbers and the following special characters "+", "/", "=", "-". 
               An operator can control the allowed characters by modifying the password validator configuration 
               in $MXOS_HOME/var/mxos/config/mxos/schema/attributes/password>
  
  * passwordRecoveryAllowed= <Specified whether password recovery enabled, allowed value- yes or no>
  
  * passwordRecoveryPreference= <Indicate password recovery - allowed value- none, email, SMS, All >
  
  * passwordRecoveryMsisdn= <Valid msisdn number used for recovery, should be between 10 to 15 length, start with(+) and only number supported >
  
  * passwordRecoveryMsisdnStatus= <Indicates whether msisdn recovery is activated or not, allowed values- activated or deactivated>
  
  * lastPasswordRecoveryMsisdnStatusChangeDate= <date in format yyyy-MM-dd'T'HH:mm:ss'Z'>
  
  * passwordRecoveryEmail= <Email used in password Recovery, valid email address of format userName@domain>
  
  * passwordRecoveryEmailStatus= <Indicates whether email recovery is activated or not, allowed values- activated or deactivated>
  
  * maxFailedLoginAttempts= <Specified maximum number of failed login attempts for a user, allowed value- positive integer>
    
  * maxFailedCaptchaLoginAttempts= <Specified maximum number of failed captcha login attempts for a user, allowed value- positive integer>
  
  * failedCaptchaLoginAttempts= <Indicates total number of failed captcha login attempts since last successful login>
  
  * unlockMailbox= <Indicates if the failedLoginAttempts and lastFailedLoginDate to be reset. allowed value- true or false>
  
** Following attributes will be available only for Stateless RME:

  * failedLoginAttempts= <Indicates total number of failed login attempts since last successful login>
  
  * lastLoginAttemptDate= <date of last login attempt in format yyyy-MM-dd'T'HH:mm:ss'Z'>
  
  * protocolType=<To update last access-time for given protocol type. It can take value "pop", "imap", "smtp", or "webmail">
  
  * lastSuccessfulLoginDate=<Last successful login date in format yyyy-MM-dd'T'HH:mm:ss'Z'>
  
  * lastFailedLoginDate=<Last failure login time in long>
  
  * accessControlStatus=<Access control status>, enum: ["unlocked", "locked", "templocked"]
   
  * ignoreSoftDelete=<true/false to determine if the operation conducts against a soft-deleted message store>.\
 Note this option is valid only for updating accessControlStatus. 

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code, if email parameter  is missing from the request.   
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters."/>
 * Error code for missing required parameters other than email 
 	<error code="MBX_CREDENTIALS_UPDATE_MISSING_PARAMS" message="One or more mandatory parameters are missing 
 		for Credentials POST operation." />
 * Error code, if password set operation is unsuccessful 		
    <error code="MBX_UNABLE_TO_SET_PASSWORD" message="Unable to perform set password." />
 * Error code, if password format is Invalid     
    <error code="MBX_INVALID_PASSWORD" message="Invalid password, check the password format." />
 * Error code, if password_Store_Type is Invalid
    <error code="MBX_INVALID_PASSWORD_STORETYPE" message="Unable to perform set passwordStoreType, 
    	Invalid passwordStoreType parameter" />
 * Error code, if preEncryptedPasswordAllowed is Invalid
    <error code="MBX_INVALID_PRENCRYPTED_PASSWORD_ALLOWED" message="Invalid value for prencrypted password allowed" />
 * Error code, if passwordRecoveryAllowed parameter is Invalid
    <error code="MBX_INVALID_PASSWORD_RECOVERY_ALLOWED" message="Unable to perform set passwordRecoveryAllowed, 
    	Invalid passwordRecoveryAllowed parameter." />
 * Error code, if passwordRecoveryPreference parameter is Invalid    	
    <error code="MBX_INVALID_PASSWORD_RECOVERY_PREFERENCE" message="Unable to perform set passwordRecoveryPreference, 
    	Invalid passwordRecoveryPreference parameter." />
 * Error code, if passwordRecoveryMsisdn parameter is Invalid        	
    <error code="MBX_INVALID_PASSWORD_RECOVERY_MSISDN" message="Unable to perform set passwordRecoveryMsisdn, 
    	Invalid passwordRecoveryMsisdn parameter." />
 * Error code, if passwordRecoveryMsisdnStatus parameter is Invalid      	
    <error code="MBX_INVALID_PASSWORD_RECOVERY_MSISDN_STATUS" message="Unable to perform set passwordRecoveryMsisdnStatus 
    	Invalid passwordRecoveryMsisdnStatus parameter" />
 * Error code, if passwordRecoveryMsisdnStatusChangeDate parameter is Invalid     	
    <error code="MBX_INVALID_PASS_REC_MSISDN_STATUS_CHANGE_DATE" message="Unable to perform set 
   		passwordRecoveryMsisdnStatusChangeDate, Invalid passwordRecoveryMsisdnStatusChangeDate parameter" />
 * Error code, if passwordRecoveryEmail format is Invalid       	
    <error code="MBX_INVALID_PASSWORD_RECOVERY_EMAIL" message="Unable to perform set passwordRecoveryEmail,
    	Invalid passwordRecoveryEmail paremeter." />
 * Error code, if passwordRecoveryEmailStatus parameter is Invalid      	
    <error code="MBX_INVALID_PASSWORD_RECOVERY_EMAIL_STATUS" message="Unable to perform set passwordRecoveryEmailStatus,
    	Invalid passwordRecoveryEmailStatus parameter" />
 * Error code, if maxFailedLoginAttempts parameter is Invalid  
    <error code="MBX_INVALID_MAX_FAILED_LOGIN_ATTEMPTS" message="Unable to perform set maxFailedLoginAttempts,
        Invalid maxFailedLoginAttempts parameter." />
 * Error code, if failedCaptchaLoginAttempts parameter is Invalid
    <error code="MBX_INVALID_FAILED_CAPTCHA_LOGIN_ATTEMPTS" message="Invalid failedCaptchaLoginAttempts parameter." />
    <error code="MBX_UNABLE_TO_SET_FAILED_CAPTCHA_LOGIN_ATTEMPTS" message="Unable to set failedCaptchaLoginAttempts." />
 * Error code, if maxFailedCaptchaLoginAttempts parameter is Invalid
    <error code="MBX_INVALID_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS" message="Invalid maxFailedCaptchaLoginAttempts parameter." />
    <error code="MBX_UNABLE_TO_SET_MAX_FAILED_CAPTCHA_LOGIN_ATTEMPTS" message="Unable to set maxFailedCaptchaLoginAttempts." />   
 * Error code, if failedLoginAttempts parameter is Invalid  
    <error code="MBX_INVALID_FAILED_LOGIN_ATTEMPTS" message="Unable to perform set failedLoginAttempts,
        Invalid failedLoginAttempts parameter." />
 * Error code, if lastLoginAttemptDate parameter is Invalid    
    <error code="MBX_INVALID_LAST_LOGIN_ATTEMPTS_DATE" message="Unable to perform set lastLoginAttemptDate,
        Invalid lastLoginAttemptDate parameter." />
 * Error code
    <error code="MBX_UNABLE_TO_SET_LAST_SUCCESSFUL_LOGIN_DATE" message="Unable to perform set successful login date." />
    <error code="MBX_INVALID_LAST_SUCCESSFUL_LOGIN_DATE" message="Invalid date format for last successful login date." />
 * Error code, if lastFailedLoginTime parameter is Invalid      
    <error code="MBX_INVALID_LAST_FAILED_LOGIN_DATE" message="Invalid last failed login time." />
 * MBX_UNABLE_TO_UPDATE_ACCESS_CONTROL_STATUS : Unable to update mailbox access control status.
 * MBX_MESSAGE_STORE_LOCKED : Unable to lock message store due to lock conflict.
 * MBX_INVALID_ACCESS_CONTROL_STATUS : Invalid access control status is given.
 
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 