 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailbox smsServices - update API
 
 To Update Mailbox smsServices attributes

* API Description

** Inoking using SDK - Return Void

 *  void ISmsServicesService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/smsServices

** Mandatory Parameters

 * email=<Subscriber's email address >
 
** Optional Parameters (Any one is required)

 * smsServicesAllowed= <Allowed value - yes or no>
 
 * smsServicesMsisdn= <SMS Services MSISDN for the email account in international format
 	(should be between 10 to 15 length, start with(+) and only number supported)>
 
 * smsServicesMsisdnStatus= <Allowed Value- activated or deactivated>
 
 * lastSmsServicesMsisdnStatusChangeDate= <Status change date - format (yyyy-MM-dd'T'HH:mm:ss'Z')>


** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code, if parameter smsServicesAllowed is Invalid.
    <error code="MBX_INVALID_SMS_SERVICES_ALLOWED" message="Invalid sms services allowed." />
 * Error code, if update operation on smsServicesAllowed is unsuccessful.
    <error code="MBX_UNABLE_TO_SET_SMS_SERVICES_ALLOWED" message="Unable to set smsServicesAllowed." />
 * Error code, if parameter smsServicesMsisdn is Invalid.
    <error code="MBX_INVALID_SMS_SERVICES_MSISDN" message="Invalid sms services msisdn." />
 * Error code, if update operation on smsServicesAllowed is unsuccessful.
    <error code="MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN" message="Unable to set smsServicesMsisdn." />
 * Error code, if parameter smsServicesMsisdnStatus is Invalid.
    <error code="MBX_INVALID_SMS_SERVICES_MSISDN_STATUS" message="Invalid sms services msisdn status." />
 * Error code, if update operation on smsServicesMsisdnStatus is unsuccessful.
    <error code="MBX_UNABLE_TO_SET_SMS_SERVICES_MSISDN_STATUS" message="Unable to set smsServicesMsisdnStatus." /> 
 * Error code, if parameter lastSmsServicesMsisdnStatusChangeDate is Invalid.
    <error code="MBX_INVALID_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE" message="Invalid last sms 
    	services msisdn status change date." />
 * Error code, if update operation on lastSmsServicesMsisdnStatusChangeDate is unsuccessful.
    <error code="MBX_UNABLE_TO_SET_LAST_SMS_SERVICES_MSISDN_STATUS_CHANGE_DATE" message="Unable to set 
    	lastSmsServicesMsisdnStatusChangeDate." />
    
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 