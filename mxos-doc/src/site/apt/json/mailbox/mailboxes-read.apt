 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Mailboxes - read API
 
 To read mailboxes POJO

* API Description

** Invoking using SDK - Returns List of Mailbox POJO in case of multiple email read 

 * Base IMailboxService.list(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns Mailbox Base JSON or List of Mailbox JSON in case of multiple email read.

 * URL - GET http://mxosHost:mxosPort/mxos/mailbox/v2/mailboxes?email={email1}&email={email2}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>

** Default Parameters
  
 * userName, domain, firstName, lastName, email, mailboxId, cosId, maxNumAliases
 
  
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body
   
+--

    Presently only LDAP parameters are supported, so response only contains LDAP Attributes not MSS attribute
    [
    {
        "base": {
            "userName": "zz123500@test.com",
            "domain": "test.com",
            "firstName": "zz123500@test.com",
            "lastName": "zz123500@test.com",
            "email": "zz123500@test.com",
            "cosId": "default"
        },
        "webMailFeatures": {},
        "credentials": {},
        "generalPreferences": {},
        "mailSend": {
            "filters": {
                "bmiFilters": {},
                "commtouchFilters": {},
                "mcAfeeFilters": {}
            }
        },
        "mailReceipt": {
            "senderBlocking": {},
            "filters": {
                "sieveFilters": {},
                "bmiFilters": {},
                "commtouchFilters": {},
                "cloudmarkFilters": {},
                "mcAfeeFilters": {}
            }
        },
        "mailAccess": {},
        "mailStore": {
            "externalStore": {}
        },
        "socialNetworks": {},
        "externalAccounts": {},
        "smsServices": {
            "smsOnline": {},
            "smsNotifications": {}
        },
        "internalInfo": {
            "version": "4"
        }
    },
    {
        "base": {
            "userName": "zz123600@test.com",
            "domain": "test.com",
            "firstName": "zz123600@test.com",
            "lastName": "zz123600@test.com",
            "email": "zz123600@test.com",
            "cosId": "default"
        },
        "webMailFeatures": {},
        "credentials": {},
        "generalPreferences": {},
        "mailSend": {
            "filters": {
                "bmiFilters": {},
                "commtouchFilters": {},
                "mcAfeeFilters": {}
            }
        },
        "mailReceipt": {
            "senderBlocking": {},
            "filters": {
                "sieveFilters": {},
                "bmiFilters": {},
                "commtouchFilters": {},
                "cloudmarkFilters": {},
                "mcAfeeFilters": {}
            }
        },
        "mailAccess": {},
        "mailStore": {
            "externalStore": {}
        },
        "socialNetworks": {},
        "externalAccounts": {},
        "smsServices": {
            "smsOnline": {},
            "smsNotifications": {}
        },
        "internalInfo": {
            "version": "4"
        }
    }
    ]
	

 * Note:This response is for mailbox get with all the parameters configured, so response is based on the parameters configured in mxos.properties configuration file

+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_GET" message="Unable to perform Mailbox GET operation." />
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    <error code="MBX_INVALID_MAX_ALLOWEDDOMAIN" message="Invalid maxAllowedDomain value." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
