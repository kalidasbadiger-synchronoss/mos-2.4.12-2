 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

AllowedDomain - update API
 
 To Update AllowedDomain attributes value

* API Description

** Invoking using SDK

 *  void IAllowedDomainService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/base/allowedDomains/\{oldallowedDomain\}/\{newallowedDomain\}

** Mandatory Parameters

 * email=<Subscriber's email address or emailAlias>
 
 * oldAllowedDomain=<Old domain for the account>
 
 * newAllowedDomain=<New domain for the account, RFC compliant domain>
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
 * Error code for invalid oldAllowedDomain parameter passed in the request.
    <error code="MBX_INVALID_OLD_ALLOWED_DOMAIN" message="Invalid old allowed domain." />
 * Error code for invalid new allowedDomain provided in parameter.    
    <error code="MBX_INVALID_NEW_ALLOWED_DOMAIN" message="Invalid new allowed domain." />
 * Error code, if email is invalid or not RFC compliant.
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
+--

*** Common errors 

 * {{{../../../errors/common-errors.html} Common errors}}

 