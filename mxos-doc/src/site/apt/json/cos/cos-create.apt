 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Cos - create API
 
 To Create Cos attributes

* API Description

  API to Create Cos. You can also load default cos data from ${MXOS_HOME}/config/default-cos.properties.
  Loading multivalued attributes of COS is not supported.

** Invoking using REST Context in SDK - Return CosID

 *  ICosService.create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - PUT http://mxosHost:mxosPort/mxos/cos/v2/\{cosId\}

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>
 
** Optional Parameters
 
 * loadDefaultCos= <yes - Load default cos data from default-cos.properties or not. no - not to load default values.>

* Note:
 For creating cos all the optional parameters which are required can be added via default-cos.properties and if some wrong values 
 or greater than the limit values like if an attribute is integer then if some value given is greater then integer limit it will 
 create cos in most of the cases but during any kind of get or update cos/mailbox operation it will fail for that optional parameters.
 
** REST URL Response

 * Success Response - HTTP 200 without body.

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--

 * Error code, if cos create operation is unsuccessful.
    <error code="COS_UNABLE_TO_CREATE" message="Unable to perform Cos Create operation." />
 * Error code for invalid cosId.
    <error code="COS_INVALID_COSID" message="Invalid cosId." />
 * Error code, if the given cos already exists.
    <error code="COS_ALREADY_EXISTS" message="The given Cos is already exists." />

+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 