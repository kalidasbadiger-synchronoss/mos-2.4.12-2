 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

COS MailSend - read API
 
 To read COS MailSend attributes

* API Description

** Invoking using SDK

 *  void read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET https://<host>/mxos/cos/v2/{cosId}/mailSend

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
    {
        "futureDeliveryEnabled" : "yes",
        "maxFutureDeliveryDaysAllowed" : 0,
        "maxFutureDeliveryMessagesAllowed" : 0,
        "useRichTextEditor": "html",
        "includeOriginalMailInReply": "yes",
        "originalMailSeperatorCharacter": ">",
        "autoSaveSentMessages": "yes",
        "addSignatureForNewMails": "yes",
        "addSignatureInReplyType": "beforeOriginalMessage",
        "autoSpellCheckEnabled": "yes",
        "confirmPromptOnDelete": "yes",
        "includeReturnReceiptReq": "yes",
        "maxSendMessageSizeKB": 2048,
        "maxAttachmentSizeKB": 2048,
        "maxAttachments": 10,
        "maxCharactersPerPage": 1024
    }
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_MAILSEND_GET" message="Unable to perform MailSend GET operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 