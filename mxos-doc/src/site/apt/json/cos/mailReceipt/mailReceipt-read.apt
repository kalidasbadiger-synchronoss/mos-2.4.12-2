 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

COS MailReceipt - read API
 
 To read COS MailReceipt attributes

* API Description

** Invoking using SDK

 *  void read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET https://<host>/mxos/cos/v2/\{cosId\}/mailReceipt

** Mandatory Parameters

 * cosId=<Distinguished name of the class of service>
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
   {
        "receiveMessages": "yes",
        "forwardingEnabled": "no",
        "copyOnForward": "yes",
        "filterHTMLContent": "no",
        "displayHeaders": "HTML",
        "webmailDisplayWidth": 80,
        "webmailDisplayFields": "data,priority,size",
        "maxMailsPerPage": 50,
        "previewPaneEnabled": "yes",
        "ackReturnReceiptReq": "always",
        "deliveryScope": "local",
        "autoReplyMode": "none",
        "maxReceiveMessageSizeKB": 40000000,
        "mobileMaxReceiveMessageSizeKB": 40000000,
        "maxNumForwardingAddresses": 18
        "senderBlocking": {
            "senderBlockingAllowed": "no",
            "senderBlockingEnabled": "no",
            "blockSendersPABActive": "no",
            "blockSendersPABAccess": "no"
        },
        "filters": {
            "sieveFilters": {
                "sieveFilteringEnabled": "yes",
                "mtaFilter": "if header 'from' contains-nocase 'ma000001' charset 'UTF-8' {fileinto 'Junk Mail';keep;stop;}",
                "rmFilter": "8XPkKnPK4qGXzeGi;version=1.0;filter=New rule1;enabled=true;forward=;folder=;delete=true;deliver=;match=any;condition:from,contains,aaa;charset=UTF-8",
                "blockedSenderAction": "bounce",
                "blockedSenderMessage": "blocked",
                "rejectBouncedMessage": "no"
            }
        }
    }
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_UNABLE_TO_MAILRECEIPT_GET" message="Unable to perform MailReceipt GET operation." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 