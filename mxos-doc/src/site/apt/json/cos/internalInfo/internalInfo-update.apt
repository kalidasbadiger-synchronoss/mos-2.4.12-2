 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Cos InternalInfo - update API
 
 To Update Cos InternalInfo attributes

* API Description

** Invoking using SDK

 *  void ICosInternalInfoService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/cos/v2/\{cosId\}/internalInfo

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>
 
** Optional Parameters (Any one is required)

 * webmailVersion=<Webmail Version>

 * selfCareAccessEnabled=<Self Care Access Enabled. Allowed values: all|trusted|none>

 * selfCareSSLAccessEnabled=<Self Care SSL Access Enabled. Allowed values: all|trusted|none>
 
 * imapProxyHost=<Imap Proxy host for the migration, imapProxyHost is case sensitive>

 * imapProxyAuthenticationEnabled=<IMAP Proxy Authentication. Allowed values: yes|no>

 * remoteCallTracingEnabled=<Remote Call Tracing Enabled. Allowed values: yes|no> 

 * interManagerAccessEnabled=<Enables account access to the InterManager for the Openwave Email application. Allowed values: yes|no>

 * interManagerSSLAccessEnabled=<Enables account access to the SSL-encrypted InterManager for the Openwave Email application. Allowed values: yes|no>

 * voiceMailAccessEnabled=<Specifies whether the subscriber account has access to voice message features from the web interface. Allowed values: yes|no>

 * faxMailAccessEnabled=<Specifies whether the subscriber account has access to fax features from the web interface. Allowed values: yes|no>

 * ldapUtilitiesAccessType=<Controls account access to standard LDAP services such as imldapsh, ldapadd, ldapmodify, and other LDAP utilities. Allowed values: all|trusted|none>

 * addressBookProvider=<Specifies the subscriber-preferred address book provider. Allowed values: mss|plaxo|sun|vox>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="COS_INVALID_COSID" message="Invalid cosId." />
    <error code="COS_NOT_FOUND" message="The given cosId does not exist." />

    <error code="MBX_INVALID_WEBMAIL_VERSION" message="Invalid webmailVersion." />
    <error code="MBX_INVALID_SELFCARE_ACCESS" message="Invalid selfcare access." />
    <error code="MBX_INVALID_SELFCARE_SSL_ACCESS" message="Invalid selfcare SSL access." />
    <error code="MBX_INVALID_IMAP_PROXY_AUTH" message="Invalid imapProxyAuthenticationEnabled." />
    <error code="MBX_INVALID_REMOTE_CALL_TRACING" message="Invalid remoteCallTracingEnabled." />
    <error code="MBX_INVALID_INTER_MANAGER_ACCESS" message="Invalid interManagerAccessEnabled" />
    <error code="MBX_INVALID_INTER_MANAGER_SSL_ACCESS" message="Invalid interManagerSSLAccessEnabled" />
    <error code="MBX_INVALID_VOICE_MAIL_ACCESS" message="Invalid voiceMailAccessEnabled." />
    <error code="MBX_INVALID_FAX_ACCESS" message="Invalid faxAccessEnabled." />
    <error code="MBX_INVALID_LDAP_UTILITIES_ACCESS_TYPE" message="Invalid ldapUtilitiesAccessType." />
    <error code="MBX_INVALID_ADDRESS_BOOK_PROVIDER" message="Invalid addressBookProvider." />

    <error code="MBX_UNABLE_TO_INTERNALINFO_GET" message="Unable to perform InternalInfo GET operation." />
    <error code="MBX_UNABLE_TO_SET_WEBMAIL_VERSION" message="Unable to set webmailVersion." />
    <error code="MBX_UNABLE_TO_SET_SELFCARE_ACCESS" message="Unable to set selfcare access." />
    <error code="MBX_UNABLE_TO_SET_SELFCARE_SSL_ACCESS" message="Unable to set selfcare access." />
    <error code="MBX_UNABLE_TO_SET_IMAP_PROXY_AUTH" message="Unable to set imapProxyAuthenticationEnabled." />
    <error code="MBX_UNABLE_TO_SET_REMOTE_CALL_TRACING" message="Unable to set remote call tracing." />
    <error code="MBX_UNABLE_TO_SET_INTER_MANAGER_ACCESS" message="Unable to set interManagerAccessEnabled." />
    <error code="MBX_UNABLE_TO_SET_INTER_MANAGER_SSL_ACCESS" message="Unable to set interManagerSSLAccessEnabled." />
    <error code="MBX_UNABLE_TO_SET_VOICE_MAIL_ACCESS" message="Unable to set voiceMailAccessEnabled." />
    <error code="MBX_UNABLE_TO_SET_FAX_ACCESS" message="Unable to set faxAccessEnabled." />
    <error code="MBX_UNABLE_TO_SET_LDAP_UTILITIES_ACCESS_TYPE" message="Unable to set ldapUtilitiesAccessType." />
    <error code="MBX_UNABLE_TO_SET_ADDRESS_BOOK_PROVIDER" message="Unable to set addressBookProvider." />
    <error code="MBX_UNABLE_TO_SET_IMAP_PROXY_HOST" message="Unable to set IMAP proxy host." />

+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
