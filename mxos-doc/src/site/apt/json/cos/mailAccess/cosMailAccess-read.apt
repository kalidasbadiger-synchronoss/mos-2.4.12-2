 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

COS MailAccess - read API
 
 To read COS MailAccess POJO

* API Description

** Invoking using SDK - Returns COS POJO

 * MailAccess ICosMailAccessService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns MailAccess JSON

 * URL - GET http://mxosHost:mxosPort/mxos/cos/v2/\{cosId\}/mailAccess

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
    "popAccessType": "none",
    "popSSLAccessType": "all",
    "imapAccessType": "none",
    "imapSSLAccessType": "all",
    "smtpAccessEnabled": "yes",
    "smtpSSLAccessEnabled": "yes",
    "smtpAuthenticationEnabled": "yes",
    "webmailAccessType": "all",
    "webmailSSLAccessType": "all",
    "mobileMailAccessType": "all"
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    // This error usually comes server is not able to get the MailAccess object.
    <error code="MBX_UNABLE_TO_MAILACCESS_GET" message="Unable to perform MailAccess GET operation." />
    // COS is not valid.
    <error code="MBX_INVALID_COSID" message="Invalid cosId." /> 
    // COS does not exist.
    <error code="COS_NOT_FOUND" message="The given Cos does not exists." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
