 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Cos AddressBookFeatures - update API
 
 To Update Cos AddressBookFeatures attributes

* API Description

** Invoking using SDK

 *  void ICosWebMailFeaturesAddressBookService.update(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/cos/v2/\{cosId\}/webMailFeatures/addressBookFeatures

** Mandatory Parameters

 * cosId= <Specifies Distinguished name of the class of service, maxLength=30>
 
** Optional Parameters (Any one is required)

 * maxContacts=<Max number of address book entries for a user>

 * maxGroups=<Specifies the maximum number of address book groups that a subscriber may define.>

 * maxContactsPerGroup=<Specifies the maximum number of email addresses allowed in a single address book entry.>

 * maxContactsPerPage=<Specifies the subscriber-preferred number of address book entries that the web interface displays on a page. Allowed values- 0 to 2147483647>

 * createContactsFromOutgoingEmails=<Specifies the subscriber-preferred method by which the web and WAP interfaces collects recipient email addresses from outgoing messages and adds them to the personal address book.>
   <MOS values - disabled, forAllAddresses, onlyForAddressesInToField>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="COS_INVALID_COSID" message="Invalid cosId." />
    <error code="COS_NOT_FOUND" message="The given cosId does not exist." />
    <error code="MBX_UNABLE_TO_SET_MAX_CONTACTS" message="Unable to update maxContacts" />
    <error code="MBX_UNABLE_TO_SET_MAX_GROUPS" message="Unable to update maxGroups" />
    <error code="MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_GROUP" message="Unable to update maxContactsPerGroup" />
    <error code="MBX_UNABLE_TO_SET_MAX_CONTACTS_PER_PAGE" message="Unable to update maxContactsPerPage" />
    <error code="MBX_UNABLE_TO_SET_CREATE_CONTACTS_FROM_OUTGOING_EMAILS" message="Unable to update createContactsFromOutgoingEmails" />
    <error code="MBX_INVALID_MAX_CONTACTS" message="Invalid value for maxContacts" />
    <error code="MBX_INVALID_MAX_GROUPS" message="Invalid value for maxGroups" />
    <error code="MBX_INVALID_MAX_CONTACTS_PER_GROUP" message="Invalid value for maxContactsPerGroup" />
    <error code="MBX_INVALID_MAX_CONTACTS_PER_PAGE" message="Invalid value for maxContactsPerPage" />
    <error code="MBX_INVALID_CREATE_CONTACTS_FROM_OUTGOING_EMAILS" message="Invalid value for createContactsFromOutgoingEmails" />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
