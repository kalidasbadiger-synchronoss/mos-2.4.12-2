 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Domain - Delete API
 
 To Delete Domain attributes

* API Description

  Delete Domain API is to create a Domain Object.

** Invoking using REST Context in SDK - Return nothing on success, otherwise MxOSException

 *  IDomainService.delete(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - DELETE http://mxosHost:mxosPort/mxos/domain/v2/\{domain\}

** Mandatory Parameters

 * domain=<Complete domain name associated with a mailDomain object>
 
** Response Format

*** Success Response Format: 

 * HTTP 200 without body
 
*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--


** Error Codes

+--    

 * Error: DMN_INVALID_NAME, Message: Invalid domain, not RFC compliant, maxLength supported is 255.

 * Error: GEN_BAD_REQUEST, Message:Bad request, please check the request and parameters.

+--    

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

** Example
 
** DELETE Request HTTP Query Parameters

 * domain=openwave.com
 
    {{{./domain-delete-resp.html}http://mxos.openwave.com:8080/mxos/domain/v2/openwave.com}} 
 

