 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Domain - Read API
 
 To Read Domain object

* API Description

  Read Domain API is to Read a Domain Object.

** Invoking using REST Context in SDK - Return nothing on success, otherwise MxOSException

 *  IDomainService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - GET http://mxosHost:mxosPort/mxos/domain/v2/\{domain\}

** Mandatory Parameters

 * domain=<Complete domain name associated with a mailDomain object>

** Response Format

*** Success Response Format: 

+--
    {
        "domain": <new Domain thats been created>,
        "type": <type of Domain | Local/Non-authoritative/Rewrite>,
        "relayHost":<Another mail host that accepts messages for a nonauthoritative domain if the primary host does not recognize the recipient>,
        "alternateDomain":<Specifies the new domain name for a recipientís email address>,
        "defaultMailbox": <default mailbox for domain>,
        "customFields": <Custom Fields to hold fields at a per domain level>
    }
+--

*** Failure Response Format: 

 * HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body as follows

+--
{
   "code" : <Error code>
   "requestParams" : <Request parameters>
   "operationType" : <Operation type - PUT/GET/POST/DELETE
   "shortMessage" : <Short description of the Error>
   "longMessage" : <Exception details of the Error>
}
+--

** Error Codes

*** Parameter type validation errors

 * Error: DMN_INVALID_NAME, Message: Invalid domain, not RFC compliant, maxLength supported is 255.
 
*** Operation related errors

 * Error: GEN_BAD_REQUEST, Message: Bad request, please check the request and parameters..

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

** Example

*** GET Request Mandatory HTTP Query Parameters

    {{{./domain-get-resp.html}http://mxos.openwave.com:8080/mxos/domain/v2/openwave.com}} 

