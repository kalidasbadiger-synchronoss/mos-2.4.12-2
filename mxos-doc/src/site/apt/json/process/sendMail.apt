 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

sendMail API
 
 Send Mail to MTA

* API Description

 * Sends Mail to MTA using the SMTP Protocol.

 * No Retry handling in case of errors from External Interface.

** Invoking using SDK

 *  void process(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/sendMail/\{fromAddress\}/\{toAddress\}  
         
         (OR)
         
 * URL - POST http://mxosHost:mxosPort/mxos/sendMail/\{fromAddress\}

** Mandatory Parameters
 
 * fromAddress=Originator email address
 
 * message
 
   Message format:
   \r\n [<Message Header Key1>=<Message Header Value1>\r\n]* \r\n [\<Message\>] 

** Optional Parameters
 
 * toAddress=Recipient email address, as per the requirement to support multiple toAddress address as part of this attribute there is no validation done for it
             (Example: if a will user give an invalid toAddress user@@@domain1.com then it will throw SEND_MAIL_ERROR)
 
 * password=password of the user, this is only required if SMTP Auth is enabled and SMTP connection will be created for each request if SMTP Auth is enabled.
 
 * smtpAuthUser=UserName used for SMTP Auth and SMTP connection will be created using this instead of fromAddress. 
                Though in the request you need to pass both smtpUser and fromAddress otherwise it will give exception

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP HTTP 5xx with JSON MxOS Error object in the body

** API Usage:

*** Examples

+--  
   Example1: If the 'toAddress' is NOT sent as part of the URL or as a parameter from the SDK then the 'toAddress' is taken from the message header. In this case fromAddress is also required to be passed with toAddress in the message header otherwise exception will occur.
 
   POST - http://localhost:8080/mxos/sendMail/test@test.com
   Post parameters:
   message=to:user3.test@domain1.co.uk%0d%0afrom:user1.test@domain1.co.in%0D%0AContent-type: text/plain;charset=iso-8859-1%0d%0acc:abc@test.com%0d%0asubject: Test Subject%0d%0a%0d%0athis is only required if SMTP Auth is enabled and SMTP connection will be created&password=test

   Example2: If the 'toAddress' is NOT sent as part of the URL or as a parameter from the SDK then the 'toAddress' is taken from the message header. In this case fromAddress is also required to be passed with toAddress in the message header otherwise exception will occur.
 
   POST - http://localhost:8080/mxos/sendMail/test@test.com
   Post parameters:
   message=To:xyz@test.com,zyx@test.com %0D%0AContent-type: text/plain; charset=iso-8859-1 %0D%0ACc:abc@test.com %0D%0A Subject: Test Subject %0D%0A%0D%0A Test Message

   Example2: If the �toAddress� is sent as part of URL or as a parameter from the SDK then the 'toAddress' is taken from the URL and not from the message header.

   POST - http://localhost:8080/mxos/sendMail/test@test.com/xyz@test.com
   Post Parameters:
   message=Content-type: text/plain; charset=iso-8859-1%0D%0ACc:abc@test.com %0D%0A Subject: Test Subject %0D%0A%0D%0A Test Message
+--

** Error Codes

*** API specific errors

+--

    <error code="SEND_MAIL_INVALID_FROM_ADDRESS" message="Invalid fromAddress"/>
    <error code="SEND_MAIL_INVALID_PASSWORD" message="Invalid password, check the password format."/>
    <error code="SEND_MAIL_ERROR" message="Permanent Error from External Interface."/>
    <error code="SEND_MAIL_AUTHENTICATION_ERROR" message="535 Authentication failed"/>
    <error code="SEND_MAIL_CONNECT_ERROR" message="Could not connect to SMTP host: , port: "/>

+--

*** Configuration

 * {{{../../config.html} Gateway Config}} - gateway-config.xml : Section <SendMailGateways>

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 