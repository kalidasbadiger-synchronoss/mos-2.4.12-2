 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

sendSMS API
 
 Sends SMS using the External Interfaces

* API Description

 * MxOS API shall send the message using the HTTP 1.1 protocol.

 * No Throttling in MxOS.

 * No retry handling in MxOS.

 * No Special handling for the Segmented Messages

** Invoking using SDK

 *  void process(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/sendSMS/{smsType}/{fromAddress}/{toAddress} 

** Mandatory Parameters

 * smsType=<SMS Type - smsonline|basicnotification|advancednotification|passwordrecoverysms>

 * fromAddress=<Source MSISDN in International Format Ex: + followed by 10/11 digits(+32478123456)>
 
 * toAddress=<Recipient MSISDN in International Format Ex: + followed by 10/11 digits(+32478123456)>
 
 * message
 
   Message format(MMG Interface - English, Dutch and French Language):
   
   <Message Header Key1>=<Message Header Value1>\r\n* \r\n [\<Message\>] 
 
   Example message (MMG Interface - English, Dutch and French Language):
   
   Content-type: text/plain; charset=iso-8859-1 \r\n Subject: Test Subject \r\n\r\n Test Message 
 
   Note: Header Content-type = text/plain; charset=iso-8859-1 need to be used for sending the message in English or French or Dutch
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="SEND_SMS_INVALID_FROM_ADDRESS" message="Invalid fromAddress"/>
    <error code="SEND_SMS_INVALID_TO_ADDRESS" message="Invalid toAddress"/>
    <error code="SEND_SMS_INVALID_MESSAGE" message="Invalid parameter message"/>
    <error code="SMS_INVALID_SMS_TYPE" message="Invalid smsType"/>
    <error code="SEND_SMS_MISSING_PARAMS" message="Given request data is invalid. One or more parameters missing"/>
    <error code="SEND_SMS_TEMPORARY_ERROR" message="Temporary Error from External Interface. Retry again longmessage=<Error Code from SMS Server>:<Error Message from SMS Server>"/>
    <error code="SEND_SMS_PERMANENT_ERROR" message="Permanent Error from External Interface. longmessage=<Error Code from SMS Server>:<Error Message from SMS Server>"/>
    <error code="SEND_SMS_PERM_SNMP_ERROR" message="Permanent Error from External Interface."/>

+--

*** Configuration

 * {{{../../config.html} Gateway Config}} - gateway-config.xml : Section <SendSMSGateway>

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 