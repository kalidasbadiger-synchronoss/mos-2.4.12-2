 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

MessageMetaData service - update multiple messages meta data in particular folder.
  
  To update message metadata for multiple messages in particular folder. This will update the same flags for all messages.

* API Description

** Invoking using SDK - Returns void

 * void IMetadataService.updateMulti(final Map\<String, List\<String\>\> inputParams)throws MxOSException
 
** Invoking using REST URL - Returns 200 OK

 * URL - POST http://mxosHost:mxosPort/mxos/mailbox/v2/\{email\}/folders/\{folderName\}/messages/metadata

** POST Request Mandatory HTTP Query Parameters

 * email=<Subscriber's email id>
 
 * folderName=<Folder Name of the folder where the message resides>
 
 * messageId=<One or more MessageIDs which need to be copied> 
 	

** Optional Parameters (Any one is required)

 * isAdmin=<Specifies whether the operation is performed by admin user or end user, allowed values are true/false> � Only for RME Version 163.
 
 * flagSeen=<Flag that indicates message is seen. Empty value is not allowed. Allowed values: true|false>

 * flagAns=<Flag that indicates message is answered. Empty value is not allowed. Allowed values: true|false>

 * flagFlagged=<Flag that indicates message is flagged. Empty value is not allowed. Allowed values: true|false>

 * flagDel=<Flag that indicates message is deleted. Empty value is not allowed. Allowed values: true|false>

 * flagRecent=<Flag that indicates message is recent. Empty value is not allowed. Allowed values: true|false>

 * flagDraft=<Flag that indicates message is in draft state. Empty value is not allowed. Allowed values: true|false>
 
 * keywords=<keyword to be updated. Empty value is not allowed. Allowed values: any string. Only for RME 175 and higher>

 * deleteKeyword=<Flag that indicates that keyword needs to be deleted. Need to pass same keyword that needs to be deleted in keyword field. Empty value is not allowed. Allowed values: true|false. Only applicable for RME 175 and above>
 
 * popDeletedFlag=<Pop deleted flag, allowed values are true/false - Only for RME version 175 and higher>
      false - means mark the message as normal deletion.
      true  - means mark the message as special pop deleted.

 * disableNotificationFlag=<Disable notification flag, allowed values are true/false - Only for RME version 175 and higher>
    false - Send notification to the IMAP client
    true - Do not send notification to the IMAP client  

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="MBX_INVALID_EMAIL" message="Invalid email format." />
    <error code="MBX_NOT_FOUND" message="The given email does not exist." />
    <error code="FLD_INVALID_FOLDERNAME" message="Invalid folderName value." />
    <error code="MSG_INVALID_MESSAGE_ID" message="Invalid messageId value." />
    <error code="MSG_INVALID_IS_ADMIN" message="Invalid isAdmin value." />
    <error code="MSG_INVALID_FLAG_SEEN" message="Invalid flagSeen." />
    <error code="MSG_INVALID_FLAG_ANS" message="Invalid flagAns." />
    <error code="MSG_INVALID_FLAG_FLAGGED" message="Invalid flagFlagged." />
    <error code="MSG_INVALID_FLAG_DEL" message="Invalid flagDel." />
    <error code="MSG_INVALID_FLAG_RECENT" message="Invalid flagRecent." />
    <error code="MSG_INVALID_FLAG_DRAFT" message="Invalid flagDraft." />
    <error code="MSG_INVALID_POP_DELETE_FLAG" message="Invalid Pop Deleted Flag." />
    <error code="MSG_INVALID_KEYWORDS" message="Invalid Keywords." />
    <error code="MSG_INVALID_DELETE_KEYWORD" message="Invalid delete keyword." />
    <error code="GEN_BAD_REQUEST" message="Bad request, please check the request and parameters" />
    <error code="MBX_UNABLE_TO_MSSLINKINFO_GET" message="Unable to perform MssLinkInfo GET operation." />
    <error code="MSG_UNABLE_TO_PERFORM_GET" message="Unable to perform Get Message operation." />
    <error code="MSG_UNABLE_TO_PERFORM_UPDATE" message="Unable to perform Update Message operation." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}

 