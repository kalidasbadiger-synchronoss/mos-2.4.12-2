 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Groups Base - read API
 
 To read groups Base POJO

* API Description

 API to read Groups Base

** Invoking using SDK 

 * GroupBase IGroupsBaseService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
 * List<GroupBase> IGroupsBaseService.list(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns Contacts Base JSON

 * URL - GET http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/groups/\{groupId\}/base
 
 * URL - GETALL http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/groups/base/list
 
** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * groupId=< AddressBook group Id, which is returned after calling create group API>

** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  

** Optional Parameters

 * query=<query for searching based on attribute criteria>  
 * Supported keys: schema fields, Example: query= firstName==John && lastName!= Does || displayName ~ John && displayName !~ Does && middleName ^= ab && birthday > 01-01-2001 && anniversary $= cd
 
 * sortKey=<attribute to sort on>  
 * Supported keys: schema field, Example: sortKey=lastName

 * sortOrder=<order of sorting>  
 * Supported values: ascending or descending, Example: sortOrder=ascending
 
 * filter=<filter type>  
 * Supported keys: displayName, Example: filter=ABC
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
[(4)
    {
        "groupId": "-2042060050",
        "groupName": "group4",
        "groupDescription": "ChangedGroupDescription",
        "folderId": "22",
        "customFields": "uid=uid1"
    },-
    {
        "groupId": "-1262605337",
        "groupName": "GROUP2",
        "groupDescription": "ChangedGroupDescription",
        "folderId": "22",
        "customFields": "uid=uid1"
    },-
    {
        "groupId": "2103009497",
        "groupName": "GROUP1",
        "groupDescription": "ChangedGroupDescription",
        "folderId": "22",
        "customFields": "uid=uid1"
    },-
    {
        "groupId": "-934261366",
        "groupName": "GROUP3",
        "groupDescription": "ChangedGroupDescription",
        "folderId": "22",
        "customFields": "uid=uid1"
    }-
]

+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_GROUP" message="Invalid group id." />
    <error code="ABS_GROUPSBASE_UNABLE_TO_GET" message="Error in getting groups base." />    
    <error code="ABS_INVALID_SEARCH_QUERY" message="Invalid search query." />
    <error code="ABS_INVALID_SEARCH_TERM" message="Invalid search term." />
    <error code="ABS_INVALID_SORT_KEY" message="Invalid sort key." />
    <error code="ABS_INVALID_SORT_ORDER" message="Invalid sort order." />
    <error code="ABS_INVALID_FILTER" message="Invalid filter." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
