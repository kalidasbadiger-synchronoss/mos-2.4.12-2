 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

PersonalInfo Communication- read API
 
 To read contacts PersonalInfo Communication POJO

* API Description

  API to read PersonalInfo Communication. 

** Invoking using SDK - Returns Contacts Base POJO

 * Communication IContactsPersonalInfoCommunicationService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns ContactsPersonalInfo JSON

 * URL - GET http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/personalInfo/communication

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>

** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>  
 
** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
 "phone1": "123",
 "phone2": "123",
 "voip": "28",
 "mobile": "123",
 "fax": "123",
 "email": "29@29.com",
 "imAddress": "dfghdklfg"
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_PERSONALINFO_COMMUNICATION_UNABLE_TO_GET" message="Error in getting Contacts PersonalInfo Communication." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
