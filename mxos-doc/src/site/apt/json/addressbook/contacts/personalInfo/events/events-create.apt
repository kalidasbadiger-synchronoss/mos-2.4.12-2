 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

PersonalInfo Event - create API
 
 To create contacts PersonalInfo Event POJO

* API Description

 Creates PersonalInfo event attributes like birthday and anniversary

** Invoking using SDK - Returns Contacts Base POJO

 * void IContactsPersonalInfoEventsService.create(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL 

 * URL - PUT http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/personalInfo/events/\{type\}

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>
 
 * type=<Type of event to be added Birthday , Anniversary>
   
 * date=<Date to be added in the event>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>
 
 ** Optional Parameters

 * folderId=<Folder Id of the contact>

** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--    
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_PERSONALINFO_EVENT_UNABLE_TO_UPDATE" message="Error in updating Contacts PersonalInfo events." />
    <error code="ABS_PERSONALINFO_INVALID_EVENTS_TYPE" message="Invalid personal Info events type." />
    <error code="ABS_PERSONALINFO_INVALID_EVENTS_DATE" message="Invalid personal Info events date." />    
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}

 