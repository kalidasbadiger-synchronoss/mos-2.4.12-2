 ------------------------------------------------------------------------------
 Openwave Messaging MxOS 2.0
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

Workinfo Communication - Read API
 
 This API will be used to get workinfo communication POJO

* API Description

 API to read WorkInfo Communication

** Invoking using SDK - Returns workinfo communication POJO

 * Communication IContactsWorkInfoCommunicationService.read(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL - Returns Workinfo Communication JSON Object

 * URL - GET http://mxosHost:mxosPort/mxos/addressBook/v2/\{userId\}/contacts/\{contactId\}/workInfo/communication

** Mandatory Parameters

 * userId=<Subscriber's email address or emailAlias>
 
 * contactId=<contact Id, which is returned after calling create contact API>
 
** Mandatory Parameters (Only when using Open Xchange backend)

 * sessionId=<Open Xchange session Id returned after calling Login API>
 
 * cookieString=<Open Xchange cookie, returned after calling Login API>

** REST URL Response

 * Success Response - HTTP 200 with the following JSON response in the body

+--
{
 "phone1": "123",
 "phone2": "123",
 "mobile": "Y3",
 "fax": "123",
 "email": "Y5@gh.com",
 "imAddress": ""
}
+--

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

+--
{
 "code": "ABS_WORKINFO_COMMUNICATION_UNABLE_TO_GET",
 "requestParams": "{folder=[22], id=[1], Cookie=[open-xchange-secret-GCkr80Iw3EeH6OGaufjXdA=b4eeb4593c424d71a47869e9aeb1a036; JSESSIONID=41871c9e3961471cbb2f4000eecfa62c.APP1; open-xchange-public-session=84d00f023f2c41af9fc2bd0768d337ee], contactId=[1, 1], session=[4668a314b26a42e7aed74b06e833a56a1], sessionId=[4668a314b26a42e7aed74b06e833a56a1], userId=[abcd1234, abcd1234], action=[get]}",
 "operationType": "ContactsBaseService:GET",
 "shortMessage": "Error in getting Contacts Base.",
 "longMessage": "Your session 4668a314b26a42e7aed74b06e833a56a1 expired. Please start a new browser session."
}
+--


** Error Codes

*** API specific errors

+--
    <error code="ABS_INVALID_USERNAME" message="Invalid username." />
    <error code="ABS_INVALID_CONTACT" message="Invalid contact id." />
    <error code="ABS_INVALID_SESSION" message="Invalid session." />
    <error code="ABS_INVALID_COOKIE" message="Invalid cookie." />
    <error code="ABS_WORKINFO_COMMUNICATION_UNABLE_TO_GET" message="Error in getting Contacts Workinfo Communication." />
    <error code="ABS_OX_ERROR" message="Open-Xchange error." />
+--

*** Common errors 

 * {{{../../errors/common-errors.html} Common errors}}
