 ------------------------------------------------------------------------------
 msisdnDeactivationFlow
 ------------------------------------------------------------------------------
 mxos-dev
 ------------------------------------------------------------------------------

msisdnDeactivationFlow API
 
 The deactivation flow will disable the user preferences on the features where the given MSISDN was configured.

[../../images/msisdnDeactivationFlow.png]

* API Description

** Invoking using SDK

 *  void IMsisdnDeactivationService.process(final Map<String, List<String>> inputParams) throws MxOSException
 
** Invoking using REST URL

 * URL - POST http://mxosHost:mxosPort/mxos/msisdnDeactivationFlow/\{msisdn\}

** Mandatory Parameters

 * msisdn=<MSISDN to be deactivated in International Format Ex: + followed by 10/11 digits(+32478123456)>
 
   <Note:> With REST context, "+" has to be replaced with "%2B". Ex: %2B32478123456
 
** REST URL Response

 * Success Response - HTTP 200 without body

 * Failure Response: - HTTP 4xx or HTTP 5xx with JSON MxOS Error object in the body

** Error Codes

*** API specific errors

+--
    <error code="FIONA_FLOW_INVALID_MSISDN" customCode="300" message="Invalid MSISDN." />
    <error code="MAA_CONNECTION_ERROR" customCode="303" message="MAA error - Communication error." />
    <error code="MBX_UNABLE_TO_SEARCH" message="Unable to perform Mailbox SEARCH operation." />
+--

*** Common errors 

 * {{{../errors/common-errors.html} Common errors}}