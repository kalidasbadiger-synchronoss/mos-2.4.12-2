/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

/**
 * Interface for Requests MBean.
 *
 * @author ajeswani
 */
public interface RequestHandlersMBean {

    /**
     * Number of backend requests currently executing.  
     *
     * @return no. of currently executing requests.
     */
    long getNumActiveRequests();

    /**
     * Total number of request so far.
     *
     * @return total no. of requests.
     */
    long getNumTotalRequests();

    /**
     * Average processing time for all requests so far.
     *
     * @return avg. processing time.
     */
    long getAvgProcessingTime();

    /**
     * Max processing time of all requests so far.
     *
     * @return max time taken for any request in milliseconds.
     */
    long getMaxProcessingTime();

    /**
     * Min processing time of all requests so far.
     *
     * @return min time taken for any request in milliseconds.
     */
    long getMinProcessingTime();

    /**
     * Reset max processing time to 0.
     *
     */
    void resetMaxProcessingTime();

    /**
     * Reset min processing time to 0.
     *
     */
    void resetMinProcessingTime();
}
