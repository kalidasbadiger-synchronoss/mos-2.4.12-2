/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.setup;

/**
 * Interface for JMX constants.
 * 
 * @author ajeswani
 */
public interface JMXConstants {
    String BROKEN_CONNECTIONS_MBEAN =
        "com.opwvmsg.mxos.monitoring:type=BrokenConnections";
    String CONNECTION_POOLS_MBEAN =
        "com.opwvmsg.mxos.pooling:type=ConnectionPools";
    String REQUEST_HANDLERS_MBEAN =
        "com.opwvmsg.mxos.requests:type=RequestProcessors";

    long SECONDS_IN_MILLIS = 1000000;
}
