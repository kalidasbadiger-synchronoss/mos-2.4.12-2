/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

/**
 * Interface for Connection Pools Mbean.
 * 
 * @author ajeswani
 */
public interface ConnectionPoolsMBean {

    /**
     * Max no. of total available ldap connections.
     * 
     * @return max ldap conenctions.
     */
    long getNumMaxLDAPConnections();

    /**
     * Max no of total available ldap search connections.
     * 
     * @return max ldap search connections.
     */
    long getNumMaxLDAPSearchConnections();

    /**
     * No. of currently used ldap connections.
     * 
     * @return active ldap connections.
     */
    long getNumActiveLDAPConnections();

    /**
     * No. of currently used ldap search connections.
     * 
     * @return active ldap search connections.
     */
    long getNumActiveLDAPSearchConnections();

    /**
     * Max no. of total available mmg connections for password recovery SMS
     * connections.
     * 
     * @return max mmg connections.
     */
    long getNumMaxMMGConnections();

    /**
     * No. of currently used mmg connections.
     * 
     * @return active mmg connections.
     */
    long getNumActiveMMGConnections();

    /**
     * Max no. of total available mta connections.
     * 
     * @return max mta conenctions.
     */
    long getNumMaxExternalMTAConnections();

    /**
     * No. of currently used mta connections.
     * 
     * @return active mta connections.
     */
    long getNumActiveExternalMTAConnections();

    /**
     * Max no. of total available maa connections.
     * 
     * @return max maa conenctions.
     */
    long getNumMaxMAAConnections();

    /**
     * No. of currently used maa connections.
     * 
     * @return active maa connections.
     */
    long getNumActiveMAAConnections();

    /**
     * Max no. of total available Http Notify connections.
     * 
     * @return max Http Notify connections.
     */
    long getNumMaxNotifyConnections();

    /**
     * No. of currently used Http Notify connections.
     * 
     * @return active Http Notify connections.
     */
    long getNumActiveNotifyConnections();

    /**
     * Max no. of total available mss meta connections.
     * 
     * @return max mss meta connections.
     */
    long getNumMaxMssMetaConnections();

    /**
     * No. of currently used mss meta connections.
     * 
     * @return active mss meta connections.
     */
    long getNumActiveMssMetaConnections();

    /**
     * Max no. of total available mss sl meta connections.
     * 
     * @return max mss sl meta connections.
     */
    long getNumMaxMssSLMetaConnections();

    /**
     * No. of currently used mss sl meta connections.
     * 
     * @return active mss sl meta connections.
     */
    long getNumActiveMssSLMetaConnections();

    /**
     * Max no. of total available ox address book connections.
     * 
     * @return max ox address book connections.
     */
    long getNumMaxOXAddressBookConnections();

    /**
     * No. of currently used ox address book connections.
     * 
     * @return active ox address book connections.
     */
    long getNumActiveOXAddressBookConnections();

    /**
     * Max no. of total available ox settings book connections.
     * 
     * @return max ox settings connections.
     */
    long getNumMaxOXSettingsConnections();

    /**
     * No. of currently used ox settings connections.
     * 
     * @return active ox settings connections.
     */
    long getNumActiveOXSettingsConnections();

    /**
     * Max no. of total available ox http settings book connections.
     * 
     * @return max ox http settings connections.
     */
    long getNumMaxOXHttpSettingsConnections();

    /**
     * No. of currently used ox http settings connections.
     * 
     * @return active ox http settings connections.
     */
    long getNumActiveOXHttpSettingsConnections();

    /**
     * Max no. of total available mail box connections.
     * 
     * @return max mail box connections.
     */
    long getNumMaxMailBoxConnections();

    /**
     * No. of currently used mail box connections.
     * 
     * @return active mail box connections.
     */
    long getNumActiveMailBoxConnections();

    /**
     * Max no. of total available we address book connections.
     * 
     * @return max we address book connections.
     */
    long getNumMaxWEAddressBookConnections();

    /**
     * No. of currently used we address book connections.
     * 
     * @return active we address book connections.
     */
    long getNumActiveWEAddressBookConnections();

    /**
     * Max no. of total available meg connections.
     * 
     * @return max meg connections.
     */
    long getNumMaxMegConnections();

    /**
     * No. of currently used meg connections.
     * 
     * @return active meg connections.
     */
    long getNumActiveMegConnections();

    /**
     * Max no. of total available http connections.
     * 
     * @return max http connections.
     */
    long getNumMaxHttpConnections();

    /**
     * No. of currently used http connections.
     * 
     * @return active http connections.
     */
    long getNumActiveHttpConnections();
}
