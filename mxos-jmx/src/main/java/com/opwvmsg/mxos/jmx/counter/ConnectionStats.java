/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.counter;

import java.util.concurrent.atomic.AtomicLong;

/**
 * Counter for connection stats.
 * 
 * @author ajeswani
 */
public enum ConnectionStats {
    LDAP, ACTIVE_LDAP, LDAPSEARCH, ACTIVE_LDAPSEARCH, MMG, ACTIVE_MMG, EXTERNAL_MTA, ACTIVE_EXTERNAL_MTA, MAA, ACTIVE_MAA, NOTIFY, ACTIVE_NOTIFY, MSSMETA, ACTIVE_MSSMETA, MSSSLMETA, ACTIVE_MSSSLMETA, OXADDRESSBOOK, ACTIVE_OXADDRESSBOOK, OXSETTINGS, ACTIVE_OXSETTINGS, OXHTTPSETTINGS, ACTIVE_OXHTTPSETTINGS, MAILBOX, ACTIVE_MAILBOX, WEADDRESSBOOK, ACTIVE_WEADDRESSBOOK, MEG, ACTIVE_MEG, HTTP, ACTIVE_HTTP;

    private AtomicLong count = new AtomicLong();

    /**
     * increment.
     */
    public void increment() {
        count.getAndIncrement();
    }

    /**
     * decrement.
     */
    public void decrement() {
        count.getAndDecrement();
    }

    /**
     * reset.
     */
    public void reset() {
        count.set(0);
    }

    /**
     * getCount.
     * 
     * @return long
     */
    public long getCount() {
        return count.get();
    }

    /**
     * setCount.
     * 
     * @param count long
     */
    public void setCount(long count) {
        this.count.set(count);
    }
}
