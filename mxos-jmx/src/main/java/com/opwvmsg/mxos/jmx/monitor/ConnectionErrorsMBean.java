/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id:$
 */

package com.opwvmsg.mxos.jmx.monitor;

/**
 * Interface for Connection Errors Mbean.
 *
 * @author ajeswani
 */
public interface ConnectionErrorsMBean {

    /**
     * Total no. of ldap connection errors.
     *
     * @return no. of ldap connection errors.
     */
    long getNumLDAPConnectionErrors();

    /**
     * Total no. of ldap search connection errors.
     *
     * @return no. of ldap search connection errors.
     */
    long getNumLDAPSearchConnectionErrors();

    /**
     * Total no. of mss connection errors.
     *
     * @return no. of mss connection errors.
     */
    long getNumMSSConnectionErrors();

    /**
     * Total no. of maa connection errors.
     *
     * @return no. of maa connection errors.
     */
    long getNumMAAConnectionErrors();

    /**
     * Total no. of SDUP connection errors.
     *
     * @return no. of SDUP connection errors.
     */
    long getNumSDUPConnectionErrors();

    /**
     * Total no. of MMG connection errors.
     *
     * @return no. of MMG connection errors.
     */
    long getNumMMGConnectionErrors();

    /**
     * Total no. of VAMP connection errors.
     *
     * @return no. of VAMP connection errors.
     */
    long getNumVAMPConnectionErrors();

    /**
     * Total no. of FBI connection errors.
     *
     * @return no. of FBI connection errors.
     */
    long getNumFBIConnectionErrors();

    /**
     * Total no. of MTA connection errors.
     *
     * @return no. of MTA connection errors.
     */
    long getNumMTAConnectionErrors();

    /**
     * Total no. of ConfigDB connection errors.
     *
     * @return no. of ConfigDB connection errors.
     */
    long getNumConfigDBConnectionErrors();
    
    /**
     * Total no. of Notify connection errors.
     *
     * @return no. of Notify connection errors.
     */
    long getNumNotifyConnectionErrors();

    /**
     * Reset number of ldap connection errors to 0.
     *
     */
    void resetNumLDAPConnectionErrors();

    /**
     * Reset number of ldap search connection errors to 0.
     *
     */
    void resetNumLDAPSearchConnectionErrors();

    /**
     * Reset number of mss connection errors to 0.
     *
     */
    void resetNumMSSConnectionErrors();

    /**
     * Reset number of maa connection errors to 0.
     *
     */
    void resetNumMAAConnectionErrors();

    /**
     * Reset number of SDUP connection errors to 0.
     *
     */
    void resetNumSDUPConnectionErrors();

    /**
     * Reset number of MMG connection errors to 0.
     *
     */
    void resetNumMMGConnectionErrors();

    /**
     * Reset number of VAMP connection errors to 0.
     *
     */
    void resetNumVAMPConnectionErrors();

    /**
     * Reset number of FBI connection errors to 0.
     *
     */
    void resetNumFBIConnectionErrors();

    /**
     * Reset number of MTA connection errors to 0.
     *
     */
    void resetNumMTAConnectionErrors();

    /**
     * Reset number of ConfigDB connection errors to 0.
     *
     */
    void resetNumConfigDBConnectionErrors();
    
    /**
     * Reset number of Notify connection errors to 0.
     *
     */
    void resetNumNotifyConnectionErrors();

}
