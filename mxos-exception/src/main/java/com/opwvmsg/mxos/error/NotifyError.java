/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Notify Service errors.
 * 
 * @author mxos-dev
 */
public enum NotifyError {
    /* Notify related Errors */
    NTF_INVALID_TOPIC,
    NTF_TOPIC_NOT_FOUND,
    NTF_UNABLE_TO_READ,
    NTF_UNABLE_TO_DELETE,
    NTF_UNABLE_TO_PUBLISH,
    NTF_INVALID_PUBLISH_MESSAGE,
    
    /* Subscription related Errors */
    NTF_SUBSCRIPTION_NOT_FOUND,
    NTF_INVALID_SUBSCRIPTION,
    NTF_UNABLE_TO_CREATE_SUBSCRIPTION,
    NTF_UNABLE_TO_READ_SUBSCRIPTION,
    NTF_UNABLE_TO_DELETE_SUBSCRIPTION
}
