/*
 * Copyright (c) 2012 Openwave Messaging Systems Inc. All rights reserved.
 *
 * The copyright to the computer software herein is the property of
 * Openwave Systems Inc. The software may be used and/or copied only
 * with the written permission of Openwave Systems Inc. or in accordance
 * with the terms and conditions stipulated in the agreement/contract
 * under which the software has been supplied.
 *
 * $Id: $
 */
package com.opwvmsg.mxos.error;

/**
 * Saml Service errors.
 * 
 * @author mxos-dev
 */
public enum SamlError {
    /* Saml related Errors */
    SAML_INVALID_TARGET_URL,
    SAML_INVALID_ASSERTION,
    SAML_ASSERTIONS_NOT_FOUND,
    SAML_ASSERTION_EXPIRED,
    SAML_ASSERTION_NOT_YET_VALID,
    SAML_ASSERTION_INVALID_ISSUER,
    SAML_ASSERTION_INVALID_DIGITAL_SIGNATURE,
    SAML_ASSERTION_AUTHENTICATION_FAILURE,
    SAML_UNABLE_TO_CREATE_AUTHENTICATION_REQUEST,
    SAML_UNABLE_TO_DECODE_SAML_ASSERTION
}
