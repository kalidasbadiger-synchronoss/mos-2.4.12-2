package com.googlecode.jsonschema2pojo;

import java.io.IOException;
import java.net.URL;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;

import com.googlecode.jsonschema2pojo.rules.RuleFactory;
import com.googlecode.jsonschema2pojo.rules.RuleFactoryImpl;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JPackage;

/**
 * Default implementation of the {@link SchemaMapper} interface, accepting a map
 * of behavioural properties that may influence code generation.
 */
public class SchemaMapperImpl implements SchemaMapper {

    private final RuleFactory ruleFactory;

    /**
     * Create a schema mapper with the given {@link RuleFactory}.
     *
     * @param ruleFactory
     *            A factory used by this mapper to create Java type generation
     *            rules.
     */
    public SchemaMapperImpl(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    /**
     * Create a schema mapper with the default {@link RuleFactory}
     * implementation.
     *
     * @see RuleFactoryImpl
     */
    public SchemaMapperImpl() {
        this(new RuleFactoryImpl());
    }

    @Override
    public void generate(JCodeModel codeModel, String className,
            String packageName, URL schemaUrl) throws IOException {

        JPackage jpackage = codeModel._package(packageName);

        ObjectNode schemaNode = new ObjectMapper().createObjectNode();
        schemaNode.put("$ref", schemaUrl.toString());

        ruleFactory.getSchemaRule()
                .apply(className, schemaNode, jpackage, null);

    }

}
