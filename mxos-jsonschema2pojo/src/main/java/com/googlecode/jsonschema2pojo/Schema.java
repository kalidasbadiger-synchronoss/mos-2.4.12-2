package com.googlecode.jsonschema2pojo;

import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import com.googlecode.jsonschema2pojo.exception.GenerationException;
import com.sun.codemodel.JType;

/**
 * A JSON Schema document.
 */
public final class Schema {

    private static Map<URI, Schema> schemas = new HashMap<URI, Schema>();

    private static FragmentResolver fragmentResolver =
            new FragmentResolverImpl();

    private URI id;
    private JsonNode content;
    private JType javaType;

    private Schema(URI id, JsonNode content) {
        this.id = id;
        this.content = content;
    }

    /**
     * Create or look up a new schema which has the given ID and read the
     * contents of the given ID as a URL. If a schema with the given ID is
     * already known, then a reference to the original schema will be returned.
     *
     * @param id
     *            the id of the schema being created
     * @return a schema object containing the contents of the given path
     */
    public static synchronized Schema create(URI id) {

        if (!schemas.containsKey(id)) {

            try {
                JsonNode content = new ObjectMapper().readTree(id.toURL()
                        .openStream());

                if (id.toString().contains("#")) {
                    content = fragmentResolver.resolve(content, '#'
                            + StringUtils.substringAfter(id.toString(), "#"));
                }

                schemas.put(id, new Schema(id, content));
            } catch (IOException e) {
                throw new GenerationException(e);
            }
        }

        return schemas.get(id);
    }

    /**
     * Create or look up a new schema using the given schema as a parent and the
     * path as a relative reference. If a schema with the given parent and
     * relative path is already known, then a reference to the original schema
     * will be returned.
     *
     * @param parent
     *            the schema which is the parent of the schema to be created.
     * @param path
     *            the relative path of this schema (will be used to create a
     *            complete URI by resolving this path against the parent
     *            schema's id)
     * @return a schema object containing the contents of the given path
     */
    public static Schema create(Schema parent, String path) {

        if (path.equals("#")) {
            return parent;
        }

        path = StringUtils.stripEnd(path, "#?&/");

        URI id = (parent == null) ? URI.create(path) : parent.getId().resolve(
                path);

        return create(id);

    }
    /**
     * Get Java Type.
     * @return type type
     */
    public JType getJavaType() {
        return javaType;
    }
    /**
     * Set Java Type.
     * @param javaType javaType
     */
    public void setJavaType(JType javaType) {
        this.javaType = javaType;
    }
    /**
     * Set Java Type if empty.
     * @param aJavaType javaType
     */
    public void setJavaTypeIfEmpty(JType aJavaType) {
        if (this.getJavaType() == null) {
            this.setJavaType(aJavaType);
        }
    }
    /**
     * get Id.
     * @return id id
     */
    public URI getId() {
        return id;
    }
    /**
     * Get Content.
     * @return content content
     */
    public JsonNode getContent() {
        return content;
    }
    /**
     * is Generated.
     * @return true if generated
     */
    public boolean isGenerated() {
        return (javaType != null);
    }
    /**
     * clear Cache.
     */
    public static synchronized void clearCache() {
        schemas.clear();
    }

}
