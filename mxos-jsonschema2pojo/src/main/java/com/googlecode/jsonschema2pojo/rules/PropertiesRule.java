package com.googlecode.jsonschema2pojo.rules;

import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.annotate.JsonPropertyOrder;

import com.googlecode.jsonschema2pojo.Schema;
import com.sun.codemodel.JAnnotationArrayMember;
import com.sun.codemodel.JDefinedClass;

/**
 * Applies the "properties" schema rule.
 *
 * @see <a
 *      href="http://tools.ietf.org/html/draft-zyp-json-schema-02#section-5.2">
 *      http://tools.ietf.org/html/draft-zyp-json-schema-02#section-5.2</a>
 */
public class PropertiesRule implements
        SchemaRule<JDefinedClass, JDefinedClass> {

    private final RuleFactory ruleFactory;
    /**
     * Constructor.
     * @param ruleFactory ruleFactory
     */
    protected PropertiesRule(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    /**
     * Applies this schema rule to take the required code generation steps.
     * <p>
     * For each property present within the properties node, this rule will
     * invoke the 'property' rule provided by the given schema mapper.
     *
     * @param nodeName
     *            the name of the node for which properties are being added
     * @param node
     *            the properties node, containing property names and their
     *            definition
     * @param jclass
     *            the Java type which will have the given properties added
     * @param schema
     *            schema
     * @return the given jclass
     */
    @Override
    public JDefinedClass apply(String nodeName, JsonNode node,
            JDefinedClass jclass, Schema schema) {

        for (Iterator<String> properties = node.getFieldNames(); properties
                .hasNext();) {
            String property = properties.next();

            ruleFactory.getPropertyRule().apply(property, node.get(property),
                    jclass, schema);
        }

        addOrderingAnnotation(node, jclass);

        return jclass;
    }

    private void addOrderingAnnotation(JsonNode node, JDefinedClass jclass) {

        JAnnotationArrayMember annotationValue = jclass.annotate(
                JsonPropertyOrder.class).paramArray("value");

        for (Iterator<String> properties = node.getFieldNames(); properties
                .hasNext();) {
            annotationValue.param(properties.next());
        }

    }
}
