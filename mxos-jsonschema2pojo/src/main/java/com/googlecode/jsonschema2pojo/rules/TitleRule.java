package com.googlecode.jsonschema2pojo.rules;

import org.codehaus.jackson.JsonNode;

import com.googlecode.jsonschema2pojo.Schema;
import com.sun.codemodel.JDocComment;
import com.sun.codemodel.JDocCommentable;

/**
 * Applies the "title" property property.
 *
 * @see <a
 *      href="http://tools.ietf.org/html/draft-zyp-json-schema-03#section-5.21">
 *      http://tools.ietf.org/html/draft-zyp-json-schema-03#section-5.21</a>
 */
public class TitleRule implements SchemaRule<JDocCommentable, JDocComment> {
    /**
     * Default constructor.
     */
    protected TitleRule() {
    }

    /**
     * Applies this schema rule to take the required code generation steps.
     * <p>
     * When a title node is found and applied with this rule, the value of the
     * title is added as a JavaDoc comment. This rule is typically applied to
     * the generated field, generated getter and generated setter for the
     * property.
     * <p>
     * Note that the title is always inserted at the top of the JavaDoc comment.
     *
     * @param nodeName
     *            the name of the property to which this title applies
     * @param node
     *            the "title" schema node
     * @param generatableType
     *            comment-able code generation construct, usually a field or
     *            method, which should have this title applied
     * @param schema
     *            the schema document content
     * @return the JavaDoc comment created to contain the title
     */
    @Override
    public JDocComment apply(String nodeName, JsonNode node,
            JDocCommentable generatableType, Schema schema) {
        JDocComment javadoc = generatableType.javadoc();

        javadoc.add(0, node.getTextValue() + "\n<p>\n");

        return javadoc;
    }

}
