package com.googlecode.jsonschema2pojo.rules;

import org.apache.commons.lang.StringUtils;

import java.util.List;
import java.util.Set;

import org.codehaus.jackson.JsonNode;

import com.googlecode.jsonschema2pojo.Schema;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JPackage;
import com.sun.codemodel.JType;

/**
 * Applies the "type":"array" schema rule.
 *
 * @see <a
 *      href="http://tools.ietf.org/html/draft-zyp-json-schema-02#section-5.3">
 *      http://tools.ietf.org/html/draft-zyp-json-schema-02#section-5.3</a>
 */
public class ArrayRule implements SchemaRule<JPackage, JClass> {

    private final RuleFactory ruleFactory;
    /**
     * Constructor.
     * @param ruleFactory ruleFactory
     */
    protected ArrayRule(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    /**
     * Applies this schema rule to take the required code generation steps.
     * <p>
     * When constructs of type "array" appear in the schema, these are mapped to
     * Java collections in the generated POJO. If the array is marked as having
     * "uniqueItems" then the resulting Java type is {@link Set}, if not, then
     * the resulting Java type is {@link List}. The schema given by "items" will
     * decide the generic type of the collection.
     * <p>
     * If the "items" property requires newly generated types, then the type
     * name will be the singular version of the nodeName (unless overridden by
     * the javaType property) e.g.
     * <p>
     *
     * <pre>
     *  "fooBars" : {
     *  "type":"array", "uniqueItems":"true", "items":{type:"object"}}
     *  ==>
     *  {@code Set<FooBar> getFooBars(); }
     * </pre>
     *
     * @param nodeName
     *            the name of the property which has type "array"
     * @param node
     *            the schema "type" node
     * @param jpackage
     *            the package into which newly generated types should be added
     * @param schema
     *            schema
     * @return the Java type associated with this array rule, either {@link Set}
     *         or {@link List}, narrowed by the "items" type
     */
    @Override
    public JClass apply(String nodeName, JsonNode node, JPackage jpackage,
            Schema schema) {

        boolean uniqueItems = node.has("uniqueItems")
                && node.get("uniqueItems").getBooleanValue();

        JType itemType;
        if (node.has("items")) {
            itemType = ruleFactory.getSchemaRule()
                    .apply(makeSingular(nodeName), node.get("items"), jpackage,
                            schema);
        } else {
            itemType = jpackage.owner().ref(Object.class);
        }

        if (uniqueItems) {
            return jpackage.owner().ref(Set.class).narrow(itemType);
        } else {
            return jpackage.owner().ref(List.class).narrow(itemType);
        }
    }

    private String makeSingular(String nodeName) {
        if (StringUtils.endsWithIgnoreCase(nodeName, "s")){
            if (isEndingWithSpecialPlural(nodeName)) {
                return StringUtils.removeEnd(
                        StringUtils.removeEnd(nodeName, "es"), "ES");
            } else {
                return StringUtils.removeEnd(
                        StringUtils.removeEnd(nodeName, "s"), "S");
            }
        } else {
            return nodeName;
        }
    }
    private boolean isEndingWithSpecialPlural(String nodeName){            
        String[] specialEnds = {"ses", "fes", "xes", "shes", "ches", "zes"};
        for (String specialEnd : specialEnds) {
            if (StringUtils.endsWithIgnoreCase(nodeName, specialEnd)) {
                return true;
            }
            continue;
        }
        return false;
    }

}
