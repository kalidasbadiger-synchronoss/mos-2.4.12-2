package com.googlecode.jsonschema2pojo.exception;

import com.sun.codemodel.JClass;

/**
 * Thrown to indicate that an attempt to create a new class failed, because a
 * class of the same name already exists (either on the classpath or in the
 * current map of types to be generated.
 */
public class ClassAlreadyExistsException extends Exception {

    private static final long serialVersionUID = 342342342342L;
    private final JClass existingClass;

    /**
     * Creates a new exception where the given existing class was found to
     * conflict with an attempt to create a new class.
     *
     * @param existingClass
     *            the class already present on the classpath (or in the map of
     *            classes to be generated) when attempt to create a new class
     *            was made.
     */
    public ClassAlreadyExistsException(JClass existingClass) {
        this.existingClass = existingClass;
    }

    /**
     * Gets the corresponding existing class that caused this exception.
     *
     * @return the class already present on the classpath (or in the map of
     *         classes to be generated) when attempt to create a new class was
     *         made.
     */
    public JClass getExistingClass() {
        return existingClass;
    }

}
